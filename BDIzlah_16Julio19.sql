-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: desclub.com.mx    Database: plataforma_lealtad3
-- ------------------------------------------------------
-- Server version	5.1.73

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `rel_categoria_subcat`
--

DROP TABLE IF EXISTS `rel_categoria_subcat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rel_categoria_subcat` (
  `idRelacion` int(11) NOT NULL AUTO_INCREMENT,
  `idCategoria` varchar(45) NOT NULL,
  `idSubcat` varchar(45) NOT NULL,
  `activo` varchar(45) NOT NULL DEFAULT '1',
  PRIMARY KEY (`idRelacion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rel_categoria_subcat`
--

LOCK TABLES `rel_categoria_subcat` WRITE;
/*!40000 ALTER TABLE `rel_categoria_subcat` DISABLE KEYS */;
/*!40000 ALTER TABLE `rel_categoria_subcat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_area`
--

DROP TABLE IF EXISTS `tbl_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_area` (
  `id_area` int(3) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `icono` varchar(255) DEFAULT NULL,
  `activo` int(2) DEFAULT NULL,
  PRIMARY KEY (`id_area`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_area`
--

LOCK TABLES `tbl_area` WRITE;
/*!40000 ALTER TABLE `tbl_area` DISABLE KEYS */;
INSERT INTO `tbl_area` VALUES (1,'Administración','cogs',0),(2,'Reportes','chart-bar',0),(3,'P. Venta','award',0),(4,'Mis Registros','address-card',0),(5,'Info App','mobile-alt',0);
/*!40000 ALTER TABLE `tbl_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_campanias`
--

DROP TABLE IF EXISTS `tbl_campanias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_campanias` (
  `id_camp` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `activo` int(2) DEFAULT NULL,
  PRIMARY KEY (`id_camp`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_campanias`
--

LOCK TABLES `tbl_campanias` WRITE;
/*!40000 ALTER TABLE `tbl_campanias` DISABLE KEYS */;
INSERT INTO `tbl_campanias` VALUES (1,'Cashback',0),(2,'Custom',0),(3,'Código de Descuento',0),(4,'Envío Gratis',0),(5,'Gift',0),(6,'% Código de Descuento',0),(7,'Código por Valor',1),(8,'Bono de Cumpleaños',0),(9,'Aniversario de Registro',0),(10,'Bono de Bienvenida',0);
/*!40000 ALTER TABLE `tbl_campanias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_campos_registro`
--

DROP TABLE IF EXISTS `tbl_campos_registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_campos_registro` (
  `id_campos_registro` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_campos_registro`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_campos_registro`
--

LOCK TABLES `tbl_campos_registro` WRITE;
/*!40000 ALTER TABLE `tbl_campos_registro` DISABLE KEYS */;
INSERT INTO `tbl_campos_registro` VALUES (1,'nombre'),(3,'telefono'),(5,'email'),(6,'rfc'),(7,'id Tarjeta');
/*!40000 ALTER TABLE `tbl_campos_registro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_cat_premios_productos_servicios`
--

DROP TABLE IF EXISTS `tbl_cat_premios_productos_servicios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_cat_premios_productos_servicios` (
  `id_cat_premios_productos_servicios` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `valor_puntos` int(11) DEFAULT NULL,
  `valor_visitas` int(3) DEFAULT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `detalle` text,
  `cantidad` int(11) DEFAULT NULL,
  `activo` int(11) DEFAULT '0',
  PRIMARY KEY (`id_cat_premios_productos_servicios`),
  KEY `id_tipo_idx` (`id_tipo`),
  CONSTRAINT `tbl_cat_premios_productos_servicios_ibfk_1` FOREIGN KEY (`id_tipo`) REFERENCES `tbl_tipo` (`id_tipo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_cat_premios_productos_servicios`
--

LOCK TABLES `tbl_cat_premios_productos_servicios` WRITE;
/*!40000 ALTER TABLE `tbl_cat_premios_productos_servicios` DISABLE KEYS */;
INSERT INTO `tbl_cat_premios_productos_servicios` VALUES (1,'Cherry Matcha',10,0,1,'te.jpg','',100,0),(2,'MATCHAI',100,0,1,'canela.jpg','',100,0),(3,'JADE MATCHA',2000,0,1,'fraptea.jpg','',10,0),(4,'CARAMELLO BELGIO',100,0,1,'','',1000,0),(5,'MINTASTIC',100,0,1,'','',1000,0),(6,'SWISS KISS',100,0,1,'','',1000,0),(7,'COCO BON BON',100,0,1,'','',1000,0),(8,'SPICY & FUNK',100,0,1,'','',1000,1),(9,'STAR LEBLANC',100,0,1,'','',1000,1),(10,'SPICE CANELÉ',1000,0,2,'no_image.png','',100,1),(11,'ALMOND SAVARIN',1000,0,2,'no_image.png','',100,1),(12,'PARIS-BREST VANILLE',1000,0,2,'no_image.png','',100,1),(13,'EIFFEL CRULLER',1000,0,2,'no_image.png','',100,1),(14,'MARSHMALLOW MACARON',1000,0,2,'no_image.png','',100,1),(15,'CARROT SOUFFLÉ',1000,0,2,'no_image.png','Rooibos(Se infusiona en 5 min)Rooibos, pedazos de zanahoria, pedazos de manzana, canela, cascara de naranja, pasas, escaramujos, rebanadas de manzana, clavo, calendula, nuez moscada, pedazos de vainilla',100,1),(16,'APPLE CRÉPE',1000,0,2,'no_image.png','Tisana(Se infusiona en 5 min)Pedazos de manzana, raíz de achicoria tostada, canela, almendra fileteada, pedazos de vainilla',100,1),(17,'MAPLE BELGIAN WAFFLE',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, pasas, granos de cocoa, varas de canela, avellana caramelizada, pedazos de durazno, pedazos de vainilla',100,1),(18,'PINKY AMARETTI',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, pedazos de piña confitada, pedazos de papaya confitada, pedazos de betabel, varas de canela, pasas, chips de coco, almendras tostadas, palomitas de maiz',100,1),(19,'TULUM',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, trozos de manzana, flor de conchita azul, bayas del sauco, flor de hibisco, chips de coco, mora azul, zarzamora, frambuesa',100,1),(20,'IBIZA',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, pedazos de papaya confitada, pedazos de zanahoria,flamingo de caramelo, flor de hibisco, pedazos de mango confitado, pedazos de betabel, chips de coco, azafrán, flor de aciano',100,1),(21,'ST. TROPEZ',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, lemongrass, pedazos de piña confitada, flor de hibisco, pedazos de fresa, rebanadas de fresa, flor de aciano, hojas de rosa',100,1),(22,'MYKONOS',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, flor de hibisco, pedazos de guayaba, pedazos de naranja, canela, cardamomo, rebanadas de fresa, hojas de brote de rosa, flor de malva',100,1),(23,'ARUBA BEACH',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nPedazos de piña confitada, pedazos de papaya confitada, flor de hibisco, bayas del saúco, pedazos de betabel,  pedazos de fresa, pedazos de guayaba, pedazos de vainilla',100,1),(24,'IPANEMA',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana,Rebanadas de persimon, pedazos de papaya confitada, cascara de naranja,  lemongrass, pedazos de durazno, grosella roja, hojas de brote de rosas',100,1),(25,'UNICORN ELIXIR',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nFlor de hibisco, pasas, bayas del sauco, unicornio de caramelo, cerezas, pedazos de cereza acida',100,1),(26,'COSMOBERRY',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nArandanos, flor de hibisco, rebanadas de manzana.',100,1),(27,'BLUE BERRY LAGOON',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, pasas, pedazos de zanahoria, pedazos de betabel, saborizante, pedazos de yogurt, mora azul.',100,1),(28,'FRESA POP',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, pedazos de papaya confitada, pasas, pedazos de betabel, pedazos de fresas, palomitas de maiz.',100,1),(29,'BLOOMSBERRY COCKTAIL',1000,0,2,'no_image.png','Té Wulong\n(Se infusiona en 3 min)\nTe oolong, pasas, pedazos de manzana, pedazos de piña confitada, escaramujo, pedazos de manzana,  flor de hibisco, hinojo, baya de enebro, regaliz, pedazos de durazno, anis, brote de rosa, hojas de rosa, anis estrella, pedazos de mandarina, pedazos de fresa, pedazos de ruibarbo, pedazos de vainilla, pedazos de naranja, pedazos de toronja',100,1),(30,'SAMBUCHINO',1000,0,2,'no_image.png','Tisana / HoneyBush (Se infusiona en 5 min) Honeybush verde, pedazos de manzana, bayas del saúco, hojas de frambuesa, pedazos de fresa, grosella negra',100,1),(31,'PRINCESS CLEANSE',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, lemongrass, rooibos verde, pedazos de naranja, verbena, pedazos de vainilla',100,1),(32,'RITUAL DETOX',1000,0,2,'no_image.png','Té Verde\n(Se infusiona en 2 min)\nPedazos de manzana, te verde China Gunpowder, lemongrass, pedazos de piña confitada, pedazos de jengibre, diente de leon, saborizante natural, cascara de limon, cilantro, hierba tulsi, arroz inflado, flor de naranjo, hojas de brote de rosa, raiz de ginseng',100,1),(33,'AQUA VITALE',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, pasas, pedazos de zanahoria, hojas de moringa, pedazos de betabel, flor de hibisco, pedazos de fresa',100,1),(34,'SUMMER BODY',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nCilantro*, menta nana*, hoja de ortiga*, cascara de naranja*, lemongrass*, pedazos de manzana*, canela*, trozos de jengibre*,  clavo*, cardamomo*\n*Certificado Organico',100,1),(35,'GINGER VITALITY',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana*, escaramujo*, hierba del sauce*, pedazos de zanahora*, hojas de frambuesa*, pedazos de jengibre, verbena*, hojas de malva*, flor del saúco*\n*Certificado Organico',100,1),(36,'SLIM TONIC',1000,0,2,'no_image.png','Té Wulong\n(Se infusiona en 3 min)\nTe Oolong, Te Pu Erh, escaramujos, pedazos de manzana, flor de hibisco, menta nana, pedazos de betabel, hojas de menta, rebanadas de fresa, hojas de flor, pedazos de manzana',100,1),(37,'CHAI WALA',1000,0,2,'no_image.png','Té Wulong\n(Se infusiona en 3 min.)\nCanela, Te oolong, pedazos de jengibre, raiz de achicoria tostada, pedazos de algarrobo, regaliz, clavo, cardamomo, pimienta.',100,1),(38,'NEW DELHI-CIOUS',1000,0,2,'no_image.png','Té blanco\n(Se infusiona en 2 min)\nTe blanco, canela, lemongrass, pedazos de piña confitada, chips de coco, pedazos de jengibre, hojas de moringa, clavo, saborizante natural, pimienta, cardamomo.',100,1),(39,'BINDI CHAI',1000,0,2,'no_image.png','Tisana\n(Se infusiona en 5 min)\nFlor de hibisco, pasas, pedazos de manzana, cascara de escaramujo, almendra fileteada, clavo.',100,1),(40,'CITRUS FRESH SCENT',1000,0,2,'no_image.png','Té Verde\n(Se infusiona en 2 min)\nTe Verde China Chun Mee, menta nana, verbena, lemongrass.',100,1),(41,'KYOTO GREEN GEMS',1000,0,2,'no_image.png','Té Verde\n(Se infusiona en 2 min)\nTe Verde - Sencha Japones, Bancha Japones, Genmaicha (Te, arroz), Kukicha, pedazos de fresa, grosella roja',100,1),(42,'EL TÉ DE LA GEISHA',1000,0,2,'no_image.png','Té Blanco / Té Verde(Se infusiona de 3 min)Te blanco, Te verde, cerezas, arandanos, escaramujo,  hojas de rosa, brote de rosa',100,1),(43,'SOL DE MATCHA',1000,0,2,'no_image.png','Té Verde y Matcha\n(Se infusiona de 30-45 seg.)\nTé Verde China Gundpowder,Té Verde Japónes Matcha,  trozos de papaya confitada, arroz tostado y arroz inflado.',100,1),(44,'ARUMI FRUTALLE',1000,0,2,'no_image.png','Té Verde\n(Se infusiona en 2 min)\nTe verde, trozos de papaya confitada, trozos de manzana, trozos de zanahoria,  lemongrass, brotes de calendúla, pasas, trozos de durazno',100,1),(45,'SILVER NEEDLE',1000,0,2,'no_image.png','Té blanco.RESERVA IMPERIAL\n(Se infusiona en 2 min)\nLa pureza y rareza de estos capullos de té blanco en forma de aguja le otorgan fama legendaria como uno de los 10 mejores tés que se producen en China. Durante siglos reservado para la familia imperial, este té es cosechado a mano con reverencia sólo dos días del año. Las hojas plateadas dotadas de una belleza única liberan un fragante sabor suave y sedoso, lleno de lujo y realeza.',100,1),(46,'MONKEY PICKED WULONG',1000,0,2,'no_image.png','Té Wulong RESERVA IMPERIAL\n(Se infusiona en 3 min)\nSegún cuenta la leyenda los monjes budistas entrenaban a los monos para recolectar las hojas superiores y más jóvenes de los árboles de té silvestres, creando este preciado tesoro de reserva imperial. El secreto radica en la recolección manual de las hojas, cuidando que éstas no se rompan para que desplieguen el característico sabor a orquídeas del más fino de los Oolongs del mundo.',100,1),(47,'GYOKURO IMPERIAL',1000,0,2,'no_image.png','Té Verde RESERVA IMPERIAL\n(Se infusiona de 30-45 seg.)\nLos arbustos de Gyokuro son cubiertos dos semanas antes de la cosecha para conservarla clorofila y concentrar el sabor y nutrientes del mejor de los tés japoneses cuyo sabor ofrece una complejidad en el color verde esmeralda de su infusión y su dulce sabor característico que lo convierten en uno de los favoritos de nuestra colección.',100,1),(48,'MENTA MARROQUI',1000,0,2,'no_image.png','Té Verde y hoja de menta\n(Se infusiona de 30-45 seg.)\nLos arbustos de Gyokuro son cubiertos dos semanas antes de la cosecha para conservarla clorofila y concentrar el sabor y nutrientes del mejor de los tés japoneses cuyo sabor ofrece una complejidad en el color verde esmeralda de su infusión y su dulce sabor característico que lo convierten en uno de los favoritos de nuestra colección.',100,1),(49,'JASMINE BIANCA',1000,0,2,'no_image.png','Té Verde y  Té Blanco\n(Se infusiona en 3 min)\nSe seleccionan las puntas  de las hojas más jovenes que se enrollan a mano en pequeñas esferas. Alto nivel de Antioxidantes',100,1),(50,'STICKY RICE',1000,0,2,'no_image.png','Té Wulong\n(Se infusiona en 3 min)\nUn té que sabe y huela a arroz pegajoso. Es una combinación única entre dulce y cremoso con notas satisfactorias de granos recien cocinados naturalmente. Un sorbo de este té y querras quedarte con el de por vida.',100,1),(51,'KOREAN JOONGJAK',1000,0,2,'no_image.png','Té Verde (Corea del Sur)\n(Se infusiona en 2 min)\nEs conocido como uno de los mejores tés verdes de Corea.',100,1),(52,'TAMARYOCUCHA',1000,0,2,'no_image.png','Té Verde (2017 Global Tea Competition Winners Hot Spring Loose Leaf)\n(Se infusiona en 45 seg.)\nSe produce principalmente en Kyushu en una isla al suroeste de Japón. Tiene un sabor suave y con astringencia ligera. Ayuda a eliminar los radicales libres y ayuda a disminuir los niveles de azúcar en la sangre.',100,1),(53,'MILKY WULONG',1000,0,2,'no_image.png','Té Wulong\n(Se infusiona en 3 min.)\nProveniente de las montañas de Wuyi en China, el sbor se caracteriza porque, unos días antes de la cosecha la temperatura desciende, lo que ocasiona que el sabor del té se altere',100,1),(54,'ENGLISH BREAKFAST',1000,0,2,'no_image.png','Té Negro\n(Se infusiona en 3 min.)\nSiéntete de la realeza con la elegancia y tradición en cada sorbo de este elegante té negro que despliega su majestuosidad en la infusión de sus hojas completas.',100,1),(55,'GOLDEN MONKEY',1000,0,2,'no_image.png','Té Negro\n(Se infusiona en 3 min.)\nLa tradición antigua y de exclusividad imperial, desde tiempos de la dinastía Song, hacen de este legendario té negro parte del top ten de tés chinos. Su delicioso sabor con tonos de cacao es obtenido tras la cuidadosa recolección de solo una hoja y un capullo.',100,1);
/*!40000 ALTER TABLE `tbl_cat_premios_productos_servicios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_categoria`
--

DROP TABLE IF EXISTS `tbl_categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_categoria` (
  `id_cat` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `activo` int(2) DEFAULT '0',
  PRIMARY KEY (`id_cat`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_categoria`
--

LOCK TABLES `tbl_categoria` WRITE;
/*!40000 ALTER TABLE `tbl_categoria` DISABLE KEYS */;
INSERT INTO `tbl_categoria` VALUES (1,'Matcha Moods','Matcha Moods',0),(2,'Chocolateas','Chocolateas',0),(3,'Pateasserie','Pateasserie',0),(4,'Beach Club','Beach Club',0),(5,'4 EveryBerry','4 EveryBerry',0),(6,'Wellness ','Wellness ',0),(7,'Chai Charm ','Chai Charm ',0),(8,'Kyoto Memories','Kyoto Memories',0),(9,'Orient Express','Orient Express',0);
/*!40000 ALTER TABLE `tbl_categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_categorias_segmento`
--

DROP TABLE IF EXISTS `tbl_categorias_segmento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_categorias_segmento` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_categoria` varchar(128) NOT NULL,
  `descripcion_categoria` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_categorias_segmento`
--

LOCK TABLES `tbl_categorias_segmento` WRITE;
/*!40000 ALTER TABLE `tbl_categorias_segmento` DISABLE KEYS */;
INSERT INTO `tbl_categorias_segmento` VALUES (1,'Aniversario Cumpleaños','Clientes proximos a cumplir años'),(2,'Aniversario Registro','Clientes que proximamente cumplen años de su registro'),(3,'Clientes que realizaron su última compra hace \"n\" días','Clientes que realizaron su ultima compra \'n\' días atrás'),(4,'Periodo de compra','Clientes que realizaron su compra en un rango de fechas específico'),(5,'Compras por punto de venta','Clientes que han realizado compras en un Punto de Venta específico'),(6,'Lista de Clientes Específico','Muestra los clientes que pueden ser ingresados al segmento'),(7,'Valor de Compra','Clientes que su valor de compra se encuentre en el rango establecido'),(8,'Genero','Clientes por género'),(9,'Edad','Clientes con una edad determinada'),(10,'Días de registro','Clientes que realizaron compras en días especificos'),(11,'Hora de registro','Clientes que realizaron compras en un horario específico'),(12,'Clientes que tienen un mínimo de \"n\" compras','Clientes que tienen un mínimo de \"n\" compras');
/*!40000 ALTER TABLE `tbl_categorias_segmento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_configuracion_proyecto`
--

DROP TABLE IF EXISTS `tbl_configuracion_proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_configuracion_proyecto` (
  `id_configuracion_proyecto` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_registro` int(11) DEFAULT NULL,
  `id_tipo_proyecto` int(11) DEFAULT NULL,
  `id_estilo` int(11) DEFAULT NULL,
  `bono_bienvenida` int(2) DEFAULT NULL,
  `bono_cumple` int(2) DEFAULT NULL,
  `id_tipo_comunicacion_registro` varchar(255) DEFAULT NULL,
  `id_tipo_comunicacion_acumular` varchar(255) DEFAULT NULL,
  `id_tipo_comunicacion_redimir` varchar(255) DEFAULT NULL,
  `id_tipo_comunicacion_periodica` varchar(255) DEFAULT NULL,
  `id_lapso_comunicacion_periodica` int(11) DEFAULT NULL,
  `porcentaje_margen` varchar(45) DEFAULT NULL,
  `puntos_por_peso` varchar(45) DEFAULT NULL,
  `num_min_visitas` int(11) DEFAULT NULL,
  `puntos_por_visita` varchar(45) DEFAULT NULL,
  `puntos_bienvenida` varchar(45) DEFAULT NULL,
  `puntos_cumple` varchar(45) DEFAULT NULL,
  `titulo_home` varchar(255) DEFAULT NULL,
  `detalle_home` varchar(255) DEFAULT NULL,
  `tel_marca` varchar(45) DEFAULT NULL,
  `mail_marca` varchar(255) DEFAULT NULL,
  `cta_face` varchar(255) DEFAULT NULL,
  `cta_twit` varchar(255) DEFAULT NULL,
  `sucursales` int(2) DEFAULT NULL,
  `menus` int(2) DEFAULT NULL,
  `promociones` int(2) DEFAULT NULL,
  `niveles` int(2) DEFAULT NULL,
  `id_tipo_punto_venta` int(11) DEFAULT NULL,
  `nsucursales` varchar(45) DEFAULT NULL,
  `app` int(2) DEFAULT NULL,
  `tipo_confirm_operacion` int(2) DEFAULT '1',
  PRIMARY KEY (`id_configuracion_proyecto`),
  KEY `id_tipo_registro_idx` (`id_tipo_registro`),
  KEY `id_tipo_proyecto_idx` (`id_tipo_proyecto`),
  KEY `id_estilo_idx` (`id_estilo`),
  KEY `id_periodo_comunicacion_idx` (`id_lapso_comunicacion_periodica`),
  CONSTRAINT `tbl_configuracion_proyecto_ibfk_1` FOREIGN KEY (`id_estilo`) REFERENCES `tbl_estilo` (`id_estilo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_configuracion_proyecto_ibfk_2` FOREIGN KEY (`id_lapso_comunicacion_periodica`) REFERENCES `tbl_periodo_comunicacion` (`id_periodo_comunicacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_configuracion_proyecto_ibfk_3` FOREIGN KEY (`id_tipo_proyecto`) REFERENCES `tbl_tipo_proyecto` (`id_tipo_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_configuracion_proyecto_ibfk_4` FOREIGN KEY (`id_tipo_registro`) REFERENCES `tbl_tipo_registro` (`id_tipo_registro`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_configuracion_proyecto`
--

LOCK TABLES `tbl_configuracion_proyecto` WRITE;
/*!40000 ALTER TABLE `tbl_configuracion_proyecto` DISABLE KEYS */;
INSERT INTO `tbl_configuracion_proyecto` VALUES (1,1,2,1,0,0,'1-2','1-2','1-2','2',2,'','',0,'10','','','IZLAH','TEA BAR','','contacto@izlah.com','https://www.facebook.com/IzlahMx/','',1,1,1,1,NULL,'20',1,1);
/*!40000 ALTER TABLE `tbl_configuracion_proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_estilo`
--

DROP TABLE IF EXISTS `tbl_estilo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_estilo` (
  `id_estilo` int(11) NOT NULL AUTO_INCREMENT,
  `logo_cte` varchar(255) DEFAULT NULL,
  `fondo_cte` varchar(255) DEFAULT NULL,
  `fondo_header` varchar(255) DEFAULT NULL,
  `color_primario` varchar(45) DEFAULT NULL,
  `color_secundario` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_estilo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_estilo`
--

LOCK TABLES `tbl_estilo` WRITE;
/*!40000 ALTER TABLE `tbl_estilo` DISABLE KEYS */;
INSERT INTO `tbl_estilo` VALUES (1,'logo.png','fondoss.jpeg',NULL,'#000000','#de3059');
/*!40000 ALTER TABLE `tbl_estilo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_forma_acumulacion`
--

DROP TABLE IF EXISTS `tbl_forma_acumulacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_forma_acumulacion` (
  `id_forma_acumulacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_forma_acumulacion`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_forma_acumulacion`
--

LOCK TABLES `tbl_forma_acumulacion` WRITE;
/*!40000 ALTER TABLE `tbl_forma_acumulacion` DISABLE KEYS */;
INSERT INTO `tbl_forma_acumulacion` VALUES (1,'Identificación con celular','celular'),(2,'Escaneo de código QR','qr'),(3,'Registro ID de la tarjeta','id'),(4,'Identificación con mail','mail');
/*!40000 ALTER TABLE `tbl_forma_acumulacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_log_transfer_registros`
--

DROP TABLE IF EXISTS `tbl_log_transfer_registros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_log_transfer_registros` (
  `id_log` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario_origen` int(11) DEFAULT NULL,
  `id_usuario_destino` int(11) DEFAULT NULL,
  `num_acumu_transfer` int(4) DEFAULT NULL,
  `num_reden_transfer` int(4) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `id_usuario_registro` int(11) DEFAULT NULL,
  `cant_p_v_transferidas_ac` int(11) DEFAULT NULL,
  `cant_p_v_transferidas_re` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_log`),
  KEY `id_usu_or_idx` (`id_usuario_origen`),
  KEY `id_usu_de_idx` (`id_usuario_destino`),
  KEY `id_usu_reg_idx` (`id_usuario_registro`),
  CONSTRAINT `tbl_log_transfer_registros_ibfk_1` FOREIGN KEY (`id_usuario_destino`) REFERENCES `tbl_usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_log_transfer_registros_ibfk_2` FOREIGN KEY (`id_usuario_origen`) REFERENCES `tbl_usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_log_transfer_registros_ibfk_3` FOREIGN KEY (`id_usuario_registro`) REFERENCES `tbl_usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_log_transfer_registros`
--

LOCK TABLES `tbl_log_transfer_registros` WRITE;
/*!40000 ALTER TABLE `tbl_log_transfer_registros` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_log_transfer_registros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mecanica_redencion`
--

DROP TABLE IF EXISTS `tbl_mecanica_redencion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mecanica_redencion` (
  `id_mecanica_redencion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `alias` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_mecanica_redencion`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mecanica_redencion`
--

LOCK TABLES `tbl_mecanica_redencion` WRITE;
/*!40000 ALTER TABLE `tbl_mecanica_redencion` DISABLE KEYS */;
INSERT INTO `tbl_mecanica_redencion` VALUES (1,'Télefono + ID de Usuario en Comercio','telefono'),(2,'ID Tarjeta ó APP en el Comercio','id'),(3,'Correo Electronico +ID de Usuario en Comercio','mail');
/*!40000 ALTER TABLE `tbl_mecanica_redencion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_menu`
--

DROP TABLE IF EXISTS `tbl_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `descripcion` varchar(512) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `precio` varchar(50) DEFAULT NULL,
  `activo` int(2) DEFAULT NULL,
  `id_cat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_menu`
--

LOCK TABLES `tbl_menu` WRITE;
/*!40000 ALTER TABLE `tbl_menu` DISABLE KEYS */;
INSERT INTO `tbl_menu` VALUES (1,'Cherry Matcha','Lo mejor de la vida es cuando encontramos personas que hacen de pequeños instantes en GRANDES MOMENTOS y con nuestra combinación Cherry Matcha ese gran momento se convertirá en un recuerdo inolvidable. Con esta atrevida combinación experimentaras sensaciones que difícilmente podrás repetir. Este BLEND fusiona las maravillas del Té Blanco con todas las bondades del MATCHA',NULL,'99.00',0,1),(2,'MATCHAI','Esta deliciosa combinación resalta los toques SPICY del CHAI, entregándonos un sabor intenso y placentero que te carga de energía.\nEL Matcha por si solo es un estimulante natural que aporta vitaminas, minerales y aminoácidos, elementos esenciales para los',NULL,'99.00',0,1),(3,'JADE MATCHA','Impresionante y deliciosa combinación de Tés Verdes con sabores extraordinarios muy refrescantes creando una perfecta armonía con las TEA PEARLS de MATCHA y ligeras notas de miel. Revitaliza y dale un twist a tu día con los refrescantes sabores que e...',NULL,'99.00',0,1),(4,'CARAMELLO BELGIO\n','Tisana\n(Se infusiona en 5 min)\nGranos de cocoa, pedazos de manzana, pedazos de chocolate, canela, pedazos de vainilla\n',NULL,'100 gr $420.00, Frapptea $69.00, Cal/Frio $49.00',0,2),(5,'MINTASTIC\n','Rooibos\n(Se infusiona en 5 min)\nRooibos, avellana caramelizada, pedazos de chocolate, hojas de hierbabuena\n',NULL,'100 gr $480.00, Frapptea $69.00, Cal/Frio $49.00',0,2),(6,'SWISS KISS','Rooibos\n(Se infusiona en 5 min)\nRooibos, pedazos de turron, pedazos de cacao, cascara de cacao, raíz de achicoria rostizada, pedazos de avellana, brote de hibisco blanco, almendra fileteada',NULL,'100 gr $480.00, Frapptea $69.00, Cal/Frio $49.00',0,2),(7,'COCO BON BON','Tisana\n(Se infusiona en 5 min)\n\"Pedazos de cocoa, pedazos de algarrobo, cascara de cacao, pedazos de chocolate, regaliz, anis, chips de coco, sal de mar, flor de calendula, paprika\nNOTA- contiene regaliz / gente con padecimiento de hipertension debe evitar el consumo excesivo\"',NULL,'100 gr $380.00, Frapptea $69.00, Cal/Frio $49.00',0,2),(8,'SPICY & FUNK','Rooibos\n(Se infusiona en 5 min)\nPedazos de cacao, Rooibos, pedazos de jengibre, raíz de achicoria tostada, cebada, canela, varas de canela,  cardamomo, pimienta negra',NULL,'100 gr $350.00, Frapptea $69.00, Cal/Frio $49.00',0,2),(9,'STAR LEBLANC','Rooibos\n(Se infusiona en 5 min)\nRooibos verde, estrellas de chocolate blanco. contiene lactosa',NULL,'100 gr $485.00, Frapptea $69.00, Cal/Frio $49.00',0,2),(10,'SPICE CANELÉ','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, trozos de manzana, varas de canela, pistaches, canela, almendra fileteada, pedazos de vainilla',NULL,'100 gr $370.00, Frapptea $69.00, Cal/Frio $49.00',0,3),(11,'ALMOND SAVARIN','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, bombones, pedazos de piña confitada, pedazos de papaya confitada, chips de coco,  varas de canela, avellana caramelizada, cardamomo, pedazos de jengibre, grosella roja, hojas de brote de rosas, pedazos de vainilla',NULL,'100 gr $360.00, Frapptea $69.00, Cal/Frio $49.00',0,3),(12,'PARIS-BREST VANILLE','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, pedazos de papaya confitada, pedazos de zanahoria, flor de hibisco, pedazos de betabel, pedazos de fresa, pedazos de vainilla',NULL,'100 gr $360.00, Frapptea $69.00, Cal/Frio $49.00',0,3),(13,'EIFFEL CRULLER','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, malvaviscos, granos de cacao, pedazos de manzana, bits de yogurt, flor de aciano, almendra fileteada, pedazos de vainilla',NULL,'100 gr $405.00, Frapptea $69.00, Cal/Frio $49.00',0,3),(14,'MARSHMALLOW MACARON','Rooibos / HoneyBush\n(Se infusiona en 5 min)\nHoneybush, Rooibos, flor de calendula, pedazos de vainilla',NULL,'100 gr $550.00, Frapptea $69.00, Cal/Frio $49.00',0,3),(15,'CARROT SOUFFLÉ','Rooibos\n(Se infusiona en 5 min)\nRooibos, pedazos de zanahoria, pedazos de manzana, canela, cascara de naranja, pasas, escaramujos, rebanadas de manzana, clavo, calendula, nuez moscada, pedazos de vainilla',NULL,'100 gr $550.00, Frapptea $69.00, Cal/Frio $49.00',0,3),(16,'APPLE CRÉPE','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, raíz de achicoria tostada, canela, almendra fileteada, pedazos de vainilla',NULL,'100 gr $380.00, Frapptea $69.00, Cal/Frio $49.00',0,3),(17,'MAPLE BELGIAN WAFFLE','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, pasas, granos de cocoa, varas de canela, avellana caramelizada, pedazos de durazno, pedazos de vainilla',NULL,'100 gr $420.00, Frapptea $69.00, Cal/Frio $49.00',0,3),(18,'PINKY AMARETTI','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, pedazos de piña confitada, pedazos de papaya confitada, pedazos de betabel, varas de canela, pasas, chips de coco, almendras tostadas, palomitas de maiz',NULL,'100 gr $420.00, Frapptea $69.00, Cal/Frio $49.00',0,3),(19,'TULUM','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, trozos de manzana, flor de conchita azul, bayas del sauco, flor de hibisco, chips de coco, mora azul, zarzamora, frambuesa',NULL,'100 gr $460.00, Frapptea $69.00, Cal/Frio $49.00',0,4),(20,'IBIZA','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, pedazos de papaya confitada, pedazos de zanahoria,flamingo de caramelo, flor de hibisco, pedazos de mango confitado, pedazos de betabel, chips de coco, azafrán, flor de aciano',NULL,'100 gr $420.00, Frapptea $69.00, Cal/Frio $49.00',0,4),(21,'ST. TROPEZ','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, lemongrass, pedazos de piña confitada, flor de hibisco, pedazos de fresa, rebanadas de fresa, flor de aciano, hojas de rosa',NULL,'100 gr $420.00, Frapptea $69.00, Cal/Frio $49.00',0,4),(22,'MYKONOS','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, flor de hibisco, pedazos de guayaba, pedazos de naranja, canela, cardamomo, rebanadas de fresa, hojas de brote de rosa, flor de malva',NULL,'100 gr $420.00, Frapptea $69.00, Cal/Frio $49.00',0,4),(23,'ARUBA BEACH','Tisana\n(Se infusiona en 5 min)\nPedazos de piña confitada, pedazos de papaya confitada, flor de hibisco, bayas del saúco, pedazos de betabel,  pedazos de fresa, pedazos de guayaba, pedazos de vainilla',NULL,'100 gr $420.00, Frapptea $69.00, Cal/Frio $49.00',0,4),(24,'IPANEMA','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana,Rebanadas de persimon, pedazos de papaya confitada, cascara de naranja,  lemongrass, pedazos de durazno, grosella roja, hojas de brote de rosas',NULL,'100 gr $420.00, Frapptea $69.00, Cal/Frio $49.00',0,4),(25,'UNICORN ELIXIR','Tisana\n(Se infusiona en 5 min)\nFlor de hibisco, pasas, bayas del sauco, unicornio de caramelo, cerezas, pedazos de cereza acida',NULL,'100 gr $400.00, Frapptea $69.00, Cal/Frio $49.00',0,5),(26,'COSMOBERRY','Tisana\n(Se infusiona en 5 min)\nArandanos, flor de hibisco, rebanadas de manzana.',NULL,'100 gr $420.00, Frapptea $69.00, Cal/Frio $49.00',0,5),(27,'BLUE BERRY LAGOON','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, pasas, pedazos de zanahoria, pedazos de betabel, saborizante, pedazos de yogurt, mora azul.',NULL,'100 gr $420.00, Frapptea $69.00, Cal/Frio $49.00',0,5),(28,'FRESA POP','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, pedazos de papaya confitada, pasas, pedazos de betabel, pedazos de fresas, palomitas de maiz.',NULL,'100 gr $420.00, Frapptea $69.00, Cal/Frio $49.00',0,5),(29,'BLOOMSBERRY COCKTAIL','Té Wulong\n(Se infusiona en 3 min)\nTe oolong, pasas, pedazos de manzana, pedazos de piña confitada, escaramujo, pedazos de manzana,  flor de hibisco, hinojo, baya de enebro, regaliz, pedazos de durazno, anis, brote de rosa, hojas de rosa, anis estrella, pedazos de mandarina, pedazos de fresa, pedazos de ruibarbo, pedazos de vainilla, pedazos de naranja, pedazos de toronja',NULL,'100 gr $420.00, Frapptea $69.00, Cal/Frio $49.00',0,5),(30,'SAMBUCHINO','Tisana / HoneyBush\n(Se infusiona en 5 min)\nHoneybush verde, pedazos de manzana, bayas del saúco, hojas de frambuesa, pedazos de fresa, grosella negra',NULL,'100 gr $420.00, Frapptea $69.00, Cal/Frio $49.00',0,5),(31,'PRINCESS CLEANSE','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, lemongrass, rooibos verde, pedazos de naranja, verbena, pedazos de vainilla',NULL,'100 gr $410.00, Frapptea $69.00, Cal/Frio $49.00',0,6),(32,'RITUAL DETOX','Té Verde\n(Se infusiona en 2 min)\nPedazos de manzana, te verde China Gunpowder, lemongrass, pedazos de piña confitada, pedazos de jengibre, diente de leon, saborizante natural, cascara de limon, cilantro, hierba tulsi, arroz inflado, flor de naranjo, hojas de brote de rosa, raiz de ginseng',NULL,'100 gr $410.00, Frapptea $69.00, Cal/Frio $49.00',0,6),(33,'AQUA VITALE','Tisana\n(Se infusiona en 5 min)\nPedazos de manzana, pasas, pedazos de zanahoria, hojas de moringa, pedazos de betabel, flor de hibisco, pedazos de fresa',NULL,'100 gr $310.00, Frapptea $69.00, Cal/Frio $49.00',0,6),(34,'SUMMER BODY','Tisana\n(Se infusiona en 5 min)\n\"Cilantro*, menta nana*, hoja de ortiga*, cascara de naranja*, lemongrass*, pedazos de manzana*, canela*, trozos de jengibre*,  clavo*, cardamomo*\n*Certificado Organico\"',NULL,'100 gr $440.00, Frapptea $69.00, Cal/Frio $49.00',0,6),(35,'GINGER VITALITY','Tisana\n(Se infusiona en 5 min)\n\"Pedazos de manzana*, escaramujo*, hierba del sauce*, pedazos de zanahora*, hojas de frambuesa*, pedazos de jengibre, verbena*, hojas de malva*, flor del saúco*\n*Certificado Organico\"',NULL,'100 gr $420.00, Frapptea $69.00, Cal/Frio $49.00',0,6),(36,'SLIM TONIC','Té Wulong\n(Se infusiona en 3 min)\nTe Oolong, Te Pu Erh, escaramujos, pedazos de manzana, flor de hibisco, menta nana, pedazos de betabel, hojas de menta, rebanadas de fresa, hojas de flor, pedazos de manzana',NULL,'100 gr $430.00, Frapptea $69.00, Cal/Frio $49.00',0,6),(37,'CHAI WALA','Té Wulong(Se infusiona en 3 min.)Canela, Te oolong, pedazos de jengibre, raiz de achicoria tostada, pedazos de algarrobo, regaliz, clavo, cardamomo, pimienta.',NULL,'100 gr $360.00, Frapptea $69.00, Cal/Frio $49.00',0,7),(38,'NEW DELHI-CIOUS','Té blanco\n(Se infusiona en 2 min)\nTe blanco, canela, lemongrass, pedazos de piña confitada, chips de coco, pedazos de jengibre, hojas de moringa, clavo, saborizante natural, pimienta, cardamomo.\n',NULL,'100 gr $450.00, Frapptea $69.00, Cal/Frio $49.00',0,7),(39,'BINDI CHAI','Tisana\n(Se infusiona en 5 min)\nFlor de hibisco, pasas, pedazos de manzana, cascara de escaramujo, almendra fileteada, clavo.',NULL,'100 gr $390.00, Frapptea $69.00, Cal/Frio $49.00',0,7),(40,'CITRUS FRESH SCENT','Té Verde\n(Se infusiona en 2 min)\nTe Verde China Chun Mee, menta nana, verbena, lemongrass.',NULL,'100 gr $350.00, Cal/Frio $55.00',0,8),(41,'KYOTO GREEN GEMS','Té Verde\n(Se infusiona en 2 min)\nTe Verde - Sencha Japones, Bancha Japones, Genmaicha (Te, arroz), Kukicha, pedazos de fresa, grosella roja',NULL,'100 gr $420.00, Cal/Frio $55.00',0,8),(42,'EL TÉ DE LA GEISHA','Té Blanco / Té Verde\n(Se infusiona de 3 min)\nTe blanco, Te verde, cerezas, arandanos, escaramujo,  hojas de rosa, brote de rosa',NULL,'100 gr $420.00, Cal/Frio $55.00',0,8),(43,'SOL DE MATCHA','Té Verde y Matcha\n(Se infusiona de 30-45 seg.)\nTé Verde China Gundpowder,Té Verde Japónes Matcha,  trozos de papaya confitada, arroz tostado y arroz inflado.',NULL,'100 gr $420.00, Cal/Frio $55.00',0,8),(44,'ARUMI FRUTALLE','Té Verde\n(Se infusiona en 2 min)\nTe verde, trozos de papaya confitada, trozos de manzana, trozos de zanahoria,  lemongrass, brotes de calendúla, pasas, trozos de durazno ',NULL,'100 gr $420.00, Cal/Frio $55.00',0,8),(45,'SILVER NEEDLE','Té blanco.RESERVA IMPERIAL\n(Se infusiona en 2 min)\nLa pureza y rareza de estos capullos de té blanco en forma de aguja le otorgan fama legendaria como uno de los 10 mejores tés que se producen en China. Durante siglos reservado para la familia imperial, este té es cosechado a mano con reverencia sólo dos días del año. Las hojas plateadas dotadas de una belleza única liberan un fragante sabor suave y sedoso, lleno de lujo y realeza.',NULL,'100 gr $720.00, Cal/Frio $55.00',0,9),(46,'MONKEY PICKED WULONG','Té Wulong RESERVA IMPERIAL\n(Se infusiona en 3 min)\nSegún cuenta la leyenda los monjes budistas entrenaban a los monos para recolectar las hojas superiores y más jóvenes de los árboles de té silvestres, creando este preciado tesoro de reserva imperial. El secreto radica en la recolección manual de las hojas, cuidando que éstas no se rompan para que desplieguen el característico sabor a orquídeas del más fino de los Oolongs del mundo.',NULL,'100 gr $720.00, Cal/Frio $55.00',0,9),(47,'GYOKURO IMPERIAL','Té Verde RESERVA IMPERIAL\n(Se infusiona de 30-45 seg.)\nLos arbustos de Gyokuro son cubiertos dos semanas antes de la cosecha para conservarla clorofila y concentrar el sabor y nutrientes del mejor de los tés japoneses cuyo sabor ofrece una complejidad en el color verde esmeralda de su infusión y su dulce sabor característico que lo convierten en uno de los favoritos de nuestra colección.',NULL,'100 gr $790.00, Cal/Frio $55.00',0,9),(48,'MENTA MARROQUI','Té Verde y hoja de menta(Se infusiona de 30-45 seg.)Los arbustos de Gyokuro son cubiertos dos semanas antes de la cosecha para conservarla clorofila y concentrar el sabor y nutrientes del mejor de los tés japoneses cuyo sabor ofrece una complejidad en el color verde esmeralda de su infusión y su dulce sabor característico que lo convierten en uno de los favoritos de nuestra colección.',NULL,'100 gr $450.00, Cal/Frio $55.00',0,9),(49,'JASMINE BIANCA','Té Verde y  Té Blanco\n(Se infusiona en 3 min)\nSe seleccionan las puntas  de las hojas más jovenes que se enrollan a mano en pequeñas esferas. Alto nivel de Antioxidantes',NULL,'100 gr $720.00, Cal/Frio $55.00',0,9),(50,'STICKY RICE','Té Wulong\n(Se infusiona en 3 min)\nUn té que sabe y huela a arroz pegajoso. Es una combinación única entre dulce y cremoso con notas satisfactorias de granos recien cocinados naturalmente. Un sorbo de este té y querras quedarte con el de por vida. ',NULL,'100 gr $610.00, Cal/Frio $55.00',0,9),(51,'KOREAN JOONGJAK','Té Verde (Corea del Sur)\n(Se infusiona en 2 min)\nEs conocido como uno de los mejores tés verdes de Corea.',NULL,'100 gr $720.00, Cal/Frio $55.00',0,9),(52,'TAMARYOCUCHA','Té Verde (2017 Global Tea Competition Winners Hot Spring Loose Leaf)\n(Se infusiona en 45 seg.)\nSe produce principalmente en Kyushu en una isla al suroeste de Japón. Tiene un sabor suave y con astringencia ligera. Ayuda a eliminar los radicales libres y ayuda a disminuir los niveles de azúcar en la sangre.',NULL,'100 gr $790.00, Cal/Frio $55.00',0,9),(53,'MILKY WULONG','Té Wulong\n(Se infusiona en 3 min.)\nProveniente de las montañas de Wuyi en China, el sbor se caracteriza porque, unos días antes de la cosecha la temperatura desciende, lo que ocasiona que el sabor del té se altere',NULL,'100 gr $720.00, Cal/Frio $55.00',0,9),(54,'ENGLISH BREAKFAST','Té Negro\n(Se infusiona en 3 min.)\nSiéntete de la realeza con la elegancia y tradición en cada sorbo de este elegante té negro que despliega su majestuosidad en la infusión de sus hojas completas. ',NULL,'100 gr $450.00, Cal/Frio $55.00',0,9),(55,'GOLDEN MONKEY','Té Negro\n(Se infusiona en 3 min.)\nLa tradición antigua y de exclusividad imperial, desde tiempos de la dinastía Song, hacen de este legendario té negro parte del top ten de tés chinos. Su delicioso sabor con tonos de cacao es obtenido tras la cuidadosa recolección de solo una hoja y un capullo.',NULL,'100 gr $720.00, Cal/Frio $55.00',0,9);
/*!40000 ALTER TABLE `tbl_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_niveles`
--

DROP TABLE IF EXISTS `tbl_niveles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_niveles` (
  `id_nivel` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `tipo` int(2) DEFAULT NULL,
  `ini` int(11) DEFAULT NULL,
  `fin` int(11) DEFAULT NULL,
  `activo` int(2) DEFAULT '0',
  PRIMARY KEY (`id_nivel`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_niveles`
--

LOCK TABLES `tbl_niveles` WRITE;
/*!40000 ALTER TABLE `tbl_niveles` DISABLE KEYS */;
INSERT INTO `tbl_niveles` VALUES (1,'Chocolateas',1,1,150,0),(2,'Pateasserie',1,151,450,0),(3,'Kioto Memories',1,451,600,0),(4,'Orient espress',1,601,200000,0);
/*!40000 ALTER TABLE `tbl_niveles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_objeto_html`
--

DROP TABLE IF EXISTS `tbl_objeto_html`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_objeto_html` (
  `id_objeto_html` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_objeto` varchar(45) NOT NULL,
  `tipo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_objeto_html`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_objeto_html`
--

LOCK TABLES `tbl_objeto_html` WRITE;
/*!40000 ALTER TABLE `tbl_objeto_html` DISABLE KEYS */;
INSERT INTO `tbl_objeto_html` VALUES (1,'input','text'),(2,'input','number'),(3,'input','email'),(4,'select',NULL),(5,'select','multiple'),(6,'rango','date'),(7,'rango','number'),(8,'rango','time'),(9,'rango','money'),(10,'slide','number');
/*!40000 ALTER TABLE `tbl_objeto_html` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pantalla`
--

DROP TABLE IF EXISTS `tbl_pantalla`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pantalla` (
  `id_pantalla` int(3) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `ruta` varchar(45) DEFAULT NULL,
  `activo` int(2) DEFAULT NULL,
  `id_area` int(3) DEFAULT NULL,
  `icono` varchar(45) DEFAULT NULL,
  `orden` int(2) DEFAULT NULL,
  PRIMARY KEY (`id_pantalla`),
  KEY `id_area_idx` (`id_area`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pantalla`
--

LOCK TABLES `tbl_pantalla` WRITE;
/*!40000 ALTER TABLE `tbl_pantalla` DISABLE KEYS */;
INSERT INTO `tbl_pantalla` VALUES (4,'Usuarios','administracion/admin_usuarios',1,1,'users',1),(5,'Registro','registro/registro2',0,3,'id-badge',1),(6,'Acumulación','acumulacion/acumulacion2',0,3,'boxes',2),(7,'Redencion','redencion/redencion2',0,3,'gift',3),(9,'Historial ','registros/historico',0,4,'list-ol',1),(10,'Segmentos','administracion/admin_segmento',0,1,'chart-pie',2),(11,'Niveles','administracion/admin_niveles',1,1,'star',2),(12,'Servicios','administracion/admin_menu',0,5,'utensils',2),(13,'Lista de Clientes','reportes/reporte_clientes',0,2,'users',1),(14,'Puntos de Venta','reportes/reporte_pv',0,2,'store-alt',2),(15,'Historial de Clientes','reportes/reporte_historial_clientes',0,2,'history',3),(16,'Cuadre de Caja','administracion/cuadre_caja',0,2,'file-invoice-dollar',4),(17,'Transfer Registros','administracion/transfer_registros',0,1,'exchange-alt',8),(18,'Promociones','reportes/reporte_promociones',0,2,'gift',5),(19,'Categorias','administracion/admin_categorias',0,5,'project-diagram',1),(20,'Sucursales','administracion/admin_sucursales',1,1,'store',6),(21,'Promociones','administracion/admin_promociones',0,1,'tags',7),(22,'Niveles','reportes/reporte_niveles',0,2,'arrow-up',6),(23,'Transfer  Registros','administracion/transfer_registros',0,3,'exchange-alt',6),(24,'Transferencia de Puntos','reportes/reporte_transferencia_puntos',0,2,'exchange-alt',7),(25,'Segmentos','reportes/reporte_segmentos',0,2,'chart-pie',8),(26,'Cuadre de Caja','administracion/cuadre_caja',0,3,'file-invoice-dollar',4),(27,'Carga Multiple de Servicios','administracion/upload_menu',0,1,'upload',8),(28,'Recompensas','administracion/admin_rewards',0,1,'gift',9),(29,'Historial de registros','administracion/admin_registros',0,3,'history',5),(30,'Clientes','administracion/admin_clientes',0,1,'users',1);
/*!40000 ALTER TABLE `tbl_pantalla` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_periodo_comunicacion`
--

DROP TABLE IF EXISTS `tbl_periodo_comunicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_periodo_comunicacion` (
  `id_periodo_comunicacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_periodo_comunicacion`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_periodo_comunicacion`
--

LOCK TABLES `tbl_periodo_comunicacion` WRITE;
/*!40000 ALTER TABLE `tbl_periodo_comunicacion` DISABLE KEYS */;
INSERT INTO `tbl_periodo_comunicacion` VALUES (1,'Semanal'),(2,'Quincenal'),(3,'Mensual');
/*!40000 ALTER TABLE `tbl_periodo_comunicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_promociones`
--

DROP TABLE IF EXISTS `tbl_promociones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_promociones` (
  `id_promociones` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `detalle` varchar(255) DEFAULT NULL,
  `texto_corto` varchar(255) DEFAULT NULL,
  `cashback` varchar(45) DEFAULT NULL,
  `cashbackperc` varchar(45) DEFAULT NULL,
  `descuento` varchar(45) DEFAULT NULL,
  `codigo` varchar(45) DEFAULT NULL,
  `imagen_qr` varchar(255) DEFAULT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `condiciones` varchar(856) DEFAULT NULL,
  `uso` varchar(856) DEFAULT NULL,
  `link_info` varchar(216) DEFAULT NULL,
  `tipo_bonus` int(2) DEFAULT NULL,
  `recompensa_directa` int(11) DEFAULT NULL,
  `recompensa_porcentaje` int(11) DEFAULT NULL,
  `limite` int(11) DEFAULT NULL,
  `limite_por_usuario` int(11) DEFAULT NULL,
  `comienza` date DEFAULT NULL,
  `vigencia` date DEFAULT NULL,
  `push` varchar(2) DEFAULT '1',
  `sms` varchar(2) DEFAULT '1',
  `whatsapp` varchar(2) DEFAULT '1',
  `email` varchar(2) DEFAULT '1',
  `pushtxt` varchar(1024) DEFAULT NULL,
  `smstxt` varchar(1024) DEFAULT NULL,
  `whatstxt` varchar(1024) DEFAULT NULL,
  `emailtxt` varchar(1024) DEFAULT NULL,
  `fecha_envio` datetime DEFAULT NULL,
  `activo` varchar(45) DEFAULT '0',
  PRIMARY KEY (`id_promociones`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_promociones`
--

LOCK TABLES `tbl_promociones` WRITE;
/*!40000 ALTER TABLE `tbl_promociones` DISABLE KEYS */;
INSERT INTO `tbl_promociones` VALUES (3,'Bebida gratis','Para celebrar tu cumpleaños, en la compra de uno de nuestros productos llévate una bebida gratis','Bono Cumpleaños','','','','','3.png','8','','','',0,0,0,2147483647,1,'2019-05-27','2025-05-27','0','0','0','0','','','','','2019-05-27 10:18:27','0'),(2,'Perlas Gratis','Como parte de tu bienvenida, en la compra de un Frappetea, agregale perlas totalmente gratis.','Bono Bienvenida','','','','','2.png','10','','','',0,0,0,2147483647,1,'2019-05-26','2025-05-30','0','0','0','1','','','','','2019-05-27 10:14:04','0'),(4,'$25 descuento en consumo ','$25 descuento en consumo','$25 Descuento','','','25','DESC25','4.png','3','','','',0,0,0,2147483647,1,'2019-06-13','2019-06-14','0','1','0','1','','','','','2019-06-13 12:51:47','0'),(5,'50% Descuento','50% Descuento','50% Descuento','','','50','50DESC','5.png','6','','','',0,0,0,200,1,'2019-06-13','2019-06-30','0','0','0','0','','','','','2019-06-13 17:17:32','0'),(6,'Bebida gratis','Para celebrar tu cumpleaños, en la compra de uno de nuestros productos llévate una bebida gratis','Bono Cumpleaños','','','','',NULL,'8','','','',NULL,0,0,2147483647,1,'2019-05-27','2025-05-27','0','0','0','0','','','','','2019-05-27 10:18:27','0'),(7,'Festeja tu cumpleaños con nostros','Recibe una bebida gratis','ladsjfalsdfj','','','','','7.png','8','No aplica con otras promociones','','',0,0,0,2147483647,1,'2019-06-20','2019-06-27','0','0','0','1','','','','<p>Felicididades Luvia por tu cumplea&ntilde;os.</p>\n\n<p>Te invitamos a una bebida en tu sucursal de Polanco.</p>\n','2019-06-20 10:50:46','0'),(8,'Envio gratis','No se cobra el envío','Free','','','','','8.png','4','Que sea mayor a 100 pesos la compra','Cuando el ticket sea mayor a 100 se le da el envío gratis','',0,5,0,100,1,'2019-07-01','2019-07-05','1','1','1','1','Hola','Hi','Ola','<p><a href=\"http://click.farmaciasbenavides.com.mx/?qs=9ba3c8e3667ad4532c3c59d996ce83c5338c893173e67e2c4532e0738a6ef34156ea398f405f1e743ed7884bd49a8e26df5d5d66fe63aa49\">H</a>ola</p>\n','2019-07-02 17:01:36','0'),(9,'Prueba','detalle','DESC','','','100','112233','9.png','3','','','',0,0,0,100,3,'2019-07-12','2019-07-31','0','0','0','0','','','','','0000-00-00 00:00:00','0'),(10,'Cashback Perc','Cashback Perc','Perc','','30','','','10.png','1','','','',0,0,0,100,8,'2019-07-15','2019-07-21','0','1','0','1','','','','','2019-07-15 09:44:24','0');
/*!40000 ALTER TABLE `tbl_promociones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_puntos_totales`
--

DROP TABLE IF EXISTS `tbl_puntos_totales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_puntos_totales` (
  `id_puntos_totales` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `puntos_totales` varchar(45) DEFAULT NULL,
  `visitas_totales` int(3) DEFAULT NULL,
  `id_tipo_puntos` int(11) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  PRIMARY KEY (`id_puntos_totales`),
  KEY `id_usuar_idx` (`id_usuario`),
  KEY `id_tipo_regi_idx` (`id_tipo_puntos`),
  CONSTRAINT `tbl_puntos_totales_ibfk_1` FOREIGN KEY (`id_tipo_puntos`) REFERENCES `tbl_tipo` (`id_tipo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_puntos_totales_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_puntos_totales`
--

LOCK TABLES `tbl_puntos_totales` WRITE;
/*!40000 ALTER TABLE `tbl_puntos_totales` DISABLE KEYS */;
INSERT INTO `tbl_puntos_totales` VALUES (1,51,'45',0,1,'2019-07-01 14:16:05'),(2,51,'15',0,2,'2019-07-01 14:06:30'),(3,52,'0',0,1,'2019-06-14 13:54:35'),(4,52,'0',0,2,'2019-06-14 13:54:35'),(5,53,'0',0,1,'2019-06-14 13:56:15'),(6,53,'0',0,2,'2019-06-14 13:56:15'),(7,54,'0',0,1,'2019-06-14 13:57:36'),(8,54,'0',0,2,'2019-06-14 13:57:36'),(9,55,'10',0,1,'2019-06-17 18:08:22'),(10,55,'0',0,2,'2019-06-17 18:07:23'),(11,56,'10',0,1,'2019-06-17 18:13:27'),(12,56,'0',0,2,'2019-06-17 18:11:55'),(13,57,'20',0,1,'2019-06-19 11:44:48'),(14,57,'0',0,2,'2019-06-19 11:40:38'),(15,58,'5',0,1,'2019-06-20 10:32:54'),(16,58,'5',0,2,'2019-06-20 10:32:54'),(17,60,'35',0,1,'2019-07-12 14:41:24'),(18,60,'15',0,2,'2019-07-12 14:41:24'),(21,72,'10',0,1,'2019-07-09 12:04:06'),(22,72,'0',0,2,'2019-07-09 12:03:08'),(23,73,'0',0,1,'2019-07-10 16:30:52'),(24,73,'0',0,2,'2019-07-10 16:30:52'),(25,74,'0',0,1,'2019-07-10 16:36:34'),(26,74,'0',0,2,'2019-07-10 16:36:34'),(27,75,'0',0,1,'2019-07-11 13:30:14'),(28,75,'0',0,2,'2019-07-11 13:30:14'),(29,76,'10',0,1,'2019-07-15 09:45:20'),(30,76,'0',0,2,'2019-07-11 13:41:11'),(31,77,'20',0,1,'2019-07-11 17:48:50'),(32,77,'0',0,2,'2019-07-11 17:23:36'),(33,78,'25',0,1,'2019-07-12 09:39:38'),(34,78,'5',0,2,'2019-07-12 09:33:06'),(35,79,'20',0,1,'2019-07-13 19:06:58'),(36,79,'10',0,2,'2019-07-13 19:01:49'),(37,80,'20',0,1,'2019-07-13 19:19:52'),(38,80,'0',0,2,'2019-07-13 18:54:51');
/*!40000 ALTER TABLE `tbl_puntos_totales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_registros`
--

DROP TABLE IF EXISTS `tbl_registros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_registros` (
  `id_registro_acumulacion` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `ticket` varchar(255) DEFAULT NULL,
  `monto_ticket` float DEFAULT NULL,
  `id_configuracion` int(11) DEFAULT NULL,
  `puntos` int(11) DEFAULT NULL,
  `num_visita` int(4) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `dia_registro` varchar(45) DEFAULT NULL,
  `id_tipo_registro` int(11) DEFAULT NULL,
  `id_usuario_registro` int(11) DEFAULT NULL,
  `id_cat_premio` int(11) DEFAULT NULL,
  `id_promo` int(11) DEFAULT NULL,
  `monto_cashback` float DEFAULT NULL,
  `monto_descuento` float DEFAULT NULL,
  PRIMARY KEY (`id_registro_acumulacion`),
  KEY `id_us_idx` (`id_usuario`),
  KEY `id_con_idx` (`id_configuracion`),
  KEY `id_tipo_idx` (`id_tipo_registro`),
  KEY `id_us_regitro_idx` (`id_usuario_registro`),
  KEY `id_cat_prem_idx` (`id_cat_premio`),
  KEY `id_promo_idx` (`id_promo`),
  CONSTRAINT `tbl_registros_ibfk_1` FOREIGN KEY (`id_cat_premio`) REFERENCES `tbl_cat_premios_productos_servicios` (`id_cat_premios_productos_servicios`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_registros_ibfk_2` FOREIGN KEY (`id_configuracion`) REFERENCES `tbl_configuracion_proyecto` (`id_configuracion_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_registros_ibfk_3` FOREIGN KEY (`id_tipo_registro`) REFERENCES `tbl_tipo` (`id_tipo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_registros_ibfk_4` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_registros_ibfk_5` FOREIGN KEY (`id_usuario_registro`) REFERENCES `tbl_usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_registros`
--

LOCK TABLES `tbl_registros` WRITE;
/*!40000 ALTER TABLE `tbl_registros` DISABLE KEYS */;
INSERT INTO `tbl_registros` VALUES (1,55,'45785',150,1,10,NULL,'2019-06-17 18:08:22','Lunes',1,3,NULL,NULL,NULL,NULL),(2,56,'4567991k',300,1,10,NULL,'2019-06-17 18:13:27','Lunes',1,3,NULL,NULL,NULL,NULL),(3,57,'1564KJK',175,1,10,NULL,'2019-06-19 11:42:46','Miercoles',1,3,NULL,NULL,NULL,NULL),(4,57,'4555618',90,1,10,NULL,'2019-06-19 11:44:48','Miercoles',1,3,NULL,NULL,NULL,NULL),(5,58,'1245325K',145,1,10,NULL,'2019-06-20 10:31:50','Jueves',1,3,NULL,NULL,NULL,NULL),(6,58,NULL,NULL,1,5,NULL,'2019-06-20 10:32:54','Jueves',2,3,1,NULL,NULL,NULL),(7,51,'JUSMN12',123,1,10,NULL,'2019-06-20 14:58:20','Jueves',1,3,NULL,NULL,NULL,NULL),(8,51,'KSINWI1723',167,1,10,NULL,'2019-06-20 14:58:56','Jueves',1,3,NULL,NULL,NULL,NULL),(9,51,'KISNLA126',124,1,10,NULL,'2019-06-20 14:59:19','Jueves',1,3,NULL,NULL,NULL,NULL),(10,51,NULL,NULL,1,5,NULL,'2019-06-20 14:59:38','Jueves',2,3,1,NULL,NULL,NULL),(11,51,'Dghu',123,1,10,NULL,'2019-07-01 14:03:58','Lunes',1,3,NULL,NULL,NULL,NULL),(12,51,NULL,NULL,1,5,NULL,'2019-07-01 14:04:29','Lunes',2,3,1,NULL,NULL,NULL),(13,51,'Dghjjii',123,1,10,NULL,'2019-07-01 14:05:19','Lunes',5,3,NULL,2,0,0),(14,51,NULL,NULL,1,5,NULL,'2019-07-01 14:06:30','Lunes',2,3,1,NULL,NULL,NULL),(15,51,'Fgfhjj',255,1,10,NULL,'2019-07-01 14:16:05','Lunes',1,3,NULL,NULL,NULL,NULL),(16,60,'Dgjfg',50,1,10,NULL,'2019-07-01 17:13:16','Lunes',1,3,NULL,NULL,NULL,NULL),(17,60,'Khfdcgj',125,1,10,NULL,'2019-07-03 11:50:30','Miercoles',1,3,NULL,NULL,NULL,NULL),(18,60,NULL,NULL,1,5,NULL,'2019-07-03 11:51:13','Miercoles',2,3,1,NULL,NULL,NULL),(19,60,'Fgfhjjg',1,1,10,NULL,'2019-07-03 11:52:04','Miercoles',1,3,NULL,NULL,NULL,NULL),(20,60,'Ddfgg',150,1,10,NULL,'2019-07-03 12:07:53','Miercoles',1,3,NULL,NULL,NULL,NULL),(21,60,NULL,NULL,1,5,NULL,'2019-07-03 12:09:54','Miercoles',2,3,1,NULL,NULL,NULL),(24,72,'12355',5558,1,10,NULL,'2019-07-09 12:04:06','Martes',1,3,NULL,NULL,NULL,NULL),(25,60,'Fghjhgff',555,1,10,NULL,'2019-07-09 16:27:40','Martes',1,3,NULL,NULL,NULL,NULL),(26,77,'25thj8yeq6ukh',569,1,10,NULL,'2019-07-11 17:38:01','Jueves',1,3,NULL,NULL,NULL,NULL),(27,77,'5E48f5g9h9',55,1,10,NULL,'2019-07-11 17:48:50','Jueves',5,3,NULL,2,0,0),(28,78,'12346',100,1,10,NULL,'2019-07-12 09:29:10','Viernes',1,3,NULL,NULL,NULL,NULL),(29,78,'12344',200,1,10,NULL,'2019-07-12 09:30:52','Viernes',1,3,NULL,NULL,NULL,NULL),(30,78,NULL,NULL,1,5,NULL,'2019-07-12 09:33:06','Viernes',2,3,1,NULL,NULL,NULL),(31,78,'152',50,1,10,NULL,'2019-07-12 09:39:38','Viernes',5,3,NULL,2,0,0),(32,60,NULL,NULL,1,5,NULL,'2019-07-12 14:41:24','Viernes',2,3,1,NULL,NULL,NULL),(33,79,'0',70,1,10,NULL,'2019-07-13 18:52:11','Sabado',1,3,NULL,NULL,NULL,NULL),(34,80,'O',1,1,10,NULL,'2019-07-13 18:58:37','Sabado',1,3,NULL,NULL,NULL,NULL),(35,79,NULL,NULL,1,10,NULL,'2019-07-13 19:01:49','Sabado',2,3,1,NULL,NULL,NULL),(36,79,'2',60,1,10,NULL,'2019-07-13 19:05:37','Sabado',1,3,NULL,NULL,NULL,NULL),(37,79,'5',1000,1,10,NULL,'2019-07-13 19:06:58','Sabado',1,3,NULL,NULL,NULL,NULL),(38,80,'xxxxhgc',200,1,10,NULL,'2019-07-13 19:19:52','Sabado',1,3,NULL,NULL,NULL,NULL),(39,76,'WYESk12',100,1,10,NULL,'2019-07-15 09:45:20','Lunes',1,3,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tbl_registros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_config_campos_registro`
--

DROP TABLE IF EXISTS `tbl_rel_config_campos_registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_config_campos_registro` (
  `id_rel_config_campos_registro` int(11) NOT NULL AUTO_INCREMENT,
  `id_config` int(11) DEFAULT NULL,
  `id_campo` int(11) DEFAULT NULL,
  `posicion` int(11) DEFAULT NULL,
  `clave` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rel_config_campos_registro`),
  KEY `id_configura_idx` (`id_config`),
  KEY `id_campo_idx` (`id_campo`),
  CONSTRAINT `tbl_rel_config_campos_registro_ibfk_1` FOREIGN KEY (`id_campo`) REFERENCES `tbl_campos_registro` (`id_campos_registro`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_rel_config_campos_registro_ibfk_2` FOREIGN KEY (`id_config`) REFERENCES `tbl_configuracion_proyecto` (`id_configuracion_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_config_campos_registro`
--

LOCK TABLES `tbl_rel_config_campos_registro` WRITE;
/*!40000 ALTER TABLE `tbl_rel_config_campos_registro` DISABLE KEYS */;
INSERT INTO `tbl_rel_config_campos_registro` VALUES (1,1,1,1,0),(2,1,3,2,1),(3,1,5,3,0),(4,1,7,4,0);
/*!40000 ALTER TABLE `tbl_rel_config_campos_registro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_config_forma_acumulacion`
--

DROP TABLE IF EXISTS `tbl_rel_config_forma_acumulacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_config_forma_acumulacion` (
  `id_rel_config_forma_acumulacion` int(11) NOT NULL AUTO_INCREMENT,
  `id_configuracion_proyecto` int(11) DEFAULT NULL,
  `id_forma_acumulacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rel_config_forma_acumulacion`),
  KEY `id_configuracion_proyecto_idx` (`id_configuracion_proyecto`),
  KEY `id_forma_acumulacion_idx` (`id_forma_acumulacion`),
  CONSTRAINT `tbl_rel_config_forma_acumulacion_ibfk_1` FOREIGN KEY (`id_configuracion_proyecto`) REFERENCES `tbl_configuracion_proyecto` (`id_configuracion_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_rel_config_forma_acumulacion_ibfk_2` FOREIGN KEY (`id_forma_acumulacion`) REFERENCES `tbl_forma_acumulacion` (`id_forma_acumulacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_config_forma_acumulacion`
--

LOCK TABLES `tbl_rel_config_forma_acumulacion` WRITE;
/*!40000 ALTER TABLE `tbl_rel_config_forma_acumulacion` DISABLE KEYS */;
INSERT INTO `tbl_rel_config_forma_acumulacion` VALUES (1,1,1),(2,1,2);
/*!40000 ALTER TABLE `tbl_rel_config_forma_acumulacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_config_mecanica_redencion`
--

DROP TABLE IF EXISTS `tbl_rel_config_mecanica_redencion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_config_mecanica_redencion` (
  `id_rel_config_mecanica_redencion` int(11) NOT NULL AUTO_INCREMENT,
  `id_configuracion_proyecto` int(11) DEFAULT NULL,
  `id_mecanica_redencion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rel_config_mecanica_redencion`),
  KEY `id_con_proyecto_idx` (`id_configuracion_proyecto`),
  KEY `id_mecanica_redencion_idx` (`id_mecanica_redencion`),
  CONSTRAINT `tbl_rel_config_mecanica_redencion_ibfk_1` FOREIGN KEY (`id_configuracion_proyecto`) REFERENCES `tbl_configuracion_proyecto` (`id_configuracion_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_rel_config_mecanica_redencion_ibfk_2` FOREIGN KEY (`id_mecanica_redencion`) REFERENCES `tbl_mecanica_redencion` (`id_mecanica_redencion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_config_mecanica_redencion`
--

LOCK TABLES `tbl_rel_config_mecanica_redencion` WRITE;
/*!40000 ALTER TABLE `tbl_rel_config_mecanica_redencion` DISABLE KEYS */;
INSERT INTO `tbl_rel_config_mecanica_redencion` VALUES (1,1,1),(2,1,2),(3,1,3);
/*!40000 ALTER TABLE `tbl_rel_config_mecanica_redencion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_config_tipo_redencion`
--

DROP TABLE IF EXISTS `tbl_rel_config_tipo_redencion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_config_tipo_redencion` (
  `id_rel_config_tipo_redencion` int(11) NOT NULL AUTO_INCREMENT,
  `id_configuracion_proyecto` int(11) DEFAULT NULL,
  `id_tipo_redencion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rel_config_tipo_redencion`),
  KEY `id_config_proyecto_idx` (`id_configuracion_proyecto`),
  KEY `id_tip_redencion_idx` (`id_tipo_redencion`),
  CONSTRAINT `tbl_rel_config_tipo_redencion_ibfk_1` FOREIGN KEY (`id_configuracion_proyecto`) REFERENCES `tbl_configuracion_proyecto` (`id_configuracion_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_rel_config_tipo_redencion_ibfk_2` FOREIGN KEY (`id_tipo_redencion`) REFERENCES `tbl_tipo_redencion` (`id_tipo_redencion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_config_tipo_redencion`
--

LOCK TABLES `tbl_rel_config_tipo_redencion` WRITE;
/*!40000 ALTER TABLE `tbl_rel_config_tipo_redencion` DISABLE KEYS */;
INSERT INTO `tbl_rel_config_tipo_redencion` VALUES (1,1,2);
/*!40000 ALTER TABLE `tbl_rel_config_tipo_redencion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_objetos_categorias`
--

DROP TABLE IF EXISTS `tbl_rel_objetos_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_objetos_categorias` (
  `id_rel` int(11) NOT NULL AUTO_INCREMENT,
  `id_objeto` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `label` varchar(128) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id_rel`),
  KEY `fk_id_objeto_idx` (`id_objeto`),
  KEY `fk_id_categoria_idx` (`id_categoria`),
  CONSTRAINT `tbl_rel_objetos_categorias_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `tbl_categorias_segmento` (`id_categoria`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_rel_objetos_categorias_ibfk_2` FOREIGN KEY (`id_objeto`) REFERENCES `tbl_objeto_html` (`id_objeto_html`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_objetos_categorias`
--

LOCK TABLES `tbl_rel_objetos_categorias` WRITE;
/*!40000 ALTER TABLE `tbl_rel_objetos_categorias` DISABLE KEYS */;
INSERT INTO `tbl_rel_objetos_categorias` VALUES (1,2,1,'Nro de días que abarcará el segmento antes de su cumpleaños','dias'),(2,2,2,'Nro de días que abarcará el segmento antes de su fecha de registro','dias'),(4,2,3,'Nro de días posteriores a su fecha de ultima compra','dias'),(5,6,4,'Ingrese el rango de fechas','periodo'),(6,5,5,'Elige uno o más puntos de venta','pos'),(7,5,6,'Elige uno o más clientes','users'),(8,9,7,'Ingresa el rango para el valor de compra','valor_compra'),(9,4,8,'Elige un género','genero'),(10,10,9,'Elige un rango de edad','edad'),(11,5,10,'Elige uno o más días','dias'),(13,8,11,'Ingresa la hora deseada','hora'),(14,2,12,'Ingresa la cantidad mínima de compras','compras');
/*!40000 ALTER TABLE `tbl_rel_objetos_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_promo_suc_segmento`
--

DROP TABLE IF EXISTS `tbl_rel_promo_suc_segmento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_promo_suc_segmento` (
  `id_rel` int(11) NOT NULL AUTO_INCREMENT,
  `id_promo` varchar(45) DEFAULT NULL,
  `id_sucursal` int(11) DEFAULT NULL,
  `id_segmento` int(11) DEFAULT NULL,
  `id_nivel` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rel`)
) ENGINE=MyISAM AUTO_INCREMENT=131 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_promo_suc_segmento`
--

LOCK TABLES `tbl_rel_promo_suc_segmento` WRITE;
/*!40000 ALTER TABLE `tbl_rel_promo_suc_segmento` DISABLE KEYS */;
INSERT INTO `tbl_rel_promo_suc_segmento` VALUES (10,'3',NULL,NULL,1),(28,'4',NULL,3,4),(27,'4',NULL,3,3),(26,'4',NULL,3,2),(25,'4',NULL,3,1),(24,'2',NULL,NULL,4),(23,'2',NULL,NULL,3),(22,'2',NULL,NULL,2),(21,'2',NULL,NULL,1),(11,'3',NULL,NULL,2),(12,'3',NULL,NULL,3),(29,'5',NULL,4,NULL),(30,'6',NULL,NULL,1),(31,'6',NULL,NULL,2),(32,'6',NULL,NULL,3),(33,'7',1,NULL,1),(34,'7',1,NULL,2),(35,'7',1,NULL,3),(36,'7',1,NULL,4),(37,'7',2,NULL,1),(38,'7',2,NULL,2),(39,'7',2,NULL,3),(40,'7',2,NULL,4),(41,'7',3,NULL,1),(42,'7',3,NULL,2),(43,'7',3,NULL,3),(44,'7',3,NULL,4),(45,'7',4,NULL,1),(46,'7',4,NULL,2),(47,'7',4,NULL,3),(48,'7',4,NULL,4),(49,'7',5,NULL,1),(50,'7',5,NULL,2),(51,'7',5,NULL,3),(52,'7',5,NULL,4),(53,'7',6,NULL,1),(54,'7',6,NULL,2),(55,'7',6,NULL,3),(56,'7',6,NULL,4),(57,'7',7,NULL,1),(58,'7',7,NULL,2),(59,'7',7,NULL,3),(60,'7',7,NULL,4),(61,'7',8,NULL,1),(62,'7',8,NULL,2),(63,'7',8,NULL,3),(64,'7',8,NULL,4),(65,'7',9,NULL,1),(66,'7',9,NULL,2),(67,'7',9,NULL,3),(68,'7',9,NULL,4),(69,'7',10,NULL,1),(70,'7',10,NULL,2),(71,'7',10,NULL,3),(72,'7',10,NULL,4),(73,'7',11,NULL,1),(74,'7',11,NULL,2),(75,'7',11,NULL,3),(76,'7',11,NULL,4),(77,'7',12,NULL,1),(78,'7',12,NULL,2),(79,'7',12,NULL,3),(80,'7',12,NULL,4),(81,'7',13,NULL,1),(82,'7',13,NULL,2),(83,'7',13,NULL,3),(84,'7',13,NULL,4),(85,'7',14,NULL,1),(86,'7',14,NULL,2),(87,'7',14,NULL,3),(88,'7',14,NULL,4),(89,'7',15,NULL,1),(90,'7',15,NULL,2),(91,'7',15,NULL,3),(92,'7',15,NULL,4),(93,'7',16,NULL,1),(94,'7',16,NULL,2),(95,'7',16,NULL,3),(96,'7',16,NULL,4),(97,'7',20,NULL,1),(98,'7',20,NULL,2),(99,'7',20,NULL,3),(100,'7',20,NULL,4),(101,'7',18,NULL,1),(102,'7',18,NULL,2),(103,'7',18,NULL,3),(104,'7',18,NULL,4),(105,'7',19,NULL,1),(106,'7',19,NULL,2),(107,'7',19,NULL,3),(108,'7',19,NULL,4),(118,'8',4,8,NULL),(117,'8',4,7,NULL),(116,'8',4,4,NULL),(115,'8',4,3,NULL),(114,'8',4,2,NULL),(119,'9',NULL,NULL,1),(120,'9',NULL,NULL,2),(121,'9',NULL,NULL,3),(122,'9',NULL,NULL,4),(130,'10',NULL,NULL,4),(129,'10',NULL,NULL,3),(128,'10',NULL,NULL,2),(127,'10',NULL,NULL,1);
/*!40000 ALTER TABLE `tbl_rel_promo_suc_segmento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_proyecto_redencion`
--

DROP TABLE IF EXISTS `tbl_rel_proyecto_redencion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_proyecto_redencion` (
  `id_rel_proyecto_redencion` int(11) NOT NULL AUTO_INCREMENT,
  `id_proyecto` int(11) DEFAULT NULL,
  `id_redencion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rel_proyecto_redencion`),
  KEY `id_redencion_idx` (`id_redencion`),
  KEY `id_proyecto_idx` (`id_proyecto`),
  CONSTRAINT `tbl_rel_proyecto_redencion_ibfk_1` FOREIGN KEY (`id_proyecto`) REFERENCES `tbl_tipo_proyecto` (`id_tipo_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_rel_proyecto_redencion_ibfk_2` FOREIGN KEY (`id_redencion`) REFERENCES `tbl_tipo_redencion` (`id_tipo_redencion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_proyecto_redencion`
--

LOCK TABLES `tbl_rel_proyecto_redencion` WRITE;
/*!40000 ALTER TABLE `tbl_rel_proyecto_redencion` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_rel_proyecto_redencion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_segmento_categorias`
--

DROP TABLE IF EXISTS `tbl_rel_segmento_categorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_segmento_categorias` (
  `id_rel` int(11) NOT NULL AUTO_INCREMENT,
  `id_segmento` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `id_rel_objeto_categoria` int(11) NOT NULL,
  `valores` varchar(45) NOT NULL,
  `nivel` int(2) NOT NULL,
  PRIMARY KEY (`id_rel`),
  KEY `fk_categoria_idx` (`id_rel_objeto_categoria`),
  KEY `fk_segmento_idx` (`id_segmento`),
  KEY `fk_categoria_idx1` (`id_categoria`),
  CONSTRAINT `tbl_rel_segmento_categorias_ibfk_1` FOREIGN KEY (`id_rel_objeto_categoria`) REFERENCES `tbl_rel_objetos_categorias` (`id_rel`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_rel_segmento_categorias_ibfk_2` FOREIGN KEY (`id_segmento`) REFERENCES `tbl_segmento` (`id_segmento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_segmento_categorias`
--

LOCK TABLES `tbl_rel_segmento_categorias` WRITE;
/*!40000 ALTER TABLE `tbl_rel_segmento_categorias` DISABLE KEYS */;
INSERT INTO `tbl_rel_segmento_categorias` VALUES (9,2,9,10,'min=27,max=39',1),(10,2,8,9,'1',2),(11,3,8,9,'1',1),(12,3,7,8,'min=200,max=9000',2),(19,4,8,9,'1',1),(20,4,9,10,'min=20,max=40',2),(21,7,8,9,'2',1),(22,7,12,14,'2',2),(23,7,12,14,'3',2),(24,7,12,14,'4',2),(25,7,12,14,'3',2),(26,7,12,14,'2',2),(27,8,8,9,'2',1),(28,8,9,10,'min=20,max=50',2);
/*!40000 ALTER TABLE `tbl_rel_segmento_categorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_segmento_usuario`
--

DROP TABLE IF EXISTS `tbl_rel_segmento_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_segmento_usuario` (
  `id_rel_segmento_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `id_segmento` int(11) DEFAULT NULL,
  `id_usuario` int(3) DEFAULT NULL,
  `activo` int(1) DEFAULT NULL,
  PRIMARY KEY (`id_rel_segmento_usuario`),
  KEY `fk_id_segmento_idx` (`id_segmento`),
  KEY `fk_id_usuario_idx` (`id_usuario`),
  CONSTRAINT `tbl_rel_segmento_usuario_ibfk_1` FOREIGN KEY (`id_segmento`) REFERENCES `tbl_segmento` (`id_segmento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_rel_segmento_usuario_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=751 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_segmento_usuario`
--

LOCK TABLES `tbl_rel_segmento_usuario` WRITE;
/*!40000 ALTER TABLE `tbl_rel_segmento_usuario` DISABLE KEYS */;
INSERT INTO `tbl_rel_segmento_usuario` VALUES (717,2,51,0),(719,2,60,0),(721,2,77,0),(723,2,78,0),(724,2,80,0),(727,3,51,0),(729,3,60,0),(731,3,77,0),(733,3,78,0),(735,4,51,0),(737,4,60,0),(739,4,77,0),(740,4,78,0),(742,4,80,0),(745,7,57,0),(746,7,58,0),(747,7,79,0),(748,8,57,0),(749,8,58,0),(750,8,79,0);
/*!40000 ALTER TABLE `tbl_rel_segmento_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_usuario_area`
--

DROP TABLE IF EXISTS `tbl_rel_usuario_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_usuario_area` (
  `id_rel_usuario_area` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(3) DEFAULT NULL,
  `id_area` int(3) DEFAULT NULL,
  `activo` int(2) DEFAULT NULL,
  PRIMARY KEY (`id_rel_usuario_area`),
  KEY `id_usuario_idx` (`id_usuario`),
  KEY `id_area_idx` (`id_area`),
  CONSTRAINT `tbl_rel_usuario_area_ibfk_1` FOREIGN KEY (`id_area`) REFERENCES `tbl_area` (`id_area`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tbl_rel_usuario_area_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_usuario_area`
--

LOCK TABLES `tbl_rel_usuario_area` WRITE;
/*!40000 ALTER TABLE `tbl_rel_usuario_area` DISABLE KEYS */;
INSERT INTO `tbl_rel_usuario_area` VALUES (1,1,1,0),(2,1,2,0),(3,1,5,0),(9,51,4,0),(10,52,4,0),(11,53,4,0),(12,54,4,0),(13,55,4,0),(14,56,4,0),(15,57,4,0),(16,58,4,0),(17,59,3,0),(18,3,3,0),(19,60,4,0),(20,61,3,0),(24,72,4,0),(25,73,4,0),(26,74,4,0),(27,75,4,0),(28,76,4,0),(29,77,4,0),(30,78,4,0),(31,79,4,0),(32,80,4,0);
/*!40000 ALTER TABLE `tbl_rel_usuario_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_usuario_cash`
--

DROP TABLE IF EXISTS `tbl_rel_usuario_cash`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_usuario_cash` (
  `id_rel_usuario_cash` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(3) DEFAULT NULL,
  `id_promo` int(11) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  PRIMARY KEY (`id_rel_usuario_cash`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_usuario_cash`
--

LOCK TABLES `tbl_rel_usuario_cash` WRITE;
/*!40000 ALTER TABLE `tbl_rel_usuario_cash` DISABLE KEYS */;
INSERT INTO `tbl_rel_usuario_cash` VALUES (1,76,10,'2019-07-15 09:45:20');
/*!40000 ALTER TABLE `tbl_rel_usuario_cash` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_usuario_tarjeta`
--

DROP TABLE IF EXISTS `tbl_rel_usuario_tarjeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_usuario_tarjeta` (
  `id_tarjeta` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `tarjeta` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tarjeta`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_usuario_tarjeta`
--

LOCK TABLES `tbl_rel_usuario_tarjeta` WRITE;
/*!40000 ALTER TABLE `tbl_rel_usuario_tarjeta` DISABLE KEYS */;
INSERT INTO `tbl_rel_usuario_tarjeta` VALUES (1,2,'111'),(2,4,'XSGDFG45645GFDG1'),(3,5,'4651'),(4,6,'156'),(5,7,'999'),(6,8,'789'),(7,9,'1234567'),(8,10,'12783783'),(9,11,'98218929818921'),(10,12,'12327'),(11,13,'1234849823'),(12,14,'12348498232'),(13,15,'278917827812781278'),(14,16,'278917827812781278'),(15,17,'09876543'),(16,18,'1515'),(17,19,'5468'),(18,20,'8287872872'),(19,21,'4669'),(20,22,'1728'),(21,23,'2345'),(22,24,'465'),(23,60,''),(24,60,'');
/*!40000 ALTER TABLE `tbl_rel_usuario_tarjeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rol`
--

DROP TABLE IF EXISTS `tbl_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rol` (
  `id_tbl_rol` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id_tbl_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rol`
--

LOCK TABLES `tbl_rol` WRITE;
/*!40000 ALTER TABLE `tbl_rol` DISABLE KEYS */;
INSERT INTO `tbl_rol` VALUES (1,'Administrador'),(2,'Punto de Venta'),(3,'Usuario');
/*!40000 ALTER TABLE `tbl_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_segmento`
--

DROP TABLE IF EXISTS `tbl_segmento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_segmento` (
  `id_segmento` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_segmento` varchar(70) NOT NULL,
  `descripcion_segmento` varchar(255) DEFAULT NULL,
  `activo` int(11) NOT NULL DEFAULT '0',
  `fecha_creacion` datetime DEFAULT NULL,
  PRIMARY KEY (`id_segmento`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_segmento`
--

LOCK TABLES `tbl_segmento` WRITE;
/*!40000 ALTER TABLE `tbl_segmento` DISABLE KEYS */;
INSERT INTO `tbl_segmento` VALUES (2,'HOMBRES VIEJOS','',0,'2019-05-06 17:53:37'),(3,'HOMBRES MAYOR A 200','HOMBRES CON TICKET PROMEDIO SUPERIOR A $200',0,'2019-06-13 12:48:31'),(4,'HOMBRES ENTRE 20 Y 40 AÑOS','',0,'2019-06-13 17:08:41'),(7,'MUJERES CLIENTES FRECUENTES','MUJERES QUE HAGAN AL MENOS 2 VISTAS AL MES POR MÁS DE DOS MESES',0,'2019-06-19 11:52:25'),(8,'MUJERES ENTRE 20 Y 50','MUJERES CON EDAD ENTRE 20 Y 50 AÑOS',0,'2019-06-20 10:45:23');
/*!40000 ALTER TABLE `tbl_segmento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_sucursales`
--

DROP TABLE IF EXISTS `tbl_sucursales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_sucursales` (
  `id_sucursales` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `Direccion` varchar(255) DEFAULT NULL,
  `latitud` varchar(255) DEFAULT NULL,
  `longitud` varchar(255) DEFAULT NULL,
  `tel_sucursal` varchar(45) DEFAULT NULL,
  `horario` varchar(255) DEFAULT NULL,
  `activo` varchar(45) DEFAULT '0',
  PRIMARY KEY (`id_sucursales`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_sucursales`
--

LOCK TABLES `tbl_sucursales` WRITE;
/*!40000 ALTER TABLE `tbl_sucursales` DISABLE KEYS */;
INSERT INTO `tbl_sucursales` VALUES (1,'Izlah Arcos Bosques','Paseo de los Tamarindos 90 Local 1a PB Bosques de las Lomas 5120 CDMX','19.3648861','-99.2028053','9135 0022',' - ','0'),(2,'Izlah Antara','Av Ejército Nacional  843-B Granada 11520 CDMX','19.4391059','-99.2025277',' ',' - ','0'),(3,'Izlah Pedregal','Av. de Las Fuentes  556 A Jardines del Pedregal 1900 CDMX','19.3164865','-99.2121096','5256 3194',' - ','0'),(4,'Izlah CC Santa Fe','AV. VASCO DE QUIROGA  3800 LOCAL 110  Lomas de Santa Fe 5348 CDMX','19.3617423','-99.2735121','5258 0881',' - ','0'),(5,'Izlah Duraznos','BOSQUES DE DURAZNOS 39 LOCAL C-03 Bosques de las Lomas 11700 CDMX','19.4024978','-99.2430577','5245 0658',' - ','0'),(6,'Izlah Galerías','AV. RAFAEL SANZIO   150 LOCAL M-4 Y M-5 RESIDENCIAL LA ESTANCIA 45030','20.6750268','-103.430205','33 3627 7616',' - ','0'),(7,'Angelopolis','Blvrd del Niño Poblano  2510 Reserva Territorial Atlixcáyotl 72197','19.0318392','-98.2329124','222 225 2685',' - ','0'),(8,'Izlah Carso','LAGO ZURICH  245, LOCAL A-17 Amp Granada 11529 CDMX','19.4416471','-99.2040373','4976 0434',' - ','0'),(9,'Izlah Park Plaza','JAVIER BARROS SIERRA  540 LOCAL N3 L1 Santa Fe 1210 CDMX','19.3644609','-99.2598455','5292 1083',' - ','0'),(10,'Izlah Interlomas','Vialidad de la barranca  6  Exhacienda de, Jesus del Monte 52763 CDMX','19.3978511','-99.2825148','2591 0516','09:00am - 09:00pm','0'),(11,'Izlah Samara','AV. SANTA FE 94 Lomas de Sta Fé 1219 CDMX','19.3685157','--99.2589028','Â 5292 6220',' - ','0'),(12,'Izlah Chedraui','AV. VASCO DE QUIROGA   3800 Santa Fe 5348 CDMX','19.3617423','-99.2735121','2591 0516',' - ','0'),(13,'Izlah Monte Athos','Monte Athos  139 Lomas de Chapultepec V Secc 11000 CDMX','19.4211159','-99.2100757,21z','5202 2374',' - ','0'),(14,'Izlah Patio Universidad','AV. POPOCATEPTL  546 LOCAL S-09 Xoco 3330 CDMX','19.3656054','-99.1671273','5658 7883',' - ','0'),(15,'Izlah Antea','CARRETERA QUERÉTARO-SAN LUIS POTOSI    12401 KIOSCO 05 FELIZ OSORES SOTOMAYOR 76220 CDMX','20.6728329','-100.4360409','442 688 6036',' - ','0'),(16,'Izlah Patio Santa Fe','Av. Prolongación paseo de la reforma 400 sc-27 Santa Fe 1210 CDMX','19.3772425','-99.2547309','2591 0414',' - ','0'),(20,'Izlah Paseo Acoxpa','Calz Acoxpa  430 Ex-Hacienda Coapa 14300 CDMX','19.2987486','-99.1370584','5678 1758',' - ','0'),(18,'Izlah Oasis','Av Miguel Ángel de Quevedo S/N Oxtopulco 3330 CDMX','19.3449698','-99.1801154','5658 7883',' - ','0'),(19,'Izlah Centtral Interlomas','Boulevard Palmas Hills  Lote I 2 Valle de las Palmas 52763 CDMX','19.3930243','-99.2808472','1105 0090',' - ','0');
/*!40000 ALTER TABLE `tbl_sucursales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tipo`
--

DROP TABLE IF EXISTS `tbl_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo` (
  `id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo`
--

LOCK TABLES `tbl_tipo` WRITE;
/*!40000 ALTER TABLE `tbl_tipo` DISABLE KEYS */;
INSERT INTO `tbl_tipo` VALUES (1,'Acumulación'),(2,'Redención'),(3,'Bono Bienvenida'),(4,'Bono Cumpleaños'),(5,'Promoción');
/*!40000 ALTER TABLE `tbl_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tipo_comunicacion`
--

DROP TABLE IF EXISTS `tbl_tipo_comunicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo_comunicacion` (
  `id_tipo_comunicacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tipo_comunicacion`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo_comunicacion`
--

LOCK TABLES `tbl_tipo_comunicacion` WRITE;
/*!40000 ALTER TABLE `tbl_tipo_comunicacion` DISABLE KEYS */;
INSERT INTO `tbl_tipo_comunicacion` VALUES (1,'Sms'),(2,'Mail');
/*!40000 ALTER TABLE `tbl_tipo_comunicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tipo_copy`
--

DROP TABLE IF EXISTS `tbl_tipo_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo_copy` (
  `id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo_copy`
--

LOCK TABLES `tbl_tipo_copy` WRITE;
/*!40000 ALTER TABLE `tbl_tipo_copy` DISABLE KEYS */;
INSERT INTO `tbl_tipo_copy` VALUES (1,'Acumulación'),(2,'Redención'),(3,'Bono Bienvenida'),(4,'Bono Cumpleaños'),(5,'Promoción');
/*!40000 ALTER TABLE `tbl_tipo_copy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tipo_proyecto`
--

DROP TABLE IF EXISTS `tbl_tipo_proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo_proyecto` (
  `id_tipo_proyecto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tipo_proyecto`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo_proyecto`
--

LOCK TABLES `tbl_tipo_proyecto` WRITE;
/*!40000 ALTER TABLE `tbl_tipo_proyecto` DISABLE KEYS */;
INSERT INTO `tbl_tipo_proyecto` VALUES (1,'Puntos como % de Consumo'),(2,'Puntos por Visitas'),(3,'Visitas'),(4,'% Incremental');
/*!40000 ALTER TABLE `tbl_tipo_proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tipo_punto_venta`
--

DROP TABLE IF EXISTS `tbl_tipo_punto_venta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo_punto_venta` (
  `id_tipo_punto_venta` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tipo_punto_venta`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo_punto_venta`
--

LOCK TABLES `tbl_tipo_punto_venta` WRITE;
/*!40000 ALTER TABLE `tbl_tipo_punto_venta` DISABLE KEYS */;
INSERT INTO `tbl_tipo_punto_venta` VALUES (26,'Físico'),(27,'Web'),(28,'Otro');
/*!40000 ALTER TABLE `tbl_tipo_punto_venta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tipo_redencion`
--

DROP TABLE IF EXISTS `tbl_tipo_redencion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo_redencion` (
  `id_tipo_redencion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `link_detalle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tipo_redencion`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo_redencion`
--

LOCK TABLES `tbl_tipo_redencion` WRITE;
/*!40000 ALTER TABLE `tbl_tipo_redencion` DISABLE KEYS */;
INSERT INTO `tbl_tipo_redencion` VALUES (2,'Canje de Puntos por Premios, Productos ó Servicios ','../../productos_servicios_catalogo.php'),(3,'Canje por voucher / Dinero electrónico para consumo en tienda',''),(4,'Entrega de Premio al alcanzar \"n\" numero de visitas','../../premios_visitas.php'),(5,'Redencion automatica en el momento de pago','../../porcentaje_incremental.php');
/*!40000 ALTER TABLE `tbl_tipo_redencion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tipo_registro`
--

DROP TABLE IF EXISTS `tbl_tipo_registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo_registro` (
  `id_tipo_registro` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tipo_registro`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo_registro`
--

LOCK TABLES `tbl_tipo_registro` WRITE;
/*!40000 ALTER TABLE `tbl_tipo_registro` DISABLE KEYS */;
INSERT INTO `tbl_tipo_registro` VALUES (1,'Auto Registro'),(2,'Registro Atendido');
/*!40000 ALTER TABLE `tbl_tipo_registro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_usuario`
--

DROP TABLE IF EXISTS `tbl_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usuario` (
  `id_usuario` int(3) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `fecha_ultimo_acceso` datetime DEFAULT NULL,
  `activo` int(2) DEFAULT '0',
  `rol` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `rfc` varchar(255) DEFAULT '0',
  `telefono` varchar(255) DEFAULT '0',
  `tarjeta` varchar(255) DEFAULT '0',
  `fecha_nacimiento` date DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `genero` int(2) DEFAULT NULL,
  `id_tipo_punto_venta` int(11) DEFAULT NULL,
  `fecha_registro_plataforma` date DEFAULT NULL,
  `id_usuario_registro` int(11) DEFAULT NULL,
  `id_nivel` int(11) DEFAULT NULL,
  `id_sucursal` int(11) DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_usuario`),
  KEY `id_rol_idx` (`rol`),
  CONSTRAINT `tbl_usuario_ibfk_1` FOREIGN KEY (`rol`) REFERENCES `tbl_rol` (`id_tbl_rol`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_usuario`
--

LOCK TABLES `tbl_usuario` WRITE;
/*!40000 ALTER TABLE `tbl_usuario` DISABLE KEYS */;
INSERT INTO `tbl_usuario` VALUES (1,'Admin','21232f297a57a5a743894a0e4a801fc3','2019-07-16 10:05:31',0,1,NULL,NULL,NULL,'0','0','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0),(3,'Erick','202cb962ac59075b964b07152d234b70','2019-07-01 17:21:03',0,2,NULL,NULL,NULL,'0','0','0',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0),(51,'5532074768','fcea920f7412b5da7be0cf42b8c93759','2019-06-20 15:53:21',1,3,'','Rogelio','rogelio.pacheco8@desclub.com.mx','','5532074768','','1990-10-04',NULL,1,NULL,'2019-06-14',0,1,NULL,0),(52,'5532074761',NULL,NULL,0,3,'v0hf5ywfdnmt32bq46e6u83bap590fj4sgxbd1kl6zc7idreo','','rogelio.pacheco1@desclub.com.mx','','5532074761','','0000-00-00',NULL,1,NULL,'2019-06-14',0,1,NULL,0),(53,'5532074762',NULL,NULL,0,3,'3z9ecb5hl0fpgd7t2ose78qfjx3ddruv38w01i1c54yk6anm0','','rogelio.pacheco2@desclub.com.mx','','5532074762','','0000-00-00',NULL,1,NULL,'2019-06-14',0,1,NULL,0),(54,'5532074763',NULL,NULL,0,3,'vm2qeflwy3u00t1j2f64e9px258czo5h50brdb30neak7dgsi','','rogelio.pacheco3@desclub.com.mx','','5532074763','','0000-00-00',NULL,1,NULL,'2019-06-14',0,1,NULL,0),(55,'5513370787',NULL,NULL,0,3,'wcj842dagnq273150k04xm9ytdb7feu8osb51rldh5pb8ivz6','','produccion.im@gmail.com','','5513370787','','0000-00-00',NULL,0,NULL,'2019-06-17',0,1,NULL,0),(56,'5591913962',NULL,NULL,0,3,'p3r0s8zyte994bohel6cnugf3d1wkmab7321xdv0574i5q8j0','','hola@hotmail.com','','5591913962','','0000-00-00',NULL,0,NULL,'2019-06-17',0,1,NULL,0),(57,'5562904804','25d55ad283aa400af464c76d713c07ad',NULL,1,3,NULL,'Daniela','daniela.bustamante@loyaltysolutions.mx','','5562904804','','1996-11-01',NULL,2,NULL,'2019-06-19',0,1,NULL,0),(58,'5578785616','64cb8e3a3b89dfb1fcfdd2cc489404f0',NULL,0,3,NULL,'Luvia','luvia-huerta@live.com','','5578785616','','1983-06-26',NULL,2,NULL,'2019-06-20',0,1,NULL,0),(59,'Erick','827ccb0eea8a706c4c34a16891f84e7b',NULL,0,2,NULL,NULL,NULL,'0','0','0',NULL,NULL,NULL,26,NULL,NULL,NULL,NULL,0),(60,'5572787938','25d55ad283aa400af464c76d713c07ad','2019-07-12 14:08:16',0,3,NULL,'Camilo','camilofarelo@gmail.com','','5572787938','','1985-07-19',NULL,1,NULL,'2019-07-01',0,1,NULL,0),(61,'admin1','b524d712d94afe11341619a8840b3258',NULL,0,2,NULL,NULL,NULL,'0','0','0',NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,0),(71,'5532074769',NULL,NULL,0,3,'8693751d603f8',NULL,'pachecob_4100@hotmail.com','0','5532074769','0',NULL,NULL,NULL,NULL,'2019-07-05',NULL,1,NULL,0),(72,'5534353667',NULL,NULL,0,3,'creu6ckq41d1pdoswan7m854h7i0tly3zbb9c0952xgc28fjv','','pachhyko@gmail.com','','5534353667','','0000-00-00',NULL,0,NULL,'2019-07-09',0,1,NULL,0),(73,'5572787939',NULL,NULL,0,3,'hi546nfzm9ycvsrdw65102dgbq1auk5c0lpxa6o98et25fj73','','camilofarulo@gmail.com','','5572787939','','0000-00-00',NULL,0,NULL,'2019-07-10',0,1,NULL,0),(74,'5567887051','fcea920f7412b5da7be0cf42b8c93759','2019-07-10 17:38:42',0,3,'','Erick','erick.linares@desclub.com.mx','','5567887051','','2029-07-13',NULL,1,NULL,'2019-07-10',0,1,NULL,0),(75,'Hhjjghkjhg',NULL,NULL,0,3,'05xypz628f83twksbjddv0c74lno27hq6i5u7r3adg91ef66m','','hggjjj@gmail.com','','Hhjjghkjhg','','0000-00-00',NULL,0,NULL,'2019-07-11',0,1,NULL,0),(76,'5532074760','25d55ad283aa400af464c76d713c07ad','2019-07-11 16:32:37',0,3,'','','rogelio.pacheco@desclub.com.mx','','5532074760','','0000-00-00',NULL,0,NULL,'2019-07-11',0,1,NULL,0),(77,'5538886837','bb4afcd1dc4bb892e7dc377c8cfee6bb','2019-07-11 17:29:38',0,3,NULL,'Jorge','jorge_adan_ldfb@hotmail.com','','5538886837','','1992-05-21',NULL,1,NULL,'2019-07-11',0,1,NULL,0),(78,'5542632092','657a998b33b06bd8abf2a9b65b3fff17','2019-07-12 09:22:17',0,3,NULL,'Jhonatan','bamburiel15@gmail.com','','5542632092','','1990-07-06',NULL,1,NULL,'2019-07-12',0,1,NULL,0),(79,'5586782797','827ccb0eea8a706c4c34a16891f84e7b','2019-07-13 18:01:38',0,3,NULL,'Katryn','padron.katryn@gmail.com','','5586782797','','1992-07-30',NULL,2,NULL,'2019-07-13',0,1,NULL,0),(80,'5520773435','3dbf13a0a8885a8ac189ce5896774df2','2019-07-15 15:49:51',0,3,NULL,'Pedro Pablo Hurtado Ramirez','pete62885@icloud.com','','5520773435','','1988-06-28',NULL,2,NULL,'2019-07-13',0,1,NULL,0);
/*!40000 ALTER TABLE `tbl_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'plataforma_lealtad3'
--
/*!50003 DROP PROCEDURE IF EXISTS `historial_ctes_mes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`mruiz`@`%` PROCEDURE `historial_ctes_mes`(IN n_dia int,IN n_mes int,IN n_anio int)
BEGIN
DECLARE x  INT;
DECLARE str  VARCHAR(255);
 SET x = 1;
 SET str = '';
    WHILE x  <= n_dia DO
		 SET  str = CONCAT(str,(SELECT COUNT(*) AS N from tbl_usuario where rol=3  and activo=0  and MONTH(fecha_registro_plataforma)=n_mes and YEAR(fecha_registro_plataforma)=n_anio and DAY(fecha_registro_plataforma)=x),',');
		 SET  x = x + 1; 
		 END WHILE;
 SELECT str;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `historial_visitas_mes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`mruiz`@`%` PROCEDURE `historial_visitas_mes`(IN n_dia int,IN n_mes int,IN n_anio int,IN tipo int)
BEGIN
DECLARE x  INT;
DECLARE str  VARCHAR(255);
 SET x = 1;
 SET str = '';
    WHILE x  <= n_dia DO
		 SET  str = CONCAT(str,(SELECT COUNT(*) AS N from tbl_registros where id_tipo_registro=tipo and MONTH(fecha_registro)=n_mes and YEAR(fecha_registro)=n_anio and DAY(fecha_registro)=x),',');
		 SET  x = x + 1; 
		 END WHILE;
 SELECT str;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `limpia_base` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `limpia_base`()
BEGIN
delete  from  tbl_rel_config_forma_acumulacion;
 
delete from tbl_rel_config_tipo_redencion;

delete from  tbl_rel_config_mecanica_redencion;

delete from tbl_registros;

delete from tbl_rel_usuario_area ;

delete from tbl_puntos_totales;

delete from tbl_log_transfer_registros;

delete from tbl_rel_segmento_usuario;

delete from tbl_usuario ;

delete from tbl_rel_config_campos_registro ;

delete from tbl_configuracion_proyecto;

delete from tbl_estilo;

delete from tbl_cat_premios_productos_servicios;

delete from tbl_categoria;

delete from tbl_menu;

delete from tbl_niveles;

delete from tbl_promociones;

delete from tbl_segmento;

delete from tbl_sucursales;

delete from tbl_tipo_punto_venta;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-16 10:22:19
