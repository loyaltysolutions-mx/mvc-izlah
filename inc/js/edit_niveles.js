function editlvl(id){
    var name = $('#n'+id).val();
    var type = $('#t'+id).val();
    var ini = $('#i'+id).val();
    var fin = $('#f'+id).val();
    var op = 12;

    ini = parseInt(ini);
    fin = parseInt(fin);

    if(name==''){
        swal('Error!','Debes llenar el campo del Nombre de Nivel','warning');
        throw new Error();
    }else if(ini==''){
        swal('Error!','Debes llenar el campo de De:','warning');
        throw new Error();
    }else if(fin==''){
        swal('Error!','Debes llenar el campo de Hasta:','warning');
        throw new Error();
    }else if(ini>=fin){
        swal('Error!','El Campo Hasta, debe ser mayor al campo De','warning');
        throw new Error();
    }

    $.ajax({
        data:{ op:op, name:name, id:id, type:type, ini:ini, fin:fin },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                swal(" ", "Registro Modificado Correctamente", "success");
                menu('administracion/admin_niveles');
            }else{
                swal('Error!','El Elemento no pudo Modificarse','warning');
            }
        }
    });
}

function activelvl(id){
    var op = 18;
    var table = 'tbl_niveles';
    var field = 'id_nivel';
    $.ajax({
        data:{ id:id, op:op, table:table, field:field },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                //swal(" ", "Usuario Eliminado", "success");
            }
            
        }
    });
}

function inactivelvl(id){
    var op = 19;
    var table = 'tbl_niveles';
    var field = 'id_nivel';
    $.ajax({
        data:{ id:id, op:op, table:table, field:field },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                //swal(" ", "Usuario Eliminado", "success");
            }
            
        }
    });
}


function delrow(id,table,field,res){
    var op = 8;

    $.ajax({
        data:{ op:op, id:id, table:table, field:field, res:res },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                swal(" ", "Registro Eliminado Correctamente", "success");
                menu('administracion/admin_niveles');
            }else{
                swal('Error!','El Registro no pudo eliminarse','warning');
            }
            
        }
    });
}