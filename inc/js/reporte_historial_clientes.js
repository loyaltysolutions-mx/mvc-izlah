/*JS REPORTE HISTORIAL CLIENTES*/

$("#btnCreateSegment").hide();
$("#divTable").hide();
$("#btnConsultar").hide();

var filters = $(".filter");
filters.change(function(){  
  if($(".filter:checked").length > 0)
    $("#btnConsultar").fadeIn();
  else
    $("#btnConsultar").fadeOut();
});

function initFiltros(){
  $("#selectCliente").selectpicker();
  $("#selectSemana").selectpicker();
  $("#selectSegmento").selectpicker();
  $("#selectNivel").selectpicker();
  $('#desde').bootstrapMaterialDatePicker({ 
    format : 'YYYY-MM-DD',
    time : false,         
    lang : 'es', 
    weekStart : 1,
    okText : 'Aceptar',  
    cancelText : 'Cancelar'
  });

  $('#hasta').bootstrapMaterialDatePicker({ 
    format : 'YYYY-MM-DD',
    time : false,         
    lang : 'es', 
    weekStart : 1,
    okText : 'Aceptar',  
    cancelText : 'Cancelar'
  });

  $('#desde').change(function(){ 
    let value = $(this).val();                
    $('#hasta').bootstrapMaterialDatePicker('setMinDate', value);
    $('#hasta').val("");        
  });
}

$("#btnCreateSegment").click(function(){
  var table = $('#tableResult').DataTable();
  var ids = table.columns(0).data();
  initCreateSegmento(ids[0]);
  
});

function initCreateSegmento(rows){  
  let selectSegmento = $("#selectSegmento");  
  let timerInterval
  swal({
    type: 'warning',
    title: 'Elige un segmento:',    
    html: '<select id="idSegmento">'+selectSegmento[0].innerHTML+'</select>',
    onBeforeOpen: () => {      
      //$('#idSegmento').selectpicker();
    },
    showCancelButton: true,
    confirmButtonText: 'Agregar clientes',
    showLoaderOnConfirm: true,
    preConfirm: (login) => {
      if( $("#idSegmento").val() != "" ){
        list = rows.filter(function (x, i, a) { 
          return a.indexOf(x) == i; 
        });
        let link = '../../controller/reportes/c_ajax_reporte.php';
        let dataSegmento = { 'action' : 3, 'idSegmento' : $("#idSegmento").val(), rows : list};
        let beforeFunction = function(){      
          
        };
        let successFunction = function(data){
          if(data.result == 1){
            swal("Exito", "Se ha creado el segmento correctamente", "success");
            $("#card-content").load('../../view/reportes/reporte_historial_clientes.php');
          }else{
            swal("Error", "Error al crear el segmento", "error");
          }
        };

        let failFunction = function(data){
          swal("", 'Ocurrio un problema de conexión.','error');
        };
        connectServer(link, dataSegmento, beforeFunction, successFunction, failFunction);
      }else{
        initCreateSegmento(rows);
      }
    },
    allowOutsideClick: () => !Swal.isLoading()
  });
}

$("#btnConsultar").click(function(){

  var dataForm = {};
  
  let checkSegmento = $("#inputSegmento").prop("checked");
  let checkFechaRegistro = $("#inputFechaRegistro").prop("checked");
  let checkCliente = $("#inputCliente").prop("checked");
  let checkNivel = $("#inputNivel").prop("checked");
  
  if(checkSegmento){
    dataForm['segmento'] = $("#selectSegmento").val();
  }

  if(checkNivel){
    dataForm['nivel'] = $("#selectNivel").val();
  }

  if(checkFechaRegistro){      
    dataForm['fechaRegistro'] = {'desde': $("#desde").val(),'hasta': $("#hasta").val()};      
  }

  if(checkCliente){
    dataForm['cliente'] = $("#selectCliente").val();
  }

  dataForm['action'] = 6;

  let link = '../../controller/reportes/c_ajax_reporte.php';
  let data = dataForm;    
  let beforeFunction = function(){
    if(!$("#divTable").is(":visible"))
      $("#divTable").fadeIn();

    $(".divTable").fadeOut();
  }
  
  let successFunction = function(data){    
    if(data.result){
      var arrayResult = [];
      if(data.result.length > 0){                  
        $.each(data.result, function(key,value1){
          $.each(value1, function(key,value){
            //console.log(value);
            var arraySegmentos = "";          
            $.each(value.segmentos, function(key,segmento){
              arraySegmentos+='<span class="label label-success">'+segmento.nombre_segmento+'</span>&nbsp;';
            });
            let auxArray = ["<span>"+value.id_usuario+"</span>", "<span>"+value.nombreCliente+"</span>", arraySegmentos, value.nombreNivel ? "<span class='label label-info'>"+value.nombreNivel+"</span>" : "Sin Nivel", "<span>"+value.ticket+"</span>", value.monto_ticket != "" ? "<span>"+"$"+value.monto_ticket+"</span>" : "", "<span>"+value.nombreTipo+"</span>", "<span>"+value.nombrePromocion+"</span>", (value.cashback != "" ? "<span>"+"$"+value.cashback+"</span>" : "")+(value.cashbackperc != "" ? "<span>"+value.cashbackperc+"%"+"</span>" : "")+(value.descuento != "" ? (value.tipoPromo == 3 ? "<span>"+"$"+value.descuento+"</span>" : "<span>"+value.descuento+"%"+"</span>") : ""), value.id_tipo_registro && value.puntos != "" ? (value.id_tipo_registro != 2 ? "<span>"+"+"+value.puntos+"</span>" : "<span>"+"-"+value.puntos+"</span>") : "", "<span>"+value.num_visita+"</span>", "<span>"+value.dia_registro+"</span>", value.fecha_registro ? "<span>"+value.fecha_registro+"</span>" : "<span>"+"Sin Fecha"+"</span>"];
            arrayResult.push(auxArray);
          });
        });  
      }

      if(arrayResult.length > 0)
        $("#btnCreateSegment").show();
      else
        $("#btnCreateSegment").hide();
      
      if ( $.fn.dataTable.isDataTable( '#tableResult' ) ) {
        let table = $('#tableResult').DataTable();
        table.destroy();
        $("#tableResult").empty();
      }

      $('#tableResult').DataTable({
        data: arrayResult,
        columns: [{title:"ID"}, {title:"Cliente"}, {title:"Segmento(s) Asignado(s)"}, {title:"Nivel"}, {title:"Ticket"}, {title:"Monto Ticket"}, {title:"Tipo de Transacción"}, {title:"Promoción Aplicada"}, {title:"Descuento"}, {title:"Puntos Otorgados"}, {title:"Nro. Visitas"}, {title:"Dia Registro"}, {title:"Fecha Registro"}],
        dom: 'Bfrtip',
        responsive: true,        
        buttons: [
          { extend: 'excelHtml5', title: 'Reporte_Historial_Clientes-'+new Date().getDay()+(new Date().getMonth()+1)+new Date().getFullYear() },
          { extend: 'pdfHtml5', title: 'Reporte_Historial_Clientes-'+new Date().getDay()+(new Date().getMonth()+1)+new Date().getFullYear(), orientation: 'landscape' },
          { extend: 'csvHtml5', title: 'Reporte_Historial_Clientes-'+new Date().getDay()+(new Date().getMonth()+1)+new Date().getFullYear() },
          'copy',
          'print'
        ]
      });

      $('#tableResult').on( 'page.dt', function (){
        var table = $("#tableResult").DataTable();
        var info = table.page.info();
        $('#pageInfo').html( 'Mostrando: '+info.page+' of '+info.pages );
      });
      $(".divTable").fadeIn();
    }
  };

  let failFunction = function(data){      
    $(".divTable").fadeIn();
  };

  if(!checkFechaRegistro || ($("#desde").val() != "" && $("#hasta").val() != "") ){
    $("#textDesde").text("");
    $("#textHasta").text("");
    connectServer(link, data, beforeFunction, successFunction, failFunction);
  }else{
    if($("#desde").val() == ""){
      $("#textDesde").text("Por favor elige una fecha");
    }
    if($("#hasta").val() == ""){
      $("#textHasta").text("Por favor elige una fecha");
    }
  }  
  
});

$(".divFilter").hide();
let filter = $(".filter");
filter.click(function(){
  let parent = $(this).parent().parent().parent();
  if($(this).prop("checked")){      
    parent.find(".divFilter").fadeIn();
  }else{      
    parent.find(".divFilter").fadeOut();
    parent.find('input, textarea').val('');
    parent.find("select").val('default').selectpicker("refresh");
  }
});

function connectServer(link, data, beforeFunction, successFunction, failFunction){
  $.ajax({
    type: "POST",
    timeout: 20000,
    url: link,
    data: data,
    dataType: "json",
    beforeSend : function(){
        beforeFunction && beforeFunction();
    }
  }).done(function(data){
      console.log(data);
      successFunction && successFunction(data);        
  }).fail(function(data){
      failFunction && failFunction(data);
      console.log(data);
  });
}


initFiltros();
