$("#cardNewClient").hide();
$("#btnNewClient").click(function(){
	$("#cardNewClient").toggle(500);
});

$('[data-toggle="tooltip"]').tooltip({
    container: 'body',
    boundary: 'window'
});

var pageParamTable = $("#tableClientes").DataTable();


$('#tableClientes').on('click', '.btnEdit', function (){
	var btn = $(this);
	let idUsuario = btn.attr("data-action");
	btn.tooltip('hide');
	
	var link = '../../view/administracion/edit_cliente.php';
	
	var data = {'idUsuario' : idUsuario};  
	
	var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
	
	var beforeFunction = function(){    
		$("#card-content").html(div);
	};
	
	var successFunction = function(response){
		$('#card-content').html(response);
	};
	
	var failFunction = function(){    
		showNotification("bg-red", "Error al realizar la petición. Intenta nuevamente", "bottom", "center", null, null);
	};
	
	loadPage(link,data, beforeFunction, successFunction, failFunction);
});

$('#tableClientes').on('click', '.btnDelete', function (){
	var btn = $(this);	
	btn.tooltip('hide');	

	if(btn.attr("data-value") == 1){
		Swal.fire({
			title: 'Actualizar Cliente',
			text: "¿Estas seguro de dar de baja al cliente?. Ésta acción no permitirá acceder al usuario a la plataforma.",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Desactivar cuenta'
		}).then((result) => {
			if (result.value) {
				updateFunction();	
			}
		});	
	}else{
		Swal.fire({
			title: 'Actualizar Cliente',
			text: "Si activas un cliente, éste podrá ingresar nuevamente a la plataforma. ¿Deseas continuar?.",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Activar cuenta'
		}).then((result) => {
			if (result.value) {
				updateFunction();	
			}
		});	
	}

	function updateFunction(){
		let idUsuario = btn.attr("data-action");
		let value = btn.attr("data-value");
		let link = '../../controller/administracion/c_llamadas_ajax.php';
		let data = { 'op' : 29, 'idUsuario' : idUsuario, 'valueStatus' : value };
		let beforeFunction = function(){

		};
		let successFunction = function(data){
			if(data.result == 1){
				swal("Exito","¡Se ha actualizado correctamente el usuario!","success");
				$("#card-content").load('../../view/administracion/admin_clientes.php');
			}else{
			  swal("", 'Ocurrio un problema al actualizar al usuario.','error');
			}
		};
		let failFunction = function(){
			swal("", 'Ocurrio un problema de conexión.','error');
		};			

		connectServer(link, data,  beforeFunction,successFunction, failFunction);
	}

});


$("#formNewClient").on("submit", function(e){
	e.preventDefault();
	Swal.fire({
		title: 'Registrar Cliente',
		text: "Se enviará una notificación al numero o email ingresado para completar su registro. ¿Deseas continuar?.",
		type: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Continuar'
	}).then((result) => {
		if (result.value) {
			let link = '../../controller/valida/c_llamadas_ajax.php';
			let data = $("#formNewClient").serialize()+"&op=4";
			let beforeFunction = function(){

			};
			let successFunction = function(data){
				if(data.result == 1){
					swal("Exito","¡Se ha actualizado correctamente el usuario!","success");
					$("#card-content").load('../../view/administracion/admin_clientes.php');
				}else if(data.result == 2){
					swal("", 'Ocurrio un problema al actualizar al usuario.','error');
				}else{
					swal("", 'El usuario ingresado ya existe en la plataforma.','error');
				}
			};
			let failFunction = function(){
				swal("", 'Ocurrio un problema de conexión.','error');
			};

			connectServer(link, data, beforeFunction, successFunction, failFunction);
		}
	});

	function registrarCliente(){

	}
});

function connectServer(link, data, beforeFunction, successFunction, failFunction){
  $.ajax({
    type: "POST",
    timeout: 8000,
    url: link,
    data: data,
    dataType: "json",
    beforeSend : function(){
        beforeFunction && beforeFunction();
    }
  }).done(function(data){
      successFunction && successFunction(data);
      //console.log(data);
  }).fail(function(data){
      failFunction && failFunction(data);
      console.log(data);
  });
}

function loadPage(link, data, beforeFunction, successFunction, failFunction){
	$.ajax({
		type: "POST",
		timeout: 8000,
		url: link,
		data: data,    
		beforeSend : function(){
		    beforeFunction && beforeFunction();
		}
	}).done(function(data){
		successFunction && successFunction(data);
		//console.log(data);
	}).fail(function(data){
		failFunction && failFunction(data);
		//console.log(data);
	});
}