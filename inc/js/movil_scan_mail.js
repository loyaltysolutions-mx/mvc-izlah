$("#formMail").on('submit', function(e){
    e.preventDefault();

    let data = $("#formMail").serialize();
    let link = 'view_info.php';
    let successFunction = function(response){
        $('#card-content').html(response);
    };
    console.log(data);

    connectServer(link, data, null, successFunction, null);

});

function connectServer(link, data, beforeFunction, successFunction, failFunction){
    $.ajax({
        type: "POST",
        timeout: 20000,
        url: link,
        data: data,        
        beforeSend : function(){
            beforeFunction && beforeFunction();
        }
    }).done(function(data){
        console.log(data);
        successFunction && successFunction(data);
    }).fail(function(data){
        failFunction && failFunction(data);
        console.log(data);
    });
}