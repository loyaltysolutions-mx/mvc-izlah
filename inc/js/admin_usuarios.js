$('#form_add_usuario').hide();
$('#tipo_pv').hide();  
$("#tipo_usu_2").click(function() { 
  $('#tipo_pv').show();   
    });  
$("#tipo_usu_1").click(function() { 
  $('#tipo_pv').hide();  
  $('#tip_pv').val('');   
    });  


   
function addusers(){
  var usu=$('#usuario').val();
  var pass=$('#password').val();
  var tipo_usu=$('input:radio[name=tipo_usu]:checked').val();
  var tipo_punt_vta=$('#tip_pv').val();
    $('#loading').show();
    $(this).prop('disabled', true);   
      $.ajax({
        url: '../../controller/administracion/c_llamadas_ajax.php',
        type: "POST",
        data:{ 
            op:1,
            usu:usu,
            pass:pass,
            tipo_usu:tipo_usu,
            tipo_punt_vta:tipo_punt_vta
         },
        success: function(resp)
            {  
                $('#loading').hide();
                $(this).prop('disabled', false);
                //alert(resp); 
              if(resp==1){
                 swal("", 'El registro se creo correctamente','success');
                 menu('administracion/admin_usuarios');
              }else if(resp==2){
                swal("", 'Esta contraseña ya esta siendo usada por otro usuario','error');
              }else{
                swal("", 'Ocurrio un problema al guardar el registro','error');
              }
            }
        });         
  }

  function editusu(id){
    $.ajax({
        data:{ id:id },
        url: '../../view/administracion/edit_usuarios.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            $('#card-content').html(response);
        }
    });
  }

function delusu(id){
  swal({
      title: '',
      text: "Estas seguro de eliminar el usuario?",
      type: 'question',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'NO',
      confirmButtonText: 'SI'
    }).then((result) => {
    
    if (result.value) {
          var id_usu=id;
        $.ajax({
        url: '../../controller/administracion/c_llamadas_ajax.php',
        type: "POST",
        data:{ 
            op:2,
            id_usu:id_usu
         },
        success: function(resp)
            {  
                
              if(resp>0){
                  swal('','El registro se elimino correctamente','success');
                  menu('administracion/admin_usuarios');
              }else{
                   swal("", 'Ocurrio un problema al guardar el registro','error');
              }
            }
            }); 
        
      }
    });
}
 //MUESTRA DATOS DE REGISTRO   
function toggles(){
  $("#form_add_usuario").toggle(800);
}
   