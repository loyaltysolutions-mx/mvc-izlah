/*function menu(id){
    var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
    $.ajax({
        data:{id:id},
        url: '../'+id+'.php',
        method:'post',
        beforeSend: function(response){
            console.log("click");
            $('#card-content').html(div);
            localStorage.setItem("url",id);
        },
        success: function(response){
            $('#card-content').html('');
            $('#card-content').html(response);
            preloader();
        },
        error: function(){
            $('#card-content').html();
            preloader();
        },
        complete: function() {
         preloader();
        }
    });
}*/

$("#btnEditPerfil").click(function(){
    var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
    $.ajax({
        data:{ 'idUsuario' : $(this).attr("data-action") },
        url: '../administracion/edit_cliente.php',
        method:'post',
        beforeSend: function(response){
            $('#card-content').html(div);
            localStorage.setItem("url","administracion/edit_cliente");
        },
        success: function(response){
            $('#card-content').html('');
            $('#card-content').html(response);
            preloader();
            console.log(response);
        },
        error: function(data){
            $('#card-content').html();
            preloader();
            console.log(data);
        },
        complete: function() {
         preloader();
        }
    });
});

function loadClientes(id){
    var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
    $.ajax({
        data:{ 'idUsuario' : id },
        url: '../administracion/edit_cliente.php',
        method:'post',
        beforeSend: function(response){
            $('#card-content').html(div);
        },
        success: function(response){
            $('#card-content').html('');
            $('#card-content').html(response);
            preloader();
            console.log(response);
        },
        error: function(data){
            $('#card-content').html();
            preloader();
            console.log(data);
        },
        complete: function() {
         preloader();
        }
    });
}