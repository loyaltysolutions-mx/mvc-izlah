function Categoria(elementos, flagRowOR, flagBtnDelete, indice){
	this.elementos = new Array();
    this.elementos.push(elementos);
    this.tagHTML = createHTML(this.elementos, flagRowOR, flagBtnDelete)[0];
    this.indice = indice;
    this.valueInputs = new Array();
    this.idCategoria = null;
    this.idRelacion = null;
    this.showRowOR = flagRowOR;
}

function createHTML(objeto, flagRow, flagBtn){
    let colContainer = $("<div>",{class : "col-lg-12 col-md-12 col-sm-12 col-xs-12"});
    let selectObject = $("<select>",{'name': 'categoria[idCat][]', required:"required", class : "form-control show-tick", title : " -- Elige una categoría -- ", "data-show-subtext":true}).selectpicker("refresh");
    let divDetails = $("<div>",{class : 'col-lg-12 col-md-12 col-sm-12 col-xs-12'});

    $.each(objeto, function(key,obj){
        $.each(obj, function(key,category){
            selectObject.append($("<option>",{value:category.id_categoria, text:category.nombre_categoria, "data-subtext": category.descripcion_categoria})).selectpicker("refresh");
        });
    });

    selectObject.change(function(){
        let val = $(this).val();
        //console.log(objeto);
        var result = objeto[0].filter(function(v) {
            //console.log(v);
            return v.id_categoria == val;
        });
        //console.log(result);
        if(result.length > 0){
            $.each(result[0].inputs, function(key,obj){
                switch(obj.id_objeto){
                    case 1:
                    case 2:
                    case 3:
                    case "1":
                    case "2":
                    case "3":                        
                        let input = $("<input>",{type: obj.tipo, name: "categoria[value]["+obj.name+"][]"});
                        divDetails.append($("<p>").html("<b>Ingresa un(a) "+obj.label+":</b>")).append(input);
                        
                        input.on("change",function(){
                            var name = "value";
                            let valInput = $(this).val();
                            valueInputs.push({ name : valInput });
                            console.log(valueInputs);
                        });
                    break;
                    case "4":
                    case "5":
                        let input = $("<select>",{type: obj.tipo, name: "categoria[]["+obj.name+"][]"});
                        divDetails.append($("<p>").html("<b>Ingresa un(a) "+obj.label+":</b>")).append(input);
                    break;
                }
            });
        }
    });

    colContainer.append(selectObject);
    colContainer.append($("<div>",{class:"row"}).append(divDetails));
    return colContainer;
    
}

Categoria.prototype.getAttributes = function() {
	return this.elementos;
};

Categoria.prototype.getHTML = function() {
	return this.tagHTML;
};

Categoria.prototype.getIndice = function() {
    return this.indice;
};

Categoria.prototype.setIndice = function(newIndex) {
    this.indice = newIndex;
    return true;
};

Categoria.prototype.insertInput = function(input) {    
    this.valueInputs.push(input);
    //console.log(this.valueInputs);
    return true;
};

Categoria.prototype.getInputs = function() {    
    return this.valueInputs;
};

Categoria.prototype.setIDCategoria = function(id) {    
    this.idCategoria = id;
    return true;
};

Categoria.prototype.showRowOR = function(id) {
    return this.showRowOR;
};

Categoria.prototype.setIDRelacion = function(id) {    
    this.idRelacion = id;
    return true;
};

Categoria.prototype.clearInputs = function() {    
    this.valueInputs = new Array();
    return true;
};