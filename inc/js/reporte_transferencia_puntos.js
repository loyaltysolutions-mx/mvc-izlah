/*JS REPORTE CLIENTES*/

function initFiltros(){    
  $("#divTable").hide();
  
  $("#selectSegmento").selectpicker();  
  $("#selectNivel").selectpicker();
  $("#selectPV").selectpicker();
  $("#selectUsuario").selectpicker();
  
  $('#desde').bootstrapMaterialDatePicker({ 
    format : 'YYYY-MM-DD',
    time : false,         
    lang : 'es', 
    weekStart : 1,
    okText : 'Aceptar',  
    cancelText : 'Cancelar'
  });

  $('#hasta').bootstrapMaterialDatePicker({ 
    format : 'YYYY-MM-DD',
    time : false,         
    lang : 'es', 
    weekStart : 1,
    okText : 'Aceptar',  
    cancelText : 'Cancelar'
  });

  $('#desde').change(function(){ 
    let value = $(this).val();                
    $('#hasta').bootstrapMaterialDatePicker('setMinDate', value);
    $('#hasta').val("");        
  });
}

$("#btnCreateSegment").click(function(){
  var table = $('#tableResult').DataTable();
  var ids = table.columns(0).data();
  //console.log(ids);
  initCreateSegmento(ids[0]);  
});

function initCreateSegmento(rows){
  swal("Escribe un nombre para el segmento:", {
    content: {
      element: "input",
      attributes: {
        placeholder: "Campo obligatorio",
        type: "text",
        id:'nombreSegmento'
      },
    },
    buttons: {
      cancelar: "Cancelar",        
      next: "Siguiente",
    }
  })
  .then((value) => {
    switch(value){
      case "next":    
        if( $("#nombreSegmento").val() != "" ){
          var nombreSegmento = $("#nombreSegmento").val();
          swal("Agrega una descripción al segmento", {
            content: {
              element: "input",
              attributes: {
                placeholder: "Campo opcional",
                type: "text",
                id:'descripcionSegmento'
              },
            },
            buttons: {
              cancel: "Cancelar",
              ant: "Regresar",
              create: "Crear Segmento"
            },
          })
          .then((value1) => {
            switch(value1){
              case "create":
                list = rows.filter(function (x, i, a) { 
                    return a.indexOf(x) == i; 
                });
                let link = '../../controller/reportes/c_ajax_reporte.php';
                let dataSegmento = { 'action' : 3, 'nombreSegmento' : nombreSegmento, 'descripcion' : $("#descripcionSegmento").val(), rows : list};
                let beforeFunction = function(){      
                  
                };
                let successFunction = function(data){
                  if(data){
                    swal("Exito", "Se ha creado el segmento correctamente", "success");
                    $("#card-content").load('../../view/reportes/reporte_clientes.php');
                  }else{
                    swal("Error", "Error al crear el segmento", "error");
                  }
                };

                let failFunction = function(data){
                  swal("", 'Ocurrio un problema de conexión.','error');
                };
                connectServer(link, dataSegmento, beforeFunction, successFunction, failFunction);
              break;
              case "ant":
                initCreateSegmento();
              break;
              case "cancel":
                swal("Cancelado", "No se ha creado el segmento", "warning");
              break;
            }
          });
        }else{
          initCreateSegmento();
        }
      break;
      case "cancelar":
        swal("Cancelado", "No se ha creado el segmento", "warning");
      break;         
    }      
  });
}

$("#btnConsultar").click(function(){

  //console.log($('.filter:checkbox:checked'));
  let checkSegmento = $("#inputSegmento").prop("checked");
  let checkNivel = $("#inputNivel").prop("checked");
  let checkPV = $("#inputPV").prop("checked");
  let checkFechaRegistro = $("#inputFechaRegistro").prop("checked");
  let checkUsuario = $("#inputUsuario").prop("checked");  
  let dataForm = {'action' : 9};
  
  if(checkSegmento){
    dataForm['segmento'] = $("#selectSegmento").val();
  }

  if(checkPV){
    dataForm['idPuntoVenta'] = $("#selectPV").val();
  }

  if(checkNivel){
    dataForm['nivel'] = $("#selectNivel").val();
  }
  
  if(checkFechaRegistro){
    dataForm['fechaRegistro'] = {'desde': $("#desde").val(),'hasta': $("#hasta").val()};      
  }

  if(checkUsuario){
    dataForm['idUsuario'] = $("#selectUsuario").val();
  }  

  let link = '../../controller/reportes/c_ajax_reporte.php';
  let data = dataForm;
  let beforeFunction = function(){
    $(".divTable").fadeOut();
  }
  let successFunction = function(data){
    if(data.result){
      var arrayResult = [];
      if(data.result.length > 0){                  
        $.each(data.result, function(key,value){
          let auxArray = [value.id_log, value.nombreOrigen, value.nombre_segmento ? value.nombre_segmento : "", value.nombreNivel ? value.nombreNivel : "", value.nombreDestino, value.num_acumu_transfer,value.cant_p_v_transferidas_ac, value.num_reden_transfer, value.cant_p_v_transferidas_re, value.nombrePV, value.fecha_registro ];          
          arrayResult.push(auxArray);
        });  
      }
      if(arrayResult.length > 0)
        $("#btnCreateSegment").show();
      else
        $("#btnCreateSegment").hide();
      
      if ( $.fn.dataTable.isDataTable( '#tableResult' ) ) {
        let table = $('#tableResult').DataTable();
        table.destroy();
        $("#tableResult").empty();
      }

      let titleFile = 'ReporteTransferenciaPuntos_'+new Date().getDay()+new Date().getMonth()+new Date().getFullYear();

      $('#tableResult').DataTable({
        data: arrayResult,
        columns: [{title:"ID Transferencia"},{title:"Usuario Origen"},{title:"Segmento Usuario Origen"},{title:"Nivel Usuario Origen"},{title:"Usuario Destino"},{title:"Registros de Acumulación Transferidos"},{title:"Pts/Visitas de Acumulación Transferidos"},{title:"Registros de Redencion Transferidas"},{title:"Pts/Visitas de Rendención Transferidas"},{title:"Transferencia realizada por:"},{title:"Fecha de Transferencia"}],
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
          'copy',
          { extend: 'csvHtml5', title: titleFile },
          { extend: 'excelHtml5', title: titleFile },
          { extend: 'pdfHtml5', title: titleFile },
          'print'
        ]
      });

      $('#tableResult').on( 'page.dt', function () {
        var table = $("#tableResult").DataTable();
        var info = table.page.info();
        $('#pageInfo').html( 'Mostrando: '+info.page+' of '+info.pages );
      });
      $("#divTable").fadeIn();
      $(".divTable").fadeIn();
    }      
  };

  let failFunction = function(data){      
    $(".divTable").fadeIn();
  };

  if(checkFechaRegistro){
    if($("#desde").val() != "" && $("#hasta").val() != ""){
      $("#textDesde").text("");
      $("#textHasta").text("");
      connectServer(link, data, beforeFunction, successFunction, failFunction);
    }else{
      if($("#desde").val() == ""){
        $("#textDesde").text("Por favor elige una fecha");
      }
      if($("#hasta").val() == ""){
        $("#textHasta").text("Por favor elige una fecha");
      }
    }
  }else{
      $("#textDesde").text("");
      $("#textHasta").text("");
    connectServer(link, data, beforeFunction, successFunction, failFunction);
  }
});


$(".divFilter").hide();
let filter = $(".filter");
filter.click(function(){
  let parent = $(this).parent().parent().parent();
  if($(this).prop("checked")){      
    parent.find(".divFilter").fadeIn();
  }else{      
    parent.find(".divFilter").fadeOut();
    parent.find('input, textarea').val('');
    parent.find("select").val('default').selectpicker("refresh");
  }
});

function connectServer(link, data, beforeFunction, successFunction, failFunction){
  $.ajax({
    type: "POST",
    timeout: 20000,
    url: link,
    data: data,
    dataType: "json",
    beforeSend : function(){
        beforeFunction && beforeFunction();
    }
  }).done(function(data){
      console.log(data);
      successFunction && successFunction(data);        
  }).fail(function(data){
      failFunction && failFunction(data);
      console.log(data);
  });
}

initFiltros();
