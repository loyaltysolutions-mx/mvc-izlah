initFiltros();
function initFiltros(){

  var slideEdad = document.getElementById('slideEdad');        
  if(slideEdad != null){
    noUiSlider.create(slideEdad, {
      start: [1,100],
      step: 1,
      connect: true,
      range: {
          'min': [1],
          'max': [100]
      },
      format: {
        to: function (value) {                  
            return value;
        },
        from: function (value) {                  
            return value;
        }
      }
    });
    slideEdad.noUiSlider.on('update', function(values, handle) {
      $("#slideEdad").parent().find(".js-nouislider-value").text("Min: "+values[0].toFixed(0)+", Max:"+values[1].toFixed(0));      
    });
  }


  $("#divTable").hide();  
  $("#btnCreateSegment").hide();

  $("#selectSegmento").selectpicker({'language': 'ES'});
  $("#selectStatus").selectpicker({'language': 'ES'});
  $("#selectGenero").selectpicker({'language': 'ES'});
  $("#selectNivel").selectpicker({'language': 'ES'});
 
  
  $('#desde').bootstrapMaterialDatePicker({ 
    format : 'YYYY-MM-DD',
    time : false,         
    lang : 'es', 
    weekStart : 1,
    okText : 'Aceptar',  
    cancelText : 'Cancelar'
  });

  $('#hasta').bootstrapMaterialDatePicker({ 
    format : 'YYYY-MM-DD',
    time : false,         
    lang : 'es', 
    weekStart : 1,
    okText : 'Aceptar',  
    cancelText : 'Cancelar'
  });

  $('#desde').change(function(){ 
    let value = $(this).val();                
    $('#hasta').bootstrapMaterialDatePicker('setMinDate', value);
    $('#hasta').val("");        
  });
}

$("#btnCreateSegment").click(function(){
  var table = $('#tableResult').DataTable();
  var ids = table.columns(0).data();  
  initCreateSegmento(ids[0]);  
});

function initCreateSegmento(rows){  
  let selectSegmento = $("#selectSegmento");  
  let timerInterval
  swal({
    type: 'warning',
    title: 'Elige un segmento:',    
    html: '<select id="idSegmento">'+selectSegmento[0].innerHTML+'</select>',
    onBeforeOpen: () => {      
      //$('#idSegmento').selectpicker();
    },
    showCancelButton: true,
    confirmButtonText: 'Agregar clientes',
    showLoaderOnConfirm: true,
    preConfirm: (login) => {
      if( $("#idSegmento").val() != "" ){
        list = rows.filter(function (x, i, a) { 
          return a.indexOf(x) == i; 
        });
        let link = '../../controller/reportes/c_ajax_reporte.php';
        let dataSegmento = { 'action' : 3, 'idSegmento' : $("#idSegmento").val(), rows : list};
        let beforeFunction = function(){      
          
        };
        let successFunction = function(data){
          if(data.result == 1){
            swal("Exito", "Se ha creado el segmento correctamente", "success");
            $("#card-content").load('../../view/reportes/reporte_clientes.php');
          }else{
            swal("Error", "Error al crear el segmento", "error");
          }
        };

        let failFunction = function(data){
          swal("", 'Ocurrio un problema de conexión.','error');
        };
        connectServer(link, dataSegmento, beforeFunction, successFunction, failFunction);
      }else{
        initCreateSegmento(rows);
      }
    },
    allowOutsideClick: () => !Swal.isLoading()
  });
}

$("#btnConsultar").click(function(){

  //console.log($('.filter:checkbox:checked'));
  let checkSegmento = $("#inputSegmento").prop("checked");
  let checkNivel = $("#inputNivel").prop("checked");
  let checkFechaRegistro = $("#inputFechaRegistro").prop("checked");
  let checkEdad = $("#inputEdad").prop("checked");
  let checkStatus = $("#inputStatus").prop("checked");
  let checkGenero = $("#inputGenero").prop("checked");

  let dataForm = {'action' : 2};
  
  if(checkSegmento){
    dataForm['segmento'] = $("#selectSegmento").val();
  }

  if(checkNivel){
    dataForm['nivel'] = $("#selectNivel").val();
  }
  
  if(checkFechaRegistro){
    dataForm['fechaRegistro'] = {'desde': $("#desde").val(),'hasta': $("#hasta").val()};      
  }

  if(checkEdad){
    let edadSlider = document.getElementById('slideEdad');
    edadSlider = edadSlider.noUiSlider.get();
    dataForm['edad'] = {'min':edadSlider[0].toFixed(0),'max':edadSlider[1].toFixed(0)};   
    console.log(dataForm);   
  }

  if(checkStatus){
    dataForm['status'] = $("#selectStatus").val();
  }

  if(checkGenero){      
    dataForm['genero'] = $("#selectGenero").val();      
  }

  let link = '../../controller/reportes/c_ajax_reporte.php';
  let data = dataForm;
  let beforeFunction = function(){
    $(".divTable").fadeOut();
  }
  let successFunction = function(data){
    if(data.result){
      var arrayResult = [];
      callback(function(){
        if(data.result.length > 0){                  
          $.each(data.result, function(key,value){
            console.log(value);
            
            var arraySegmentos = "";
            var arrayTarjetas = "";
            $.each(value[0].segmentos, function(key,segmento){
              arraySegmentos+='<span class="label label-success">'+segmento.nombre_segmento+'</span>&nbsp;';
            });
            $.each(value[0].tarjetas, function(key,tarjeta){
              arrayTarjetas+='<span class="label bg-amber">'+tarjeta.tarjeta+'</span>&nbsp;';
            });
            let auxArray = [value[0].id_usuario, value[0].nombre, value[0].email, value[0].rfc, value[0].telefono, arrayTarjetas, arraySegmentos, '<span class="label label-primary">'+value[0].nombreNivel+'</span>',value[0].promedio == "" ? "$0.00" : "$"+value[0].promedio,(value[0].activo == 0 ? "Activo" : "Inactivo"), value[0].fecha_nacimiento ? value[0].fecha_nacimiento : "No disponible", value[0].fecha_registro_plataforma ? value[0].fecha_registro_plataforma : "No disponible", value[0].genero ? (value[0].genero == 0 ? "Mujer" : "Hombre") : "No disponible"];          
            arrayResult.push(auxArray);
          });  
        }
      }, function(){
        if ( $.fn.dataTable.isDataTable( '#tableResult' ) ) {
          let table = $('#tableResult').DataTable();
          table.destroy();
          $("#tableResult").empty();
        }

        let titleFile = 'ReporteClientes_'+new Date().getDay()+new Date().getMonth()+new Date().getFullYear();

        $('#tableResult').DataTable({
          data: arrayResult,
          columns: [{title:"ID"},{title:"Nombre"},{title:"Email"},{title:"RFC"},{title:"Telefono"},{title:"Tarjeta(s)"},{title:"Segmento(s)"},{title:"Nivel"},{title:"Promedio de compra (RFM):"},{title:"Status"},{title:"Fecha Nacimiento"},{title:"Fecha Registro"}, {title:"Genero"}],
          dom: 'Bfrtip',
          responsive: true,
          buttons: [
            'copy',
            { extend: 'csvHtml5', title: titleFile },
            { extend: 'excelHtml5', title: titleFile },
            { extend: 'pdfHtml5', title: titleFile, orientation: 'landscape' },
            'print'
          ]
        });
        $("#divTable").fadeIn();
        $(".divTable").fadeIn();
      });
    }      
  };

  let failFunction = function(data){      
    $(".divTable").fadeIn();
  };

  if(checkFechaRegistro){
    if($("#desde").val() != "" && $("#hasta").val() != ""){
      $("#textDesde").text("");
      $("#textHasta").text("");
      connectServer(link, data, beforeFunction, successFunction, failFunction);
    }else{
      if($("#desde").val() == ""){
        $("#textDesde").text("Por favor elige una fecha");
      }
      if($("#hasta").val() == ""){
        $("#textHasta").text("Por favor elige una fecha");
      }
    }
  }else{
      $("#textDesde").text("");
      $("#textHasta").text("");
    connectServer(link, data, beforeFunction, successFunction, failFunction);
  }
});


$(".divFilter").hide();
var filter = $(".filter");
filter.click(function(){
  let parent = $(this).parent().parent().parent();
  if($(this).prop("checked")){      
    parent.find(".divFilter").fadeIn();
  }else{
    callback(function(){
      parent.find(".divFilter").fadeOut();  
    }, function(){
      parent.find('input:text, input:password, input:file, textarea').val('');
      parent.find("select").val('default').selectpicker("refresh");
    });
  }
});

function callback(callback1, callback2){
  callback1 && callback1();
  callback2 && callback2();
}

function connectServer(link, data, beforeFunction, successFunction, failFunction){
  $.ajax({
    type: "POST",
    timeout: 20000,
    url: link,
    data: data,
    dataType: "json",
    beforeSend : function(){
        beforeFunction && beforeFunction();
    }
  }).done(function(data){
      console.log(data);
      successFunction && successFunction(data);        
  }).fail(function(data){
      failFunction && failFunction(data);
      console.log(data);
  });
}


