 //INIALIZAMOS LOS DROPDOWS
 $("#usuario_origen").selectpicker();
 $("#usuario_destino").selectpicker();
//FUNCIONALIDAD QUE TRANSFIERE DATOS
$( "#tranfer_info" ).click(function() {

  var usu_origen=$("#usuario_origen").val();
  var usu_destino=$("#usuario_destino").val();
  var usu_session=$("#id_usu_session").val();
  var msj='';
 if(usu_origen==usu_destino){
	 msj='Los usuarios son los mismos';
 }
 
 if(usu_origen=='' || usu_destino==''){
	  msj='Algún usuario esta en blanco';
 }
  
  if(msj!=''){
	  swal('Error!',msj,'warning');
  }else{
	  //ENTRA PARA LA TRANSFERENCIA DE DATOS
	  $.ajax({
        data:{
				op:10,
				id_usu_origen:usu_origen,
				id_usu_destino:usu_destino,
				id_usu_session:usu_session
				
			},
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
			$('#resp_transfer').html(response);
        }
    });
  }
});