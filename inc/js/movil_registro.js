function btns(id,logo){
	var urls = 'scan_'+id+'.php';
	/*if(id=="id"){
		urls= 'view_info.php';
	}*/
	$.ajax({
	    data:{ logo:logo },
	    url: urls,
	    method:'post',
	    beforeSend: function(response){

	    },
	    success: function(response){
	        $('#card-content').html(response);
	    }
	});
}

function btnqr(id,logo){
	var urls = 'scan_'+id+'.php';
	/*if(id=="id"){
		urls= 'view_info.php';
	}*/
	$.ajax({
	    data:{ logo:logo },
	    url: urls,
	    method:'post',
	    beforeSend: function(response){

	    },
	    success: function(response){
	        $('#vistaqr').html(response);
	    }
	});
}

function sendnum(send){
	var num = $('#cel').val();

	if(send=='r'){
		num = num.substring(0,num.length-1);
		$('#cel').val(num);
	}else{
		if(num.length>=10){
			swal('Error!','Máximo 10 dígitos','warning');
	        throw new Error();
		}
		if(num==null || num=='undefined'){
			num='';
		}
		$('#cel').val(num+''+send);
	}

	
}

function sendtel(logo){
	var tel = $('#cel').val();
	if(tel.length==10){

		$.ajax({
		    data:{ tel:tel,logo:logo },
		    url: 'view_info.php',
		    method:'post',
		    beforeSend: function(response){

		    },
		    success: function(response){
		    	//console.log(tel);
		        $('#card-content').html(response);
		    }
		});
		
	}else{
		swal('Error!','El teléfono debe ser de 10 dígitos','warning');
        throw new Error('Error Intencional');
	}
}

function registrar(logo,resp){
  var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
  $('#loading').html(div);
  var username = $("input[name=nombre]").val();
  var genero = $('#genero').val();
  var cadena = $('#form_registro').serialize();
  var submsj = 'En un momento recibiras un correo de confirmación de registro.';
  
  if(resp>=1){

  	const swalWithBootstrapButtons = Swal.mixin({
	  confirmButtonClass: 'btn btn-success',
	  cancelButtonClass: 'btn btn-danger',
	  buttonsStyling: false,
	})

	swalWithBootstrapButtons.fire({
	  title: '¿Quieres Agregar esta tarjeta a tu Cuenta?',
	  text: "¡El cambio será irreversible!",
	  type: 'warning',
	  showCancelButton: true,
	  confirmButtonText: '¡Si, Agregar!',
	  cancelButtonText: '¡No, cancelar!',
	  reverseButtons: true
	}).then((result) => {
	  if (result.value) {
	  	addtarjeta(cadena);
	    swalWithBootstrapButtons.fire(
	      '',
	      'Tu tarjeta ha sido agregada a tu cuenta',
	      'success'
	    )
	  } else if (
	    // Read more about handling dismissals
	    result.dismiss === Swal.DismissReason.cancel
	  ) {
	    swalWithBootstrapButtons.fire(
	      '',
	      'Operacion Cancelada',
	      'error'
	    )
	  }
	})

  }else if(resp<1){
  	$.ajax({  
	    url: '../../controller/registro/c_llamadas_ajax.php',
	    type: "POST",
	    data:{ 
	        op:1,
	        cadena:cadena
	        
	    },
	    success: function(response){  
	      $('#loading').html('');
	      //response = response.trim();
	      if(response=='El usuario ya existe en la plataforma'){
	      	swal('Error!',response,'warning');
	      }else{
	      	respuesta = JSON.parse(response);
	      	//console.log(respuesta);
	      	if(genero==0 || genero=='0'){
	      		sendmsj('¡Bienvenida',username+'!',logo,submsj,null,null,respuesta.id_usuario);
	      	}else{
	      		sendmsj('¡Bienvenido',username+'!',logo,submsj,null,null,respuesta.id_usuario);
	      	}
	      	
	      }
	      
	    }
	  }); 
  }
     
}

function revisar(logo){
	var cadena = $('#form_registro').serialize();
	$.ajax({  
	    url: '../../controller/registro/c_llamadas_ajax.php',
	    type: "POST",
	    data:{ 
	        op:5,
	        cadena:cadena
	        
	    },
	    success: function(response){ 		 
	      	registrar(logo,response);
	    }
  	});
}

function addtarjeta(cadena){
	$.ajax({  
	    url: '../../controller/registro/c_llamadas_ajax.php',
	    type: "POST",
	    data:{ 
	        op:6,
	        cadena:cadena
	        
	    },
	    success: function(response){  
	      $('#loading').html('');
	      
	    }
  	}); 
}

function sendmsj(msj,username,logo,submsj,congrats,orangetxt,id_user){
	
	$.ajax({
	    data:{ msj:msj, username:username, logo:logo, submsj:submsj, congrats:congrats, orangetxt:orangetxt, opt:3, user:id_user, bs:1 },
	    url: 'msj.php',
	    method:'post',
	    beforeSend: function(response){

	    },
	    success: function(response){
	        $('#card-content').html(response);
	    }
	});
}

function review(){
	if( $('#nivel').length!==0 ){
		//important!!
	  	//$("#nivel").val($("#nivel option:eq(1)").val());
	  	$("#nivel option:eq(1)").attr('selected','selected');
	  	$("#nivel").hide();
	}else{
		
	}
}