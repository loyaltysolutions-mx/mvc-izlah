function addsuc(){
    var suc = $('#suc').val();
    var address = $('#address').val();
    var phone = $('#phone').val();
    var latitud = $('#latitud').val();
    var longitud = $('#longitud').val();
    var ini = $('#open').val();
    var fin = $('#close').val();
    var op = 14;

    $.ajax({
        data:{ op: op, suc: suc, address: address, phone:phone, latitud: latitud, longitud: longitud, ini: ini, fin: fin },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                swal(" ", "Elemento Agregado a Sucursales", "success");
                menu('administracion/admin_sucursales');
            }else{
                swal('Error!','El Elemento no pudo agregarse','warning');
            }
            
        }
    });
}