/*JS DE FORMULARIO DE REGISTRO*/
var arraySegmento = [];
let boxAnd = $("#boxSegmento");
$('#statusSegmentoEdit').selectpicker();
//$("#idUsuario").selectpicker();
$('#loading').hide();
$('#form_add_segmento').hide();
var divLoad = $("#divLoad");
divLoad.hide();
//Tooltip
$('[data-toggle="tooltip"]').tooltip({
    container: 'body',
    boundary: 'window'
});

initSegmento(false, false, false, true);

$(".btnANDCondition").click(function(){
  initSegmento(true, false, true, true);
  console.log(arraySegmento);
});

function initSegmento(showBtnDeleteBox, showBtnDeleteCategoria, showRowAND, showBtnAdd){
  var linkCategoria = "../../controller/administracion/c_llamadas_ajax.php";
  var data = { 'op' : 3, 'action' : 9 };
  var successFunction = function(data){
    //$.each( data, function(key,value){      
      let categoria = new Categoria(data, false, showBtnDeleteCategoria, 0);
      //console.log(data);
      let boxSegmento = new BoxSegmento(arraySegmento.length);
      boxSegmento.insertCategoria(categoria,showBtnAdd,2,1);

      /* INICIO EDICION */
      let divContainer = $("<div>");      
      let divRowAND = $("<div>",{'class':'row', style : "margin-top:8px"}).append( $("<div>",{'class':'col-lg-5 col-md-5 col-sm-5 col-xs-5'}).append($("<hr>",{style : "border: 1px solid gray"})) ).append($("<div>",{'class':'col-lg-2 col-md-2 col-sm-2 col-xs-2'}).append($("<a>",{class:'btn btn-block primary-back', style: "color: white"}).text("AND")) ).append($("<div>",{'class':'col-lg-5 col-md-5 col-sm-5 col-xs-5'}).append($("<hr>",{style : "border: 1px solid gray"})));
      let btnDeleteSegmento = $("<a>",{'href' : 'javascript:;', class : 'btn second-color', style : "box-shadow: none; font-weight:bold"}).append($("<i>",{class:"fas fa-trash-alt"}));
      let divRowBtnSegmento = $("<div>",{class:"row"}).append($("<div>",{'class' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right'}).append(btnDeleteSegmento));

      showRowAND && divContainer.append(divRowAND);
      showBtnDeleteBox && divContainer.append(divRowBtnSegmento);
      //console.log("paso");
      divContainer.append(boxSegmento.getHTML());

      btnDeleteSegmento.click(function(){
        divContainer.remove();        
        arraySegmento.splice(boxSegmento.getIndex(),1);
        $.each(arraySegmento, function(key,value){
          value.setIndex(key);
        });
        console.log(arraySegmento);
      });

      /* FIN EDICION */

      $("#boxSegmento").append(divContainer);
      arraySegmento.push(boxSegmento);
      
    //});
  };
  connectServer(linkCategoria, data, null, successFunction, null);
}

$(".btnUsersSegmento").click(function(){
  let seg = $(this).attr("data-seg");
  $(this).tooltip('hide');
  var link = '../../view/administracion/view_user_segmento.php';
  var data = {'idSegmento' : seg};  
  var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
  var beforeFunction = function(){    
    $("#card-content").html(div);
  };
  var successFunction = function(response){
    $('#card-content').html(response);
  };
  var failFunction = function(){    
    showNotification("bg-red", "Error al realizar la petición. Intenta nuevamente", "bottom", "center", null, null);
  };
  loadPage(link,data, beforeFunction, successFunction, failFunction);
});


if ( $.fn.dataTable.isDataTable( '#tableSegmentos' ) ) {
  let table = $('#tableSegmentos').DataTable();
  table.destroy();
}

var myTable = $('#tableSegmentos').DataTable();


$("#categoria").selectpicker();
$('#categoria').on('changed.bs.select', function (e, clickedIndex, isSelected, previousValue) {
  console.log(previousValue);
});



var pageParamTable = $('#tableSegmentos').DataTable();
$('#tableSegmentos').on( 'click', '.btnRefreshSegmento', function () {
  let btn = $(this);
  btn.tooltip('hide');
  let idSegmento = btn.attr("data-action");
  var link = '../../controller/administracion/c_llamadas_ajax.php';
  var data = {'op' : 3, 'action' : 12, 'idSegmento' : idSegmento};
  var beforeFunction = function(){
    var object = btn.find("div").addClass("rotateIcon"); 
    var icon = btn.find("i").css({top:0});
  };
  var successFunction = function(response){
    var rowBtn = btn.parents('tr');
    //console.log(rowBtn);
    var tableRow = pageParamTable.row(rowBtn);
    let btnViewUsers = $("<a>",{href:""});
    pageParamTable.row(tableRow).cell(rowBtn.find("td")[3]).data(response.count ? response.count : 0).draw();
    response.count ? btn.parent().find(".btnUsersSegmento").fadeIn() : btn.parent().find(".btnUsersSegmento").fadeOut();
    let btnView = $("<a>",{class:"btn btn_color btnUsersSegmento", style:"color:white", href:"javascript:;", 'data-seg':idSegmento });
    btnView.append('<i class="fas fa-eye"></i>');
    btn.parent().find(".btnUsersSegmento").length == 0 && response.count && btn.parent().append("&nbsp;").append(btnView)
    btn.find("div").removeClass("rotateIcon");
    var placementFrom = btn.data('placement-from');
    var placementAlign = btn.data('placement-align');
    var animateEnter = btn.data('animate-enter');
    var animateExit = btn.data('animate-exit');
    var colorName = btn.data('color-name');

    btnView.click(function(){
      let seg = $(this).attr("data-seg");
      var link = '../../view/administracion/view_user_segmento.php';
      var data = {'idSegmento' : seg};  
      var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
      var beforeFunction = function(){
        $("#card-content").html(div);
      };
      var successFunction = function(response){
        $('#card-content').html(response);
      };
      var failFunction = function(){    
        showNotification("bg-red", "Error al realizar la petición. Intenta nuevamente", "bottom", "center", null, null);
      };
      loadPage(link,data, beforeFunction, successFunction, failFunction);
    });

    showNotification(colorName, "¡Segmento Actualizado!", placementFrom, placementAlign, animateEnter, animateExit);
  };
  var failFunction = function(){
    btn.find("div").removeClass("rotateIcon");
    showNotification("bg-red", "Error al actualizar el segmento. Intenta nuevamente", "bottom", "center", null, null);
  };

  connectServer(link, data,  beforeFunction,successFunction, failFunction);
});


function showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
  //console.log("click");
  if (colorName === null || colorName === '') { colorName = 'bg-black'; }
  if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
  if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
  if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
  var allowDismiss = true;

  $.notify({
      message: text
  },{
    type: colorName,
    allow_dismiss: allowDismiss,
    newest_on_top: true,
    timer: 1000,
    placement: {
        from: placementFrom,
        align: placementAlign
    },
    animate: {
        enter: animateEnter,
        exit: animateExit
    },
    template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
    '<span data-notify="icon"></span> ' +
    '<span data-notify="title">{1}</span> ' +
    '<span data-notify="message">{2}</span>' +
    '<div class="progress" data-notify="progressbar">' +
    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
    '</div>' +
    '<a href="{3}" target="{4}" data-notify="url"></a>' +
    '</div>'
  });
}

$('#tableSegmentos').on('click', '.btnViewSegmento', function (){
  let idSegmento = $(this).attr("data-action");
  $(this).tooltip('hide');
  var link = '../../view/administracion/view_info_segmento.php';
  var data = {'idSegmento' : idSegmento};  
  var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
  var beforeFunction = function(){    
    $("#card-content").html(div);
  };
  var successFunction = function(response){
    $('#card-content').html(response);
  };
  var failFunction = function(){    
    showNotification("bg-red", "Error al realizar la petición. Intenta nuevamente", "bottom", "center", null, null);
  };
  loadPage(link,data, beforeFunction, successFunction, failFunction);
});

$('#tableSegmentos').on('click', '.btnDeleteSegmento', function (){
  let object = $(this).parents("tr");
  swal({
    title: 'Eliminar Segmento',
    text: "¿Estas seguro de eliminar el segmento?. Ésta acción no se puede deshacer.",
    icon:'warning',      
    buttons: {
      cancel: 'Cancelar',
      catch: 'Eliminar',
    },
    dangerMode: true,      
  })
  .then((willDelete) => {
    if (willDelete) {
      var idSegmento = $(this).attr("data-value");
      let link = '../../controller/administracion/c_llamadas_ajax.php';
      let data = {'op' : 3, 'action' : 3, 'idSegmento' : idSegmento};
      let beforeFunction = function(){
        
      };
      let successFunction = function(data){        
        
        if(data.result == 1){
          swal("¡Se ha eliminado el segmento correctamente!", {
            icon: "success",
          });          
          var tableRow = pageParamTable.row(object);          
          pageParamTable.row(tableRow).remove().draw();
          
        }else{
          swal("", 'Ocurrio un problema al eliminar el segmento.','error');
        }
      };
      let failFunction = function(){
        swal("", 'Ocurrio un problema de conexión.','error');
      };

      connectServer(link, data,  beforeFunction,successFunction, failFunction);        
    }
  });
});

$('#muestra_form_add_usu').click(function(){  
  $("#form_add_segmento").toggle(800);   
});

$(".btn").click(function(){
  if($(this).find(".fa-plus").length > 0)
    $(this).find(".fa-plus").removeClass("fa-plus").addClass("fa-minus");
  else
    $(this).find(".fa-minus").removeClass("fa-minus").addClass("fa-plus");
  
});  

$("#formSegmento").on("submit", function(e){
  e.preventDefault();
  let datos = [];

  $.each(arraySegmento, function(key, value){
    var inputs = value.getCategorias();    
    var aux = [];
    $.each(inputs, function(key1, value1){
      aux.push({idCat:value1.idCategoria,idRel:value1.idRelacion, data:value1.getInputs()});
    });
    datos.push(aux);
  });
  
  var link = "../../controller/administracion/c_llamadas_ajax.php";
  var data = {'op' : 3, 'action' : 11, 'nombre': $("#nombreSegmento").val(), 'descripcion': $("#descripcionSegmento").val(), 'datos': datos };
  var beforeFunction = function(){
      
  };
  var successFunction = function(data){
    if(data){
      if(data.result == 1){
        showNotification("bg-green","Se ha creado el segmento correctamente", "bottom", "center", null, null);
        $("#card-content").load("../../view/administracion/admin_segmento.php");
      }else{
        showNotification("bg-red","Error al crear el segmento, intenta nuevamente", "bottom", "center", null, null);
      }
    }else{
      showNotification("bg-red","Error de conexión. Intenta nuevamente", "bottom", "center", null, null);
    }
  };
  var failFunction = function(data){
    showNotification("bg-red","Ocurrio un problema de conexión", "bottom", "center", null, null);
  };
  connectServer(link,data,beforeFunction,successFunction,failFunction);
});


function callback(callback1, callback2){
  callback1 && callback1();
  callback2 && callback2();
}

function connectServer(link, data, beforeFunction, successFunction, failFunction){
  $.ajax({
    type: "POST",
    timeout: 15000,
    url: link,
    data: data,
    dataType: "json",
    beforeSend : function(){
        beforeFunction && beforeFunction();
    }
  }).done(function(data){
      successFunction && successFunction(data);
      //console.log(data);
  }).fail(function(data){
      failFunction && failFunction(data);
      console.log(data);
  });
}

function loadPage(link, data, beforeFunction, successFunction, failFunction){
  $.ajax({
    type: "POST",
    timeout: 15000,
    url: link,
    data: data,    
    beforeSend : function(){
        beforeFunction && beforeFunction();
    }
  }).done(function(data){
      successFunction && successFunction(data);
      //console.log(data);
  }).fail(function(data){
      failFunction && failFunction(data);
      //console.log(data);
  });
}

