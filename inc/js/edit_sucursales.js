function editsuc(id){
    var suc = $('#n'+id).val();
    var address = $('#a'+id).val();
    var lat = $('#l'+id).val();
    var long = $('#g'+id).val();
    var phone = $('#p'+id).val();
    var open = $('#o'+id).val();
    var close = $('#c'+id).val();
    var op = 15;

    if(suc==''){
        swal('Error!','Debes llenar el campo del Nombre de Sucursal','warning');
        throw new Error();
    }else if(address==''){
        swal('Error!','Debes llenar el campo de Dirección','warning');
        throw new Error();
    }else if(lat==''){
        swal('Error!','Debes llenar el campo de Latitud:','warning');
        throw new Error();
    }else if (long=='') {
        swal('Error!','Debes llenar el campo de Longitud:','warning');
        throw new Error();
    }else if (phone=='') {
        swal('Error!','Debes llenar el campo de Teléfono:','warning');
        throw new Error();
    }

    $.ajax({
        data:{ op:op, id:id, suc:suc, address:address, lat:lat, long:long, phone:phone, open:open, close:close },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                swal(" ", "Registro Modificado Correctamente", "success");
                menu('administracion/admin_sucursales');
            }else{
                swal('Error!','El Elemento no pudo Modificarse','warning');
            }
        }
    });
}

function activesuc(id){
    var op = 18;
    var table = 'tbl_sucursales';
    var field = 'id_sucursales';
    $.ajax({
        data:{ id:id, op:op, table:table, field:field },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                //swal(" ", "Usuario Eliminado", "success");
            }
            
        }
    });
}

function inactivesuc(id){
    var op = 19;
    var table = 'tbl_sucursales';
    var field = 'id_sucursales';
    $.ajax({
        data:{ id:id, op:op, table:table, field:field },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                //swal(" ", "Usuario Eliminado", "success");
            }
            
        }
    });
}

function delrow(id,table,field,res){
    var op = 8;

    $.ajax({
        data:{ op:op, id:id, table:table, field:field, res:res },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                //swal(" ", "Usuario Eliminado", "success");
                //menus('usuario/edituser');
            }else if(response==2){
                swal(" ", "Registro Eliminado Correctamente", "success");
                menu('administracion/edit_menu');
            }else if(response==3){
                swal(" ", "Categoría Eliminada Correctamente", "success");
                menus('administracion/edit_categorias');
            }else if(response==4){
                swal(" ", "Sucursal Eliminada", "success");
                menu('administracion/admin_sucursales');
            }else if(response==5){
                //swal(" ", "Promoción Eliminada", "success");
                //menus('usuario/editpromo');
            }else{
                swal('Error!','El Registro no pudo eliminarse','warning');
            }
            
        }
    });
}