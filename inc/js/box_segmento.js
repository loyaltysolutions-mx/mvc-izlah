function BoxSegmento(indice) {    
	
    var arrayCategoria = new Array();
    this.tagHTML = createBoxHTML(arrayCategoria, false, true, true, 1)[0];
    this.indice = indice;
    //var insertRowOR = 1;
    
    function createBoxHTML(objeto, flagBtnAdd, typeRow, insertRowOR){
        let divContainer = $("<div>");
        
        let col = $("<div>",{'class':'col-lg-12 col-md-12 col-sm-12 col-xs-12'});
        var totalContainer = $("<div>",{class: "row boxSegmento"});
        let col1 = $("<div>",{'class':'col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right'});
        let divAND = $("<div>",{class: "row boxAnd"});
        var selectObject = null;

        $.each(objeto, function(key,obj){
            //console.log(obj);
            var btnDeleteCategoria = $("<a>",{'href' : 'javascript:;', class : 'btn second-color', style : "box-shadow: none; font-weight:bold; color:gray", "data-index" : indice }).append($("<i>",{class:"fas fa-trash-alt"}));
            let divRowBtnCategoria = $("<div>",{'class' : 'col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right'}).append(btnDeleteCategoria);

            let divRowAND = $("<div>",{'class':'row', style : "margin-top:8px"}).append( $("<div>",{'class':'col-lg-5 col-md-5 col-sm-5 col-xs-5'}).append($("<hr>",{style : "border: 1px solid gray"})) ).append($("<div>",{'class':'col-lg-2 col-md-2 col-sm-2 col-xs-2'}).append($("<a>",{class:'btn btn-block primary-back', style: "color: white"}).text(typeRow == 1 ? "AND" : "OR")) ).append($("<div>",{'class':'col-lg-5 col-md-5 col-sm-5 col-xs-5'}).append($("<hr>",{style : "border: 1px solid gray"})));
            let row = $("<div>",{'class':'row align-items-center'});             
            let divBoxAND = $("<div>",{'class':'row'}); 
            let colHeader = $("<div>",{class : (obj.flagBtnDelete ? "col-lg-6 col-md-6 col-sm-6 col-xs-6" : "col-lg-12 col-md-12 col-sm-12 col-xs-12") }).append('<p class="second-color" style="padding:8px"><b>Nueva Condición:</b></p>');
            let colContainer = $("<div>",{class : "col-lg-12 col-md-12 col-sm-12 col-xs-12", style: "padding:15px"});
            selectObject = $("<select>",{required:"required", title : " -- Elige una categoría -- ", "data-show-subtext":true, "data-width":"100%"});
            let divDetails = $("<div>",{class : 'col-lg-12 col-md-12 col-sm-12 col-xs-12'});
            
            var elementos = obj.getAttributes()[0];
            
            $.each(elementos, function(key,category){
                selectObject.append($("<option>",{value:category.id_categoria, text:category.nombre_categoria, "data-subtext": category.descripcion_categoria}));
            });

            selectObject.change(function(){
                var valSelect = $(this).val();
                var result = obj.elementos[0].filter(function(v) {
                    //console.log(v);
                    return v.id_categoria == valSelect; // Filter out the appropriate one
                });

                //console.log(result);
                if(result.length > 0){
                    divDetails.empty();
                    obj.setIDCategoria(valSelect-0);
                    obj.clearInputs();
                    $.each(result[0].inputs, function(key,objResult){
                        switch(objResult.id_objeto){                        
                            case "1":
                            case "2":
                            case "3":                            
                                var input = $("<input>",{type: objResult.tipo, required:true, class:"form-control", min: objResult.tipo == "number" ? 1 : "", value:objResult.tipo == "number" ? 1 : ""});
                                var inputGroup = $("<div>",{ class : "input-group"});
                                var addOn = $("<span>",{ class:"input-group-addon" }).append($("<i>",{class:"material-icons", text:valSelect == 1 ? "cake" : (valSelect == 2 ? "date_range" : "today")}));
                                
                                divDetails.append($("<p>").html("<b>"+objResult.label+":</b>"));
                                divDetails.append(inputGroup.append(addOn).append($("<div>",{class:"form-line"}).append(input)));
                                
                                input.on("change",function(){
                                    var valInput = $(this).val();
                                    var aux = {};
                                    aux["value"] = valInput;
                                    aux["name"] = objResult.name;
                                    aux["id_objeto"] = objResult.id_objeto;
                                    
                                    obj.insertInput(aux);
                                    obj.setIDRelacion(objResult.id_rel-0);
                                });
                            break;                        
                            case "4":
                            case "5":
                                let select = $("<select>",{ "data-live-search":true, title: valSelect == 5 ? " -- Elige uno o varios Puntos de Venta -- " : (valSelect == 6 ? " -- Elige uno o varios Clientes -- " : (valSelect == 10 ? " -- Elige uno o más días -- " : " -- Elige un género -- ")), "data-width":"100%"});
                                objResult.tipo != "" && objResult.tipo != null && select.attr("multiple",objResult.tipo);
                                select.empty();
                                
                                $.each(objResult.container, function(key, value){
                                    select.append($("<option>",{value: value.id_usuario, text: valSelect == 5 ? value.usuario : value.nombre}));    
                                });

                                var inputGroup = $("<div>",{ class : "input-group"});
                                var addOn = $("<span>",{ class:"input-group-addon" }).append($("<i>",{class:"material-icons", text:valSelect == 5 ? "pin_drop" : (valSelect == 10 ? " date_range " : " person ")}));
                                divDetails.append($("<p>").html("<b>"+objResult.label+":</b>"));                            
                                divDetails.append(inputGroup.append(addOn).append($("<div>",{class:"form-line"}).append(select)));
                                //divDetails.append();
                                //divDetails.find(select).selectpicker();

                                select.change(function(){                                
                                    var aux = {};
                                    let selectItems = $(this).val();

                                    aux['valores'] = selectItems.length ? $(this).val() : $(this).val()-0;
                                    aux["name"] = objResult.name;
                                    aux["id_objeto"] = objResult.id_objeto;
                                    selectItems.length && obj.clearInputs();
                                    obj.insertInput(aux);
                                    obj.setIDRelacion(objResult.id_rel-0);
                                    //console.log(obj.getInputs());
                                });

                                select.selectpicker();
                            break;

                            case "6":
                            case "7":
                            case "8":
                            case "9":
                                var inputMin = $("<input>",{type: objResult.tipo == "date" ? "text" : (objResult.tipo == "money" ? "number" : objResult.tipo), step: (objResult.tipo == "money" || objResult.tipo == "number" ? 0.01 : ""), required:true, class:"form-control", placeholder : "Valor mínimo", min: objResult.tipo == "number" ? 1 : (objResult.tipo == "money" ? 0.01 : ""), value: objResult.tipo == "number" || objResult.tipo == "money" ? "1.00" : ""});
                                var inputGroupMin = $("<div>",{ class : "input-group"});
                                var addOnMin = $("<span>",{ class:"input-group-addon" }).append($("<i>",{class:"material-icons", text: objResult.tipo == "date" ? "date_range" : (objResult.tipo == "number" ? "expand_more" :  (objResult.tipo == "money" ? "attach_money" : "access_time"))}));

                                var inputMax = $("<input>",{type: objResult.tipo == "date" ? "text" : (objResult.tipo == "money" ? "number" : objResult.tipo), step: (objResult.tipo == "money" || objResult.tipo == "number" ? 0.01 : ""), required:true, class:"form-control", placeholder : "Valor máximo", min: objResult.tipo == "number" ? 1 : (objResult.tipo == "money" ? 0.01 : ""), value:objResult.tipo == "number" || objResult.tipo == "money" ? "1.00" : ""});
                                var inputGroupMax = $("<div>",{ class : "input-group"});
                                var addOnMax = $("<span>",{ class:"input-group-addon" }).append($("<i>",{class:"material-icons", text: objResult.tipo == "date" ? "date_range" : (objResult.tipo == "number" ? "expand_less" :  (objResult.tipo == "money" ? "attach_money" : "access_time"))}));
                                
                                divDetails.append($("<p>").html("<b>"+objResult.label+":</b>"));
                                divDetails.append(inputGroupMin.append(addOnMin).append($("<div>",{class:"form-line"}).append(inputMin)));
                                divDetails.append(inputGroupMax.append(addOnMax).append($("<div>",{class:"form-line"}).append(inputMax)));

                                if(objResult.tipo == "date"){
                                    inputMin.bootstrapMaterialDatePicker({ 
                                        format : 'YYYY-MM-DD',
                                        time : false,         
                                        lang : 'es', 
                                        weekStart : 1,
                                        okText : 'Aceptar',  
                                        cancelText : 'Cancelar'
                                    });

                                    inputMax.bootstrapMaterialDatePicker({ 
                                        format : 'YYYY-MM-DD',
                                        time : false,         
                                        lang : 'es', 
                                        weekStart : 1,
                                        okText : 'Aceptar',  
                                        cancelText : 'Cancelar'
                                    });
                                }

                                if(objResult.tipo == "time"){
                                    inputMin.bootstrapMaterialDatePicker({
                                        format: "HH:mm",
                                        date : false,
                                        lang : 'es',                                         
                                        okText : 'Aceptar',  
                                        cancelText : 'Cancelar'
                                    });

                                    inputMax.bootstrapMaterialDatePicker({ 
                                        format: "HH:mm",
                                        date : false,         
                                        lang : 'es',                                         
                                        okText : 'Aceptar',  
                                        cancelText : 'Cancelar'
                                    });
                                }

                                inputMin.change(function(){
                                    let valueMin = $(this).val();
                                    objResult.tipo == "date" || objResult.tipo == "time" && inputMax.bootstrapMaterialDatePicker('setMinDate', valueMin);
                                    (objResult.tipo == "money" || objResult.tipo == "number" ) && inputMax.attr('min', valueMin);
                                    inputMax.val("");
                                });

                                inputMax.change(function(){
                                    let valueMax = $(this).val();
                                    if(inputMin.val() != "" && valueMax != ""){
                                        var aux = {};
                                        aux["value"] = {min: (objResult.tipo == "money" || objResult.tipo == "number") ? inputMin.val().replace(",",".")-0 : inputMin.val(), max: (objResult.tipo == "money" || objResult.tipo == "number") ? valueMax.replace(",",".")-0 : valueMax };
                                        aux["name"] = objResult.name;
                                        aux["id_objeto"] = objResult.id_objeto;                                        
                                        obj.insertInput(aux);
                                        obj.setIDRelacion(objResult.id_rel-0);
                                    }
                                });
                            break;

                            case "10":
                                //console.log("Entro");
                                let slide = $('<div>');
                                var inputGroup = $("<div>",{ class : "input-group"});
                                var addOn = $("<span>",{ class:"input-group-addon" }).append($("<i>",{class:"material-icons", text: "cake"}));
                                let spanValores = $("<span>");
                                
                                divDetails.append($("<p>").html("<b>"+objResult.label+":</b>"));
                                divDetails.append(inputGroup.append(addOn).append($("<div>").append(slide)).append($("<div>",{class:"m-t-20 font-12"}).append("<b>Valores:</b>").append(spanValores)));

                                noUiSlider.create(slide[0], {
                                    start: [1,100],
                                    step: 1,
                                    connect: true,
                                    range: {
                                      'min': [1],
                                      'max': [100]
                                    },
                                    format: {
                                        to: function (value) {                  
                                            return value;
                                        },
                                        from: function (value) {                  
                                            return value;
                                        }
                                    }
                                });
                                slide[0].noUiSlider.on('update', function(values, handle){
                                    spanValores.text("Min: "+values[0].toFixed(0)+", Max:"+values[1].toFixed(0));
                                    var valInput = [values[0].toFixed(0)-0, values[1].toFixed(0)-0];
                                    //console.log(valInput);
                                    var aux = {};

                                    aux["value"] = valInput;
                                    aux["name"] = objResult.name;
                                    aux["id_objeto"] = objResult.id_objeto;
                                    obj.clearInputs();
                                    obj.insertInput(aux);
                                    obj.setIDRelacion(objResult.id_rel-0);
                                });
                            break;
                        }
                    });
                }
            });

            var headerBox = $("<div>",{'class':'row align-items-center primary-back'}).append(colHeader);
            obj.flagBtnDelete && headerBox.append(divRowBtnCategoria);            
            var inputGroupCategory = $("<div>",{ class : "input-group"});
            let addOnCategory = $("<span>",{ class:"input-group-addon" }).append($("<i>",{class:"material-icons", text: "category"}));        
            colContainer.append(inputGroupCategory.append(addOnCategory).append($("<div>",{class:"form-line"}).append(selectObject)));
            //colContainer.append(selectObject);
            valueSelectCategoria = obj.getIDCategoria();            
            //colContainer.find(selectObject).selectpicker();
            //selectObject.selectpicker('val', valueSelectCategoria);

            selectObject.selectpicker();
            selectObject.selectpicker('val', valueSelectCategoria);

            let containerSegmento = $("<div>",{'class':'col-lg-12 col-md-12 col-sm-12 col-xs-12'}).append(headerBox).append($("<div>",{'class':'row align-items-center'}).append(colContainer)).append($("<div>",{'class':'row align-items-center'}).append(divDetails));
            obj.showRowOR && ( insertRowOR == 1 ?  divAND.append(divRowAND) : divContainer.append(divRowAND) );
            divAND.append(containerSegmento);

            /**** Inicio de construccion de los elementos dentro de las categorias ****/
            
            $.each(obj.getInputs(),function(index, inputA){
                //console.log(inputA);
                switch(inputA.id_objeto){
                    case "1":
                    case "2":
                    case "3":
                        var input = $("<input>",{type: "text", required:true, class:"form-control", min: inputA.tipo == "number" ? 1 : "", value : inputA.value });
                        var inputGroup = $("<div>",{ class : "input-group"});
                        var addOn = $("<span>",{ class:"input-group-addon" }).append($("<i>",{class:"material-icons", text:obj.getIDCategoria() == 1 ? "cake" : (obj.getIDCategoria() == 2 ? "date_range" : "today")}));
                        
                        divDetails.append($("<p>").html("<b>"+inputA.label+":</b>"));
                        divDetails.append(inputGroup.append(addOn).append($("<div>",{class:"form-line"}).append(input)));
                        
                        input.on("change",function(){
                            console.log(inputA);
                            var aux = {};
                            aux["value"] = $(this).val();
                            aux["name"] = inputA.name;
                            aux["id_objeto"] = inputA.id_objeto;
                            obj.clearInputs();
                            obj.insertInput(aux);
                            obj.setIDRelacion(inputA.id_relacion-0);
                            
                        });
                    break;
                    case "4":
                    case "5":
                        //console.log(inputA);
                        var select = $("<select>",{ "data-live-search":true, title: obj.getIDCategoria() == 5 ? " -- Elige uno o varios Puntos de Venta -- " : (obj.getIDCategoria() == 6 ? " -- Elige uno o varios Clientes -- " : " -- Elige un género -- "), "data-width":"100%"});
                        inputA.tipo != "" && inputA.tipo != null && select.attr("multiple",inputA.tipo);
                        select.empty();
                        //select.append($("<option>",{value:"", text: valSelect == 5 ? " -- Elige uno o varios Puntos de Venta -- " : " -- Elige uno o varios Clientes -- "}));                            
                        //console.log(elementos);
                        let container = elementos.filter(function(v) {
                            //console.log(v);
                            return v.id_categoria == obj.getIDCategoria(); // Filter out the appropriate one
                        });
                        
                        $.each(container[0].inputs[0].container, function(key, value){
                            select.append($("<option>",{value: value.id_usuario, text: obj.getIDCategoria() == 5 ? value.usuario : value.nombre}));    
                        });

                        var inputGroup = $("<div>",{ class : "input-group"});
                        var addOn = $("<span>",{ class:"input-group-addon" }).append($("<i>",{class:"material-icons", text:obj.getIDCategoria() == 5 ? "pin_drop" : "person"}));
                        divDetails.append($("<p>").html("<b>"+inputA.label+":</b>"));                            
                        divDetails.append(inputGroup.append(addOn).append($("<div>",{class:"form-line"}).append(select)));
                        //divDetails.append();
                        //divDetails.find(select).selectpicker();
                        //divDetails.find(select).selectpicker('val', inputA.valores);
                        //console.log(obj.getIDCategoria());

                        select.change(function(){
                            var aux = {};
                            let selectItems = $(this).val();

                            console.log(selectItems);

                            aux['valores'] = selectItems != null ? ( selectItems.length ? $(this).val() : $(this).val()-0 ) : 0 ;
                            aux["name"] = inputA.name;
                            aux["id_objeto"] = inputA.id_objeto;
                            obj.clearInputs();
                            obj.insertInput(aux);
                            obj.setIDRelacion(inputA.id_relacion-0);
                            
                        });

                        select.selectpicker();
                        select.selectpicker('val', inputA.valores);

                        //console.log("getInputs");
                    break;

                    case "6":
                    case "7":
                    case "8":
                    case "9":
                        //console.log(inputA.valores);
                        //console.log(inputA);
                        var inputMin = $("<input>",{type: inputA.tipo == "date" || inputA.tipo == "time" ? "text" : (inputA.tipo == "money" ? "number" : inputA.tipo), step: (inputA.tipo == "money" || inputA.tipo == "number" ? 0.01 : ""), required:true, class:"form-control", placeholder : "Valor mínimo", min: inputA.value.min, value: inputA.value.min});
                        var inputGroupMin = $("<div>",{ class : "input-group"});
                        var addOnMin = $("<span>",{ class:"input-group-addon" }).append($("<i>",{class:"material-icons", text: inputA.tipo == "date" ? "date_range" : (inputA.tipo == "number" ? "expand_more" :  (inputA.tipo == "money" ? "attach_money" : "access_time"))}));

                        var inputMax = $("<input>",{type: inputA.tipo == "date" ? "text" : (inputA.tipo == "money" ? "number" : inputA.tipo), step: (inputA.tipo == "money" || inputA.tipo == "number" ? 0.01 : ""), required:true, class:"form-control", placeholder : "Valor máximo", min: inputA.value.max, value: inputA.value.max});
                        var inputGroupMax = $("<div>",{ class : "input-group"});
                        var addOnMax = $("<span>",{ class:"input-group-addon" }).append($("<i>",{class:"material-icons", text: inputA.tipo == "date" ? "date_range" : (inputA.tipo == "number" ? "expand_less" :  (inputA.tipo == "money" ? "attach_money" : "access_time"))}));
                        
                        divDetails.append($("<p>").html("<b>"+inputA.label+":</b>"));
                        divDetails.append(inputGroupMin.append(addOnMin).append($("<div>",{class:"form-line"}).append(inputMin)));
                        divDetails.append(inputGroupMax.append(addOnMax).append($("<div>",{class:"form-line"}).append(inputMax)));

                        if(inputA.tipo == "date"){
                            inputMin.bootstrapMaterialDatePicker({ 
                                format : 'YYYY-MM-DD',
                                time : false,         
                                lang : 'es', 
                                weekStart : 1,
                                okText : 'Aceptar',  
                                cancelText : 'Cancelar'
                            });
                            inputMin.bootstrapMaterialDatePicker('setDate', inputA.value.min);

                            inputMax.bootstrapMaterialDatePicker({ 
                                format : 'YYYY-MM-DD',
                                time : false,         
                                lang : 'es', 
                                weekStart : 1,
                                okText : 'Aceptar',  
                                cancelText : 'Cancelar'
                            });
                            inputMax.bootstrapMaterialDatePicker('setDate', inputA.value.max);
                        }

                        if(inputA.tipo == "time"){
                            inputMin.bootstrapMaterialDatePicker({
                                format: "HH:mm",
                                date : false,
                                lang : 'es',                                         
                                okText : 'Aceptar',  
                                cancelText : 'Cancelar'
                            });
                            inputMin.bootstrapMaterialDatePicker('setDate', inputA.value.min);

                            inputMax.bootstrapMaterialDatePicker({ 
                                format: "HH:mm",
                                date : false,         
                                lang : 'es',                                         
                                okText : 'Aceptar',  
                                cancelText : 'Cancelar'
                            });
                            inputMax.bootstrapMaterialDatePicker('setDate', inputA.value.max);
                        }

                        inputMin.change(function(){
                            let valueMin = $(this).val();
                            inputA.tipo == "date" || inputA.tipo == "time" && inputMax.bootstrapMaterialDatePicker('setMinDate', valueMin);
                            (inputA.tipo == "money" || inputA.tipo == "number" ) && inputMax.attr('min', valueMin);
                            inputMax.val("");
                        });

                        inputMax.change(function(){
                            let valueMax = $(this).val();
                            if(inputMin.val() != "" && valueMax != ""){
                                var aux = {};
                                aux["value"] = {min: (inputA.tipo == "money" || inputA.tipo == "number") ? inputMin.val().replace(",",".")-0 : inputMin.val(), max: (inputA.tipo == "money" || inputA.tipo == "number") ? valueMax.replace(",",".")-0 : valueMax };
                                aux["name"] = inputA.name;
                                aux["id_objeto"] = inputA.id_objeto;
                                obj.clearInputs();
                                obj.insertInput(aux);
                                obj.setIDRelacion(inputA.id_relacion-0);
                                console.log(obj.getInputs());
                            }
                        });
                        

                    break;
                    case "10":                        
                        let slide = $('<div>',{id:"slideNumber"});
                        var inputGroup = $("<div>",{ class : "input-group"});
                        var addOn = $("<span>",{ class:"input-group-addon" }).append($("<i>",{class:"material-icons", text: "cake"}));
                        let spanValores = $("<span>");
                        spanValores.text("Min: "+inputA.value.min+", Max:"+inputA.value.max);
                        
                        divDetails.append($("<p>").html("<b>"+inputA.label+":</b>"));
                        divDetails.append(inputGroup.append(addOn).append($("<div>").append(slide)).append($("<div>",{class:"m-t-20 font-12"}).append("<b>Valores:</b>").append(spanValores)));

                        noUiSlider.create(slide[0], {
                            start: [inputA.value.min, inputA.value.max],
                            step: 1,
                            connect: true,
                            range: {
                              'min': [1],
                              'max': [100]
                            },
                            format: {
                                to: function (value) {                  
                                    return value;
                                },
                                from: function (value) {                  
                                    return value;
                                }
                            }
                        });
                        slide[0].noUiSlider.on('update', function(values, handle){
                            spanValores.text("Min: "+values[0].toFixed(0)+", Max:"+values[1].toFixed(0));
                            var valInput = [values[0].toFixed(0)-0, values[1].toFixed(0)-0];
                            var aux = {};
                            aux["value"] = valInput;
                            aux["name"] = inputA.name;
                            aux["id_objeto"] = inputA.id_objeto;
                            
                            obj.clearInputs();
                            obj.insertInput(aux);
                            obj.setIDRelacion(inputA.id_relacion-0);
                            //console.log(obj.getInputs());
                        });
                    break;

                }
            });
            /**** FIN DE CONSTRUCCION ****/

            btnDeleteCategoria.on("click",function(){
                arrayCategoria.splice(obj.getIndice(), 1);
                console.log(obj.getIndice());
                $.each(arrayCategoria, function(key,value){
                    value.setIndice(key);
                });
                //containerSegmento.remove();
                if(insertRowOR == 1){
                    containerSegmento.remove();
                    divRowAND.remove();
                }else{
                    divContainer.remove();
                }
            });
            
        });
        let btnAdd = $("<a>",{href:"javascript:;", class : "btn btn_color btnORCondition"}).text("Agregar condición OR");    
        let boxAdd = $("<div>",{'class':'col-lg-12 col-md-12 col-sm-12 col-xs-12'});
        
        if(objeto.length > 0){
            btnAdd.click(function(){
                var cnt = arrayCategoria.length;
                var categoria = new Categoria(objeto[0].elementos[0], true, true, cnt);
                var aux = createBoxHTML([categoria],false, 2, 2);
                boxAdd.append(aux);
                //console.log(categoria);
                arrayCategoria.push(categoria);
                //console.log(arrayCategoria);
                //activeBtnDelete(btnDeleteCategoria, objeto);
            });
        }
        
        divContainer.append(divAND.append(boxAdd));
        flagBtnAdd && divAND.append(col1.append(btnAdd));

    	return divContainer;
    }


    this.insertCategoria = function(categoria, flagBtnAdd, typeRow, insertRowOR){
    	arrayCategoria.push(categoria);
        //this.tagHTML = createBoxHTML(arrayCategoria, false, false, true, 1)[0];
        this.tagHTML = createBoxHTML(arrayCategoria, flagBtnAdd, typeRow, insertRowOR)[0];
        //console.log(arrayCategoria);
        return true;
    };

    this.getCategorias = function(index){
    	return (index ? arrayCategoria[index] : arrayCategoria) ;
    };

    this.getHTML = function(){
    	return this.tagHTML;
    };

    this.getIndex = function(){
        return this.indice;
    };

    this.getCountCategorias = function(){
        //lengthCount = arrayCategoria.length;
        return arrayCategoria.length;
    };
    
    this.setIndex = function(newIndex){
        this.indice = newIndex;
        return true;
    };
}