$("#btnMasive").click(function(){
    $("#divMasivo").toggle(500);
});


function deleterw(id){
    var op = 6;

    $.ajax({
        data:{ op:op, id:id },
        url: '../../controller/configuracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                swal(" ", "Producto Eliminado", "success");
                menu('administracion/admin_rewards');
            }else{
                swal('Error!','El producto no pudo eliminarse','warning');
            }
            
        }
    });
}

function editrw(id){
    var name = $('#n'+id).val();
    var pts = $('#p'+id).val();
    var visits = $('#v'+id).val();
    var details = $('#d'+id).val();
    var cant = $('#c'+id).val();
    var op = 4;

    $.ajax({
        data:{ op:op, id:id, name:name, pts:pts, visits:visits, details:details, cant:cant },
        url: '../../controller/configuracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            swal(" ", "Producto Modificado", "success");
            menu('administracion/admin_rewards');
        }
    });
}

function setfile(id,file){
    //console.log(id+':'+file);
    var op = 5;
    $.ajax({
        data:{ op:op, id:id, file:file },
        url: '../../controller/configuracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            //swal(" ", "Producto Agregado", "success");
        }
    });
}

function cleanfile(id){
    $('#filename2').attr('my',id);
    $('#frmFileUpload2').removeClass('dz-started'); 
    $(".dz-preview").remove();
    
}

function activerwd(id){
    var op = 18;
    var table = 'tbl_cat_premios_productos_servicios';
    var field = 'id_cat_premios_productos_servicios';
    $.ajax({
        data:{ id:id, op:op, table:table, field:field },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                //swal(" ", "Usuario Eliminado", "success");
            }
            
        }
    });
}

function inactiverwd(id){
    var op = 19;
    var table = 'tbl_cat_premios_productos_servicios';
    var field = 'id_cat_premios_productos_servicios';
    $.ajax({
        data:{ id:id, op:op, table:table, field:field },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                //swal(" ", "Usuario Eliminado", "success");
            }
            
        }
    });
}

function delrow(id,table,field,res){
    var op = 8;

    $.ajax({
        data:{ op:op, id:id, table:table, field:field, res:res },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==5){
                //swal(" ", "Promoción Eliminada", "success");
                //menu('administracion/edit_promociones');
                menu('administracion/admin_rewards');
            }else{
                //swal('Error!','El Registro no pudo eliminarse','warning');
            }
            
        }
    });
}
