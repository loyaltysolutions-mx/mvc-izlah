function editpromoview(id,tipo){
    $.ajax({
        data:{ id:id },
        url: '../../view/administracion/edit_promociones.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            $('#card-content').html(response);
            /*if(response==1){
                swal(" ", "Registro Modificado Correctamente", "success");
                menu('administracion/edit_promociones');
            }else{
                swal('Error!','El Elemento no pudo Modificarse','warning');
            }*/
        }
    });
}

function editpromo(id){

    var type = $('#type').val();
    var promo = $('#promo').val();
    var descript = $('#detail').val();
    var smalltxt = $('#smalltxt').val();
    var conditions = $('#conditions').val();
    var howuse = $('#howuse').val();
    var infolink = $('#infolink').val();
    var bonustype = $('#bonustype').val();
    var rewardto = $('#rewardto').val();
    var rewardperc = $('#rewardperc').val();
    var npromos = $('#npromos').val();
    var nredention = $('#nredention').val();
    var start = $('#start').val();
    var ending = $('#ending').val();
    var pushtxt = $('#pushtxt').val();
    var smstxt = $('#smstxt').val();
    var whatstxt = $('#whatstxt').val();
    var cashback = $('#cashback').val();
    var cashbackperc = $('#cashbackperc').val();
    var descuento = $('#custom').val();
    var codigo = $('#codigo').val();
    //only for ckeditor
    var emailtxts = CKEDITOR.instances.emailtxt.getData();

    var sendtime = $('#sendtime').val();

    if($('#push').is(":checked")){
        var push = 1;
    }else{
        var push = 0;
    }

    if($('#sms').is(":checked")){
        var sms = 1;
    }else{
        var sms = 0;
    }

    if($('#whats').is(":checked")){
        var whats = 1;
    }else{
        var whats = 0;
    }

    if($('#email').is(":checked")){
        var email = 1;
    }else{
        var email = 0;
    }

    var suc = [];
    $('#suc :selected').each(function(i, selectedsuc) {
        suc[i] = $(selectedsuc).val();
    });

    var segment = [];
    $('#segment :selected').each(function(j, selected) {
        segment[j] = $(selected).val();
    });

    var lvl = [];
    $('#lvl :selected').each(function(k, selectedlvl) {
        lvl[k] = $(selectedlvl).val();
    });
   
    var op = 17;
    if(suc.length==0 && segment.length==0 && lvl.length==0){
        swal('Error!','Debes seleccionar al menos una Sucursal ó Segmento ó Nivel ','warning');
        throw new Error();
    }else{
        $.ajax({
            data:{ op:op, id:id, suc:suc, promo:promo, descript:descript, smalltxt:smalltxt, segment:segment, lvl:lvl,
            type:type, conditions:conditions, howuse:howuse, infolink:infolink, bonustype: bonustype, rewardto:rewardto, rewardperc:rewardperc,
            npromos:npromos, nredention:nredention, start:start, ending:ending, push:push, sms:sms, whats:whats,
            email:email, pushtxt:pushtxt, smstxt:smstxt, whatstxt:whatstxt, emailtxts:emailtxts, sendtime:sendtime,
            cashback:cashback, cashbackperc:cashbackperc, descuento:descuento, codigo:codigo },
            url: '../../controller/administracion/c_llamadas_ajax.php',
            method:'post',
            beforeSend: function(response){

            },
            success: function(response){
                if(response==1){
                    swal(" ", "Registro Modificado Correctamente", "success");
                    menu('administracion/admin_promociones');
                }else{
                    swal('Error!','El Elemento no pudo Modificarse','warning');
                }
            }
        });
    }    
}

/*function tog(id){
    ids ='mul'+id;
    $('#'+ids).toggle();
    if ($('#'+ids).is(':visible')) {
            $('#btn'+id).text('Ocultar');               
        } else {
            $('#btn'+id).text('Mostrar');              
        }   
}

function tog2(id){
    ids ='mul2'+id;
    $('#'+ids).toggle();
    if ($('#'+ids).is(':visible')) {
            $('#btn2'+id).text('Ocultar');               
        } else {
            $('#btn2'+id).text('Mostrar');              
        }   
}*/

function activepromo(id){
    var op = 18;
    var table = 'tbl_promociones';
    var field = 'id_promociones';
    $.ajax({
        data:{ id:id, op:op, table:table, field:field },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                //swal(" ", "Usuario Eliminado", "success");
            }
            
        }
    });
}

function inactivepromo(id){
    var op = 19;
    var table = 'tbl_promociones';
    var field = 'id_promociones';
    $.ajax({
        data:{ id:id, op:op, table:table, field:field },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                //swal(" ", "Usuario Eliminado", "success");
            }
            
        }
    });
}

function refreshing(id){
    var op = 21;

    $.ajax({
        data:{ op:op, id:id },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){
            //$('#ir'+id).addClass('fa-spin');
        },
        success: function(response){
            //$('#ir'+id).removeClass('fa-spin');
            $('#r'+id).html('');
            $('#r'+id).html(response);
        }
    });
}

function duplicate(id){
    var op = 20;

    $.ajax({
        data:{ op:op, id:id },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                swal(" ", "Promoción Duplicada", "success");
                menu('administracion/admin_promociones');
            }else{
                //swal('Error!','El Registro no pudo eliminarse','warning');
            }
            
        }
    });
}


function delrow(id,table,field,res){
    var op = 8;

    $.ajax({
        data:{ op:op, id:id, table:table, field:field, res:res },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==5){
                //swal(" ", "Promoción Eliminada", "success");
                //menu('administracion/edit_promociones');
            }else{
                //swal('Error!','El Registro no pudo eliminarse','warning');
            }
            
        }
    });
}

function delrow2(id,table,field,res){
    var op = 8;

    $.ajax({
        data:{ op:op, id:id, table:table, field:field, res:res },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==5){
                swal(" ", "Promoción Eliminada", "success");
                menu('administracion/admin_promociones');
            }else{
                swal('Error!','El Registro no pudo eliminarse','warning');
            }
            
        }
    });
}