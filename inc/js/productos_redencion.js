/*JS DE FORMULARIO DE REDENCION*/
$(document).ready(function() {
    $('.load_pantalla').hide();
 //GUARDA REDENCION 
 $('.redimir_puntos').click(function(){
     swal({
            title: 'Estas seguro de Redimir por este premio?',
            text: "",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'SI',
            cancelButtonText:'NO'
          }).then((result) => {
            if (result.value) {
                 $('.load_pantalla').show();
                var id_cte=$('#id_cte').val();
                var id_usu=$('#id_usu').val();
                var id_prod=$(this).val();
				var nom_usu_autoriza='';
				var pass_usu_autoriza='';
                $.ajax({
                      url: '../../controller/redencion/c_llamadas_ajax.php',
                      type: "POST",
                      data:{ 
                          op:2,
                          id_cte:id_cte,
                          id_prod:id_prod,
                          id_usu:id_usu,
						  nom_usu_autoriza:nom_usu_autoriza,
						  pass_usu_autoriza:pass_usu_autoriza
                       },
                      success: function(resp)
                          {  
                              $('.load_pantalla').hide();
                              if(resp==0){
                                  swal("", 'Ocurrio un Error al guardar el registro',"error"); 
                              }else{
                                //swal('Felicidades!', 'Se registro la redencion correctamente', 'success');
                                swal({
                                    type: 'success',
                                    title: 'Felicidades!',
                                    text:'Se registro la redencion correctamente',
                                    showConfirmButton: false,
                                    timer: 1500
                                  });
                                setTimeout(function() {
                                    location.reload();
                                }, 1600);
                              } 
                          }
            }); 
            }
          })

   });  
   
  //GUARDA REDENCION SIN SESSION
 $('.redimir_puntos_sin_sesion').click(function(){

     swal({
            title: 'Estas seguro de Redimir por este premio?',
            text: "",
            type: 'question',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'SI',
            cancelButtonText:'NO'
          }).then((result) => {
            if (result.value) {
                
                var id_cte=$('#id_cte').val();
                var id_usu='';
                var id_prod=$(this).val();
				var pass_usu_autoriza=$('#pass_usu_autoriza').val();
                if( pass_usu_autoriza==''){
					swal('', 'Ingresa los datos de autorización para poder Redimir el premio', 'error');
					
				}else{
					 $('.load_pantalla').show();
					$.ajax({
                      url: '../../controller/redencion/c_llamadas_ajax.php',
                      type: "POST",
                      data:{ 
                          op:2,
                          id_cte:id_cte,
                          id_prod:id_prod,
                          id_usu:id_usu,
						  pass_usu_autoriza:pass_usu_autoriza
                       },
                      success: function(resp)
                          {  
                              $('.load_pantalla').hide();
                              if(resp==0){
                                  swal("", 'Ocurrio un Error al guardar el registro',"error"); 
                              }else{
                                //swal('Felicidades!', 'Se registro la redencion correctamente', 'success');
                                swal({
                                    type: 'success',
                                    title: 'Felicidades!',
                                    text:'Se registro la redencion correctamente',
                                    showConfirmButton: false,
                                    timer: 1500
                                  });
                                setTimeout(function() {
                                    location.reload();
                                }, 1600);
                              } 
                          }
						}); 
				}
            }
          })

   });    
   
   
   
});