﻿//script by Allan M.


function preloader(){
    $('.page-loader-wrapper').empty();
}
function adduser(){
    var op = 2;
    var name = $('#name').val();
    var lastname1 = $('#lastname1').val();
    var lastname2 = $('#lastname2').val();
    var user = $('#user').val();
    var email = $('#email').val();
    var rol = $( "select[name='rol']" ).val();
    if(rol==2){
        var park = $( "select[name='park']" ).val();
    }else{
        var park = '';
    }

    var password = $('#password').val();

    $.ajax({
        data:{ op:op, name:name, lastname1:lastname1, lastname2:lastname2, user:user, email:email, rol:rol, park:park, password:password},
        url: '../../controller/login/c_login.php',
        method:'post',
        beforeSend: function(response){
        
        },
        success: function(response){
            if(response==1){
                swal(" ", "Usuario Agregado", "success");
                menus('usuario/newuser')
            }else if(response==0){
                swal('Error!','El usuario/email ya se encuentra registrado','warning');
            }else{
                swal('Error!','El usuario no pudo agregarse','warning');
            }
        }
    });
}

function viewpark(){
    var rol = $( "select[name='rol']" ).val();
    if(rol==2){
        $('#parks').show();
        $('#park').attr('required', true);
    }else{
        $('#parks').hide();
        $('#park').removeAttr('required');
    }
}

//funtion to change if is operator(park) or admin(no park)
function viewparks(id){
    var rol = $("#r"+id).val();
    if(rol==2){
        $('#p'+id).show();
        $('#p'+id).attr('required', true);
    }else{
        $('#p'+id).hide();
        $('#p'+id).removeAttr('required');
    }
}

function addcoupon(){
    var option = 4;
    var park = $('#park').val();
    var coupon = $('#coupon').val();
    var detail = $('#detail').val();
    var barcode = $('#barcode').val();
    var ncoupons = $('#ncoupons').val();
    var redention = $('#redention').val();
    var halfdays = $('#halfdays').val();
    var filename = $('#filename').val();

    $.ajax({
        data:{ option: option, park,park, coupon: coupon, filename: filename, detail: detail, barcode: barcode, 
            ncoupons: ncoupons, redention: redention, halfdays: halfdays },
        url: '../../controller/user/c_user.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                swal(" ", "Cupón Agregado", "success");
                menus('usuario/newcoupon');
            }else{
                swal('Error!','El cupón no pudo agregarse','warning');
            }
        }
    });
}

function addlvl(){
    var option = 5;
    var park = $('#park').val();
    var color = $('#color1').val();
    var filename = $('#filename').val();

    $.ajax({
        data:{ option: option, park: park, color: color, filename : filename },
        url: '../../controller/user/c_user.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                swal(" ", "Parque Agregado", "success");
                menus('usuario/newpark');
            }else{
                swal('Error!','El parque no pudo agregarse','warning');
            }
            
        }
    });
}

function addpromo(){
    var option = 8;
    var park = $('#park').val();
    var promo = $('#promo').val();
    var filename = $('#filename').val();

    $.ajax({
        data:{ option: option, park: park, promo: promo, filename : filename },
        url: '../../controller/user/c_user.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                swal(" ", "Promoción Agregada", "success");
                menus('usuario/newpromo');
            }else{
                swal('Error!','La promoción no pudo agregarse','warning');
            }
            
        }
    });
}


/*IMPORTANT, DON´T ERASE*/
// We use this code to select file to upload by diynamical ajax
/*function menus(id){
    var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
    $.ajax({
        data:{id:id},
        url: '../'+id+'.php',
        method:'post',
        beforeSend: function(response){
            $('#card-content').html(div);
        },
        success: function(response){
            $('#card-content').html(response);
            preloader();
        }
    });
}*/

function menus(id){
    $('.menus').hide();
    $('#'+id+'m').show();
}

//edit and delete

function delrow(id,table,field,res){
    var option = 2;

    $.ajax({
        data:{ option:option, id:id, table:table, field:field, res:res },
        url: '../../controller/user/c_user.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                swal(" ", "Usuario Eliminado", "success");
                menus('usuario/edituser');
            }else if(response==2){
                swal(" ", "Cupón Eliminado", "success");
                menus('usuario/editcoupon');
            }else if(response==3){
                swal(" ", "Parque Eliminado", "success");
                menus('usuario/editpark');
            }else if(response==4){
                swal(" ", "Promoción Eliminada", "success");
                menus('usuario/editpromo');
            }else{
                swal('Error!','El Registro no pudo eliminarse','warning');
            }
            
        }
    });
}

function edituser(id){
    var name = $('#n'+id).val();
    var last1 = $('#f'+id).val();
    var last2 = $('#l'+id).val();
    var user = $('#u'+id).val();
    var email = $('#e'+id).val();
    var rol = $( "select[name='r"+id+"']" ).val();
    var park = $('#p'+id).val();

    var option = 1;

    $.ajax({
        data:{ option:option, id:id, name:name, last1:last1, last2:last2, user:user, email:email, rol:rol, park: park },
        url: '../../controller/user/c_user.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            swal(" ", "Usuario Modificado", "success");
        }
    });
}

function editpass(id){
    $("#editpass").modal();
    $( "button[name='passbutton']" ).attr('id', id);
}

function editpassword(id){
    var password = $('#password').val();
    var option = 10;
    $.ajax({
        data:{ option:option, id:id, password:password },
        url: '../../controller/user/c_user.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                swal(" ", "Password Modificado", "success");
            }else{
                swal('Error!','El password no pudo cambiarse','warning');
            }
        }
    });
}

function editcoupon(id){
    var park = $('#p'+id).val();
    var title = $('#t'+id).val();
    var detail = $('#d'+id).val();
    var barcode = $('#b'+id).val();
    var enddate = $('#e'+id).val();
    var ncoupons = $('#n'+id).val();
    var redention = $('#r'+id).val();
    var halfdays = $('#h'+id).val();
    var option = 6;

    $.ajax({
        data:{ option:option, park:park, id:id, title:title, detail:detail, barcode:barcode, enddate:enddate, ncoupons:ncoupons,
        redention:redention, halfdays:halfdays },
        url: '../../controller/user/c_user.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            swal(" ", "Cupón Modificado", "success");
        }
    });
}

function editpark(id){
    var name = $('#n'+id).val();
    var color = $('#c'+id).val();
    var option = 7;

    $.ajax({
        data:{ option:option, id:id, name:name, color:color },
        url: '../../controller/user/c_user.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            swal(" ", "Parque Modificado", "success");
        }
    });
}

function editpromo(id){
    var promo = $('#n'+id).val();
    var park = $('#p'+id).val();
    var option = 9;

    $.ajax({
        data:{ option:option, id:id, park:park, promo:promo },
        url: '../../controller/user/c_user.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            swal(" ", "Promoción Modificada", "success");
        }
    });
}

function cleanfile(id){
    $('#filename').attr('my',id);
    $('#frmFileUpload').removeClass('dz-started'); 
    $(".dz-preview").remove();
    
}

//saves img database
function setfile(id,file,dbs){
    var option = 3;

    $.ajax({
        data:{ option:option, id:id, file:file, dbs:dbs },
        url: '../../controller/user/c_user.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            //swal(" ", "", "success");
        }
    });
}
//here ends

//function to change page ajax
function gotpage(page){
	$.ajax({
        data:{page:page},
        url: page+'.php',
        method:'post',
        beforeSend: function(response){
            $('#login-box').html('');
        },
        success: function(response){
            $('#login-box').html(response);
        }
    });
}


//function to change password
function changepass(id,key){
    
    var op = 4;
    var password = $('#password').val();

    $.ajax({
        data:{ id:id, key:key, op:op, password:password },
        url: '../../controller/login/c_login.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            window.location.href = "../../view/login/login.php";
        }
    });
}


//function to fix burguer menu maybe not need
/*function burguermenu(){
    $("#body").toggleClass("overlay-open");
    $("#body").toggleClass("overlay-open");
}*/

function draw(response){
    data = '['+response+']';
}