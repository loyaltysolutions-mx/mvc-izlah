var mediaStream = null;
        var video = document.createElement("video");
        var canvasElement = document.getElementById("canvas");
        var canvas = canvasElement.getContext("2d");
        var loadingMessage = document.getElementById("loadingMessage");            
        function drawLine(begin, end, color) {
          canvas.beginPath();
          canvas.moveTo(begin.x, begin.y);
          canvas.lineTo(end.x, end.y);
          canvas.lineWidth = 4;
          canvas.strokeStyle = color;
          canvas.stroke();
        }
        // Use facingMode: environment to attemt to get the back camera on phones, user to get the front camera

        navigator.mediaDevices.getUserMedia({ 
            video: { 
                facingMode: "user",                    
            } 
        }).then(function(stream) {            
            
            mediaStream = stream;
            mediaStream.stop = function () {
                this.getAudioTracks().forEach(function (track) {
                    track.stop();
                });
                this.getVideoTracks().forEach(function (track) { //in case... :)
                    track.stop();
                });
            };
            video.srcObject = stream;
            video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
            video.play();
            requestAnimationFrame(tick);
            
        });

        //alert("ready"):
        
        function tick() {            
            if (video.readyState === video.HAVE_ENOUGH_DATA) {
                loadingMessage.hidden = true;
                canvasElement.hidden = false;                    
                canvasElement.height = video.videoHeight;
                canvasElement.width = video.videoWidth;
                canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
                var imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
                var code = jsQR(imageData.data, imageData.width, imageData.height, {
                  inversionAttempts: "invert",
                });
                if (code) {                        
                    $.ajax({
                        data:{ idUsuario: code.data, logo : '' },
                        url: 'view_info.php',
                        method:'post',
                        beforeSend: function(response){
                            mediaStream.stop(); 
                        },
                        success: function(response){                                                               
                            $('#card-content').html(response);
                        }
                    });
                }
            }
            requestAnimationFrame(tick);
        }