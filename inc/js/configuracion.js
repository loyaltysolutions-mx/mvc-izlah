$(document).ready(function(){
	//DESHABILITAMOS CAMPOS NOMBRE Y EDAD
	$('#camposregistro_1').prop('disabled', true); 
    $('#camposregistro_2').prop('disabled', true); 
    //DESHABILITAMOS PUNTOS DE BIENVENIDA
     $('#div_ptos_bienvenida').hide();
     $('#div_ptos_cumple').hide();
    //OCULTAMOS DETALLE DE ACUMULACION
    $('#tipoproyecto_1_detalle').hide();
    $('#tipoproyecto_2_detalle').hide();
    //DESHABILITAMOS CAMPOS MAIL
    $('#tipoacumilacion_4').prop('disabled', true);
    $('#tipo_comunicacion_registro2').prop('disabled', true); 
    $('#tipocomunicacion_2').prop('disabled', true); 
    $('#tipocomunicacionredimir_2').prop('disabled', true); 
    $('#tipocomunicacionperiodica_2').prop('disabled', true); 
    $('#mecanicaredencion_3').prop('disabled', true); 
    
    //DESHABILITAMOS CAMPOS DE SMS
    $('#tipoacumilacion_1').prop('disabled', true);
    $('#tipo_comunicacion_registro1').prop('disabled', true); 
    $('#tipocomunicacion_1').prop('disabled', true); 
    $('#tipocomunicacionredimir_1').prop('disabled', true); 
    $('#tipocomunicacionperiodica_1').prop('disabled', true); 
    $('#mecanicaredencion_1').prop('disabled', true); 
    //DESHABILITA CAMPOS ID TARJETA
    $('#tipoacumilacion_2').prop('disabled', true);
    $('#tipoacumilacion_3').prop('disabled', true);
    $('#mecanicaredencion_2').prop('disabled', true);
    
    
    $('#loading').hide();
    $('#redencion_1').hide();
    $('#redencion_2').hide();
    $('#redencion_3').hide();
    $('#redencion_4').hide();
    $('#redencion_5').hide();
    var form_count = 1, previous_form, next_form, total_forms;
    total_forms = $("fieldset").length;
    $(".next-form").click(function(){
        previous_form = $(this).parent();
        next_form = $(this).parent().next();
        next_form.show();
        previous_form.hide();
        setProgressBarValue(++form_count);
    });
    $(".previous-form").click(function(){
        previous_form = $(this).parent();
        next_form = $(this).parent().prev();
        next_form.show();
        previous_form.hide();
        setProgressBarValue(--form_count);
        });
    setProgressBarValue(form_count);
    function setProgressBarValue(value){
    var percent = parseFloat(100 / total_forms) * value;
    percent = percent.toFixed();
    $(".progress-bar")
    .css("width",percent+"%")
    .html(percent+"%");
    }
 //FUNCIONALIDAD ACTIVA /DESACTIVA ESTILOS DEFAULT
$('input[name=estilos_default]').on('change', function() {
    if ($(this).is(':checked') ) {
        $("#conten_estilo").hide(400);

    } else {
        $("#conten_estilo").show(400);
       
    }
});

$("input[name='tipo_reden[]']").on('change', function() {
    if ($('#tiporedencion_2').is(':checked')) {
        $("#modalr").removeAttr("disabled");
        $("#editmodal").removeAttr("disabled");
    }else{
        $("#modalr").attr("disabled","true");
        $("#editmodal").attr("disabled","true");
    }

});

$("#tipoproyecto_1").click(function()
{ 
    limpia_checked();
    
   $('#redencion_1').show();
   $('#redencion_2').show();
   $('#redencion_3').show();
   $('#redencion_4').hide();
   $('#redencion_5').hide();
   $("#tipoproyecto_1_detalle").show();
   $("#tipoproyecto_2_detalle").hide();
   $('#visitas_premio').val('');
   $('#puntos_por_visita').val('');
   $("#div_ptos_bienvenida").show();
   $("#div_ptos_cumple").show();
   $("#modalr").attr("data-target", "#premios1");
   
   
});

$("#tipoproyecto_2").click(function()
{ 
    limpia_checked();
  $('#redencion_1').hide();
  $('#redencion_2').show();
  $('#redencion_3').show();
  $('#redencion_4').hide();
  $('#redencion_5').hide();
  $("#tipoproyecto_1_detalle").hide();
  $("#tipoproyecto_2_detalle").show();
  $('#por_cant_vendida').val('');
  $('#num_puntos_peso').val('');
  $("#div_ptos_bienvenida").show();
  $("#div_ptos_cumple").show();
  $("#modalr").attr("data-target", "#premios2");
  
});

$("#tipoproyecto_3").click(function()
{ 
    limpia_checked();
  $('#redencion_1').hide();
  $('#redencion_2').hide();
  $('#redencion_3').hide();
  $('#redencion_4').show();
  $('#redencion_5').hide();
  $("#tipoproyecto_1_detalle").hide();
  $("#tipoproyecto_2_detalle").hide();
  $('#visitas_premio').val('');
  $('#puntos_por_visita').val('');
  $('#por_cant_vendida').val('');
  $('#num_puntos_peso').val('');
  $("#div_ptos_bienvenida").show();
  $("#div_ptos_cumple").show();
});

$("#tipoproyecto_4").click(function()
{ 
    limpia_checked();
  $('#redencion_1').hide();
  $('#redencion_2').hide();
  $('#redencion_3').hide();
  $('#redencion_4').hide();
  $('#redencion_5').show();
  $("#tipoproyecto_1_detalle").hide();
  $("#tipoproyecto_2_detalle").hide();
  $('#visitas_premio').val('');
  $('#puntos_por_visita').val('');
  $('#por_cant_vendida').val('');
  $('#num_puntos_peso').val('');
  $("#div_ptos_bienvenida").hide();
  $("#div_ptos_cumple").hide();
});

function limpia_checked(){
    $('#tiporedencion_1').prop('checked', false);
    $('#tiporedencion_2').prop('checked', false);
    $('#tiporedencion_3').prop('checked', false);
    $('#tiporedencion_4').prop('checked', false);
    $('#tiporedencion_5').prop('checked', false);
}

//FUNCION SUBE IMAGEN LOGO
$("input[name='file']").on("change", function(){
            //$('#loading').show();
            $("#respuesta_logo").html('');
            //$("#nombre_imagen").html('');
            var formData = new FormData($("#formulario")[0]);
            //SE AGREGA VALOR AL FORMDATA
            formData.append("op", 2);
            formData.append("ruta", "../../inc/imagenes/img_configuracion/");
            var ruta = "../../controller/configuracion/c_llamadas_ajax.php";
            $.ajax({
                url: ruta,
                type: "POST",
                data:
                       formData,
                contentType: false,
                processData: false,
                success: function(datos)
                {
                    $("#respuesta_logo").html(datos);    
                    //$('#loading').hide();
                }
            });
        });
//FUNCION SUBE IMAGEN FONDO
$("input[name='file2']").on("change", function(){
            $("#respuesta_imagen_fondo").html('');
            //$("#nombre_imagen").html('');
            var formData = new FormData($("#formulario2")[0]);
            //SE AGREGA VALOR AL FORMDATA
            formData.append("op", 2);
            formData.append("ruta", "../../inc/imagenes/img_configuracion/");
            var ruta = "../../controller/configuracion/c_llamadas_ajax.php";
            $.ajax({
                url: ruta,
                type: "POST",
                data:
                       formData,
                contentType: false,
                processData: false,
                success: function(datos)
                {
                    $("#respuesta_imagen_fondo").html(datos);    
                    //$('#loading').hide();
                }
            });
        });        
        
//FUNCIONALIDAD  MUESTRA PUNTOS DE BIENVENIDA
$("[name='bono']").change(function () {  
    var res=$(this).val();
    if(res==1 ){
         $('#ptos_bienvenida').prop('disabled', false);
    }else{
         $('#ptos_bienvenida').prop('disabled', true);
         $('#ptos_bienvenida').val('');
    }
});
//FUNCIONALIDAD  MUESTRA PUNTOS DE CUMPLEAÑOS
$("[name='cumple']").change(function () {  
    var res=$(this).val();
    if(res==1){
         $('#ptos_cumple').prop('disabled', false);
    }else{
         $('#ptos_cumple').prop('disabled', true);
         $('#ptos_cumple').val('');
    }
});



 //FUNCIONALIDAD ACTIVA POSICION
$("[name='campos_registro[]']").change(function () {
        var id='';
         if (this.checked) {
               id=$(this).val();
             $('#pos_'+id).prop('disabled', false);  
             $('#campoclave_'+id).prop('disabled', false); 
			 //HABILITAMOS  CAMPOS NOMBRE Y EDAD
             if(id==6){
				$('#camposregistro_1').prop('disabled', false); 
			    $('#camposregistro_2').prop('disabled', false); 
             }
             //HABILITAMOS TODOS LOS CAMPOS DE EMAIL
             if(id==5){
				$('#camposregistro_1').prop('disabled', false); 
			    $('#camposregistro_2').prop('disabled', false); 
                $('#tipoacumilacion_4').prop('disabled', false); 
                $('#tipo_comunicacion_registro2').prop('disabled', false); 
                $('#tipocomunicacion_2').prop('disabled', false); 
                $('#tipocomunicacionredimir_2').prop('disabled', false); 
                $('#tipocomunicacionperiodica_2').prop('disabled', false); 
                $('#mecanicaredencion_3').prop('disabled', false); 
             }
             //HABILITAMOS TODOS LOS CAMPOS DE SMS
             if(id==3){
				$('#camposregistro_1').prop('disabled', false); 
			    $('#camposregistro_2').prop('disabled', false); 
                $('#tipoacumilacion_1').prop('disabled', false); 
                $('#tipo_comunicacion_registro1').prop('disabled', false); 
                $('#tipocomunicacion_1').prop('disabled', false); 
                $('#tipocomunicacionredimir_1').prop('disabled', false); 
                $('#tipocomunicacionperiodica_1').prop('disabled', false); 
                $('#mecanicaredencion_1').prop('disabled', false); 
             }
             //HABILITAMOS TODOS LOS CAMPOS PARA ID TARJETA
              if(id==7){
				$('#camposregistro_1').prop('disabled', false); 
			    $('#camposregistro_2').prop('disabled', false); 
                $('#tipoacumilacion_2').prop('disabled', false); 
                $('#tipoacumilacion_3').prop('disabled', false); 
                $('#mecanicaredencion_2').prop('disabled', false); 
                
              }
             
            } else {
                id=$(this).val();
                 //DESHABILITAMOS Y DESCHECKEAMOS TODOS LOS CAMPOS DE EMAIL
                if(id==5){
                     $('#tipoacumilacion_4').prop('disabled', true); 
                     $('#tipoacumilacion_4').prop( "checked", false );
                     $('#tipo_comunicacion_registro2').prop('disabled', true); 
                     $('#tipo_comunicacion_registro2').prop( "checked", false );
                     $('#tipocomunicacion_2').prop('disabled', true); 
                     $('#tipocomunicacion_2').prop( "checked", false );
                     $('#tipocomunicacionredimir_2').prop('disabled', true); 
                     $('#tipocomunicacionredimir_2').prop( "checked", false );
                     $('#tipocomunicacionperiodica_2').prop('disabled', true); 
                     $('#tipocomunicacionperiodica_2').prop( "checked", false );
                     $('#mecanicaredencion_3').prop('disabled', true); 
                     $('#mecanicaredencion_3').prop( "checked", false ); 
                }
                 //DESHABILITAMOS Y DESCHECKEAMOS TODOS LOS CAMPOS DE EMAIL
                if(id==3){
                     $('#tipoacumilacion_1').prop('disabled', true); 
                     $('#tipoacumilacion_1').prop( "checked", false );
                     $('#tipo_comunicacion_registro1').prop('disabled', true); 
                     $('#tipo_comunicacion_registro1').prop( "checked", false );
                     $('#tipocomunicacion_1').prop('disabled', true); 
                     $('#tipocomunicacion_1').prop( "checked", false );
                     $('#tipocomunicacionredimir_1').prop('disabled', true); 
                     $('#tipocomunicacionredimir_1').prop( "checked", false );
                     $('#tipocomunicacionperiodica_1').prop('disabled', true); 
                     $('#tipocomunicacionperiodica_1').prop( "checked", false );
                     $('#mecanicaredencion_1').prop('disabled', true);
                     $('#mecanicaredencion_1').prop( "checked", false ); 
                }
                 //DESHABILITAMOS Y DESCHECKEAMOS TODOS LOS CAMPOS DE EMAIL
                if(id==7){
                     $('#tipoacumilacion_2').prop('disabled', true); 
                     $('#tipoacumilacion_2').prop( "checked", false );
                     $('#tipoacumilacion_3').prop('disabled', true); 
                     $('#tipoacumilacion_3').prop( "checked", false );
                     $('#mecanicaredencion_2').prop('disabled', true); 
                     $('#mecanicaredencion_2').prop( "checked", false );
                }
                
                
                $('#pos_'+id).prop('disabled', true);  
                $('#pos_'+id).val('');
                $('#campoclave_'+id).prop('disabled', true); 
                $('#campoclave_'+id).prop( "checked", false );
                
            }
            });
          
 //FUNCION GUARDA CONFIGURACION DEL PROYECTO        
$(".guarda_config").click(function(){ 
    $('#loading').show();
    $('.guarda_config').prop('disabled',false);
    var msg='';
    //ETAPA1
    /*if ($('#estilos_default').prop('checked') ) {
        var img_logo=$('#nombre_logo').val('');
        var img_fondo=$('#nombre_fondo').val('');
        var color_primario=$('#color_prim').val('');
        var color_secundario=$('#color_sec').val('');
    }else{
         img_logo=$('#nombre_logo').val();
         img_fondo=$('#nombre_fondo').val();
         color_primario=$('#color_prim').val();
         color_secundario=$('#color_sec').val();
    }*/
  
    if ($('#estilos_default').prop('checked')) {
        var img_logo= " ";
        var img_fondo=" ";
        var color_primario = " ";
        var color_secundario= " ";
    }else{
        img_logo=$('#nombre_logo').val();
         img_fondo=$('#nombre_fondo').val();
         color_primario=$('#color_prim').val();
         color_secundario=$('#color_sec').val();
    }
    //ETAPA1
    var autoregistro=$(".tipo_registro:checked" ).val();
    if(autoregistro==undefined){
       msg+='-La forma de REGISTRO es obligatoria <br>';
    }
    //ETAPA1-1
    var campos_registro='';
    var pos_registro='';
    var reg=new Object();
    var check=0;
	var arr_clave=[];
    $("[name='campos_registro[]']:checked").each(function (){
         campos_registro=$(this).val().replace('-','');
          if ($('#campoclave_'+campos_registro).is(':checked') ) {
                check=1;
				arr_clave.push(check);
            }else{
               check=0;
			   arr_clave.push(check);
			   
            }
         pos_registro=$('#pos_'+(campos_registro.replace('-', ''))).val();
         reg[campos_registro]=pos_registro+'|'+check;
		 
		 if(pos_registro==''){
			msg+='-La POSICION del campo esta en blanco <br>';
		 }

            
			});
      var s=arr_clave.indexOf(1);
		if(s==-1){
		msg+='-Selecciona el CAMPO CLAVE <br>'; 
		 }  

    //ETAPA2-1
    var forma_acumulacion='';
    $("[name='form_acum[]']:checked").each(function () {
         forma_acumulacion=forma_acumulacion+$(this).val()+'-';
            });
    if(forma_acumulacion==''){
       msg+='-La FORMA de Acumulación es obligatoria <br>'
    }
    //ETAPA2-2
    var tipo_acumulacion=$('input:radio[name=tipo_acumulacion]:checked').val()
    if(tipo_acumulacion==undefined){
       msg+='-El TIPO de Acumulación es obligatorio <br>'
    }
    //ETAPA2-3
    var por_margen=$('#por_cant_vendida').val();
    var puntos_por_peso=$('#num_puntos_peso').val();
    var num_min_visitas=$('#visitas_premio').val();
    var puntos_por_visita=$('#puntos_por_visita').val();
    //ETAPA3-1
    var tipo_redencion='';
    $("[name='tipo_reden[]']:checked").each(function () {
         tipo_redencion=tipo_redencion+$(this).val()+'-';
    });

    if(tipo_redencion==''){
       msg+='-El TIPO de Redención es obligatoria <br>'
    }   
    //ETAPA3-2
    var mecanica_redencion='';
    $("[name='mecanica_reden[]']:checked").each(function () {
         mecanica_redencion=mecanica_redencion+$(this).val()+'-';
            });
    if(mecanica_redencion==''){
       msg+='-La MECANICA de Redención es obligatoria <br>'
    }       
    //ETAPA4-1
    var bono_bienvenida=$(".bono_bienvenida:checked" ).val();
    var ptos_bienvenida=$("#ptos_bienvenida" ).val();
    if(bono_bienvenida==undefined){
       msg+='-El BONO de bienvenida es obligatorio <br>'
    }
    //ETAPA4-2
    var bono_cumpleanios=$(".bono_cumpleanios:checked" ).val();
      var ptos_cumple=$("#ptos_cumple" ).val();
    if(bono_cumpleanios==undefined){
       msg+='-El BONO de cumpleaños es obligatorio <br>'
    }
     //ETAPA4-3
    var tipo_comunicacion_registro='';
    $("[name='tipo_comunicacion_registro[]']:checked").each(function () {
         tipo_comunicacion_registro=tipo_comunicacion_registro+$(this).val()+'-';
            });
    if(tipo_comunicacion_registro==''){
       msg+='-El tipo de Comunucación al REGISTRARSE es obligatorio <br>'
    } 
    //ETAPA4-4
    var tipo_comunicacion_acumular='';
    $("[name='tipo_comunicacion_acumular[]']:checked").each(function () {
         tipo_comunicacion_acumular=tipo_comunicacion_acumular+$(this).val()+'-';
            });
    if(tipo_comunicacion_acumular==''){
       msg+='-El tipo de Comunucación al ACUMULAR es obligatorio <br>'
    }  
    //ETAPA4-5
    var tipo_comunicacion_redimir='';
    $("[name='tipo_comunicacion_redimir[]']:checked").each(function () {
         tipo_comunicacion_redimir=tipo_comunicacion_redimir+$(this).val()+'-';
            });
    if(tipo_comunicacion_redimir==''){
       msg+='-El tipo de Comunucación al REDIMIR es obligatorio <br>'
    }  
    //ETAPA4-6
    var tipo_comun_periodica='';
    $("[name='tipo_comun_periodica[]']:checked").each(function () {
        tipo_comun_periodica=tipo_comun_periodica+$(this).val()+'-';
            });
    if(tipo_comun_periodica==''){
       msg+='-El tipo de Comunucación PERIODICA es obligatoria <br>'
    }  ;  
    //ETAPA4-7     
    var lapso_comun=$(".lapso_comun:checked" ).val();     
    if(lapso_comun==undefined){
       msg+='-El lapso de Comunicación es obligatoria <br>'
    }

if(msg!=''){
     $('.guarda_config').prop('disabled',false);
     $('#loading').hide();
    swal(
        ' ',
        msg,
        'error'
        );
}else{
    //FUNCION GUARDA CONFIGURACION
    $.ajax({ 
        url: '../../controller/configuracion/c_llamadas_ajax.php',
        type: "POST",
        data:{ 
            op:1,
            img_logo:img_logo,img_fondo:img_fondo,color_primario:color_primario,color_secundario:color_secundario,
            autoregistro:autoregistro,campos_registro:campos_registro,reg:reg,
            forma_acumulacion:forma_acumulacion,tipo_acumulacion:tipo_acumulacion,por_margen:por_margen,puntos_por_peso:puntos_por_peso,num_min_visitas:num_min_visitas,puntos_por_visita:puntos_por_visita,
            tipo_redencion:tipo_redencion,mecanica_redencion:mecanica_redencion,
            bono_bienvenida:bono_bienvenida,ptos_bienvenida:ptos_bienvenida,bono_cumpleanios:bono_cumpleanios,ptos_cumple:ptos_cumple,tipo_comunicacion_registro:tipo_comunicacion_registro,tipo_comunicacion_acumular:tipo_comunicacion_acumular,tipo_comunicacion_redimir:tipo_comunicacion_redimir,tipo_comun_periodica:tipo_comun_periodica,lapso_comun:lapso_comun
            },
        success: function(datos)
            {
                
                $('#loading').hide();
                $('.guarda_config').prop('disabled',false);
                if(datos=='0'){
                    swal(
                        'Felicidades!',
                        '¡Se configuro correctamente la Plataforma!',
                        'success'
                      );
                }else if(datos=='1'){
                    swal(
                        'Felicidades!',
                        '¡Se actualizo correctamente la Configuración!',
                        'success'
                      );
                }else{
                     $('.guarda_config').prop('disabled',false);
                    swal(
                        'Error!',
                        'La plataforma ya habia sido configurada',
                        'warning'
                      );
                }
               
            }
        });
    }


});

    sendata();
    icons();
    function sendata(){
      var data = $('#data').val();
      if(data.length>10){
        selectedinfo(data);
      }else{

      }
      
    }

});
