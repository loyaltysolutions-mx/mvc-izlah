function editcate(id){
    var name = $('#n'+id).val();
    var descript = $('#d'+id).val();
    var op = 4;

    if(name==''){
        swal('Error!','Debes llenar el campo del Nombre de Catgoría','warning');
        throw new Error();
    }else if(descript==''){
        swal('Error!','Debes llenar el campo de Descripción','warning');
        throw new Error();
    }

    $.ajax({
        data:{ op:op, name:name, id:id, descript:descript },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                swal(" ", "Registro Modificado Correctamente", "success");
                menu('administracion/admin_categorias');
            }else{
                swal('Error!','El Elemento no pudo Modificarse','warning');
            }
        }
    });
}


function setfile(id,file,table){
    var op = 7;

    $.ajax({
        data:{ op:op, id:id, file:file, table:table },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            //swal(" ", "", "success");
        }
    });
}


function cleanfile(id){
    $('#filename').attr('my',id);
    $('#frmFileUpload').removeClass('dz-started'); 
    $(".dz-preview").remove();
    
}

function activecat(id){
    var op = 18;
    var table = 'tbl_categoria';
    var field = 'id_cat';
    $.ajax({
        data:{ id:id, op:op, table:table, field:field },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                //swal(" ", "Usuario Eliminado", "success");
            }
            
        }
    });
}

function inactivecat(id){
    var op = 19;
    var table = 'tbl_categoria';
    var field = 'id_cat';
    $.ajax({
        data:{ id:id, op:op, table:table, field:field },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                //swal(" ", "Usuario Eliminado", "success");
            }
            
        }
    });
}

function delrow(id,table,field,res){
    var op = 8;

    $.ajax({
        data:{ op:op, id:id, table:table, field:field, res:res },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                //swal(" ", "Usuario Eliminado", "success");
                //menus('usuario/edituser');
            }else if(response==2){
                swal(" ", "Registro Eliminado Correctamente", "success");
                menu('administracion/edit_menu');
            }else if(response==3){
                swal(" ", "Categoría Eliminada Correctamente", "success");
                menu('administracion/admin_categorias');
            }else if(response==4){
                //swal(" ", "Promoción Eliminada", "success");
                //menus('usuario/editpromo');
            }else{
                swal('Error!','El Registro no pudo eliminarse','warning');
            }
            
        }
    });
}