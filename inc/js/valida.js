/*JS DE FORMULARIO DE ACTIVACION DE LA CUENTA*/
//function setuser() {
$(document).ready(function(){
  //GUARDA DATOS DE REGISTRO   
  $('#form_confirma_registro').on("submit", function(e){    
    e.preventDefault();    
    $.ajax({
      url: '../../controller/valida/c_llamadas_ajax.php',
      type: "POST",
      data: $("#form_confirma_registro").serialize()+"&op=2",
      dataType: "json",
      beforeSend: function(){
  
      },
      success: function(resp){  
        $('#loading').hide();
        //console.log(resp);
        if(resp == 1){            
            Swal({
              type: 'success',
              title: 'Felicidades',
              text: 'Ahora puedes entrar a tu cuenta para ver tu información',
              footer: '<a href="../../">Ir a mi cuenta</a>'
            },function(){
                window.close();
            });
        }else{
          Swal("", "Ocurrio un error al guardar tu información", "error");
        }
      }
    }).fail(function(data){
      console.log(data);
    });
  });

});