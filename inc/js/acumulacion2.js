/*JS DE FORMULARIO DE ACUMULACION*/
function addacumula(){
    var monto=$('#monto_ticket').val();
    if (!/^-?[0-9]+([,\.][0-9]*)?$/.test(monto)){
       swal("", "El monto no es un número correcto", "error");
       exit;
    }
    var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
    $('#loading').html(div);
    var cadena = $('#form_valida_usu').serialize();
    var id_usu=$('#id_usuario').val();
    var nom_usu_autoriza='';
    var pass_usu_autoriza='';
    
    $.ajax({
        url: '../../controller/acumulacion/c_llamadas_ajax.php',
        type: "POST",
        data:{ 
        op:1,
        cadena:cadena,
        id_usu:id_usu,
  			nom_usu_autoriza:nom_usu_autoriza,
  			pass_usu_autoriza:pass_usu_autoriza
            
        },
        success: function(resp){ 
          $('#loading').html('');
          if(resp==0){
             swal("", "Todos los campos estan en blanco", "error");
          }else if(resp==1){
             swal("", "No se encontro Usuario con los datos ingresados", "error");
          }else if(resp==2){
             swal("", "El número y monto de ticket es obligatorio", "error");
          }else{
            //console.log(cadena); 
            console.log(resp); 
             swal("", "Se agrego el registro correctamente", "success");
          }
          menu('acumulacion/acumulacion2');   
          $( "input[name='celular']" ).val( "" );
          $( "input[name='id']" ).val( "" );
          $( "input[name='mail']" ).val( "" );
          $( "input[name='num_ticket']" ).val( "" );
          $( "input[name='monto_ticket']" ).val( "" );

        }//end success
    }); // end ajax  
}//end function 
