function addlvl(){
    var level = $('#name').val();
    var type = $('#type').val();
    var ini = $('#de').val();
    var end = $('#hasta').val();
    var op = 11;

    ini = parseInt(ini);
    end = parseInt(end);
    if(ini>=end){
        swal('Error!','El Campo Hasta, debe ser mayor al campo De','warning');
    }else{
        $.ajax({
            data:{ op: op, level: level, type: type, ini: ini, end: end },
            url: '../../controller/administracion/c_llamadas_ajax.php',
            method:'post',
            beforeSend: function(response){

            },
            success: function(response){
                if(response==1){
                    swal(" ", "Elemento Agregado a Niveles", "success");
                    menu('administracion/admin_niveles');
                }else{
                    swal('Error!','El Elemento no pudo agregarse','warning');
                }
                
            }
        });//end Ajax
    }

}