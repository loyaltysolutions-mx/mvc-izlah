/*JS REPORTE CLIENTES*/

function initFiltros(){    
  $("#divTable").hide();  
  $("#selectStatus").selectpicker();  
    
  $('#desde').bootstrapMaterialDatePicker({ 
    format : 'YYYY-MM-DD',
    time : false,         
    lang : 'es', 
    weekStart : 1,
    okText : 'Aceptar',  
    cancelText : 'Cancelar'
  });

  $('#hasta').bootstrapMaterialDatePicker({ 
    format : 'YYYY-MM-DD',
    time : false,         
    lang : 'es', 
    weekStart : 1,
    okText : 'Aceptar',  
    cancelText : 'Cancelar'
  });

  $('#desde').change(function(){ 
    let value = $(this).val();                
    $('#hasta').bootstrapMaterialDatePicker('setMinDate', value);
    $('#hasta').val("");        
  });
}

$("#btnConsultar").click(function(){

  //console.log($('.filter:checkbox:checked'));
  let checkStatus = $("#inputStatus").prop("checked");  
  let checkFechaCreacion = $("#inputFechaCreacion").prop("checked");  
  let dataForm = {'action' : 10};

  if(checkStatus){
    dataForm['status'] = $("#selectStatus").val();      
  }
  
  if(checkFechaCreacion){
    dataForm['fechaCreacion'] = {'desde': $("#desde").val(),'hasta': $("#hasta").val()};      
  }  

  let link = '../../controller/reportes/c_ajax_reporte.php';
  let data = dataForm;
  let beforeFunction = function(){
    $(".divTable").fadeOut();
  }
  let successFunction = function(data){
    if(data.result){
      var arrayResult = [];
      if(data.result.length > 0){                  
        $.each(data.result, function(key,value){
          let auxArray = [value.id_segmento, value.nombre_segmento, value.descripcion_segmento, value.countClients[0].countClients, value.activo == 0 ? "Activo" : "Inactivo", value.fecha_creacion ];          
          arrayResult.push(auxArray);
        });  
      }
      if(arrayResult.length > 0)
        $("#btnCreateSegment").show();
      else
        $("#btnCreateSegment").hide();
      
      if ( $.fn.dataTable.isDataTable( '#tableResult' ) ) {
        let table = $('#tableResult').DataTable();
        table.destroy();
        $("#tableResult").empty();
      }

      let titleFile = 'ReporteSegmento_'+new Date().getDay()+new Date().getMonth()+new Date().getFullYear();
      $('#tableResult').DataTable({
        data: arrayResult,
        columns: [{title:"ID Segmento"},{title:"Nombre"},{title:"Descripción"},{title:"Clientes Inscritos"},{title:"Status"},{title:"Fecha Creación"}],
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
          'copy',
          { extend: 'csvHtml5', title: titleFile },
          { extend: 'excelHtml5', title: titleFile },
          { extend: 'pdfHtml5', title: titleFile },
          'print'
        ]
      });

      $('#tableResult').on( 'page.dt', function () {
        var table = $("#tableResult").DataTable();
        var info = table.page.info();
        $('#pageInfo').html( 'Mostrando: '+info.page+' of '+info.pages );
      });
      $("#divTable").fadeIn();
      $(".divTable").fadeIn();
    }      
  };

  let failFunction = function(data){      
    $(".divTable").fadeIn();
  };

  if(checkFechaCreacion){
    if($("#desde").val() != "" && $("#hasta").val() != ""){
      $("#textDesde").text("");
      $("#textHasta").text("");
      connectServer(link, data, beforeFunction, successFunction, failFunction);
    }else{
      if($("#desde").val() == ""){
        $("#textDesde").text("Por favor elige una fecha");
      }
      if($("#hasta").val() == ""){
        $("#textHasta").text("Por favor elige una fecha");
      }
    }
  }else{
      $("#textDesde").text("");
      $("#textHasta").text("");
    connectServer(link, data, beforeFunction, successFunction, failFunction);
  }
});

$(".divFilter").hide();
let filter = $(".filter");
filter.click(function(){
  let parent = $(this).parent().parent().parent();
  if($(this).prop("checked")){      
    parent.find(".divFilter").fadeIn();
  }else{      
    parent.find(".divFilter").fadeOut();
    parent.find('input, textarea').val('');
    parent.find("select").val('default').selectpicker("refresh");
  }
});

function connectServer(link, data, beforeFunction, successFunction, failFunction){
  $.ajax({
    type: "POST",
    timeout: 20000,
    url: link,
    data: data,
    dataType: "json",
    beforeSend : function(){
        beforeFunction && beforeFunction();
    }
  }).done(function(data){
      console.log(data);
      successFunction && successFunction(data);        
  }).fail(function(data){
      failFunction && failFunction(data);
      console.log(data);
  });
}

initFiltros();
