/*JS DE FORMULARIO DE REDENCION*/
$(document).ready(function() {
$('#loading').hide();
swal({
  position: 'top-end',
  type: 'info',
  text: 'Recuerda que es necesario que el cliente muestre una Identificacion Personal',
  showConfirmButton: false,
  timer: 3000
})
 //GUARDA DATOS DE REGISTRO   
 $('#guarda_redencion').click(function(){
   $('#loading').show();
   var cadena = $('#form_redencion').serialize();
  $.ajax({
        url: '../../controller/redencion/c_llamadas_ajax.php',
        type: "POST",
        data:{ 
            op:1,
            cadena:cadena
         },
        success: function(resp)
            {  
                $('#loading').hide();
                if(resp==1){
                    swal("", 'Todos los campos estan en blanco',"error"); 
                }
                if(resp!=1 && resp!=3){
                    var id=resp;
					$('#guarda_redencion').hide();
                    $(location).attr('href', '../../view/redencion/productos_redencion.php?id_usu='+id);
                }
                if(resp==3){
                    swal("", 'No se encontro Registro con la información proporcionada',"error"); 
                }
            }
            });     
   });  
});