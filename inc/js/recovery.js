/*JS DE FORMULARIO DE ACTIVACION DE LA CUENTA*/
$(document).ready(function() {
  let password = $("#pass");
  let retype_pass = $("#retype_pass");

  password.keyup(function(){
    let val_pass = $(this).val();
    let val_retype = retype_pass.val();

    if(val_pass == val_retype){
      if(val_retype == ""){
        $("#textPass").text("Ingresa una contraseña");
        $("#textRetype").text("Ingresa una contraseña");
      }else{
        $("#textRetype").text("");
        $("#textPass").text("");
      }
    }else{
      $("#textPass").text("Las contraseñas no coinciden");
      $("#textRetype").text("Las contraseñas no coinciden");
    }
  });

  retype_pass.keyup(function(){
    let val_pass = password.val();
    let val_retype = $(this).val();

    if(val_pass == val_retype){
      if(val_retype == ""){
        $("#textPass").text("Ingresa una contraseña");
        $("#textRetype").text("Ingresa una contraseña");
      }else{
        $("#textRetype").text("");
        $("#textPass").text("");
      }
    }else{
      $("#textPass").text("Las contraseñas no coinciden");
      $("#textRetype").text("Las contraseñas no coinciden");
    }
  });

  $('#loading').hide();
   //GUARDA DATOS DE REGISTRO   
  $('#confirma_contrasena').click(function(){
    
    var id_usuario=$('#id_usu').val();
    var pass=$('#pass').val();
    var pass_confirm=$('#retype_pass').val();
    if(pass == pass_confirm && pass != ""){
      $.ajax({
        url: '../../controller/recupera/c_recupera.php',
        type: "POST",
        data:{ 
          id_usuario:id_usuario,
          pass:pass
        },
        beforeSend: function(){
          $('#loading').show();
        },
        success: function(resp){  
          $('#loading').hide();
          if(resp!=0){
               swal("Éxito", "Tu contraseña ha sido restablecida", "success");
                
              swal({
                type: 'success',
                title: 'Éxito',
                text: 'Ahora puedes entrar a tu cuenta para ver tu información con la nueva contraseña.',
                footer: '<a href="../../">Ir a mi cuenta</a>'
              });
          }else{
               swal("", "Ocurrio un error al actualizar la contraseña", "error");
          }
        }
      });
    }
      
  });
});