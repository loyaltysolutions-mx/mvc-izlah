
var modal1 = $("#modalPush");
var modal2 = $("#modalPromocion");
let formDelete = $("#formDelete");
var selectUser = $("#selectUser");
var monto = $("#monto");
var ticket = $("#ticket");
var monto = $("#monto");
var puntos = $("#puntos");
var visitas = $("#visitas");
var tipo = $("#tipo");

function init(){
    selectUser.selectpicker();    
}


$('[data-toggle="tooltip"]').tooltip({
    container: 'body',
    boundary: 'window'
});

$("#formPromocion").on("submit", function(e){
    e.preventDefault();
    var form = $("#formPromocion").serialize();
    //console.log(form);

    var link = '../../controller/administracion/c_llamadas_ajax.php';
    var data = { op : 24, form: form };
    var beforeFunction = function(){
        modal2.modal("hide");
    };
    var successFunction = function(data){
        if(data){
            if(data.result == 1){
                $("#card-content").load("../../view/administracion/admin_registros.php");
                showNotification("bg-green", "Se ha actualizado el registro correctamente", "bottom", "center", null, null);                
            }else{
                showNotification("bg-red", "No se pudo actualizar el registro correctamente. Verifica que los datos sean correctos. <br>En caso de redención, verifica que el usuario tenga los puntos necesarios para dicha operación ", "bottom", "center", null, null);                
            }
        }else{
            showNotification("bg-red", "Error al realizar la petición. Intenta nuevamente", "bottom", "center", null, null);    
        }        
    };
    var failFunction = function(){
        showNotification("bg-red", "Error de conexión. Intenta nuevamente", "bottom", "center", null, null);
    };
    connectServer(link,data, beforeFunction, successFunction, failFunction);
});

$("#formDelete").on("submit", function(e){
    e.preventDefault();
    var form = $("#formDelete").serialize();
    //console.log(form);

    var link = '../../controller/administracion/c_llamadas_ajax.php';
    var data = { op : 27, form: form };
    var beforeFunction = function(){
        modal1.modal("hide");
    };
    var successFunction = function(data){
        if(data){
            if(data.result == 1){
                $("#card-content").load("../../view/administracion/admin_registros.php");
                showNotification("bg-green", "Se ha eliminado el registro correctamente", "bottom", "center", null, null);                
            }else{
                showNotification("bg-red", "No se pudo eliminar el registro. Intenta nuevamente ", "bottom", "center", null, null);                
            }
        }else{
            showNotification("bg-red", "Error al realizar la petición. Intenta nuevamente", "bottom", "center", null, null);    
        }        
    };
    var failFunction = function(){
        showNotification("bg-red", "Error de conexión. Intenta nuevamente", "bottom", "center", null, null);
    };
    connectServer(link,data, beforeFunction, successFunction, failFunction);
});

// Edit record
$('#tableRegistros').on('click', 'a.btnDelete', function (e) {
    e.preventDefault();
    let btn = $(this);
    btn.tooltip('hide');
    formDelete.find("input[name='idRegistro']").val(btn.attr("data-id"));
    modal1.modal("show");
} );

// Delete a record
$('#tableRegistros').on('click', 'a.btnEdit', function (e){
    
    e.preventDefault();    
    let btn = $(this);
    btn.tooltip('hide');        
    var row = $("#rowModalBody");
    row.empty();

    var link = '../../controller/administracion/c_llamadas_ajax.php';
    var data = { op : 23, idRegistro :btn.attr("data-id") };
    var beforeFunction = function(){
    };
    var successFunction = function(response){
        row.html(response);
        modal2.modal("show");
    };
    var failFunction = function(){
        showNotification("bg-red", "Error al realizar la petición. Intenta nuevamente", "bottom", "center", null, null);
    };
    loadPage(link,data, beforeFunction, successFunction, failFunction);
    
});

$('#tableRegistros').DataTable();

function connectServer(link, data, beforeFunction, successFunction, failFunction){
  $.ajax({
    type: "POST",
    timeout: 15000,
    url: link,
    data: data,
    dataType: "json",
    beforeSend : function(){
        beforeFunction && beforeFunction();
    }
  }).done(function(data){
      successFunction && successFunction(data);
      console.log(data);
  }).fail(function(data){
      failFunction && failFunction(data);
      console.log(data);
  });
}

function showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
  //console.log("click");
  if (colorName === null || colorName === '') { colorName = 'bg-black'; }
  if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
  if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
  if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
  var allowDismiss = true;

  $.notify({
      message: text
  },{
    type: colorName,
    allow_dismiss: allowDismiss,
    newest_on_top: true,
    timer: 1000,
    placement: {
        from: placementFrom,
        align: placementAlign
    },
    animate: {
        enter: animateEnter,
        exit: animateExit
    },
    template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
    '<span data-notify="icon"></span> ' +
    '<span data-notify="title">{1}</span> ' +
    '<span data-notify="message">{2}</span>' +
    '<div class="progress" data-notify="progressbar">' +
    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
    '</div>' +
    '<a href="{3}" target="{4}" data-notify="url"></a>' +
    '</div>'
  });
}

function loadPage(link, data, beforeFunction, successFunction, failFunction){
  $.ajax({
    type: "POST",
    timeout: 15000,
    url: link,
    data: data,    
    beforeSend : function(){
        beforeFunction && beforeFunction();
    }
  }).done(function(data){
      successFunction && successFunction(data);
      console.log(data);
  }).fail(function(data){
      failFunction && failFunction(data);
      console.log(data);
  });
}

init();