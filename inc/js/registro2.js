/*JS DE FORMULARIO DE REGISTRO*/
$('#fecha_cumple').bootstrapMaterialDatePicker({ 
    format : 'YYYY-MM-DD',
    time : false,         
    lang : 'es', 
    weekStart : 1,
    okText : 'Aceptar',  
    cancelText : 'Cancelar'
  });

//Date
/*var $demoMaskedInput = $('.demo-masked-input');
$demoMaskedInput.find('.date').inputmask('dd/mm/yyyy', { placeholder: '__/__/____' });*/

/*function registro(){
  var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
  $('#loading').html(div);
  var cadena = $('#form_registro').serialize();
  $.ajax({  
    url: '../../controller/registro/c_llamadas_ajax.php',
    type: "POST",
    data:{ 
        op:1,
        cadena:cadena
        
    },
    success: function(resp){  
      $('#loading').html('');
      swal(" ", "registro Agregado", "success");
      menu('registro/registro2');
    }
  });     
}*/

function registro(){  
  var cadena = $('#form_registro').serialize();
  $.ajax({
    url: '../../controller/registro/c_llamadas_ajax.php',
    type: "POST",
    data:{ 
        op:5,
        cadena:cadena
    },
    success: function(response){     
        registrar(response);
    }
  });
}

function registrar(resp){
  var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
  $('#loading').html(div);
  var username = $("input[name=nombre]").val();
  var genero = $('#genero').val();
  var cadena = $('#form_registro').serialize();
  var submsj = 'En un momento recibiras un correo de confirmación de registro.';
  
  if(resp>=1){
    const swalWithBootstrapButtons = Swal.mixin({
      confirmButtonClass: 'btn btn-success',
      cancelButtonClass: 'btn btn-danger',
      buttonsStyling: false,
    });

    swalWithBootstrapButtons.fire({
      title: '¿Quieres Agregar esta tarjeta a tu Cuenta?',
      text: "Nota: Éste proceso no se puede deshacer..",
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: '¡Si, Agregar!',
      cancelButtonText: '¡No, cancelar!',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        addtarjeta(cadena);
        swalWithBootstrapButtons.fire(
          '',
          'Tu tarjeta ha sido agregada a tu cuenta',
          'success'
        )
      }else if(
        // Read more about handling dismissals
        result.dismiss === Swal.DismissReason.cancel
      ) {
        swalWithBootstrapButtons.fire(
          '',
          'Operacion Cancelada',
          'error'
        )
      }
    });

  }else{
    $.ajax({  
      url: '../../controller/registro/c_llamadas_ajax.php',
      type: "POST",
      data:{ 
        op:1,
        cadena:cadena
      },
      success: function(response){  
        $('#loading').html('');
        response = response.trim();
        if(response=='El usuario ya existe en la plataforma'){
          swal('Error!',response,'warning');
        }else{
          swal('Éxito!',"Se ha creado el usuario correctamente",'success');
        }        
      }
    }); 
  }
}

function addtarjeta(cadena){
  $.ajax({  
      url: '../../controller/registro/c_llamadas_ajax.php',
      type: "POST",
      data:{ 
          op:6,
          cadena:cadena
      },
      success: function(response){  
        $('#loading').html('');
        
      }
    }); 
}

function sendmsj(msj,username,logo,submsj,congrats,orangetxt){
  
  $.ajax({
      data:{ msj:msj, username:username, logo:logo, submsj:submsj, congrats:congrats, orangetxt:orangetxt },
      url: 'msj.php',
      method:'post',
      beforeSend: function(response){

      },
      success: function(response){
          $('#card-content').html(response);
      }
  });
}

