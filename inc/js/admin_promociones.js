function addpromo(){

    var type = $('#type').val();
    var promo = $('#promo').val();
    var descript = $('#detail').val();
    var smalltxt = $('#smalltxt').val();
    var conditions = $('#conditions').val();
    var howuse = $('#howuse').val();
    var infolink = $('#infolink').val();
    var bonustype = $('#bonustype').val();
    var rewardto = $('#rewardto').val();
    var rewardperc = $('#rewardperc').val();
    var npromos = $('#npromos').val();
    var nredention = $('#nredention').val();
    var start = $('#start').val();
    var ending = $('#ending').val();
    var pushtxt = $('#pushtxt').val();
    var smstxt = $('#smstxt').val();
    var whatstxt = $('#whatstxt').val();
    var cashback = $('#cashback').val();
    var cashbackperc = $('#cashbackperc').val();
    var descuento = $('#custom').val();
    var codigo = $('#codigo').val();
    //only for ckeditor
    var emailtxts = CKEDITOR.instances.emailtxt.getData();

    var sendtime = $('#sendtime').val();

    if($('#push').is(":checked")){
        var push = 1;
    }else{
        var push = 0;
    }

    if($('#sms').is(":checked")){
        var sms = 1;
    }else{
        var sms = 0;
    }

    if($('#whats').is(":checked")){
        var whats = 1;
    }else{
        var whats = 0;
    }

    if($('#email').is(":checked")){
        var email = 1;
    }else{
        var email = 0;
    }

    var suc = [];
    $('#suc :selected').each(function(i, selectedsuc) {
        suc[i] = $(selectedsuc).val();
    });

    var segment = [];
    $('#segment :selected').each(function(j, selected) {
        segment[j] = $(selected).val();
    });

    var lvl = [];
    $('#lvl :selected').each(function(k, selectedlvl) {
        lvl[k] = $(selectedlvl).val();
    });

    //alert(suc);
    //alert(segment);

    var op = 16;

    if(suc.length==0 && segment.length==0 && lvl.length==0){
        swal('Error!','Debes seleccionar al menos una Sucursal ó Segmento ó Nivel ','warning');
        throw new Error();
    }else{
        $.ajax({
            data:{ op:op, suc:suc, promo:promo, descript:descript, smalltxt:smalltxt, segment:segment, lvl:lvl,
            type:type, conditions:conditions, howuse:howuse, infolink:infolink, bonustype: bonustype, rewardto:rewardto, rewardperc:rewardperc,
            npromos:npromos, nredention:nredention, start:start, ending:ending, push:push, sms:sms, whats:whats,
            email:email, pushtxt:pushtxt, smstxt:smstxt, whatstxt:whatstxt, emailtxts:emailtxts, sendtime:sendtime, 
            cashback:cashback, cashbackperc:cashbackperc, descuento:descuento, codigo:codigo },
            url: '../../controller/administracion/c_llamadas_ajax.php',
            method:'post',
            beforeSend: function(response){

            },
            success: function(response){
                if (!isNaN(response)){
                    sendqr(response);
                    swal(" ", "Elemento Agregado a Promociones", "success");
                    menu('administracion/admin_promociones');
                }else{
                    swal('Error!','El Elemento no pudo agregarse','warning');
                }
                
            }
        });
    }
    
}

function sendqr(data){
    $.ajax({
        data: data,
        url: '../../inc/phpqrcode/index.php?data='+data,
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            console.log(data);
        }
    });
}

function togglemode(id){
    $('#'+id).toggle();
}


    