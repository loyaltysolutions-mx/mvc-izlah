 $(document).ready(function(){
  
/* FUNCION PARA  MENU Y SUB MENU*/
 $('.sub_menu_cont').hide();
 $('.sub_menu').click(function(){ 
            var sub=$(this).attr('id');    
            $('#sub_menu_'+sub).slideToggle('fast');      
         });
/*FUNCION PARA TOOLTIP BOOTSTRAP */
 $('[data-toggle="tooltip"]').tooltip();
 /* FUNCION PARA CALENDARIO */
 $.datepicker.regional['es'] = {
    closeText: 'Cerrar',
    prevText: '<Ant',
    nextText: 'Sig>',
    currentText: 'Hoy',
    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
    monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
    dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
    dayNamesShort: ['Dom','Lun','Mar','Mie','Juv','Vie','Sab'],
    dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sab'],
    weekHeader: 'Sm',
    dateFormat: 'dd/mm/yy',
    firstDay: 1,
    isRTL: false,
    showMonthAfterYear: false,
    yearSuffix: ''
 };
 $.datepicker.setDefaults($.datepicker.regional['es']);
   $('.datepicker').datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd' 
   });
   
 
});

