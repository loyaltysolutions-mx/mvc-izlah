function addcat(){
    var cat = $('#cat').val();
    var descript = $('#descript').val();
    var op = 9;

    $.ajax({
        data:{ op: op, cat: cat, descript: descript },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                swal(" ", "Elemento Agregado a Categorías", "success");
                menu('administracion/admin_categorias');
            }else{
                swal('Error!','El Elemento no pudo agregarse','warning');
            }
            
        }
    });
}