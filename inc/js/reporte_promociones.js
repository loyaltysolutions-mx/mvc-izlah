/*JS REPORTE HISTORIAL CLIENTES*/

function initFiltros(){
  $("#selectSucursal").selectpicker();
  $("#selectSegmento").selectpicker();
  $("#selectNivel").selectpicker();
  $("#divTable").hide();
}

$("#btnConsultar").click(function(){

  var dataForm = {};
  
  let checkSucursal = $("#inputSucursal").prop("checked");
  let checkSegmento = $("#inputSegmento").prop("checked");  
  let checkNivel = $("#inputNivel").prop("checked");
  let sucursal = $("#selectSucursal");
  let segmento = $("#selectSegmento");
  let nivel = $("#selectNivel");
  
  if(checkSucursal){
    dataForm['idSucursal'] = sucursal.val();
  }

  if(checkSegmento){
    dataForm['segmento'] = segmento.val();
  }

  if(checkNivel){
    dataForm['nivel'] = nivel.val();
  }

  dataForm['action'] = 7;

  let link = '../../controller/reportes/c_ajax_reporte.php';
  let data = dataForm;    
  let beforeFunction = function(){
    if(!$("#divTable").is(":visible"))
      $("#divTable").fadeIn();
    $(".divTable").fadeOut();
  }
  
  let successFunction = function(data){    
    if(data.result){
      var arrayResult = [];      
      callback(function(){
        if(data.result.length > 0){
          $.each(data.result, function(key,value){
            var arraySegmentos = [];
            var arraySucursales = [];
            var arrayNiveles = [];
            let listSegmento = value.segmentos; 
            let listNiveles = value.niveles; 
            let listSucursales = value.sucursales; 

            $.each(listSegmento, function(index, auxSegmento){
              //console.log(auxSegmento);
              arraySegmentos.push("<span class='label label-info'>"+auxSegmento.nombre_segmento+"</span>");
            });

            $.each(listNiveles, function(index, auxNivel){
              //console.log(auxNivel);
              arrayNiveles.push("<span class='label label-success'>"+auxNivel.nombre+"</span>");
            });

            $.each(listSucursales, function(index, auxSucursal){
              //console.log(auxSucursal);
              arraySucursales.push("<span class='label label-warning'>"+auxSucursal.nombreSucursal+"</span>");
            });

            let auxArray = [value.id_promo, value.nombrePromocion, value.detalle, value.texto_corto, value.imagen_qr && value.imagen_qr != "" ? '<a target="_blank" href="http://desclub.com.mx/plataforma_lealtad/inc/imagenes/img_qr/'+value.imagen_qr+'" class="btn btn-primary"><i class="fas fa-qrcode" aria-hidden="true"></i> Ver Imagen</a>' : "No disponible", arraySucursales, arraySegmentos, arrayNiveles, value.activo == 0 ? "Activo" : "Inactivo"];          
            arrayResult.push(auxArray);
          });
        }
      }, function(){
        if ( $.fn.dataTable.isDataTable( '#tableResult' ) ) {
          let table = $('#tableResult').DataTable();
          table.destroy();
        }
        let titleFile = 'ReportePromociones_'+new Date().getDay()+new Date().getMonth()+new Date().getFullYear();
        $('#tableResult').DataTable({
          data: arrayResult,
          columns: [{title : "ID Promocion"},{title : "Nombre"},{title : "Detalles"},{title : "Texto Corto"},{title : "Imagen QR"}, {title : "Sucursal(es)"}, {title : "Segmento(s)"}, {title : "Nivel(es)"}, {title : "Status"}],
          dom: 'Bfrtip',
          responsive: true,
          buttons: [
            'copy',
            { extend: 'csvHtml5', title: titleFile },
            { extend: 'excelHtml5', title: titleFile },
            { extend: 'pdfHtml5', title: titleFile },
            'print'
          ]
        });

        $(".divTable").fadeIn();
        //console.log(arrayResult);
      });
    }
  };

  let failFunction = function(data){      
    $(".divTable").fadeIn();
  };

  connectServer(link, data, beforeFunction, successFunction, failFunction);  
});

$(".divFilter").hide();
let filter = $(".filter");
filter.click(function(){
  let parent = $(this).parent().parent().parent();
  if($(this).prop("checked")){      
    parent.find(".divFilter").fadeIn();
  }else{      
    parent.find(".divFilter").fadeOut();
    parent.find('input, textarea').val('');
    parent.find("select").val('default').selectpicker("refresh");
  }
});

function connectServer(link, data, beforeFunction, successFunction, failFunction){
  $.ajax({
    type: "POST",
    timeout: 20000,
    url: link,
    data: data,
    dataType: "json",
    beforeSend : function(){
        beforeFunction && beforeFunction();
    }
  }).done(function(data){
      console.log(data);
      successFunction && successFunction(data);        
  }).fail(function(data){
      failFunction && failFunction(data);
      console.log(data);
  });
}

function callback(callback1, callback2){
  callback1 && callback1();
  callback2 && callback2();
}


initFiltros();
