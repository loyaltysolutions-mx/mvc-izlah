function setfile(id,file,table){
    var op = 7;

    $.ajax({
        data:{ op:op, id:id, file:file, table:table },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            //swal(" ", "", "success");
        }
    });
}

function cleanfile(id){
    $('#filename').attr('my',id);
    $('#frmFileUpload').removeClass('dz-started'); 
    $(".dz-preview").remove();
    
}

function delrow(id,table,field,res){
    var op = 8;

    $.ajax({
        data:{ op:op, id:id, table:table, field:field, res:res },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                //swal(" ", "Usuario Eliminado", "success");
                //menus('usuario/edituser');
            }else if(response==2){
                swal(" ", "Registro Eliminado Correctamente", "success");
                menu('administracion/admin_menu');
            }else if(response==3){
                //swal(" ", "Parque Eliminado", "success");
                //menus('usuario/editpark');
            }else if(response==4){
                //swal(" ", "Promoción Eliminada", "success");
                //menus('usuario/editpromo');
            }else{
                swal('Error!','El Registro no pudo eliminarse','warning');
            }
            
        }
    });
}

function changesel(type){
    if(type==1){
        $('#cashbackd').show();
        customhide();
    }else if(type==3){
        $('#customd').show();
        $('#codigod').show();
        $('#custom').attr('placeholder','* $ de Descuento');
        $('#custom').attr('required','required');
        cashbackhide();
    }else if(type==6){
        $('#customd').show();
        $('#codigod').show();
        $('#custom').attr('placeholder','* % de Descuento');
        $('#custom').attr('required','required');
        cashbackhide();
    }else{
        customhide();
        cashbackhide();
    }
}

function customhide(){
    $('#custom').removeAttr('required');
    $('#customd').hide();
    $('#codigod').hide();
    $('#custom').val('');
    $('#codigo').val('');
}

function cashbackhide(){
    $('#cashbackd').hide();
    $('#cashback').val('');
    $('#cashbackperc').val('');
}

$("#cashback").on('keyup', function(){
    var cashback = $(this).val().length;
    if(cashback>0){
        $('#cashbackperc').val('');
    }
}).keyup();

$("#cashbackperc").on('keyup', function(){
    var cashbackperc = $(this).val().length;
    if(cashbackperc>0){
        $('#cashback').val('');
    }
}).keyup();

$('#push').change(function() {
    if(this.checked) {
        $('#pushdiv').show();
    }else{
        $('#pushdiv').hide();
        $('#pushtxt').val('');
    }
});

$('#sms').change(function() {
    if(this.checked) {
        $('#smsdiv').show();
    }else{
        $('#smsdiv').hide();
        $('#smstxt').val('');
    }
});

$('#whats').change(function() {
    if(this.checked) {
        $('#whatsdiv').show();
    }else{
        $('#whatsdiv').hide();
        $('#whatstxt').val('');
    }
});

$('#email').change(function() {
    if(this.checked) {
        $('#emaildiv').show();
    }else{
        $('#emaildiv').hide();
        $('#emailtxt').val('');
        CKEDITOR.instances.emailtxt.setData('');
    }
});

$("#rewardto").on('keyup', function(){
    var rewardto = $(this).val().length;
    if(rewardto>0){
        $('#rewardperc').val('');
        $('#rewardval').val('');
    }else{
        
    }
}).keyup();

$("#rewardperc").on('keyup', function(){
    var rewardto = $(this).val().length;
    if(rewardto>0){
        $('#rewardto').val('');
        $('#rewardval').val('');
    }
}).keyup();

$("#rewardval").on('keyup', function(){
    var rewardto = $(this).val().length;
    if(rewardto>0){
        $('#rewardto').val('');
        $('#rewardperc').val('');
    }
}).keyup();

CKEDITOR.replace('emailtxt');

$('.datetimepicker').bootstrapMaterialDatePicker(
    { 
        format : 'YYYY-MM-DD HH:mm:ss', 
        lang : 'es', 
        weekStart : 1,
        okText : 'Aceptar',  
        cancelText : 'Cancelar',
        minDate : new Date() 
    }
);
//edit content
//multi select initiate
$('.sucursal').multiSelect();
$('.segment').multiSelect();
$('.lvl').multiSelect();

//$('.selectpicker').selectpicker();
/*$(document).ready(function() {
    $("i.fas").popover({'trigger':'hover'});
});*/
//Active/Inactive
