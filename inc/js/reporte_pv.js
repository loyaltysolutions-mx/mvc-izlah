/*JS REPORTE PV*/

$("#btnCreateSegment").hide();
$("#divSegmento").hide();
$("#divTable").hide();
$("#btnConsultar").hide();
var filters = $(".filter");
$("#selectSemana").selectpicker();
$("#selectTipo").selectpicker();
$("#selectPV").selectpicker();

filters.change(function(){  
  if($(".filter:checked").length > 0)
    $("#btnConsultar").fadeIn();
  else
    $("#btnConsultar").fadeOut();
});

function initFiltros(){   

  var puntosSlider = document.getElementById('slidePuntos');
  if(puntosSlider != null){
    noUiSlider.create(puntosSlider, {
      start: [0,10000],
      step: 1,
      connect: true,
      range: {
          'min': [0],
          'max': [10000]
      },
      format: {
        to: function (value) {                  
            return value;
        },
        from: function (value) {                  
            return value;
        }
      }
    });
    puntosSlider.noUiSlider.on('update', function(values, handle) {
      $("#slidePuntos").parent().find(".js-nouislider-value").text("Min: "+values[0].toFixed(0)+", Max:"+values[1].toFixed(0));      
    });
    //getNoUISliderValue(puntosSlider, false);  
  }
  

  var puntosVisitas = document.getElementById('slideVisitas');        
  if(puntosVisitas != null){
    noUiSlider.create(puntosVisitas, {
      start: [1,100],
      step: 1,
      connect: true,
      range: {
          'min': [1],
          'max': [100]
      },
      format: {
        to: function (value) {                  
            return value.fixed(2);
        },
        from: function (value) {                  
            return value.fixed(2);
        }
      }
    });
    puntosVisitas.noUiSlider.on('update', function(values, handle) {
      $("#slideVisitas").parent().find(".js-nouislider-value").text("Min: "+values[0].toFixed(0)+", Max:"+values[1].toFixed(0));      
    });
  }

  var ticketSlider = document.getElementById('slideTicketPromedio');        
  noUiSlider.create(ticketSlider, {
    start: [0,20000],
    step: 1,
    connect: true,
    range: {
        'min': [0],
        'max': [20000]
    },
    format: {
      to: function (value) {                  
          return value;
      },
      from: function (value) {                  
          return value;
      }
    }
  });
  ticketSlider.noUiSlider.on('update', function(values, handle) {
    $("#slideTicketPromedio").parent().find(".js-nouislider-value").text("Min: "+values[0].toFixed(0)+", Max:"+values[1].toFixed(0));      
  });
  //getNoUISliderValue(ticketSlider, false);

  $('#desde').bootstrapMaterialDatePicker({ 
    format : 'YYYY-MM-DD',
    time : false,         
    lang : 'es', 
    weekStart : 1,
    okText : 'Aceptar',  
    cancelText : 'Cancelar'
  });

  $('#hasta').bootstrapMaterialDatePicker({ 
    format : 'YYYY-MM-DD',
    time : false,         
    lang : 'es', 
    weekStart : 1,
    okText : 'Aceptar',  
    cancelText : 'Cancelar'
  });

  $('#desde').change(function(){ 
    let value = $(this).val();                
    $('#hasta').bootstrapMaterialDatePicker('setMinDate', value);
    $('#hasta').val("");        
  });

}

$("#btnCreateSegment").click(function(){
  var table = $('#tableResult').DataTable();
  var ids = table.columns(0).data();
  console.log($("#selectSegmento"));
  initCreateSegmento(ids[0]);
  
});

function initCreateSegmento(rows){  
  let selectSegmento = $("#selectSegmento");  
  let timerInterval
  swal({
    type: 'warning',
    title: 'Elige un segmento:',    
    html: '<select id="idSegmento">'+selectSegmento[0].innerHTML+'</select>',
    onBeforeOpen: () => {      
      //$('#idSegmento').selectpicker();
    },
    showCancelButton: true,
    confirmButtonText: 'Agregar clientes',
    showLoaderOnConfirm: true,
    preConfirm: (login) => {
      if( $("#idSegmento").val() != "" ){
        list = rows.filter(function (x, i, a) { 
          return a.indexOf(x) == i; 
        });
        let link = '../../controller/reportes/c_ajax_reporte.php';
        let dataSegmento = { 'action' : 3, 'idSegmento' : $("#idSegmento").val(), rows : list};
        let beforeFunction = function(){      
          
        };
        let successFunction = function(data){
          if(data.result == 1){
            swal("Exito", "Se ha creado el segmento correctamente", "success");
            $("#card-content").load('../../view/reportes/reporte_pv.php');
          }else{
            swal("Error", "Error al crear el segmento", "error");
          }
        };

        let failFunction = function(data){
          swal("", 'Ocurrio un problema de conexión.','error');
        };
        connectServer(link, dataSegmento, beforeFunction, successFunction, failFunction);
      }else{
        initCreateSegmento(rows);
      }
    },
    allowOutsideClick: () => !Swal.isLoading()
  });
}

$("#btnConsultar").click(function(){

  var dataForm = {};
  
  let checkPV = $("#inputPV").prop("checked");
  let checkTipo = $("#inputTipoPV").prop("checked");
  let checkFechaRegistro = $("#inputFechaRegistro").prop("checked");
  let checkSemana = $("#inputSemana").prop("checked");
  let checkTicketPromedio = $("#inputTicketPromedio").prop("checked");
  let checkPuntos = $("#inputPuntos").prop("checked");
  let checkVisitas = $("#inputVisitas").prop("checked");

  if(checkPV){
    dataForm['idPuntoVenta'] = $("#selectPV").val();
  }

  if(checkTipo){
    dataForm['tipoPV'] = $("#selectTipo").val();
  }

  if(checkFechaRegistro){      
    dataForm['fechaRegistro'] = {'desde': $("#desde").val(),'hasta': $("#hasta").val()};      
  }

  if(checkSemana){
    dataForm['diasSemana'] = $("#selectSemana").val();
  }

  if(checkTicketPromedio){      
    let slideTicket = document.getElementById('slideTicketPromedio');
    slideTicket = slideTicket.noUiSlider.get();
    dataForm['ticketPromedio'] = {'min':slideTicket[0],'max':slideTicket[1]};      
  }
  
  if(checkPuntos){
    let slidePuntos = document.getElementById('slidePuntos');
    slidePuntos = slidePuntos.noUiSlider.get();
    dataForm['puntos'] = {'min':slidePuntos[0],'max':slidePuntos[1]};      
  }

  if(checkVisitas){
    let slideVisitas = document.getElementById('slideVisitas');
    slideVisitas = slideVisitas.noUiSlider.get();
    dataForm['visitas'] = {'min':slideVisitas[0],'max':slideVisitas[1]};      
  }

  dataForm['action'] = 5;

  let link = '../../controller/reportes/c_ajax_reporte.php';
  let data = dataForm;    
  let beforeFunction = function(){
    if(!$("#divTable").is(":visible"))
      $("#divTable").fadeIn();

    $(".divTable").fadeOut();
  }
  /*let successFunction = function(data){    
    if(data.result){
      var arrayResult = [];
      if(data.result.length > 0){                  
        $.each(data.result, function(key,value){
          let auxArray = [value.id_usuario, value.nombreCliente, value.nombrePV, value.nombreTipo, value.ticket, value.monto_ticket != "" ? "$"+value.monto_ticket : "", value.id_tipo_registro && value.puntos != "" ? (value.id_tipo_registro != 2 ? "+"+value.puntos : "-"+value.puntos) : "", value.num_visita, value.nombreTipoRegistro, value.dia_registro, value.fecha_registro];         
          arrayResult.push(auxArray);
        });  
      }
      if(arrayResult.length > 0)
        $("#btnCreateSegment").show();
      else
        $("#btnCreateSegment").hide();
      
      if ( $.fn.dataTable.isDataTable( '#tableResult' ) ) {
        let table = $('#tableResult').DataTable();
        table.destroy();
        $("#tableResult").empty();
      }

      let titleFile = 'ReportePuntoVenta_'+new Date().getDay()+new Date().getMonth()+new Date().getFullYear();

      $('#tableResult').DataTable({
        data: arrayResult,
        columns: [ {title:"ID Cliente"}, {title:"Nombre Cliente"},{ title:"Nombre Punto Venta"}, {title:"Tipo Punto Venta"}, {title:"Ticket"}, {title:"Monto Ticket"}, {title:"Puntos"}, {title:"Visitas"}, {title:"Tipo Registro"}, {title:"Dia Registro"}, {title:"Fecha Registro"}],
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
          'copy',
          { extend: 'csvHtml5', title: titleFile },
          { extend: 'excelHtml5', title: titleFile },
          { extend: 'pdfHtml5', title: titleFile },
          'print'
        ]
      });

      $('#tableResult').on( 'page.dt', function (){
        var table = $("#tableResult").DataTable();
        var info = table.page.info();
        $('#pageInfo').html( 'Mostrando: '+info.page+' of '+info.pages );
      });
      $(".divTable").fadeIn();
    }
  };*/

  let successFunction = function(response){
    $("#ajaxResult").fadeIn();
    $("#ajaxResult").html(response);
    console.log(response);
  }

  let failFunction = function(data){      
    $(".divTable").fadeIn();
    $("#ajaxResult").fadeIn();
  };

  if(!checkFechaRegistro || ($("#desde").val() != "" && $("#hasta").val() != "") ){
    $("#textDesde").text("");
    $("#textHasta").text("");
    loadPage(link, data, beforeFunction, successFunction, failFunction);
  }else{
    if($("#desde").val() == ""){
      $("#textDesde").text("Por favor elige una fecha");
    }
    if($("#hasta").val() == ""){
      $("#textHasta").text("Por favor elige una fecha");
    }
  }
  
});

$(".divFilter").hide();
let filter = $(".filter");
filter.click(function(){
  let parent = $(this).parent().parent().parent();
  if($(this).prop("checked")){      
    parent.find(".divFilter").fadeIn();
  }else{      
    parent.find(".divFilter").fadeOut();
    parent.find('input:text, input:password, input:file, textarea').val('');
    parent.find("select").val('default').selectpicker("refresh");
  }
});

function connectServer(link, data, beforeFunction, successFunction, failFunction){
  $.ajax({
    type: "POST",
    timeout: 20000,
    url: link,
    data: data,
    dataType: "json",
    beforeSend : function(){
        beforeFunction && beforeFunction();
    }
  }).done(function(data){
      console.log(data);
      successFunction && successFunction(data);        
  }).fail(function(data){
      failFunction && failFunction(data);
      console.log(data);
  });
}

function loadPage(link, data, beforeFunction, successFunction, failFunction){
  $.ajax({
    type: "POST",
    timeout: 15000,
    url: link,
    data: data,    
    beforeSend : function(){
        beforeFunction && beforeFunction();
    }
  }).done(function(data){
      successFunction && successFunction(data);
      //console.log(data);
  }).fail(function(data){
      failFunction && failFunction(data);
      //console.log(data);
  });
}


initFiltros();
