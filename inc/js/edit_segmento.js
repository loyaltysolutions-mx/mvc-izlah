/*JS DE FORMULARIO DE REGISTRO*/

$('#loading2').hide();
$('#loading1').hide();
$('#form_add_cliente').hide();
$('#tableUsers').DataTable();

$('.btnDeleteUserSegmento').click(function(){
  let object = $(this).parents('tr');
  swal({
    title: '',
    text: "¿Estas seguro de eliminar el usuario del segmento?. Ésta acción no se puede deshacer.",                
    icon:'warning',                
    buttons: {
      cancel: 'Cancelar',
      catch: 'Eliminar Usuario',
    },
    dangerMode: true,                
  }).then((result) => {
    if (result) {
      var idUsuario = $(this).attr("data-action");
      let link = '../../controller/administracion/c_llamadas_ajax.php';
      let data = {'op' : 3, 'action' : 5, 'idUsuario' : idUsuario};
      let beforeFunction = function(){          
        $(this).prop('disabled', true);
      };
      let successFunction = function(data){          
        $(this).prop('disabled', false); 
        if(data.result == 1){
          swal({ 
            title : "", 
            text : 'Se ha eliminado el usuario correctamente', 
            icon: 'success'
          });
          var table = $("#tableUsers").DataTable();              
          table.row(object).remove().draw();
        }else{
          swal("", 'Ocurrio un problema al eliminar el usuario del segmento.','error');
        }
      };
      let failFunction = function(){
        swal("", 'Ocurrio un problema de conexión.','error');
      };

      connectServer(link, data,  beforeFunction,successFunction, failFunction);
    }
  });
});

$(".btnBack").click(function(){
  var link = "../../view/administracion/admin_segmento.php";
  var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
  var beforeFunction = function(){
    $("#card-content").html(div);
  };
  var successFunction = function(response){    
    $("#card-content").html(response);
  };
  var failFunction = function(data){
    
  };
  loadPage(link,null,beforeFunction,successFunction,failFunction);  
});

function callback(callback1, callback2){
  callback1 && callback1();
  callback2 && callback2();
}

$('#btnNewUserSegmento').click(function(){
  
  let idUsuario = $("#idUsuario").val();
  let idSegmento = $("#idSegmento").val();

  if(idUsuario == ''){
      $("#labelUser").text("Por favor elige un usuario");
  }else{
    var link = "../../controller/administracion/c_llamadas_ajax.php";
    var data = {'op' : 3, 'action' : 6, 'idUsuario' : idUsuario, 'idSegmento' : idSegmento};
    var beforeFunction = function(){
      $("#labelUser").text("");
      $('#loading1').show();
      $(this).prop('disabled', true);
    };
    var successFunction = function(data){        
      $(this).prop('disabled', false); 
      if(data.result == 1){
        swal({
          title: "Èxito",
          text: "¡Se ha agregado el usuario satisfactoriamente!",
          icon: "success"
        });          
        $('#loading1').hide();
        var table = $("#tableUsers").DataTable();
        table.row.add( [
            data.user[0].id_usuario,
            data.user[0].nombre,
            data.user[0].email,
            data.user[0].activo == 0 ? "Activo" : "Inactivo",
            '<a class="btn btn-primary" href="../../view/administracion/view_info_user_segmento.php?id='+data.user[0].id_usuario+'" target="_blank" onClick="window.open(this.href, this.target, \'width=700,height=800\'); return false;"><i class="far fa-user-circle"></i><span> Ver más</span></a>&nbsp;<a href="javascript:;" class="btn btn-danger btnDeleteUserSegmento" data-action="'+data.user[0].id_usuario+'"><i class="far fa-trash-alt"></i> Quitar del segmento</a>'
        ] ).draw( false );
        //activeButton();
      }else{
        swal("", 'Ocurrio un problema al agregar al usuario.','error');
        $('#loading1').hide();
      }
    };
    var failFunction = function(data){
      swal("", 'Ocurrio un problema de conexión.','error');
      $('#loading1').hide();
    };
    connectServer(link,data,beforeFunction,successFunction,failFunction);
  }
  
});

/*function activeButton(){

  $("#form_add_cliente").toggle(800);
  $("#muestra_form_add_usu1").find("i").removeClass("fa-minus").addClass("fa-plus");
  $('.btnViewUser').click(function(){    
    let action = $(this).attr("data-action");
    var link = "../../controller/administracion/c_llamadas_ajax.php";
    var data = {'op' : 3, 'action' : 4, 'idUsuario' : action};
    var beforeFunction = function(){
      
    };
    var successFunction = function(data){      
      if(data.result == 1 && data.user){
        let registro = data.user;
        swal({
          title: registro.nombre,
          text: 
            "Teléfono: "+registro.telefono+
            "\nEmail: "+registro.email+
            "\nRFC: "+registro.rfc+
            "\nTarjeta: "+registro.tarjeta+
            "\nFecha Nacimiento: "+registro.fecha_nacimiento,
          icon:'info',
        });                      
          
      }else{
        swal("", 'Ocurrio un problema al consultar la información. Intenta nuevamente.','error');
      }
    };
    var failFunction = function(data){
      swal("", 'Ocurrio un problema de conexión.','error');
    };
    connectServer(link,data,beforeFunction,successFunction,failFunction)
    
  });

  $('.btnDeleteUserSegmento').click(function(){
    let object = $(this).parents('tr');
    
    swal({
      title: '',
      text: "¿Estas seguro de eliminar el usuario del segmento?. Ésta acción no se puede deshacer.",                
      icon:'warning',                
      buttons: {
        cancel: 'Cancelar',
        catch: 'Eliminar Usuario',
      },
      dangerMode: true,                
    }).then((result) => {
      if (result) {
        var idUsuario = $(this).attr("data-action");
        let link = '../../controller/administracion/c_llamadas_ajax.php';
        let data = {'op' : 3, 'action' : 5, 'idUsuario' : idUsuario};
        let beforeFunction = function(){          
          $(this).prop('disabled', true);
        };
        let successFunction = function(data){          
          $(this).prop('disabled', false); 
          if(data.result == 1){
            swal({ 
              title : "", 
              text : 'Se ha eliminado el usuario correctamente', 
              icon: 'success'
            });
            var table = $("#tableUsers").DataTable();              
            table.row(object).remove().draw();
          }else{
            swal("", 'Ocurrio un problema al eliminar el usuario del segmento.','error');
          }
        };
        let failFunction = function(){
          swal("", 'Ocurrio un problema de conexión.','error');
        };

        connectServer(link, data,  beforeFunction,successFunction, failFunction);
      }
    });
  });
}*/

$('#btnEditSegmento').click(function(){
  
  var nombre = $('#nombreSegmentoEdit').val();
  var descripcion = $('#descripcionSegmentoEdit').val();
  var status = $('#statusSegmentoEdit').val();
  var idSegmento = $('#idSegmento').val();  

  if(nombre==''){
      $("#labelSegmento").text("Por favor ingrese un nombre");
  }else{
    var link = "../../controller/administracion/c_llamadas_ajax.php";
    var data = {'op' : 3, 'action' : 2, 'nombreSegmento' : nombre, 'descripcionSegmento' : descripcion, 'statusSegmento' : status, 'idSegmento' : idSegmento};
    var beforeFunction = function(){
      $("#labelSegmento").text("");
      $('#loading2').show();
      $(this).prop('disabled', true);
    };
    var successFunction = function(data){
      $('#loading2').hide();
      $(this).prop('disabled', false); 
      if(data.result == 1){
        swal({
          title: "Èxito",
          text: "¡Se ha actualizado el segmento correctamente!",
          icon: "success"
        });
        $("#card-content").load('../../view/administracion/admin_segmento.php');
      }else{
        swal("", 'Ocurrio un problema al actualizar el segmento.','error');
      }
    };
    var failFunction = function(data){
      swal("", 'Ocurrio un problema de conexión.','error');
    };
    connectServer(link,data,beforeFunction,successFunction,failFunction)
  }    
});
 

$('#muestra_form_add_usu1').click(function(){
  $("#form_add_cliente").toggle(800);   
});

function connectServer(link, data, beforeFunction, successFunction, failFunction){
  $.ajax({
    type: "POST",
    timeout: 15000,
    url: link,
    data: data,
    dataType: "json",
    beforeSend : function(){
        beforeFunction && beforeFunction();
    }
  }).done(function(data){
      successFunction && successFunction(data);
      console.log(data);
  }).fail(function(data){
      failFunction && failFunction(data);
      console.log(data);
  });
}

function loadPage(link, data, beforeFunction, successFunction, failFunction){
  $.ajax({
    type: "POST",
    timeout: 15000,
    url: link,
    data: data,    
    beforeSend : function(){
        beforeFunction && beforeFunction();
    }
  }).done(function(data){
      successFunction && successFunction(data);
      console.log(data);
  }).fail(function(data){
      failFunction && failFunction(data);
      console.log(data);
  });
}
