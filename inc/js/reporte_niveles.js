/*JS REPORTE NIVELES*/

function initFiltros(){    
  $("#divTable").hide();
  $("#btnConsultar").hide();
  
  var filters = $(".filter");
  filters.change(function(){  
    if($(".filter:checked").length > 0)
      $("#btnConsultar").fadeIn();
    else
      $("#btnConsultar").fadeOut();
  });

  $("#selectTipo").selectpicker();  
}

$("#btnConsultar").click(function(){  
  let checkTipo = $("#inputTipo").prop("checked");  
  let dataForm = {'action' : 8};
  
  if(checkTipo){
    dataForm['nivel'] = $("#selectTipo").val();
  }

  let link = '../../controller/reportes/c_ajax_reporte.php';
  let data = dataForm;
  let beforeFunction = function(){
    $(".divTable").fadeOut();
  }
  let successFunction = function(data){
    if(data.result){
      var arrayResult = [];
      if(data.result.length > 0){                  
        $.each(data.result, function(key,value){
          let auxArray = [value.id_nivel, value.nombre, value.nombreTipo, value.ini+" - "+value.fin+" "+value.nombreTipo, value.num_usuarios];          
          arrayResult.push(auxArray);
        });  
      }
            
      if ( $.fn.dataTable.isDataTable( '#tableResult' ) ) {
        let table = $('#tableResult').DataTable();
        table.destroy();
        $("#tableResult").empty();
      }

      let titleFile = 'Reporte_Niveles'+new Date().getDay()+new Date().getMonth()+new Date().getFullYear();

      $('#tableResult').DataTable({
        data: arrayResult,
        columns: [{title:"ID Nivel"}, {title:"Nombre Nivel"},{title:"Tipo de Nivel"}, {title:"Rango del Nivel"}, {title:"Usuarios Inscritos"}],
        dom: 'Bfrtip',
        responsive: true,
        buttons: [
          'copy',
          { extend: 'csvHtml5', title: titleFile },
          { extend: 'excelHtml5', title: titleFile },
          { extend: 'pdfHtml5', title: titleFile },
          'print'
        ]
      });

      $('#tableResult').on( 'page.dt', function () {
        var table = $("#tableResult").DataTable();
        var info = table.page.info();
        $('#pageInfo').html( 'Mostrando: '+info.page+' of '+info.pages );
      });
      $("#divTable").fadeIn();
      $(".divTable").fadeIn();
    }      
  };

  let failFunction = function(data){      
    $(".divTable").fadeIn();
  };
  
  connectServer(link, data, beforeFunction, successFunction, failFunction);
  
});


$(".divFilter").hide();
let filter = $(".filter");
filter.click(function(){
  let parent = $(this).parent().parent().parent();
  if($(this).prop("checked")){      
    parent.find(".divFilter").fadeIn();
  }else{      
    parent.find(".divFilter").fadeOut();
    parent.find('input, textarea').val('');
    parent.find("select").val('default').selectpicker("refresh");
  }
});

function connectServer(link, data, beforeFunction, successFunction, failFunction){
  $.ajax({
    type: "POST",
    timeout: 20000,
    url: link,
    data: data,
    dataType: "json",
    beforeSend : function(){
        beforeFunction && beforeFunction();
    }
  }).done(function(data){
      console.log(data);
      successFunction && successFunction(data);        
  }).fail(function(data){
      failFunction && failFunction(data);
      console.log(data);
  });
}

initFiltros();
