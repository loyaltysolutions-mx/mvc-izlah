function redencion(){
    var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
    $('#loading').html(div);
    var cadena = $('#form_redencion').serialize();
    
    $.ajax({
      url: '../../controller/redencion/c_llamadas_ajax.php',
      type: "POST",
      data:{ 
        op:1,
        cadena:cadena
      },
      success: function(resp){  
        console.log(resp);
        $('#loading').html('');
        if(resp==1){
            swal("", 'Todos los campos estan en blanco',"error"); 
        }
        if(resp!=1 && resp!=3){
            var id=resp;
            //$('#guarda_redencion').hide();
            $.ajax({
                type: "post",
                url: "../../view/redencion/productos_redencion2.php",
                data: {id_usu: id},
                success: function (response) {
                  //console.log(id);
                  $('#card-content').html(response);
                }
            });
            //$(location).attr('href', '?id_usu='+id);
        }
        if(resp==3){
            swal("", 'No se encontro Registro con la información proporcionada',"error"); 
        }
      }
    });//end ajax     
}//end function

initial();
/*JS DE FORMULARIO DE REDENCION*/
function initial(){
  swal({
    position: 'top-end',
    type: 'info',
    text: 'Recuerda que es necesario que el cliente muestre una Identificacion Personal',
    showConfirmButton: false,
    timer: 3000
  }) 
}//end 