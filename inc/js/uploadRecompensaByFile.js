var dropzoneRecompensa = new Dropzone("#dropzoneRecompensa",{
    url : "../../controller/administracion/c_llamadas_ajax.php",
    autoProcessQueue: false,
    paramName: "file_reward", // The name that will be used to transfer the file
    maxFilesize: 1, // MB
    maxFiles: 1, // MB
    addRemoveLinks: true,
    acceptedFiles: ".xls,.xlsx",
    init: function(){
        var _this = this;
        $("#formFileRecompensa").on("submit", function(e){
            e.preventDefault();
            _this.processQueue();            
        });
    },
    success: function(data){
        console.log(data);
    }
});

dropzoneRecompensa.on("sending", function(file, xhr, data) {        
    data.append("op", 28);
    //console.log("sending");
});

dropzoneRecompensa.on("error", function(file, data){
    dropzoneRecompensa.removeFile(file);
    console.log(data);
    //showNotification("bg-red", "Archivo inválido", "bottom", "center", null, null);
});

dropzoneRecompensa.on("success", function(file, data){    
    console.log(data);
    $("#card-content").load("../../view/administracion/admin_rewards.php");
    swal('', 'Recompensas subidas correctamente', 'success');
});

