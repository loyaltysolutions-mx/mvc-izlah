$('.load').hide();


//INICIALIZAMOS DROPDOWN
$("#establecimientos").selectpicker();
 $('#desde').bootstrapMaterialDatePicker({ 
    format : 'YYYY-MM-DD',
    time : false,         
    lang : 'es', 
    weekStart : 1,
    okText : 'Aceptar',  
    cancelText : 'Cancelar'
  });
  //INICIALIZAMOS DATAPICKERS
  $('#desde').bootstrapMaterialDatePicker({ 
    format : 'YYYY-MM-DD',
    time : false,         
    lang : 'es', 
    weekStart : 1,
    okText : 'Aceptar',  
    cancelText : 'Cancelar'
  });
  
  $('#hasta').bootstrapMaterialDatePicker({ 
    format : 'YYYY-MM-DD',
    time : false,         
    lang : 'es', 
    weekStart : 1,
    okText : 'Aceptar',  
    cancelText : 'Cancelar'
  });

  $('#desde').change(function(){ 
    let value = $(this).val();                
    $('#hasta').bootstrapMaterialDatePicker('setMinDate', value);
    $('#hasta').val("");        
  });
  
 //FUNCIONALIDAD DE GENERAR REPORTE
 $( "#genera_cuadre" ).click(function() {
	  var id_establecimiento=$("#establecimientos").val();
	  var f_inicio=$("#desde").val();
	  var f_fin=$("#hasta").val();
	  var msj='';
	  
	  if(f_inicio==''){
		msj+='-Selecciona la fecha de inicio<br>';  
	  }
	  if(f_fin==''){
		msj+='-Selecciona la fecha de fin<br>';  
	  }
	  if(id_establecimiento==''){
		msj+='-Selecciona la sucursal<br>';  
	  }
	  if(msj==''){
		  $('.load').show();
			$.ajax({
			data:{
					op:13,
					id_establecimiento:id_establecimiento,
					f_inicio:f_inicio,
					f_fin:f_fin
				},
			url: '../../controller/administracion/c_llamadas_ajax.php',
			method:'post',
			beforeSend: function(response){

			},
			success: function(response){
				$('#resp_cuadre').html(response);
				$('.load').hide();
			}
		});
	  }else{
		   swal('Error!',msj,'error');
	  }
});