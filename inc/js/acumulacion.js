/*JS DE FORMULARIO DE ACUMULACION*/
$(document).ready(function() {
$('#loading').hide();
$('#acumulacion').hide();
$('#loading2').hide();
 //GUARDA DATOS DE REGISTRO  CON SESSION INICIADA 
 $('#valida_usuario').click(function(){
   var monto=$('#monto_ticket').val();
   if (!/^-?[0-9]+([,\.][0-9]*)?$/.test(monto)){
       swal("", "El monto no es un número correcto", "error");
       exit;
  }
   $('#loading').show();
   var cadena = $('#form_valida_usu').serialize();
   var id_usu=$('#id_usuario').val();
   var nom_usu_autoriza='';
   var pass_usu_autoriza='';
  $.ajax({
        url: '../../controller/acumulacion/c_llamadas_ajax.php',
        type: "POST",
        data:{ 
            op:1,
            cadena:cadena,
            id_usu:id_usu,
			nom_usu_autoriza:nom_usu_autoriza,
			pass_usu_autoriza:pass_usu_autoriza
            
         },
        success: function(resp)
            {  
                 $('#loading').hide();
                 if(resp==0){
                     swal("", "Todos los campos estan en blanco", "error");
                 }else if(resp==1){
                     swal("", "No se encontro Usuario con los datos ingresados", "error");
                 }else if(resp==2){
                     swal("", "El número y monto de ticket es obligatorio", "error");
                 }else{
                     swal("", "Se agrego el registro correctamente", "success");
                 }   
             $( "input[name='celular']" ).val( "" );
			 $( "input[name='id']" ).val( "" );
			 $( "input[name='mail']" ).val( "" );
			 $( "input[name='num_ticket']" ).val( "" );
			 $( "input[name='monto_ticket']" ).val( "" ); 
            }
            });   
   }); 
  //GUARDA DATOS DE REGISTRO  SIN SESSION INICIADA 
 $('#valida_usuario_sin_sesion').click(function(){
  var monto=$('#monto_ticket').val();
   if (!/^-?[0-9]+([,\.][0-9]*)?$/.test(monto)){
       swal("", "El monto no es un número correcto", "error");
       exit;
  }
   $('#loading').show();
   var cadena = $('#form_valida_usu').serialize();
   //var nom_usu_autoriza=$('#nom_usuario').val();
   var pass_usu_autoriza=$('#pass_usuario').val();
   var id_usu='';
  $.ajax({
        url: '../../controller/acumulacion/c_llamadas_ajax.php',
        type: "POST",
        data:{ 
            op:1,
            cadena:cadena,
            //nom_usu_autoriza:nom_usu_autoriza,
			pass_usu_autoriza:pass_usu_autoriza,
			id_usu:id_usu
			
            
         },
        success: function(resp)
            {  
                 $('#loading').hide();
                 if(resp==0){
                     swal("", "Todos los campos estan en blanco", "error");
                 }else if(resp==1){
                     swal("", "No se encontro Usuario con los datos ingresados", "error");
                 }else if(resp==2){
                     swal("", "El número y monto de ticket es obligatorio", "error");
                 }else{
					//SE PREGUNTA PASSWORD DEL PUNTO DE VENTA 
				swal("", "Se agrego el registro correctamente", "success");
                 }   
             $( "input[name='celular']" ).val( "" );
			 $( "input[name='id']" ).val( "" );
			 $( "input[name='mail']" ).val( "" );
			 $( "input[name='num_ticket']" ).val( "" );
			 $( "input[name='monto_ticket']" ).val( "" );
			 $( "input[name='nombre_usu']" ).val( "" );
			 $( "input[name='pass_usu']" ).val( "" );
			   
            }
            });   
   });    
   


   
    
    
});