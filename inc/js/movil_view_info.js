var modal1 = $("#modalPush");
var modal2 = $("#modalPromocion");
var object = {};

var divLoad = $("#divLoad");
divLoad.hide();

$("#divLoad1").hide();

$("#formAcumulacion").on('submit', function(e){
    e.preventDefault();
    $("#btnYes").attr("data-action", 1);
    //console.log("click");

    var mediaStream = null;
    var video = document.createElement("video");
    var canvasElement = document.getElementById("canvas");
    if(canvasElement != undefined || canvasElement != null){
        var canvas = canvasElement.getContext("2d");
        var loadingMessage = document.getElementById("loadingMessage");            
        function drawLine(begin, end, color) {
          canvas.beginPath();
          canvas.moveTo(begin.x, begin.y);
          canvas.lineTo(end.x, end.y);
          canvas.lineWidth = 4;
          canvas.strokeStyle = color;
          canvas.stroke();
        }
        // Use facingMode: environment to attemt to get the back camera on phones, user to get the front camera

        navigator.mediaDevices.getUserMedia({ 
            video: { 
                facingMode: "user",                    
            } 
        }).then(function(stream) {            
            
            mediaStream = stream;
            mediaStream.stop = function () {
                this.getAudioTracks().forEach(function (track) {
                    track.stop();
                });
                this.getVideoTracks().forEach(function (track) { //in case... :)
                    track.stop();
                });
            };
            video.srcObject = stream;
            video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
            video.play();
            requestAnimationFrame(tick);                    
        });
        
        function tick() {            
            if (video.readyState === video.HAVE_ENOUGH_DATA) {
                loadingMessage.hidden = true;
                canvasElement.hidden = false;                    
                canvasElement.height = video.videoHeight;
                canvasElement.width = video.videoWidth;
                canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
                var imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
                var code = jsQR(imageData.data, imageData.width, imageData.height, {
                  inversionAttempts: "invert",
                });
                if (code) {                   
                    //console.log("Code:"+JSON.stringify(code));
                    $("#inputPasswd").val(code.data);
                    mediaStream.stop();               
                    $("#formPasswd").submit();
                    
                }
            }
            requestAnimationFrame(tick);
        }

        $('#modalPush').on('hidden.bs.modal', function () {
            mediaStream.stop();
        });
    }

    modal1.modal("show");
});

$("#formPasswd").on("submit", function(e){
    e.preventDefault();
    //console.log("click");
    //var idUsuario = $("#inputIDUser").val();
    let action = $("#btnYes").attr("data-action");
    var link = null;
    var data = null;
    var beforeFunction = null;
    var successFunction = null;
    var failFunction = null;
    switch(action){
        case 1:
        case "1":
            var innerModal = $("#bodyModalPasswd").innerHTML;
            link = "../../controller/registro/c_llamadas_ajax.php";
            data = $("#formPasswd").serialize()+"&"+$("#formAcumulacion").serialize()+"&op=2";
            beforeFunction = function(){
                //$("#rowPasswd").hide();
                //console.log($("#bodyModalPasswd"));
                $(".divHide").hide();           
                $("#divLoad").show();
            };
            successFunction = function(data){

                modal1.modal("hide");
                switch(data.result){
                    case 1:
                        var canvasElement = document.getElementById("canvas");
                        if(canvasElement != undefined || canvasElement != null){                    
                            canvasElement.parentNode.removeChild(canvasElement);
                        }
                        var idUsuario1 = $("#formAcumulacion").find("[name='user']").val();
                        sendmsj("¡Has acumulado "+data.puntos+" puntos por tu compra!", "", $("#logo").attr("data-name"), "","Felicidades","", idUsuario1, 1);
                    break;

                    case 2:
                        showNotification("bg-orange", "Contraseña incorrecta, verifica que sea correcta e intenta nuevamente!", "bottom", "center", null, null);
                        $(".divHide").show();           
                        $("#divLoad").hide();
                    break;

                    case 3:
                        showNotification("bg-orange", "El ticket ingresado ya se encuentra registrado, verifica que la información del ticket sea correcta e intenta nuevamente!", "bottom", "center", null, null);
                        $(".divHide").show();
                        $("#divLoad").hide();
                    break;

                    default:
                        showNotification("bg-red", "Hubo un error al registrar la acumulacion, intenta nuevamente. Si el problema persiste, contacta al administrador", "bottom", "center", null, null);
                        $(".divHide").show();
                        $("#divLoad").hide();
                    break;
                }
            }

            failFunction = function(){
                modal1.modal("hide");
                showNotification("bg-red", "No se pudo procesar la solicitud. Verifica tu conexión a internet e intenta nuevamente!", "bottom", "center", null, null);
                $(".divHide").show();           
                $("#divLoad").hide();
            }
        break;

        case 2:
        case "2":
            link = "../../controller/registro/c_llamadas_ajax.php";
            data = $("#formPasswd").serialize()+'&user='+object.idUser+"&idRedencion="+object.idRedencion+"&op=3";
            //console.log(data);
            beforeFunction = function(){
                $(".divHide").hide();           
                $("#divLoad").show();                
            };
            successFunction = function(data){

                modal1.modal("hide");
                switch(data.result){
                    case 1:
                        var canvasElement = document.getElementById("canvas");
                        if(canvasElement != undefined || canvasElement != null){                    
                            canvasElement.parentNode.removeChild(canvasElement);
                        }
                        sendmsj("¡Tu redención se ha registrado correctamente. Has redimido "+data.producto[0].valor_puntos+" puntos por "+data.producto[0].nombre+"!", "", $("#logo").attr("data-name"), "","Felicidades","", object.idUser, 2);
                    break;

                    case 2:
                        showNotification("bg-orange", "No se pudo realizar la redencion, el usuario no ha completado su registro", "bottom", "center", null, null);
                        $(".divHide").show();           
                        $("#divLoad").hide();
                    break;

                    case 4:
                        showNotification("bg-orange", "Contraseña incorrecta, verifica que sea correcta e intenta nuevamente!", "bottom", "center", null, null);
                        $(".divHide").show();           
                        $("#divLoad").hide();
                    break;

                    default:
                        showNotification("bg-red", "Hubo un error al registrar la acumulacion, intenta nuevamente. Si el problema persiste, contacta al administrador", "bottom", "center", null, null);
                        $(".divHide").show();           
                        $("#divLoad").hide();
                    break;
                }

            }

            failFunction = function(){
                modal1.modal("hide");
                showNotification("bg-red", "No se pudo procesar la solicitud. Verifica tu conexión a internet e intenta nuevamente!", "bottom", "center", null, null);
                $(".divHide").show();           
                $("#divLoad").hide();
            }
        break;

        case 3:
        case "3":
            link = "../../controller/registro/c_llamadas_ajax.php";
            data = $("#formPasswd").serialize()+'&user='+object.idUser+"&idPromocion="+object.idPromo+"&op=4";                
            beforeFunction = function(){
                $(".divHide").hide();           
                $("#divLoad").show();                 
            };
            successFunction = function(data){
                modal1.modal("hide");
                if(data.result == 1){
                    var canvasElement = document.getElementById("canvas");
                    if(canvasElement != undefined || canvasElement != null){                    
                        canvasElement.parentNode.removeChild(canvasElement);
                    }
                    sendmsj("¡Se ha aplicado la promoción "+data.promocion[0].nombre+" correctamente!", "", $("#logo").attr("data-name"), "","Felicidades","Gratis", object.idUser, 1);
                }else{
                    showNotification("bg-red", "No se pudo aplicar la promoción. Verifica que la contraseña sea correcta e intenta nuevamente!", "bottom", "center", null, null);
                    $(".divHide").show();           
                    $("#divLoad").hide();
                }
            }

            failFunction = function(){
                modal1.modal("hide");
                showNotification("bg-red", "No se pudo procesar la solicitud. Verifica tu conexión a internet e intenta nuevamente!", "bottom", "center", null, null);
                $(".divHide").show();           
                $("#divLoad").hide();
            }
        break;
    }

    connectServer(link, data, beforeFunction, successFunction, failFunction);
    
    //console.log(idUsuario);
});

$(".btnRedencion").click(function(){
    let pass = $(this).attr("disabled");
    //console.log(pass);
    if(pass == undefined){
        object = {idUser : $(this).attr("data-id-user"), idRedencion : $(this).attr("data-id-redencion")};
        $("#btnYes").attr("data-action", 2);

        var mediaStream = null;
        var video = document.createElement("video");
        var canvasElement = document.getElementById("canvas");
        if(canvasElement != undefined || canvasElement != null){
            var canvas = canvasElement.getContext("2d");
            var loadingMessage = document.getElementById("loadingMessage");            
            function drawLine(begin, end, color) {
              canvas.beginPath();
              canvas.moveTo(begin.x, begin.y);
              canvas.lineTo(end.x, end.y);
              canvas.lineWidth = 4;
              canvas.strokeStyle = color;
              canvas.stroke();
            }
            // Use facingMode: environment to attemt to get the back camera on phones, user to get the front camera

            navigator.mediaDevices.getUserMedia({ 
                video: { 
                    facingMode: "user",                    
                } 
            }).then(function(stream) {
                
                mediaStream = stream;
                mediaStream.stop = function () {
                    this.getAudioTracks().forEach(function (track) {
                        track.stop();
                    });
                    this.getVideoTracks().forEach(function (track) { //in case... :)
                        track.stop();
                    });
                };
                video.srcObject = stream;
                video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
                video.play();
                requestAnimationFrame(tick);                    
            });
            
            function tick() {            
                if (video.readyState === video.HAVE_ENOUGH_DATA) {
                    loadingMessage.hidden = true;
                    canvasElement.hidden = false;                    
                    canvasElement.height = video.videoHeight;
                    canvasElement.width = video.videoWidth;
                    canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
                    var imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
                    var code = jsQR(imageData.data, imageData.width, imageData.height, {
                      inversionAttempts: "invert",
                    });
                    if (code) {                   
                        //console.log("Code:"+JSON.stringify(code));
                        $("#inputPasswd").val(code.data);
                        mediaStream.stop();          
                        $("#formPasswd").submit();
                    }
                }
                requestAnimationFrame(tick);
            }

            $('#modalPush').on('hidden.bs.modal', function () {
                mediaStream.stop();
            });
        }
        

        modal1.modal("show");
    }
});

$(".btnPromo").click(function(){
    let tipoPromo = $(this).attr("data-tipo-promo");
    var tipoCashback = $(this).attr("tipo-cashback");
    var valueCashBack = $(this).attr("data-cashback");
    var idPromo = $(this).attr("data-id-promo");
    var idUser = $(this).attr("data-id-user");

    $("#inputIDPromo").val(idPromo);
    $("#inputTipoPromo").val(tipoPromo);
    $("#inputIDUser").val(idUser);

    //$("#formPromocion")[0].reset();
    
    var modalBody = $("#bodyModalPromocion");    
    modalBody.empty();
    switch(tipoPromo){
        case 1:
        case "1":
            var spanCashBack = $("<p>",{text: "Monto Cashback a devolver "+(tipoCashback==1 ? "($"+valueCashBack+")" : "("+valueCashBack+"%)")+":"});
            var inputCashBack = $("<input>",{type:"number", readonly: "readonly", name:"montoCashback" }).css({width:"100%"});

            var spanMonto = $("<p>",{text:"Ingresa el monto de la compra:"});
            var monto = $("<input>",{type:"number", step: 0.01, required : "required", name:"monto"}).css({width:"100%"});
            var spanTicket = $("<p>",{text:"Ingresa el código del ticket:"});
            var ticket = $("<input>",{type:"text", required : "required", name:"ticket"}).css({width:"100%"});
            
            modalBody.append($("<div>",{class:"text-center"}).append(spanMonto).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class:"glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(monto)))));
            modalBody.append($("<div>",{class:"text-center"}).append(spanCashBack).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class: "glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(inputCashBack)))));            
            modalBody.append($("<div>",{class:"text-center"}).append(spanTicket).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class:"glyphicon glyphicon-barcode"})).append($("<div>",{class:"form-line"}).append(ticket)))));

            if(tipoCashback == 2){
                monto.keyup(function(){
                    let valueMonto = $(this).val();
                    let result = valueMonto*(valueCashBack/100);
                    inputCashBack.val(result.toFixed(2));
                });
            }else{
                inputCashBack.val(valueCashBack);
            }            
        break;

        case 2:
        case "2":
        case 4:
        case "4":
        case 5:
        case "5":
        case 8:
        case "8":
        case 9:
        case "9":
        case 10:
        case "10":
            var spanMonto = $("<p>",{text:"Ingresa el monto de la compra:"});
            var monto = $("<input>",{type:"number", step: 0.01, required : "required", name:"monto"}).css({width:"100%"});
            var spanTicket = $("<p>",{text:"Ingresa el código del ticket:"});
            var ticket = $("<input>",{type:"text", required : "required", name:"ticket"}).css({width:"100%"});

            modalBody.append($("<div>",{class:"text-center"}).append(spanMonto).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class:"glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(monto)))));
            //modalBody.append($("<div>",{class:"text-center"}).append(spanTicket).append("<br>").append(ticket));
            modalBody.append($("<div>",{class:"text-center"}).append(spanTicket).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class:"glyphicon glyphicon-barcode"})).append($("<div>",{class:"form-line"}).append(ticket)))));
        break;

        case 6:
        case "6":        
            var valueDesc = $(this).attr("data-desc");
            var spanDesc = $("<p>",{text: "Ingrese el monto con descuento ("+valueDesc+"%)"});
            var montoDesc = $("<input>",{type:"number", step: 0.01, required : "required", name:"montoDesc", min:1 }).css({width:"100%"});
            var spanMonto = $("<p>",{text:"Ingresa el monto de la compra real:"});
            var monto = $("<input>",{type:"number", step: 0.01, required : "required", name:"monto", min:1}).css({width:"100%"});
            var spanTicket = $("<p>",{text:"Ingresa el código del ticket:"});
            var ticket = $("<input>",{type:"text", required : "required", name:"ticket"}).css({width:"100%"});
            
            modalBody.append($("<div>",{class:"text-center"}).append(spanMonto).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class:"glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(monto)))).append($("<h5>").text("ó")).append(spanDesc).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class: "glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(montoDesc)))));
            modalBody.append($("<div>",{class:"text-center"}).append($("<hr>")));
            //modalBody.append($("<div>",{class:"text-center"}).append(spanDesc).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class: "glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(montoDesc)))));            
            modalBody.append($("<div>",{class:"text-center"}).append(spanTicket).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class:"glyphicon glyphicon-barcode"})).append($("<div>",{class:"form-line"}).append(ticket)))).append($("<hr>")));
            
            monto.keyup(function(){
                let valueMonto = $(this).val();
                let result = valueMonto*(1-(valueDesc/100));
                montoDesc.val(result.toFixed(2));
            });

            montoDesc.keyup(function(){
                let value = $(this).val();
                let result = value/(1-(valueDesc/100));
                monto.val(result.toFixed(2));
            });            
        break;

        case 3:
        case "3":
            var valueDesc = $(this).attr("data-desc");
            var spanDesc = $("<p>",{text: "Ingrese el monto con descuento ($"+valueDesc+")"});
            var montoDesc = $("<input>",{type:"number", step: 0.01, required : "required", name:"montoDesc", min:1 }).css({width:"100%"});
            var spanMonto = $("<p>",{text:"Ingresa el monto de la compra real:"});
            var monto = $("<input>",{type:"number", step: 0.01, required : "required", name:"monto", min:valueDesc}).css({width:"100%"});
            var spanTicket = $("<p>",{text:"Ingresa el código del ticket:"});
            var ticket = $("<input>",{type:"text", required : "required", name:"ticket"}).css({width:"100%"});
            
            modalBody.append($("<div>",{class:"text-center"}).append(spanMonto).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class:"glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(monto)))).append($("<h5>").text("ó")).append(spanDesc).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class: "glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(montoDesc)))));
            modalBody.append($("<div>",{class:"text-center"}).append($("<hr>")));
            //modalBody.append($("<div>",{class:"text-center"}).append(spanDesc).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class: "glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(montoDesc)))));            
            modalBody.append($("<div>",{class:"text-center"}).append(spanTicket).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class:"glyphicon glyphicon-barcode"})).append($("<div>",{class:"form-line"}).append(ticket)))).append($("<hr>")));
            
            monto.keyup(function(){
                valueMonto = $(this).val();
                result = valueMonto-valueDesc;
                montoDesc.val(result.toFixed(2));
            });

            montoDesc.keyup(function(){
                value = $(this).val();
                result = (value-0)+(valueDesc-0);
                monto.val(result.toFixed(2));
            });
        break;

    }

    var mediaStream = null;
    var video = document.createElement("video");
    var canvasElement = document.getElementById("canvas");
    if(canvasElement != undefined || canvasElement != null){
        var canvas = canvasElement.getContext("2d");
        var loadingMessage = document.getElementById("loadingMessage");            
        function drawLine(begin, end, color) {
          canvas.beginPath();
          canvas.moveTo(begin.x, begin.y);
          canvas.lineTo(end.x, end.y);
          canvas.lineWidth = 4;
          canvas.strokeStyle = color;
          canvas.stroke();
        }
        // Use facingMode: environment to attemt to get the back camera on phones, user to get the front camera

        navigator.mediaDevices.getUserMedia({ 
            video: { 
                facingMode: "user",                    
            } 
        }).then(function(stream) {            
            
            mediaStream = stream;
            mediaStream.stop = function () {
                this.getAudioTracks().forEach(function (track) {
                    track.stop();
                });
                this.getVideoTracks().forEach(function (track) { //in case... :)
                    track.stop();
                });
            };
            video.srcObject = stream;
            video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
            video.play();
            requestAnimationFrame(tick);                    
        });
        
        function tick() {            
            if (video.readyState === video.HAVE_ENOUGH_DATA) {
                loadingMessage.hidden = true;
                canvasElement.hidden = false;                    
                canvasElement.height = video.videoHeight;
                canvasElement.width = video.videoWidth;
                canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
                var imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
                var code = jsQR(imageData.data, imageData.width, imageData.height, {
                  inversionAttempts: "invert",
                });
                if (code) {              
                    //console.log("Code:"+JSON.stringify(code));
                    $("#inputPasswdPromo").val(code.data);
                    mediaStream.stop();          
                    $("#formPromocion").submit();
                }
            }
            requestAnimationFrame(tick);
        }

        $('#modalPromocion').on('hidden.bs.modal', function () {
            mediaStream.stop();
        });
    }

    modal2.modal("show");
});

$("#formPromocion").on("submit", function(e){
    e.preventDefault();

    var idUsuario = $("#inputIDUser").val();
    var link = "../../controller/registro/c_llamadas_ajax.php";
    var data = $("#formPromocion").serialize()+"&op=4";
    var beforeFunction = function(){
        $("#rowPasswd").hide();
        $("#divLoad1").show();
        $("#bodyModalPromocion").hide();
        console.log(idUsuario);
    };

    var successFunction = function(data){
        modal2.modal("hide");
        switch(data.result){
            case 1:
                var canvasElement = document.getElementById("canvas");
                if(canvasElement != undefined || canvasElement != null){                    
                    canvasElement.parentNode.removeChild(canvasElement);
                }
                sendmsj("¡Se ha aplicado la promoción "+data.promocion[0].nombre+" correctamente!","", $("#logo").attr("data-name"), "","Felicidades","Gratis", idUsuario, 1);
            break;
            
            case 2:
                showNotification("bg-orange", "El ticket ingresado ya se encuentra registrado, verifica que la información del ticket sea correcta e intenta nuevamente!", "bottom", "center", null, null);
                $("#bodyModalPromocion").show();
                $("#rowPasswd").show();
                $("#divLoad1").hide();
            break;

            case 3:
                showNotification("bg-orange", "Contraseña incorrecta, verifica que sea correcta e intenta nuevamente!", "bottom", "center", null, null);
                $("#bodyModalPromocion").show();
                $("#rowPasswd").show();
                $("#divLoad1").hide();
            break;

            default:
                showNotification("bg-red", "Hubo un error al registrar la acumulacion, intenta nuevamente. Si el problema persiste, contacta al administrador", "bottom", "center", null, null);
                $("#bodyModalPromocion").show();
                $("#rowPasswd").show();
                $("#divLoad1").hide();
            break;
        }  
        //modal2.modal("show");
    }

    var failFunction = function(){
        modal2.modal("hide");
        $("#rowPasswd").show();
        showNotification("bg-red", "No se pudo procesar la solicitud. Verifica tu conexión a internet e intenta nuevamente!", "bottom", "center", null, null);
        $("#bodyModalPromocion").show();
        $("#divLoad1").hide();
        $("#rowPasswd").show();
    }

    connectServer(link, data, beforeFunction, successFunction, failFunction);
    //console.log(idUsuario);
});

function showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
    //console.log("click");
    if (colorName === null || colorName === '') { colorName = 'bg-black'; }
    if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
    if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
    if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
    var allowDismiss = true;

    $.notify({
      message: text
    },{
    type: colorName,
    allow_dismiss: allowDismiss,
    newest_on_top: true,
    timer: 1000,
    placement: {
        from: placementFrom,
        align: placementAlign
    },
    animate: {
        enter: animateEnter,
        exit: animateExit
    },
    template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
    '<span data-notify="icon"></span> ' +
    '<span data-notify="title">{1}</span> ' +
    '<span data-notify="message">{2}</span>' +
    '<div class="progress" data-notify="progressbar">' +
    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
    '</div>' +
    '<a href="{3}" target="{4}" data-notify="url"></a>' +
    '</div>'
    });
}

function connectServer(link, data, beforeFunction, successFunction, failFunction){
    $.ajax({
        type: "POST",
        timeout: 20000,
        url: link,
        data: data,
        dataType: "json",
        beforeSend : function(){
            beforeFunction && beforeFunction();
        }
    }).done(function(data){
        console.log(data);
        successFunction && successFunction(data);
    }).fail(function(data){
        failFunction && failFunction(data);
        console.log(data);
    });
}

function sendmsj(msj,username,logo,submsj,congrats,orangetxt,idUsuario,opt){
    
    $.ajax({
        data:{ msj:msj, username:username, logo:logo, submsj:submsj, congrats:congrats, orangetxt:orangetxt, user : idUsuario, opt:opt },
        url: 'msj.php',
        method:'post',
        beforeSend: function(response){
            console.log(idUsuario);
        },
        success: function(response){
            $('#card-content').html(response);
        }
    });
}
