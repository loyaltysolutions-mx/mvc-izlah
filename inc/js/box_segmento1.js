function BoxSegmento(indice) {    
    
    var arrayCategoria = new Array();
    this.tagHTML = createBoxHTML(arrayCategoria, false, true, true, 1)[0];
    this.indice = indice;    
    var selectObject = null;

    function createBoxHTML(objeto, flagRow, flagBtn, flagBtnAdd, typeRow){
        let divContainer = $("<div>");
        
        var btnDeleteCategoria = $("<a>",{'href' : 'javascript:;', class : 'btn', style : "box-shadow: none; font-weight:bold; color:gray", "data-index" : indice }).append($("<i>",{class:"fas fa-trash-alt"}));
        let divRowBtnCategoria = $("<div>",{'class' : 'col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right'}).append(btnDeleteCategoria);
        
        let col = $("<div>",{'class':'col-lg-12 col-md-12 col-sm-12 col-xs-12'});
        
        var totalContainer = $("<div>",{class: "row boxSegmento"});
        
        let col1 = $("<div>",{'class':'col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right'});
        let divAND = $("<div>",{class: "row boxAnd"});

        $.each(objeto, function(key,obj){
            let divRowAND = $("<div>",{'class':'row', style : "margin-top:8px"}).append( $("<div>",{'class':'col-lg-5 col-md-5 col-sm-5 col-xs-5'}).append($("<hr>",{style : "border: 1px solid gray"})) ).append($("<div>",{'class':'col-lg-2 col-md-2 col-sm-2 col-xs-2'}).append($("<a>",{class:'btn btn-block primary-back', style: "color: white"}).text(typeRow == 1 ? "AND" : "OR")) ).append($("<div>",{'class':'col-lg-5 col-md-5 col-sm-5 col-xs-5'}).append($("<hr>",{style : "border: 1px solid gray"})));
            let row = $("<div>",{'class':'row align-items-center'});             
            let divBoxAND = $("<div>",{'class':'row'}); 
            let colHeader = $("<div>",{class : (flagBtn ? "col-lg-6 col-md-6 col-sm-6 col-xs-6" : "col-lg-12 col-md-12 col-sm-12 col-xs-12") }).append('<p style="padding:8px"><b>Nueva Condición:</b></p>');
            let colContainer = $("<div>",{class : "col-lg-12 col-md-12 col-sm-12 col-xs-12", style: "padding:15px"});
            selectObject = $("<select>",{'name': 'categoria[box]['+indice+'][]', required:"required", title : " -- Elige una categoría -- ", "data-show-subtext":true, "data-width":"100%"});
            let divDetails = $("<div>",{class : 'col-lg-12 col-md-12 col-sm-12 col-xs-12'});
            
        
            //console.log(obj.getAttributes()[0]);
            $.each(obj.getAttributes()[0], function(key,category){
                selectObject.append($("<option>",{value:category.id_categoria, text:category.nombre_categoria, "data-subtext": category.descripcion_categoria}));
            });
        //});

            selectObject.change(function(){
                let valSelect = $(this).val();
                var result = obj.elementos[0].filter(function(v) {
                    //console.log(v);
                    return v.id_categoria == valSelect; // Filter out the appropriate one
                });
                //console.log(result);
                if(result.length > 0){
                    divDetails.empty();
                    obj.setIDCategoria(valSelect-0);
                    obj.clearInputs();
                    $.each(result[0].inputs, function(key,obj){
                        switch(obj.id_objeto){                        
                            case "1":
                            case "2":
                            case "3":                            
                                let input = $("<input>",{type: obj.tipo, required:true, class:"form-control", min: obj.tipo == "number" ? 1 : "", value:obj.tipo == "number" ? 1 : ""});
                                var inputGroup = $("<div>",{ class : "input-group"});
                                var addOn = $("<span>",{ class:"input-group-addon" }).append($("<i>",{class:"material-icons", text:valSelect == 1 ? "cake" : (valSelect == 2 ? "date_range" : "today")}));
                                
                                divDetails.append($("<p>").html("<b>"+obj.label+":</b>"));
                                divDetails.append(inputGroup.append(addOn).append($("<div>",{class:"form-line"}).append(input)));
                                
                                input.on("change",function(){
                                    var aux = {};
                                    aux["value"] = $(this).val();
                                    aux["name"] = obj.name;
                                    aux["id_objeto"] = obj.id_objeto;
                                    obj.clearInputs();
                                    obj.insertInput(aux);
                                    obj.setIDRelacion(obj.id_rel-0);
                                });
                            break;                        
                            case "4":
                            case "5":
                                let select = $("<select>",{ "data-live-search":true, title: valSelect == 5 ? " -- Elige uno o varios Puntos de Venta -- " : (valSelect == 6 ? " -- Elige uno o varios Clientes -- " : " -- Elige un género -- "), "data-width":"100%"});
                                obj.tipo != "" && obj.tipo != null && select.attr("multiple",obj.tipo);
                                select.empty();
                                //select.append($("<option>",{value:"", text: valSelect == 5 ? " -- Elige uno o varios Puntos de Venta -- " : " -- Elige uno o varios Clientes -- "}));                            
                                $.each(obj.container, function(key, value){
                                    select.append($("<option>",{value: value.id_usuario, text: valSelect == 5 ? value.usuario : value.nombre}));    
                                });

                                var inputGroup = $("<div>",{ class : "input-group"});
                                var addOn = $("<span>",{ class:"input-group-addon" }).append($("<i>",{class:"material-icons", text:valSelect == 5 ? "pin_drop" : "person"}));
                                divDetails.append($("<p>").html("<b>"+obj.label+":</b>"));                            
                                divDetails.append(inputGroup.append(addOn).append($("<div>",{class:"form-line"}).append(select)));
                                //divDetails.append();
                                divDetails.find(select).selectpicker();

                                select.change(function(){                                
                                    var aux = {};
                                    let selectItems = $(this).val();                                

                                    aux['valores'] = selectItems.length ? $(this).val() : $(this).val()-0;
                                    aux["name"] = obj.name;
                                    aux["id_objeto"] = obj.id_objeto;
                                    obj.clearInputs();
                                    obj.insertInput(aux);
                                    obj.setIDRelacion(obj.id_rel-0);
                                });
                            break;

                            case "6":
                            case "7":
                            case "8":
                            case "9":
                                let inputMin = $("<input>",{type: obj.tipo == "date" ? "text" : (obj.tipo == "money" ? "number" : obj.tipo), step: (obj.tipo == "money" || obj.tipo == "number" ? 0.01 : ""), required:true, class:"form-control", placeholder : "Valor mínimo", min: obj.tipo == "number" ? 1 : (obj.tipo == "money" ? 0.01 : ""), value: obj.tipo == "number" || obj.tipo == "money" ? "1.00" : ""});
                                var inputGroupMin = $("<div>",{ class : "input-group"});
                                var addOnMin = $("<span>",{ class:"input-group-addon" }).append($("<i>",{class:"material-icons", text: obj.tipo == "date" ? "date_range" : (obj.tipo == "number" ? "expand_more" :  (obj.tipo == "money" ? "attach_money" : "access_time"))}));

                                let inputMax = $("<input>",{type: obj.tipo == "date" ? "text" : (obj.tipo == "money" ? "number" : obj.tipo), step: (obj.tipo == "money" || obj.tipo == "number" ? 0.01 : ""), required:true, class:"form-control", placeholder : "Valor máximo", min: obj.tipo == "number" ? 1 : (obj.tipo == "money" ? 0.01 : ""), value:obj.tipo == "number" || obj.tipo == "money" ? "1.00" : ""});
                                var inputGroupMax = $("<div>",{ class : "input-group"});
                                var addOnMax = $("<span>",{ class:"input-group-addon" }).append($("<i>",{class:"material-icons", text: obj.tipo == "date" ? "date_range" : (obj.tipo == "number" ? "expand_less" :  (obj.tipo == "money" ? "attach_money" : "access_time"))}));
                                
                                divDetails.append($("<p>").html("<b>"+obj.label+":</b>"));
                                divDetails.append(inputGroupMin.append(addOnMin).append($("<div>",{class:"form-line"}).append(inputMin)));
                                divDetails.append(inputGroupMax.append(addOnMax).append($("<div>",{class:"form-line"}).append(inputMax)));

                                if(obj.tipo == "date"){
                                    inputMin.bootstrapMaterialDatePicker({ 
                                        format : 'YYYY-MM-DD',
                                        time : false,         
                                        lang : 'es', 
                                        weekStart : 1,
                                        okText : 'Aceptar',  
                                        cancelText : 'Cancelar'
                                    });

                                    inputMax.bootstrapMaterialDatePicker({ 
                                        format : 'YYYY-MM-DD',
                                        time : false,         
                                        lang : 'es', 
                                        weekStart : 1,
                                        okText : 'Aceptar',  
                                        cancelText : 'Cancelar'
                                    });
                                }

                                inputMin.change(function(){
                                    let valueMin = $(this).val();
                                    obj.tipo == "date" && inputMax.bootstrapMaterialDatePicker('setMinDate', valueMin);
                                    (obj.tipo == "money" || obj.tipo == "number" ) && inputMax.attr('min', valueMin);
                                    inputMax.val("");
                                });

                                inputMax.change(function(){
                                    let valueMax = $(this).val();
                                    if(inputMin.val() != "" && valueMax != ""){
                                        var aux = {};
                                        aux["value"] = {min: (obj.tipo == "money" || obj.tipo == "number") ? inputMin.val().replace(",",".")-0 : inputMin.val(), max: (obj.tipo == "money" || obj.tipo == "number") ? valueMax.replace(",",".")-0 : valueMax };
                                        aux["name"] = obj.name;
                                        aux["id_objeto"] = obj.id_objeto;
                                        obj.clearInputs();
                                        obj.insertInput(aux);
                                        obj.setIDRelacion(obj.id_rel-0);
                                    }
                                });

                                /*input.on("change",function(){
                                    var aux = {};
                                    aux["value"] = $(this).val();
                                    aux["name"] = obj.name;
                                    aux["id_objeto"] = obj.id_objeto;                                
                                    objeto[0].insertInput(aux);                                
                                });*/
                            break;
                        }
                    });
                }
            });

            var headerBox = $("<div>",{'class':'row align-items-center primary-back'}).append(colHeader);
            flagBtn && headerBox.append(divRowBtnCategoria);

            let inputGroupCategory = $("<div>",{ class : "input-group"});
            let addOnCategory = $("<span>",{ class:"input-group-addon" }).append($("<i>",{class:"material-icons", text: "category"}));        
            colContainer.append(inputGroupCategory.append(addOnCategory).append($("<div>",{class:"form-line"}).append(selectObject)));
            //colContainer.append(selectObject);
            colContainer.find(selectObject).selectpicker();
            

            let containerSegmento = $("<div>",{'class':'col-lg-12 col-md-12 col-sm-12 col-xs-12'}).append(headerBox).append($("<div>",{'class':'row align-items-center'}).append(colContainer)).append($("<div>",{'class':'row align-items-center'}).append(divDetails));
            flagRow && divContainer.append(divRowAND);
            //divAND.append(containerSegmento);
            divAND.append(col.append(totalContainer));          

            btnDeleteCategoria.on("click",function(){
                arrayCategoria.splice(obj.getIndice()-1, 1);
                //console.log(obj.getIndice());
                $.each(arrayCategoria, function(key,value){
                    value.setIndice(key);
                });
                divContainer.remove();
            });


            
        });
        let btnAdd = $("<a>",{href:"javascript:;", class : "btn btn_color btnORCondition"}).text("Agregar condición OR");    
        let boxAdd = $("<div>",{'class':'col-lg-12 col-md-12 col-sm-12 col-xs-12'});
        
        if(objeto.length > 0){
            btnAdd.click(function(){
                var cnt = arrayCategoria.length;
                var categoria = new Categoria(objeto[0].elementos[0], true, true, cnt);
                var aux = createBoxHTML([categoria],true, true, false, 2);
                boxAdd.append(aux);
                //console.log(categoria);
                arrayCategoria.push(categoria);
                //console.log(arrayCategoria);
                //activeBtnDelete(btnDeleteCategoria, objeto);
            });

        }
        
        
        
        divContainer.append(divAND.append(boxAdd));
        flagBtnAdd && divAND.append(col1.append(btnAdd));

        return divContainer;
    }


    this.insertCategoria = function(categoria, flagRow, flagBtn, flagBtnAdd, typeRow){
        arrayCategoria.push(categoria);        
        //this.tagHTML = createBoxHTML(arrayCategoria, false, false, true, 1)[0];
        this.tagHTML = createBoxHTML(arrayCategoria, flagRow, flagBtn, flagBtnAdd, typeRow)[0];
        //console.log(arrayCategoria);
        return true;
    };

    this.getCategorias = function(index){
        return (index ? arrayCategoria[index] : arrayCategoria) ;
    };

    this.getHTML = function(){
        return this.tagHTML;
    };

    this.getIndex = function(){
        return this.indice;
    };

    this.getCountCategorias = function(){
        //lengthCount = arrayCategoria.length;
        return arrayCategoria.length;
    };

    this.setSelect = function(values){
        selectObject.val(values);
        return true;
    }

    this.setIndex = function(newIndex){
        this.indice = newIndex;
        return true;
    };
}