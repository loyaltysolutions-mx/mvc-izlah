//saves img database
function preloader(){
    $('.page-loader-wrapper').empty();
}

function setfile(id,file){
    //console.log(id+':'+file);
    var op = 5;
    $.ajax({
        data:{ op:op, id:id, file:file },
        url: '../../controller/configuracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            //swal(" ", "Producto Agregado", "success");
        }
    });
}

function cleanfile(id){
    $('#filename').attr('my',id);
    $('#frmFileUpload').removeClass('dz-started'); 
    $(".dz-preview").remove();
    
}
//functions to products and configuration (edit,delete,etc..)
function upreward(option){
    var reward = $('#reward').val();
    var filename = $('#filename').val();
    var description = $('#descript').val();
    var cant = $('#cant').val();
    var points = $('#points').val();
    var op = 9;


    $.ajax({
        data:{ op: op, option: option, reward: reward, filename: filename, description: description, cant: cant, points: points },
        url: '../../controller/configuracion/c_llamadas_ajax2.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            swal(" ", "Producto Agregado", "success");
        }
    });
}
function uprew2(option){
    var reward = $('#reward2').val();
    var filename = $('#filename2').val();
    var description = $('#descript2').val();
    var cant = $('#cant2').val();
    var points = $('#points2').val();
    var op = 9;


    $.ajax({
        data:{ op: op, option: option, reward: reward, filename: filename, description: description, cant: cant, points: points },
        url: '../../controller/configuracion/c_llamadas_ajax2.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            cleanfile();
            $('#reward2').val('');
            $('#filename2').val('');
            $('#descript2').val('');
            $('#points2').val('');
            swal(" ", "Producto Agregado", "success");
        }
    });
}

function uprew(option){
    var reward = $('#reward').val();
    var filename = $('#filename').val();
    var description = $('#descript').val();
    var cant = $('#cant').val();
    var points = $('#points').val();
    var op = 9;


    $.ajax({
        data:{ op: op, option: option, reward: reward, filename: filename, description: description, cant:cant, points: points },
        url: '../../controller/configuracion/c_llamadas_ajax2.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            cleanfile();
            $('#reward').val();
            $('#filename').val();
            $('#descript').val();
            $('#points').val();
            swal(" ", "Producto Agregado", "success");
        }
    });
}

function deleterw(id){
    var op = 6;

    $.ajax({
        data:{ op:op, id:id },
        url: '../../controller/configuracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                swal(" ", "Producto Eliminado", "success");
                location.reload();
            }else{
                swal('Error!','El producto no pudo eliminarse','warning');
            }
            
        }
    });
}

function editrw(id){
    var name = $('#n'+id).val();
    var pts = $('#p'+id).val();
    var visits = $('#v'+id).val();
    var details = $('#d'+id).val();
    var cant = $('#c'+id).val();
    var op = 4;

    $.ajax({
        data:{ op:op, id:id, name:name, pts:pts, visits:visits, details:details, cant:cant },
        url: '../../controller/configuracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            swal(" ", "Producto Modificado", "success");
        }
    });
}

function deletepto(id){
    var op = 7;

    $.ajax({
        data:{ op:op, id:id },
        url: '../../controller/configuracion/c_llamadas_ajax2.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                swal(" ", "Producto Eliminado", "success");
            }else{
                swal('Error!','El producto no pudo eliminarse','warning');
            }
            
        }
    });
}

function editpto(id){
    var name = $('#nm'+id).val();
    var op = 8;

    $.ajax({
        data:{ op:op, id:id, name:name },
        url: '../../controller/configuracion/c_llamadas_ajax2.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            swal(" ", "Producto Modificado", "success");
        }
    });
}

/*function menus(id){
    var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
    $.ajax({
        data:{id:id},
        url: '../'+id+'.php',
        method:'post',
        beforeSend: function(response){
            $('#card-content').html(div);
        },
        success: function(response){
            $('#card-content').html(response);
            preloader();
        }
    });
}*/
//
function menu(id){
    var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
    $.ajax({
        data:{id:id},
        url: '../'+id+'.php',
        method:'post',
        beforeSend: function(){
            //console.log("click");
            $('#card-content').html(div);
            sessionStorage.setItem("url",id);
        },
        success: function(response){
            $('#card-content').html('');
            $('#card-content').html(response);
            preloader();
        },
        error: function(){
            $('#card-content').html();
            preloader();
        },
        complete: function() {
         preloader();
        }
    });
}

$(document).ready(function(){

    $("#btnIndex").click(function(){
        sessionStorage.removeItem("url");
        window.location = "index.php";
    });

    
    var url = sessionStorage.getItem("url");
    if(url != null && url != undefined){
        
        var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';        
        $.ajax({            
            url: '../'+url+'.php',
            method:'post',
            beforeSend: function(){
                //console.log("click");                
                $('#card-content').html("");
                sessionStorage.setItem("url",url);
            },
            success: function(response){

                //$('#card-content').html(''); 
                $('#card-content').html(response);
                //preloader();
            },
            error: function(){
                $('#card-content').html();
                preloader();
            },
            complete: function() {
                preloader();
            }
        });    
    }
    
});

function shownew(id){
    $('#'+id).toggle();
}

//other functions 

function selectedinfo(data){
    var obj = JSON.parse(data);
    //console.log(obj);
    
    for (var i = 0; i < obj.length; i++){
        
        var logo_cte = obj[i].logo_cte;
        var fondo_cte = obj[i].fondo_cte;
        var color_primario = obj[i].color_primario;
        var color_secundario = obj[i].color_secundario;
        //
        var autoregistro = obj[i].id_tipo_registro; //autoregistro

        var id_campo = obj[i].id_campo;
        var posicion = obj[i].posicion;
        var clave = obj[i].clave;

        var id_estilo = obj[i].id_estilo;
        var id_forma_acumulacion = obj[i].id_forma_acumulacion;
        var id_tipo_proyecto = obj[i].id_tipo_proyecto;
        var id_mecanica_redencion = obj[i].id_mecanica_redencion;
        var id_tipo_redencion = obj[i].id_tipo_redencion;
        var bono_bienvenida = obj[i].bono_bienvenida;
        var bono_cumple = obj[i].bono_cumple;
        var id_tipo_comunicacion_registro = obj[i].id_tipo_comunicacion_registro;
        var id_tipo_comunicacion_acumular = obj[i].id_tipo_comunicacion_acumular;
        var id_tipo_comunicacion_redimir = obj[i].id_tipo_comunicacion_redimir;
        var id_tipo_comunicacion_periodica = obj[i].id_tipo_comunicacion_periodica;
        var id_lapso_comunicacion_periodica = obj[i].id_lapso_comunicacion_periodica;

        var puntos_por_visita = obj[i].puntos_por_visita;
        var puntos_por_peso = obj[i].puntos_por_peso;
        var puntos_bienvenida = obj[i].puntos_bienvenida;
        var puntos_cumple = obj[i].puntos_cumple;
        var porcentaje_margen = obj[i].porcentaje_margen;
        var sucursales = obj[i].sucursales;
        var menus = obj[i].menus;
        var promos = obj[i].promociones;
        var niveles = obj[i].niveles;
        var app = obj[i].app;
        
        var title = obj[i].titulo_home;
        var detail = obj[i].detalle_home;
        var tel = obj[i].tel_marca;
        var email = obj[i].mail_marca;
        var face = obj[i].cta_face;
        var twit = obj[i].cta_twit;

        var nsucursales = obj[i].nsucursales;

        //campos seleccionados para Registro
        $("#camposregistro_"+id_campo).prop("checked", true);
        $("#camposregistro_"+id_campo).removeAttr("disabled");
        camporeg(id_campo);
        //Forma de Acumulación
        $("#tipoacumilacion_"+id_forma_acumulacion).prop("checked", true);
        $("#tipoacumilacion_"+id_forma_acumulacion).prop("disabled", false);
        //posición de campo/registro
        $("#pos_"+id_campo).val(posicion);
        //clave de campo/registro
        $("#campoclave_"+id_campo).removeAttr("disabled");
        if(clave=='1'){
            $("#campoclave_"+id_campo).prop("checked", true);
        }
        
        //Mecánica redención
        $("#mecanicaredencion_"+id_mecanica_redencion).removeAttr("disabled");
        $("#mecanicaredencion_"+id_mecanica_redencion).prop("checked", true);
        //tipo Redención
        $("#tiporedencion_"+id_tipo_redencion).prop("checked", true);
        if(id_tipo_redencion==2 || id_tipo_redencion==4){
            $("#modalr").removeAttr("disabled");
            $("#editmodal").removeAttr("disabled");
        }else{}
        
        
        
    }

    $('#title').val(title);
    $('#detail').val(detail);
    $('#tel').val(tel);
    $('#email').val(email);
    $('#face').val(face);
    $('#twit').val(twit);
    $('#nsucursales').val(nsucursales);

    //campos seleccionados para Niveles
    $("#lvl"+niveles).prop("checked", true);

    //campos seleccionados para Promociones
    $("#promo"+promos).prop("checked", true);

    //campos seleccionados para Sucursales
    $("#suc"+sucursales).prop("checked", true);

    //campos seleccionados para Menus
    $("#mnu"+menus).prop("checked", true);

    $("#app"+app).prop("checked", true);

    $("#puntos_por_visita").val(puntos_por_visita);
    $("#num_puntos_peso").val(puntos_por_peso);
    $("#por_cant_vendida").val(porcentaje_margen);

    $("#registro_"+autoregistro).prop("checked", true);
    //¿Cúal será el Tipo  de Acumulación?
    $("#tipoproyecto_"+id_tipo_proyecto).prop("checked", true);
    if(id_tipo_proyecto==3){
        $("#modalr").attr("data-target", "#premios2");
    }else{
        $("#modalr").attr("data-target", "#premios"+id_tipo_proyecto);
    }
    tipoproy(id_tipo_proyecto);


    
    //bono bienvenida
    if(bono_bienvenida==1){
        $("#si_bono").prop("checked", true);
        $("#ptos_bienvenida").val(puntos_bienvenida);
    }else{
        $("#no_bono").prop("checked", true);
    }
    

    if(bono_cumple==1){
        $("#si_cumple_anios").prop("checked", true);
        $("#ptos_cumple").removeAttr("disabled");
        $("#ptos_cumple").val(puntos_cumple);
    }else{
        $("#no_cumple_anios").prop("checked", true);
    }

    var chain = id_tipo_comunicacion_registro;
    var chain2 = id_tipo_comunicacion_acumular;
    var chain3 = id_tipo_comunicacion_redimir;
    var chain4 = id_tipo_comunicacion_periodica;
    
        if(chain.indexOf('1') != -1){
            var find = "1";
            //Tipo Comunicación al Registrarse
            $("#tipo_comunicacion_registro"+find).removeAttr("disabled");
            $("#tipo_comunicacion_registro"+find).prop("checked", true); 
        }

        if(chain.indexOf('2') != -1){
            var find2 = "2";
            //Tipo Comunicación al Registrarse
            $("#tipo_comunicacion_registro"+find2).removeAttr("disabled");
            $("#tipo_comunicacion_registro"+find2).prop("checked", true);
        }

        if(chain2.indexOf('1') != -1){
            var find = "1";
            //Tipo Comunicación al acumular
            $("#tipocomunicacion_"+find).removeAttr("disabled");
            $("#tipocomunicacion_"+find).prop("checked", true);
        }

        if(chain2.indexOf('2') != -1){
            var find2 = "2";
            //Tipo Comunicación al acumular
            $("#tipocomunicacion_"+find2).removeAttr("disabled");
            $("#tipocomunicacion_"+find2).prop("checked", true);
        }

        if(chain3.indexOf('1') != -1){
            var find = "1";
            //Tipo Comunicación al redimir
            $("#tipocomunicacionredimir_"+find).removeAttr("disabled");
            $("#tipocomunicacionredimir_"+find).prop("checked", true);
        }

        if(chain3.indexOf('2') != -1){
            var find2 = "2";
            //Tipo Comunicación al redimir
            $("#tipocomunicacionredimir_"+find2).removeAttr("disabled");
            $("#tipocomunicacionredimir_"+find2).prop("checked", true);
        }

        if(chain4.indexOf('1') != -1){
            var find = "1";
            //Tipo Comunicación Periódica
            $("#tipocomunicacionperiodica_"+find).removeAttr("disabled");
            $("#tipocomunicacionperiodica_"+find).prop("checked", true);
        }

        if(chain4.indexOf('2') != -1){
            var find2 = "2";
            //Tipo Comunicación Periódica
            $("#tipocomunicacionperiodica_"+find2).removeAttr("disabled");
            $("#tipocomunicacionperiodica_"+find2).prop("checked", true);
        }

   
    bono(bono_bienvenida); 

    //Lapso de Comunicación Periódica
    $("#mediocomunicacionperiodica_"+id_lapso_comunicacion_periodica).prop("checked", true);

    if(logo_cte!=" " && color_primario!=" " && color_secundario!=" "){
        $('#nombre_logo').val(logo_cte);
        $('#nombre_fondo').val(fondo_cte);
        $("#color_prim").val(color_primario);
        $("#color_sec").val(color_secundario);
        var logosrc = "../../inc/imagenes/img_configuracion/"+logo_cte;
        var fondosrc = "../../inc/imagenes/img_configuracion/"+fondo_cte;
        $("#logo").attr("src",logosrc);
        $("#fondo").attr("src",fondosrc);
        $("#file").val(logo_cte);
        $("#file2").val(fondo_cte);
        
    }else{
        $("#estilos_default").prop("checked", true);
    }
    

}

function camporeg(id){

    if (id!="") {
        $('#pos_'+id).prop('disabled', false);  
        $('#campoclave_'+id).prop('disabled', false); 
        //HABILITAMOS  CAMPOS NOMBRE Y EDAD
        if(id==6){
            $('#camposregistro_1').prop('disabled', false); 
            $('#camposregistro_2').prop('disabled', false); 
        }
        //HABILITAMOS TODOS LOS CAMPOS DE EMAIL
        if(id==5){
            $('#camposregistro_1').prop('disabled', false); 
            $('#camposregistro_2').prop('disabled', false); 
            $('#tipoacumilacion_4').prop('disabled', false); 
            $('#tipo_comunicacion_registro2').prop('disabled', false); 
            $('#tipocomunicacion_2').prop('disabled', false); 
            $('#tipocomunicacionredimir_2').prop('disabled', false); 
            $('#tipocomunicacionperiodica_2').prop('disabled', false); 
            $('#mecanicaredencion_3').prop('disabled', false); 
        }
        //HABILITAMOS TODOS LOS CAMPOS DE SMS
        if(id==3){
            $('#camposregistro_1').prop('disabled', false); 
            $('#camposregistro_2').prop('disabled', false); 
            $('#tipoacumilacion_1').prop('disabled', false); 
            $('#tipo_comunicacion_registro1').prop('disabled', false); 
            $('#tipocomunicacion_1').prop('disabled', false); 
            $('#tipocomunicacionredimir_1').prop('disabled', false); 
            $('#tipocomunicacionperiodica_1').prop('disabled', false); 
            $('#mecanicaredencion_1').prop('disabled', false); 
        }
        //HABILITAMOS TODOS LOS CAMPOS PARA ID TARJETA
        if(id==7){
            $('#camposregistro_1').prop('disabled', false); 
            $('#camposregistro_2').prop('disabled', false); 
            $('#tipoacumilacion_2').prop('disabled', false); 
            $('#tipoacumilacion_3').prop('disabled', false); 
            $('#mecanicaredencion_2').prop('disabled', false); 
        }

    } 
}

function tipoproy(proyecto){
  //limpia_checked();
  if(proyecto=='1'){
    $('#redencion_1').show();
    $('#redencion_2').show();
    $('#redencion_3').show();
    $('#redencion_4').hide();
    $('#redencion_5').hide();
    $("#tipoproyecto_1_detalle").show();
    $("#tipoproyecto_2_detalle").hide();
    $('#visitas_premio').val('');
    $('#puntos_por_visita').val('');
    $("#div_ptos_bienvenida").show();
    $("#div_ptos_cumple").show();
  }else if(proyecto=='2'){
    $('#redencion_1').hide();
    $('#redencion_2').show();
    $('#redencion_3').show();
    $('#redencion_4').hide();
    $('#redencion_5').hide();
    $("#tipoproyecto_1_detalle").hide();
    $("#tipoproyecto_2_detalle").show();
    $('#por_cant_vendida').val('');
    $('#num_puntos_peso').val('');
    $("#div_ptos_bienvenida").show();
    $("#div_ptos_cumple").show();
  }else if(proyecto=='3'){
    $('#redencion_1').hide();
    $('#redencion_2').hide();
    $('#redencion_3').hide();
    $('#redencion_4').show();
    $('#redencion_5').hide();
    $("#tipoproyecto_1_detalle").hide();
    $("#tipoproyecto_2_detalle").hide();
    $('#visitas_premio').val('');
    $('#puntos_por_visita').val('');
    $('#por_cant_vendida').val('');
    $('#num_puntos_peso').val('');
    $("#div_ptos_bienvenida").show();
    $("#div_ptos_cumple").show();
  }else if(proyecto=='4'){
    $('#redencion_1').hide();
    $('#redencion_2').hide();
    $('#redencion_3').hide();
    $('#redencion_4').hide();
    $('#redencion_5').show();
    $("#tipoproyecto_1_detalle").hide();
    $("#tipoproyecto_2_detalle").hide();
    $('#visitas_premio').val('');
    $('#puntos_por_visita').val('');
    $('#por_cant_vendida').val('');
    $('#num_puntos_peso').val('');
    $("#div_ptos_bienvenida").hide();
    $("#div_ptos_cumple").hide();
  }
  
}
function bono(res){
    if(res==1){
         $('#ptos_bienvenida').prop('disabled', false);
    }else{
         $('#ptos_bienvenida').prop('disabled', true);
         $('#ptos_bienvenida').val('');
    }
}

function icons(){
    var icon1 = 'perm_data_setting';
    var icon2 = 'supervisor_account';
    var icon3 = 'exposure_plus_1';
    var icon4 = 'card_giftcard';
    var icon5 = 'card_travel';
    var icon6 = 'trending_up';
    $('#ico_1').html(icon1);
    $('#ico_2').html(icon2);

    $('#2ico_1').html(icon3);
    $('#2ico_2').html(icon4);
    $('#2ico_3').html(icon5);
    $('#2ico_4').html(icon6);
}


$(function() {
     $("input:file").change(function (){
       $('#fondo').hide();
       $('#logo').hide();
     });
  });


var touchSensitivity = 5;
$(".carousel").on("touchstart", function (event) {
    var xClick = event.originalEvent.touches[0].pageX;
    $(this).one("touchmove", function (event) {
        var xMove = event.originalEvent.touches[0].pageX;
        if (Math.floor(xClick - xMove) > touchSensitivity) {
            $(this).carousel('next');
        } else if (Math.floor(xClick - xMove) < -(touchSensitivity)) {
            $(this).carousel('prev');
        }
    });
    $(".carousel").on("touchend", function () {
        $(this).off("touchmove");
    });
});




$(function() {


      //end swipe tu_busca

      //start swipe id=demox by class 
    /*$(".carousel").swipe({

      swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
        
        if (direction == 'left') {
            
            $(".carousel").carousel('next');
            $(".carousel").next();
            console.log('si sirve');
        }
        else if (direction == 'right'){
            
            $(".carousel").carousel('prev');
            $(".carousel").prev();
            console.log('si sirve2');
        } 

      },
      allowPageScroll:"vertical"

    });*/

    

});







