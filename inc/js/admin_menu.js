function addmenu(){
    var op = 5;
    var item = $('#element').val();
    var cat = $('#cat').val();
    var descript = $('#description').val();
    var price = $('#price').val();
    var filename = $('#filename').val();

    $.ajax({
        data:{ op: op, item: item, cat: cat, descript: descript, price: price, filename : filename },
        url: '../../controller/administracion/c_llamadas_ajax.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            if(response==1){
                swal(" ", "Elemento Agregado al Menú", "success");
                menu('administracion/admin_menu');
            }else{
                swal('Error!','El Elemento no pudo agregarse','warning');
            }
            
        }
    });
}