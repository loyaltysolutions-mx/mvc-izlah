<?php 
/**
 *  * ##+> ################################# <+##
 * VISTA DE CONFIGURACION INICIAL DE LA PLATAFORMA DE LEALTAD
 * Desarrolado ->Miguel Ruiz
 *  * ##+> ################################# <+##
 */
include_once '../../inc/cont_fijos.php';
include_once '../../inc/parametros.php';
include_once '../../inc/funciones.php';
$ins_cont_fijos=new Contenidos_Fijos($ser,$usu,$pas,$bd);
$ins_fun_basicas=new Funciones_Basicas();
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <?php  $ins_cont_fijos->head(); ?>
    <!-- JS  DE CONFIGURACION-->
    <script src="../../inc/js/configuracion.js"></script>  

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!--  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
    <script src="../../inc/plugins/bootstrap/js/bootstrap.min.js"></script>
  </head>
  <style type="text/css">

    #register_form fieldset:not(:first-of-type) {
      display: none;
    }

  </style>
  <body id="config">
    <?php
        $ins_cont_fijos->header_config();
    ?>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12 col-sm-offset-3 col-md-12 col-md-offset-2 main " style='    margin-left: 0px!important;'>
         <div class="container">
            <h2>Configuración de Plataforma de Lealtad</h2>
            <div class="progress">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <div id="register_form"    method="post">

            <fieldset>
              <br>
              <h2> <i class="far fa-id-badge"></i>¿Donde vivira el Programa?</h2>
              <hr>
              <div class="row">
                <div class="form-check customradio col-md-4">
                  <input class=" tipo_registro form-check-input" type="radio" name="device" id="movil" value="movil" >
                  <label class="form-check-label" for="movil">

                    <!-- In Label -->
                    <div class="info-box bg-cyan hover-expand-effect">
                      <div class="icon">
                          <i class="material-icons">phonelink_ring</i>
                      </div>
                      <div class="content">
                          <h1 class="text">Móvil/Tablet</h1>
                      </div>
                    </div>

                  </label> 
                </div>

                <div class="form-check customradio col-md-4">
                    <input class=" tipo_registro form-check-input" type="radio" name="device" id="pc" value="pc" >
                    <label class="form-check-label" for="pc">

                      <!-- In Label -->
                      <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">computer</i>
                        </div>
                        <div class="content">
                            <h1 class="text">Escritorio/PC</h1>
                        </div>
                      </div>

                    </label> 
                </div>
              </div>
              
              <br/>

              <button type="button" class="next-form btn btn-info" value="Siguiente" />Siguiente <i class="far fa-arrow-alt-circle-right"></i></button><br><br>
            </fieldset>         
<!-- REGISTRO -->
            <fieldset>
                <br>
                <h2> <i class="far fa-id-badge"></i> ¿Cómo se hará el registro?</h2>
                <hr>
                <label><b>Forma del Registro</b></label>
                <br>
                <div class="row">
                  <?php 
                    $res=$ins_fun_basicas->consulta_generica('tbl_tipo_registro', '');
                         while($fila = $res->fetch_assoc()){ ?>
                            <div class="form-check customradio col-md-4">
                                <input class=" tipo_registro form-check-input" type="radio" name="registro" id="registro_<?php echo $fila['id_tipo_registro']; ?>" value="<?php echo $fila['id_tipo_registro']; ?>" >
                                <label class="form-check-label" for="registro_<?php echo $fila['id_tipo_registro']; ?>">
                                  <!--?php echo utf8_encode($fila['nombre']); ?-->

                                  <!-- In Label -->
                                  <div class="info-box bg-cyan hover-expand-effect">
                                    <div class="icon">
                                        <i id="ico_<?php echo $fila['id_tipo_registro']; ?>" class="material-icons"></i>
                                    </div>
                                    <div class="content">
                                        <h1 class="text"><?php echo utf8_encode($fila['nombre']); ?></h1>
                                    </div>
                                  </div>

                                </label> 
                            </div>
                        <?php } ?>
                  <br>
                </div>
                
                <!--<button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
                <button type="button" class="next-form btn btn-info" >Siguiente <i class="far fa-arrow-alt-circle-right"></i></button><br><br>
            -->
            <br>
            <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
  					<button type="button" class="next-form btn btn-info" value="Siguiente" />Siguiente <i class="far fa-arrow-alt-circle-right"></i></button><br><br>
                    
					</fieldset>
<!-- CAMPOS DE REGISTRO -->
          <fieldset>
                <div class="form-group">
                  <br>
                  <h2> <i class="far fa-id-badge"></i> ¿Qué campos se capturan para el Registro?</h2>
                  <hr>
                  <table border='0'>
                      <tr>
                          <td style='text-align:center;'><b>Campo  </b></td>
                          <td><b>| Posición |</b></td>
                          <td><b>Clave para Acceso</b></td>
                      </tr>
                  
                    <?php 
                    $res=$ins_fun_basicas->consulta_generica('tbl_campos_registro', '');
                         while($fila = $res->fetch_assoc()){ ?>
                            <tr>
								<td class="customcheck">
									<input name="campos_registro[]" class="styled" type="checkbox" value="<?php echo utf8_encode($fila['id_campos_registro']); ?>" id="camposregistro_<?php echo utf8_encode($fila['id_campos_registro']); ?>">
								   
									<label class="styled" for="camposregistro_<?php echo utf8_encode($fila['id_campos_registro']); ?>">
										<?php echo utf8_encode(ucwords ($fila['nombre'])); ?>
									</label>
								</td>
								<td style=" text-align: center">
									<input  id="pos_<?php echo utf8_encode($fila['id_campos_registro']); ?>" type='number'  value=" " style='width:40px;height:25px' min="1"  disabled>
								</td>
								<td style=" text-align: center">
								<?php 
								 if(utf8_encode(ucwords ($fila['nombre']))=='Nombre' or utf8_encode(ucwords ($fila['nombre']))=='Edad' ){ }else{ ?>
									<input class=" campo_clave form-check-input" type="radio" name="campo_clave" id="campoclave_<?php echo utf8_encode($fila['id_campos_registro']); ?>" disabled >
									<label class="form-check-label" for="campoclave_<?php echo utf8_encode($fila['id_campos_registro']); ?>"></label>
								 <?php } ?>
								</td>
                            </tr>
                         <?php  }  ?>
                  </table>
                </div>
                <!--<button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
                <button type="button" class="next-form btn btn-info" >Siguiente <i class="far fa-arrow-alt-circle-right"></i></button><br><br>
            -->
            <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
            <button type="button" class="next-form btn btn-info" value="Siguiente" />Siguiente <i class="far fa-arrow-alt-circle-right"></i></button><br><br>
                    
          </fieldset>
<!-- ACUMULACION -->
          <fieldset>
              <br>
              <h2><i class="fas fa-boxes"></i> ¿Cúal será mécanica de Acumulación?</h2>
              <hr>
              <div class="row ">
                <div class="col-md-6 col-xs-12">
                  <br>
                  <label><b>Forma de Acumulación </b></label>
                  <?php 
                  $res=$ins_fun_basicas->consulta_generica('tbl_forma_acumulacion', '');
                       while($fila = $res->fetch_assoc()){ ?>
                          <div class="customcheck">
                              <input name="form_acum[]" class="styled" type="checkbox" value="<?php echo utf8_encode($fila['id_forma_acumulacion']); ?>" id="tipoacumilacion_<?php echo utf8_encode($fila['id_forma_acumulacion']); ?>">
                              <label class="styled" for="tipoacumilacion_<?php echo utf8_encode($fila['id_forma_acumulacion']); ?>">
                                  <?php echo utf8_encode($fila['nombre']); ?>
                              </label>
                          </div>
                       <?php  }  ?>
                  <br>     
                </div>

                <div class="col-md-6 col-xs-12">
                  <br>
                  <label><b> ¿Cúal será el Tipo  de Acumulación?</b></label>
                  <?php 
                      $res=$ins_fun_basicas->consulta_generica('tbl_tipo_proyecto', '');
                        while($fila = $res->fetch_assoc()){ ?>
                          <div class="form-check miniradio">
                              <input class=" tipo_acumulacion_proy form-check-input" type="radio" name="tipo_acumulacion" id="tipoproyecto_<?php echo utf8_encode($fila['id_tipo_proyecto']); ?>" value="<?php echo utf8_encode($fila['id_tipo_proyecto']); ?>" >
                              <label class="form-check-label" for="tipoproyecto_<?php echo utf8_encode($fila['id_tipo_proyecto']); ?>">
                                <!--?php echo utf8_encode($fila['nombre']); ?-->
                                <!-- In Label -->
                                  <div class="info-box bg-cyan hover-expand-effect">
                                    <div class="icon">
                                        <i id="2ico_<?php echo $fila['id_tipo_proyecto']; ?>" class="material-icons small-icons"></i>
                                    </div>
                                    <div class="content">
                                        <h1 class="text"><?php echo utf8_encode($fila['nombre']); ?></h1>
                                    </div>
                                  </div>
                            </label>
                          </div>
                  
                 <?php  }  ?>
                  <!-- DETALLE DE PUNTOS COMO % DE CONSUMO -->
                  <section id='tipoproyecto_1_detalle'>
                      <hr>
                      <i class="fas fa-chevron-right"></i> % de la cantidad vendida a otorgar para el programa <input id="por_cant_vendida" type="number" value=" " style="width:80px;height:25px" min="1" ><br>
                      <i class="fas fa-chevron-right"></i> N° de puntos por cada peso gastado <input id="num_puntos_peso" type="number" value=" " style="width:80px;height:25px" min="1" ><br>
                  </section>
                  <!-- DETALLE DE PUNTOS COMO VISITAS-->
                  <section id='tipoproyecto_2_detalle'>
                     <hr>
                      <!--<i class="fas fa-chevron-right"></i> N° mínimo visitas para premio <input id="visitas_premio" type="number" value=" " style="width:80px;height:25px" min="1" ><br>-->
                      <i class="fas fa-chevron-right"></i> N° de puntos por visita <input id="puntos_por_visita" type="number" value=" " style="width:80px;height:25px" min="1" ><br>
               
                  </section>
                  <br>
                </div>
              </div>
              
              <br>
              <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
              <button type="button" class="next-form btn btn-info" value="Siguiente" />Siguente <i class="far fa-arrow-alt-circle-right"></i></button><br><br>
          </fieldset>
          <!-- REDENCION -->
          <fieldset>
              <br>
              <h2><i class="fas fa-gift"></i> ¿Cuál será el Tipo de Redención?</h2>
              <hr>
              <h6>Configura la Forma de Redención</h6>
              <div class="form-group">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#premios_puntos">
                  Configurar
                </button>

                <!-- Modal -->
                <div class="modal fade" id="premios_puntos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Cinfiguración Puntos pos Visitas </h5><hr>
                      </div>
                      <div class="modal-body">
                       <?php  include '../../view/configuracion/premios_visitas.php'; ?>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
                        <button type="button" class="btn btn-primary">Guardar Configuración</button>
                      </div>
                    </div>
                  </div>
                </div> 
								
								
                <label><b> ¿Cuál será la forma de redención?</b></label>
                <hr>
                <?php
                    $res=$ins_fun_basicas->consulta_generica('tbl_tipo_redencion', '');
                    while($fila = $res->fetch_assoc()){ 
                ?>
                      <div class="customcheck" id="redencion_<?php echo utf8_encode($fila['id_tipo_redencion']); ?>">
                        <input name='tipo_reden[]' class="styled" type="checkbox" value="<?php echo utf8_encode($fila['id_tipo_redencion']); ?>" id="tiporedencion_<?php echo utf8_encode($fila['id_tipo_redencion']); ?>">
                        <label  class="styled" for="tiporedencion_<?php echo utf8_encode($fila['id_tipo_redencion']); ?>">
                        <?php echo utf8_encode($fila['nombre']); ?>
                        </label>
                <?php 
                          if($fila['link_detalle']!=''){ 
                ?>
                              <a href='_<?php echo utf8_encode($fila['link_detalle']); ?>'> Ir a Configuración</a>
                <?php 
                          }
                          
                                   
                ?>
                      </div>
                <?php
                    }  
                ?>
              </div>
              <div class="form-group">
                  <label><b>Seleccione la Mecánica de Redención</b></label>
                  <hr>
                  <?php
                      $res=$ins_fun_basicas->consulta_generica('tbl_mecanica_redencion', '');
                      while($fila = $res->fetch_assoc()){ ?>
                          <div class="customcheck">
                             <input name='mecanica_reden[]' class="styled" type="checkbox" value="<?php echo utf8_encode($fila['id_mecanica_redencion']); ?>" id="mecanicaredencion_<?php echo utf8_encode($fila['id_mecanica_redencion']); ?>">
                             <label  class="styled" for="mecanicaredencion_<?php echo utf8_encode($fila['id_mecanica_redencion']); ?>">
                               <?php echo utf8_encode($fila['nombre']); ?>
                             </label>
                          </div>
                  <?php  }  ?>
              </div>
              <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
              <button type="button" class="next-form btn btn-info" value="Siguiente" />Siguente <i class="far fa-arrow-alt-circle-right"></i></button><br><br>
            </fieldset>
<!-- COMUNICACION -->
            <fieldset>
                <br>
                <h2><i class="far fa-comments"></i> ¿Cuál será la forma de Comunicación?</h2>
                <h6>Configura la Forma de Comunicación</h6>

                <div class="row">

                  <div class="col-xs-12 col-md-3">
                    <label><b>Bono de Bienvenida</b></label>
                    <div class="form-check">
                        <input class=" bono_bienvenida form-check-input"  type="radio" id="si_bono" name="bono"  value='1' />
                        <label for="si_bono">Si</label><br>
                        <input class=" bono_bienvenida form-check-input"  type="radio" id="no_bono" name="bono"  value='0' />
                        <label for="no_bono">No</label>
                        <section id='div_ptos_bienvenida'>
                            Puntos de Bienvenida:<input type="number" id='ptos_bienvenida' style='width:80px;height:25px' min="1" disabled >
                        </section>
                    </div>
                  </div>

                  <div class="col-xs-12 col-md-3">
                    <label><b>Bono por Cumpleaños</b></label>
                    <div class="form-check">
                        <input class=" bono_cumpleanios form-check-input"  type="radio" id="si_cumple_anios" name="cumple"  value='1' />
                        <label for="si_cumple_anios">Si</label><br>
                        <input class=" bono_cumpleanios form-check-input"  type="radio" id="no_cumple_anios" name="cumple"  value='0' />
                         <label for="no_cumple_anios">No</label>
                         <section id='div_ptos_cumple'>
                            Puntos de Cumpleaños:<input type="number" id='ptos_cumple' style='width:80px;height:25px' min="1"  disabled >
                        </section>
                    </div>
                  </div>
                  <br><br>

                </div>
                
                <hr>

                <div class="row">

                  <div class="col-xs-12 col-md-3">
                    <label><b>Tipo de Comunicación al Registrarse</b></label>
                    <?php
                    //class: form-check-label
                    // input class: tipo_comunicacion_registro form-check-input
                        $res=$ins_fun_basicas->consulta_generica('tbl_tipo_comunicacion', '');
                        while($fila = $res->fetch_assoc()){ ?>
                            <div class="customcheck form-check" id="comunicacion_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                               <input name='tipo_comunicacion_registro[]' class="styled" type="checkbox" value="<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>" id="tipo_comunicacion_registro<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                               <label class="styled" for="tipo_comunicacion_registro<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                                 <?php echo utf8_encode($fila['nombre']); ?>
                               </label>
                            </div>
                    <?php  }  ?>
                  </div>

                  <div class="col-xs-12 col-md-3">
                    <label><b>Tipo de Comunicación al Acumular</b></label>
                    <?php
                        $res=$ins_fun_basicas->consulta_generica('tbl_tipo_comunicacion', '');
                        while($fila = $res->fetch_assoc()){ ?>
                            <div class="customcheck form-check" id="comunicacion_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                               <input name='tipo_comunicacion_acumular[]' class="styled" type="checkbox" value="<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>" id="tipocomunicacion_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                               <label class="styled" for="tipocomunicacion_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                                 <?php echo utf8_encode($fila['nombre']); ?>
                               </label>
                            </div>
                    <?php  }  ?>
                  </div>

                  <div class="col-xs-12 col-md-3">
                    <label><b>Tipo de Comunicación al Redimir</b></label>
                    <?php
                        $res=$ins_fun_basicas->consulta_generica('tbl_tipo_comunicacion', '');
                        while($fila = $res->fetch_assoc()){ ?>
                            <div class="customcheck form-check" id="comunicacionredimir_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                               <input name='tipo_comunicacion_redimir[]' class="styled" type="checkbox" value="<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>" id="tipocomunicacionredimir_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                               <label class="styled" for="tipocomunicacionredimir_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                                 <?php echo utf8_encode($fila['nombre']); ?>
                               </label>
                            </div>
                    <?php  }  ?>
                  </div>

                  <div class="col-xs-12 col-md-3">
                    <label><b>Comunicación Periódica</b></label>
                   <?php
                        $res=$ins_fun_basicas->consulta_generica('tbl_tipo_comunicacion', '');
                        while($fila = $res->fetch_assoc()){ ?>
                            <div class="customcheck form-check" id="comunicacionperiodica_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                               <input name='tipo_comun_periodica[]' class="styled" type="checkbox" value="<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>" id="tipocomunicacionperiodica_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                               <label class="styled" for="tipocomunicacionperiodica_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                                 <?php echo utf8_encode($fila['nombre']); ?>
                               </label>
                            </div>
                    <?php  }  ?>
                   
                  </div> 
				  <div class="col-xs-12 col-md-3">
				      <label>Lapso de Comunicación Periódica</label>
                       <?php
                            $res=$ins_fun_basicas->consulta_generica('tbl_periodo_comunicacion', '');
                            while($fila = $res->fetch_assoc()){ ?>
                                <div class="miniradio form-check">
                                    <input class="lapso_comun form-check-input" type="radio" name="mediocomunicacionperiodica" id="mediocomunicacionperiodica_<?php echo utf8_encode($fila['id_periodo_comunicacion']); ?>" value="<?php echo utf8_encode($fila['id_periodo_comunicacion']); ?>" >
                                    <label class="form-check-label" for="mediocomunicacionperiodica_<?php echo utf8_encode($fila['id_periodo_comunicacion']); ?>">
                                      <!--?php echo utf8_encode($fila['nombre']); ?-->
                                      <div class="info-box bg-cyan hover-expand-effect">
                                        <div class="icon">
                                            <i id="2ico_<?php echo $fila['id_tipo_proyecto']; ?>" class="far fa-calendar-alt small-icons"></i>
                                        </div>
                                        <div class="content">
                                            <h1 class="text"><?php echo utf8_encode($fila['nombre']); ?></h1>
                                        </div>
                                      </div>
                                    </label>
                                </div>
                        <?php  }  ?><br>
				  </div>
                </div>   
				<button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
                <button type="button" class="next-form btn btn-info" value="Siguiente" />Siguente <i class="far fa-arrow-alt-circle-right"></i></button><br><br>
                
            </fieldset>
				 
<!-- ESTILO -->
            <fieldset>
                <br>
                <h2><i class="far fa-id-card"></i>  Configura el Estilo de la Plataforma del Cliente</h2>
                <input name="estilos_default" class="estilos_default form-check-input" type="checkbox" value="1" id="estilos_default">
                <label class="form-check-label" for="estilos_default"> 
                  <h6>Tomar estilos Default </h6>
                </label>
                <br>  
    						<section id='conten_estilo'>
    							<h6>Configura los Estilos Generales del Cliente</h6>
    							<!-- LOGO-->
                  <form method="post" id="formulario" enctype="multipart/form-data">
                   <b>Selecciona Logo:</b> <input type="file" name="file" id='file'>
                  </form>
    							<input type="hidden" id='nombre_logo'>
    							<div id='respuesta_logo'></div><br>
    							<!--IMAGEN FONDO -->
                  <form method="post" id="formulario2" enctype="multipart/form-data">
                   <b>Selecciona Imagen de Fondo:</b> <input type="file" name="file2" id='file2'>
                  </form>
    							<input type="hidden" id='nombre_fondo'>
    							<div id='respuesta_imagen_fondo'></div><br>
    							<div class="form-group">
    								<label for="color_prim"><b>Color Primario</b></label>
    								<input type="color" class="form-control select_color" name="color_prim" id="color_prim" >
    							</div>
    							<div class="form-group">
    								<label for="color_sec"><b>Color Secundario</b></label>
    								<input type="color" class="form-control select_color" name="color_sec" id="color_sec" >
    							</div><br>
    						</section>
                <br>
                        <!-- <button type="button" class="next-form btn btn-info" value="Siguiente" />Siguiente <i class="far fa-arrow-alt-circle-right"></i></button><br><br>
                   -->

				        <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
                <input type="submit" name="submit" class="submit btn btn-success guarda_config" value="Generar Configuración" />
                <div id='loading'> <img src="../../inc/imagenes/load.gif"></div><br><br>
            </fieldset>
				 
				 
				 
				 
				 
                </div>
                </div>
      </div>
    </div>
       <!-- <div class="row ">
            <div class="col-md-2 text-center"></div>
            <div class="col-md-10">
                <div id='resp_buscador'>sdfsdf</div><br><br>
            </div>
        </div>-->
  
 <!-- <div class="footer">
   <?php  
    $ins_cont_fijos->footer_config();
  ?>
  </div>-->
  </body>
  <script>
    icons();
  </script>
</html>

