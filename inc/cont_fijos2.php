<?php

class Cont_fijos{
    private $ser;
    private $usu;
    private $pas;
    private $bd;

     public function __construct($ser,$usu,$pas,$bd) {
         $this->ser=$ser;
         $this->usu=$usu;
         $this->pas=$pas;
         $this->bd=$bd;
        
     }
    public function header()
    {?>
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <!--a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a-->
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="home.php">Configuración</a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <!--ul class="nav navbar-nav navbar-right">
                    <!-- Notifications -->
                    <!--li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">notifications</i>
                            <span class="label-count">7</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">NOTIFICATIONS</li>
                            <li class="body">
                                <ul class="menu">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <div class="icon-circle bg-light-green">
                                                <i class="material-icons">person_add</i>
                                            </div>
                                            <div class="menu-info">
                                                <h4>12 new members joined</h4>
                                                <p>
                                                    <i class="material-icons">access_time</i> 14 mins ago
                                                </p>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);">View All Notifications</a>
                            </li>
                        </ul>
                    </li>
                    <!-- #END# Notifications -->
                    <!-- Tasks -->
                    <!--li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons">flag</i>
                            <span class="label-count">9</span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="header">TASKS</li>
                            <li class="body">
                                <ul class="menu tasks">
                                    <li>
                                        <a href="javascript:void(0);">
                                            <h4>
                                                Footer display issue
                                                <small>32%</small>
                                            </h4>
                                            <div class="progress">
                                                <div class="progress-bar bg-pink" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: 32%">
                                                </div>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="footer">
                                <a href="javascript:void(0);">View All Tasks</a>
                            </li>
                        </ul-->
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->
    
        <?php
            //require_once ('../../left-sidebar.php');
        ?>
        <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section> 
     <?php }
    public function head(){ ?>  
            
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Configuración - Plataforma de Lealtad</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../../inc/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../../inc/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../../inc/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Colorpicker Css -->
    <link href="../../inc/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="../../inc/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

    <!-- Wait Me Css -->
    <link href="../../inc/plugins/waitme/waitMe.css" rel="stylesheet" />

    <!-- Bootstrap Select Css -->
    <link href="../../inc/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- noUISlider Css -->
    <link href="../../inc/plugins/nouislider/nouislider.min.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="../../inc/plugins/morrisjs/morris.css" rel="stylesheet" />
    
    <!-- JQuery DataTable Css -->
    <link href="../../inc/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="../../inc/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../../inc/css/themes/all-themes.css" rel="stylesheet" />

    <!-- Dropzone Css -->
    <link href="../../inc/plugins/dropzone/dropzone.css" rel="stylesheet">

    <!-- Before -->
    <!-- Que es esto de banco? --ª
    <!--link href="../../inc/css/banco.css" rel="stylesheet"-->

    <!-- bootstrap y jquery-->
    <script src="../../inc/js/jquery-1.11.2.min.js"></script>
    <!--script src="../../inc/bootstrap/js/bootstrap.js"></script-->
    <!--<script src="../../inc/bootstrap/js/bootstrap-typeahead.js"></script>-->
     
    <!-- JS GLOBAL DEL SISTEMA -->
    <script src="../../inc/js/global.js"></script>
    <!-- sweet alert -->
    <link rel="stylesheet" href="../../inc/css/sweet-alert2.css">
     <!-- Grid -->
    <script src="../../inc/js/jquery.dataTables.min.js"></script>
    <!--link rel="stylesheet" href="../../inc/css/jquery.dataTables.min.css"-->
     <!--CALENDARIO -->
    <!--script src="../../inc/js/jquery-ui.js"></script-->
    <!--link rel="stylesheet" href="../../inc/css/jquery-ui.css"-->

    <!--link rel="stylesheet" href="../../inc/css/font-awesome.css"/>
    <link rel="stylesheet" href="../../inc/css/build.css"/-->

    <!-- GRAFICAS -->
    <!--script src="../../inc/js/Chart.bundle.js"></script-->
           
    <?php }
     
    public function menu($idusu,$usuario,$email){
        /*$res=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $sql="SELECT * FROM tbl_usuarios WHERE id_usuarios='$idusu'";
        $query=mysqli_query($res,$sql); 

        while($r=mysqli_fetch_assoc($query))
            {
                $rol = $r['id_rol'];
            }
        //use for user control 4(SuperAdmin)
        if($rol==4){
            $sql2 ="SELECT * FROM menu";
        }else{
            $sql2 ="SELECT * FROM menu WHERE idmenu=1";
        }
        
            
        $query2=mysqli_query($res,$sql2);*/
        
        ?>

        <section id="<?php $rol; ?>">
        <!-- Left Sidebar -->
            <aside id="leftsidebar" class="sidebar">
                <!-- User Info -->
                <div class="user-info">
                    <div class="image">
                        <img src="../../inc/imagenes/user.png" width="48" height="48" alt="User" />
                    </div>
                    <div class="info-container">
                        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php //echo $usuario; ?></div>
                        <div class="email"><?php //echo $email; ?></div>
                        <div class="btn-group user-helper-dropdown">
                            <!--i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                            <ul class="dropdown-menu pull-right">
                                <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                                <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                                <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                            </ul-->
                        </div>
                    </div>
                </div>
                <!-- #User Info -->
                <!-- Menu -->
                <div class="menu">
                    <ul class="list">
                        <li class="active">
                            <a href="home.php">
                                <i class="material-icons">home</i>
                                <span>Home</span>
                            </a>   
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <span class="glyphicon glyphicon-list-alt"  aria-hidden="true"></span>
                                <span>Registro</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a id="1" onclick="menus(this.id);">
                                        Forma Registro
                                    </a>  
                                </li>
                                <li>
                                    <a id="2" onclick="menus(this.id);">
                                        Campos Registro
                                    </a>  
                                </li>
                            </ul>    
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <span class="glyphicon glyphicon-plus-sign"  aria-hidden="true"></span>
                                <span>Acumulación</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a id="3" onclick="menus(this.id);">
                                        Mécanica de Acumulación 
                                    </a>  
                                </li>
                                <li>
                                    <a id="15" onclick="menus(this.id);">
                                        Confirmación de Operación
                                    </a>  
                                </li>
                            </ul>    
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <span class="glyphicon glyphicon-transfer"  aria-hidden="true"></span>
                                <span>Redención</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a id="4" onclick="menus(this.id);">
                                        Tipo de Redención 
                                    </a>  
                                </li>
                            </ul>    
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle">
                                <span class="glyphicon glyphicon-cog"  aria-hidden="true"></span>
                                <span>Configuración Gral.</span>
                            </a>
                            <ul class="ml-menu">
                                <li>
                                    <a id="5" onclick="menus(this.id);">
                                        Donde Vive el Programa
                                    </a>  
                                </li>
                                <li>
                                    <a id="6" onclick="menus(this.id);">
                                        Comunicación
                                    </a>  
                                </li>
                                <li>
                                    <a id="7" onclick="menus(this.id);">
                                        Estilos del Cliente
                                    </a>  
                                </li>
                                <li>
                                    <a id="8" onclick="menus(this.id);">
                                        Sucursales
                                    </a>  
                                </li>
                                <li>
                                    <a id="9" onclick="menus(this.id);">
                                        Menú
                                    </a>  
                                </li>
                                <li>
                                    <a id="10" onclick="menus(this.id);">
                                        Promociones
                                    </a>  
                                </li>
                                <li>
                                    <a id="11" onclick="menus(this.id);">
                                        Premios de Redención
                                    </a>  
                                </li>
                                <li>
                                    <a id="12" onclick="menus(this.id);">
                                        Niveles de Usuarios
                                    </a>  
                                </li>
                                <li>
                                    <a id="13" onclick="menus(this.id);">
                                        Información de APP
                                    </a>  
                                </li>
                                <li>
                                    <a id="14" onclick="menus(this.id);">
                                        Puntos de Venta
                                    </a>  
                                </li>
                            </ul>    
                        </li>
                <br>
                <!--li class="">
                    <a href="../usuario/uploadcode.php">
                        <i class="material-icons">cloud_upload</i>
                        <span>Codigos de Activación</span>
                    </a>   
                </li-->          
                <li class="" id="<?php echo $reg['idarea']; ?>">
                    <a href="../../controller/login/c_logout.php">
                        <?php echo $reg['icono']; ?>
                        <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
                        <span>Salir</span>
                    </a>
                </li>
      </div>
    </div> 
    <?php       
} //end function

    public function submenu(){
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $sql=" SELECT * FROM pantallas WHERE activo=1";
        $query=mysqli_query($res_con,$sql); 
      
        ?>
        <div class='sub_menu_cont' id='sub_menu_<?php  //echo $idarea; ?>'><?php
            while($reg=mysqli_fetch_assoc($query) )
            { 
        ?>
                <a href="<?php echo $reg['ruta']; ?>" class="list-group-item"><?php echo $reg['nombre']; ?></a>   
        <?php 
            } ?>
          </div>
   <?php  }
    public function conecta_bd($ser,$usu,$pas,$bd,$con){
      $con=mysqli_connect($this->ser,$this->usu,$this->pas,$this->bd);
        if ($con)
        {
            return $con;
            mysqli_close($con);
        }else{
            echo("Error description: " . mysqli_error($con));
                exit(); 
            }
    }
    public function footer(){?>
             
        <!-- Jquery Core Js -->
        <script src="../../inc/plugins/jquery/jquery.min.js"></script>

        <!-- Sweet Alerts -->
        <script src="../../inc/js/sweet-alert2.js"></script>

        <!-- Bootstrap Core Js -->
        <script src="../../inc/plugins/bootstrap/js/bootstrap.js"></script>

         <!-- Select Plugin Js -->
        <script src="../../inc/plugins/bootstrap-select/js/bootstrap-select.js"></script>

        <!-- Slimscroll Plugin Js -->
        <script src="../../inc/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

        <!-- Bootstrap Colorpicker Js -->
        <script src="../../inc/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>

        <!-- Dropzone Plugin Js -->
        <script src="../../inc/plugins/dropzone/dropzone.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="../../inc/plugins/node-waves/waves.js"></script>

        <!-- Jquery CountTo Plugin Js -->
        <script src="../../inc/plugins/jquery-countto/jquery.countTo.js"></script>

        <!-- Morris Plugin Js -->
        <script src="../../inc/plugins/raphael/raphael.min.js"></script>
        <script src="../../inc/plugins/morrisjs/morris.js"></script>

        <!-- ChartJs -->
        <script src="../../inc/plugins/chartjs/Chart.bundle.js"></script>

        <!-- Flot Charts Plugin Js -->
        <script src="../../inc/plugins/flot-charts/jquery.flot.js"></script>
        <script src="../../inc/plugins/flot-charts/jquery.flot.resize.js"></script>
        <script src="../../inc/plugins/flot-charts/jquery.flot.pie.js"></script>
        <script src="../../inc/plugins/flot-charts/jquery.flot.categories.js"></script>
        <script src="../../inc/plugins/flot-charts/jquery.flot.time.js"></script>

        <!-- Sparkline Chart Plugin Js -->
        <script src="../../inc/plugins/jquery-sparkline/jquery.sparkline.js"></script>

        <!-- Custom Js -->
        <script src="../../inc/js/admin.js"></script>
        <script src="../../inc/js/pages/forms/advanced-form-elements.js"></script>
        <script src="../../inc/js/pages/index.js"></script>

        <!-- Custom Js -->
        <script src="../../inc/js/custom2.js"></script>
        <script src="../../inc/js/custom.js"></script>

        <!-- Demo Js -->
        <script src="../../inc/js/demo.js"></script>
        
   <?php  }
   
}
?>