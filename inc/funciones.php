<?php
class Funciones_Basicas{
//FUNCION ENVIA MAIL DE CUMPLE
public function bono_cumpleanios(){
 include './parametros.php';
 $con=new mysqli($ser,$usu,$pas,$bd);
   if($con->connect_errno)
        {
         printf("Conexión fallida: %s\n", $con->connect_error);
         exit();
        }else
		{
			//OBTENEMOS CONFIGURACION DE PLATAFORMA DE BONO DE CUMPLE
			$qr_config_plat="select * from tbl_configuracion_proyecto";
			$resp_consulta_config_plat=$con->query($qr_config_plat);
			$reg_config = $resp_consulta_config_plat->fetch_assoc();
			if($reg_config['bono_cumple']==1)
			{
				$qr_cumple="SELECT * FROM tbl_usuario WHERE MONTH(fecha_nacimiento) = MONTH(NOW()) AND DAY( fecha_nacimiento ) = DAY(NOW()) AND rol=3";
				$resp_consulta_cumple=$con->query($qr_cumple);
				while($fila = $resp_consulta_cumple->fetch_assoc())
				{ 
					//ENVIA MENSAJE DE CUMPLEAÑOS Y ASIGNA  BONO DE CUMPLE 
					$id_insert=$fila['id_usuario'];
					$mail=$fila['email'];
					$ptos_cumple=$reg_config['puntos_cumple'];
					$qr_insert_bono='insert into tbl_registros(id_usuario,ticket,id_configuracion,puntos,fecha_registro,id_tipo_registro) values('.$id_insert.',"'.utf8_decode("Bono Cumpleaños").'",'.$reg_config['id_configuracion_proyecto'].',"'.$ptos_cumple.'",now(),4)';
					$con->query($qr_insert_bono);
					//INSERTAMOS REGISTRO DE PUNTOS
						 $qri_consulta_pts_acumulacion=' select * from tbl_puntos_totales where id_usuario='.$id_insert.' and id_tipo_puntos=1';
						 $resp_consulta_pts_acumulacion=$con->query($qri_consulta_pts_acumulacion); 
						 $reg= mysqli_fetch_assoc($resp_consulta_pts_acumulacion);
						 $ptos_actuales=$reg['puntos_totales'];
						
						 $nuevos_ptos=$ptos_actuales+$ptos_cumple; 
						 if($ptos_actuales==''){
							 //INSERTA
							$qri_insert_pts_acumulacion='insert tbl_puntos_totales(id_usuario,puntos_totales,id_tipo_puntos,fecha_registro)values('.$id_insert.',"'.$nuevos_ptos.'",1,now())';
							$con->query($qri_insert_pts_acumulacion); 
							  }else{
							 //ACTUALIZA
							$id_reg=$reg['id_puntos_totales']; 
							$qri_actualiza_pts_acumulacion='update tbl_puntos_totales set puntos_totales='.$nuevos_ptos.',fecha_registro=now() where id_puntos_totales='.$id_reg;
							$con->query($qri_actualiza_pts_acumulacion);
						 }
					//ENVIAMOS COMUNICACION AL ABONAR BONO CUMPLEAÑOS
					  $tipo_comuni_acumular=$reg_config['id_tipo_comunicacion_acumular'];
                      $tipo_comunicado=explode('-',$tipo_comuni_acumular);
                        for($i=0;$i<=count($tipo_comunicado)-1;$i++){
                            switch ( $tipo_comunicado[$i]) {
                                case 1:
                                    //ENVIA SMS
                                   $msg='Felicidades Acumulaste  '.$ptos_cumple .' puntos, por tu Cumpleaños ve tu historial de puntos en www.sitio.com.mx/puntos';
                                   $cel=$num_celular_cte;
                                    //$cel=5527278290;
                                    $this->envia_sms($msg, $cel, $usu_sms, $pas_sms, $url_sms);
                                 break;
                                 case 2:
                                     //ENVIA MAIL
                                    $mensa='<h3>Notificaci&oacute;n Acumulación  </h3> Felicidades Acumulaste <b>'. $ptos_cumple .'</b> puntos por tu Cumpleaños<br><br>
                                    <hr><h5><font color="#8c8c8c" size="1">* Este correo es generado de forma automatica por la Plataforma de Lealtad, no es necesario que lo respondas</font></h5>';
								   
								   $this->envia_mail_cron($usu_mail, $pas_mail, $from_mail,$mail , 'Acumulación',$mensa);
                                 break;

                                default:
                                    break;
                            }
                            
                        }

			    }	
			}   
		}

}
//FUNCION TRAE NOMBRE DE LA SEMANA
public function nombre_dia($fecha){
$fechats = strtotime($fecha);
$nombre_dia='';
    switch (date('w', $fechats)){ 
            case 0: $nombre_dia="Domingo"; break; 
            case 1: $nombre_dia="Lunes"; break; 
            case 2: $nombre_dia="Martes"; break; 
            case 3: $nombre_dia="Miercoles"; break; 
            case 4: $nombre_dia="Jueves"; break; 
            case 5: $nombre_dia="Viernes"; break; 
            case 6: $nombre_dia="Sabado"; break; 
        }
    return $nombre_dia;
}
//FUNCION ENVIA SMS 
public function envia_sms($msg,$cel,$usu_sms,$pas_sms,$url_sms){
$post=json_encode(
		array(
				"user"=>$usu_sms,
				"password"=>$pas_sms,
				"service"=>"1",
				"data"=>array(
								"message"=>utf8_encode($msg),
								"cellphones"=>array($cel),
								"lada"=>"1"
							  )
				)
		);
//print_r($post);
//die();
$target_url = $url_sms;
 $ch = curl_init();
 curl_setopt($ch,CURLOPT_URL,$target_url);
 curl_setopt($ch,CURLOPT_POST,1);
 curl_setopt($ch,CURLOPT_POSTFIELDS,array("JSON"=>$post));
 curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
 $data = curl_exec($ch);
 curl_close($ch); 
 $arrayResponse = json_decode($data);
 //print_r($arrayResponse);
 //die();

 return $arrayResponse->status == 'Added' ? 1 : 0;
 //echo 'men:'.$msg.'cel:'.$cel.'usu:'.$usu_sms.'pas:'.$pas_sms.'url:'.$url_sms;
 //echo $post.'<br>';
}
//FUNCION ENVIA EMAIL  SOLO PARA CRON DE CUMPLEAÑOS
public function envia_mail_cron($usu_mail,$pas_mail,$from_mail,$email,$subject,$mensa){
    include_once  './phpmailer/class.phpmailer.php';
    $correo= str_replace('%40',"@",$email);
	    $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->SMTPDebug  = 0;
            $mail->Host       = 'smtp.gmail.com';
            $mail->Port       = 465;
            $mail->SMTPSecure = 'ssl';
            $mail->SMTPAuth   = true;
            //Definimos la cuenta que vamos a usar
            $mail->Username   = $usu_mail;
            //Introducimos nuestra contraseña 
            $mail->Password   = $pas_mail;
            $mail->From =$from_mail;
            $mail->FromName = $from_mail;
            $mail->AddAddress($correo, 'El Destinatario');
            $mail->Subject = $subject;
            $mail->isHTML(true);              
            $mail->MsgHTML($mensa);  
            $mail->Send();
    
}
//FUNCION ENVIA EMAIL 
public function envia_mail($usu_mail,$pas_mail,$from_mail,$email,$subject,$mensa){
    include  '../../inc/phpmailer/class.phpmailer.php';
    $correo= str_replace('%40',"@",$email);
	    $mail = new PHPMailer();
            $mail->IsSMTP();
            $mail->SMTPDebug  = 0;
            $mail->Host       = 'smtp.gmail.com';
            $mail->Port       = 465;
            $mail->SMTPSecure = 'ssl';
            $mail->SMTPAuth   = true;
            $mail->SMTPOptions = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'allow_self_signed' => true
                )
            );
            //Definimos la cuenta que vamos a usar
            $mail->Username   = $usu_mail;
            //Introducimos nuestra contraseña 
            $mail->Password   = $pas_mail;
            $mail->From =$from_mail;
            $mail->FromName = $from_mail;
            $mail->AddAddress($correo, 'El Destinatario');
            $mail->Subject = $subject;
            $mail->isHTML(true);              
            $mail->MsgHTML($mensa);  
            $mail->Send();

        return true;
    
}
//FUNCION GENERA LISTAS DESPLEGABLES CON SELECT
    public function drop_down_selec($tabla,$select,$condicion){
        $id=explode("_", $tabla);
        $res_con=$this->conecta_bd();
        $consulta="select * from $tabla $condicion";
        if ($resultado=$res_con->query($consulta)) {
            if(isset($id[2])){
                 while ($fila = $resultado->fetch_assoc()) {?>
                    <option value='<?php  echo $fila["id_$id[1]_$id[2]"]; ?>'<?php if($fila["id_$id[1]_$id[2]"]==$select){ echo "selected";} ?>><?php  echo utf8_encode($fila["nombre"]); ?></option>
                 <?php }
            }else{
             while ($fila = $resultado->fetch_assoc()) {?>
                    <option value='<?php  echo $fila["id_$id[1]"]; ?>' <?php if($fila["id_$id[1]"]==$select){ echo "selected";} ?>><?php  echo utf8_encode($fila["nombre"]); ?></option>
                <?php } 
            $resultado->free();
        }
        $res_con->close();
    }
  }
//FUNCION GENERA LISTAS DESPLEGABLES
    public function drop_down($tabla){
        $id=explode("_", $tabla);
        $res_con=$this->conecta_bd();
        $consulta="select * from $tabla where activo=0";
        if ($resultado=$res_con->query($consulta)) {
            if(isset($id[2])){
                while ($fila = $resultado->fetch_assoc()) {?>
                    <option value='<?php  echo $fila["id_$id[1]_$id[2]"]; ?>'><?php  echo utf8_encode($fila["nombre"]); ?></option>
                <?php } 
            }else{ 
                 while ($fila = $resultado->fetch_assoc()) {?>
                    <option value='<?php  echo $fila["id_$id[1]"]; ?>'><?php  echo utf8_encode($fila["nombre"]); ?></option>
               <?php  } 
           }   
           $resultado->free();
        }
        $res_con->close();
    }

//CONSULTA GENERICA TRAE TODOS LOS DATOS DE TABLA REQUERIDA CON CONDICION
public function consulta_generica($tabla,$condicion){
    $res_con=$this->conecta_bd();
    $consulta="select * from $tabla $condicion";
    //echo $consulta;
    $resultado=$res_con->query($consulta);
    return $resultado;
    
}  
//CONSULTA GENERICA TRAE TODOS LOS DATOS DE TABLA REQUERIDA CON CONDICION
public function consulta_generica2($tabla,$condicion){
    $res_con=$this->conecta_bd2();
    $consulta="select * from $tabla $condicion";
    //echo $consulta;
    $resultado=$res_con->query($consulta);
    return $resultado;    
}  
//CONSULTA GENERICA ALL
public function consulta_generica_all($consult){
    $res_con=$this->conecta_bd();
    $consulta=$consult;
    //echo $consulta;
    $resultado=$res_con->query($consulta);
    return $resultado;
    
} 
//CONSULTA GENERICA ST
public function consulta_generica_st($consult){
   $res_con=$this->conecta_bd();
   $consulta=$consult;
   $resultado=$res_con->query($consulta);
   $r=mysqli_fetch_assoc($resultado);
    return $r;
} 
//UPDATE GENERICO MODIFICA REGISTRO DEPENDIENDO CONDICION
public function update_generico($tabla,$columnas,$condicion){
    $res_con=$this->conecta_bd();
    $consulta="update $tabla set $columnas $condicion";
    $resultado=$res_con->query($consulta);
    return $resultado;
    
}
//FUNCION CONECTA BASEDE DATOS
public function conecta_bd(){
        include '../../inc/parametros.php';
        $con=new mysqli($ser,$usu,$pas,$bd);
        if ($con->connect_errno)
        {
             printf("Conexión fallida: %s\n", $con->connect_error);
            exit();
        }else{
            return $con;
        }
    }
//FUNCION CONECTA BASEDE DATOS
public function conecta_bd2(){
        include './inc/parametros.php';
        $con=new mysqli($ser,$usu,$pas,$bd);
        if ($con->connect_errno)
        {
             printf("Conexión fallida: %s\n", $con->connect_error);
            exit();
        }else{
            return $con;
        }
    }
//FUNCION FORMATEA FECHA DE CALENDATIO PARA INSERTAR A BD
   public function formatea_fecha($fecha){
    $arr = explode('/', $fecha);
    $n_fecha = $arr[2].'-'.$arr[1].'-'.$arr[0];
        return $n_fecha;
   }
}
