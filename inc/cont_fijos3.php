<?php


if(isset($_SESSION["usuario"])){ 
    $catalogue = 1;
    //echo "<script>console.log('si estas logueado');</script>";
}else{
    $catalogue = 0;
    //echo "<script>console.log('No estas logueado');</script>";
}

class Cont_fijos{
    private $ser;
    private $usu;
    private $pas;
    private $bd;

     public function __construct($ser,$usu,$pas,$bd) {
         $this->ser=$ser;
         $this->usu=$usu;
         $this->pas=$pas;
         $this->bd=$bd;
        
     }
    public function header(){
        session_start();
        if(isset($_SESSION)){ 

?>
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Please wait...</p>
            </div>
        </div>
        <!-- #END# Page Loader -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->
        <!-- Top Bar -->
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href="./index.php">Plataforma de Lealtad</a>
                </div>
                <?php if(isset($_SESSION["rol"]) && $_SESSION["rol"] == 1 ){ ?>
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                        </ul>
                    </div>
                    <aside id="rightsidebar" class="right-sidebar">
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            <li role="presentation" class="active" style='width: 100%!important;'><a href="#skins" data-toggle="tab"> <i class="fa fa-cog" style="margin-top: 9px!important;"></i> Opciones</a></li>
                            <!--<li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li> -->
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                                <ul class="demo-choose-skin">
                                    <li style="">
                                        <a id="administracion/admin_usuarios" onclick="menu(this.id);" class=" waves-effect waves-block" style="    color: black!important;">
                                            <span><i class="fa fa-users" style="margin-top: 9px!important;"></i>Usuarios</span>
                                        </a>
                                    </li>
                                    <li style="">
                                        <a id="administracion/admin_niveles" onclick="menu(this.id);" class=" waves-effect waves-block" style="    color: black!important;">
                                            <span><i class="fa fa-star" style="margin-top: 9px!important;"></i>Niveles</span>
                                        </a>
                                    </li>
                                    <li style="">
                                        <a id="administracion/admin_sucursales" onclick="menu(this.id);" class=" waves-effect waves-block" style="    color: black!important;">
                                            <span><i class="fa fa-store" style="margin-top: 9px!important;"></i>Sucursales</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </aside>
                <?php } ?>
                <?php if(isset($_SESSION["rol"]) && $_SESSION["rol"] == 3 ){ ?>
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></li>
                        </ul>
                    </div>
                    <aside id="rightsidebar" class="right-sidebar">
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            <li role="presentation" class="active" style='width: 100%!important;'><a href="#skins" data-toggle="tab"> <i class="fa fa-cog" style="margin-top: 9px!important;"></i>&nbsp;&nbsp;Configuración</a></li>
                            <!--<li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li> -->
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                                <ul class="demo-choose-skin">
                                    <li style="">
                                        <a id="btnEditPerfil" data-action="<?php echo $_SESSION["id_usuario"] ?>" class=" waves-effect waves-block" style="color: black!important;">
                                            <span><i class="fa fa-users" style="margin-top: 9px!important;"></i>Mi perfil</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </aside>
                <?php } ?>
            </div>
        </nav>
        <!-- #Top Bar -->
<?php   
        }
    }//end function

    public function head(){ 
        require_once '../../inc/funciones.php';
        $ins_funciones=new Funciones_Basicas();

        $res_con1=$ins_funciones->consulta_generica('tbl_estilo', ' ');
        $registro1= mysqli_fetch_assoc($res_con1);

        $res_con2=$ins_funciones->consulta_generica('tbl_configuracion_proyecto', ' ');
        $config = mysqli_fetch_assoc($res_con2);

        //VALIDAMOS LOGO
        if($registro1['logo_cte']==''){
            $img_logo1='imagenes/logo.png';
        }else{
            $img_logo1='imagenes/img_configuracion/'.$registro1['logo_cte'];
        }
?>  
            
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name='viewport' content='width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1'/>    
    <link rel="icon" sizes="192x192" href="../../inc/<?php echo $img_logo1 ?>">
    <title>:: <?php echo $config['titulo_home'] ?> :: <?php echo $config['detalle_home'] ?> </title>
    
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    <!-- Bootstrap Core Css -->
    <link href="../../inc/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- Waves Effect Css -->
    <link href="../../inc/plugins/node-waves/waves.css" rel="stylesheet" />
    <!-- Animation Css -->
    <link href="../../inc/plugins/animate-css/animate.css" rel="stylesheet" />
    <!-- Colorpicker Css -->
    <link href="../../inc/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" />
    <!-- Dropzone Css -->
    <link href="../../inc/plugins/dropzone/dropzone.css" rel="stylesheet" />
      <!-- Multi Select Css -->
    <link href="../../inc/plugins/multi-select/css/multi-select.css" rel="stylesheet">
    <!-- Bootstrap Spinner Css -->
    <link href="../../inc/plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet">
    <!-- Bootstrap Tagsinput Css -->
    <link href="../../inc/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">
    <!-- Bootstrap Select Css -->
    <link href="../../inc/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <!-- noUISlider Css -->
    <link href="../../inc/plugins/nouislider/nouislider.min.css" rel="stylesheet" />
    <!-- Custom Css -->
    <link href="../../inc/css/style.css" rel="stylesheet"/>
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../../inc/css/themes/all-themes.css" rel="stylesheet" />
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="../../inc/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
    <!-- Wait Me Css -->
    <link href="../../inc/plugins/waitme/waitMe.css" rel="stylesheet" />
    <!-- Morris Chart Css-->
    <link href="../../inc/plugins/morrisjs/morris.css" rel="stylesheet" />    
    <!-- JQuery DataTable Css -->
    <link href="../../inc/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet"/>
    <!-- Before -->
    <!-- Que es esto de banco? --ª
    <--link href="../../inc/css/banco.css" rel="stylesheet"-->

    <!-- bootstrap y jquery-->
    
    <!--script src="../../inc/bootstrap/js/bootstrap.js"></script-->
    <!--<script src="../../inc/bootstrap/js/bootstrap-typeahead.js"></script>-->     
    <!-- JS GLOBAL DEL SISTEMA -->
    <!--script src="../../inc/js/global.js"></script-->
    <!-- sweet alert -->
    <link rel="stylesheet" href="../../inc/css/sweet-alert2.css" />
    <!-- Grid -->    
    <!--link rel="stylesheet" href="../../inc/css/jquery.dataTables.min.css"-->
     <!--CALENDARIO -->
    <!--script src="../../inc/js/jquery-ui.js"></script-->
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link rel="stylesheet" href="../../inc/fontawesome_5.2/css/all.css"/>
    <!--link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous"-->
    <!--link rel="stylesheet" href="../../inc/css/build.css"/-->
   
    <!-- GRAFICAS -->
    <style type="text/css">        
        /*body{
            font-family: Verdana, Geneva, sans-serif;
        }*/

        .boxAnd{
            border:1px solid gray; padding:8px; border-radius:8px; background-color:white;
        }

        .btnORCondition{
            font-size: 70% !important;
            width: auto !important;
        }

        .btnANDCondition{
            font-size: 70% !important;
            width: auto !important;
        }

        /* Safari 4.0 - 8.0 */
        @-webkit-keyframes example {
          0%   {transform: rotate(0deg);}          
          100% {transform: rotate(360deg);}
        }

        /* Standard syntax */
        @keyframes example {
          0%   {transform: rotate(0deg);}          
          100% {transform: rotate(360deg);}
        }

        .rotateIcon {                 
          -webkit-animation-name: example; /* Safari 4.0 - 8.0 */
          -webkit-animation-duration: 0.6s; /* Safari 4.0 - 8.0 */
          -webkit-animation-iteration-count: infinite; /* Safari 4.0 - 8.0 */
          animation-name: example;
          animation-duration: 0.6s;
          animation-iteration-count: infinite;
          animation-timing-function: linear;
          
        }

        /* Safari 4.0 - 8.0 */
        @-webkit-keyframes btnAnimation {
          0%   {-webkit-box-shadow: 5px 5px 8px 5px transparent;}
          50%  {-webkit-box-shadow: 5px 5px 8px 5px rgba(255,255,255,0.5);}
          100% {-webkit-box-shadow: 5px 5px 8px 5px transparent;}
        }

        /* Standard syntax */
        @keyframes btnAnimation {
          0%   {box-shadow: 5px 5px 8px 5px transparent;}
          50%  {box-shadow: 5px 5px 8px 5px rgba(255,255,255,0.5);}
          100% {box-shadow: 5px 5px 8px 5px transparent;}
        }

        .btnHome {
            width:100%;
            border-radius: 30px !important;
            border:2px solid black;

            -webkit-animation-name: btnAnimation; /* Safari 4.0 - 8.0 */
            -webkit-animation-duration: 2s; /* Safari 4.0 - 8.0 */
            -webkit-animation-iteration-count: infinite; /* Safari 4.0 - 8.0 */
            -webkit-animation-timing-function: ease-out;
            animation-name: btnAnimation;
            animation-duration: 2s;
            animation-iteration-count: infinite;
            animation-timing-function: ease-out;          
        }

        .rowMedia{
            /*border-bottom: 1px solid gray;*/
        }

        .rowMedia:hover{
            background-color:#ababab;            
        }

        .rowMedia:hover > h4, .rowMedia:hover > p  {
            color:white;
        }

        .media {
            margin-top: 12px !important;
            margin-bottom: 12px;
        }

        .cellNumber{
            padding:25px;            
            border:1px solid white;
            color:white;
            background-color: black
        }

        /*.btnHome{
            font-size:2rem !important; 
            border-radius:15px; 
            width:100%;
            font-weight: 900;

            -webkit-transition: width 1s, background 1s;
            -moz-transition: width 1s, background 1s;
            -o-transition: width 1s, background 1s;
            transition: width 1s, background 1s;
        }

        .btnHome {
            -webkit-transition-timing-function: ease-in;
            -moz-transition-timing-function: ease-in;
            -o-transition-timing-function: ease-in;
            transition-timing-function: ease-in;
        }

        .btnHome:hover {
            width: 300px;
            background: #428bca !important;
        }*/

       
    </style>
           
    <?php }


    public function head_movil(){ ?>  
            
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Configuración - Plataforma de Lealtad</title>
    <!-- Favicon-->
    <link rel="icon" sizes="192x192" href="../../inc/<?php echo $img_logo1 ?>">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../../inc/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../../inc/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../../inc/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Colorpicker Css -->
    <link href="../../inc/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" />


    <!-- Dropzone Css -->
    <!--link href="../../inc/plugins/dropzone/dropzone.css" rel="stylesheet" />

    <!-- Multi Select Css -->
    <link href="../../inc/plugins/multi-select/css/multi-select.css" rel="stylesheet">

    <!-- Bootstrap Spinner Css -->
    <link href="../../inc/plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet">


    <!-- Bootstrap Tagsinput Css -->
    <link href="../../inc/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">   

    <!-- Bootstrap Select Css -->
    <link href="../../inc/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- noUISlider Css -->
    <link href="../../inc/plugins/nouislider/nouislider.min.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../../inc/css/style.css" rel="stylesheet"/>   

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../../inc/css/themes/all-themes.css" rel="stylesheet" />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="../../inc/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

    <!-- Wait Me Css -->
    <link href="../../inc/plugins/waitme/waitMe.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="../../inc/plugins/morrisjs/morris.css" rel="stylesheet" />
    
    <!-- JQuery DataTable Css -->
    <!--link href="../../inc/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet"/>




    <!-- Before -->
    <!-- Que es esto de banco? --ª
    <--link href="../../inc/css/banco.css" rel="stylesheet"-->

    <!-- bootstrap y jquery-->
    
    <!--script src="../../inc/bootstrap/js/bootstrap.js"></script-->
    <!--<script src="../../inc/bootstrap/js/bootstrap-typeahead.js"></script>-->
     
    <!-- JS GLOBAL DEL SISTEMA -->
    <!--script src="../../inc/js/global.js"></script-->
    <!-- sweet alert -->
    <link rel="stylesheet" href="../../inc/css/sweet-alert2.css" />
     <!-- Grid -->
    
    <!--link rel="stylesheet" href="../../inc/css/jquery.dataTables.min.css"-->
     <!--CALENDARIO -->
    <!--script src="../../inc/js/jquery-ui.js"></script>
    <!--link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"-->

    <link rel="stylesheet" href="../../inc/fontawesome_5.2/css/all.css"/>
    <!--link rel="stylesheet" href="../../inc/css/build.css"/-->
   
    <!-- GRAFICAS -->
    
           
    <?php }

     
    public function menu($idusu,$img_logo){

        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $sql="SELECT tbl_area.*,tbl_usuario.*,tbl_area.nombre as seccion,tbl_rol.nombre as tipo_rol
            FROM tbl_rel_usuario_area 
            INNER JOIN tbl_usuario on tbl_usuario.id_usuario=tbl_rel_usuario_area.id_usuario 
            INNER JOIN tbl_area on tbl_area.id_area=tbl_rel_usuario_area.id_area
            INNER JOIN tbl_rol on tbl_usuario.rol=tbl_rol.id_tbl_rol
            WHERE tbl_rel_usuario_area.id_usuario='$idusu' AND tbl_area.activo=0";
        $sql_rol="SELECT tbl_usuario.*,tbl_rol.* FROM tbl_usuario
                INNER JOIN tbl_rol on tbl_usuario.rol=tbl_rol.id_tbl_rol 
                where tbl_usuario.id_usuario='$idusu'";
        $query_rol=mysqli_query($res_con,$sql_rol);
        $rol=mysqli_fetch_assoc($query_rol);
        $query=mysqli_query($res_con,$sql);


        
        ?>

        <section id="<?php $rol; ?>">
        <!-- Left Sidebar -->
            <aside id="leftsidebar" class="sidebar">
                <!-- User Info -->
                <div class="user-info">
                    <div class="image">
                        <img src="../../inc/imagenes/user.png" width="48" height="48" alt="User" />
                        <img src="../../inc/<?php echo $img_logo; ?>" style='    margin-left: 130px;' width="60" height="60" alt="Logo" />
                    </div>
                    <div class="info-container">
                        <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION["usuario"];?></div>
                        <!--<div class="email">john.doe@example.com</div>-->
                    </div>
                </div>
                <!-- #User Info -->
                <!-- Menu -->
                <div class="menu">
                    <ul class="list">
                        <li class="header"><?php echo utf8_encode($rol['nombre']); ?></li>
                        <li class="active">
                            <a id="btnIndex" href="javascript:;">
                                <i class="material-icons">home</i>
                                <span>Home</span>
                            </a>   
                        </li>
                       
                        <?php 
                            while($reg=mysqli_fetch_assoc($query)){
                        ?>
                            <li>
                                <a href="javascript:;" class="menu-toggle">
                                    <i class="fa fa-<?php echo $reg['icono']; ?>" style='margin-top: 9px!important;'></i>
                                    <span> <?php echo utf8_encode($reg['seccion']); ?></span>
                                </a>    
                        <?php   
                                $this->submenu($reg['id_area']);
                        ?> 
                            </li>
                        <?php        
                            } 
                        ?>         
                        <li class="" id="<?php echo $reg['idarea']; ?>">
                            <a href="../../controller/login/c_logout.php">
                                <?php echo $reg['icono']; ?>
                                <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
                                <span>Salir</span>
                            </a>
                        </li>
                    </ul>    
                </div>
                <div class="legal">
                    <div class="copyright">
                        &#169; Copyright 2018 <br>Loyalty Solutions
                    </div>
                </div>
            </aside>
        </section>    
    <?php       
} //end function

    public function submenu($idarea){
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $sql="SELECT  * FROM tbl_pantalla WHERE id_area=$idarea AND activo=0 order by orden asc";
        $query=mysqli_query($res_con,$sql);

        $sql2="SELECT sucursales,menus,promociones,niveles FROM tbl_configuracion_proyecto";
        $query2=mysqli_query($res_con,$sql2);

        while($reg2=mysqli_fetch_assoc($query2)){
            $suc = $reg2['sucursales'];
            $mnu = $reg2['menus'];
            $pmo = $reg2['promociones'];
            $lvl = $reg2['niveles'];
        } 
        ?>
        <ul class="ml-menu">
            <?php
            while($reg=mysqli_fetch_assoc($query)){
                $pagina = $reg['ruta'];
                $pagina = strtolower($pagina);
                $icono = $reg['icono'];
                $nombre = $reg['nombre'];

                //comparation to view menu element
                if(($suc=='0' OR $suc==0) AND $pagina=='administracion/admin_sucursales'){
                    $style='display:none;';
                }elseif(($mnu=='0' OR $mnu==0) AND $pagina=='administracion/admin_menu'){
                    $style='display:none;';
                }elseif(($pmo=='0' OR $pmo==0) AND $pagina=='administracion/admin_promociones'){
                    $style='display:none;';
                }elseif(($lvl=='0' OR $lvl==0) AND $pagina=='administracion/admin_niveles'){
                    $style='display:none;';
                }else{
                    $style='';
                }
                ?>
                 <li style="<?php echo $style;?>">
                    <a id="<?php echo $pagina; ?>" onclick="menu(this.id);">
                        <span>
                            <i class="fa fa-<?php echo $icono; ?>" style='margin-top: 9px!important;'></i>
                            <?php echo utf8_encode($nombre); ?>
                        </span>
                    </a>
                </li>
                <?php 
            } 
            ?>
        </ul>
        <?php  
    }

    public function estilos($img_logo,$img_background,$color_primario,$color_secundario){ 
?>
        <style>
            /* ESTILOS SEGUN LA CONFIGURCION */
            .primary-back{
                background-color: <?php echo $color_primario; ?> !important;
            }
            .primary-color{
                color:<?php echo $color_primario ?> !important;
            }

            .boxSegmento{
                border: 1px solid <?php echo $color_primario; ?>;
                border-radius:5px;
            }

            .second-back{
                background-color: <?php echo $color_secundario; ?> !important;
            }
            .second-color{
                color:<?php echo $color_secundario ?> !important;
            }

            .border-color{
                border: solid 2px <?php echo $color_primario ?> !important;
                outline: 2px solid <?php echo $color_primario ?> !important;
            }

            .btn_color{
                background:<?php echo $color_primario ?> !important;
                color:<?php echo $color_secundario ?> !important;
                padding:8px !important;
                font-size: 1.1rem !important;
            }

            .btn_color_sec{
                background:<?php echo $color_primario ?> !important;
                color: #fff !important;
                padding:8px !important;
                font-size: 1.1rem !important;
            }

            /* ESTILO COLOR TEXTO */
            .text_color{                
                color:<?php echo $color_primario ?> !important;
            }

            /*ESTILO COLOR DE BARRA TOP */
            .navbar {
                background-color: <?php echo $color_primario; ?>!important;
            }
            /*ESTILO INFORMACION CUENTA */
            .sidebar .user-info {
                padding: 13px 15px 12px 15px;
                white-space: nowrap;
                position: relative;
                border-bottom: 1px solid #e9e9e9;
                background: url(../../inc/<?php echo $img_background; ?>) no-repeat no-repeat;
                height: 135px;
                background-size: cover;
            }
            /*ESTILOS DE LISTAS */
            .sidebar .menu .list a {
                color: <?php echo $color_secundario; ?>!important;
                position: relative;
                display: inline-flex;
                vertical-align: middle;
                width: 100%;
                padding: 10px 13px;
            }
            /*ESTILOS PARA LISTA ACTIVA */
            .theme-red .sidebar .menu .list li.active {
              background-color: transparent; }
            .theme-red .sidebar .menu .list li.active > :first-child i, .theme-red .sidebar .menu .list li.active > :first-child span {
                color:<?php echo $color_primario; ?>!important; 
            }
            /*ESTILOS PARA LINEA EN FORMULARIOS*/
            .form-group .form-line:after {
                content: '';
                position: absolute;
                left: 0;
                width: 100%;
                height: 0;
                bottom: -1px;
                -moz-transform: scaleX(0);
                -ms-transform: scaleX(0);
                -o-transform: scaleX(0);
                -webkit-transform: scaleX(0);
                transform: scaleX(0);
                -moz-transition: 0.25s ease-in;
                -o-transition: 0.25s ease-in;
                -webkit-transition: 0.25s ease-in;
                transition: 0.25s ease-in;
                border-bottom: 2px solid <?php echo $color_secundario; ?>!important;
            }  
            /*ESTILOS PARA BOTON DE COLOR*/
            .btn_color{
                 background-color: <?php echo $color_primario; ?>!important;
            }

            .btn_color_txt_w{
                background:<?php echo $color_primario ?> !important;
                color: #fff !important;
            }

            .color-circle{
                border:20px solid <?php echo $color_primario; ?> !important;
            }
        </style>  
<?php 
    }

    public function estilos_home($img_logo,$img_background,$color_primario,$color_secundario){ 
?>
        <style>
        /* ESTILOS SEGUN LA CONFIGURCION */
        /*ESTILOS DE LOS BOTONES*/
        .btn_color{
            background:<?php echo $color_primario ?> !important;
            color:<?php echo $color_secundario ?> !important;
        }

        .text_color{            
            color:<?php echo $color_primario ?> !important;
        }

        .login-page {
            padding-left: 0;
            max-width: 360px;
            margin: 10% auto;
            overflow-x: hidden;
            width: 100%;
            height: 400px;
            background: url(./inc/<?php echo $img_background; ?>) no-repeat ;
            background-repeat: no-repeat;
            background-size: cover;
           
        }   
        .navbar {
            background-color: #95542E!important;
        }
        a{
                color: white!important;
        }
        /*ESTILO COLOR DE BARRA TOP */
        .navbar {
            background-color: <?php echo $color_primario; ?>!important;
        }
        /*ESTILO INFORMACION CUENTA */
        .sidebar .user-info {
            padding: 13px 15px 12px 15px;
            white-space: nowrap;
            position: relative;
            border-bottom: 1px solid #e9e9e9;
            background: url(../../inc/<?php echo $img_background; ?>) no-repeat ;
            height: 135px;
            background-size: cover;
        }
        /*ESTILOS DE LISTAS */
        .sidebar .menu .list a {
            color: <?php echo $color_secundario; ?>!important;
            position: relative;
            display: inline-flex;
            vertical-align: middle;
            width: 100%;
            padding: 10px 13px;
        }
        /*ESTILOS PARA LISTA ACTIVA */
        .theme-red .sidebar .menu .list li.active {
          background-color: transparent; 
        }
        .theme-red .sidebar .menu .list li.active > :first-child i, .theme-red .sidebar .menu .list li.active > :first-child span { 
            color:<?php echo $color_primario; ?>!important; 
        }
        /*ESTILOS PARA LINEA EN FORMULARIOS*/
        .form-group .form-line:after {
            content: '';
            position: absolute;
            left: 0;
            width: 100%;
            height: 0;
            bottom: -1px;
            -moz-transform: scaleX(0);
            -ms-transform: scaleX(0);
            -o-transform: scaleX(0);
            -webkit-transform: scaleX(0);
            transform: scaleX(0);
            -moz-transition: 0.25s ease-in;
            -o-transition: 0.25s ease-in;
            -webkit-transition: 0.25s ease-in;
            transition: 0.25s ease-in;
            border-bottom: 2px solid <?php echo $color_secundario; ?>!important;
        }  
        /*ESTILOS PARA BOTON DE COLOR*/
        .btn_color{
             background-color: <?php echo $color_primario; ?>!important;
          
        }
        .color-circle{
            border:20px solid <?php echo $color_primario; ?> !important;
        }
    </style>  
<?php 
    }

public function estilos_front($img_logo,$img_background,$color_primario,$color_secundario){
    ?>
    <style>
        .btn_color{
            background:<?php echo $color_primario ?> !important;
            color:<?php echo $color_secundario ?> !important;
        }

        .text_color{
            color:<?php echo $color_primario ?> !important;
        }

        section.content {
            max-width: 80%!important;
      
        }
        .card .card-inside-title {
            color:white!important;           
        }
        .card2 {
            /*background: <?php echo $color_secundario ?>!important;*/
            color:white!important;
            min-height: 50px!important;
            box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2)!important;
            position: relative!important;
            margin-bottom: 0px 0px 30px 30px!important;
            -webkit-border-radius: 0px 0px 30px 30px!important;
            -moz-border-radius: 0px 0px 30px 30px!important;
            -ms-border-radius: 0px 0px 30px 30px!important;
            border-radius:0px 0px 30px 30px!important;
            background-color:rgba(0,0,0,0.6);
            
        }
        .cabecera{
            height: 70px;
            background:<?php echo $color_primario ?>;
            color: white;
            text-align: center;
            padding-top: 15px;
            font-size: 23px;
            border-radius: 35px 35px 0px 0px;
            -moz-border-radius: 35px 35px 0px 0px;
            -webkit-border-radius:  35px 35px 0px 0px;
            -webkit-border-radius:35px 35px 0px 0px;
        }
        @media screen and (max-width: 599px) {
            .cabecera{
                height: 70px;
                background:<?php echo $color_primario ?>;
                color: white;
                text-align: center;
                padding-top: 15px;
                font-size: 14px;
                border-radius: 35px 35px 0px 0px;
                -moz-border-radius: 35px 35px 0px 0px;
                -webkit-border-radius:  35px 35px 0px 0px;
                -webkit-border-radius:35px 35px 0px 0px;
            }
        }   

        .form_front{
            background: url(../../inc/<?php echo $img_background ?>) no-repeat no-repeat;
            background-size: auto auto;

            background-size: cover;  
        }
        body{
            background: transparent!important;
        }

        .color-circle{
            border:20px solid <?php echo $color_primario; ?> !important;
        }
    </style>
    <?php
}

//FUNCION REGRESA SALDO
public function saldo($arr){
    $saldo=array_sum($arr);
return  $saldo;  
}

public function catalogo_productos($catalogue,$tipo_proyecto, $idUsuario = NULL){ 
    require_once('../../controller/registro/c_registro.php');
    $ins_funciones=new Funciones_Basicas();
    if($catalogue==0){
        $textcolor = "text-white";
        $qr_catalogo_premios=$ins_funciones->consulta_generica_all(' SELECT * FROM tbl_cat_premios_productos_servicios WHERE activo=0 ORDER BY RAND() LIMIT 3');
        
    }else{
        //MUESTRA CATALOGO CUANDO ESTA LOGUEADO UN USUARIO
        $textcolor = "text-black";
        $qr_catalogo_premios=$ins_funciones->consulta_generica_all(' SELECT * FROM tbl_cat_premios_productos_servicios WHERE activo=0');
    }
    //die("Lego");
    $inst_registro = new C_registro($this->ser, $this->usu, $this->pas, $this->bd);
    //print_r($inst_registro);
    //die();
    //echo ("Usuario: ".$idUsuario);
    $res_productos = $inst_registro->get_productos_promociones($idUsuario);
    //print_r($res_productos);
    //die();
    ?>
    <div class="card">
        <div class="header">
            <h2 class="<?php echo $textcolor; ?>">
                Catalogo de Productos para Redimir
            </h2>
        </div>
        <div class="body">
            <div class="row">                
                <?php foreach($res_productos['productos'] as $producto){ ?>
                    <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
                        <div class="thumbnail" style="height:250px; border-radius:5px; border: 3px dashed orange">
                            <img src="../../inc/imagenes/img_catalogo/<?php echo utf8_encode($producto['imagen']); ?>"  style="max-height:100px">
                            <div class="caption">
                                <h3 class="text-center"><u><?php echo $producto['nombre'] ?></u></h3>
                                <?php 
                                    switch ($tipo_proyecto){
                                        //PROYECTO  CON PUNTOS
                                        case 1:
                                        case 2:
                                            echo '<p class="text-center">'.utf8_encode($producto['valor_puntos']).' Ptos </p>';
                                            echo '<p class="text-center">'.utf8_encode($producto['restantes']).' Restantes </p>';
                                        break;
                                        //PROYECTO POR VISTA
                                        case 3:
                                            echo '<p class="text-center">'.utf8_encode($producto['valor_visitas']).' Visitas </p>';
                                            echo '<p class="text-center">'.utf8_encode($producto['restantes']).' Restantes </p>';
                                        break;
                                        
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                    <!--div class="col-xs-9 col-sm-6 col-md-4 col-lg-4 justify-content-center">
                        <div class="thumbnail circlex color-circle">
                            <div class="img-catalogue">
                                <img class="img-fluid" src="../../inc/imagenes/img_catalogo/<?php echo utf8_encode($producto['imagen']); ?>" width="100%">
                            </div>
                            
                            <div class="caption txt-catalogue">
                                <h3><?php echo $producto['nombre'] ?></h3>
                                <h6><?php 
                                    switch ($tipo_proyecto){
                                        //PROYECTO  CON PUNTOS
                                        case 1:
                                        case 2:
                                            echo utf8_encode($producto['valor_puntos']).' Ptos';
                                        break;
                                        //PROYECTO POR VISTA
                                        case 3:
                                            echo utf8_encode($producto['valor_visitas']).' Visitas';
                                        break;
                                        
                                    }
                                
                                
                                ?></h6>
                                <div class="detalle_prod">
                                    <small ><?php //echo utf8_encode($fila['detalle']) ?> </small><br><br>
                                </div> 
                            </div>
                        </div>
                    </div-->
                <?php   } ?>
            </div>
        </div>
    </div>
    <?php if(count($res_productos['cashback']) > 0){ ?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="card">
            <div class="header">
                <h2 class="<?php echo $textcolor; ?>">
                    Promociones Disponibles
                </h2>
            </div>
            <div class="body">
                <div class="row justify-content-center">
                    <?php                     
                       foreach($res_productos['cashback'] as $cashback){  
                            
                            ?>
                            <div class="col-xs-9 col-sm-6 col-md-4 col-lg-4 justify-content-center">
                                <div class="thumbnail circlex color-circle">
                                    <div class="img-catalogue">
                                        <img class="img-fluid" src="../../inc/imagenes/gift-flat.svg" width="100%">
                                    </div>
                                    
                                    <div class="caption txt-catalogue">
                                        <h3><?php echo utf8_encode(ucwords($cashback['nombre'])) ?></h3>
                                        <h6>Vigencia del <?php echo  $cashback['comienza'] ?> hasta <?php echo  $cashback['vigencia'] ?> </h6>
                                        <!--div class="detalle_prod">
                                            <small ><?php //echo utf8_encode($fila['detalle']) ?> </small><br><br>
                                        </div--> 
                                    </div>
                                </div>
                            </div>
                    <?php   } ?>
                    <?php                     
                       foreach($res_productos['promociones'] as $promocion){  
                            
                            ?>
                            <div class="col-xs-9 col-sm-6 col-md-4 col-lg-4 justify-content-center">
                                <div class="thumbnail circlex color-circle">
                                    <div class="img-catalogue">
                                        <img class="img-fluid" src="../../inc/imagenes/gift-flat.svg" width="100%">
                                    </div>
                                    
                                    <div class="caption txt-catalogue">
                                        <h3><?php echo utf8_encode(ucwords($promocion['nombre'])) ?></h3>
                                        <h6>Vigencia del <?php echo  $promocion['comienza'] ?> hasta <?php echo  $promocion['vigencia'] ?> </h6>
                                        <!--div class="detalle_prod">
                                            <small ><?php //echo utf8_encode($fila['detalle']) ?> </small><br><br>
                                        </div--> 
                                    </div>
                                </div>
                            </div>
                    <?php   } ?>                
                </div>
            </div>
        </div>
    </div>        
        <?php   } ?>
    
<?php } 
   


    public function conecta_bd($ser,$usu,$pas,$bd,$con){
      $con=mysqli_connect($this->ser,$this->usu,$this->pas,$this->bd);
        if ($con)
        {
            return $con;
            mysqli_close($con);
        }else{
            echo("Error description: " . mysqli_error($con));
                exit(); 
            }
    }
    public function footer(){?>
             
        <!-- Jquery Core Js -->
        <script src="../../inc/plugins/jquery/jquery.min.js"></script>
        <script src="../../inc/js/sweet-alert2.js"></script>
        <script src="../../inc/js/mainmenu.js"></script>
        
        <!-- Bootstrap Core Js -->
        <script src="../../inc/plugins/bootstrap/js/bootstrap.js"></script>

         <!-- Select Plugin Js -->
        <script src="../../inc/plugins/bootstrap-select/js/bootstrap-select.js"></script>

        <!-- Slimscroll Plugin Js -->
        <script src="../../inc/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

        <!-- Bootstrap Colorpicker Js -->
        <script src="../../inc/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>

        <!-- Dropzone Plugin Js -->
        <script src="../../inc/plugins/dropzone/dropzone.js"></script>

        <!-- Input Mask Plugin Js -->
        <script src="../../inc/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

        <!-- Multi Select Plugin Js -->
        <script src="../../inc/plugins/multi-select/js/jquery.multi-select.js"></script>

        <!-- Jquery Spinner Plugin Js -->
        <script src="../../inc/plugins/jquery-spinner/js/jquery.spinner.js"></script>
        
        <!-- Bootstrap Tags Input Plugin Js -->
        <script src="../../inc/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

        <!-- noUISlider Plugin Js -->
        <script src="../../inc/plugins/nouislider/nouislider.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="../../inc/plugins/node-waves/waves.js"></script>

        <!-- Custom Js -->
        <script src="../../inc/js/admin.js"></script>
        <!--script src="../../inc/js/pages/forms/advanced-form-elements.js"></script-->

        <!-- Demo Js -->
        <script src="../../inc/js/demo.js"></script>

        <!-- Morris Plugin Js -->
        <script src="../../inc/plugins/raphael/raphael.min.js"></script>
        <script src="../../inc/plugins/morrisjs/morris.js"></script>

        <!-- ChartJs -->
        <script src="../../inc/plugins/chartjs/Chart.bundle.js"></script>
        <script src="../../inc/plugins/chartjs/Chart.js"></script>

        <!-- Flot Charts Plugin Js -->
        <!--script src="../../inc/plugins/flot-charts/jquery.flot.js"></script>
        <script src="../../inc/plugins/flot-charts/jquery.flot.resize.js"></script>
        <script src="../../inc/plugins/flot-charts/jquery.flot.pie.js"></script>
        <script src="../../inc/plugins/flot-charts/jquery.flot.categories.js"></script>
        <script src="../../inc/plugins/flot-charts/jquery.flot.time.js"></script-->

        <!-- Sparkline Chart Plugin Js >
        <script src="../../inc/plugins/jquery-sparkline/jquery.sparkline.js"></script-->

        <!-- Jquery CountTo Plugin Js -->
        <script src="../../inc/plugins/jquery-countto/jquery.countTo.js"></script>
 
        
        <!-- Custom Js -->
        <script src="../../inc/js/custom.js"></script>
        <script src="../../inc/js/pages/charts/chartjs.js"></script>
        

   <?php  }//end footer

   public function footer_movil(){?>
             
        <!-- Jquery Core Js -->
        <script src="../../inc/plugins/jquery/jquery.min.js"></script>
        <script src="../../inc/js/sweet-alert2.js"></script>
        <script src="../../inc/js/mainmenu.js"></script>
        
        <!-- Bootstrap Core Js -->
        <script src="../../inc/plugins/bootstrap/js/bootstrap.js"></script>

         <!-- Select Plugin Js -->
        <script src="../../inc/plugins/bootstrap-select/js/bootstrap-select.js"></script>

        <!-- Slimscroll Plugin Js -->
        <script src="../../inc/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

        <!-- Bootstrap Colorpicker Js -->
        <!--script src="../../inc/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script-->

        <!-- Dropzone Plugin Js -->
        <script src="../../inc/plugins/dropzone/dropzone.js"></script>

        <!-- Input Mask Plugin Js -->
        <!--script src="../../inc/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script-->

        <!-- Multi Select Plugin Js -->
        <script src="../../inc/plugins/multi-select/js/jquery.multi-select.js"></script>

        <!-- Jquery Spinner Plugin Js -->
        <script src="../../inc/plugins/jquery-spinner/js/jquery.spinner.js"></script>
        
        <!-- Bootstrap Tags Input Plugin Js -->
        <script src="../../inc/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

        <!-- noUISlider Plugin Js -->
        <script src="../../inc/plugins/nouislider/nouislider.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="../../inc/plugins/node-waves/waves.js"></script>

        <!-- Custom Js -->
        <script src="../../inc/js/admin.js"></script>
        <script src="../../inc/js/pages/forms/advanced-form-elements.js"></script>

        <!-- Demo Js -->
        <script src="../../inc/js/demo.js"></script>

        <!-- Morris Plugin Js -->
        <script src="../../inc/plugins/raphael/raphael.min.js"></script>
        <script src="../../inc/plugins/morrisjs/morris.js"></script>

        <!-- ChartJs -->
        <!--script src="../../inc/plugins/chartjs/Chart.bundle.js"></script>
        <script src="../../inc/plugins/chartjs/Chart.js"></script-->

        <!-- Flot Charts Plugin Js -->
        <!--script src="../../inc/plugins/flot-charts/jquery.flot.js"></script>
        <script src="../../inc/plugins/flot-charts/jquery.flot.resize.js"></script>
        <script src="../../inc/plugins/flot-charts/jquery.flot.pie.js"></script>
        <script src="../../inc/plugins/flot-charts/jquery.flot.categories.js"></script>
        <script src="../../inc/plugins/flot-charts/jquery.flot.time.js"></script-->

        <!-- Sparkline Chart Plugin Js >
        <script src="../../inc/plugins/jquery-sparkline/jquery.sparkline.js"></script-->

        <!-- Jquery CountTo Plugin Js -->
        <script src="../../inc/plugins/jquery-countto/jquery.countTo.js"></script>
 
        
        <!-- Custom Js -->
        <script src="../../inc/js/custom.js"></script>
        <script src="../../inc/js/pages/charts/chartjs.js"></script>
        

   <?php  }//end footer_movil
   
}
?>