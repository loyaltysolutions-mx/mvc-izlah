Pure CSS Toggles
----------------
Getting fun with some animations & transitions :)

A [Pen](http://codepen.io/rgg/pen/waEYye) by [Rafael González](http://codepen.io/rgg) on [CodePen](http://codepen.io/).

[License](http://codepen.io/rgg/pen/waEYye/license).