<?php 
session_start();
if(isset($_SESSION["usuario"])){ 
    $catalogue = 1;
    //echo "<script>console.log('si estas logueado');</script>";
}else{
    $catalogue = 0;
    //echo "<script>console.log('No estas logueado');</script>";
}

class Contenidos_Fijos{
    private $ser;
    private $usu;
    private $pas;
    private $bd;
public function __construct($ser,$usu,$pas,$bd) {
         $this->ser=$ser;
         $this->usu=$usu;
         $this->pas=$pas;
         $this->bd=$bd;
     }
       //FUNCION HEADER CONFIGURACION
public function header_config(){?>
        <nav class="navbar navbar-default navbar-fixed-top azul-fuerte" style='position: inherit!important;'>
          <div class="container-fluid text-center">
            <div class="navbar-header row navbar-w">
                <div class="col-md-6 col-xs-12">
                    <h3 class="text-primary "><span class="tit"><img class="config-logo" src="../../inc/imagenes/logo.png"></span></h3>
                </div>
                <div class="col-md-6 col-xs-12">
                    <h3 class="text-primary "><span class="tit">Plataforma de Lealtad</span>
                </div>
                </h3>
            </div>
          </div>
        </nav> 
     <?php }
     //FUNCION HEAD
public function head(){ 

        require_once '../../inc/funciones.php';
        $ins_funciones=new Funciones_Basicas();

        $res_con1=$ins_funciones->consulta_generica('tbl_estilo', ' ');
        $registro1= mysqli_fetch_assoc($res_con1);

        $res_con2=$ins_funciones->consulta_generica('tbl_configuracion_proyecto', ' ');
        $config = mysqli_fetch_assoc($res_con2);

        //VALIDAMOS LOGO
        if($registro1['logo_cte']==''){
            $img_logo1='imagenes/logo.png';
        }else{
            $img_logo1='imagenes/img_configuracion/'.$registro1['logo_cte'];
        }
    ?>  
             <meta http-equiv="content-type" content="text/html; utf-8">
             <meta http-equiv="X-UA-Compatible" content="IE=edge">
             <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
             <meta name="description" content="Plataforma de lealtad">             
             <link rel="icon" sizes="192x192" href="../../inc/<?php echo $img_logo1 ?>">
             <title>:: <?php echo $config['titulo_home'] ?> :: <?php echo $config['detalle_home'] ?> </title>
             <!-- ESTILOS GENERALES DEL SISTEMA-->
              <link href="../../inc/css/estilos.css" rel="stylesheet">
             <!-- Bootstrap core CSS -->
      <!--<link rel="stylesheet" href="../../inc/css/bootstrap.css" >-->
             <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
             <link href="../../inc/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
             <!-- SWEET ALERT2 -->
             <script src="../../inc/js/sweet-alert2.js"></script>
             <!-- <script src="../../inc/js/promise-polyfill.js"></script>-->
             <link rel="stylesheet" href="../../inc/css/sweet-alert2.css">
              
             <!-- FONTAWESOME-->
             <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
   
           <!-- Bootstrap Core Css -->
           <link href="../../inc/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
         <!-- <link href="../../inc/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">-->
             <!-- Waves Effect Css -->
             <link href="../../inc/plugins/node-waves/waves.css" rel="stylesheet" />
             <!-- Animation Css -->
             <link href="../../inc/plugins/animate-css/animate.css" rel="stylesheet" />
             <!-- Morris Chart Css-->
             <link href="../../inc/plugins/morrisjs/morris.css" rel="stylesheet" />
             <!-- Custom Css -->
             <link href="../../inc/css/style.css" rel="stylesheet">
             <link href="../../inc/plugins/dropzone/dropzone.css" rel="stylesheet">
             <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
             <link href="../../inc/css/themes/all-themes.css" rel="stylesheet" />
            <!-- BOOTSTRAP y JQUERY-->

             <script src="../../inc/js/jquery.js"></script>
             <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
             <script src="../../inc/js/bootstrap.js"></script>
            
             
              <!-- JS GLOBAL DEL SISTEMA -->
             <script src="../../inc/js/global.js"></script>          
                       <!-- Jquery Core Js -->
             <!--<script src="../../inc/plugins/jquery/jquery.min.js"></script>-->
             <!-- Bootstrap Core Js -->
             <!--<script src="../../inc/plugins/bootstrap/js/bootstrap.js"></script>-->
             <!-- Select Plugin Js -->
             <script src="../../inc/plugins/bootstrap-select/js/bootstrap-select.js"></script>
             <!-- Slimscroll Plugin Js -->
             <script src="../../inc/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
             <!-- Waves Effect Plugin Js -->
             <script src="../../inc/plugins/node-waves/waves.js"></script>
             <!-- Jquery CountTo Plugin Js -->
             <script src="../../inc/plugins/jquery-countto/jquery.countTo.js"></script>
             <!-- Morris Plugin Js -->
             <script src="../../inc/plugins/raphael/raphael.min.js"></script>
             <script src="../../inc/plugins/morrisjs/morris.js"></script>
             <!-- ChartJs -->
             <script src="../../inc/plugins/chartjs/Chart.bundle.js"></script>
             <!-- Flot Charts Plugin Js -->
             <script src="../../inc/plugins/flot-charts/jquery.flot.js"></script>
             <script src="../../inc/plugins/flot-charts/jquery.flot.resize.js"></script>
             <script src="../../inc/plugins/flot-charts/jquery.flot.pie.js"></script>
             <script src="../../inc/plugins/flot-charts/jquery.flot.categories.js"></script>
             <script src="../../inc/plugins/flot-charts/jquery.flot.time.js"></script>
             <!-- Sparkline Chart Plugin Js -->
             <script src="../../inc/plugins/jquery-sparkline/jquery.sparkline.js"></script>
             <!-- Custom Js -->
             <script src="../../inc/js/admin.js"></script>
             <!--script src="../../inc/js/pages/index.js"></script-->
             <script src="../../inc/js/custom.js"></script>
             <script src="../../inc/js/jquery.touchSwipe.js"></script>
             <!-- GRIDS -->
           <!--<script src="../../inc/plugins/bootstrap-select/js/bootstrap-select.js"></script>-->
           <script src="../../inc/plugins/jquery-datatable/jquery.dataTables.js"></script>
           <script src="../../inc/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
           <script src="../../inc/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
           <script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
           <script src="../../inc/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
           <script src="../../inc/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
           <script src="../../inc/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
           <script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
           <script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
           <script src="../../inc/js/pages/tables/jquery-datatable.js"></script>
        <link href="../../inc/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
        <!-- Chart Js -->
        <script src="../../inc/js/pages/charts/chartjs.js"></script>
        <!-- datapicker-->
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
           
    <?php }

    public function headx(){ ?>  
             <meta http-equiv="content-type" content="text/html; utf-8">
             <meta http-equiv="X-UA-Compatible" content="IE=edge">
             <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
             <meta name="description" content="Plataforma de lealtad">
             <meta name="keywords" content="Plataforma de lealtad">
             <meta name="author" content="Allan Ayrton">
             <link rel="icon" href="../inc/imagenes/favicon.ico" rel="shortcut icon" type="image/x-icon" >
             <title>Plataforma de Lealtad</title>
             <!-- ESTILOS GENERALES DEL SISTEMA-->
              <link href="../../inc/css/estilos.css" rel="stylesheet">
             <!-- Bootstrap core CSS -->
      <!--<link rel="stylesheet" href="../../inc/css/bootstrap.css" >-->
             <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
             <link href="../../inc/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
             <!-- SWEET ALERT2 -->
             <script src="../../inc/js/sweet-alert2.js"></script>
             <!-- <script src="../../inc/js/promise-polyfill.js"></script>-->
             <link rel="stylesheet" href="../../inc/css/sweet-alert2.css">
              
             <!-- FONTAWESOME-->
             <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
            <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
   
           <!-- Bootstrap Core Css -->
           <link href="../../inc/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
         <!-- <link href="../../inc/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">-->
             <!-- Waves Effect Css -->
             <link href="../../inc/plugins/node-waves/waves.css" rel="stylesheet" />
             <!-- Animation Css -->
             <link href="../../inc/plugins/animate-css/animate.css" rel="stylesheet" />
             <!-- Morris Chart Css-->
             <link href="../../inc/plugins/morrisjs/morris.css" rel="stylesheet" />
             <!-- Custom Css -->
             <link href="../../inc/css/style.css" rel="stylesheet">
             <link href="../../inc/plugins/dropzone/dropzone.css" rel="stylesheet">
             <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
             <link href="../../inc/css/themes/all-themes.css" rel="stylesheet" />

    <?php }
    public function footerx(){ ?>  
        <!-- BOOTSTRAP y JQUERY-->
        <script src="../../inc/js/jquery.js"></script>
        <script src="../../inc/plugins/bootstrap.js"></script>

        <!-- JS GLOBAL DEL SISTEMA -->
        <script src="../../inc/js/global.js"></script>          
               <!-- Jquery Core Js -->
        <!-- Flot Charts Plugin Js -->
        <script src="../../inc/plugins/flot-charts/jquery.flot.js"></script>
        <script src="../../inc/plugins/flot-charts/jquery.flot.resize.js"></script>
        <script src="../../inc/plugins/flot-charts/jquery.flot.pie.js"></script>
        <script src="../../inc/plugins/flot-charts/jquery.flot.categories.js"></script>
        <script src="../../inc/plugins/flot-charts/jquery.flot.time.js"></script>
        <!-- Sparkline Chart Plugin Js -->
        <script src="../../inc/plugins/jquery-sparkline/jquery.sparkline.js"></script>
        <!-- Custom Js -->
        <script src="../../inc/js/admin.js"></script>
        <script src="../../inc/js/pages/index.js"></script>
        <script src="../../inc/js/custom.js"></script>
        <script src="../../inc/js/jquery.touchSwipe.js"></script>
        <!-- GRIDS -->
        <!-- Jquery DataTable Plugin Js -->
        <script src="../../inc/plugins/jquery-datatable/jquery.dataTables.js"></script>
        <script src="../../inc/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
        <script src="../../inc/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
        <script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
        <script src="../../inc/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
        <script src="../../inc/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
        <script src="../../inc/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
        <script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
        <script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
        <script src="../../inc/plugins/editable-table/mindmup-editabletable.js"></script>

        <script src="../../inc/js/pages/tables/jquery-datatable.js"></script>

    <?php }
    
//FUNCION CREA MENU
public function menu($idusu){
          $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
          $sql="select tbl_area.*,tbl_usuario.*,tbl_area.nombre as seccion,tbl_rol.nombre as tipo_rol from tbl_rel_usuario_area 
                inner join tbl_usuario on tbl_usuario.id_usuario=tbl_rel_usuario_area.id_usuario 
                inner join tbl_area on tbl_area.id_area=tbl_rel_usuario_area.id_area
                inner join tbl_rol on tbl_usuario.rol=tbl_rol.id_tbl_rol
                where tbl_rel_usuario_area.id_usuario=$idusu and tbl_area.activo=0";
          $sql_rol="select tbl_usuario.*,tbl_rol.* from tbl_usuario
                inner join tbl_rol on tbl_usuario.rol=tbl_rol.id_tbl_rol 
                where tbl_usuario.id_usuario=$idusu";
          $query_rol=mysqli_query($res_con,$sql_rol);
          $rol=mysqli_fetch_assoc($query_rol);
        $query=mysqli_query($res_con,$sql); 
       
        ?>
    <ul class="list">
        <li class="header"><?php echo utf8_encode($rol['nombre']); ?></li>
        <li class="active" >
            <a href="../../view/home/index.php">
                <i class="fas fa-home" style='margin-top: 9px!important;'></i>
                <span>Home</span>
            </a>
        </li>
        <?php 
        while($reg=mysqli_fetch_assoc($query) )
         { ?>
            <a href="javascript:;" class="menu-toggle">
                <i class="fas fa-<?php echo $reg['icono']; ?>" style='margin-top: 9px!important;'></i>
                <span> <?php echo utf8_encode($reg['seccion']); ?></span>
            </a>
        <?php $this->submenu($reg['id_area']);
         } ?>
         <li  >
            <a href="../../controller/login/c_logout.php">
                <i class="fas fa-share-square" style='margin-top: 9px!important;'></i>
                <span>Salir</span>
            </a>
        </li>
  <?php }

//FUNCION SUBMENU
public function submenu($idarea){
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $sql=" SELECT  * FROM tbl_pantalla WHERE id_area=$idarea AND activo=0";
        $query=mysqli_query($res_con,$sql); 
        ?>
        <ul class="ml-menu">
            <?php
            while($reg=mysqli_fetch_assoc($query) )
             { ?>
                 <li>
                    <a href="<?php echo $reg['ruta']; ?>">
                        <span><i class="fas fa-<?php echo $reg['icono']; ?>" style='margin-top: 9px!important;'></i> <?php echo utf8_encode($reg['nombre']); ?></span>
                    </a>
                </li>
          <?php } ?>
           </ul>
   <?php  }
   
//FUCNION CONECTA A BD
public function conecta_bd($ser,$usu,$pas,$bd,$con){
      $con=mysqli_connect($this->ser,$this->usu,$this->pas,$this->bd);
        if ($con)
        {
            return $con;
             mysqli_close($con);
        }else{
           echo("Error description: " . mysqli_error($con));
                 exit(); 
            }
    }
//FUNCION FOOTER
public function footer(){?>
    &#169; Copyright 2018 <br>Loyalty Solutions 
   <?php  }
 //FUNCION FOOTER CONFIGURACION
public function footer_config(){?>
    <nav class="navbar navbar-default navbar-fixed-bottom ">
        &#169; Copyright 2018 Loyalty Solutions | Plataforma de Lealtad Ver. 1.0.0 | <a href='../home/home.php'>Home</a> </span>
    </nav>
   <?php  }
//FUNCION TOMA LOS ESTILOS HOME
public function estilos_home($img_logo,$img_background,$color_primario,$color_secundario){ ?>
<style>
/* ESTILOS SEGUN LA CONFIGURCION */
/*ESTILOS DE LOS BOTONES*/
.btn_color{
    background:<?php echo $color_primario ?> !important;
    color:<?php echo $color_secundario ?> !important;
}

.text_color{    
    color:<?php echo $color_secundario ?> !important;
}

.boxSegmento{
    border: 1px solid <?php echo $color_primario ?> !important;
    margin: 0px 5px;
}

.login-page {
    padding-left: 0;
    max-width: 360px;
    margin: 10% auto;
    overflow-x: hidden;
    width: 100%;
    height: 400px;
    background: url(./inc/<?php echo $img_background; ?>) no-repeat ;
    background-repeat: no-repeat;
    background-size: cover;
   
}   
.navbar {
    background-color: #95542E!important;
}
a{
        color: white!important;
}
/*ESTILO COLOR DE BARRA TOP */
.navbar {
    background-color: <?php echo $color_primario; ?>!important;
}
/*ESTILO INFORMACION CUENTA */
.sidebar .user-info {
    padding: 13px 15px 12px 15px;
    white-space: nowrap;
    position: relative;
    border-bottom: 1px solid #e9e9e9;
    background: url(../../inc/<?php echo $img_background; ?>) no-repeat ;
    height: 135px;
    background-size: cover;
}
/*ESTILOS DE LISTAS */
.sidebar .menu .list a {
    color: <?php echo $color_secundario; ?>!important;
    position: relative;
    display: inline-flex;
    vertical-align: middle;
    width: 100%;
    padding: 10px 13px;
}
/*ESTILOS PARA LISTA ACTIVA */
.theme-red .sidebar .menu .list li.active {
  background-color: transparent; }
  .theme-red .sidebar .menu .list li.active > :first-child i, .theme-red .sidebar .menu .list li.active > :first-child span {
    color:<?php echo $color_primario; ?>!important; }
 /*ESTILOS PARA LINEA EN FORMULARIOS*/
  .form-group .form-line:after {
    content: '';
    position: absolute;
    left: 0;
    width: 100%;
    height: 0;
    bottom: -1px;
    -moz-transform: scaleX(0);
    -ms-transform: scaleX(0);
    -o-transform: scaleX(0);
    -webkit-transform: scaleX(0);
    transform: scaleX(0);
    -moz-transition: 0.25s ease-in;
    -o-transition: 0.25s ease-in;
    -webkit-transition: 0.25s ease-in;
    transition: 0.25s ease-in;
    border-bottom: 2px solid <?php echo $color_secundario; ?>!important;;
}  
/*ESTILOS PARA BOTON DE COLOR*/
.btn_color{
     background-color: <?php echo $color_primario; ?>!important;
}


.color-circle{
    border:20px solid <?php echo $color_primario; ?> !important;
}
</style>  
<?php }

public function estilos_front($img_logo,$img_background,$color_primario,$color_secundario){ ?>
  <style>
  .btn_color{
    background:<?php echo $color_primario ?> !important;
    color:<?php echo $color_secundario ?> !important;
}

.text_color{    
    color:<?php echo $color_secundario ?> !important;
}

.boxSegmento{
    border: 1px solid <?php echo $color_primario ?> !important;
    margin: 0px 5px;
}
    section.content {
  
    max-width: 80%!important;
  
}
  .card .card-inside-title {
     color:white!important;     
          
      }
 .card2 {
    /*background: <?php echo $color_secundario ?>!important;*/
    color:white!important;
    min-height: 50px!important;
    box-shadow: 0 2px 10px rgba(0, 0, 0, 0.2)!important;
    position: relative!important;
    margin-bottom: 0px 0px 30px 30px!important;
    -webkit-border-radius: 0px 0px 30px 30px!important;
    -moz-border-radius: 0px 0px 30px 30px!important;
    -ms-border-radius: 0px 0px 30px 30px!important;
    border-radius:0px 0px 30px 30px!important;
      background-color:rgba(0,0,0,0.6);
        
}
.cabecera{
    height: 70px;
    background:<?php echo $color_primario ?>;
    color: white;
    text-align: center;
    padding-top: 15px;
    font-size: 23px;
    border-radius: 35px 35px 0px 0px;
    -moz-border-radius: 35px 35px 0px 0px;
    -webkit-border-radius:  35px 35px 0px 0px;
    -webkit-border-radius:35px 35px 0px 0px;
}
@media screen and (max-width: 599px) {
    .cabecera{
        height: 70px;
        background:<?php echo $color_primario ?>;
        color: white;
        text-align: center;
        padding-top: 15px;
        font-size: 14px;
        border-radius: 35px 35px 0px 0px;
        -moz-border-radius: 35px 35px 0px 0px;
        -webkit-border-radius:  35px 35px 0px 0px;
        -webkit-border-radius:35px 35px 0px 0px;
    }
}   

 .form_front{
  background: url(../../inc/<?php echo $img_background ?>) no-repeat no-repeat;
  background-size: auto auto;

  background-size: cover;  
}
body{
    background: transparent!important;
}

.color-circle{
    border:20px solid <?php echo $color_primario; ?> !important;
}
  </style>
<?php }
//FUNCION TOMA LOS ESTILOS 
public function estilos($img_logo,$img_background,$color_primario,$color_secundario){ ?>
<style>
/* ESTILOS SEGUN LA CONFIGURCION */
.btn_color{
    background:<?php echo $color_primario ?> !important;
    color:<?php echo $color_secundario ?> !important;
}

.text_color{
    background:<?php echo $color_primario ?> !important;
    color:<?php echo $color_secundario ?> !important;
}

.boxSegmento{
    border: 1px solid <?php echo $color_primario ?> !important;
    margin: 0 5px;
}
/*ESTILO COLOR DE BARRA TOP */
.navbar {
    background-color: <?php echo $color_primario; ?>!important;
}
/*ESTILO INFORMACION CUENTA */
.sidebar .user-info {
    padding: 13px 15px 12px 15px;
    white-space: nowrap;
    position: relative;
    border-bottom: 1px solid #e9e9e9;
    background: url(../../inc/<?php echo $img_background; ?>) no-repeat no-repeat;
    height: 135px;
    background-size: cover;
}
/*ESTILOS DE LISTAS */
.sidebar .menu .list a {
    color: <?php echo $color_secundario; ?>!important;
    position: relative;
    display: inline-flex;
    vertical-align: middle;
    width: 100%;
    padding: 10px 13px;
}
/*ESTILOS PARA LISTA ACTIVA */
.theme-red .sidebar .menu .list li.active {
  background-color: transparent; }
  .theme-red .sidebar .menu .list li.active > :first-child i, .theme-red .sidebar .menu .list li.active > :first-child span {
    color:<?php echo $color_primario; ?>!important; }
 /*ESTILOS PARA LINEA EN FORMULARIOS*/
  .form-group .form-line:after {
    content: '';
    position: absolute;
    left: 0;
    width: 100%;
    height: 0;
    bottom: -1px;
    -moz-transform: scaleX(0);
    -ms-transform: scaleX(0);
    -o-transform: scaleX(0);
    -webkit-transform: scaleX(0);
    transform: scaleX(0);
    -moz-transition: 0.25s ease-in;
    -o-transition: 0.25s ease-in;
    -webkit-transition: 0.25s ease-in;
    transition: 0.25s ease-in;
    border-bottom: 2px solid <?php echo $color_secundario; ?>!important;;
}  
/*ESTILOS PARA BOTON DE COLOR*/
.btn_color{
     background-color: <?php echo $color_primario; ?>!important;

    
}
.color-circle{
    border:20px solid <?php echo $color_primario; ?> !important;
}
</style>  
<?php }

//FUNCION REGRESA SALDO
public function saldo($arr){
    $saldo=array_sum($arr);
return  $saldo;  
}

public function catalogo_productos($catalogue,$tipo_proyecto){ 
    include_once '../../inc/funciones.php';
    include_once '../../inc/parametros.php';
    
    $ins_funciones=new Funciones_Basicas();
    if($catalogue==0){
        $textcolor = "text-white";
        $qr_catalogo_premios=$ins_funciones->consulta_generica_all(' SELECT * FROM tbl_cat_premios_productos_servicios WHERE activo=0 ORDER BY RAND() LIMIT 3');
        
    }else{
        //MUESTRA CATALOGO CUANDO ESTA LOGUEADO UN USUARIO
        $textcolor = "text-black";
        $qr_catalogo_premios=$ins_funciones->consulta_generica_all(' SELECT * FROM tbl_cat_premios_productos_servicios WHERE activo=0');
    }
    
    ?>
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="">
            <div class="header">
                <h2 class="<?php echo $textcolor; ?>">
                    Catalogo de Productos para Redimir
                </h2>
            </div>
            <br>
            <div class="row margin-auto container-catalogue">
                <?php 
                    $rest=$qr_catalogo_premios;
                   while($fila = $rest->fetch_assoc()){  
                        if($ptos_totales_acumulado['puntos_totales']>=$fila['valor_puntos']){
                            $estatus='';
                        }else{
                            $estatus='disabled';
                        }
                        
                        ?>
                        <div class="col-md-4">
                            <div class="thumbnail circle color-circle">
                                <div class="img-catalogue">
                                    <img class="img-fluid" src="../../inc/imagenes/img_catalogo/<?php echo utf8_encode($fila['imagen']); ?>">
                                </div>
                                
                                <div class="caption txt-catalogue">
                                    <h3><?php echo utf8_encode(ucwords($fila['nombre'])) ?></h3>
                                    <h6><?php 
                                        switch ($tipo_proyecto){
                                            //PROYECTO  CON PUNTOS
                                            case 1:
                                            case 2:
                                                echo utf8_encode($fila['valor_puntos']).' Ptos';
                                            break;
                                            //PROYECTO POR VISTA
                                            case 3:
                                                echo utf8_encode($fila['valor_visitas']).' Visitas';
                                            break;
                                            
                                        }
                                    
                                    
                                    ?> </h6>
                                    <!--div class="detalle_prod">
                                        <small ><?php //echo utf8_encode($fila['detalle']) ?> </small><br><br>
                                    </div--> 
                                </div>
                            </div>
                        </div>
                           <?php   } ?>
            </div>
        </div>
    </div>



    
    
<?php } 
}
?>