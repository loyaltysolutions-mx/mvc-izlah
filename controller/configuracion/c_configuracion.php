<?php
 /*  * ##+> ################################# <+##
 * CONTROL DE CONFIGURACION INICIAL DE LA PLATAFORMA DE LEALTAD
 * Desarrolado ->Miguel Ruiz
 *  * ##+> ################################# <+##
 */
include_once '../../model/configuracion/m_configuracion.php';
class C_configuracion{
    private $ser;
    private $usu;
    private $pas;
    private $bd;
    

    public function __construct($ser,$usu,$pas,$bd) {
             $this->ser=$ser;
             $this->usu=$usu;
             $this->pas=$pas;
             $this->bd=$bd;
             $this->model=$model=new M_configuracion();
         }
    //FUNCION INSERTA DATOS DE CONFIGURACION
    public function guarda_configuracion($img_logo,$img_fondo,$color_primario,$color_secundario,$autoregistro,$forma_acumulacion,$tipo_acumulacion,$tipo_redencion,$mecanica_redencion,$bono_bienvenida,$bono_cumpleanios,$tipo_comunicacion_registro,$tipo_comunicacion_acumular,$tipo_comunicacion_redimir,$tipo_comun_periodica,$lapso_comun,$reg,$por_margen,$puntos_por_peso,$num_min_visitas,$puntos_por_visita,$ptos_bienvenida,$ptos_cumpleanios,$sucursales,$menus,$promociones,$niveles,$title,$detail,$tel,$email,$face,$twit,$nsucursales,$app, $tipoConfirmation){
        $res=$this->model->inserta_configuracion($this->ser,$this->usu,$this->pas,$this->bd,$img_logo,$img_fondo,$color_primario,$color_secundario,$autoregistro,$forma_acumulacion,$tipo_acumulacion,$tipo_redencion,$mecanica_redencion,$bono_bienvenida,$bono_cumpleanios,$tipo_comunicacion_registro,$tipo_comunicacion_acumular,$tipo_comunicacion_redimir,$tipo_comun_periodica,$lapso_comun,$reg,$por_margen,$puntos_por_peso,$num_min_visitas,$puntos_por_visita,$ptos_bienvenida,$ptos_cumpleanios,$sucursales,$menus,$promociones,$niveles,$title,$detail,$tel,$email,$face,$twit,$nsucursales,$app, $tipoConfirmation);  
    }
    //up/add rewards
    public function upreward($option,$reward,$filename,$description,$cant,$points){
        $res=$this->model->upreward($this->ser,$this->usu,$this->pas,$this->bd,$option,$reward,$filename,$description,$cant,$points);
    }

    // function to add Tipo de Punto Venta
    public function addptovta($t_punto_venta){
        $res=$this->model->addptovta($this->ser,$this->usu,$this->pas,$this->bd,$t_punto_venta);
          echo '1';
    }

    public function checks(){
        $res=$this->model->checks($this->ser,$this->usu,$this->pas,$this->bd);

        $rows = array();
        while($r = mysqli_fetch_assoc($res)) {
            $rows[] = $r;
        }
        $json = json_encode($rows);
        echo $json;
    }

    public function deleterw($id){
        $res=$this->model->deleterw($this->ser,$this->usu,$this->pas,$this->bd,$id);
        if($res){
            $reponse = 1;
            echo $reponse;
        }
    }

    public function deletepto($id){
        $res=$this->model->deletepto($this->ser,$this->usu,$this->pas,$this->bd,$id);
        if($res){
            $reponse = 1;
            echo $reponse;
        }
    }

    public function updaterw($id,$name,$pts,$visits,$details,$cant){
      $res=$this->model->updaterw($this->ser,$this->usu,$this->pas,$this->bd,$id,$name,$pts,$visits,$details,$cant);
      if($res){
            $reponse = 1;
            echo $reponse;
        }
    }

    public function updatepto($id,$name){
      $res=$this->model->updatepto($this->ser,$this->usu,$this->pas,$this->bd,$id,$name);
      if($res){
            $reponse = 1;
            echo $reponse;
        }
    }

    public function updateimg($id,$file){
      $res=$this->model->updateimg($this->ser,$this->usu,$this->pas,$this->bd,$id,$file);
    }

    //edit configuration products
    public function gridedit($option){
        $res=$this->model->gridedit($this->ser,$this->usu,$this->pas,$this->bd);

      //while($reg=mysqli_fetch_assoc($res)){
        //$arr = $reg[''];
      //}

        if($option=='json'){
            echo $json;
        }elseif($option=='tr'){
            while($reg=mysqli_fetch_assoc($res) ){
              $id = $reg['id_cat_premios_productos_servicios'];
              $nombre = $reg['nombre'];
              $puntos = $reg['valor_puntos'];
              $visitas = $reg['valor_visitas'];
              $img = $reg['imagen'];
              $detalle = $reg['detalle'];
              $cant = $reg['cantidad'];
                ?>
                    <tr>
                        <td class='bor_der text-center'><?php echo $id;?></td>
                        <td class='bor_der text-center'><input class="form-control" type="text" id="n<?php echo $id;?>" value="<?php echo utf8_encode($nombre);?>"></td>
                        <td class='bor_der text-center'><input class="form-control" type="number" id="p<?php echo $id;?>" value="<?php echo ($puntos);?>"></td>
                        <td class='bor_der text-center'><input class="form-control" type="number" id="v<?php echo $id;?>" value="<?php echo ($visitas);?>"></td>
                        <td class='bor_der text-center row'><button class='mod-al btn btn-info' id='i<?php echo $id;?>' onclick="cleanfile('<?php echo $id;?>')" data-toggle="modal" data-target="#editimg">Editar Img</button></td>
                        <td class='bor_der text-center'><input class="form-control" type="text" id="d<?php echo $id;?>" value="<?php echo utf8_encode($detalle);?>"></td>
                        <td class='bor_der text-center'><input class="form-control" type="text" id="c<?php echo $id;?>" value="<?php echo utf8_encode($cant);?>"></td>
                        <td class='bor_der text-center'><button class='btn btn-info' id='<?php echo $id;?>' onclick='editrw(this.id)'>Editar</button></td>
                        <td class='bor_der text-center'><button class='btn btn-danger' id='<?php echo $id;?>' name="<?php echo utf8_encode($nombre);?>" onclick='deleterw(this.id)'>Eliminar</button></td>
                    </tr> 
                <?php
            }
        }
    }//end function gridedit

    //edit configuration products
    public function grideditpto($option){
        $res=$this->model->grideditpto($this->ser,$this->usu,$this->pas,$this->bd);

      //while($reg=mysqli_fetch_assoc($res)){
        //$arr = $reg[''];
      //}

        if($option=='json'){
            echo $json;
        }elseif($option=='tr'){
            while($reg=mysqli_fetch_assoc($res) ){
              $id = $reg['id_tipo_punto_venta'];
              $nombre = $reg['nombre'];
                ?>
                    <tr>
                        <td class='bor_der text-center'><?php echo $id;?></td>
                        <td class='bor_der text-center'><input class="form-control" type="text" id="nm<?php echo $id;?>" value="<?php echo utf8_encode($nombre);?>"></td>
                        <td class='bor_der text-center'><button class='btn btn-info' id='<?php echo $id;?>' onclick='editpto(this.id)'>Editar</button></td>
                        <td class='bor_der text-center'><button class='btn btn-danger' id='<?php echo $id;?>' name="<?php echo utf8_encode($nombre);?>" onclick='deletepto(this.id)'>Eliminar</button></td>
                    </tr> 
                <?php
            }
        }
    }//end function gridedit
}
?>