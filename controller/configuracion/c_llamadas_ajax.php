<?php
 /*  * ##+> ################################# <+##
 * LLAMADAS DE AJAX DE LA PLATAFORMA DE LEALTAD
 * Desarrolado ->Miguel Ruiz
 *  * ##+> ################################# <+##
 */

include_once './c_configuracion.php';
include_once '../../inc/parametros.php';
$ins_control=new C_configuracion($ser,$usu,$pas,$bd);
$op=trim($_POST['op']);
switch ($op) {
    //LLAMADA PARA FUNCIONALIDAD DE CONFIGURACION
    case 1:
        //ETAPA1
         $autoregistro= trim($_POST['autoregistro']);
         $reg=$_POST['reg'];
        //ETAPA2
         $forma_acumulacion= trim($_POST['forma_acumulacion']);
         $tipo_acumulacion= trim($_POST['tipo_acumulacion']);
         $por_margen= trim($_POST['por_margen']);
         $puntos_por_peso= trim($_POST['puntos_por_peso']);
         $num_min_visitas= trim($_POST['num_min_visitas']);
         $puntos_por_visita= trim($_POST['puntos_por_visita']);
        //ETAPA3
         $tipo_redencion= trim($_POST['tipo_redencion']);
         $mecanica_redencion= trim($_POST['mecanica_redencion']);
        //ETAPA4
         $bono_bienvenida= trim($_POST['bono_bienvenida']);
         $ptos_bienvenida=trim($_POST['ptos_bienvenida']);
         $bono_cumpleanios= trim($_POST['bono_cumpleanios']);
         $ptos_cumpleanios=trim($_POST['ptos_cumple']);
         $tipo_comunicacion_registro=trim($_POST['tipo_comunicacion_registro']);
         $tipo_comunicacion_acumular= trim($_POST['tipo_comunicacion_acumular']);
         $tipo_comunicacion_redimir= trim($_POST['tipo_comunicacion_redimir']);
         $tipo_comun_periodica= trim($_POST['tipo_comun_periodica']);
         $lapso_comun= trim($_POST['lapso_comun']);
		 //ETAPA5
         $img_logo= trim($_POST['img_logo']);
         $img_fondo= trim($_POST['img_fondo']);
         $color_primario= trim($_POST['color_primario']);
         $color_secundario= trim($_POST['color_secundario']);
        $ins_control->guarda_configuracion($img_logo,$img_fondo,$color_primario,$color_secundario,$autoregistro,$forma_acumulacion,$tipo_acumulacion,$tipo_redencion,$mecanica_redencion,$bono_bienvenida,$bono_cumpleanios,$tipo_comunicacion_registro,$tipo_comunicacion_acumular,$tipo_comunicacion_redimir,$tipo_comun_periodica,$lapso_comun,$reg,$por_margen,$puntos_por_peso,$num_min_visitas,$puntos_por_visita,$ptos_bienvenida,$ptos_cumpleanios);
    break;
    //PROCESO SUBE IMAGEN
    case 2:
        //SUBE LOGO 
         if (isset($_FILES["file"]))
        {
           $file = $_FILES["file"];
           $carpeta=$_POST['ruta'];
            $nombre = $file["name"];
            $tipo = $file["type"];
            $ruta_provisional = $file["tmp_name"];
            $size = $file["size"];
            $dimensiones = @getimagesize($ruta_provisional);
            $width = $dimensiones[0];
            $height = $dimensiones[1];
            //if ($tipo != 'image/jpg' && $tipo != 'image/jpeg' && $tipo != 'image/png' && $tipo != 'image/gif')
             $x=substr($nombre,-4);
          
            
            
                $src = $carpeta.$nombre;
                //LIMPIAMOS CARPETA
                 foreach(glob("../../inc/img_tmp/*.*") as $filename) {
                    unlink($filename);
                    }
                move_uploaded_file($ruta_provisional, $src);
                //$inst_basicas->redimenciona($carpeta,$nombre);
                
              $solo_nombre=rtrim(strtolower(substr($nombre,0,-4)),".");
                ?>
             <br><div class="thumbnail">
                    <img style=' width: 85px;' src="<?php echo $src; ?>" alt="...">
                     <div class="caption">
                    </div>
                  </div>
             <script> $("#nombre_logo").val('<?php echo $nombre; ?>');</script>
            <?php 
        }else{
            //SUBE IMAGEN FONDO
           $file = $_FILES["file2"];
           $carpeta=$_POST['ruta'];
            $nombre = $file["name"];
            $tipo = $file["type"];
            $ruta_provisional = $file["tmp_name"];
            $size = $file["size"];
            $dimensiones = @getimagesize($ruta_provisional);
            $width = $dimensiones[0];
            $height = $dimensiones[1];
            $x=substr($nombre,-4);
            $src = $carpeta.$nombre;
                //LIMPIAMOS CARPETA
                 foreach(glob("../../inc/img_tmp/*.*") as $filename) {
                    unlink($filename);
                    }
              move_uploaded_file($ruta_provisional, $src);
              $solo_nombre=rtrim(strtolower(substr($nombre,0,-4)),".");
                ?>
             <br><div class="thumbnail">
                    <img style=' width: 85px;' src="<?php echo $src; ?>" alt="...">
                     <div class="caption">
                    </div>
                  </div>
             <script> $("#nombre_fondo").val('<?php echo $nombre; ?>');</script>
            
     <?php  }
        
    break;

    case 3:
    // up rewards / products
    $option = $_POST['option'];
    $reward = $_POST['reward'];
    $filename = $_POST['filename'];
    $description = $_POST['description'];
    $points = $_POST['points'];

    $reward = utf8_decode($reward);
    $description = utf8_decode($description);

    $ins_control->upreward($option,$reward,$filename,$description,$points);

    break;

    case 4:
    //update rewards / products
    $id = $_POST['id'];
    $name = $_POST['name'];
    $pts = $_POST['pts'];
    $visits = $_POST['visits'];
    $details = $_POST['details'];
    $cant = $_POST['cant'];

    $name = utf8_decode($name);
    $details = utf8_decode($details);

    $ins_control->updaterw($id,$name,$pts,$visits,$details,$cant);

    break;

    case 5:
    //update rewards / products
    $id = $_POST['id'];
    $file = $_POST['file'];
    $ins_control->updateimg($id,$file);

    break;

    case 6:
    //delete rewards / products
    $id = $_POST['id'];
    $ins_control->deleterw($id);

    break;
    
    default:
        echo 'Ocurrio algun error :(';
    break;
}


?>
