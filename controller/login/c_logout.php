<?php                                      
/************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#-   Controlador para cerrar la sesion de usuario-#
##################################################################################### 
session_start();
unset($_SESSION["usuario"]);  
$redirec= "../../" ;
header('Location:'.$redirec );
 ?>

