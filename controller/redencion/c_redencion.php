<?php
 /*  * ##+> ################################# <+##
 * CONTROL DE REDENCION
 * Desarrolado ->Miguel Ruiz
 *  * ##+> ################################# <+##
 */
include_once '../../model/redencion/m_redencion.php';
class C_redencion{
    private $ser;
    private $usu;
    private $pas;
    private $bd;
    

  public function __construct($ser,$usu,$pas,$bd) {
    $this->ser=$ser;
    $this->usu=$usu;
    $this->pas=$pas;
    $this->bd=$bd;
    $this->model=$model=new M_redencion();
  }

  //FUNCION RESTA PUNTOS
  public function resta_puntos_redencion($id_cte,$id_prod,$id_usu){
    $res=$this->model->resta_redencion($this->ser,$this->usu,$this->pas,$this->bd,$id_cte,$id_prod,$id_usu);  
    return $res; 
  }
  
  //FUNCION TRAE CAMPOS A MOSTRAR
  public function trae_campos(){
    $res=$this->model->campos_registro($this->ser,$this->usu,$this->pas,$this->bd);  
    return $res;
  }
  
  //FUNCION TRAE PRODUCTOS POR VISITAS
  public function trae_productos_visitas(){
    $res=$this->model->productos_catalogo_visitas($this->ser,$this->usu,$this->pas,$this->bd);  
    return $res;
  }

  //FUNCION TRAE PRODUCTOS
  public function trae_productos(){
    $res=$this->model->productos_catalogo($this->ser,$this->usu,$this->pas,$this->bd);  
    return $res;
  }

  //FUNCION BUSC CLIENTE
  public function nueva_redencion($telefono,$id_tarjeta,$mail){
    $res=$this->model->inserta_redencion($this->ser,$this->usu,$this->pas,$this->bd,$telefono,$id_tarjeta,$mail);  
    return $res;
  }
}
?>