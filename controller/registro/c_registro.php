<?php
 /*  * ##+> ################################# <+##
 * CONTROL DE REGISTRO
 * Desarrolado ->Miguel Ruiz
 *  * ##+> ################################# <+##
 */
include_once '../../model/registro/m_registro.php';
include_once '../../model/valida/m_valida.php';
include_once '../../controller/valida/c_valida.php';
include_once '../../controller/administracion/c_admin_segmento.php';
class C_registro{
    private $ser;
    private $usu;
    private $pas;
    private $bd;
    
  public function __construct($ser,$usu,$pas,$bd) {
    $this->ser=$ser;
    $this->usu=$usu;
    $this->pas=$pas;
    $this->bd=$bd;
    $this->model=new M_registro($ser, $usu, $pas, $bd);
    $this->c_admin = new C_admin_segmento($ser, $usu, $pas, $bd);
    $this->valida_model = new M_valida($ser, $usu, $pas, $bd);
    $this->c_valida = new C_valida($ser, $usu, $pas, $bd);
  }

  //FUNCION TRAE CAMPOS A MOSTRAR
  public function trae_campos(){
    $res=$this->model->campos_registro($this->ser,$this->usu,$this->pas,$this->bd);  
    return $res;
  }

  //campos para menu sin loguear
  public function trae_campos_reg(){
    $res=$this->model->trae_campos_reg($this->ser,$this->usu,$this->pas,$this->bd);  
    return $res;
  }

  public function select_niveles(){
    $res=$this->model->select_niveles($this->ser,$this->usu,$this->pas,$this->bd);  
    ?>    
    <div class="input-group">      
      <div class="form-line">
          <select id="nivel" name="nivel" onchange="" class="form-control show-tick" required>
              <option value="">Elige un Nivel</option>
                                  
                              
    <?php
      while($reg=mysqli_fetch_assoc($res)){
          $id = $reg['id_nivel'];
          $nivel = $reg['nombre'];
          
          ?>
              <option value="<?php echo $id; ?>"><?php echo utf8_encode($nivel); ?></option>
          <?php
        }
    ?>
          </select>
        </div>
    </div>
    <?php  
  }

  // funcion que muestra niveles si esta en la tabla de configuración principal
  public function trae_nivel(){
    $res=$this->model->trae_nivel($this->ser,$this->usu,$this->pas,$this->bd);
    $r=mysqli_fetch_assoc($res);
    $nivel = $r['niveles'];
    if($nivel=='1'){
      $select = $this->select_niveles();
    }else{

    }  
      
  }

  public function get_niveles(){
    return $this->model->select_niveles1($this->ser,$this->usu,$this->pas,$this->bd);
  }

  // funcion que muestra niveles si esta en la tabla de configuración principal
  public function get_status_nivel(){
    $res=$this->model->trae_nivel($this->ser,$this->usu,$this->pas,$this->bd);
    $r=mysqli_fetch_assoc($res);
    //print_r($r);
    //die();
    return $r['niveles'];      
  }
  //FUNCION CREA NUEVO USUARIO
  public function nuevo_usu($nombre,$edad,$telefono,$email,$rfc,$clave,$tarjeta,$fecha_cumple,$id_usu_session,$nivel,$genero = null){
    $res1=$this->model->inserta_usuario($this->ser,$this->usu,$this->pas,$this->bd,$nombre,$edad,$telefono,$email,$rfc,$clave,$tarjeta,$fecha_cumple,$id_usu_session,$nivel,$genero);  
    $res = $this->model->get_sum_bienvenida($this->ser,$this->usu,$this->pas,$this->bd,$res1['id_usuario']);
    
    $res2 = $this->model->obt_nivel($this->ser,$this->usu,$this->pas,$this->bd,1);
    while($reg2=mysqli_fetch_assoc($res2)){
      //print_r($reg2);
      $lvls = $reg2['id_nivel'];
    }

    $res3 = $this->model->update_nivel($this->ser,$this->usu,$this->pas,$this->bd,$res1['id_usuario'],$lvls);

    $resAdmin = $this->c_admin->updateClientsSegmento(NULL);

    return $res1;   
  }

  public function verifyUser($datos){
    $telefono = isset($datos['telefono']) ? $this->cleanData($datos['telefono']) : NULL;

    if(isset($datos['email']) && isset($datos['domainEmail']) ){
      switch ($datos['domainEmail']) {
        case 1:
          $email = $datos['email']."@hotmail.com";
        break;
        case 2:
          $email = $datos['email']."@gmail.com";
        break;
        case 3:
          $email = $datos['email']."@outlook.com";
        break;
        case 4:
          $email = $datos['email']."@".$this->cleanData($datos['domainEmailOther']);
        break;
        case 5:
          $email = $datos['email']."@icloud.com";
        break;
        
        default:
          $email = null;
        break;
      }
    }else{
      $email = null;
    }
    //die("Email:".$email);
    
    if($email == null){  //Verificar que exista el usuario
      $existUser = $this->model->verifyUser($this->ser,$this->usu,$this->pas,$this->bd,$telefono);
      return array('result' => count($existUser) > 0 ? 1 : 2);
    }else{ //Completar el registro al enviar el email
      $user = $this->valida_model->verifyUser($telefono);
      $tel = $this->valida_model->verifyTel($telefono);
      $mail = $this->valida_model->verifyMail($email);

      $usuario = array();
      
      $usuario['telefono'] = $telefono;
      $usuario['email'] = $email;

      //print_r($tel);
      //die();

      if(count($user) == 0 && count($tel) == 0 && count($mail) == 0){
        $usuario['fecha_registro_plataforma'] = date("Y-m-d");
        $token = $this->valida_model->generarToken();
        $usuario['token'] = $token;
        $usuario['usuario'] = $this->cleanData($usuario['telefono']);
        $usuario['rol'] = 3;
        $resultInsert = $this->valida_model->insertClient($this->cleanData($usuario));
        //$resultInsert['result'] = true;
        //$resultInsert['idUsuario'] = 95;

        if($resultInsert['result']){
            $res1 = $this->valida_model->insertMenuClient($resultInsert['idUsuario']);
            $res = $this->model->get_sum_bienvenida($this->ser,$this->usu,$this->pas,$this->bd,$resultInsert['idUsuario']);
            $res2 = $this->model->obt_nivel($this->ser,$this->usu,$this->pas,$this->bd,1);
            while($reg2=mysqli_fetch_assoc($res2)){
                $lvls = $reg2['id_nivel'];
            }
            $res3 = $this->model->update_nivel($this->ser,$this->usu,$this->pas,$this->bd,$resultInsert['idUsuario'],$lvls);
            //$resAdmin = $this->admin->updateClientsSegmento(NULL);

            $sms = "Bienvenido a la plataforma de lealtad, para concluir tu registro visita el siguiente link: desclub.com.mx/izlah/u.php%3Ftoken=$token";
            
            //$mail = "Bienvenido a la plataforma de lealtad, para concluir tu registro y acceder a grandes promociones te invitamos a que visites el siguiente link: www.desclub.com.mx/izlah/u.php%3Ftoken=$token";
            
            $link = "https://www.desclub.com.mx/izlah/u.php?token=$token";
            
            $shtml = file_get_contents('../../view/tablet/mailing.html');
                        
            $msj = '<span>Te damos la bienvenida a IZLAH</span><span style="font-style:normal;font-weight:bold;">, ya casi terminas tu registro<br/>por favor da clic en el siguiente link para<br/> ingresar tu información.</span>';

            $incss  = str_replace('id="linkCompleteRegistro"','id="linkCompleteRegistro" href="'.$link.'"',$shtml);
            $cuerpo = str_replace('<div id="msjMailing">','<div id="msjMailing" style="text-align: center;">'.$msj,$incss);
            
            $tituloMail = "Plataforma Lealtad - Izlah";
            
            $resultNotification = $this->valida_model->sendNotification($resultInsert['idUsuario'], $sms, $tituloMail, $cuerpo);


            return array('result' => $resultNotification ? 3 : 4);
            
        }
        return array('result' => 4);
        
      }else{
        return array('result' => 5);
       
      }
    }
    
  }

  public function revisar($telefono,$email){
    $res=$this->model->revisar($this->ser,$this->usu,$this->pas,$this->bd,$telefono,$email);  
    while($reg=mysqli_fetch_assoc($res)){
      $numero = $reg['numero'];
    }
    echo $numero;
  }

  public function addtarjeta($telefono,$email,$tarjeta){
    $res=$this->model->obtener_usuario($this->ser,$this->usu,$this->pas,$this->bd,$telefono,$email);  
    while($reg=mysqli_fetch_assoc($res)){
      $id = $reg['id_usuario'];
    }

    $res2=$this->model->addtarjeta($this->ser,$this->usu,$this->pas,$this->bd,$tarjeta,$id);
    //echo 1;
  }

  //FUNCION OBTENER USUARIO
  public function get_usuario($idUsuario = NULL, $tel = NULL, $mail = NULL, $id = NULL){
    $res=$this->model->get_usuario($this->ser, $this->usu, $this->pas, $this->bd, $idUsuario, $tel, $mail, $id);
    if(isset($res[0]['idUsuario']))
      $res[0]['puntos_disponibles'] = $this->model->get_puntos_disponibles($this->ser, $this->usu, $this->pas, $this->bd, $res[0]['idUsuario']);
    return $res;
  }

  //FUNCION OBTENER PROMOCIONES
  public function get_productos_promociones($idUsuario){

    $puntos_disponibles = $this->model->get_puntos_disponibles($this->ser, $this->usu, $this->pas, $this->bd, $idUsuario);    

    $puntosTotales = $puntos_disponibles[0]['puntos_totales'];
    $visitasTotales = $puntos_disponibles[0]['visitas_totales'];
    
    $res['productos'] = $this->model->get_productos($this->ser, $this->usu, $this->pas, $this->bd, null, null);
    $res['promociones'] = $this->model->get_promociones($this->ser, $this->usu, $this->pas, $this->bd, $idUsuario);
    $res['cashback'] = $this->model->get_cashback_user($this->ser, $this->usu, $this->pas, $this->bd, $idUsuario);

    $arrayProductos = array();
    for($index = 0; $index < count($res['productos']); $index++) {
      $producto = $res['productos'][$index];
      $registrosProducto = $this->model->get_count_productos($this->ser, $this->usu, $this->pas, $this->bd, null, $producto['id_cat_premios_productos_servicios']);
      //print_r($registrosProducto);
      if( $registrosProducto[0]['conteo'] > 0 ){
        $producto['restantes'] = (int)$producto['cantidad'] - (int)$registrosProducto[0]['conteo'];
      }else{
        $producto['restantes'] = (int)$producto['cantidad'];
      }
      $producto['restantes'] > 0 && array_push($arrayProductos, $producto);        
    }
    
    //die();

    $arrayPromociones = array();
    for($index = 0; $index < count($res['promociones']); $index++){
      $promocion = $res['promociones'][$index];

      $registrosPromocion = $this->model->get_count_promociones($this->ser, $this->usu, $this->pas, $this->bd, null, $promocion['id_promociones']);
      $registrosUsuarioPromocion = $this->model->get_count_promociones($this->ser, $this->usu, $this->pas, $this->bd, $idUsuario, $promocion['id_promociones']);

      $flagCumpleaños = null;
      $flagRegistro = null;

      if($promocion['tipo'] == 8){
        if(!empty($puntos_disponibles[0]['fecha_nacimiento']) && $puntos_disponibles[0]['fecha_nacimiento'] != '0000-00-00'){
          $auxDesde = new DateTime($puntos_disponibles[0]['fecha_nacimiento']);        
          $auxHasta = new DateTime(strtotime('Y-m-d H:i:s'));
          $auxHasta->modify('-1 years');

          $hasta = new DateTime(strtotime('Y-m-d'));
          $auxDesde->setDate($auxHasta->format("Y"), $auxDesde->format("m"), $auxDesde->format("d"));
          $desde = $auxDesde->getTimestamp() > $auxHasta->getTimestamp() ? $auxDesde : $auxHasta;

          $auxBetween = $desde->format("Y-m-d 00:00:00")."' AND '".$hasta->format("Y-m-d H:i:s");
          //echo("Cumpleaños:".$auxBetween);

          $registrosUsuarioPromocion = $this->model->get_count_promociones($this->ser, $this->usu, $this->pas, $this->bd, $idUsuario, $promocion['id_promociones'], $auxBetween);    
          //print_r($registrosUsuarioPromocion);
          
          $fecha_nacimiento = new DateTime($puntos_disponibles[0]['fecha_nacimiento']);
          $year = date("Y");
          $fecha_nacimiento->setDate((int)$year, (int)$fecha_nacimiento->format("m"), (int)$fecha_nacimiento->format("d"));

          $fechaInicio = new DateTime($promocion['comienza']);
          $fechaFin = new DateTime($promocion['vigencia']);

          $hoy = new DateTime();

          /*print_r($fecha_nacimiento->getTimestamp()."<br>");
          print_r($fechaInicio->getTimestamp()."<br>");
          print_r($fechaFin->getTimestamp()."<br>");*/

          $flagCumpleaños = $fecha_nacimiento->getTimestamp() >= $fechaInicio->getTimestamp() && $fecha_nacimiento->getTimestamp() <= $fechaFin->getTimestamp() && $hoy->getTimestamp() >= $fecha_nacimiento->getTimestamp();  
        }else{
          $flagCumpleaños = FALSE;
        }
      }


      if($promocion['tipo'] == 9){
        if(!empty($puntos_disponibles[0]['fecha_registro_plataforma']) && $puntos_disponibles[0]['fecha_registro_plataforma'] != '0000-00-00'){
          $auxDesde = new DateTime($puntos_disponibles[0]['fecha_registro_plataforma']);
          $auxHasta = new DateTime(strtotime('Y-m-d H:i:s'));
          $auxHasta->modify('-1 years');

          $hasta = new DateTime(strtotime('Y-m-d'));
          $auxDesde->setDate($auxHasta->format("Y"), $auxDesde->format("m"), $auxDesde->format("d"));
          $desde = $auxDesde->getTimestamp() > $auxHasta->getTimestamp() ? $auxDesde : $auxHasta;

          $auxBetween = $desde->format("Y-m-d 00:00:00")."' AND '".$hasta->format("Y-m-d H:i:s");
          //echo("Registro:".$auxBetween);

          $registrosUsuarioPromocion = $this->model->get_count_promociones($this->ser, $this->usu, $this->pas, $this->bd, $idUsuario, $promocion['id_promociones'], $auxBetween);    

          //print_r($registrosUsuarioPromocion);

          $fecha_registro = new DateTime($puntos_disponibles[0]['fecha_registro_plataforma']);
          $year = date("Y");
          $fecha_registro->setDate((int)$year, (int)$fecha_registro->format("m"), (int)$fecha_registro->format("d"));

          $fechaInicio = new DateTime($promocion['comienza']);
          $fechaFin = new DateTime($promocion['vigencia']);

          $fecha_registro_aux = new DateTime($puntos_disponibles[0]['fecha_registro_plataforma']);
          $fecha_registro_aux->modify('+1 years');
          $hoy = new DateTime(strtotime('Y-m-d'));

          $flagRegistro = $fecha_registro_aux->getTimestamp() <= $hoy->getTimestamp() &&  $fecha_registro->getTimestamp() >= $fechaInicio->getTimestamp() && $fecha_registro->getTimestamp() <= $fechaFin->getTimestamp();        
        }else{
          $flagRegistro = FALSE;
        }
      }
      
      if(count($registrosPromocion) > 0 && count($registrosUsuarioPromocion) > 0 ){
        if($promocion['limite_por_usuario'] <= $registrosUsuarioPromocion[0]['conteo'] || $promocion['limite'] <= $registrosPromocion[0]['conteo'] || $flagCumpleaños === FALSE || $flagRegistro === FALSE ){
          //echo "Entro: ";
        }else{

          $promocion['restantes'] = (int)$promocion['limite_por_usuario'] - (int)$registrosUsuarioPromocion[0]['conteo'];
          //print_r($promocion);
          array_push($arrayPromociones, $promocion);
        }
      }
    }

    $arrayCashback = array();
    for($index = 0; $index < count($res['cashback']); $index++) {
      $cashback = $res['cashback'][$index];
      $registrosPromocion1 = $this->model->get_count_promociones($this->ser, $this->usu, $this->pas, $this->bd, null, $cashback['id_promociones']);
      $registrosUsuarioPromocion1 = $this->model->get_count_promociones($this->ser, $this->usu, $this->pas, $this->bd, $idUsuario, $cashback['id_promociones']);

      if(count($registrosPromocion1) > 0 && count($registrosUsuarioPromocion1) > 0 ){        
        if($cashback['limite_por_usuario'] <= $registrosUsuarioPromocion1[0]['conteo'] || $cashback['limite'] <= $registrosPromocion1[0]['conteo']){
          
        }else{
          $cashback['restantes'] = (int)$cashback['limite_por_usuario'] - (int)$registrosUsuarioPromocion1[0]['conteo'];
          array_push($arrayCashback, $cashback);
        }
      }

    }
    //die();
    //print_r($res['promociones']);
    $res['productos'] = $arrayProductos;
    $res['promociones'] = $arrayPromociones;
    $res['cashback'] = $arrayCashback;
    //print_r($res);
    //die();

    return $res;
  }
//acumulacion
public function insertar_registro($datos = NULL){    
    $passwd = md5($datos['password']);
    $user = $datos['user'];
    $monto = isset($datos['monto']) ? $datos['monto'] : NULL;
    $ticket = isset($datos['ticket']) ? $datos['ticket'] : NULL;
    
    $sucursal = $this->model->get_sucursal($this->ser, $this->usu, $this->pas, $this->bd, $passwd);
    //print_r($datos);
    //die();
    if(count($sucursal) > 0){
      //SE VERIFICA QUE EL TICKET INGRESADO NO EXISTA
      $verifyTicket = $this->model->verifyTicket($this->ser, $this->usu, $this->pas, $this->bd, $ticket);
      if(!$verifyTicket){
        $removeCashBackUser = $this->model->removeCashbackUser($this->ser, $this->usu, $this->pas, $this->bd, $user);
        $verifyActiveCashback = $this->model->getActiveCashback($this->ser, $this->usu, $this->pas, $this->bd, date("Y-m-d"));              
        foreach ($verifyActiveCashback as $cashback) {
          $resultInsertActiveCashback = $this->model->insertUserCashback($this->ser, $this->usu, $this->pas, $this->bd, $user, $cashback['id_promociones'], date("Y-m-d H:i:s"));
        }

        $idSucursal = $sucursal[0]['id_usuario'];
        $resultInsert = $this->model->insert_registro($this->ser, $this->usu, $this->pas, $this->bd, $user, $idSucursal, $monto, $ticket);

        $res = $this->model->get_sum($this->ser,$this->usu,$this->pas,$this->bd,$user);
        //por puntos
        $reg=mysqli_fetch_assoc($res);
        $variable = $reg['sumpuntos'];
      
        $res2 = $this->model->obt_nivel($this->ser,$this->usu,$this->pas,$this->bd,$variable);
        while($reg2=mysqli_fetch_assoc($res2)){
          $lvls = $reg2['id_nivel'];
        }

        $res3 = $this->model->update_nivel($this->ser,$this->usu,$this->pas,$this->bd,$user,$lvls);
        //print_r($resultInsert);
        //die();

        $result = array('result' => $resultInsert['result'] ? 1 : 4, 'puntos' => $resultInsert['puntos']);
      }else{
        $result = array('result' => 3);
      }

      return $result;
      
    }
    return array('result' => 2);
}

public function insertar_redencion($datos = NULL){    
    $passwd = md5($datos['password']);
    $user = $datos['user'];
    $idRedencion = $datos['idRedencion'];    
    
    $verifyRegistro = $this->model->verifyRegistro($this->ser, $this->usu, $this->pas, $this->bd, $user);
    if($verifyRegistro){
      $sucursal = $this->model->get_sucursal($this->ser, $this->usu, $this->pas, $this->bd, $passwd);
      $redencion = $this->model->get_redencion($this->ser, $this->usu, $this->pas, $this->bd, $idRedencion);
      
      if(count($sucursal) > 0 && count($redencion) > 0){
        $idSucursal = $sucursal[0]['id_usuario'];
        $puntos = $redencion[0]['valor_puntos'];        
        $resultInsert = $this->model->insert_redencion($this->ser, $this->usu, $this->pas, $this->bd, $user, $idSucursal, $puntos, $idRedencion);
        
        return array('result' => $resultInsert ? 1 : 3, 'producto' => $redencion);
      }

      return array('result' => 4);

    }else{
      return array('result' => 2);
    }    
    
  }

  public function insertar_promocion($datos = NULL){    
    
    $passwd = md5($datos['password']);
    $user = $datos['idUser'];
    $idPromo = $datos['idPromo'];
    $monto_ticket = $datos['monto'];
    $num_ticket = isset($datos['ticket']) ? $datos['ticket'] : NULL;
    $montoCashback = isset($datos['montoCashback']) ? $datos['montoCashback'] : 0;
    $montoDesc = isset($datos['montoDesc']) ? $datos['montoDesc'] : 0;
    
    $sucursal = $this->model->get_sucursal($this->ser, $this->usu, $this->pas, $this->bd, $passwd);
        
    if( count($sucursal) > 0 ){
      $verifyTicket = $this->model->verifyTicket($this->ser, $this->usu, $this->pas, $this->bd, $num_ticket);
      //die("Verify:".$verifyTicket);
      if(!$verifyTicket){
        $fecha = date("Y-m-d");
        $removeCashBackUser = $this->model->removeCashbackUser($this->ser, $this->usu, $this->pas, $this->bd, $user);
        $verifyActiveCashback = $this->model->getActiveCashback($this->ser, $this->usu, $this->pas, $this->bd, $fecha);
        foreach ($verifyActiveCashback as $cashback) {
          $resultInsertActiveCashback = $this->model->insertUserCashback($this->ser, $this->usu, $this->pas, $this->bd, $user, $cashback['id_promociones'], date("Y-m-d H:i:s"));
        }
       
        $idSucursal = $sucursal[0]['id_usuario'];

        $promocion = $this->model->getPromocion($this->ser, $this->usu, $this->pas, $this->bd, $idPromo);

        $result = $this->model->insert_promocion($this->ser, $this->usu, $this->pas, $this->bd, $user, $idSucursal, $monto_ticket, $num_ticket, $montoCashback, $montoDesc, $idPromo);
        
        return array('result' => $result ? 1 : 4, 'promocion' => $result ? $promocion : null);
      }else{
        return array('result' => 2);
      }
    }
    return array('result' => 3);
  }

  private function cleanData($input){
      if(is_array($input)){
          $aux = array();
          foreach ($input as $key => $value) {
              //echo $key."=>".$value."<br>";
              $aux[$key] = trim(addslashes(htmlspecialchars($value)));
          }
          return $aux;
      }else{
          return trim(addslashes(htmlspecialchars($input)));
      }        
  }

}
?>