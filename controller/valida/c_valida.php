<?php
/*  * ##+> ################################# <+##
* CONTROL DE VALIDACION DE CUENTA DE  PLATAFORMA DE LEALTAD
* Desarrolado ->Miguel Ruiz
*  * ##+> ################################# <+##
*/

require '../../inc/parametros.php';
include_once '../../model/registro/m_registro.php';
include_once '../../model/valida/m_valida.php';
include_once '../../controller/administracion/c_admin_segmento.php';    

class C_valida{
        
    private $ser;
    private $usu;
    private $pas;
    private $bd;
    private $model;
    private $admin;
    
    public function __construct($ser,$usu,$pas,$bd) {
         $this->ser=$ser;
         $this->usu=$usu;
         $this->pas=$pas;
         $this->bd=$bd;
         $this->model = new M_valida($ser, $usu, $pas, $bd);
         $this->admin = new C_admin_segmento($ser, $usu, $pas, $bd);
         $this->registro_model= new M_registro($ser,$usu,$pas,$bd);         
     }

    //FUNCION INSERTA PASSWORD DE CUENTA
    public function guarda_pass($datos){

        $idUsuario = isset($datos['id_usu']) ? $this->cleanData($datos['id_usu']) : NULL;
        $usuario = isset($datos['usuario']) ? $this->cleanData($datos['usuario']) : NULL;
        $password = isset($datos['password']) ? md5(utf8_decode($datos['password'])) : NULL;
        $nombre = isset($datos['nombre']) ? $this->cleanData($datos['nombre']) : NULL;
        $telefono = isset($datos['telefono']) ? $this->cleanData($datos['telefono']) : NULL;    
        $email = isset($datos['email']) ? $this->cleanData(utf8_decode($datos['email'])) : NULL;
        $rfc = isset($datos['rfc']) ? $this->cleanData($datos['rfc']) : NULL;
        $tarjeta = isset($datos['tarjeta']) ? $this->cleanData(utf8_decode($datos['tarjeta'])) : NULL;
        $genero = isset($datos['genero']) ? $this->cleanData($datos['genero']) : NULL;
        $encuesta = isset($datos['encuesta']) ? $this->cleanData(utf8_decode($datos['encuesta'])) : NULL;
        $edad = isset($datos['fecha_nacimiento']) ? $this->cleanData($datos['fecha_nacimiento']) : NULL;

        $res=$this->model->inserta_pass($idUsuario, $usuario, $password, $nombre, $telefono, $email, $rfc, $tarjeta, $edad, $genero, $encuesta);

        $resAdmin  = $this->admin->updateClientsSegmento(NULL);
        
        return $res;        
    }

    //FUNCION PARA CREAR NUEVO CLIENTE
    public function newClient($datos){

        //print_r($datos);
        //die();
        $user = $this->model->verifyUser($datos['telefono']);
        $tel = $this->model->verifyTel($datos['telefono']);
        $mail = $this->model->verifyMail($datos['email']);

        if(count($user) == 0 && count($tel) == 0 && count($mail) == 0){
            die("Entro");
            $datos['fecha_registro_plataforma'] = date("Y-m-d");
            $token = $this->model->generarToken();
            $datos['token'] = $token;
            $datos['usuario'] = $this->cleanData($datos['telefono']);
            $datos['rol'] = 3;
            $resultInsert = $this->model->insertClient($this->cleanData($datos));

            
            if($resultInsert['result']){
                //die("Entro");
                $resultMenu = $this->model->insertMenuClient($this->cleanData($resultInsert['idUsuario']));
                $res = $this->registro_model->get_sum_bienvenida($this->ser,$this->usu,$this->pas,$this->bd,$resultInsert['idUsuario']);
                $res2 = $this->registro_model->obt_nivel($this->ser,$this->usu,$this->pas,$this->bd,1);
                while($reg2=mysqli_fetch_assoc($res2)){
                    $lvls = $reg2['id_nivel'];
                }
                $res3 = $this->registro_model->update_nivel($this->ser,$this->usu,$this->pas,$this->bd,$resultInsert['idUsuario'],$lvls);
                //$resAdmin = $this->admin->updateClientsSegmento(NULL);

                $sms = "Bienvenido a la plataforma de lealtad, para concluir tu registro visita el siguiente link: desclub.com.mx/izlah/u.php%3Ftoken=$token";
                $mail = "Bienvenido a la plataforma de lealtad, para concluir tu registro y acceder a grandes promociones te invitamos a que visites el siguiente link: www.desclub.com.mx/izlah/u.php%3Ftoken=$token";
                $tituloMail = "Plataforma Lealtad - Izlah";
                //die("length: ".strlen($sms));
                $resultNotification = $this->model->sendNotification($resultInsert['idUsuario'], $sms, $tituloMail, $mail);

                echo json_encode(array('result' => $resultNotification ? 1 : 2));
                die();
            }
            echo json_encode(array('result' => 2));
            die();
        }else{
            die("Else");
            echo json_encode(array('result' => 3));
            die();
        }
    }

    //FUNCION RECUPERAR PASSWORD DE CUENTA
    public function recoveryPass($datos){
        
        $username = isset($datos['username']) ? $this->cleanData($datos['username']) : NULL;

        $user = $this->model->verifyUser($username);
        if(count($user) == 1){
            $usuario = $user[0];
            $nombre = explode(" ",trim($usuario['nombre']));
            $nombre = $nombre[0];
            $token = $this->model->generarToken();

            $data = array('token' => $token);
            $updateUser = $this->model->updateUser($usuario['id_usuario'], $data);
            if($updateUser){
                $sms = "Hola $nombre, para restablecer tu cuenta visita el siguiente link: desclub.com.mx/izlah/redirect.php%3Fop=1%26token=$token";
                $mail = "Hola $nombre, has solicitado restablecer tu contraseña, para continuar visita el siguiente link: desclub.com.mx/izlah/redirect.php%3Fop=1%26token=$token";
                $tituloMail = "Plataforma Lealtad - Izlah";
                //die("length: ".strlen($sms));
                $resultNotification = $this->model->sendNotification($usuario['id_usuario'], $sms, $tituloMail, $mail);    
            }else{
                return array('result' => 4);
            }

            return array('result' => $resultNotification ? 1 : 3);
        }
        return array('result' => 2);
    }

    //FUNCION ACTUALIZAR PASSWORD DE CUENTA
    public function updatePass($datos){
        
        $idUsuario = isset($datos['idUsuario']) ? $this->cleanData($datos['idUsuario']) : NULL;
        $password = isset($datos['password']) ? md5($datos['password']) : NULL;
        $repassword = isset($datos['repassword']) ? md5($datos['repassword']) : NULL;
        $nombre = isset($datos['nombre']) ? $this->cleanData($datos['nombre']) :'';
        if( $idUsuario != NULL && $password != NULL && $password == $repassword){
            
            $data = array('token' => '', 'password' => $password);
            $updateUser = $this->model->updateUser($idUsuario, $data);
            if($updateUser){
                $sms = "Hola $nombre, has reestablecido tu cuenta satisfactoriamente, ahora puedes ver tu historial con tu nueva contraseña en el siguiente link: www.desclub.com.mx/izlah";
                $mail = "Hola $nombre, has reestablecido tu cuenta satisfactoriamente, ahora puedes ver tu historial con tu nueva contraseña en el siguiente link: www.desclub.com.mx/izlah";
                $tituloMail = "Reestablecimiento de cuenta - Plataforma Lealtad Izlah";
                //die("length: ".strlen($sms));
                $resultNotification = $this->model->sendNotification($idUsuario, $sms, $tituloMail, $mail);
            }else{
                return array('result' => 4);
            }

            return array('result' => $resultNotification ? 1 : 3);
        }
        return array('result' => 2);
    }

    private function cleanData($input){
        if(is_array($input)){
            $aux = array();
            foreach ($input as $key => $value) {
                //echo $key."=>".$value."<br>";
                $aux[$key] = trim(addslashes(htmlspecialchars($value)));
            }
            return $aux;
        }else{
            return trim(addslashes(htmlspecialchars($input)));
        }        
    }
}

