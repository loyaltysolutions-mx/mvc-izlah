<?php 
include_once '../../model/administracion/m_admin_registros.php';
include_once '../../controller/registro/c_registro.php';

class C_admin_registros{

  function __construct($ser,$usu,$pas,$bd) {    
    $this->model = new M_admin_registros($ser, $usu, $pas, $bd);
    $this->c_registro = new C_registro($ser, $usu, $pas, $bd);
  }

  public function getRegistros($idSucursal = NULL){
  	$result['registros'] = $this->model->getRegistros($idSucursal, NULL);
  	$result['tipo_proyecto'] = $this->model->getTipoProyecto();
  	$result['users'] = $this->model->getUser(NULL);
  	//print_r($result);
  	//die();
    return $result;
  }

  public function getPremios($idPremio = NULL){
    
    $result = $this->model->getPremios(NULL);
    return $result;
  }

  public function deleteRegistro($datos = NULL){
    
    parse_str($datos, $datos);
    $idRegistro = isset($datos['idRegistro']) ? $datos['idRegistro'] : NULL;
    
    if($idRegistro != NULL){
      $tipo_proyecto = $this->model->getTipoProyecto();
      $registro = $this->model->getRegistros(NULL, $idRegistro);

      if(count($registro) > 0 ){
        switch ($registro[0]['id_tipo_registro']) {
          case 1:
          case "1":
            $total_pts = $this->model->getPuntosTotales((int)$registro[0]['id_usuario'], 1);
            $pts = 0;
            //Se restan los puntos del usuario original
            if( (int)$tipo_proyecto[0]['id_tipo_proyecto'] != 3 ){
              $pts = (int)$total_pts[0]['puntos_totales'] - (int)$registro[0]['puntos'];
            }else{
              $pts = (int)$total_pts[0]['visitas_totales'] - (int)$registro[0]['num_visita'];
            }
            
            //Se actualizan los puntos totales
            $updatePts = $this->model->updatePts((int)$registro[0]['id_usuario'], $pts, (int)$tipo_proyecto['id_tipo_registro'], 1);
          break;

          case 2:
          case "2":        
            //Se obtienen los puntos totales del usuario original
            $total_pts_acumulacion = $this->model->getPuntosTotales((int)$registro[0]['id_usuario'], 1);
            $total_pts_redencion = $this->model->getPuntosTotales((int)$registro[0]['id_usuario'], 2);
            $ptsAcumulacion = 0;
            $ptsRedencion = 0;
            
            //Se restan los puntos del usuario original
            if( (int)$tipo_proyecto[0]['id_tipo_proyecto'] != 3 ){
              $ptsAcumulacion = (int)$total_pts_acumulacion[0]['puntos_totales'] + (int)$registro[0]['puntos'];
              $ptsRedencion = (int)$total_pts_redencion[0]['puntos_totales'] - (int)$registro[0]['puntos'];          
            }else{
              $ptsAcumulacion = (int)$total_pts[0]['visitas_totales'] + (int)$registro[0]['num_visita'];
              $ptsRedencion = (int)$total_pts[0]['visitas_totales'] - (int)$registro[0]['num_visita'];
            }
            
            //Se actualizan los puntos totales (acumulacion y redencion)
            $updatePtsAcumulacion = $this->model->updatePts((int)$registro[0]['id_usuario'], $ptsAcumulacion, (int)$tipo_proyecto['id_tipo_registro'], 1);            
            $updatePtsAcumulacion && $updatePtsRedencion = $this->model->updatePts((int)$registro[0]['id_usuario'], $ptsRedencion, (int)$tipo_proyecto['id_tipo_registro'], 2);
            
          break;

          //PROMOCION
          case 5:
          case "5":
            
            $total_pts = $this->model->getPuntosTotales((int)$registro[0]['id_usuario'], 1);
            //Se restan los puntos del usuario original
            if( (int)$tipo_proyecto[0]['id_tipo_proyecto'] != 3 ){
              $pts = (int)$total_pts[0]['puntos_totales'] - (int)$registro[0]['puntos'];
            }else{
              $pts = (int)$total_pts[0]['visitas_totales'] - (int)$registro[0]['num_visita'];
            }

            $idPromo = $data['id_promo'];
            $monto_ticket = $data['monto'];
            $num_ticket = $data['ticket'];
            $montoCashback = isset($data['montoCashback']) ? $data['montoCashback'] : 0;
            $montoDesc = isset($data['montoDesc']) ? $data['montoDesc'] : 0;
            $idUsuario = $data['id_usuario'];

            //Se actualizan los puntos totales acumulados
            $updatePts = $this->model->updatePts((int)$registro[0]['id_usuario'], $pts, (int)$tipo_proyecto['id_tipo_registro'], 1);        
          break;
        }
        
        $result = $this->model->deleteRegistro($idRegistro);
        return array('result' => $result ? 1 : 2);
      }      
      return array('result' => 2);
      
    }
    
    return array('result' => 2);
  }

  public function updateRegistro($datos = NULL){
    
    parse_str($datos['form'], $data);
    $tipo_proyecto = $this->model->getTipoProyecto();
    $registro = $this->model->getRegistros(NULL, $data['id_registro']);
    $idUsuario = $data['id_usuario'];

    switch ($registro[0]['id_tipo_registro']) {
      //ACUMULACION
      case 1:
      case "1":
        //Se obtienen los puntos totales del usuario original
        $total_pts = $this->model->getPuntosTotales((int)$registro[0]['id_usuario'], 1);
        $pts = 0;
        //Se restan los puntos del usuario original
        if( (int)$tipo_proyecto[0]['id_tipo_proyecto'] != 3 ){
          $pts = (int)$total_pts[0]['puntos_totales'] - (int)$registro[0]['puntos'];
        }else{
          $pts = (int)$total_pts[0]['visitas_totales'] - (int)$registro[0]['num_visita'];
        }
        
        //Se actualizan los puntos totales
        $updatePts = $this->model->updatePts((int)$registro[0]['id_usuario'], $pts, (int)$tipo_proyecto['id_tipo_registro'], 1);
        $updateAcumulacion = $updatePts ? $this->model->update_acumulacion((int)$registro[0]['id_registro_acumulacion'], $idUsuario, $data['monto'], $data['ticket']) : 2;

        //die("updateAcumulacion: ".$updateAcumulacion);
        return $updateAcumulacion;
      break;

      //REDENCION
      case 2:
      case "2":        
        //Se obtienen los puntos totales del usuario original
        $total_pts_acumulacion = $this->model->getPuntosTotales((int)$registro[0]['id_usuario'], 1);
        $total_pts_redencion = $this->model->getPuntosTotales((int)$registro[0]['id_usuario'], 2);
        $ptsAcumulacion = 0;
        $ptsRedencion = 0;
        
        //Se restan los puntos del usuario original
        if( (int)$tipo_proyecto[0]['id_tipo_proyecto'] != 3 ){
          $ptsAcumulacion = (int)$total_pts_acumulacion[0]['puntos_totales'] + (int)$registro[0]['puntos'];
          $ptsRedencion = (int)$total_pts_redencion[0]['puntos_totales'] - (int)$registro[0]['puntos'];          
        }else{
          $ptsAcumulacion = (int)$total_pts[0]['visitas_totales'] + (int)$registro[0]['num_visita'];
          $ptsRedencion = (int)$total_pts[0]['visitas_totales'] - (int)$registro[0]['num_visita'];
        }
        
        //Se actualizan los puntos totales (acumulacion y redencion)
        $updatePtsAcumulacion = $this->model->updatePts((int)$registro[0]['id_usuario'], $ptsAcumulacion, (int)$tipo_proyecto['id_tipo_registro'], 1);
        
        $updatePtsAcumulacion && $updatePtsRedencion = $this->model->updatePts((int)$registro[0]['id_usuario'], $ptsRedencion, (int)$tipo_proyecto['id_tipo_registro'], 2);

        $updateRedencion = $updatePtsAcumulacion && $updatePtsRedencion ? $this->model->update_redencion((int)$registro[0]['id_registro_acumulacion'], $idUsuario, $data['id_producto'], (int)$tipo_proyecto[0]['id_tipo_proyecto']) : 2;

        //die("updateAcumulacion: ".$updateAcumulacion);
        return $updateRedencion;
      break;

      //PROMOCION
      case 5:
      case "5":
        
        $total_pts = $this->model->getPuntosTotales((int)$registro[0]['id_usuario'], 1);
        //Se restan los puntos del usuario original
        if( (int)$tipo_proyecto[0]['id_tipo_proyecto'] != 3 ){
          $pts = (int)$total_pts[0]['puntos_totales'] - (int)$registro[0]['puntos'];
        }else{
          $pts = (int)$total_pts[0]['visitas_totales'] - (int)$registro[0]['num_visita'];
        }

        $idPromo = $data['id_promo'];
        $monto_ticket = $data['monto'];
        $num_ticket = $data['ticket'];
        $montoCashback = isset($data['montoCashback']) ? $data['montoCashback'] : 0;
        $montoDesc = isset($data['montoDesc']) ? $data['montoDesc'] : 0;
        $idUsuario = $data['id_usuario'];

        //Se actualizan los puntos totales acumulados
        $updatePts = $this->model->updatePts((int)$registro[0]['id_usuario'], $pts, (int)$tipo_proyecto['id_tipo_registro'], 1);        
        $updatePromocion = $updatePts ? $this->model->update_promocion($idUsuario, $monto_ticket, $num_ticket, $montoCashback, $montoDesc, $idPromo, $data['id_registro']) : 2;

        //die("updateAcumulacion: ".$updateAcumulacion);
        return $updatePromocion;
      break;
    }
  }

  //acumulacion
  public function insertar_registro($datos = NULL){    
    $passwd = md5($datos['password']);
    $user = $datos['user'];
    $monto = $datos['monto'];
    $ticket = $datos['ticket'];
    
    $sucursal = $this->model->get_sucursal($this->ser, $this->usu, $this->pas, $this->bd, $passwd);
    //print_r($datos);
    //die();
    if(count($sucursal) > 0){
      $removeCashBackUser = $this->model->removeCashbackUser($this->ser, $this->usu, $this->pas, $this->bd, $user);
      $verifyActiveCashback = $this->model->getActiveCashback($this->ser, $this->usu, $this->pas, $this->bd);      
      foreach ($verifyActiveCashback as $cashback) {
        $resultInsertActiveCashback = $this->model->insertUserCashback($this->ser, $this->usu, $this->pas, $this->bd, $user, $cashback['id_promociones']);
      }

      $idSucursal = $sucursal[0]['id_usuario'];
      $result = $this->model->insert_registro($this->ser, $this->usu, $this->pas, $this->bd, $user, $idSucursal, $monto, $ticket);

      $res = $this->model->get_sum($this->ser,$this->usu,$this->pas,$this->bd,$user);
      //por puntos
      $reg=mysqli_fetch_assoc($res);
      $variable = $reg['sumpuntos'];
    
      $res2 = $this->model->obt_nivel($this->ser,$this->usu,$this->pas,$this->bd,$variable);
      while($reg2=mysqli_fetch_assoc($res2)){
        $lvls = $reg2['id_nivel'];
      }

      $res3 = $this->model->update_nivel($this->ser,$this->usu,$this->pas,$this->bd,$user,$lvls);

      return $result;
      
    }
    return 2;
  }
  
  public function getModal($idRegistro = NULL){

    $result = $this->model->getRegistros(NULL, $idRegistro); 
    $users = $this->model->getUser(NULL);
    $tipo_proyecto = $this->model->getTipoProyecto();
    //print_r($result);
    //die();

    foreach ($result as $registro) {
      switch($registro['id_tipo_registro']){
        case 1:
        case "1":         
        ?>        
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <h5 class="text_color">Usuario:</h5>
              <div class="input-group">
                  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                  <div class="form-line">
                      <select id="selectUser" name="id_usuario" title=" -- Elige un usuario --" class="form-control show-tick" data-live-search="true" data-show-subtext="true" required >
                          <?php foreach ($users as $key) { ?>
                              <option data-subtext="<?php echo $key['usuario'] ?>" value="<?php echo $key['id_usuario'] ?>" <?php echo $registro['id_usuario'] ==  $key['id_usuario'] ? 'selected' : '' ?>><?php echo $key['nombre'] ?></option>
                          <?php }?>
                      </select>
                  </div>
              </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h5 class="text_color">Monto:</h5>
            <div class="input-group">
              <span class="input-group-addon">
                <i class="glyphicon glyphicon-usd"></i>
              </span>
              <input type="number" step="0.01" name="monto" min="1" required style="width:100%" value="<?php echo $registro['monto_ticket'] ?>">
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h5 class="text_color">Ticket:</h5>
            <div class="input-group">
              <span class="input-group-addon">
                <i class="glyphicon glyphicon-barcode"></i>
              </span>
              <div class="form-line">
                <input type="text" name="ticket" required style="width:100%" value="<?php echo $registro['ticket'] ?>">
                <input type="hidden" name="id_registro" value="<?php echo $idRegistro ?>" required>
              </div>
            </div>
          </div>
          <script type="text/javascript">
            $("#selectUser").selectpicker();          
          </script>
  <?php  
        break;

        case 2:
        case "2": 
          $productos = $this->model->getPremios(NULL);

  ?>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h5 class="text_color">Usuario:</h5>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <div class="form-line">
                    <select id="selectUser" name="id_usuario" title=" -- Elige un usuario --" class="form-control show-tick" data-live-search="true" data-show-subtext="true" required >
                        <?php foreach ($users as $key) { ?>
                            <option data-subtext="<?php echo $key['usuario'] ?>" value="<?php echo $key['id_usuario'] ?>" <?php echo $registro['id_usuario'] ==  $key['id_usuario'] ? 'selected' : '' ?>><?php echo $key['nombre'] ?></option>
                        <?php }?>
                    </select>
                </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h5 class="text_color">Producto:</h5>
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-gift"></i></span>
              <div class="form-line">
                <select id="selectProducto" name="id_producto" title=" -- Elige una promocion -- " class="form-control show-tick" data-show-subtext="true" data-live-search="true" required >
                  <?php foreach ($productos as $producto) { ?>
                    <option value="<?php echo $producto['id_cat_premios_productos_servicios'] ?>" data-subtext="(<?php echo $producto['valor_puntos'] ?> Pts.)" <?php echo $registro['id_cat_premio'] == $producto['id_cat_premios_productos_servicios'] ? 'selected' : '' ?> ><?php echo $producto['nombre'] ?></option>
                  <?php }?>
                </select>
                <input type="hidden" name="id_registro" value="<?php echo $idRegistro ?>" required>
              </div>
            </div>
          </div>
          <script type="text/javascript">
            $("#selectUser").selectpicker();
            $("#selectProducto").selectpicker();
          </script>
  <?php
        break;

        case 5: 
        case "5":
          $idUsuario = $registro['id_usuario'];          
          $promos = $this->model->getPromos(NULL);          
        ?>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h5 class="text_color">Usuario:</h5>
            <div class="input-group">
                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                <div class="form-line">
                    <select id="selectUser" name="id_usuario" title=" -- Elige un usuario --" class="form-control show-tick" data-live-search="true" data-show-subtext="true" required >
                        <?php foreach ($users as $key) { ?>
                            <option data-subtext="<?php echo $key['usuario'] ?>" value="<?php echo $key['id_usuario'] ?>" <?php echo $registro['id_usuario'] ==  $key['id_usuario'] ? 'selected' : '' ?>><?php echo $key['nombre'] ?></option>
                        <?php }?>
                    </select>
                </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <h5 class="text_color">Promocion:</h5>
            <div class="input-group">
              <span class="input-group-addon"><i class="glyphicon glyphicon-gift"></i></span>
              <div class="form-line">
                <select id="selectPromo" name="id_promo" title=" -- Elige una promocion --" class="form-control show-tick" data-live-search="true" data-show-subtext="true" required >
                  <?php foreach ($promos as $key) { ?>
                    <option data-subtext="<?php echo $key['texto_corto'] ?>" value="<?php echo $key['id_promociones'] ?>" <?php echo $registro['id_promo'] == $key['id_promociones'] ? 'selected' : '' ?> > <?php echo $key['nombrePromo'] ?> </option>
                  <?php }?>
                </select>
                <input type="hidden" name="id_registro" value="<?php echo $idRegistro ?>" required>
              </div>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="divDetailsPromo"></div>
          <script type="text/javascript">
            $("#selectPromo").on("change", function(){
              //console.log("click");
              var link = "../../controller/administracion/c_llamadas_ajax.php";
              var data = "op=25&id_promo="+$(this).val()+"&id_usuario="+$("#selectUser").val();
              var beforeFunction = function(){
                  
              };

              var successFunction = function(data){
                //console.log(data);
                let tipoPromo = data[0].tipo_promo;
                var tipoCashback = data[0].tipo_cashback;
                var valueCashBack = data[0].cashback;
                var idPromo = data[0].id_promo;
                var idUser = data[0].id_user;
                var dataDesc = data[0].data_desc;

                /*$("#inputIDPromo").val(idPromo);
                $("#inputTipoPromo").val(tipoPromo);
                $("#inputIDUser").val(idUser);*/

                //$("#formPromocion")[0].reset();              
                var modalBody = $("#divDetailsPromo");    
                modalBody.empty();
                switch(tipoPromo){
                  case 1:
                  case "1":
                      var spanCashBack = $("<p>",{text: "Monto Cashback a devolver "+(tipoCashback==1 ? "($"+valueCashBack+")" : "("+valueCashBack+"%)")+":"});
                      var inputCashBack = $("<input>",{type:"number", readonly: "readonly", name:"montoCashback" }).css({width:"100%"});

                      var spanMonto = $("<p>",{text:"Ingresa el monto de la compra:"});
                      var monto = $("<input>",{type:"number", step: 0.01, required : "required", name:"monto"}).css({width:"100%"});
                      var spanTicket = $("<p>",{text:"Ingresa el código del ticket:"});
                      var ticket = $("<input>",{type:"text", required : "required", name:"ticket"}).css({width:"100%"});
                      
                      modalBody.append($("<div>",{class:"text-center"}).append(spanMonto).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class:"glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(monto)))));
                      modalBody.append($("<div>",{class:"text-center"}).append(spanCashBack).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class: "glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(inputCashBack)))));            
                      modalBody.append($("<div>",{class:"text-center"}).append(spanTicket).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class:"glyphicon glyphicon-barcode"})).append($("<div>",{class:"form-line"}).append(ticket)))));

                      if(tipoCashback == 2){
                          monto.keyup(function(){
                              let valueMonto = $(this).val();
                              let result = valueMonto*(valueCashBack/100);
                              inputCashBack.val(result.toFixed(2));
                          });
                      }else{
                          inputCashBack.val(valueCashBack);
                      }            
                  break;

                  case 2:
                  case "2":
                  case 4:
                  case "4":
                  case 5:
                  case "5":
                      var spanMonto = $("<p>",{text:"Ingresa el monto de la compra:"});
                      var monto = $("<input>",{type:"number", step: 0.01, required : "required", name:"monto"}).css({width:"100%"});
                      var spanTicket = $("<p>",{text:"Ingresa el código del ticket:"});
                      var ticket = $("<input>",{type:"text", required : "required", name:"ticket"}).css({width:"100%"});

                      modalBody.append($("<div>",{class:"text-center"}).append(spanMonto).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class:"glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(monto)))));
                      //modalBody.append($("<div>",{class:"text-center"}).append(spanTicket).append("<br>").append(ticket));
                      modalBody.append($("<div>",{class:"text-center"}).append(spanTicket).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class:"glyphicon glyphicon-barcode"})).append($("<div>",{class:"form-line"}).append(ticket)))));
                  break;

                  case 6:
                  case "6":        
                      var valueDesc = dataDesc;
                      var spanDesc = $("<p>",{text: "Ingrese el monto con descuento ("+valueDesc+"%)"});
                      var montoDesc = $("<input>",{type:"number", step: 0.01, required : "required", name:"montoDesc", min:1 }).css({width:"100%"});
                      var spanMonto = $("<p>",{text:"Ingresa el monto de la compra real:"});
                      var monto = $("<input>",{type:"number", step: 0.01, required : "required", name:"monto", min:1}).css({width:"100%"});
                      var spanTicket = $("<p>",{text:"Ingresa el código del ticket:"});
                      var ticket = $("<input>",{type:"text", required : "required", name:"ticket"}).css({width:"100%"});
                      
                      modalBody.append($("<div>",{class:"text-center"}).append(spanMonto).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class:"glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(monto)))).append($("<h5>").text("ó")).append(spanDesc).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class: "glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(montoDesc)))));
                      modalBody.append($("<div>",{class:"text-center"}).append($("<hr>")));
                      //modalBody.append($("<div>",{class:"text-center"}).append(spanDesc).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class: "glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(montoDesc)))));            
                      modalBody.append($("<div>",{class:"text-center"}).append(spanTicket).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class:"glyphicon glyphicon-barcode"})).append($("<div>",{class:"form-line"}).append(ticket)))).append($("<hr>")));
                      
                      monto.keyup(function(){
                          let valueMonto = $(this).val();
                          let result = valueMonto*(1-(valueDesc/100));
                          montoDesc.val(result.toFixed(2));
                      });

                      montoDesc.keyup(function(){
                          let value = $(this).val();
                          let result = value/(1-(valueDesc/100));
                          monto.val(result.toFixed(2));
                      });            
                  break;

                  case 3:
                  case "3":
                      var valueDesc = dataDesc;
                      var spanDesc = $("<p>",{text: "Ingrese el monto con descuento ($"+valueDesc+")"});
                      var montoDesc = $("<input>",{type:"number", step: 0.01, required : "required", name:"montoDesc", min:1 }).css({width:"100%"});
                      var spanMonto = $("<p>",{text:"Ingresa el monto de la compra real:"});
                      var monto = $("<input>",{type:"number", step: 0.01, required : "required", name:"monto", min:valueDesc}).css({width:"100%"});
                      var spanTicket = $("<p>",{text:"Ingresa el código del ticket:"});
                      var ticket = $("<input>",{type:"text", required : "required", name:"ticket"}).css({width:"100%"});
                      
                      modalBody.append($("<div>",{class:"text-center"}).append(spanMonto).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class:"glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(monto)))).append($("<h5>").text("ó")).append(spanDesc).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class: "glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(montoDesc)))));
                      modalBody.append($("<div>",{class:"text-center"}).append($("<hr>")));
                      //modalBody.append($("<div>",{class:"text-center"}).append(spanDesc).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class: "glyphicon glyphicon-usd"})).append($("<div>",{class:"form-line"}).append(montoDesc)))));            
                      modalBody.append($("<div>",{class:"text-center"}).append(spanTicket).append($("<div>", {class:"input-group"}).append($("<span>",{class:"input-group-addon"}).append($("<i>",{class:"glyphicon glyphicon-barcode"})).append($("<div>",{class:"form-line"}).append(ticket)))).append($("<hr>")));
                      
                      monto.keyup(function(){
                          valueMonto = $(this).val();
                          result = valueMonto-valueDesc;
                          montoDesc.val(result.toFixed(2));
                      });

                      montoDesc.keyup(function(){
                          value = $(this).val();
                          result = (value-0)+(valueDesc-0);
                          monto.val(result.toFixed(2));
                      });
                  break;
                }
              }

              var failFunction = function(){
                    
              }

              connectSelect(link, data, beforeFunction, successFunction, failFunction);
            });

            $("#selectUser").change(function(){
              //console.log("click");
              var link = "../../controller/administracion/c_llamadas_ajax.php";
              var data = "op=26&id_usuario="+$(this).val();
              var beforeFunction = function(){
                var modalBody = $("#divDetailsPromo");    
                modalBody.empty();
              };

              var successFunction = function(data){
                //console.log(data);
                $("#selectPromo").empty();
                $.each(data.cashback, function(index, value){
                  $("#selectPromo").append($("<option>",{ text:value.nombre, value:value.id_promociones, "data-subtext": value.texto_corto }));
                });
                $.each(data.promociones, function(index, value){
                  $("#selectPromo").append($("<option>",{text:value.nombre, value:value.id_promociones, "data-subtext": value.texto_corto }));
                });
                $("#selectPromo").selectpicker("refresh");
              }

              var failFunction = function(){
              
              }

              connectSelect(link, data, beforeFunction, successFunction, failFunction);
            });

            function connectSelect(link, data, beforeFunction, successFunction, failFunction){
                $.ajax({
                  type: "POST",
                  timeout: 20000,
                  url: link,
                  data: data,
                  dataType: "json",
                  beforeSend : function(){
                    beforeFunction && beforeFunction();
                  }
                }).done(function(data){
                    console.log(data);
                    successFunction && successFunction(data);
                }).fail(function(data){
                    failFunction && failFunction(data);
                    console.log(data);
                });
              }
            $("#selectUser").selectpicker();
            $("#selectPromo").selectpicker();

          </script>
  <?php
        break;
      }
    }
  }

  public function getPromociones($datos = NULL){

    $idPromo = isset($datos['id_promo']) ? $datos['id_promo'] : NULL;    
    $result = $this->model->getPromos($idPromo);

    //print_r($result);
    //die();
    
    $arrayPromo = array();
    $arrayAux = array();
    foreach ($result as $key => $value) {
      //print_r($key);
      if($result[$key]['tipo'] == "1"){
        if(!empty($result[$key]['cashback'])){
          $arrayPromo['tipo_cashback'] = 1; 
          $arrayPromo['cashback'] = $result[$key]["cashback"];
        }else{
          $arrayPromo['tipo_cashback'] = 2; 
          $arrayPromo['cashback'] = $result[$key]['cashbackperc'];
        }
      }
      $arrayPromo['tipo_promo'] = $result[$key]['tipo'];
      $arrayPromo['id_user'] = $datos['id_usuario'];
      $arrayPromo['nombrePromo'] = $result[$key]['nombrePromo'];
      $arrayPromo['texto_corto'] = $result[$key]['texto_corto'];

      ((int)$result[$key]['tipo'] == 3 || (int)$result[$key]['tipo'] == 6) && $arrayPromo['data_desc'] = $result[$key]["descuento"];

      array_push($arrayAux, $arrayPromo);
    }

    //print_r($arrayAux);
    //die();

    return $arrayAux;
    
  }

  public function getInfoPromo($datos = NULL){
    
    $idPromo = isset($datos['id_promo']) ? $datos['id_promo'] : NULL;
    $idUsuario = isset($datos['id_usuario']) ? $datos['id_usuario'] : NULL;

    $result = $this->c_registro->get_productos_promociones($idUsuario);

    //print_r($result);
    //die();
    return $result;    
  }

  public function getUsuarios($idUsuario = NULL){    
    $users = $this->model->getUsuarios($idUsuario);
    return $users;
  }


}
?>