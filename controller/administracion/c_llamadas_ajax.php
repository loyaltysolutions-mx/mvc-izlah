<?php                                   
/************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#- Archivo llama controladores  por ajax -#
##################################################################################### 
include_once '../../controller/administracion/c_admin_usu.php';
include_once '../../controller/administracion/c_admin_segmento.php';
include_once '../../controller/administracion/c_admin_menu.php';
include_once '../../controller/administracion/c_admin_categoria.php';
include_once '../../controller/administracion/c_admin_nivel.php';
include_once '../../controller/administracion/c_tranfer_registros.php';
include_once '../../controller/administracion/c_cuadre_caja.php';
include_once '../../controller/administracion/c_admin_sucursales.php';
include_once '../../controller/administracion/c_admin_promociones.php';
include_once '../../controller/administracion/c_admin_registros.php';
include_once '../../controller/administracion/c_admin_rewards.php';
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
$ins_control=new C_admin_usu($ser,$usu,$pas,$bd);
$menuc = new C_admin_menu($ser,$usu,$pas,$bd);
$category = new C_admin_categoria($ser,$usu,$pas,$bd);
$levelc = new C_admin_nivel($ser,$usu,$pas,$bd);
$ins_cont_fijos=new Funciones_Basicas();
$inst_transfer_registros=new C_transfer_registros($ser,$usu,$pas,$bd);
$inst_cuadre=new C_cuadre_caja($ser,$usu,$pas,$bd);
$succ = new C_admin_sucursales($ser,$usu,$pas,$bd);
$promoc = new C_admin_promociones($ser,$usu,$pas,$bd);
$adminRegistros = new C_admin_registros($ser,$usu,$pas,$bd);
$adminRecompensas = new C_admin_rerwards($ser,$usu,$pas,$bd);
//print_r($_FILES);
//die();
$op=trim($_POST['op']);
switch ($op) {
    //CREA NUEVO USUARIO
    case 1:
        $usua=$_REQUEST['usu'];
        $pass=$_REQUEST['pass'];
        $tipo_usu=$_REQUEST['tipo_usu'];
		$tipo_punt_vta=$_REQUEST['tipo_punt_vta'];
        //INSERTA REGISTRO A LA BASE DE DATOS
        $ins_control->nuevo_usuario($usua,$pass,$tipo_usu,$tipo_punt_vta);
 
    break;
    //ELIMINA USUARIO
    case 2:
        $id_usu=$_REQUEST['id_usu'];
        $ins_control->elimina_usuario($id_usu); 
    break;
    case 3: //ADMIN SEGMENTO
        if(isset($_POST['action'])){
            $instanceSegmento =new C_admin_segmento($ser,$usu,$pas,$bd);
            $action = $_POST['action'];
            switch ($action) {
                case 1://CREAR SEGMENTO
                    $nombreSegmento = isset($_POST['nombreSegmento']) ? mb_strtoupper($_POST['nombreSegmento']) : NULL; 
                    $descripcionSegmento = isset($_POST['descripcionSegmento']) ? mb_strtoupper($_POST['descripcionSegmento']) : NULL; 
                    $statusSegmento = 0;
                    if($nombreSegmento != NULL){
                        $resp = $instanceSegmento->newSegmento($nombreSegmento, $descripcionSegmento, $statusSegmento);
                        //die("Respuesta: ".$resp);
                        echo json_encode(array('result' => ($resp == 1 ? 1 : 2)));
                    }else{
                        echo 'Error, debe ingresar un nombre';
                    }
                break;

                case 2: //EDITAR SEGMENTO                    
                    $idSegmento = isset($_POST['idSegmento']) ? $_POST['idSegmento'] : NULL;
                    $arrayUpdate = array(
                        'nombre_segmento' => (isset($_POST['nombreSegmento']) ? "'".mb_strtoupper(utf8_decode($_POST['nombreSegmento'])) ."'" : NULL ),
                        'descripcion_segmento' => (isset($_POST['descripcionSegmento']) ? "'".mb_strtoupper(utf8_decode($_POST['descripcionSegmento']))."'" : NULL ),
                        'activo' => (isset($_POST['statusSegmento']) ? $_POST['statusSegmento'] : NULL )
                    );
                    $resp = $instanceSegmento->updateSegmento($idSegmento, $arrayUpdate);
                    //die("Respuesta: ".$resp);
                    echo json_encode(array('result' => ($resp == 1 ? 1 : 2)));                    
                break;

                case 3: //ELIMINAR SEGMENTO
                    $idSegmento = isset($_POST['idSegmento']) ? $_POST['idSegmento'] : NULL;
                    $resp = $instanceSegmento->removeSegmento($idSegmento);
                    //die("Respuesta: ".$resp);
                    echo json_encode(array('result' => ($resp == 1 ? 1 : 2)));
                break;

                case 4: //OBTENER USUARIO
                    $idUsuario = isset($_POST['idUsuario']) ? $_POST['idUsuario'] : NULL;
                    $resp = $instanceSegmento->getClientesSegmento(NULL, $idUsuario);
                    $registro= mysqli_fetch_assoc($resp);                
                    echo json_encode(array('result' => 1, 'user' => $registro));
                break;

                case 5: //ELIMINAR USUARIO DE SEGMENTO
                    $idUsuario = isset($_POST['idUsuario']) ? $_POST['idUsuario'] : NULL;
                    $resp = $instanceSegmento->removeClientesSegmento($idUsuario);
                    echo json_encode(array('result' => ($resp == 1 ? 1 : 2)));
                break;
                case 6: //AGREGAR USUARIO AL SEGMENTO
                    $idUsuario = isset($_POST['idUsuario']) ? $_POST['idUsuario'] : NULL;
                    $idSegmento = isset($_POST['idSegmento']) ? $_POST['idSegmento'] : NULL;
                    $resp = $instanceSegmento->addClientesSegmento($idUsuario, $idSegmento);
                    $user = $instanceSegmento->getUsuario($idUsuario);
                    echo json_encode(array('result' => ($resp == 1 ? 1 : 2), 'user' => $user));
                break;
                case 7: //OBTENER SEGMENTO                    
                    $idSegmento = isset($_POST['idSegmento']) ? $_POST['idSegmento'] : NULL;
                    $resp = $instanceSegmento->getSegmentosAll($idSegmento);

                    echo json_encode(array('result' => $resp));
                break;

                case 8:
                    $resp = $instanceSegmento->getArraySegmentos(NULL);
                    echo json_encode(array('result' => $resp));
                break;

                case 9: //OBTENER CATEGORIAS
                    $resp = $instanceSegmento->getCategoriasSegmento(NULL);
                    echo json_encode($resp);
                break;
                case 10: //OBTENER CATEGORIAS
                    $resp = $instanceSegmento->getCategoriaClass(NULL);
                    echo json_encode($resp);
                break;

                case 11: //INSERTAR SEGMENTO                    
                    $resp = $instanceSegmento->insertSegmento($_POST);
                    echo json_encode($resp);
                break;
                case 12: //ACTUALIZAR CLIENTES SEGMENTO
                    $resp = $instanceSegmento->updateClientsSegmento($_POST);
                    echo json_encode($resp);
                break;                
                case 13: //ACTUALIZAR SEGMENTO
                    $resp = $instanceSegmento->editSegmento($_POST);
                    echo json_encode($resp);
                break;
                default:
                    echo json_encode(array());
                break;
            }            
        }else{
            echo 'Error';
        }
    break;

    case 4:
        //edit categoria
        $id = $_POST['id'];
        $name = $_POST['name'];
        $name = utf8_decode($name);
        $descript = $_POST['descript'];
        $descript = utf8_decode($descript);
        $category->editcat($id,$name,$descript);
    break;

    case 5:
    //add menu
        $menu = $_POST['item'];
        $menu = utf8_decode($menu);
        $cat = $_POST['cat'];
        $descript = $_POST['descript'];
        $descript = utf8_decode($descript);
        $price = $_POST['price'];
        $filename = $_POST['filename'];
        $menuc->addmenu($menu,$cat,$descript,$price,$filename);
    break;

    case 6:
    //edit menu
        $id = $_POST['id'];
        $name = $_POST['name'];
        $name = utf8_decode($name);
        $descript = $_POST['descript'];
        $descript = utf8_decode($descript);
        $cat = $_POST['cat'];
        $price = $_POST['price'];
        $menuc->editmenu($id,$name,$descript,$cat,$price);
        
    break;

    case 7:
        //update img in db
        $id = $_POST['id'];
        $file = $_POST['file'];

        $db = $_POST['table'];

        if($db==1){
          $table = 'tbl_menu';
          $field = 'id_menu';
          $imgfld = 'imagen';
        }elseif($dbs==2){
        
        }

        $menuc->updateimg($id,$file,$table,$field,$imgfld);
    break;

    case 8:
        //delete rows
        $id = $_POST['id'];
        $table = $_POST['table'];
        $field = $_POST['field'];
        $response = $_POST['res'];

        $menuc->delrow($id,$table,$field,$response);
    break;

    case 9:
        //add category
        $cat = $_POST['cat'];
        $descript = $_POST['descript'];
        $cat = utf8_decode($cat);
        $descript = utf8_decode($descript);
        $category->addcat($cat,$descript);
    break;
	
	case 10://TRANSFERENCIA DE REGISTROS
		$id_usu_origen=$_POST['id_usu_origen'];
		$id_usu_destino=$_POST['id_usu_destino'];
		$id_usu_session=$_POST['id_usu_session'];
		$inst_transfer_registros->transfiere_datos($id_usu_origen,$id_usu_destino,$id_usu_session);
	break;

    case 11:
        //add level
        $level = $_POST['level'];

        //$level = utf8_decode($level);
        $type = $_POST['type'];
        $ini = $_POST['ini'];
        $fin = $_POST['end'];
        $levelc->addlvl($level,$type,$ini,$fin);
    break;

    case 12:
        //edit level
        $id = $_POST['id'];
        $name = $_POST['name'];
        $name = utf8_decode($name);
        $type = $_POST['type'];
        $ini = $_POST['ini'];
        $fin = $_POST['fin'];

        $levelc->editlvl($id,$name,$type,$ini,$fin);
    break;
	
	case 13://CUADRE DE CAJA
		$id_sucursal = $_POST['id_establecimiento'];
		$f_inicio=$_POST['f_inicio'];
		$f_fin=$_POST['f_fin'];
		$inst_cuadre->grid_cuadre($id_sucursal,$f_inicio,$f_fin);
	break;

    case 14:
        //add Sucursal
        $suc = $_POST['suc'];
        $address = $_POST['address'];
        $phone = $_POST['phone'];
        $latt = $_POST['latitud'];
        $longi = $_POST['longitud'];
        $open = $_POST['ini'];
        $close = $_POST['fin'];

        $open = str_replace(' ', '', $open);
        $close = str_replace(' ', '', $close);

        $suc = utf8_decode($suc);
        $address = utf8_decode($address);

        $hour = $open.' - '.$close;

        $succ->addsuc($suc,$address,$phone,$latt,$longi,$hour);

    break;

    case 15:
        //edit Sucursal
        $id = $_POST['id'];
        $suc = $_POST['suc'];
        $address = $_POST['address'];
        $phone = $_POST['phone'];
        $latt = $_POST['lat'];
        $longi = $_POST['long'];
        $open = $_POST['open'];
        $close = $_POST['close'];

        $open = str_replace(' ', '', $open);
        $close = str_replace(' ', '', $close);

        $suc = utf8_decode($suc);
        $address = utf8_decode($address);

        $hour = $open.' - '.$close;

        $succ->editsuc($id,$suc,$address,$phone,$latt,$longi,$hour);

    break;

    case 16:
        //add Promoción
        $suc = $_POST['suc'];
        $segment = $_POST['segment'];
        $lvl = $_POST['lvl'];
        $promo = $_POST['promo'];
        $descript = $_POST['descript'];
        $smalltxt = $_POST['smalltxt'];

        $type = $_POST['type'];
        $conditions = $_POST['conditions'];
        $howuse = $_POST['howuse'];
        $infolink = $_POST['infolink'];
        $bonustype = $_POST['bonustype'];
        $rewardto = $_POST['rewardto'];
        $rewarperc = $_POST['rewarperc'];
        $npromos = $_POST['npromos'];
        $nredention = $_POST['nredention'];
        $start = $_POST['start'];
        $ending = $_POST['ending'];
        //message set
        $push = $_POST['push'];
        $sms = $_POST['sms'];
        $whats = $_POST['whats'];
        $email = $_POST['email'];
        //message texts
        $pushtxt = $_POST['pushtxt'];
        $smstxt = $_POST['smstxt'];
        $whatstxt = $_POST['whatstxt'];
        $emailtxt = $_POST['emailtxts'];

        $sendtime = $_POST['sendtime'];

        $cashback = $_POST['cashback'];
        $cashbackperc = $_POST['cashbackperc'];
        $descuento = $_POST['descuento'];
        $codigo = $_POST['codigo'];

        $promo = utf8_decode($promo);
        $descript = utf8_decode($descript);
        $smalltxt = utf8_decode($smalltxt);

        //added
        $conditions = utf8_decode($conditions);
        $howuse = utf8_decode($howuse);
        $infolink = utf8_decode($infolink);
        $pushtxt = utf8_decode($pushtxt);
        $smstxt = utf8_decode($smstxt);
        $whatstxt = utf8_decode($whatstxt);
        $emailtxt = utf8_decode($emailtxt);


        $promoc->addpromo($suc,$segment,$lvl,$promo,$descript,$smalltxt,$type,$conditions,$howuse,$infolink,$bonustype,$rewardto,$rewarperc,$npromos,$nredention,$start,$ending,$push,$sms,$whats,$email,$pushtxt,$smstxt,$whatstxt,$emailtxt,$sendtime,$cashback,$cashbackperc,$descuento,$codigo);

    break;

    case 17:
        //edit Promoción
        $id = $_POST['id'];
        $suc = $_POST['suc'];
        $segment = $_POST['segment'];
        $lvl = $_POST['lvl'];
        $promo = $_POST['promo'];
        $descript = $_POST['descript'];
        $smalltxt = $_POST['smalltxt'];

        $type = $_POST['type'];
        $conditions = $_POST['conditions'];
        $howuse = $_POST['howuse'];
        $infolink = $_POST['infolink'];
        $bonustype = $_POST['bonustype'];
        $rewardto = $_POST['rewardto'];
        $rewarperc = $_POST['rewarperc'];
        $npromos = $_POST['npromos'];
        $nredention = $_POST['nredention'];
        $start = $_POST['start'];
        $ending = $_POST['ending'];
        //message set
        $push = $_POST['push'];
        $sms = $_POST['sms'];
        $whats = $_POST['whats'];
        $email = $_POST['email'];
        //message texts
        $pushtxt = $_POST['pushtxt'];
        $smstxt = $_POST['smstxt'];
        $whatstxt = $_POST['whatstxt'];
        $emailtxt = $_POST['emailtxts'];

        $sendtime = $_POST['sendtime'];

        $cashback = $_POST['cashback'];
        $cashbackperc = $_POST['cashbackperc'];
        $descuento = $_POST['descuento'];
        $codigo = $_POST['codigo'];

        $promo = utf8_decode($promo);
        $descript = utf8_decode($descript);
        $smalltxt = utf8_decode($smalltxt);

        //added
        $conditions = utf8_decode($conditions);
        $howuse = utf8_decode($howuse);
        $infolink = utf8_decode($infolink);
        $pushtxt = utf8_decode($pushtxt);
        $smstxt = utf8_decode($smstxt);
        $whatstxt = utf8_decode($whatstxt);
        $emailtxt = utf8_decode($emailtxt);

        $promoc->editpromo($id,$suc,$segment,$lvl,$promo,$descript,$smalltxt,$type,$conditions,$howuse,$infolink,$bonustype,$rewardto,$rewarperc,$npromos,$nredention,$start,$ending,$push,$sms,$whats,$email,$pushtxt,$smstxt,$whatstxt,$emailtxt,$sendtime,$cashback,$cashbackperc,$descuento,$codigo);

    break;

    case 18:
        //active Promotion
        $id = $_POST['id'];
        $table = $_POST['table'];
        $field = $_POST['field'];

        //if($table=='tbl_categoria'){
        $category->active($id,$table,$field);
        //}elseif($table=='tbl_menu'){
            //$menuc
        //}
    break;
    
    case 19:
        //unactive Promotion
        $id = $_POST['id'];
        $table = $_POST['table'];
        $field = $_POST['field'];

        $category->unactive($id,$table,$field);
    break;

    case 20:
        //duplicate Promotion
        $id = $_POST['id'];

        $promoc->duplicate($id);
    break;

    case 21:
        //duplicate Promotion
        $id = $_POST['id'];

        $promoc->usedpromo($id);
    break;

    case 22:
        $id = $_REQUEST['id'];
        $usua=$_REQUEST['usu'];
        $pass=$_REQUEST['pass'];
        $tipo_usu=$_REQUEST['tipo_usu'];
        $tipo_punt_vta=$_REQUEST['tipo_punt_vta'];
        //INSERTA REGISTRO A LA BASE DE DATOS
        $ins_control->edita_usuario($id,$usua,$pass,$tipo_usu,$tipo_punt_vta);
    break;

    //Imprime el contenido del formulario para la edición de los registros
    case 23:
        $idRegistro = isset($_POST['idRegistro']) ? $_POST['idRegistro'] : NULL;
        $adminRegistros->getModal($idRegistro);
    break;

    //Actualiza el registro (acumulacion, redencion, promocion)
    case 24:
        $result = $adminRegistros->updateRegistro($_POST);
        echo json_encode(array("result" => $result));
        die();
    break;

    //Obtiene las promociones del usuario
    case 25:
        //die("Entro");
        $result = $adminRegistros->getPromociones($_POST);
        echo json_encode($result);
        die();
    break;

    //Obtiene las promociones del usuario
    case 26:
        //die("Entro");
        $result = $adminRegistros->getInfoPromo($_POST);
        echo json_encode($result);
        die();
    break;

    //Elimina un registro (acumulacion, redencion o promocion) existente en la BD
    case 27:        
        $result = $adminRegistros->deleteRegistro($_POST['form']);
        echo json_encode($result);
        die();
    break;

    //Funcion que permite subir masivamente las recompensas (archivo excel)
    case 28:
        $result = $adminRecompensas->uploadFileRecompensa($_FILES);
        echo json_encode($result);
        die();
    break;

    //Funcion que permite dar de baja a cliente final
    case 29:
        $idUsuario = isset($_POST['idUsuario']) ? trim(addslashes(htmlspecialchars($_POST['idUsuario']))) : NULL;
        if($idUsuario != NULL){
            $status = isset($_POST['valueStatus']) ? trim(addslashes(htmlspecialchars($_POST['valueStatus']))) : NULL;
            $arrayUpdate = array('activo' => $status);
            $result = $ins_control->updateCliente($idUsuario, $arrayUpdate);
            echo json_encode($result);    
            die();
        }
        echo json_encode(array());
        die();
    break;

    //Funcion que permite editar un cliente final
    case 30:        
        $idUsuario = isset($_POST['idUsuario']) ? trim(addslashes(htmlspecialchars($_POST['idUsuario']))) : NULL;
        if($idUsuario != NULL){
            unset($_POST['idUsuario']);
            unset($_POST['op']);
            $result = $ins_control->updateCliente($idUsuario, $_POST);
            echo json_encode($result);    
            die();
        }
        echo json_encode(array());
        die();
    break;

    default:
        echo 'Ocurrio algun error en las llamadas ajax :(';
    break;
}
?>

