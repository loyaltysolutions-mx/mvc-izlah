<?php
include_once '../../inc/parametros.php';
include_once '../../model/administracion/m_cuadre_caja.php';
class C_cuadre_caja{
    private $ser;
    private $usu;
    private $pas;
    private $bd;
    

  public function __construct($ser,$usu,$pas,$bd) {
    $this->ser=$ser;
    $this->usu=$usu;
    $this->pas=$pas;
    $this->bd=$bd;
    $this->model=$model=new M_cuadre_caja();
  }

  public function grid_cuadre($id_sucursal,$f_inicio,$f_fin){
    $res=$this->model->trae_reg_cuadre($this->ser,$this->usu,$this->pas,$this->bd,$id_sucursal,$f_inicio,$f_fin);
	?>
    <!-- Bootstrap Core Css -->
    <link href="../../inc/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
    <!-- JQuery DataTable Css -->
    <link href="../../inc/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
           <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <div class="header">
						<h4>Tabla de Resultados</h4>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>
                                            <th>Establecimiento</th>
                                            <th>Tipo</th>
                                            <th>Ticket</th>
                                            <th>Monto Inicial</th>
                                            <th>Promocion</th>
                                            <th>Descuento</th>
                                            <th>Monto Final</th>
                                            <th>Dia Registro</th>
                                            <th>Articulo Redención</th>
                                            <th>Fecha Registro</th>
                                        </tr>
                                    </thead>
                                    <tbody>
								     <?php 
									  while($reg=mysqli_fetch_assoc($res)){?>
										   <tr>
												<td><?php echo utf8_encode($reg['usuario']); ?></td>
												<td><?php echo utf8_encode($reg['tipo_registro']); ?></td>
												<td><?php echo $reg['ticket']; ?></td>
												<td><?php if($reg['monto_ticket']!=''){ echo '$'.$reg['monto_ticket'];}  ?></td>
                                                <td><?php if($reg['nombrePromo']!=''){ echo $reg['nombrePromo'];}  ?></td>
                                                <td><?php if($reg['cashback']!= null){ echo '$'.$reg['cashback'];} if($reg['cashbackperc']!= null){ echo $reg['cashbackperc'].'%';} if($reg['descuento']!= null ){if($reg['tipoPromo'] == 6 ){ echo $reg['descuento'].'%';}else{echo '$'.$reg['descuento'];}}  ?></td>
                                                <td><?php if(isset($reg['id_tipo']) && $reg['id_tipo']!= 2 ){ if($reg['monto_descuento'] != null){echo '$'.$reg['monto_descuento'];}else{ echo '$'.$reg['monto_ticket'];} } ?></td>
												<td><?php echo utf8_encode($reg['dia_registro']); ?></td>
												<td><?php echo utf8_encode($reg['premio']); ?></td>
												<td><?php echo$reg['fecha_registro']; ?></td>
											</tr>
										  <?php }  ?>
								     </tbody>
                                </table>
                            </div>
                        </div>
                </div>
         </div>
<script src="../../inc/plugins/jquery/jquery.min.js"></script>

<script src="../../inc/plugins/bootstrap-select/js/bootstrap-select.js"></script>   
<script src="../../inc/plugins/node-waves/waves.js"></script>
<script src="../../inc/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="../../inc/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<script src="../../inc/js/pages/tables/jquery-datatable.js"></script>
<script src="../../inc/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
		<?php 
    }

}
?>