<?php
include_once '../../inc/parametros.php';
include_once '../../model/administracion/m_transfer_registros.php';
class C_transfer_registros{
    private $ser;
    private $usu;
    private $pas;
    private $bd;
    

  public function __construct($ser,$usu,$pas,$bd) {
    $this->ser=$ser;
    $this->usu=$usu;
    $this->pas=$pas;
    $this->bd=$bd;
    $this->model=$model=new M_transfer_registros();
  }


  public function transfiere_datos($id_usu_origen,$id_usu_destino,$id_usu_session){
	
    //NUMERO DE ACUMULACIONES
	$num_reg_acum=$this->model->num_reg($this->ser,$this->usu,$this->pas,$this->bd,$id_usu_origen,1);
	$res_acum=mysqli_fetch_assoc($num_reg_acum);
	$n_acum=$res_acum['n'];
	//NUMERO DE REDENCIONES
	$num_reg_red=$this->model->num_reg($this->ser,$this->usu,$this->pas,$this->bd,$id_usu_origen,2);
	$res_red=mysqli_fetch_assoc($num_reg_red);
	$n_red=$res_red['n'];
	if($n_acum!=0 || $n_red!=0 ){
		//REALIZAMOS MIGRACION DE REGISTROS
		$cam_re=$this->model->cambia_registros($this->ser,$this->usu,$this->pas,$this->bd,$id_usu_origen,$id_usu_destino,$n_acum,$n_red,$id_usu_session);
		if($cam_re){
			echo '<div class="alert bg-green  alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
									Se transfirieron <b>'.$n_acum.'</b> Acumulaciones<br> Se transfirieron <b>'.$n_red.'</b> Redenciones<br>
								</div>';
		}else{
			echo '<div class="alert bg-red alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
									Ocurrio un problema al asignar los registros
								</div>';
		}
	}else{
		echo '<div class="alert bg-orange  alert-dismissible" role="alert">
									<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
									No tiene registros para poder Transferir
								</div>';
	}
  }

}
?>