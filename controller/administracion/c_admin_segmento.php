<?php
 /*  * ##+> ################################# <+##
 * CONTROL DE ADMIN USUARIOS
 * Desarrolado ->Miguel Ruiz
 *  * ##+> ################################# <+##
 */
include_once '../../model/administracion/m_admin_segmento.php';
class C_admin_segmento{
  private $ser;
  private $usu;
  private $pas;
  private $bd;    

  function __construct($ser,$usu,$pas,$bd) {
    $this->ser=$ser;
    $this->usu=$usu;
    $this->pas=$pas;
    $this->bd=$bd;
    $this->model = new M_admin_segmento($ser, $usu, $pas, $bd);
  }

  //FUNCION OBTENER SEGMENTOS
  public function getSegmentos($idSegmento = NULL){
    
    $res=$this->model->getSegmentos($idSegmento);
    return $res;
  }

  //FUNCION OBTENER SEGMENTOS
  public function getInfoUser($idUsuario = NULL){
    
    $result = array();
    $result['info'] = $this->model->getInfoUser($idUsuario);
    $result['registros'] = $this->model->getRegistrosUser($idUsuario);
    $result['puntos'] = $this->model->getPuntosUser($idUsuario);
    $result['transferencias'] = $this->model->getTransferenciasUser($idUsuario);
    $result['segmentos'] = $this->model->getSegmentosUser($idUsuario);
    $meses = 6;
    $result['actividad'] = $this->model->getRegistrosUserFecha($idUsuario, $meses);
    $result['diasUltimaCompra'] = $this->model->getDiasUltimaCompra($idUsuario);
    $result['promedio'] = $this->model->getPromedioMontoTicket($idUsuario);
    $result['tipo_proyecto'] = $this->model->getTipoProyecto();
    //print_r($result['tipo_proyecto']);
    //die();

    return $result;
  }

  //FUNCION CREAR NUEVO SEGMENTO
  public function newSegmento($nombreSegmento = NULL, $descripcionSegmento = NULL, $statusSegmento = NULL){
    $fecha = date("Y-m-d H:i:s");    
    $idSegmento = $this->model->insertSegmentoID(mb_strtoupper(utf8_decode($nombreSegmento)), mb_strtoupper(utf8_decode($descripcionSegmento)), $statusSegmento);    
    $result = $this->updateClientsSegmento(array('idSegmento'=>$idSegmento));

    return ($result ? 1 : 0);
  }

  //FUNCION PASA PARAMETROS PARA GUARDAR NUEVO USUARIO
  public function removeSegmento($idSegmento = NULL){
    if($idSegmento != NULL){
      $res=$this->model->removeClientsSegmento(NULL, $idSegmento);
      if($res == 1)
        $res=$this->model->removeCategoriasSegmento($idSegmento);
      if($res == 1)
        $res=$this->model->removeSegmento($idSegmento);
      
      return $res;  
    }  
    return 0;
  }

  //OBTIENE EL USUARIO DEL SEGMENTO PROPORCIONADO O VICEVERSA
  public function getClientesSegmento($idSegmento = NULL, $idUsuario = NULL){    
    $res=$this->model->getClientsSegmento($idSegmento, $idUsuario);      
    return $res;
  }

  //ELIMINA USUARIO DEL SEGMENTO PROPORCIONADO
  public function removeClientesSegmento($idUsuario = NULL){    
    if($idUsuario != NULL){
      $res=$this->model->removeClientsSegmento($idUsuario);  
      return $res;  
    }
  
    return 0;
  }

  //OBTIENE LOS CLIENTES QUE PUEDEN SER INGRESADOS AL SEGMENTO INGRESADO
  public function getClientsAdd($idSegmento = NULL){
    if($idSegmento != NULL){
      $res=$this->model->getClientsAdd($idSegmento);  
      return $res;  
    }
  
    return 0;
  }

  //INSERTAR USUARIO A SEGMENTO PROPORCIONADO
  public function addClientesSegmento($idUsuario = NULL, $idSegmento = NULL){    
    if($idUsuario != NULL && $idSegmento != NULL){      
      $res=$this->model->addClientsSegmento($idUsuario, $idSegmento);  
      return $res;
    }
  
    return 0;
  }

  //ACTUALIZA EL SEGMENTO CON LOS VALORES ENVIADOS
  public function updateSegmento($idSegmento = NULL, $values = NULL){    
    if($idSegmento != NULL && $values != NULL){      
      $res=$this->model->updateSegmento($idSegmento, $values);  
      return $res;  
    }
  
    return 0;
  }

  //OBTIENE LAS CATEGORIAS DISPONIBLES
  public function getCategoriasSegmento($idCategoria = NULL){
    $categorias = $this->model->getCategorias($idCategoria);
    $arrayAux = array();
    foreach ($categorias as $key){
      $res=$this->model->getObjetosCategorias($key['id_categoria']);
      if(isset($res[0]) && $key['id_categoria'] == 5){
        $res[0]['container'] = $this->model->getUsuarios(NULL, 2);
      }
      if(isset($res[0]) && $key['id_categoria'] == 6){
        $res[0]['container'] = $this->model->getUsuarios(NULL, 3);
      }

      if(isset($res[0]) && $key['id_categoria'] == 8){
        $res[0]['container'] = array(array('id_usuario' => 1,'nombre' => "Hombre"), array('id_usuario' => 2, 'nombre' => "Mujer") );
      }

      if(isset($res[0]) && $key['id_categoria'] == 10){
        $res[0]['container'] = array(
          array('id_usuario' => "Lunes",'nombre' => "Lunes"), 
          array('id_usuario' => "Martes",'nombre' => "Martes"), 
          array('id_usuario' => "Miércoles",'nombre' => "Miércoles"), 
          array('id_usuario' => "Jueves",'nombre' => "Jueves"), 
          array('id_usuario' => "Viernes",'nombre' => "Viernes"), 
          array('id_usuario' => "Sábado",'nombre' => "Sábado"), 
          array('id_usuario' => "Domingo",'nombre' => "Domingo") 
        );
      }
       
      array_push($arrayAux, array('id_categoria' => $key['id_categoria'], 'nombre_categoria' => $key['nombre_categoria'], 'descripcion_categoria' => $key['descripcion_categoria'], 'inputs' => $res));
    }    
    return $arrayAux;  
  }

  //OBTIENE TODA LA INFORMACION DEL SEGMENTO PROPORCIONADO
  public function getSegmentosAll($idSegmento = NULL){
    
    $res = $this->model->getSegmentos($idSegmento);
    //print_r($res);
    //die();
    $arraySegmento = array();
    $arrayUsers = array();
    while($segmento = mysqli_fetch_assoc($res)){      
      $arraySegmento['idSegmento'] = $segmento['id_segmento'];
      $arraySegmento['nombreSegmento'] = utf8_encode($segmento['nombre_segmento']);
      $arraySegmento['descripcionSegmento'] = utf8_encode($segmento['descripcion_segmento']);
      $arraySegmento['activo'] = $segmento['activo'];
      $users = $this->model->getClientsSegmento($segmento['id_segmento'], null);
      while($user = mysqli_fetch_assoc($users)) {
        unset($user['password']);
        unset($user['token']);
        array_push($arrayUsers, $user);
      }
      $arraySegmento['users'] = $arrayUsers;
    }

    $clientsAddSegmento = $this->getClientsAdd($idSegmento);
    $arrayAux = array();
    while ($row = mysqli_fetch_assoc($clientsAddSegmento)) {
      array_push($arrayAux, array('id_usuario' => utf8_encode($row['id_usuario']), 'nombre' => utf8_encode($row['nombre'])));
    }    
    $arraySegmento['usersAddSegmento'] = $arrayAux;
    $array = array();
    array_push($array, $arraySegmento);
    return $array;  
  }

  //OBTIENE UN ARRAY DE LOS SEGMENTOS
  public function getArraySegmentos($idSegmento = NULL){
    $res=$this->model->getSegmentos($idSegmento);    
    $segmentos = array();
    while($segmento = mysqli_fetch_assoc($res)){      
      $auxArray = array();
      $auxArray['idSegmento'] = $segmento['id_segmento'];
      $auxArray['nombreSegmento'] = $segmento['nombre_segmento'];
      $auxArray['descripcionSegmento'] = $segmento['descripcion_segmento'];
      $auxArray['activo'] = $segmento['activo'];
      $auxArray['countClients'] = $this->model->getCountSegmento($segmento['id_segmento']);
      array_push($segmentos, $auxArray);
    }   

    return $segmentos;
  }

  public function getInfoSegmento($idSegmento = NULL){
    $segmentos=$this->model->getSegmento1($idSegmento);

    for($index = 0; $index < count($segmentos); $index++){
      $arrayConfiguracion = array();
      $arrayCategorias = array();
      for($nivel=1;  $nivel<=$segmentos[$index]['maxNivel']; $nivel++) {        
        $aux = $this->model->getDistinctCategoriasSegmento($segmentos[$index]['id_segmento'],NULL, $nivel);
        for($indexCategoria=0; $indexCategoria < count($aux); $indexCategoria++){
          $categoria = $aux[$indexCategoria];          
          $arrayAux = array(
            'id_categoria' => $categoria['id_categoria'],
            'nombre_categoria' => $categoria['nombre_categoria'],
            'descripcion_categoria' => $categoria['descripcion_categoria'],
            'objetos' => $this->model->getCategoriasSegmento($segmentos[$index]['id_segmento'],$categoria['id_categoria'], $nivel)
          );

          /*print_r($arrayAux);
          die();*/

          $auxCategorias = $this->model->getDistinctCategoriasSegmento($segmentos[$index]['id_segmento'],$categoria['id_categoria'], $nivel);
          $valoresCategorias = array();
          $arrayAux['objetos'] = array();
          //$indexObjetos = 0;
          foreach ($auxCategorias as $key) {
            $elementsCategorias = $this->model->getElementsCategoriasSegmento($segmentos[$index]['id_segmento'],$key['id_categoria'], $nivel);
            foreach ($elementsCategorias as $valores) {
              array_push($valoresCategorias, $valores['valores']);
            }
            $key['id_rel'] = $elementsCategorias[0]['id_rel'];
            $key['id_segmento'] = $elementsCategorias[0]['id_segmento'];
            $key['id_rel_objeto_categoria'] = $elementsCategorias[0]['id_rel_objeto_categoria'];
            $key['nivel'] = $elementsCategorias[0]['nivel'];
            $key['id_objeto'] = $elementsCategorias[0]['id_objeto'];
            $key['label'] = $elementsCategorias[0]['label'];
            $key['name'] = $elementsCategorias[0]['name'];
            $key['id_objeto_html'] = $elementsCategorias[0]['id_objeto_html'];
            $key['id_objeto'] = $elementsCategorias[0]['id_objeto'];
            $key['nombre_objeto'] = $elementsCategorias[0]['nombre_objeto'];
            $key['tipo'] = $elementsCategorias[0]['tipo'];
            $key['valores'] = $valoresCategorias;

            array_push($arrayAux['objetos'],$key);
          }
          

          if($categoria['id_categoria'] == 5){
            $arrayAux['container'] = $this->model->getUsuarios(NULL, 2);
          }
          if($categoria['id_categoria'] == 6){
            $arrayAux['container'] = $this->model->getUsuarios(NULL, 3);
          }

          if($categoria['id_categoria'] == 8){
            $arrayAux['container'] = array(
              array('id_usuario' => 1,'nombre' => "Hombre"), 
              array('id_usuario' => 0, 'nombre' => "Mujer") 
            );
          }

          if($categoria['id_categoria'] == 10){
            //die("cate 10");
            $arrayAux['container'] = array(
              array('id_usuario' => "Lunes",'nombre' => "Lunes"), 
              array('id_usuario' => "Martes",'nombre' => "Martes"), 
              array('id_usuario' => "Miércoles",'nombre' => "Miércoles"), 
              array('id_usuario' => "Jueves",'nombre' => "Jueves"), 
              array('id_usuario' => "Viernes",'nombre' => "Viernes"), 
              array('id_usuario' => "Sábado",'nombre' => "Sábado"), 
              array('id_usuario' => "Domingo",'nombre' => "Domingo") 
            );
          }
          $arrayCategorias[$indexCategoria] = $arrayAux;      
        }
        array_push($arrayConfiguracion, $arrayCategorias); 
      }
      
      $segmentos[$index]['configuracion'] = $arrayConfiguracion;
    }    
    return $segmentos;
  }

  //OBTIENE UN ARRAY DE LOS SEGMENTOS
  public function getCategorias($idCategoria = NULL){
    $res=$this->model->getCategorias($idSegmento);    
    return $res;
  }  

  public function getUsuario($idUsuario = NULL){
    return $this->model->getUsuario($idUsuario);
  }

  public function getCategoriaClass(){
    require_once '../../view/administracion/categoria.php';
    
    $categoria = new Categoria($this->ser, $this->usu, $this->pas, $this->bd, true, true);
    return $categoria;

  }

  public function editSegmento($datos = NULL){
    $idSegmento = isset($datos['idSegmento']) ? utf8_decode($datos['idSegmento']) : NULL;
    $resultDelete = $this->model->deleteCategoriasSegmento($idSegmento, null, null);
    //print_r($datos);
    //print_r(array('nombre_segmento' => $datos['nombre'],'descripcion_segmento' => $datos['descripcion']));
    //die();
    $resultUpdate = $this->model->updateSegmento($idSegmento, array('nombre_segmento' => $datos['nombre'],'descripcion_segmento' => $datos['descripcion']));
    //die($resultDelete);
    if($resultDelete && $resultUpdate)
      return $this->insertSegmento($datos);
    else
      return array('result' => 2);
  }

  public function insertSegmento($datos = NULL){
    
    $nombre = isset($datos['nombre']) ? mb_strtoupper(utf8_decode($datos['nombre'])) : NULL;
    $descripcion = isset($datos['descripcion']) ? mb_strtoupper(utf8_decode($datos['descripcion'])) : NULL;
    $idSegmento = isset($datos['idSegmento']) && $datos['idSegmento'] != "" && $datos['idSegmento'] != NULL ? mb_strtoupper(utf8_decode($datos['idSegmento'])) : $this->model->insertSegmentoID($nombre, $descripcion, 0);
    //die("Segmento: ".$idSegmento);
    $indexLevel = 1;    
    $resultInsert = false;
    foreach ($datos['datos'] as $nivel) {
      //print_r($nivel);
      foreach ($nivel as $condicion) {
        //print_r($condicion);
        foreach($condicion['data'] as $categoria){
          //print_r($categoria);
          //die();
          switch ($categoria['id_objeto']){
            case 1:
            case 2:
            case 3:
              $values = $categoria['value'];
              $name = $categoria['name'];
              $idObjeto = $categoria['id_objeto'];
              $idRelacion = $condicion['idRel'];
              $idCategoria = $condicion['idCat'];              
              $resultInsert = $this->model->addSegmentoCategoria($idSegmento, $idCategoria, $idRelacion, $indexLevel, $values);
            break; 
            case 4:
            case 5:
              if(is_array($categoria['valores'])){
                foreach ($categoria['valores'] as $key) {
                  $values = utf8_decode($key);
                  $name = $categoria['name'];
                  $idObjeto = $categoria['id_objeto'];
                  $idRelacion = $condicion['idRel'];
                  $idCategoria = $condicion['idCat'];
                  $resultInsert = $this->model->addSegmentoCategoria($idSegmento, $idCategoria, $idRelacion, $indexLevel, $values);
                }
              }else{
                $values = $categoria['valores'];                
                $name = $categoria['name'];
                $idObjeto = $categoria['id_objeto'];
                $idRelacion = $condicion['idRel'];
                $idCategoria = $condicion['idCat'];
                $resultInsert = $this->model->addSegmentoCategoria($idSegmento, $idCategoria, $idRelacion, $indexLevel, $values);
              }              
            break;

            case 6:
            case 7:
            case 8:
            case 9:
            //case 10:
              //print_r($categoria);
              //die();
              $values = "min=".$categoria['value']['min'].",max=".$categoria['value']['max'];              
              $name = $categoria['name'];
              $idObjeto = $categoria['id_objeto'];
              $idRelacion = $condicion['idRel'];
              $idCategoria = $condicion['idCat'];
              
              $resultInsert = $this->model->addSegmentoCategoria($idSegmento, $idCategoria, $idRelacion, $indexLevel, $values);
            break;

            case 10:
              //print_r($categoria);
              //die();
              $values = "min=".$categoria['value'][0].",max=".$categoria['value'][1];
              $name = $categoria['name'];
              $idObjeto = $categoria['id_objeto'];
              $idRelacion = $condicion['idRel'];
              $idCategoria = $condicion['idCat'];
              
              $resultInsert = $this->model->addSegmentoCategoria($idSegmento, $idCategoria, $idRelacion, $indexLevel, $values);
            break;
          }
        }
      }
      $indexLevel++;
    }
    //die("Segmento: ".$idSegmento);
    $result = $this->updateClientsSegmento(array('idSegmento' => $idSegmento));
    return array('result' => $resultInsert ? 1 : 2);   
  }

  public function updateClientsSegmento($datos = NULL){
    $idSegmento = isset($datos['idSegmento']) ? $datos['idSegmento'] : NULL;
    $segmento =  $this->model->getSegmentos($idSegmento, null, null);
        
    while ($key1 = mysqli_fetch_assoc($segmento)) {
      $objects = $this->model->getQuerySegmentos($key1['id_segmento']);    
      $usuarios = $this->model->getUsuariosSegmento($objects);      
      $removeClients = $this->model->removeClientsSegmento(NULL, $key1['id_segmento']);
      
      foreach ($usuarios as $key) {        
        $resultUpdateSegmento = $removeClients && $this->model->addClientsSegmento($key['id_usuario'], $key1['id_segmento']);        
        if(!$resultUpdateSegmento)
          break;
      }
    }        

    return ($resultUpdateSegmento ? array('count' => count($usuarios)) : false);
  }
  

}
?>