<?php
 /*  * ##+> ################################# <+##
 * CONTROL DE ADMIN USUARIOS
 * Desarrolado ->Miguel Ruiz AND Allan Ayrton
 *  * ##+> ################################# <+##
 */
include_once '../../model/administracion/m_admin_usu.php';
include_once '../../model/administracion/m_admin_segmento.php';
include_once '../../model/valida/m_valida.php';
  class C_admin_usu{
    private $ser;
    private $usu;
    private $pas;
    private $bd;
    

    public function __construct($ser,$usu,$pas,$bd) {
         $this->ser=$ser;
         $this->usu=$usu;
         $this->pas=$pas;
         $this->bd=$bd;
         $this->model=$model=new M_admin_usu();
         $this->segmento_model = new M_admin_segmento($ser,$usu,$pas,$bd);
         $this->valida_model = new M_valida($ser, $usu, $pas, $bd);
    }
//FUNCION TRAE HISTORIAL USUARIO
public function trae_usuarios(){
  $res=$this->model->registros_usuarios($this->ser,$this->usu,$this->pas,$this->bd);  
  return $res;
}

//FUNCION OBTENER CLIENTES
public function get_clientes(){
  $idUsuario = NULL;
  $res=$this->segmento_model->getInfoUser($idUsuario);  
  return $res;
}

//FUNCION ACTUALIZAR CLIENTES
public function updateCliente($idUsuario = NULL, $data = NULL){

  $res=$this->valida_model->updateUser($idUsuario, $data);

  return array('result' => $res ? 1 : 2);
}

//FUNCION ELIMINAR CLIENTE
public function removeCliente($idUsuario = NULL){

  if($idUsuario != NULL){
    $res=$this->valida_model->deleteUser($idUsuario);
    return array('result' => $res ? 1 : 2);  
  }
  
}


//FUNCION PASA PARAMETROS PARA GUARDAR NUEVO USUARIO
public function nuevo_usuario($usua,$pass,$tipo_usu,$tipo_punt_vta){
  $res=$this->model->passrev($this->ser,$this->usu,$this->pas,$this->bd,$pass);  

  while($reg=mysqli_fetch_assoc($res)){
      $count = $reg['countpass'];
  }
  if($count>=1){
    echo 2;
  }else{
    $res2=$this->model->registra_usuario($this->ser,$this->usu,$this->pas,$this->bd,$usua,$pass,$tipo_usu,$tipo_punt_vta);
    echo 1;
  }
    
}

public function edita_usuario($id,$usua,$pass,$tipo_usu,$tipo_punt_vta){
  $res=$this->model->passrev($this->ser,$this->usu,$this->pas,$this->bd,$pass);  

  while($reg=mysqli_fetch_assoc($res)){
      $count = $reg['countpass'];
  }
  if($count>=1){
    echo 2;
  }else{
    $res2=$this->model->edita_usuario($this->ser,$this->usu,$this->pas,$this->bd,$id,$usua,$pass,$tipo_usu,$tipo_punt_vta);
    echo 1;
  }
    
}

public function grideditable($id){
  $res=$this->model->grideditable($this->ser,$this->usu,$this->pas,$this->bd,$id);

      while($reg=mysqli_fetch_assoc($res) ){
        $usuario = $reg['usuario'];
        $rol = $reg['rol'];

        ?>
              <div class="form-line">
                <input type="text" class="form-control"  name='nom_usu' placeholder="Nombre de Usuario" id='usuario' value="<?php echo $usuario; ?>">
              </div>
              <br>
              <div class="form-line">
                &nbsp;<input type="password" class="form-control"  name='pass_usu' placeholder="Password" id='password' required>
              </div>
              <br>
              Tipo de Usuario:
              <div class="demo-radio-button">
                <?php 
                  if($rol==1){
                    ?>
                      <input name="tipo_usu" type="radio" id="tipo_usu_1"  value='1' checked />
                      <label for="tipo_usu_1">Administrador</label>
                      <input name="tipo_usu" type="radio" id="tipo_usu_2" value='2'/>
                      <label for="tipo_usu_2">Punto de Venta</label>
                    <?php
                  }else{
                    ?>
                      <input name="tipo_usu" type="radio" id="tipo_usu_1"  value='1'/>
                      <label for="tipo_usu_1">Administrador</label>
                      <input name="tipo_usu" type="radio" id="tipo_usu_2" value='2' checked />
                      <label for="tipo_usu_2">Punto de Venta</label>
                    <?php
                  }
                ?>
               
              </div>  
        <?php
      }//end while
    }//end function gridedit to users

//FUNCION PASA PARAMETROS PARA GUARDAR NUEVO USUARIO
public function elimina_usuario($id_usu){
  $res=$this->model->elimina_usuario($this->ser,$this->usu,$this->pas,$this->bd,$id_usu);  
  return $res;
    
}

}
?>