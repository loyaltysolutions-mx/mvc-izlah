<?php
/*By Allan*/
include_once '../../inc/parametros.php';
include_once '../../model/administracion/m_admin_menu.php';
class C_admin_menu{
    private $ser;
    private $usu;
    private $pas;
    private $bd;
    

  public function __construct($ser,$usu,$pas,$bd) {
    $this->ser=$ser;
    $this->usu=$usu;
    $this->pas=$pas;
    $this->bd=$bd;
    $this->model=$model=new M_admin_menu();
  }
//function to get categories (options)
  public function selectcat(){
    $res=$this->model->selectcat($this->ser,$this->usu,$this->pas,$this->bd);

    while($reg=mysqli_fetch_assoc($res)){
      $id = $reg['id_cat'];
      $cat = $reg['nombre'];
      
      ?>
          <option value="<?php echo $id; ?>"><?php echo utf8_encode($cat); ?></option>
      <?php
    }
  }

  public function selectedcat($selected){
    $res=$this->model->selectcat($this->ser,$this->usu,$this->pas,$this->bd);

    while($reg=mysqli_fetch_assoc($res) ){
            $id = $reg['id_cat'];
            $cat = $reg['nombre'];
            if($id==$selected){
              ?>
                <option value="<?php echo $id; ?>" selected><?php echo utf8_encode($cat); ?></option>
              <?php
            }else{
              ?>
                <option value="<?php echo $id; ?>"><?php echo utf8_encode($cat); ?></option>
              <?php
            }
            
          }
  }

  public function updateimg($id,$file,$table,$field,$imgfld){
    $res=$this->model->updateimg($this->ser,$this->usu,$this->pas,$this->bd,$id,$file,$table,$field,$imgfld);
  }

  public function addmenu($menu,$cat,$descript,$price,$filename){
    $res=$this->model->addmenu($this->ser,$this->usu,$this->pas,$this->bd,$menu,$cat,$descript,$price,$filename);
    echo 1;
  }

  public function editmenu($id,$name,$descript,$cat,$price){
    $res=$this->model->editmenu($this->ser,$this->usu,$this->pas,$this->bd,$id,$name,$descript,$cat,$price);
    echo 1;
  }

  //deletes
  public function delrow($id,$table,$field,$response){
    $res=$this->model->delrow($this->ser,$this->usu,$this->pas,$this->bd,$id,$table,$field);
    echo $response;
  }

  //edit menu info
  public function grideditm($option){
    $res=$this->model->grideditm($this->ser,$this->usu,$this->pas,$this->bd);

    //while($reg=mysqli_fetch_assoc($res)){
      //$arr = $reg[''];
    //}

    if($option=='json'){
        echo $json;
    }elseif($option=='tr'){
      while($reg=mysqli_fetch_assoc($res) ){
        $id = $reg['id_menu'];
        $name = $reg['titulo'];
        $descript = $reg['descripcion'];
        $img = $reg['imagen'];
        $price = $reg['precio'];
        $cat = $reg['id_cat'];
        
        $active = $reg['activo'];

        if($active==0){
          $ischeck='checked="true"';
        }elseif($active==1){
          $ischeck="";
        }

        if(empty($img) OR $img==''){
          $txt = 'Agregar Imagen';
        }else{
          $txt = 'Editar Imagen';
        }
          ?>
              <tr>
                  <td class='bor_der text-center'><?php echo $id;?></td>
                  <td class='bor_der text-center'><input class="form-control" type="text" id="n<?php echo $id;?>" value="<?php echo utf8_encode($name);?>"></td>
                  <td class='bor_der text-center'><input class="form-control" type="text" id="d<?php echo $id;?>" value="<?php echo utf8_encode($descript);?>"></td>
                  <td class='bor_der text-center'>
                    <select id="c<?php echo $id;?>" name="cat" onchange="" class="form-control show-tick" style="<?php echo $style; ?>">
                      <option value="">Elige una Categoría</option>
                      <?php echo $this->selectedcat($cat); ?>
                    </select>
                  </td>
                  <td class='bor_der text-center'><input class="form-control" type="text" id="p<?php echo $id;?>" value="<?php echo ($price);?>"></td>
                  <td class='bor_der text-center'><img src="../../inc/imagenes/img_menu/<?php echo !empty($reg['imagen']) && $reg['imagen'] != null ? $reg['imagen'] : 'no-image.png' ?>" style="width:80px"></td>
                  <td class='bor_der text-center row'><a href="javascript:;" class='mod-al btn btn-info btnModal' data-action='<?php echo $id;?>' ><?php echo $txt; ?></a></td>
                  <td class='bor_der text-center'><button class='btn btn-info' id='<?php echo $id;?>' onclick='editmenu(this.id)'>Editar</button></td>
                  <td>
                    <div class="switch">
                      <label><input id="x<?php echo $id;?>" class="ifcheck" type="checkbox"<?php echo $ischeck; ?>><span class="lever switch-col-blue"></span></label>
                    </div>
                  </td>
                  <td class='bor_der text-center'><button class='btn btn-danger' id='<?php echo $id;?>' name="<?php echo utf8_encode($name);?>" onclick="delrow(this.id,'tbl_menu','id_menu',2);"><i class="fas fa-trash-alt"></i></button></td>
              </tr> 
          <?php
        }//end while
      }//end elseif
    }//end function gridedit to menu

    public function uploadgrid($option){
    $res=$this->model->gridupload($this->ser,$this->usu,$this->pas,$this->bd);

    //while($reg=mysqli_fetch_assoc($res)){
      //$arr = $reg[''];
    //}

    if($option=='json'){
        echo $json;
    }elseif($option=='tr'){
      while($reg=mysqli_fetch_assoc($res) ){
        $id = $reg['id_menu'];
        $name = $reg['titulo'];
        $descript = $reg['descripcion'];
        $img = $reg['imagen'];
        if(empty($img) OR $img==''){
          $txt = 'Agregar Imagen';
        }else{
          $txt = 'Editar Imagen';
        }
        $price = $reg['precio'];
        $cat = $reg['id_cat'];
          ?>
              <tr>
                  <td class='bor_der text-center'><?php echo $id;?></td>
                  <td class='bor_der text-center'><?php echo utf8_encode($name);?></td>
                  <td class='bor_der text-center'><?php echo utf8_encode($descript);?></td>
                  <td class='bor_der text-center'>
                        <?php echo $cat; ?>
                  </td>
                  <td class='bor_der text-center'><?php echo ($price);?></td>
                  <!--td class='bor_der text-center row'><button class='mod-al btn btn-info' id='i<?php //echo $id;?>' onclick="cleanfile('<?php //echo $id;?>')" data-toggle="modal" data-target="#editimg"><?php //echo $txt; ?></button></td>
                  <td class='bor_der text-center'><button class='btn btn-danger' id='<?php //echo $id;?>' name="<?php //echo utf8_encode($name);?>" onclick="delrow(this.id,'tbl_menu','id_menu',2);"><i class="fas fa-trash-alt"></i></button></td-->
              </tr> 
          <?php
        }//end while
      }//end elseif
    }//end function gridedit to menu

}
?>