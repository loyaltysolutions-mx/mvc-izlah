<?php
/*By Allan*/
include_once '../../inc/parametros.php';
include_once '../../model/administracion/m_admin_categoria.php';
class C_admin_categoria{
    private $ser;
    private $usu;
    private $pas;
    private $bd;
    

  public function __construct($ser,$usu,$pas,$bd) {
    $this->ser=$ser;
    $this->usu=$usu;
    $this->pas=$pas;
    $this->bd=$bd;
    $this->model=$model=new M_admin_categoria();
  }


  public function addcat($cat,$descript){
    $res=$this->model->addcat($this->ser,$this->usu,$this->pas,$this->bd,$cat,$descript);
    echo 1;
  }

  public function editcat($id,$name,$descript){
    $res=$this->model->editcat($this->ser,$this->usu,$this->pas,$this->bd,$id,$name,$descript);
    echo 1;
  }

  //activate 
  public function active($id,$table,$field){
    $res=$this->model->active($this->ser,$this->usu,$this->pas,$this->bd,$id,$table,$field);
    echo 1;
  }

  //desactivate 
  public function unactive($id,$table,$field){
    $res=$this->model->unactive($this->ser,$this->usu,$this->pas,$this->bd,$id,$table,$field);
    echo 1;
  }

  //deletes
  public function delrow($id,$table,$field,$response){
    $res=$this->model->delrow($this->ser,$this->usu,$this->pas,$this->bd,$id,$table,$field);
    echo $response;
  }

  //edit coupons info
  public function grideditc($option){
    $res=$this->model->grideditc($this->ser,$this->usu,$this->pas,$this->bd);

    //while($reg=mysqli_fetch_assoc($res)){
      //$arr = $reg[''];
    //}

    if($option=='json'){
        echo $json;
    }elseif($option=='tr'){
      while($reg=mysqli_fetch_assoc($res) ){
        $id = $reg['id_cat'];
        $name = $reg['nombre'];
        $descript = $reg['descripcion'];
        $active = $reg['activo'];

        if($active==0){
          $ischeck='checked="true"';
        }elseif($active==1){
          $ischeck="";
        }
        
          ?>
              <tr>
                  <td class='bor_der text-center'><?php echo $id;?></td>
                  <td class='bor_der text-center'><input class="form-control" type="text" id="n<?php echo $id;?>" value="<?php echo utf8_encode($name);?>"></td>
                  <td class='bor_der text-center'><input class="form-control" type="text" id="d<?php echo $id;?>" value="<?php echo utf8_encode($descript);?>"></td>
                  <td class='bor_der text-center'><button class='btn btn-info' id='<?php echo $id;?>' onclick='editcate(this.id)'>Editar</button></td>
                  <td>
                    <div class="switch">
                        <label><input id="x<?php echo $id;?>" class="ifcheck" type="checkbox"<?php echo $ischeck; ?>><span class="lever switch-col-blue"></span></label>
                    </div>
                  </td>
                  <td class='bor_der text-center'><button class='btn btn-danger' id='<?php echo $id;?>' name="<?php echo utf8_encode($name);?>" onclick="delrow(this.id,'tbl_categoria','id_cat',3);"><i class="fas fa-trash-alt"></i></button></td>
              </tr> 
          <?php
        }//end while
      }//end elseif
    }//end function gridedit to coupons

}
?>