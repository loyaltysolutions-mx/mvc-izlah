<?php
/*By Allan*/
include_once '../../inc/parametros.php';
include_once '../../model/administracion/m_admin_promociones.php';
class C_admin_promociones{
    private $ser;
    private $usu;
    private $pas;
    private $bd;
    

  public function __construct($ser,$usu,$pas,$bd) {
    $this->ser=$ser;
    $this->usu=$usu;
    $this->pas=$pas;
    $this->bd=$bd;
    $this->model=$model=new M_admin_promociones();
  }

  //select Promo/Campaign type
  public function projectype(){
    $res=$this->model->projectype($this->ser,$this->usu,$this->pas,$this->bd);

    while($reg=mysqli_fetch_assoc($res) ){
            $type = $reg['id_tipo_proyecto'];
          }
    return $type;      
  }

  public function addpromo($suc,$segment,$lvl,$promo,$descript,$smalltxt,$type,$conditions,$howuse,$infolink,$bonustype,$rewardto,$rewarperc,$npromos,$nredention,$start,$ending,$push,$sms,$whats,$email,$pushtxt,$smstxt,$whatstxt,$emailtxt,$sendtime,$cashback,$cashbackperc,$descuento,$codigo){
    $res=$this->model->addpromo($this->ser,$this->usu,$this->pas,$this->bd,$promo,$descript,$smalltxt,$type,$conditions,$howuse,$infolink,$bonustype,$rewardto,$rewarperc,$npromos,$nredention,$start,$ending,$push,$sms,$whats,$email,$pushtxt,$smstxt,$whatstxt,$emailtxt,$sendtime,$cashback,$cashbackperc,$descuento,$codigo);
    
    //IMPORTANT!! This echo generated an id to qr img
    echo $res;
    //add img to table DB
    $id = $res;
    $img= $res.'.png';
    //insert relation promotion/sucursal/segment
    $res1=$this->model->addpromorel($this->ser,$this->usu,$this->pas,$this->bd,$id,$suc,$segment,$lvl);

    $res2=$this->model->addimg($this->ser,$this->usu,$this->pas,$this->bd,$id,$img);
  }

  //select Promo/Campaign type
  public function selectype(){
    $res=$this->model->selectype($this->ser,$this->usu,$this->pas,$this->bd);

    while($reg=mysqli_fetch_assoc($res) ){
      $id = $reg['id_camp'];
      $nombre = $reg['nombre'];
      
      ?>
          <option value="<?php echo $id; ?>"><?php echo utf8_encode($nombre); ?></option>
      <?php
    }
  }

  public function selectedtype($selected){
    $res=$this->model->selectype($this->ser,$this->usu,$this->pas,$this->bd);

    $res2=$this->model->selectedtype($this->ser,$this->usu,$this->pas,$this->bd,$selected);
      while($reg2=mysqli_fetch_assoc($res2) ){
        $idtype = $reg2['tipo'];
      }

    while($reg=mysqli_fetch_assoc($res) ){
      $id = $reg['id_camp'];
      $nombre = $reg['nombre'];
      if($id==$idtype){
        $select = "selected";
      }else{
        $select ="";
      }
      ?>
          <option value="<?php echo $id; ?>" <?php echo $select;?> ><?php echo utf8_encode($nombre); ?></option>
      <?php
    }//end while
  }

  //select suc to add promo
  public function selectsuc(){
    $res=$this->model->selectsuc($this->ser,$this->usu,$this->pas,$this->bd);

    while($reg=mysqli_fetch_assoc($res) ){
            $id = $reg['id_sucursales'];
            $suc = $reg['nombre'];
            
            ?>
                <option value="<?php echo $id; ?>"><?php echo utf8_encode($suc); ?></option>
            <?php
          }
  }

  //select segment to add promo
  public function selectsegment(){
    $res=$this->model->selectsegment($this->ser,$this->usu,$this->pas,$this->bd);

    while($reg=mysqli_fetch_assoc($res) ){
            $id = $reg['id_segmento'];
            $seg = $reg['nombre_segmento'];
            
            ?>
                <option value="<?php echo $id; ?>"><?php echo utf8_encode($seg); ?></option>
            <?php
          }
  }

  //select lvl to add promo
  public function selectlvl(){
    $res=$this->model->selectlvl($this->ser,$this->usu,$this->pas,$this->bd);

    while($reg=mysqli_fetch_assoc($res) ){
            $id = $reg['id_nivel'];
            $lvl = $reg['nombre'];
            
            ?>
                <option value="<?php echo $id; ?>"><?php echo utf8_encode($lvl); ?></option>
            <?php
          }
  }

  public function selectedsuc($selected,$opt){
    $res=$this->model->selectsuc($this->ser,$this->usu,$this->pas,$this->bd);

    $res2=$this->model->selectedsuc($this->ser,$this->usu,$this->pas,$this->bd,$selected);
      while($reg2=mysqli_fetch_assoc($res2) ){
        $idsuc[] = $reg2['id_sucursal'];
      }

      //var_dump($idsuc);
      
      while($reg=mysqli_fetch_assoc($res) ){
        $id = $reg['id_sucursales'];
        $suc = $reg['nombre'];
        if($opt=='tbl'){
          if(in_array($id, $idsuc)){
            ?>  
              <p><span class="label label-default"><?php echo utf8_encode($suc); ?></span></p>
            <?php
          }else{
            
          }
        }else{
          if(in_array($id, $idsuc)){
            ?>
              <option value="<?php echo $id; ?>" selected><?php echo utf8_encode($suc); ?></option>
            <?php
          }else{
            ?>
              <option value="<?php echo $id; ?>"><?php echo utf8_encode($suc); ?></option>
            <?php
          }
        }//end if/else tbl
        
        
      }//end While
        
  }//end function

  public function selectedsegm($selected,$opt){
    $res=$this->model->selectsegment($this->ser,$this->usu,$this->pas,$this->bd);

    $res2=$this->model->selectedsegm($this->ser,$this->usu,$this->pas,$this->bd,$selected);
      while($reg2=mysqli_fetch_assoc($res2) ){
        $idseg[] = $reg2['id_segmento'];
      }

      //var_dump($idsuc);

      while($reg=mysqli_fetch_assoc($res) ){
        $id = $reg['id_segmento'];
        $seg = $reg['nombre_segmento'];
        if($opt=='tbl'){
          if(in_array($id, $idseg)){
            ?>  
              <p><span class="label label-default"><?php echo utf8_encode($seg); ?></span></p>
            <?php
          }else{
            
          }
        }else{
          if(in_array($id, $idseg)){
            ?>
              <option value="<?php echo $id; ?>" selected><?php echo utf8_encode($seg); ?></option>
            <?php
          }else{
            ?>
              <option value="<?php echo $id; ?>"><?php echo utf8_encode($seg); ?></option>
            <?php
          }
        }//end if/else tbl  
        
      }  
  }

  public function selectedlvl($selected,$opt){
    $res=$this->model->selectlvl($this->ser,$this->usu,$this->pas,$this->bd);

    $res2=$this->model->selectedlvl($this->ser,$this->usu,$this->pas,$this->bd,$selected);
      while($reg2=mysqli_fetch_assoc($res2) ){
        $idlvl[] = $reg2['id_nivel'];
      }

      //var_dump($idsuc);

      while($reg=mysqli_fetch_assoc($res) ){
        $id = $reg['id_nivel'];
        $lvl = $reg['nombre'];
        if($opt=='tbl'){
          if(in_array($id, $idlvl)){
            ?>  
              <p><span class="label label-default"><?php echo utf8_encode($lvl); ?></span></p>
            <?php
          }else{
            
          }
        }else{
          if(in_array($id, $idlvl)){
            ?>
              <option value="<?php echo $id; ?>" selected><?php echo utf8_encode($lvl); ?></option>
            <?php
          }else{
            ?>
              <option value="<?php echo $id; ?>"><?php echo utf8_encode($lvl); ?></option>
            <?php
          }
        }//end if/else tbl    
        
      }  
  }

  public function editpromo($id,$suc,$segment,$lvl,$promo,$descript,$smalltxt,$type,$conditions,$howuse,$infolink,$bonustype,$rewardto,$rewarperc,$npromos,$nredention,$start,$ending,$push,$sms,$whats,$email,$pushtxt,$smstxt,$whatstxt,$emailtxt,$sendtime,$cashback,$cashbackperc,$descuento,$codigo){
    $res=$this->model->editpromo($this->ser,$this->usu,$this->pas,$this->bd,$id,$promo,$descript,$smalltxt,$type,$conditions,$howuse,$infolink,$bonustype,$rewardto,$rewarperc,$npromos,$nredention,$start,$ending,$push,$sms,$whats,$email,$pushtxt,$smstxt,$whatstxt,$emailtxt,$sendtime,$cashback,$cashbackperc,$descuento,$codigo);

    $res1=$this->model->delrow($this->ser,$this->usu,$this->pas,$this->bd,$id,'tbl_rel_promo_suc_segmento','id_promo');

    $res2=$this->model->addpromorel($this->ser,$this->usu,$this->pas,$this->bd,$id,$suc,$segment,$lvl);
    echo 1;
  }

  //deletes
  public function delrow($id,$table,$field,$response){
    $res=$this->model->delrow($this->ser,$this->usu,$this->pas,$this->bd,$id,$table,$field);
    echo $response;
  }

  //duplicate
  public function duplicate($id){
    $res=$this->model->duplicate($this->ser,$this->usu,$this->pas,$this->bd,$id);
    $idnew = $res;
    //review fields
    $res2=$this->model->duplicaterel($this->ser,$this->usu,$this->pas,$this->bd,$id,$idnew);

    echo 1;
  }

  //used # promotions
  public function usedpromo($id){
    $res=$this->model->usedpromo($this->ser,$this->usu,$this->pas,$this->bd,$id);
    while($reg=mysqli_fetch_assoc($res) ){
      $usedpromos = $reg['usedpromos'];
    }
    echo $usedpromos;
  }

  //edit promos info
  public function grideditp($option){
    $res=$this->model->grideditp($this->ser,$this->usu,$this->pas,$this->bd);

    //while($reg=mysqli_fetch_assoc($res)){
      //$arr = $reg[''];
    //}
    $opt = 'tbl';
    if($option=='json'){
        echo $json;
    }elseif($option=='tr'){
      while($reg=mysqli_fetch_assoc($res) ){
        $id = $reg['id_promociones'];
        $promo = $reg['nombre'];
        $descript = $reg['detalle'];
        $smalltxt = $reg['texto_corto'];
        $start = $reg['comienza'];
        $ending = $reg['vigencia'];

        $active = $reg['activo'];

        if($active==0){
          $ischeck='checked="true"';
        }elseif($active==1){
          $ischeck="";
        }
          ?>
              <tr>
                  <td class='bor_der text-center'><?php echo $id;?></td>
                  <td class='bor_der text-center'><?php echo utf8_encode($promo);?></td>
                  <td class='bor_der text-center'><?php echo utf8_encode($descript);?></td>
                  <td class='bor_der text-center'><?php echo utf8_encode($smalltxt);?></td>
                  <td class='bor_der text-center'>
                    <div class="input-group">
                      <?php echo $this->selectedsegm($id,$opt); ?>
                    </div>   
                  </td>
                  <td class='bor_der text-center'>
                    <div class="input-group">
                      <?php echo $this->selectedsuc($id,$opt); ?>
                    </div>   
                  </td>
                  <td class='bor_der text-center'>
                    <div class="input-group">
                      <?php echo $this->selectedlvl($id,$opt); ?>
                    </div>   
                  </td>
                  <td class='bor_der text-center'><?php echo $start;?></td>
                  <td class='bor_der text-center'><?php echo $ending;?></td>
                  <td id="r<?php echo $id;?>" class='bor_der text-center'>
                    <?php echo $this->usedpromo($id); ?>  
                  </td>
                  <td>
                    <div class="switch">
                        <label><input id="x<?php echo $id;?>" class="ifcheck" type="checkbox"<?php echo $ischeck; ?>><span class="lever switch-col-blue"></span></label>
                    </div>
                  </td>
                  <td class='bor_der text-center'>
                    <div class="action-container">
                      <button class="refresh" onclick="refreshing('<?php echo $id;?>');"><i id="ir<?php echo $id;?>" class="fas fa-sync-alt"></i></button>
                      <button class='edit' id='<?php echo $id;?>' onclick='editpromoview(this.id)'><i class="fas fa-pen"></i></button>
                      <button class='delete' id='<?php echo $id;?>' name="<?php echo utf8_encode($promo);?>" onclick="delrow(this.id,'tbl_promociones','id_promociones',5); delrow2(this.id,'tbl_rel_promo_suc_segmento','id_promo',5);"><i class="fas fa-trash-alt"></i></button>
                      <button class='copy' id="" onclick="duplicate('<?php echo $id;?>')"><i class="far fa-copy"></i></button>
                    </div>
                  </td>
              </tr> 
          <?php
        }//end while
      }//end elseif
    }//end function gridedit to promos


    public function grideditable($id){
    $res=$this->model->grideditable($this->ser,$this->usu,$this->pas,$this->bd,$id);

      while($reg=mysqli_fetch_assoc($res) ){
        $tipo = $reg['tipo'];
        $id = $reg['id_promociones'];
        $promo = $reg['nombre'];
        $descript = $reg['detalle'];
        $smalltxt = $reg['texto_corto'];
        $conditions = $reg['condiciones'];
        $howuse = $reg['uso'];
        $infolink = $reg['link_info'];
        $rewardto = $reg['recompensa_directa'];
        $rewardperc = $reg['recompensa_porcentaje'];
        $npromos  = $reg['limite'];
        $nredention = $reg['limite_por_usuario'];
        $start = $reg['comienza'];
        $ending = $reg['vigencia'];
        $push = $reg['push'];
        
        if($npromos==999999999){
          $npromocheck = "checked";
          $stylenpromo = 'style="display: none;"';
        }
        if($nredention==999999999){
          $nredencheck = "checked";
          $stylenreden = 'style="display: none;"'; 
        }

        if($push==1){
          $push = "checked";
        }else{
          $push = "";
        }
        $sms = $reg['sms'];
        if($sms==1){
          $sms = "checked";
        }else{
          $sms = "";
        }
        $whats = $reg['whatsapp'];
        if($whats==1){
          $whats = "checked";
        }else{
          $whats = "";
        }
        $email = $reg['email'];
        if($email==1){
          $email = "checked";
        }else{
          $email = "checked";
        }
        
        $sendtime = $reg['fecha_envio'];
        $cashback = $reg['cashback'];
        $cashbackperc = $reg['cashbackperc'];
        $descuento = $reg['descuento'];
        $codigo = $reg['codigo'];
        $pushtxt = $reg['pushtxt'];
        $smstxt = $reg['smstxt'];
        $whatstxt = $reg['whatstxt'];
        $emailtxt = $reg['emailtxt'];

        if($tipo==1){
          $styleval = 'style="display: none;"';
          $styleperc = 'style="display: none;"';
        }elseif($tipo==3){
          $stylecash = 'style="display: none;"';
          $styleperc = 'style="display: none;"';
        }elseif($tipo==6){
          $stylecash = 'style="display: none;"';
          $styleval = 'style="display: none;"';
        }else{
          $stylecash = 'style="display: none;"';
          $styleval = 'style="display: none;"';
          $styleperc = 'style="display: none;"';
        }

        if($tipo==1 OR $tipo==2){
            $tipo = 'puntos';
        }elseif($tipo==3){
            $tipo = 'visitas';
        }else{
            $tipo= 'error';
        }

        ?>  <form id="addpromo" onsubmit="editpromo('<?php echo $id;?>'); return false;">
            <!-- Init Accordion -->
              <div class="panel-group" id="accordion_1" role="tablist" aria-multiselectable="true">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons"></i>
                    </span>
                    <h5>* Tipo</h5>
                    <div class="form-line">
                        <select id="type" name="type" onchange="changesel(this.value)" class="form-control show-tick" required>
                            <option value="" selected>Selecciona el tipo de Campaña</option>
                            <?php
                                echo $this->selectedtype($id);
                            ?>    
                        </select>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control" id="promo" name="promo" placeholder="* Nombre Promoción" value="<?php echo utf8_encode($promo);?>" required>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control" id="detail" name="detail" placeholder="* Detalle" value="<?php echo utf8_encode($descript);?>" required>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control" id="smalltxt" name="smalltxt" placeholder="* Texto Corto" value="<?php echo utf8_encode($smalltxt);?>" required>
                    </div>
                </div>
                <div id="cashbackd" class="row" <?php echo $stylecash; ?> >
                    <div class="col-lg-5 col-md-5 col-xs-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="number" class="form-control" id="cashback" name="cashback" placeholder="$ Valor a Devolver" value="<?php echo $cashback; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-12">
                        <div class="input-group">
                            <div align="center">
                                Ó
                            </div>
                        </div>    
                    </div>    
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="number" class="form-control" id="cashbackperc" name="cashbackperc" placeholder="% Porcentaje a Devolver" value="<?php echo $cashbackperc; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="customd" class="input-group" <?php echo $styleval; ?> >
                    <div class="form-line">
                        <input type="number" class="form-control" id="custom" name="custom" placeholder="" value="<?php echo $descuento; ?>">
                    </div>
                </div>
                <div id="codigod" class="input-group" <?php echo $styleperc; ?> >
                    <div class="form-line">
                        <input type="text" class="form-control" id="codigo" name="codigo" placeholder="* Código" value="<?php echo $codigo; ?>">
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading" role="tab" id="headingOne_1">
                        <h4 class="panel-title primary-back">
                            <a onclick="togglemode('step1');" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#step1">
                                <i class="fas fa-plus"></i> Condiciones, Ver más, Uso...
                            </a>
                        </h4>
                    </div>
                    <div id="step1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_1" style="display: none;">
                        <div class="panel-body">
                            <!-- Init Accordion content 1 -->
                            <div class="input-group">
                                <div class="form-line">
                                    <textarea rows="4" class="form-control no-resize" id="conditions" name="conditions" placeholder=" Condiciones"><?php echo utf8_encode($conditions);?></textarea>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="form-line">
                                    <textarea rows="4" class="form-control no-resize" id="howuse" name="howuse" placeholder=" ¿Cómo usar cupones?"><?php echo utf8_encode($howuse);?></textarea>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="infolink" name="infolink" value="<?php echo utf8_encode($infolink);?>" placeholder=" Link de más información">
                                </div>
                            </div>
                            <!-- End Accordion content -->
                        </div>
                    </div>
                </div>
                <br/>
                <div class="panel panel-primary">
                    <div class="panel-heading" role="tab" id="headingTwo_1">
                        <h4 class="panel-title primary-back">
                            <a onclick="togglemode('step2');" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#step2">
                                <i class="fas fa-plus"></i> Bonus (si es que aplica)...
                            </a>
                        </h4>
                    </div>
                    <div id="step2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_1" style="display: none;">
                        <div class="panel-body">
                            <!-- Init Accordion content 1 -->
                                <div class="input-group">
                                    <div class="form-line">
                                        <select id="bonustype" name="bonustype" class="form-control show-tick">
                                            <option value="0" selected>Selecciona el tipo de Retribución</option>
                                            <!--option value="1">Excluyente(solo <?php //echo $tipo; ?> Bonus, Reemplazando la Configuración General)</option-->
                                            <option value="2">Puntos por defecto en el Sistema + <?php echo $tipo; ?> de Bonus)</option>    
                                        </select>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <div class="form-line">
                                        <input type="hidden" class="form-control" id="projectype" name="projectype" value="<?php echo $tipo; ?>">
                                        <input type="number" class="form-control" id="rewardto" name="rewardto" value="<?php echo $rewardto; ?>" placeholder=" Valor de <?php echo $tipo; ?> (Bonus)">
                                    </div>
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">%</span>
                                    <div class="form-line">
                                        <input type="number" class="form-control" id="rewardperc" name="rewardperc" value="<?php echo $rewardperc; ?>" placeholder=" Valor en Multiplos Ejem. <?php echo $tipo; ?> triples:(%) 300">
                                    </div>
                                </div>
                            <!-- End Accordion content -->
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-xs-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="number" min="1" class="form-control" id="npromos" name="npromos" placeholder="* Límite de Promociones" value="<?php echo $npromos; ?>" required="" <?php echo $stylenpromo; ?> >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="checkbox" class="ilimitado" id="ilimitadop" class="filled-in" <?php echo $npromocheck; ?> >
                                <label for="ilimitadop">Promociones Ilimitadas</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="number" min="1" class="form-control" id="nredention" name="nredention" placeholder="* Limite de uso por Usuario" value="<?php echo $nredention; ?>" required="" <?php echo $stylenreden; ?> >
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="checkbox" class="ilimitado" id="ilimitador" class="filled-in" <?php echo $nredencheck; ?> >
                                <label for="ilimitador">Ilimitadas</label>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Input´s with mask Date -->
                <div class="demo-masked-input">
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-xs-12">
                            <b>* Inicio de Promoción</b>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">date_range</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" id="start" name="start" class="form-control date datepicker" placeholder="Ejem: 9999-01-01" value="<?php echo $start; ?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-xs-12">
                            <b>* Fin de Promoción</b>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">date_range</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" id="ending" name="ending" class="form-control date datepicker" placeholder="Ejem: 9999-01-01" value="<?php echo $ending; ?>" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Input´s with mask Date -->
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-xs-6">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="checkbox" id="push" class="filled-in" <?php echo $push; ?> >
                                <label for="push">¿Envíar Push?</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-6">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="checkbox" id="sms" class="filled-in" <?php echo $sms; ?> >
                                <label for="sms">¿Envíar Sms?</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-6">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="checkbox" id="whats" class="filled-in" <?php echo $whats; ?> >
                                <label for="whats">¿Envíar WhatsApp?</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-6">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="checkbox" id="email" class="filled-in" <?php echo $email; ?> >
                                <label for="email">¿Envíar Email?</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-xs-12">
                        <b>Fecha y Hora de Envío</b>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">date_range</i>
                            </span>
                            <div class="form-line">
                                <input mdc-datetime-picker="" date="true" time="true" type="text" id="sendtime" placeholder="Time" minutes="true" minute-steps="30" value="<?php echo $sendtime; ?>" min-date="minDate" auto-ok="true" format="hh" short-time="true" ng-model="time" class="md-input datetimepicker">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="pushdiv" class="input-group" style="display: none;">
                    <div class="form-line">
                        <input type="text" class="form-control" id="pushtxt" name="pushtxt" value="<?php echo $pushtxt; ?>" placeholder=" Agregar Notificación para App">
                    </div>
                </div>
                <div id="smsdiv" class="input-group" style="display: none;">
                    <div class="form-line">
                        <input type="text" class="form-control" id="smstxt" name="smstxt" minlength="5" maxlength="160" value="<?php echo $smstxt; ?>" placeholder=" Agregar Notificación para Sms">
                    </div>
                </div>
                <div id="whatsdiv" class="input-group" style="display: none;">
                    <div class="form-line">
                        <input type="text" class="form-control" id="whatstxt" name="whatstxt" value="<?php echo $whatstxt; ?>" placeholder=" Agregar Notificación para WhatsApp">
                    </div>
                </div>
                <!-- Init send sms -->
                <div id="emaildiv" class="panel panel-primary" style="display: none;">
                    <div class="panel-heading" role="tab" id="heading_4">
                        <h4 class="panel-title primary-back">
                            <a onclick="togglemode('step4');" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#step4">
                                <i class="fas fa-plus"></i> Texto Email...
                            </a>
                        </h4>
                    </div>
                    <div id="step4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_4" style="display: none;">
                        <div class="panel-body">
                            <!-- Init Accordion content 1 -->
                            <textarea name="emailtxt" id="emailtxt" rows="10" cols="80" placeholder="Message Email">
                                <?php echo $emailtxt; ?>
                            </textarea>
                            <!-- End Accordion content -->
                        </div>
                    </div>
                </div>
                <br/>
                            
                <!-- End send sms -->
                <div class="panel panel-primary">
                    <div class="panel-heading" role="tab" id="heading3_1">
                        <h4 class="panel-title primary-back">
                            <a onclick="togglemode('step3');" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#step3">
                                <i class="fas fa-plus"></i> * Asignacion a Sucursales, Segmentos y/o Niveles
                            </a>
                        </h4>
                    </div>
                    <div id="step3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3_1" style="display: none;">
                        <div class="panel-body">
                            <!-- Init Accordion content 1 -->
                                <div class="input-group">
                                    <h5>* Segmento(s)</h5>
                                    <div class="body">
                                        <div id='select-all' class="btn btn-primary btn-block waves-effect" >Todos los Segementos</div>
                                        <div id='deselect-all' class="btn btn-primary btn-block waves-effect" >Quitar Segementos</div>
                                        <select id="segment" name="segment[]" class="ms"  multiple="multiple">
                                            <optgroup label="Elige uno o varios Segmentos">
                                                <?php echo $this->selectedsegm($id); ?>
                                            </optgroup>    
                                        </select>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <div align="center">
                                        <h2>Y/O</h2>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <h5>* Sucursal(es)</h5>
                                    <div class="body">
                                        <div id='select-all2' class="btn btn-primary btn-block waves-effect" >Todas las Sucursales</div>
                                        <div id='deselect-all2' class="btn btn-primary btn-block waves-effect" >Quitar Sucursales</div>
                                        <select id="suc" name="suc[]" class="ms"  multiple="multiple" >
                                            <optgroup label="Elige uno o varias sucursales">
                                                <?php echo $this->selectedsuc($id); ?>
                                            </optgroup>    
                                        </select>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <div align="center">
                                        <h2>Y/O</h2>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <h5>* Nivel(es)</h5>
                                    <div class="body">
                                        <div id='select-all3' class="btn btn-primary btn-block waves-effect" >Todos los Niveles</div>
                                        <div id='deselect-all3' class="btn btn-primary btn-block waves-effect" >Quitar Niveles</div>
                                        <select id="lvl" name="lvl[]" class="ms"  multiple="multiple">
                                            <optgroup label="Elige uno o varios Niveles">
                                                <?php echo $this->selectedlvl($id); ?>
                                            </optgroup>    
                                        </select>
                                    </div>
                                </div>
                            <!-- End Accordion content -->
                        </div>
                    </div>
                </div>
                <br/>
                <button class='btn btn-block btn-lg btn_color waves-effect' id='' onclick="sendform();">Editar</button>
              </div>
            </form>    
        <?php
      }//end while
    }//end function gridedit to promos

}
?>
