<?php
/*By Allan*/
include_once '../../inc/parametros.php';
include_once '../../model/administracion/m_admin_sucursales.php';
class C_admin_sucursales{
    private $ser;
    private $usu;
    private $pas;
    private $bd;
    

  public function __construct($ser,$usu,$pas,$bd) {
    $this->ser=$ser;
    $this->usu=$usu;
    $this->pas=$pas;
    $this->bd=$bd;
    $this->model=$model=new M_admin_sucursales();
  }


  public function addsuc($suc,$address,$phone,$latt,$longi,$hour){
    $res=$this->model->addsuc($this->ser,$this->usu,$this->pas,$this->bd,$suc,$address,$phone,$latt,$longi,$hour);
    echo 1;
  }

  public function revsuc(){
    $res=$this->model->revsuc($this->ser,$this->usu,$this->pas,$this->bd);
    $reg=mysqli_fetch_assoc($res);
    $numsuc = $reg['nsucursales'];

    $res2=$this->model->sucsub($this->ser,$this->usu,$this->pas,$this->bd);
    $reg2=mysqli_fetch_assoc($res2);
    $subidas = $reg2['subidas'];

    if($subidas<$numsuc){
      return 1;
    }else{
      return 0;
    }
    //echo $numsuc.'---'.$subidas;
  }

  public function editsuc($id,$suc,$address,$phone,$latt,$longi,$hour){
    $res=$this->model->editsuc($this->ser,$this->usu,$this->pas,$this->bd,$id,$suc,$address,$phone,$latt,$longi,$hour);
    echo 1;
  }

  //deletes
  public function delrow($id,$table,$field,$response){
    $res=$this->model->delrow($this->ser,$this->usu,$this->pas,$this->bd,$id,$table,$field);
    echo $response;
  }

  //edit coupons info
  public function gridedits($option){
    $res=$this->model->gridedits($this->ser,$this->usu,$this->pas,$this->bd);

    //while($reg=mysqli_fetch_assoc($res)){
      //$arr = $reg[''];
    //}

    if($option=='json'){
        echo $json;
    }elseif($option=='tr'){
      while($reg=mysqli_fetch_assoc($res) ){
        $id = $reg['id_sucursales'];
        $name = $reg['nombre'];
        $address = $reg['Direccion'];
        $lat = $reg['latitud'];
        $long = $reg['longitud'];
        $phone = $reg['tel_sucursal'];
        $times = $reg['horario'];

        $to = substr($times, 5,1); 
        $open = substr($times, 0,5);
        $open = $open.' '.$to;

        $tc = substr($times, 15,1);
        $close = substr($times, 10,5);
        $close = $close.' '.$tc;

        $active = $reg['activo'];

        if($active==0){
          $ischeck='checked="true"';
        }elseif($active==1){
          $ischeck="";
        }
        
          ?>
              <tr>
                  <td class='bor_der text-center'><?php echo $id;?></td>
                  <td class='bor_der text-center'><input class="form-control" type="text" id="n<?php echo $id;?>" value="<?php echo utf8_encode($name);?>"></td>
                  <td class='bor_der text-center'><input class="form-control" type="text" id="a<?php echo $id;?>" value="<?php echo utf8_encode($address);?>"></td>
                  <td class='bor_der text-center'><input class="form-control" type="text" id="l<?php echo $id;?>" value="<?php echo $lat ;?>"></td>
                  <td class='bor_der text-center'><input class="form-control" type="text" id="g<?php echo $id;?>" value="<?php echo $long ;?>"></td>
                  <td class='bor_der text-center'><input class="form-control" type="text" id="p<?php echo $id;?>" value="<?php echo $phone ;?>"></td>
                  <td class='bor_der text-center'>
                    <div class="demo-masked-input">
                        <div class="input-group no-margin">
                            <div class="form-line">
                                <input id="o<?php echo $id;?>" type="text" class="mask form-control time12" value="<?php echo $open; ?>" >
                            </div>
                        </div>
                    </div>  
                  </td>
                  <td class='bor_der text-center'>
                    <div class="demo-masked-input">
                        <div class="input-group no-margin">
                            <div class="form-line">
                                <input id="c<?php echo $id;?>" type="text" class="mask form-control time12" value="<?php echo $close; ?>" >
                            </div>
                        </div>
                    </div>  
                  </td>
                  <td class='bor_der text-center'><button class='btn btn-info' id='<?php echo $id;?>' onclick='editsuc(this.id)'>Editar</button></td>
                  <td>
                    <div class="switch">
                        <label><input id="x<?php echo $id;?>" class="ifcheck" type="checkbox"<?php echo $ischeck; ?>"><span class="lever switch-col-blue"></span></label>
                    </div>
                  </td>
                  <td class='bor_der text-center'><button class='btn btn-danger' id='<?php echo $id;?>' name="<?php echo utf8_encode($name);?>" onclick="delrow(this.id,'tbl_sucursales','id_sucursales',4);"><i class="fas fa-trash-alt"></i></button></td>
              </tr> 
          <?php
        }//end while
      }//end elseif
    }//end function gridedit to coupons

}
?>