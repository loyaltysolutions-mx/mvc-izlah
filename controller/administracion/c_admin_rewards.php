<?php
/*By Allan*/
include_once '../../inc/parametros.php';
include_once '../../model/administracion/m_admin_rewards.php';

include_once '../../inc/vendor/excel/PHPExcel.php';
//require '../../inc/vendor/excel/PHPExcel/IOFactory.php';


class C_admin_rerwards{
    private $ser;
    private $usu;
    private $pas;
    private $bd;
    

  public function __construct($ser,$usu,$pas,$bd) {
    $this->ser=$ser;
    $this->usu=$usu;
    $this->pas=$pas;
    $this->bd=$bd;
    $this->model=$model=new M_admin_rewards();
  }

  //select
  public function projectype(){
    $res=$this->model->projectype($this->ser,$this->usu,$this->pas,$this->bd);

    while($reg=mysqli_fetch_assoc($res) ){
      $type = $reg['id_tipo_proyecto'];
    }
    return $type;
  }

  public function uploadFileRecompensa($file){

    //print_r($file);
    //die();

    if(isset($file['file_reward'])){
      $errors= array();
      $file = $file['file_reward'];
      $path = "../../inc/xls/";

      $file_size = $file['size'];
      $file_tmp = $file['tmp_name'];
      $file_type= $file['type'];
      $file_ext=strtolower(end(explode('.',$file['name'])));
      $file_name = "upload".date("Ymd_His").".".$file_ext;
      $routeFile = $path.$file_name;
      $extensions= array("xls","xlsx");
      
      if(in_array($file_ext,$extensions)=== false){
         $errors[]="Extension no válida, por favor elige un archivo con extension .xls o .xlsx";
      }

      //print_r($errors);      
      
      if(empty($errors)==true){
        echo 'Entro';
        if(move_uploaded_file($file_tmp,$routeFile)){
          echo 'Move';
          include '../../inc/vendor/excel/PHPExcel/IOFactory.php';
          $inputFileName = $routeFile;
          try {
            $objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
          } catch(PHPExcel_Reader_Exception $e) {
            die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
          }
          //die("Entro");

          $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
          print_r($sheetData);

          $drawing1 = $objPHPExcel->getActiveSheet()->getDrawingCollection();
          foreach ($sheetData as $key => $value) {
            if(isset($drawing1[$key-1])){
              
              $drawing = $drawing1[$key-1];
              if ($drawing instanceof PHPExcel_Worksheet_MemoryDrawing){
                  ob_start();
                  call_user_func(
                      $drawing->getRenderingFunction(),
                      $drawing->getImageResource()
                  );

                  $imageContents = ob_get_contents();
                  ob_end_clean();
                  $extension = 'png';
              }else{
                  $zipReader = fopen($drawing->getPath(),'r');
                  $imageContents = '';

                  while (!feof($zipReader)){
                      $imageContents .= fread($zipReader,1024);
                  }
                  fclose($zipReader);
                  $extension = $drawing->getExtension();
              }
              //die("exist");
              $pathImg = '../../inc/imagenes/img_catalogo/';
              $myFileName = 'img_'.uniqid().'.'.$extension;
              //echo "Name:".$drawing->getName();
              //echo "Description:".$drawing->getDescription();
              file_put_contents($pathImg.$myFileName,$imageContents);
            }else{
              $myFileName = "no_image.png";              
            }

            $tipoProyecto = $this->projectype();
            $nombre = $value['A'];
            $detalle = $value['B'];            
            $visitas = $tipoProyecto == 3 ? $value['C'] : 0;
            $puntos = $tipoProyecto != 3 ?  $value['C'] : 0;
            $cantidad = $value['D'];
            $imagen = $myFileName;

            print_r($value);

            $result = $this->model->insertRecompensa($this->ser,$this->usu,$this->pas,$this->bd,$nombre, $detalle, $tipoProyecto, $puntos, $visitas, $cantidad, $imagen, 0);
          }
          
          die("Result: ".$result);
        }
            
      }else{
         print_r($errors);
      }      
   }

    
    //die();
    
  }

  public function updaterw($id,$name,$pts,$visits,$details,$cant){
    $res=$this->model->updaterw($this->ser,$this->usu,$this->pas,$this->bd,$id,$name,$pts,$visits,$details,$cant);
    if($res){
          $reponse = 1;
          echo $reponse;
      }
  }

  public function updateimg($id,$file){
    $res=$this->model->updateimg($this->ser,$this->usu,$this->pas,$this->bd,$id,$file);
  }

  public function deleterw($id){
      $res=$this->model->deleterw($this->ser,$this->usu,$this->pas,$this->bd,$id);
      if($res){
          $reponse = 1;
          echo $reponse;
      }
  }

  //edit configuration products
  public function gridedit($option,$projectype){
      $res=$this->model->gridedit($this->ser,$this->usu,$this->pas,$this->bd);

    //while($reg=mysqli_fetch_assoc($res)){
      //$arr = $reg[''];
    //}

      if($option=='json'){
          echo $json;
      }elseif($option=='tr'){
          while($reg=mysqli_fetch_assoc($res) ){
            $id = $reg['id_cat_premios_productos_servicios'];
            $nombre = $reg['nombre'];
            $puntos = $reg['valor_puntos'];
            $visitas = $reg['valor_visitas'];
            $img = $reg['imagen'];
            $detalle = $reg['detalle'];

            $active = $reg['activo'];
            $cant = $reg['cantidad'];

            if($active==0){
              $ischeck='checked="true"';
            }elseif($active==1){
              $ischeck="";
            }
              ?>
                  <tr>
                      <td class='bor_der text-center'><?php echo $id;?></td>
                      <td class='bor_der text-center'><input class="form-control" type="text" id="n<?php echo $id;?>" value="<?php echo utf8_encode($nombre);?>"></td>
                      <?php
                        if($projectype==1 OR $projectype==2){ 
                      ?>
                      <td class='bor_der text-center'><input class="form-control" type="number" id="p<?php echo $id;?>" value="<?php echo ($puntos);?>"></td>
                      <?php
                        }elseif($projectype==3){
                      ?>
                      <td class='bor_der text-center'><input class="form-control" type="number" id="v<?php echo $id;?>" value="<?php echo ($visitas);?>"></td>
                      <?php
                        }
                      ?>
                      <td class='bor_der text-center row'><button class='mod-al btn btn-info' id='i<?php echo $id;?>' onclick="cleanfile('<?php echo $id;?>')" data-toggle="modal" data-target="#editimg">Editar Img</button></td>
                      <td class='bor_der text-center'><input class="form-control" type="text" id="d<?php echo $id;?>" value="<?php echo utf8_encode($detalle);?>"></td>
                      <td class='bor_der text-center'><input class="form-control" type="number" id="c<?php echo $id;?>" value="<?php echo utf8_encode($cant);?>"></td>
                      <td>
                        <div class="switch">
                            <label><input id="x<?php echo $id;?>" class="ifcheck" type="checkbox" <?php echo $ischeck; ?>><span class="lever switch-col-blue"></span></label>
                        </div>
                      </td>
                      <td class='bor_der text-center'>
                        <button class='edit' id='<?php echo $id;?>' onclick='editrw(this.id)'><i class="fas fa-pen"></i></button>
                        <button class='delete' id='<?php echo $id;?>' name="<?php echo utf8_encode($nombre);?>" onclick='deleterw(this.id)'><i class="fas fa-trash-alt"></i></button>
                      </td>
                      
                  </tr> 
              <?php
          }
      }
  }//end function gridedit

}
?>
