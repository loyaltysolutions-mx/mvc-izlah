<?php
/*By Allan*/
include_once '../../inc/parametros.php';
include_once '../../model/administracion/m_admin_nivel.php';
class C_admin_nivel{
    private $ser;
    private $usu;
    private $pas;
    private $bd;
    

  public function __construct($ser,$usu,$pas,$bd) {
    $this->ser=$ser;
    $this->usu=$usu;
    $this->pas=$pas;
    $this->bd=$bd;
    $this->model=$model=new M_admin_nivel();
  }

  //function to add levels
  public function addlvl($level,$type,$ini,$fin){
    $res=$this->model->addlvl($this->ser,$this->usu,$this->pas,$this->bd,$level,$type,$ini,$fin);
    echo 1;
  }

  public function editlvl($id,$name,$type,$ini,$fin){
    $res=$this->model->editlvl($this->ser,$this->usu,$this->pas,$this->bd,$id,$name,$type,$ini,$fin);
    echo 1;
  }

  //deletes
  public function delrow($id,$table,$field,$response){
    $res=$this->model->delrow($this->ser,$this->usu,$this->pas,$this->bd,$id,$table,$field);
    echo $response;
  }

  //select project type to lvl
  public function selectype(){
    $res=$this->model->selectype($this->ser,$this->usu,$this->pas,$this->bd);

    while($reg=mysqli_fetch_assoc($res) ){
            $id = $reg['id_tipo_proyecto'];
            $nombre = $reg['nombre'];
            
            ?>
                <option value="<?php echo $id; ?>" selected><?php echo utf8_encode($nombre); ?></option>
            <?php
          }
  }

  //min range to set (constant and logic lvl)
  public function minrange(){
    $res=$this->model->minrange($this->ser,$this->usu,$this->pas,$this->bd);

    while($reg=mysqli_fetch_assoc($res) ){
      $minrange = $reg['minrange'];
    }
    echo $minrange;
  }

  //to verify if exist an user in this level before to delete
  public function verify($idlvl){
    $res=$this->model->verify($this->ser,$this->usu,$this->pas,$this->bd,$idlvl);

    while($reg=mysqli_fetch_assoc($res)){
      $count = $reg['verify'];
    }

    return $count;
    
  }

  //edit levels info
  public function grideditl($option){
    $res=$this->model->grideditl($this->ser,$this->usu,$this->pas,$this->bd);

    //while($reg=mysqli_fetch_assoc($res)){
      //$arr = $reg[''];
    //}
    $sum = mysqli_num_rows($res);

    if($option=='json'){
        echo $json;
    }elseif($option=='tr'){
      while($reg=mysqli_fetch_assoc($res) ){
        $inc++;
        $id = $reg['id_nivel'];
        $name = $reg['nombre'];
        $type = $reg['tipo'];
        $ini = $reg['ini'];
        $fin = $reg['fin'];

        $active = $reg['activo'];

        if($active==0){
          $ischeck='checked="true"';
        }elseif($active==1){
          $ischeck="";
        }
        ?>
        <div style="">
          <?php $numreg = $this->verify($id); ?>
        </div>

        
        <?php
        $numreg = $numreg*1;
        if($numreg>=1){
          $disabled = 'disabled';
        }elseif($numreg==0){
          $disabled = '';
        }
        
          ?>  
              <tr>  
                  <td class='bor_der text-center'><?php echo $id;?></td>
                  <td class='bor_der text-center'>
                    <input class="form-control" type="text" id="n<?php echo $id;?>" value="<?php echo utf8_encode($name);?>">
                    <input class="form-control" type="hidden" id="t<?php echo $id;?>" value="<?php echo utf8_encode($type);?>">
                  </td>
                  <!--td class='bor_der text-center'>
                    <!--select id="t<?php //echo $id;?>" name="type" onchange="" class="form-control show-tick" style="<?php //echo $style; ?>">
                        <option value="">Elige un Tipo</option>
                        <?php 
                          //if($type==1){
                        ?>
                          <!--option value="1" selected>Puntos</option>
                          <option value="2">Visitas</option>
                        <?php    
                          //}elseif($type==2){
                        ?>
                          <!--option value="1">Puntos</option>
                          <option value="2" selected>Visitas</option>  
                        <?php    
                          //}
                        ?>
                        
                      <!--/select-->
                  <!--/td-->
                  <td class='bor_der text-center'><input class="form-control" type="text" id="i<?php echo $id;?>" value="<?php echo utf8_encode($ini);?>"></td>
                  <td class='bor_der text-center'><input class="form-control" type="text" id="f<?php echo $id;?>" value="<?php echo utf8_encode($fin);?>"></td>
                  <td class='bor_der text-center'><button class='btn btn-info' id='<?php echo $id;?>' onclick='editlvl(this.id)'>Editar</button></td>
                  <td>
                    <div class="switch">
                        <label><input id="x<?php echo $id;?>" class="ifcheck" type="checkbox" <?php echo $ischeck; ?>><span class="lever switch-col-blue"></span></label>
                    </div>
                  </td>
                  <td class='bor_der text-center'>
                    <?php 
                      if($inc==$sum){
                    ?>
                    <button class='btn btn-danger' id='<?php echo $id;?>' name="<?php echo utf8_encode($name);?>" onclick="delrow(this.id,'tbl_niveles','id_nivel',1);" <?php echo $disabled; ?> ><i class="fas fa-trash-alt"></i></button>
                    <?php 
                      }else{

                      }
                    ?>
                  </td>
              </tr> 
          <?php
        }//end while
      }//end elseif
    }//end function gridedit to levels

}
?>