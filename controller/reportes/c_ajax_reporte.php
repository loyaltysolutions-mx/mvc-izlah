<?php
include_once '../../inc/parametros.php';
include_once '../../controller/reportes/c_reporte.php';
include_once '../../controller/reportes/c_reporte_niveles.php';
$instanceReport=new C_reporte($ser,$usu,$pas,$bd);
$instanceNivel = new C_reporte_niveles($ser,$usu,$pas,$bd);
if(isset($_POST)){        
    $op=trim($_POST['action']);
    switch($op){        
        case 2:
            include_once '../../controller/reportes/c_reporte_clientes.php';
            $instanceReport=new C_reporte_clientes($ser,$usu,$pas,$bd);
            $result = $instanceReport->getReporteClientes($_POST);            
            echo json_encode($result);
            die();
        break;
        case 3:
            include_once '../../controller/reportes/c_reporte_clientes.php';
            $instanceReport=new C_reporte_clientes($ser,$usu,$pas,$bd);
        	$result = $instanceReport->insertSegmento($_POST);            
            echo json_encode($result);
            die();
        break;        
        case 5:
            $result = $instanceReport->getReportePV1($_POST);
?>
            <script type="text/javascript">
                $(".table_rp").DataTable({
                    dom: 'Bfrtlip',
                    responsive: true,
                    buttons: [
                        'copy',
                        { extend: 'csvHtml5'},
                        { extend: 'excelHtml5'},
                        { extend: 'pdfHtml5'},
                        'print'
                    ]
                });
            </script>
<?php
            //echo json_encode($result);
            die();
        break;
        case 6:
            $result = $instanceReport->getReporteHistorialClientes($_POST);
            echo json_encode($result);
            die();
        break;
        case 7:                        
            $result = $instanceReport->getReportePromociones($_POST);
            echo json_encode($result);
            die();
        break;
        case 8:            
            $result = $instanceNivel->getReporteNiveles($_POST);
            echo json_encode($result);
            die();
        break;
        case 9:
            include_once '../../controller/reportes/c_reporte_transferencia_puntos.php';
            $instanceReport = new C_reporte_transferencia_puntos($ser,$usu,$pas,$bd);
            $result = $instanceReport->getReporteTransferenciaPuntos($_POST);
            echo json_encode($result);
            die();
        break;
        case 10:
            include_once '../../controller/reportes/c_reporte_segmentos.php';
            $instanceReport = new C_reporte_segmentos($ser,$usu,$pas,$bd);
            $result = $instanceReport->getReporteSegmentos($_POST);
            echo json_encode($result);
            die();
        break;
        default:
            echo 'Ocurrio algun error :(';
        break;
    }
}

?>

