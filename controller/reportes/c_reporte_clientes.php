<?php

include_once '../../model/reportes/m_reporte_clientes.php';
include_once '../../model/administracion/m_admin_segmento.php';
class C_reporte_clientes{
  private $ser;
  private $usu;
  private $pas;
  private $bd;    

  function __construct($ser,$usu,$pas,$bd) {
    $this->ser=$ser;
    $this->usu=$usu;
    $this->pas=$pas;
    $this->bd=$bd;
    $this->model = new M_reporte_clientes($ser, $usu, $pas, $bd);
    $this->segmento_model = new M_admin_segmento($ser, $usu, $pas, $bd);
  }

  function getFiltros(){    
    $segmentos = $this->model->getSegmentos(NULL, NULL);    
    $niveles = $this->model->getNiveles(NULL, NULL);
    
    
    $result = array(
      'segmentos' => $segmentos,
      'niveles' => $niveles      
    );
    return $result;
  }

  function getReporteClientes($datos = NULL){
    if($datos != NULL){

      $fecha = date("Y-m-d");

      if(isset($datos['segmento'])){
        $idSegmento = NULL;
        foreach ($datos['segmento'] as $value) {          
          $idSegmento.= $idSegmento != NULL ? ' OR B.id_segmento ='.$value : $value;
        }        
      }else{
        $idSegmento = NULL;
      }
          
      if(isset($datos['nivel'])){
        $idNivel = NULL;
        foreach ($datos['nivel'] as $value) {          
          $idNivel.= $idNivel != NULL ? ' OR A.id_nivel ='.$value : $value;
        }        
      }else{
        $idNivel = NULL;
      }

      if(isset($datos['fechaRegistro'])){
        $desde = $datos['fechaRegistro']['desde'];
        $hasta = $datos['fechaRegistro']['hasta'];
        $fechaRegistro = $desde."' AND '".$hasta."'";        
      }else{
        $fechaRegistro = NULL;
      }

      if(isset($datos['edad'])){
        $fechaMin = (int)$datos['edad']['min'];
        $fechaMax = (int)$datos['edad']['max'];
        $edad = " BETWEEN SUBDATE(NOW(), INTERVAL $fechaMax YEAR) AND SUBDATE(NOW(), INTERVAL $fechaMin YEAR) ";
      }else{
        $edad = NULL;
      }

      if( isset($datos['genero']) ){
        $genero = $datos['genero'];        
      }else{
        $genero =  NULL;
      }

      if( isset($datos['status']) ){
        $status = $datos['status'];        
      }else{
        $status =  NULL;
      }
      
      $result = $this->model->getReporteClientes(NULL, $idSegmento, $fechaRegistro, $edad, $genero, $status, $idNivel);
      $resultQuery = array();
      foreach ($result as $key) {
        $data = $this->model->getUsers($key['id_usuario']);
        unset($data[0]['password']);        
        $data[0]['segmentos'] = $this->model->getSegmentos(NULL, $key['id_usuario']);
        $data[0]['tarjetas'] = $this->model->getTarjetas($key['id_usuario']);
        $promedio = $this->model->getPromedioMontoTicket($key['id_usuario']);
        $data[0]['promedio'] = $promedio[0]['promedio'];
        array_push($resultQuery, $data);
      }

      //print_r($resultQuery);
      //die();
 
      return array('result' => $resultQuery);
    }
    return array();

  }  

  function insertSegmento($data = NULL){

    if($data != NULL && isset($data['idSegmento'])){
      $idSegmento = $data['idSegmento'];
      $status = TRUE;
      foreach ($data['rows'] as $key => $value){
        $existeCliente = $this->model->getClientsSegmento1($idSegmento, $value);
        if(count($existeCliente) == 0 ){
          $resultInsert = $this->segmento_model->addClientsSegmento($value, $idSegmento);
          if($resultInsert != 1){
            $status = FALSE;
            break;
          }  
        }        
      }
      return (array('result' => $status ? 1 : 2));      
    }else{
      return false;
    }
  }
  
}
