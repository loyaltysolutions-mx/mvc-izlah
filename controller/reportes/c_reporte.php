<?php

include_once '../../model/reportes/m_reporte.php';
include_once '../../model/administracion/m_admin_segmento.php';
class C_reporte{
  private $ser;
  private $usu;
  private $pas;
  private $bd;    

  function __construct($ser,$usu,$pas,$bd) {
    $this->ser=$ser;
    $this->usu=$usu;
    $this->pas=$pas;
    $this->bd=$bd;
    $this->model = new M_reporte($ser, $usu, $pas, $bd);
    $this->segmento_model = new M_admin_segmento($ser, $usu, $pas, $bd);
  }

  function getFiltros($tipoReporte = NULL){

    if($tipoReporte != NULL){
      switch ($tipoReporte) {
        case 1:
          $segmentos = $this->model->getSegmentos(NULL);
          $arraySegmentos = array();      
          while ($fila = mysqli_fetch_assoc($segmentos)) {
            array_push($arraySegmentos,array('idSegmento' => utf8_encode($fila['id_segmento']),'nombreSegmento' => utf8_encode($fila['nombre_segmento']),'descripcionSegmento' => utf8_encode($fila['descripcion_segmento'])));
          }
          $result = array(      
            'segmentos' => $arraySegmentos,      
          );
          return $result;  
        break;        
        case 2:
          $tipos = $this->model->getTipoPV(NULL);
          $puntosVenta = $this->model->getPuntosVenta(NULL);
          $segmentos = $this->model->getSegmentos(NULL);
          $tipoProyecto = $this->model->getConfigProyecto();
          
          $arraySegmentos = array();
          while ($fila = mysqli_fetch_assoc($segmentos)) {
            array_push($arraySegmentos,array('idSegmento' => $fila['id_segmento'],'nombreSegmento' => utf8_encode($fila['nombre_segmento']),'descripcionSegmento' => utf8_encode($fila['descripcion_segmento'])));
          }
          $result = array(      
            'tipos' => $tipos,
            'puntosVenta' => $puntosVenta,
            'segmentos' => $arraySegmentos,
            'tipoProyecto' => (isset($tipoProyecto) && count($tipoProyecto) > 0 ? $tipoProyecto[0]['id_tipo_proyecto'] : 1)
          );
          return $result;
        break;
        case 3:
          $sucursales = $this->model->getSucursales(NULL);
          $segmentos = $this->model->getSegmentos(NULL);
          $niveles = $this->model->getNivel(NULL);
          $arraySegmentos = array();
          while ($fila = mysqli_fetch_assoc($segmentos)) {
            array_push($arraySegmentos,array('idSegmento' => utf8_encode($fila['id_segmento']),'nombreSegmento' => utf8_encode($fila['nombre_segmento']),'descripcionSegmento' => utf8_encode($fila['descripcion_segmento'])));
          }
          $result = array(      
            'sucursales' => $sucursales,
            'segmentos' => $arraySegmentos,
            'niveles' => $niveles
          );
          return $result;
        break;
        case 4:
          //die("Die");
          $segmentos = $this->model->getSegmentos(NULL);
          $arraySegmentos = array();      
          while ($fila = mysqli_fetch_assoc($segmentos)) {
            array_push($arraySegmentos,array('idSegmento' => $fila['id_segmento'],'nombreSegmento' => $fila['nombre_segmento'],'descripcionSegmento' => $fila['descripcion_segmento']));
          }
          $users = $this->model->getUsers(NULL);
          $niveles = $this->model->getNivel(NULL);
          $result = array(      
            'segmentos' => $arraySegmentos,
            'usuarios' => $users,
            'niveles' => $niveles
          );
          return $result;
        break;
      }
    
    }else{
      return false;
    }

  }

  function getReporteClientes($datos = NULL){
    if($datos != NULL){

      $fecha = date("Y-m-d");
     
      if( isset($datos['segmento']) ){
        $idSegmento = $datos['segmento'] != "" ? $datos['segmento'] : NULL;       
      }else{
        $idSegmento =  NULL;
      }

      if(isset($datos['fechaRegistro'])){
        $desde = $datos['fechaRegistro']['desde'];
        $hasta = $datos['fechaRegistro']['hasta'];
        $fechaRegistro = $desde."' AND '".$hasta."'";        
      }else{
        $fechaRegistro = NULL;
      }

      if(isset($datos['edad'])){
        $fechaMin = (int)date("Y") - $datos['edad']['min'];
        $fechaMax = (int)date("Y") - $datos['edad']['max'];
        $edad = "'".date($fechaMax."-01-01")."' AND '".date($fechaMin."-12-31")."'";        
      }else{
        $edad = NULL;
      }

      if( isset($datos['genero']) ){
        $genero = $datos['genero'];        
      }else{
        $genero =  NULL;
      }

      if( isset($datos['status']) ){
        $status = $datos['status'];        
      }else{
        $status =  NULL;
      }
      
      $result = $this->model->getReporteClientes($idSegmento, $fechaRegistro, $edad, $genero, $status);
 
      return array('result' => $result);
    }
    return array();

  }

  function getReportePV($datos = NULL){
    if($datos != NULL){
      
      $columns = array(
        array('title' => 'ID Usuario'),
        array('title' => 'Nombre Cliente'),
        array('title' => 'Email'),
        array('title' => 'Punto de Venta')
      );      
      
      if(isset($datos['idPuntoVenta'])){
        $idPuntoVenta = NULL;
        foreach ($datos['idPuntoVenta'] as $value) {          
          $idPuntoVenta.= $idPuntoVenta != NULL ? ' OR A.id_usuario_registro ='.$value : $value;
        }
        //$idPuntoVenta = $datos['idPuntoVenta'] == "" ? NULL : $datos['idPuntoVenta'];
        array_push($columns, array('title' => 'Nombre del Punto Venta'));
      }else{
        $idPuntoVenta = NULL;
      }

      if(isset($datos['tipoPV'])){
        $tipo = NULL;
        foreach ($datos['tipoPV'] as $value) {          
          $tipo.= $tipo != NULL ? ' OR D.id_tipo_punto_venta ="'.$value.'"' : $value.'"';
        }
        $tipo = $datos['tipoPV'] == "" ? NULL : $datos['tipoPV'];
        array_push($columns, array('title' => 'Tipo del PV'));
      }else{
        $tipo = NULL;
      }

      if(isset($datos['fechaRegistro'])){
        $desde = $datos['fechaRegistro']['desde'];
        $hasta = $datos['fechaRegistro']['hasta'];
        $fechaRegistro = $desde." 00:00:00' AND '".$hasta." 23:59:59'";
        $object['title'] = 'Fecha Registro';
        array_push($columns, $object );
      }else{
        $fechaRegistro = NULL;
      }

      if(isset($datos['diasSemana'])){
        $diasSemana = NULL;

        foreach ($datos['diasSemana'] as $value) {          
          $diasSemana.= $diasSemana != NULL ? ' OR A.dia_registro ="'.$value.'"' : $value.'"';
        }
        $object['title'] = 'Dia Semana';
        array_push($columns, $object );
      }else{
        $diasSemana = NULL;
      }

      if( isset($datos['ticketPromedio']) ){
        $ticketPromedio = $datos['ticketPromedio']['min']." AND ".$datos['ticketPromedio']['max'];
        array_push($columns, array('title' => 'Ticket Promedio'));
      }else{
        $ticketPromedio =  NULL;
      }

      if( isset($datos['puntos']) ){
        $puntos = $datos['puntos']['min']." AND ".$datos['puntos']['max'];
        //die($puntos);
        array_push($columns, array( 'title' => 'Puntos') );
      }else{
        $puntos =  NULL;
      }

      if( isset($datos['visitas']) ){
        $visitas = $datos['visitas']['min']." AND ".$datos['visitas']['max'];
        //die($puntos);
        array_push($columns, array( 'title' => 'Visitas') );
      }else{
        $visitas =  NULL;
      }
      
      $result = $this->model->getReporteActividadesPV($idPuntoVenta, $tipo, $fechaRegistro, $diasSemana, $ticketPromedio, $puntos, $visitas);

      return array('result' => $result, 'columns' => $columns);
    }
    return array();
  }

  function getReportePV1($datos = NULL){
    if($datos != NULL){
    
      if(isset($datos['tipoPV'])){
        $tipo = NULL;
        foreach ($datos['tipoPV'] as $value) {          
          $tipo.= $tipo != NULL ? ' OR D.id_tipo_punto_venta ="'.$value.'"' : $value.'"';
        }
        $tipo = $datos['tipoPV'] == "" ? NULL : $datos['tipoPV'];
        array_push($columns, array('title' => 'Tipo del PV'));
      }else{
        $tipo = NULL;
      }

      if(isset($datos['fechaRegistro'])){
        $desde = $datos['fechaRegistro']['desde'];
        $hasta = $datos['fechaRegistro']['hasta'];
        $fechaRegistro = $desde." 00:00:00' AND '".$hasta." 23:59:59'";
        $object['title'] = 'Fecha Registro';
        array_push($columns, $object );
      }else{
        $fechaRegistro = NULL;
      }

      if(isset($datos['diasSemana'])){
        $diasSemana = NULL;

        foreach ($datos['diasSemana'] as $value) {          
          $diasSemana.= $diasSemana != NULL ? ' OR A.dia_registro ="'.$value.'"' : $value.'"';
        }
        $object['title'] = 'Dia Semana';
        array_push($columns, $object );
      }else{
        $diasSemana = NULL;
      }

      if( isset($datos['ticketPromedio']) ){
        $ticketPromedio = $datos['ticketPromedio']['min']." AND ".$datos['ticketPromedio']['max'];
        array_push($columns, array('title' => 'Ticket Promedio'));
      }else{
        $ticketPromedio =  NULL;
      }

      if( isset($datos['puntos']) ){
        $puntos = $datos['puntos']['min']." AND ".$datos['puntos']['max'];
        //die($puntos);
        array_push($columns, array( 'title' => 'Puntos') );
      }else{
        $puntos =  NULL;
      }

      if( isset($datos['visitas']) ){
        $visitas = $datos['visitas']['min']." AND ".$datos['visitas']['max'];
        //die($puntos);
        array_push($columns, array( 'title' => 'Visitas') );
      }else{
        $visitas =  NULL;
      }

      $idPuntoVenta = NULL;

      if(isset($datos['idPuntoVenta']) && !empty($datos['idPuntoVenta'])){
        //echo 'Entro';
        //print_r($datos['idPuntoVenta']);
        foreach ($datos['idPuntoVenta'] as $pos) {          
          //echo $pos;
          $result = $this->model->getReporteActividadesPV($pos, $tipo, $fechaRegistro, $diasSemana, $ticketPromedio, $puntos, $visitas); 
          //print_r($result);
?>
          <div id="divTable" class="card">
            <div class="header clearfix">
                <div class="pull-left"><h2>Tabla de Resultados</h2></div>
                <!--div class="pull-right"><a id="btnCreateSegment" href="javascript:;" class="btn btn-primary">Crear segmento de la tabla</a></div-->
            </div>
            <div class="body divTable">                
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table_rp">
                      <thead>
                        <th>ID Registro</th>                        
                        <th>Usuario</th>                        
                        <th>Correo</th>
                        <th>Monto</th>
                        <th>Ticket</th>
                        <th>Puntos</th>
                        <th>Recompensa (Redencion)</th>
                        <th>Promocion</th>
                        <th>Tipo Transacción</th>
                        <th>Punto de Venta</th>
                        <th>Fecha de Registro</th>
                      </thead>
                      <tbody>
                        <?php foreach ($result as $key) { ?>
                          <tr>
                            <td><?php echo $key['id_registro_acumulacion'] ?></td>
                            <td><?php echo $key['nombreCliente'] ?></td>
                            <td><?php echo $key['emailCliente'] ?></td>
                            <td><?php echo $key['monto_ticket'] ?></td>
                            <td><?php echo $key['ticket'] ?></td>
                            <td><b><?php echo $key['id_tipo_registro'] == 2 ? "<span style='color:red'>-".$key['puntos']."</span>" : "+".$key['puntos'] ?></b></td>
                            <td><?php echo $key['nombreProductoRedencion'] ?></td>
                            <td><?php echo $key['nombrePromo'] ?></td>
                            <td><?php echo $key['nombreTipoRegistro'] ?></td>
                            <td><?php echo $key['nombrePV'] ?></td>
                            <td><?php echo $key['fecha_registro'] ?></td>
                          </tr>
                        <?php }?>
                      </tbody>
                    </table>
                </div>
            </div>
          </div>
<?php
        }
        //$idPuntoVenta = $datos['idPuntoVenta'] == "" ? NULL : $datos['idPuntoVenta'];
        //array_push($columns, array('title' => 'Nombre del Punto Venta'));


        

      }else{
        //echo 'Else';
        $result = $this->model->getReporteActividadesPV(null, $tipo, $fechaRegistro, $diasSemana, $ticketPromedio, $puntos, $visitas);        
?>
        <div id="divTable" class="card">
            <div class="header clearfix">
                <div class="pull-left"><h2>Tabla de Resultados</h2></div>
                <!--div class="pull-right"><a id="btnCreateSegment" href="javascript:;" class="btn btn-primary">Crear segmento de la tabla</a></div-->
            </div>
            <div class="body divTable">                
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table_rp">
                      <thead>
                        <th>ID Registro</th>                        
                        <th>Usuario</th>                        
                        <th>Correo</th>
                        <th>Monto</th>
                        <th>Ticket</th>
                        <th>Puntos</th>
                        <th>Recompensa (Redencion)</th>
                        <th>Promocion</th>
                        <th>Tipo Transacción</th>
                        <th>Punto de Venta</th>
                        <th>Fecha de Registro</th>
                      </thead>
                      <tbody>
                        <?php foreach ($result as $key) { ?>
                          <tr>
                            <td><?php echo $key['id_registro_acumulacion'] ?></td>
                            <td><?php echo $key['nombreCliente'] ?></td>
                            <td><?php echo $key['emailCliente'] ?></td>
                            <td><?php echo $key['monto_ticket'] ?></td>
                            <td><?php echo $key['ticket'] ?></td>
                            <td><b><?php echo $key['id_tipo_registro'] == 2 ? "<span style='color:red'>-".$key['puntos']."</span>" : "+".$key['puntos'] ?></b></td>
                            <td><?php echo $key['nombreProductoRedencion'] ?></td>
                            <td><?php echo $key['nombrePromo'] ?></td>
                            <td><?php echo $key['nombreTipoRegistro'] ?></td>
                            <td><?php echo $key['nombrePV'] ?></td>
                            <td><?php echo $key['fecha_registro'] ?></td>
                          </tr>
                        <?php }?>
                      </tbody>
                    </table>
                </div>
            </div>
          </div>
<?php      
      }
    }   
    //return array();
  }

  function getReporteHistorialClientes($datos = NULL){
    if($datos != NULL){

      $columns = array(
        array('title' => 'ID Usuario'),
        array('title' => 'Nombre Cliente'),
        array('title' => 'Ticket'),
        array('title' => 'Monto Ticket'),
        array('title' => 'Puntos'),
        array('title' => 'Nro. Visita'),
        array('title' => 'Nombre Segmento'),
        array('title' => 'Fecha Registro'),
        array('title' => 'Dia Registro')
      );      
    
      if(isset($datos['fechaRegistro'])){
        $desde = $datos['fechaRegistro']['desde'];
        $hasta = $datos['fechaRegistro']['hasta'];
        $fechaRegistro = $desde." 00:00:00' AND '".$hasta." 23:59:59'";        
      }else{
        $fechaRegistro = NULL;
      }

      if(isset($datos['cliente'])){
        $idUsuario = NULL;
        foreach ($datos['cliente'] as $value) {          
          $idUsuario.= $idUsuario != NULL ? ' OR A.id_usuario ='.$value : $value;
        }        
      }else{
        $idUsuario = NULL;
      }

      if( isset($datos['segmento']) ){        
        $idSegmento = NULL;
        foreach ($datos['segmento'] as $value) {          
          $idSegmento.= $idSegmento != NULL ? ' OR B.id_segmento ='.$value : $value;
        }
      }else{
        $idSegmento =  NULL;
      }

      if( isset($datos['nivel']) ){        
        $idNivel = NULL;
        foreach ($datos['nivel'] as $value) {          
          $idNivel.= $idNivel != NULL ? ' OR E.id_nivel ='.$value : $value;
        }
      }else{
        $idNivel =  NULL;
      }

      $users = $this->model->getUsersHistorialClientes($idUsuario, $idSegmento, $idNivel, $fechaRegistro);
      
      $result = array();
      for($index = 0; $index < count($users); $index++) {        
        $data = $this->model->getReporteHistorialClientes($users[$index]['id_usuario'], $idSegmento, $idNivel, $fechaRegistro);
        //print_r($data);
        //die();
        for($j = 0; $j < count($data); $j++) {        
          $data[$j]['segmentos'] = $this->model->getSegmentos1(NULL, $data[$j]['id_usuario']);
        }
        array_push($result, $data);
      }

      return array('result' => $result, 'columns' => $columns);
    }
    return array();
  }

  function insertSegmento($data = NULL){
    if($data != NULL && isset($data['nombreSegmento'])){      
      
      $idSegmento = $this->segmento_model->insertSegmentoID($data['nombreSegmento'], $data['descripcion'], 1);      
      if($idSegmento > 0){
        $status = TRUE;
        foreach ($data['rows'] as $key => $value) {
          $resultInsert = $this->segmento_model->addClientsSegmento($value, $idSegmento);
          if($resultInsert != 1){
            $status = FALSE;
            break;
          }          
        }
        return $status;
      }else{
        return(array('result' => 2));
      }
    }else{
      return false;
    }
  }

  function getReportePromociones($datos = NULL){
    if($datos != NULL){

      //print_r($datos);
      //die();

      $fecha = date("Y-m-d");
     
      if( isset($datos['idSucursal']) ){
        $idSucursal = NULL;
        foreach ($datos['idSucursal'] as $value) {          
          $idSucursal.= $idSucursal != NULL ? ' OR A.id_sucursal ='.$value : $value;
        }
      }else{
        $idSucursal =  NULL;
      }      

      if( isset($datos['segmento']) ){
        $idSegmento = NULL;
        foreach ($datos['segmento'] as $value) {          
          $idSegmento.= $idSegmento != NULL ? ' OR A.id_segmento ='.$value : $value;
        }
      }else{
        $idSegmento =  NULL;
      }

      if( isset($datos['nivel']) ){
        $idNivel = NULL;
        foreach ($datos['nivel'] as $value) {          
          $idNivel.= $idNivel != NULL ? ' OR A.id_nivel ='.$value : $value;
        }
      }else{
        $idNivel =  NULL;
      }

      $promos = $this->model->getDistinctPromociones($idSucursal, $idSegmento, $idNivel);
      /*print_r($promos);
      die();*/
      foreach ($promos as $key => $value) {
        $promos[$key]['segmentos'] = $this->model->getCategoriasPromociones($value['id_promo'],1);
        $promos[$key]['niveles'] = $this->model->getCategoriasPromociones($value['id_promo'],2);
        $promos[$key]['sucursales'] = $this->model->getCategoriasPromociones($value['id_promo'],3);
      }
            
      /*print_r($promos);
      die();*/
      
      return array('result' => $promos);
    }
    return array();

  }

}
?>