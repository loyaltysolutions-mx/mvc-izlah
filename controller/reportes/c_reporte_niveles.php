<?php
include_once '../../model/reportes/m_reporte_niveles.php';

class C_reporte_niveles{
  private $ser;
  private $usu;
  private $pas;
  private $bd;    

  function __construct($ser,$usu,$pas,$bd){
    $this->ser=$ser;
    $this->usu=$usu;
    $this->pas=$pas;
    $this->bd=$bd;
    $this->model = new M_reporte_niveles($ser, $usu, $pas, $bd);    
  }

  function getFiltros(){
    $niveles = $this->model->getReporteNiveles(NULL, NULL);

    $array = array('niveles' => $niveles);
    return $array;
  }

  function getReporteNiveles($datos = NULL){

    if($datos != NULL){
      if( isset($datos['nivel']) ){
        $idNivel = NULL;
        foreach ($datos['nivel'] as $value) {          
          $idNivel.= $idNivel != NULL ? ' OR A.id_nivel = '.$value : $value;
        }        
      }else{
        $idNivel =  NULL;
      }
      
      $tipo = NULL;      
      $result = $this->model->getReporteNiveles($idNivel, $tipo);
      $index = 0;
      foreach ($result as $key) {        
        $usuarios = $this->model->getUsersLevel($key['id_nivel']);
        $result[$index]['num_usuarios'] = count($usuarios);
        $index++;
      }
      return array('result' => $result);
    }
    return array();
  }  

}