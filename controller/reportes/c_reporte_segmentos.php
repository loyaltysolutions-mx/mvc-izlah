<?php
include_once '../../model/reportes/m_admin_segmento.php';

class C_reporte_segmentos{
  private $ser;
  private $usu;
  private $pas;
  private $bd;    

  function __construct($ser,$usu,$pas,$bd) {
    $this->ser=$ser;
    $this->usu=$usu;
    $this->pas=$pas;
    $this->bd=$bd;
    $this->model = new M_admin_segmento($ser, $usu, $pas, $bd);    
  }

  
  function getReporteSegmentos($datos = NULL){
    
    if($datos != NULL){

      if(isset($datos['fechaCreacion'])){
        $desde = $datos['fechaCreacion']['desde'];
        $hasta = $datos['fechaCreacion']['hasta'];
        $fechaCreacion = $desde." 00:00:00' AND '".$hasta." 23:59:59'";
      }else{
        $fechaCreacion = NULL;
      }

      if(isset($datos['status'])){
        $status = $datos['status'];
      }else{
        $status = NULL;
      }
      
      $result = $this->model->getSegmentos(NULL, $status, $fechaCreacion);

      $auxArray = array();
      while ($fila = mysqli_fetch_assoc($result)) {
          foreach ($fila as $key => $value) {
              $fila[$key] = utf8_encode($value);
              $fila['countClients'] = $this->model->getCountSegmento($fila['id_segmento']);
          }
          array_push($auxArray, $fila);
      }
 
      return array('result' => $auxArray);
    }

    return array();

  }  
  
}
