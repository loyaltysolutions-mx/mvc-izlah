<?php
include_once '../../model/reportes/m_reporte_transferencia_puntos.php';

class C_reporte_transferencia_puntos{
  private $ser;
  private $usu;
  private $pas;
  private $bd;    

  function __construct($ser,$usu,$pas,$bd) {
    $this->ser=$ser;
    $this->usu=$usu;
    $this->pas=$pas;
    $this->bd=$bd;
    $this->model = new M_reporte_transferencia_puntos($ser, $usu, $pas, $bd);    
  }

  function getFiltros(){    
    $segmentos = $this->model->getSegmentos(NULL);    
    $niveles = $this->model->getNiveles(NULL);
    $puntosVenta = $this->model->getPuntoVenta(NULL);
    $usuarios = $this->model->getUsuarios(NULL);

    $result = array(      
      'segmentos' => $segmentos,
      'niveles' => $niveles,
      'puntos_venta' => $puntosVenta,
      'usuarios' => $usuarios
    );
    return $result;
  }

  function getReporteTransferenciaPuntos($datos = NULL){
    if($datos != NULL){

      /*if(isset($datos['usuario'])){
        $idUsuario = NULL;
        foreach ($datos['usuario'] as $value) {          
          $idUsuario.= $idUsuario != NULL ? ' OR A.id_usuario ='.$value : $value;
        }
      }else{
        $idUsuario = NULL;
      }

      if( isset($datos['idPuntoVenta']) ){
        $idPuntoVenta = NULL;
        foreach ($datos['idPuntoVenta'] as $value){
          $idPuntoVenta.= $idPuntoVenta != NULL ? ' OR A.id_usuario_registro ='.$value : $value;
        }        
      }else{
        $idPuntoVenta =  NULL;
      }

      if( isset($datos['nivel']) ){
        $idNivel = NULL;
        foreach ($datos['nivel'] as $value){
          $idNivel.= $idNivel != NULL ? ' OR B.id_nivel ='.$value : $value;
        }        
      }else{
        $idNivel =  NULL;
      }

      if( isset($datos['segmento']) ){
        $idSegmento = NULL;
        foreach ($datos['segmento'] as $value){
          $idSegmento.= $idSegmento != NULL ? ' OR C.id_segmento ='.$value : $value;
        }        
      }else{
        $idSegmento =  NULL;
      }

      $usuarios = $this->model->getDistinctUserSegmento($idUsuario, $idNivel, $idPuntoVenta, $idSegmento);

      //print_r($usuarios);
      $array = array();
      foreach ($usuarios as $key) {        
        $arrayAux = array('nombre' => $key['nombreCliente']);        
        $segmentos = $this->model->getSegmentosUser($key['id_usuario'], $idSegmento);

        $arraySegmento = array();
        foreach ($segmentos as $segmento) {
          array_push($arraySegmento, $segmento['nombre_segmento']);          
        }
        $arrayAux['segmentos'] = $arraySegmento;        
        array_push($array, $arrayAux);
      }

      print_r($array);
      die();*/   

      if(isset($datos['segmento'])){
        $idSegmento = NULL;
        foreach ($datos['segmento'] as $value) {          
          $idSegmento.= $idSegmento != NULL ? ' OR E.id_segmento ='.$value : $value;
        }
      }else{
        $idSegmento = NULL;
      }

      if(isset($datos['nivel'])){
        $idNivel = NULL;
        foreach ($datos['nivel'] as $value) {          
          $idNivel.= $idNivel != NULL ? ' OR B.id_nivel ='.$value : $value;
        }
      }else{
        $idNivel = NULL;
      }

      /*if( isset($datos['segmento']) ){
        $idSegmento = $datos['segmento'] != "" ? $datos['segmento'] : NULL;       
      }else{
        $idSegmento =  NULL;
      }

      if( isset($datos['nivel']) ){
        $idNivel = $datos['nivel'] != "" ? $datos['nivel'] : NULL;
      }else{
        $idNivel =  NULL;
      }

      if( isset($datos['idPuntoVenta']) ){
        $idPuntoVenta = $datos['idPuntoVenta'] != "" ? $datos['idPuntoVenta'] : NULL;
      }else{
        $idPuntoVenta =  NULL;
      }*/

      if(isset($datos['idPuntoVenta'])){
        $idPuntoVenta = NULL;
        foreach ($datos['idPuntoVenta'] as $value) {          
          $idPuntoVenta.= $idPuntoVenta != NULL ? ' OR A.id_usuario_registro ='.$value : $value;
        }
      }else{
        $idPuntoVenta = NULL;
      }

      if(isset($datos['idUsuario'])){
        $idUsuario = NULL;
        foreach ($datos['idUsuario'] as $value){
          $idUsuario.= $idUsuario != NULL ? ' OR A.id_usuario_origen ='.$value : $value;
        }
      }else{
        $idUsuario = NULL;
      }      

      if(isset($datos['fechaRegistro'])){
        $desde = $datos['fechaRegistro']['desde'];
        $hasta = $datos['fechaRegistro']['hasta'];
        $fechaRegistro = $desde." 00:00:00' AND '".$hasta." 23:59:59'";
      }else{
        $fechaRegistro = NULL;
      }
      
      $result = $this->model->getReporte(NULL, $idSegmento, $idNivel, $idPuntoVenta, $fechaRegistro, $idUsuario);
 
      return array('result' => $result);
    }
    return array();

  }  
  
}
