-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: plataforma_lealtad
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.21-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tbl_area`
--

DROP TABLE IF EXISTS `tbl_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_area` (
  `id_area` int(3) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `icono` varchar(255) DEFAULT NULL,
  `activo` int(2) DEFAULT NULL,
  PRIMARY KEY (`id_area`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_area`
--

LOCK TABLES `tbl_area` WRITE;
/*!40000 ALTER TABLE `tbl_area` DISABLE KEYS */;
INSERT INTO `tbl_area` VALUES (1,'Administración','cogs',0),(2,'Reportes','chart-bar',0),(3,'P. Venta','award',0),(4,'Mis Registros','address-card',0);
/*!40000 ALTER TABLE `tbl_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_campos_registro`
--

DROP TABLE IF EXISTS `tbl_campos_registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_campos_registro` (
  `id_campos_registro` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_campos_registro`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_campos_registro`
--

LOCK TABLES `tbl_campos_registro` WRITE;
/*!40000 ALTER TABLE `tbl_campos_registro` DISABLE KEYS */;
INSERT INTO `tbl_campos_registro` VALUES (1,'nombre'),(2,'edad'),(3,'teléfono'),(5,'email'),(6,'rfc'),(7,'id Tarjeta');
/*!40000 ALTER TABLE `tbl_campos_registro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_cat_premios_productos_servicios`
--

DROP TABLE IF EXISTS `tbl_cat_premios_productos_servicios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_cat_premios_productos_servicios` (
  `id_cat_premios_productos_servicios` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `valor_puntos` varchar(45) DEFAULT NULL,
  `valor_visitas` int(3) DEFAULT NULL,
  `id_tipo` int(11) DEFAULT NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `detalle` varchar(255) DEFAULT NULL,
  `activo` int(11) DEFAULT '0',
  PRIMARY KEY (`id_cat_premios_productos_servicios`),
  KEY `id_tipo_idx` (`id_tipo`),
  CONSTRAINT `id_tipo` FOREIGN KEY (`id_tipo`) REFERENCES `tbl_tipo` (`id_tipo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_cat_premios_productos_servicios`
--

LOCK TABLES `tbl_cat_premios_productos_servicios` WRITE;
/*!40000 ALTER TABLE `tbl_cat_premios_productos_servicios` DISABLE KEYS */;
INSERT INTO `tbl_cat_premios_productos_servicios` VALUES (1,'Televisión 34 pulgadas','100',3,1,'tele.jpg','Tv 34 pulgadas, full HD, smart TV',0),(2,'Calculadora','1500',5,1,'calc.jpg','Detalles del producto',0),(3,'Microondas','3000',13,1,'micro.jpg','Micro ondas que calienta lo que sea ',0),(4,'Celular','5500',20,1,'cel.jpg','Celulár ultimo modelo, touch ultima generacion, smartphone',0),(5,'Laptop','2000',10,1,'lap.jpg','Laptop dell 13pulgadas HDD 1TB, 4 RAM, color negra con cargador incluido y antivirus ',0);
/*!40000 ALTER TABLE `tbl_cat_premios_productos_servicios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_configuracion_proyecto`
--

DROP TABLE IF EXISTS `tbl_configuracion_proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_configuracion_proyecto` (
  `id_configuracion_proyecto` int(11) NOT NULL AUTO_INCREMENT,
  `id_tipo_registro` int(11) DEFAULT NULL,
  `id_tipo_proyecto` int(11) DEFAULT NULL,
  `id_estilo` int(11) DEFAULT NULL,
  `bono_bienvenida` int(2) DEFAULT NULL,
  `bono_cumple` int(2) DEFAULT NULL,
  `id_tipo_comunicacion_registro` varchar(255) DEFAULT NULL,
  `id_tipo_comunicacion_acumular` varchar(255) DEFAULT NULL,
  `id_tipo_comunicacion_redimir` varchar(255) DEFAULT NULL,
  `id_tipo_comunicacion_periodica` varchar(255) DEFAULT NULL,
  `id_lapso_comunicacion_periodica` int(11) DEFAULT NULL,
  `porcentaje_margen` varchar(45) DEFAULT NULL,
  `puntos_por_peso` varchar(45) DEFAULT NULL,
  `num_min_visitas` int(11) DEFAULT NULL,
  `puntos_por_visita` varchar(45) DEFAULT NULL,
  `puntos_bienvenida` varchar(45) DEFAULT NULL,
  `puntos_cumple` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_configuracion_proyecto`),
  KEY `id_tipo_registro_idx` (`id_tipo_registro`),
  KEY `id_tipo_proyecto_idx` (`id_tipo_proyecto`),
  KEY `id_estilo_idx` (`id_estilo`),
  KEY `id_periodo_comunicacion_idx` (`id_lapso_comunicacion_periodica`),
  CONSTRAINT `id_estilo` FOREIGN KEY (`id_estilo`) REFERENCES `tbl_estilo` (`id_estilo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_periodo_comunicacion` FOREIGN KEY (`id_lapso_comunicacion_periodica`) REFERENCES `tbl_periodo_comunicacion` (`id_periodo_comunicacion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_tipo_proyecto` FOREIGN KEY (`id_tipo_proyecto`) REFERENCES `tbl_tipo_proyecto` (`id_tipo_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_tipo_registro` FOREIGN KEY (`id_tipo_registro`) REFERENCES `tbl_tipo_registro` (`id_tipo_registro`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_configuracion_proyecto`
--

LOCK TABLES `tbl_configuracion_proyecto` WRITE;
/*!40000 ALTER TABLE `tbl_configuracion_proyecto` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_configuracion_proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_estilo`
--

DROP TABLE IF EXISTS `tbl_estilo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_estilo` (
  `id_estilo` int(11) NOT NULL AUTO_INCREMENT,
  `logo_cte` varchar(255) DEFAULT NULL,
  `fondo_cte` varchar(255) DEFAULT NULL,
  `color_primario` varchar(45) DEFAULT NULL,
  `color_secundario` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_estilo`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_estilo`
--

LOCK TABLES `tbl_estilo` WRITE;
/*!40000 ALTER TABLE `tbl_estilo` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_estilo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_forma_acumulacion`
--

DROP TABLE IF EXISTS `tbl_forma_acumulacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_forma_acumulacion` (
  `id_forma_acumulacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_forma_acumulacion`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_forma_acumulacion`
--

LOCK TABLES `tbl_forma_acumulacion` WRITE;
/*!40000 ALTER TABLE `tbl_forma_acumulacion` DISABLE KEYS */;
INSERT INTO `tbl_forma_acumulacion` VALUES (1,'Identificación con celular','celular'),(2,'Escaneo de código QR','qr'),(3,'Registro ID de la tarjeta','id'),(4,'Identificación con mail','mail');
/*!40000 ALTER TABLE `tbl_forma_acumulacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_mecanica_redencion`
--

DROP TABLE IF EXISTS `tbl_mecanica_redencion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_mecanica_redencion` (
  `id_mecanica_redencion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `alias` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_mecanica_redencion`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_mecanica_redencion`
--

LOCK TABLES `tbl_mecanica_redencion` WRITE;
/*!40000 ALTER TABLE `tbl_mecanica_redencion` DISABLE KEYS */;
INSERT INTO `tbl_mecanica_redencion` VALUES (1,'Télefono + ID de Usuario en Comercio','telefono'),(2,'ID Tarjeta ó APP en el Comercio','id'),(3,'Correo Electronico +ID de Usuario en Comercio','mail');
/*!40000 ALTER TABLE `tbl_mecanica_redencion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_pantalla`
--

DROP TABLE IF EXISTS `tbl_pantalla`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_pantalla` (
  `id_pantalla` int(3) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `ruta` varchar(45) DEFAULT NULL,
  `activo` int(2) DEFAULT NULL,
  `id_area` int(3) DEFAULT NULL,
  `icono` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_pantalla`),
  KEY `id_area_idx` (`id_area`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_pantalla`
--

LOCK TABLES `tbl_pantalla` WRITE;
/*!40000 ALTER TABLE `tbl_pantalla` DISABLE KEYS */;
INSERT INTO `tbl_pantalla` VALUES (4,'Usuarios','../administracion/admin_usuarios.php',0,1,'users'),(5,'Registro','../registro/registro.php',0,3,'id-badge'),(6,'Acumulación','../acumulacion/acumulacion.php',0,3,'boxes'),(7,'Redencion','../redencion/redencion.php',0,3,'gift'),(9,'Historial ','../registros/historico.php',0,4,'list-ol');
/*!40000 ALTER TABLE `tbl_pantalla` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_periodo_comunicacion`
--

DROP TABLE IF EXISTS `tbl_periodo_comunicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_periodo_comunicacion` (
  `id_periodo_comunicacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_periodo_comunicacion`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_periodo_comunicacion`
--

LOCK TABLES `tbl_periodo_comunicacion` WRITE;
/*!40000 ALTER TABLE `tbl_periodo_comunicacion` DISABLE KEYS */;
INSERT INTO `tbl_periodo_comunicacion` VALUES (1,'Quincenal'),(2,'Mensual');
/*!40000 ALTER TABLE `tbl_periodo_comunicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_puntos_totales`
--

DROP TABLE IF EXISTS `tbl_puntos_totales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_puntos_totales` (
  `id_puntos_totales` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `puntos_totales` varchar(45) DEFAULT NULL,
  `id_tipo_puntos` int(11) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  PRIMARY KEY (`id_puntos_totales`),
  KEY `id_usuar_idx` (`id_usuario`),
  KEY `id_tipo_regi_idx` (`id_tipo_puntos`),
  CONSTRAINT `id_tipo_regi` FOREIGN KEY (`id_tipo_puntos`) REFERENCES `tbl_tipo` (`id_tipo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_usuar` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_puntos_totales`
--

LOCK TABLES `tbl_puntos_totales` WRITE;
/*!40000 ALTER TABLE `tbl_puntos_totales` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_puntos_totales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_registros`
--

DROP TABLE IF EXISTS `tbl_registros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_registros` (
  `id_registro_acumulacion` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) DEFAULT NULL,
  `ticket` varchar(255) DEFAULT NULL,
  `monto_ticket` float DEFAULT NULL,
  `id_configuracion` int(11) DEFAULT NULL,
  `puntos` int(11) DEFAULT NULL,
  `num_visita` int(4) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `dia_registro` varchar(45) DEFAULT NULL,
  `id_tipo_registro` int(11) DEFAULT NULL,
  `id_usuario_registro` int(11) DEFAULT NULL,
  `id_cat_premio` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_registro_acumulacion`),
  KEY `id_us_idx` (`id_usuario`),
  KEY `id_con_idx` (`id_configuracion`),
  KEY `id_tipo_idx` (`id_tipo_registro`),
  KEY `id_us_regitro_idx` (`id_usuario_registro`),
  KEY `id_cat_prem_idx` (`id_cat_premio`),
  CONSTRAINT `id_cat_prem` FOREIGN KEY (`id_cat_premio`) REFERENCES `tbl_cat_premios_productos_servicios` (`id_cat_premios_productos_servicios`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_con` FOREIGN KEY (`id_configuracion`) REFERENCES `tbl_configuracion_proyecto` (`id_configuracion_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_tipo_reg` FOREIGN KEY (`id_tipo_registro`) REFERENCES `tbl_tipo` (`id_tipo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_us` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_us_regitro` FOREIGN KEY (`id_usuario_registro`) REFERENCES `tbl_usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_registros`
--

LOCK TABLES `tbl_registros` WRITE;
/*!40000 ALTER TABLE `tbl_registros` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_registros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_config_campos_registro`
--

DROP TABLE IF EXISTS `tbl_rel_config_campos_registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_config_campos_registro` (
  `id_rel_config_campos_registro` int(11) NOT NULL AUTO_INCREMENT,
  `id_config` int(11) DEFAULT NULL,
  `id_campo` int(11) DEFAULT NULL,
  `posicion` int(11) DEFAULT NULL,
  `clave` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rel_config_campos_registro`),
  KEY `id_configura_idx` (`id_config`),
  KEY `id_campo_idx` (`id_campo`),
  CONSTRAINT `id_campo` FOREIGN KEY (`id_campo`) REFERENCES `tbl_campos_registro` (`id_campos_registro`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_configura` FOREIGN KEY (`id_config`) REFERENCES `tbl_configuracion_proyecto` (`id_configuracion_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_config_campos_registro`
--

LOCK TABLES `tbl_rel_config_campos_registro` WRITE;
/*!40000 ALTER TABLE `tbl_rel_config_campos_registro` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_rel_config_campos_registro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_config_forma_acumulacion`
--

DROP TABLE IF EXISTS `tbl_rel_config_forma_acumulacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_config_forma_acumulacion` (
  `id_rel_config_forma_acumulacion` int(11) NOT NULL AUTO_INCREMENT,
  `id_configuracion_proyecto` int(11) DEFAULT NULL,
  `id_forma_acumulacion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rel_config_forma_acumulacion`),
  KEY `id_configuracion_proyecto_idx` (`id_configuracion_proyecto`),
  KEY `id_forma_acumulacion_idx` (`id_forma_acumulacion`),
  CONSTRAINT `id_configuracion_proyecto` FOREIGN KEY (`id_configuracion_proyecto`) REFERENCES `tbl_configuracion_proyecto` (`id_configuracion_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_forma_acumulacion` FOREIGN KEY (`id_forma_acumulacion`) REFERENCES `tbl_forma_acumulacion` (`id_forma_acumulacion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_config_forma_acumulacion`
--

LOCK TABLES `tbl_rel_config_forma_acumulacion` WRITE;
/*!40000 ALTER TABLE `tbl_rel_config_forma_acumulacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_rel_config_forma_acumulacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_config_mecanica_redencion`
--

DROP TABLE IF EXISTS `tbl_rel_config_mecanica_redencion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_config_mecanica_redencion` (
  `id_rel_config_mecanica_redencion` int(11) NOT NULL AUTO_INCREMENT,
  `id_configuracion_proyecto` int(11) DEFAULT NULL,
  `id_mecanica_redencion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rel_config_mecanica_redencion`),
  KEY `id_con_proyecto_idx` (`id_configuracion_proyecto`),
  KEY `id_mecanica_redencion_idx` (`id_mecanica_redencion`),
  CONSTRAINT `id_con_proyecto` FOREIGN KEY (`id_configuracion_proyecto`) REFERENCES `tbl_configuracion_proyecto` (`id_configuracion_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_mecanica_redencion` FOREIGN KEY (`id_mecanica_redencion`) REFERENCES `tbl_mecanica_redencion` (`id_mecanica_redencion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_config_mecanica_redencion`
--

LOCK TABLES `tbl_rel_config_mecanica_redencion` WRITE;
/*!40000 ALTER TABLE `tbl_rel_config_mecanica_redencion` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_rel_config_mecanica_redencion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_config_tipo_redencion`
--

DROP TABLE IF EXISTS `tbl_rel_config_tipo_redencion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_config_tipo_redencion` (
  `id_rel_config_tipo_redencion` int(11) NOT NULL AUTO_INCREMENT,
  `id_configuracion_proyecto` int(11) DEFAULT NULL,
  `id_tipo_redencion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rel_config_tipo_redencion`),
  KEY `id_config_proyecto_idx` (`id_configuracion_proyecto`),
  KEY `id_tip_redencion_idx` (`id_tipo_redencion`),
  CONSTRAINT `id_config_proyecto` FOREIGN KEY (`id_configuracion_proyecto`) REFERENCES `tbl_configuracion_proyecto` (`id_configuracion_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_tip_redencion` FOREIGN KEY (`id_tipo_redencion`) REFERENCES `tbl_tipo_redencion` (`id_tipo_redencion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_config_tipo_redencion`
--

LOCK TABLES `tbl_rel_config_tipo_redencion` WRITE;
/*!40000 ALTER TABLE `tbl_rel_config_tipo_redencion` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_rel_config_tipo_redencion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_proyecto_redencion`
--

DROP TABLE IF EXISTS `tbl_rel_proyecto_redencion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_proyecto_redencion` (
  `id_rel_proyecto_redencion` int(11) NOT NULL AUTO_INCREMENT,
  `id_proyecto` int(11) DEFAULT NULL,
  `id_redencion` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_rel_proyecto_redencion`),
  KEY `id_redencion_idx` (`id_redencion`),
  KEY `id_proyecto_idx` (`id_proyecto`),
  CONSTRAINT `id_proyecto` FOREIGN KEY (`id_proyecto`) REFERENCES `tbl_tipo_proyecto` (`id_tipo_proyecto`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_redencion` FOREIGN KEY (`id_redencion`) REFERENCES `tbl_tipo_redencion` (`id_tipo_redencion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_proyecto_redencion`
--

LOCK TABLES `tbl_rel_proyecto_redencion` WRITE;
/*!40000 ALTER TABLE `tbl_rel_proyecto_redencion` DISABLE KEYS */;
INSERT INTO `tbl_rel_proyecto_redencion` VALUES (2,1,2),(3,1,3),(4,2,2),(5,2,3),(6,3,4),(7,4,5);
/*!40000 ALTER TABLE `tbl_rel_proyecto_redencion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rel_usuario_area`
--

DROP TABLE IF EXISTS `tbl_rel_usuario_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rel_usuario_area` (
  `id_rel_usuario_area` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(3) DEFAULT NULL,
  `id_area` int(3) DEFAULT NULL,
  `activo` int(2) DEFAULT NULL,
  PRIMARY KEY (`id_rel_usuario_area`),
  KEY `id_usuario_idx` (`id_usuario`),
  KEY `id_area_idx` (`id_area`),
  CONSTRAINT `id_area` FOREIGN KEY (`id_area`) REFERENCES `tbl_area` (`id_area`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `id_usuario` FOREIGN KEY (`id_usuario`) REFERENCES `tbl_usuario` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=144 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rel_usuario_area`
--

LOCK TABLES `tbl_rel_usuario_area` WRITE;
/*!40000 ALTER TABLE `tbl_rel_usuario_area` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_rel_usuario_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_rol`
--

DROP TABLE IF EXISTS `tbl_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_rol` (
  `id_tbl_rol` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id_tbl_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_rol`
--

LOCK TABLES `tbl_rol` WRITE;
/*!40000 ALTER TABLE `tbl_rol` DISABLE KEYS */;
INSERT INTO `tbl_rol` VALUES (1,'Administrador'),(2,'Punto de Venta'),(3,'Usuario');
/*!40000 ALTER TABLE `tbl_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tipo`
--

DROP TABLE IF EXISTS `tbl_tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo` (
  `id_tipo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo`
--

LOCK TABLES `tbl_tipo` WRITE;
/*!40000 ALTER TABLE `tbl_tipo` DISABLE KEYS */;
INSERT INTO `tbl_tipo` VALUES (1,'Acumulación'),(2,'Redención'),(3,'Bono Bienvenida'),(4,'Bono Cumpleaños');
/*!40000 ALTER TABLE `tbl_tipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tipo_comunicacion`
--

DROP TABLE IF EXISTS `tbl_tipo_comunicacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo_comunicacion` (
  `id_tipo_comunicacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tipo_comunicacion`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo_comunicacion`
--

LOCK TABLES `tbl_tipo_comunicacion` WRITE;
/*!40000 ALTER TABLE `tbl_tipo_comunicacion` DISABLE KEYS */;
INSERT INTO `tbl_tipo_comunicacion` VALUES (1,'Sms'),(2,'Mail');
/*!40000 ALTER TABLE `tbl_tipo_comunicacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tipo_proyecto`
--

DROP TABLE IF EXISTS `tbl_tipo_proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo_proyecto` (
  `id_tipo_proyecto` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tipo_proyecto`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo_proyecto`
--

LOCK TABLES `tbl_tipo_proyecto` WRITE;
/*!40000 ALTER TABLE `tbl_tipo_proyecto` DISABLE KEYS */;
INSERT INTO `tbl_tipo_proyecto` VALUES (1,'Puntos como % de Consumo'),(2,'Puntos como Visitas'),(3,'Visitas'),(4,'% Incremental');
/*!40000 ALTER TABLE `tbl_tipo_proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tipo_redencion`
--

DROP TABLE IF EXISTS `tbl_tipo_redencion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo_redencion` (
  `id_tipo_redencion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  `link_detalle` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tipo_redencion`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo_redencion`
--

LOCK TABLES `tbl_tipo_redencion` WRITE;
/*!40000 ALTER TABLE `tbl_tipo_redencion` DISABLE KEYS */;
INSERT INTO `tbl_tipo_redencion` VALUES (2,'Canje de Puntos por Premios, Productos ó Servicios ','../../productos_servicios_catalogo.php'),(3,'Canje por voucher / Dinero electrónico para consumo en tienda',''),(4,'Entrega de Premio al alcanzar \"n\" numero de visitas','../../premios_visitas.php'),(5,'Redencion automatica en el momento de pago','../../porcentaje_incremental.php');
/*!40000 ALTER TABLE `tbl_tipo_redencion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_tipo_registro`
--

DROP TABLE IF EXISTS `tbl_tipo_registro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_tipo_registro` (
  `id_tipo_registro` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tipo_registro`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_tipo_registro`
--

LOCK TABLES `tbl_tipo_registro` WRITE;
/*!40000 ALTER TABLE `tbl_tipo_registro` DISABLE KEYS */;
INSERT INTO `tbl_tipo_registro` VALUES (1,'Auto Registro'),(2,'Registro Atendido');
/*!40000 ALTER TABLE `tbl_tipo_registro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbl_usuario`
--

DROP TABLE IF EXISTS `tbl_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_usuario` (
  `id_usuario` int(3) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `fecha_ultimo_acceso` datetime DEFAULT NULL,
  `activo` int(2) DEFAULT '0',
  `rol` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `rfc` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `tarjeta` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `id_rol_idx` (`rol`),
  CONSTRAINT `id_rol` FOREIGN KEY (`rol`) REFERENCES `tbl_rol` (`id_tbl_rol`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbl_usuario`
--

LOCK TABLES `tbl_usuario` WRITE;
/*!40000 ALTER TABLE `tbl_usuario` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'plataforma_lealtad'
--
/*!50003 DROP PROCEDURE IF EXISTS `limpia_base` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `limpia_base`()
BEGIN
delete  from  tbl_rel_config_forma_acumulacion;
 
delete from tbl_rel_config_tipo_redencion;

delete from  tbl_rel_config_mecanica_redencion;

delete from tbl_registros;

delete from tbl_rel_usuario_area ;

delete from tbl_puntos_totales;

delete from tbl_usuario ;

delete from tbl_rel_config_campos_registro ;

delete from tbl_configuracion_proyecto;

delete from tbl_estilo;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-15 14:33:54
