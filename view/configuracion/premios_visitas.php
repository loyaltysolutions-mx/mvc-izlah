
    <div id="lead-response"></div>
    <div class="card">
        <div class="body">
            <form id="addreward" onsubmit="uprew2(1); return false;">
                <div class="msg">Registro de Recompensa/Producto</div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control" id="reward2" name="reward2" placeholder="* Recompensa" required>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control" id="descript2" name="descript2" placeholder="* Descripción" required>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="number" min="1" class="form-control" id="cant2" name="cant2" placeholder="* Número de Recompensas" required>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="number" min="1" class="form-control" id="points2" name="points2" placeholder="* Visitas Necesarias" required>
                    </div>
                </div>
                <input type="submit" value="Enviar" class="hide" />
            </form>

            <form action="/" id="frmFileUpload2" class="dropzone needsclick dz-clickable" method="post" enctype="multipart/form-data">
                <div class="dz-message needsclick">
                    <div class="drag-icon-cph">
                        <i class="material-icons">touch_app</i>
                    </div>
                    <div>
                        Arrastra tu imagen o haz click para agregarla
                    </div>
                </div>
                <div class="fallback">
                    <input id="files" name="files" type="file" />
                </div>
            </form>
            <input id="filename2" type="hidden" name="filename2">

            <button class="btn btn-block btn-lg bg-blue waves-effect" onclick="sendform2();" id="submit-lead2" name="submit-lead2">Agregar</button>

        </div>

    </div>

<!-- IMPORTANT CONTENT-->
<!-- Dropzone Plugin Js -->

<script>

    Dropzone.options.frmFileUpload2 = {
        maxFilesize: 2,
        acceptedFiles: "image/jpeg,image/png",

        accept: function(file, done) {
            console.log("uploaded");
            done();
            var filename = file.name;
            $('#filename2').val(filename);
            $('#submit-lead2').prop( 'disabled',false);
            
            
        },
        init: function() {
            this.on("addedfile", function() {
              if (this.files[1]!=null){
                this.removeFile(this.files[0]);
              }
            });
        }
    };
    $("form#frmFileUpload2").dropzone({ 
        url: "../configuracion/upload.php"
     });

    function sendform2(){
        var form = $('#addreward');
        $('#addreward').find('[type="submit"]').trigger('click');
    }
</script>