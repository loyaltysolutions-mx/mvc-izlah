<?php 
$userid = $_SESSION["idusuario"];
include_once '../../controller/user/c_user.php';
$select = new user($ser,$usu,$pas,$bd);
?>  
    <meta name="csrf-token" content="XYZ123">
    <div id="lead-response"></div>
    <div class="card">
        <div class="body">
            <form id="addcoupon" onsubmit="addcoupon(); return false;">
                <div class="msg">Registro de Nuevo Cupón</div>
                <div class="input-group">
                    <h5>* Parque</h5>
                    <div class="form-line">
                        <select id="park" name="park" onchange="" class="form-control show-tick" required>
                            <?php echo $select->selectpark(); ?>
                        </select>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control" id="coupon" name="coupon" placeholder="* Título Cupón" required>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control" id="detail" name="detail" placeholder="* Detalle" required>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control" id="barcode" name="barcode" placeholder="* Código de Barras" pattern="[A-Za-z0-9!?-]{6,10}" required>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-lg-4 col-xs-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="number" min="1" class="form-control" id="ncoupons" name="ncoupons" placeholder="* No. De Cupones" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xs-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="number" min="1" class="form-control" id="redention" name="redention" placeholder="* No. Redención por Visita" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4 col-xs-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="number" min="0" class="form-control" id="halfdays" name="halfdays" placeholder="* No. Días para Activación" required>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="submit" value="Enviar" class="hide" />
            </form>

            <form action="/" id="frmFileUpload" class="dropzone needsclick dz-clickable" method="post" enctype="multipart/form-data">
                <div class="dz-message needsclick">
                    <div class="drag-icon-cph">
                        <i class="material-icons">touch_app</i>
                    </div>
                    <div>
                        Arrastra tu imagen o haz click para agregarla
                    </div>
                </div>
                <div class="fallback">
                    <input id="file" name="file" type="file" />
                </div>
            </form>
            <input id="filename" type="hidden" name="filename">

            <button class="btn btn-block btn-lg bg-deep-purple waves-effect" onclick="sendform();" id="submit-lead" name="submit-lead" disabled="true">Agregar</button>

        </div>

    </div>

<!-- IMPORTANT CONTENT-->
<!-- Jquery Core Js -->
<script src="../../plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="../../plugins/bootstrap/js/bootstrap.js"></script>

<!-- Dropzone Plugin Js -->
<script src="../../plugins/dropzone/dropzone.js"></script>
<!-- END IMPORTANT -->
<script>

    Dropzone.options.frmFileUpload = {
        maxFilesize: 2,
        acceptedFiles: "image/jpeg,image/png",

        accept: function(file, done) {
            console.log("uploaded");
            done();
            var filename = file.name;
            $('#filename').val(filename);
            $('#submit-lead').prop( 'disabled',false);
            
            
        },
        init: function() {
            this.on("addedfile", function() {
              if (this.files[1]!=null){
                this.removeFile(this.files[0]);
              }
            });
        }
    };
    $("form#frmFileUpload").dropzone({
        url: "../usuario/upload.php"
    });

    function sendform(){
        var form = $('#addcoupon');
        $('#addcoupon').find('[type="submit"]').trigger('click');
    }
</script>