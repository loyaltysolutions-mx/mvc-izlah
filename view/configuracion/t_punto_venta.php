<div class="body">
    <ul class="nav nav-tabs tab-nav-right" role="tablist">
        <li role="presentation" class="active"><a href="#insert" data-toggle="tab" aria-expanded="true">Insertar</a></li>
        <li role="presentation" class=""><a href="#modify" data-toggle="tab" aria-expanded="false">Modificar/Eliminar</a></li>
    </ul>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade active in" id="insert">
            
            <div class="body">
                <form id="register" onsubmit="register(); return false;">
                    <div class="msg">Registro de Nuevo Tipo de Punto de Venta</div>
                    <div class="input-group">
                        <div class="form-line">
                            <input type="text" class="form-control" id="ptovta" name="ptovta" placeholder="* Tipo de Punto de Venta" required autofocus>
                        </div>
                    </div>
                    

                    <button class="btn btn-block btn-lg bg-blue waves-effect" type="submit" name="submit-lead">Agregar</button>
                </form>
            </div>
      
        </div>
        <div role="tabpanel" class="tab-pane fade" id="modify">
            <div class="body">
            <!-- Edit Users table -->
                <h3>Edición de Promociones</h3>
                <div class="table-responsive">
                    <table id="mainTable" class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Punto de Venta</th>
                                <th>Editar</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                echo $ins_control->grideditpto('tr');
                            ?>
                        </tbody>
                    </table>
                </div>
                <!-- #END# Exportable Table -->
            </div>    
        </div>
    </div>
</div>
<script>
    function register(){
        var tipo_ptovta = $('#ptovta').val();
        var op = 3;

        $.ajax({
            data:{ op:op, tipo_ptovta:tipo_ptovta },
            url: '../../controller/configuracion/c_llamadas_ajax2.php',
            method:'post',
            beforeSend: function(response){

            },
            success: function(response){
                if(response==1){
                    swal(" ", "Registro Agregado", "success");
                }else{
                    swal('Error!','El resgistro no pudo agregarse','warning');
                }    
            }
        });
    }
</script>