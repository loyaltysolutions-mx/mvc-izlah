
    <div id="lead-response"></div>
    <div class="card">
        <div class="body">
            <div class="msg">Registro de Recompensa/Producto</div>
            <form id="rewardpts" onsubmit="uprew(2); return false;">
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control" id="reward" name="reward" placeholder="* Recompensa" required>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control" id="descript" name="descript" placeholder="* Descripción" required>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="number" min="1" class="form-control" id="cant" name="cant" placeholder="* Número de Recompensas" required>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="number" min="1" class="form-control" id="points" name="points" placeholder="* Puntos Necesarios" required>
                    </div>
                </div>
                <input type="submit" value="Enviar" class="hide" />
            </form>

            <form action="/" id="frmFileUpload" class="dropzone needsclick dz-clickable" method="post" enctype="multipart/form-data">
                <div class="dz-message needsclick">
                    <div class="drag-icon-cph">
                        <i class="material-icons">touch_app</i>
                    </div>
                    <div>
                        Arrastra tu imagen o haz click para agregarla
                    </div>
                </div>
                <div class="fallback">
                    <input id="files" name="files" type="file" />
                </div>
            </form>
            <input id="filename" type="hidden" name="filename">

            <button class="btn btn-block btn-lg bg-blue waves-effect" onclick="sendform();" id="submit-lead" name="submit-lead">Agregar</button>

        </div>

    </div>

<!-- Dropzone Plugin Js -->
<script src="../../inc/plugins/dropzone/dropzone.js"></script>
<script>

    Dropzone.options.frmFileUpload = {
        maxFilesize: 2,
        acceptedFiles: "image/jpeg,image/png",

        accept: function(file, done) {
            console.log("uploaded");
            done();
            var filename = file.name;
            $('#filename').val(filename);
            $('#submit-lead').prop( 'disabled',false);
            
            
        },
        init: function() {
            this.on("addedfile", function() {
              if (this.files[1]!=null){
                this.removeFile(this.files[0]);
              }
            });
        }
    };
    $("form#frmFileUpload").dropzone({ 
        url: "../configuracion/upload.php"
     });

    function sendform(){
        var form = $('#rewardpts');
        $('#rewardpts').find('[type="submit"]').trigger('click');
    }
</script>