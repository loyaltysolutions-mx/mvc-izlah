<?php
include '../../controller/administracion/c_admin_rewards.php';
$select = new C_admin_rerwards($ser,$usu,$pas,$bd);
// 1 y 2 puntos....
// 3 Visitas
?>
<script src="../../inc/js/edit_rewards.js"></script>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="pull-left">
            <button type="button" name="" class="previous-form btn btn-info " onclick="shownew('newreward')"><i class="fas fa-plus"></i> Agregar Nueva Recompensa </button>
        </div>
        <div class="pull-right">
            <a id="btnMasive" class="previous-form btn btn-info" href="javascript:;" ><i class="fas fa-plus"></i> Carga Múltiple de Recompensas </a>
        </div>
    </div>
</div>
<br>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div id="newreward" style="display: none;">
            <?php 
                $projectype = $select->projectype();
                if($projectype==1 OR $projectype==2){
                    include 'premios_puntos.php';
                    $pts = 1;
                }elseif ($projectype==3) {
                    include 'premios_visitas.php';
                    $vts = 1;
                } 
            ?>  
        </div>
    </div>
</div>
<div id="divMasivo" class="row" style="display:none;">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">        
        <div class="card">
            <div class="header"><h2 class="text_color"><i class="fa fa-upload"></i> Subir archivo de recompensas </h2></div>
            <div class="body">
                <form id="formFileRecompensa" action="#" method="post" enctype="multipart/form-data">
                    <div id="dropzoneRecompensa" class="dropzone needsclick dz-clickable">
                        <div class="dz-message needsclick">
                            <div class="drag-icon-cph">
                                <i class="material-icons">attach_file</i>
                            </div>
                            <h5>
                                Arrastra tu archivo aquí o click para seleccionar
                            </h5>
                        </div>
                        <div class="fallback">
                            <input id="file_reward" name="file_reward" type="file" required />
                        </div>
                    </div>
                    <br>
                    <button type="submit" class="btn btn_color btn-block">Subir Archivo</button>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
        <div class="card">
            <div class="header"><h2 class="text_color"><i class="fas fa-gift"></i> Lista de recompensas </h2></div>            
            <div class="body">
                <!-- rewards table -->
                <div class="table-responsive">
                    <table id="mainTable" class="table table-bordered table-striped table-hover dataTable js-exportable">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                            <?php 
                                if($pts==1){
                            ?>  
                                <th>Puntos</th>
                            <?php 
                                }
                            ?>
                            <?php 
                                if($vts==1){
                            ?>    
                                <th>Visitas</th>
                            <?php 
                                }
                            ?>
                                <th>Imagen</th>
                                <th>Detalle</th>
                                <th>Cantidad</th>
                                <th>Activar/Descativar</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                echo $select->gridedit('tr',$projectype);
                            ?>
                        </tbody>
                    </table>
                </div>  
            </div>
        </div>
    </div>
</div>
<!-- #END# Exportable Table -->
<!-- #Modal -->
<div class="modal fade" id="editimg" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Elija a quien se Asignara</h4>
            </div>
            <div class="modal-body">
                <form action="/" id="frmFileUpload2" class="dropzone needsclick dz-clickable" method="post" enctype="multipart/form-data">
                    <div class="dz-message needsclick">
                        <div class="drag-icon-cph">
                            <i class="material-icons">touch_app</i>
                        </div>
                        <div>
                            Arrastra tu imagen o haz click para agregarla
                        </div>
                    </div>
                    <div class="fallback">
                        <input id="files" name="file-reward" type="file" />
                    </div>
                </form>
            <input id="filename2" type="hidden" name="filename2">  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>
<!-- IMPORTANT CONTENT-->
<!-- Jquery Core Js -->


<!-- Dropzone Plugin Js -->
<!--script src="../../inc/plugins/dropzone/dropzone.js"></script-->
<script src="../../inc/js/uploadRecompensaByFile.js"></script>
<script>
    //Active/Inactive
    $('.ifcheck').change(function() {
        if($(this).is(":checked")) {
            var idx = $(this).attr('id');
            var id = idx.replace("x", "");
            activerwd(id);
        }else{
            var idx = $(this).attr('id');
            var id = idx.replace("x", "");
            inactiverwd(id);
        }        
    });

    //Dropzone config by Allan M.
    Dropzone.options.frmFileUpload2 = {
        maxFilesize: 2,
        acceptedFiles: "image/jpeg,image/png"
    };


    $("form#frmFileUpload2").dropzone({ 
        url: "../configuracion/upload.php",

        removedfile: function(file) {
            this.on("dragstart", function(file) { 
               this.removeAllFiles(true); 
            });
        },

        accept: function(file, done) {
            
            console.log("uploaded");
            done();
            var sendid = $('#filename2').attr('my');
            //nedd add table and field id
            setfile(sendid,file.name);
            var filename = file.name;
            $('#filename2').val(filename);
            
        },
        init: function() {
            this.on("addedfile", function() {
              if (this.files[1]!=null){
                this.removeFile(this.files[0]);
              }
            });
        }
    });

    //function to set reward by points and visits
    function uprew(option){
    var reward = $('#reward').val();
    var filename = $('#filename').val();
    var description = $('#descript').val();
    var cant = $('#cant').val();
    var points = $('#points').val();
    var op = 9;

    $.ajax({
        data:{ op: op, option: option, reward: reward, filename: filename, description: description, cant:cant, points: points },
        url: '../../controller/configuracion/c_llamadas_ajax2.php',
        method:'post',
        beforeSend: function(response){

        },
        success: function(response){
            swal('', 'Producto Agregado', 'success');
            menu('administracion/admin_rewards');
        }
    });
}
</script>

<!-- #End Modal -->
<?php
 //require_once('../../inc/datatables.php');
?>

