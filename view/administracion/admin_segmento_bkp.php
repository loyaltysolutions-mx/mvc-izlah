<?php 
/*************************************************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#- Vista de Registro -#
##################################################################################### 
include_once '../../inc/cont_fijos.php';
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
include_once '../../controller/administracion/c_admin_segmento.php';
$instanceSegmento=new C_admin_segmento($ser,$usu,$pas,$bd);
$segmentos = $instanceSegmento->getSegmentos(NULL);
$ins_cont_fijos=new Contenidos_Fijos($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
session_start();
if(isset($_SESSION["usuario"])){ 
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?php  
 $ins_cont_fijos->head();
 //TRAEMOS CONFIGURACION DE ESTILOS
 $res_con=$ins_funciones->consulta_generica('tbl_estilo', ' ');
 $registro= mysqli_fetch_assoc($res_con);
 //VALIDAMOS LOGO
if($registro['logo_cte']==''){
    $img_logo='imagenes/logo.png';
}else{
    $img_logo='imagenes/img_configuracion/'.$registro['logo_cte'];
}
 //VALIDAMOS IMAGEN FONDO
if($registro['fondo_cte']==''){
     $img_background='imagenes/background_default.jpg';
}else{
    $img_background='imagenes/img_configuracion/'.$registro['fondo_cte'];
}
 //VALIDAMOS COLOR PRIMARIO
if($registro['color_primario']==''){
     $color_primario='#006AA9';
}else{
    $color_primario=$registro['color_primario'];
}
 //VALIDAMOS COLOR SECUNDARIO
if($registro['color_secundario']==''){
     $color_secundario='#006AA9';
}else{
    $color_secundario=$registro['color_secundario'];
}
  ?>
      <script type="text/javascript" src='../../inc/js/admin_segmento.js'></script>
</head>
<?php
//LLAMAMOS ESTILOS DE CONFIGURACION
$ins_cont_fijos->estilos($img_logo,$img_background,$color_primario,$color_secundario);
?>

<body class="theme-red">
    <!-- Pag. Loading-->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Espera un poco...</p>
        </div>
    </div>
    <!-- Barra Ariba -->
    <nav class="navbar" style='height: 70px;'> 
        <div class="container-fluid">
            <div class="navbar-header">
                 <a href="javascript:void(0);" class="bars"></a>
                <span class="navbar-brand" >Plataforma Lealtad </span>
            </div>
        </div>
    </nav>
    <section>
        <!-- MeNU IZQUIERDO -->
        <aside id="leftsidebar" class="sidebar">
            <!-- Usuario-->
            <div class="user-info">
                <div class="image">
                    <img src="../../inc/imagenes/user.png" width="48" height="48" alt="User" />
                    <img src="../../inc/<?php echo $img_logo; ?>" style='    margin-left: 130px;' width="60" height="60" alt="Logo" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION["usuario"];?></div>
                    <!--<div class="email">john.doe@example.com</div>-->
                </div>
            </div>
            <!-- MENU -->
            <div class="menu">
                <ul class="list">
                    <?php echo $ins_cont_fijos->menu($_SESSION["id_usuario"]);?>
                </ul>
            </div>
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                        <?php 
                        $ins_cont_fijos->footer();
                        ?>
                </div>
            </div>
        </aside>
    </section>
<!-- CONTENIDO -->
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2><i class="fas fa-tags"></i></i> Administración de Segmentos</h2>
            </div>
            <div class="row clearfix">                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <button type="button" name="previous" class="previous-form btn btn-info " id='muestra_form_add_usu'><i class="fas fa-plus"></i> Agregar Nuevo Segmento </button><br><br>
                </div>
                <div class="w-100"></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="form_add_segmento" class="card">
                        <div class="row">                    
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div style="padding:10px 0 0 10px">
                                    <h5>Por favor ingrese los siguientes campos:</h5>
                                </div>                                
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <form class="form-group" style="padding:10px">
                                    <div class="form-group">
                                        <label for="nombreSegmento">Nombre Segmento: *</label><input type="text" class="form-control"  name='nombre_segmento' placeholder="Nombre del Segmento" id='nombreSegmento'>
                                        <span id="labelNombre" style="color:red; font-weight:bold"></span>
                                    </div>                            
                                    <div class="form-group" style="margin-top:8px">
                                        <label for="descripcionSegmento">Descripción:</label><textarea class="form-control" name='descripcion' placeholder="Campo opcional de 256 caracteres" id='descripcionSegmento'></textarea>
                                    </div>
                                    <div class="form-group" style="margin-top:8px">
                                        <div id='loading'><img src="../../inc/imagenes/load.gif"></div>
                                        <button id='btnNewSegmento' type="button" class="btn  m-t-15 waves-effect btn-primary" >Crear Segmento</button>
                                    </div>
                                </form>
                            </div>                            
                        </div>                
                    </div>
                </div>                
            </div>
            
            <!-- GRID -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Lista de Segmentos
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>                                            
                                            <th>ID </th>
                                            <th>Nombre Segmento</th>
                                            <th>Descripción</th>
                                            <th>Status</th>
                                            <th>Acciones</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                <?php 
                                    //GENERAMOS CONTENIDO DE TABLA                               
                                    while($fila = $segmentos->fetch_assoc()){ ?>
                                        <tr>
                                            <td><?php echo $fila['id_segmento']; ?></td>                                                
                                            <td><?php echo utf8_encode($fila['nombre_segmento']); ?></td>
                                            <td><?php echo utf8_encode($fila['descripcion_segmento']); ?></td>
                                            <td><?php echo $fila['activo'] == 1 ? 'Activo' : 'Inactivo' ; ?></td>
                                            <td><a href="<?php echo 'edit_segmento.php?id='.$fila['id_segmento'] ?>" class="btn btn-primary "><i class="fa fa-edit"></i> </a><button type="button" class="btn btn-danger btnDeleteSegmento" value="<?php echo $fila['id_segmento']; ?>"><i class="far fa-trash-alt"></i></button></td>
                                        </tr>
                                   <?php  } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </section>
 
    <!-- Bootstrap Select Css -->
    <link href="../../inc/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <!-- Select Plugin Js -->
    <script src="../../inc/plugins/bootstrap-select/js/bootstrap-select.js"></script>



    <script src="../../inc/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="../../inc/plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="../../inc/plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="../../inc/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
  <script src="../../inc/js/pages/forms/basic-form-elements.js"></script>
</body>
</html>
 <?php }else{
      $redirec= "../../" ;
      header('Location:'.$redirec );
 }