
<?php 
/*************************************************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#- Vista de Registro -#
##################################################################################### 
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
include_once '../../controller/administracion/c_admin_usu.php';
include '../../controller/registro/c_registro.php';

$ins_control_admin_usu=new C_admin_usu($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();

$fields = new C_registro($ser,$usu,$pas,$bd);

session_start();
if(isset($_SESSION["usuario"])){ 
?>
<div class="row">
    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
        <a href="javascript:;" class="btn btn-primary" id='btnNewClient'><i class="fas fa-plus"></i> Agregar cliente </a><br><br>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">
        <div class="card" id='cardNewClient'>   
            <div class="header">
                <h2 class="text_color"><i class="fas fa-plus"></i> Agregar Cliente <br><small>Ingrese los campos solicitados para agregar un nuevo cliente</small></h2>
            </div>
            <div class="body">  
                <form id="formNewClient">            
                    <?php 
                        $res=$fields->trae_campos();
                        $clave='';

                        while($fila = $res->fetch_assoc()){ 
                            //EVALUAMOS SI ES CAMPO CLAVE O NO
                            if($fila['clave']==0)
                            {
                                $placeholder='';
                                $caracter='';
                                $clave.='';
                            }else{
                                $placeholder='*Este campo sera el usuario de la cuenta';
                                $caracter='<i class="fas fa-user"></i>';
                                $clave.=$fila['campo'];
                            }
                            //EVALUAMOS SI ES TELEFONO     
                            if(utf8_encode(ucwords($fila['campo']))=='Teléfono' || $fila['campo']=='telefono')
                            {
                                $coment_tel='/ Número Celular a 10 dígitos';
                                $restriction = 'minlength=10 maxlength=10';
                                $campo0=utf8_encode(ucwords($fila['campo'])) ;
                            }else{
                                $coment_tel='';
                                $restriction = '';
                                $campo0=utf8_encode(ucwords($fila['campo']));
                            } 
                            $bono_cumple=$fila['bono_cumple'];
                            $id = $fila['campo'];
                            if($id=='teléfono'){
                                $id = 'telefono';
                            }

                            if($id=='id Tarjeta'){
                    ?>
                                    <input type="hidden" id="tarjeta" value="1">
                    <?php
                            }

                            if($id=='nombre'){

                            }else{
                                if($id!='id Tarjeta'){                    
                    ?> 
                                    <div class="input-group">
                                        <span class="input-group-addon" style="color: red !important;">
                                            <?php echo $caracter; ?>
                                        </span>
                                        <div class="form-line">
                                            <input type="<?php echo $fila['campo'] != "email" ? 'text' : 'email' ?>" class="form-control" <?php echo $restriction; ?> id='<?php echo utf8_encode($id)?>' name='<?php echo utf8_encode($fila['campo'])?>' placeholder="<?php echo $campo0; ?> <?php echo $coment_tel; ?>" required>
                                        </div>
                                    </div>
                    <?php
                                }
                            }
                        }

                    ?>
                    <button class="btn btn-block btn-lg btn_color waves-effect" type="submit">Registrar Cliente</button>
                </form>
            </div>
        </div>
        <div class="card">    
            <div class="header">
                <h2><i class="fa fa-users"></i> Lista de Clientes <small>Lista la información de los clientes registrados en la plataforma</small></h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table id="tableClientes" class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>                            
                                <th>ID</th>                            
                                <th>Nombre</th>
                                <th>Nro. Celular</th> 
                                <th>Email</th> 
                                <th>Género</th>
                                <th>Fecha Nacimiento</th>
                                <th>Fecha Registro</th>
                                <th>Status Registro</th>
                                <th>Status Cuenta</th>
                                <th>Acciones</th>                            
                            </tr>
                        </thead>
                        <tbody>
                    <?php 
                        //GENERAMOS CONTENIDO DE TABLA
                        $res=$ins_control_admin_usu->get_clientes();
                        foreach($res as $fila){ ?>
                            <tr>
                                <td><?php echo $fila['id_usuario']; ?></td>                                
                                <td><?php echo utf8_encode($fila['nombre']); ?></td>
                                <td><?php echo utf8_encode($fila['telefono']); ?></td>
                                <td><?php echo utf8_encode($fila['email']); ?></td>
                                <td><?php echo ($fila['genero'] == 1 ? "Hombre" : ($fila['genero'] == 2 ? "Mujer" : "") ); ?></td>
                                <td><?php echo $fila['fecha_nacimiento'] != '0000-00-00' && $fila['fecha_nacimiento'] != null ? utf8_encode($fila['fecha_nacimiento']) : ""; ?></td>
                                <td><?php echo $fila['fecha_registro_plataforma']; ?></td>
                                <td><?php echo $fila['token'] == '' || $fila['token'] == NULL ? "Registro Completado" : "Registro Incompleto" ; ?></td>
                                <td><?php echo $fila['activo'] == 0 ? "Cuenta Activa" : "Cuenta Inactiva"; ?></td>
                                <td>
                                    <a href="javascript:;" class="btn btn-primary btnEdit" data-action="<?php echo $fila['id_usuario']; ?>" data-toggle="tooltip" title="Editar" style="width:30px"><i class="fa fa-pen"></i></a>
                                    <a href="javascript:;" class="btn btn-danger btnDelete" data-action="<?php echo $fila['id_usuario']; ?>" data-value="<?php echo $fila['activo'] == 0 ? 1 : 0 ?>" data-toggle="tooltip" title="<?php echo $fila['activo'] == 0 ? "Desactivar Cuenta" : "Activar Cuenta"; ?>" style="width:30px"><i class="fa fa-<?php echo $fila['activo'] == 0 ? "times" : "check"; ?>"></i></a>
                                </td>
                            </tr>
                        <?php  } ?>
                        </tbody>
                    </table>
                </div>        
            </div>
        </div>
    </div>
</div>

<script src="../../inc/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="../../inc/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="../../inc/js/pages/tables/jquery-datatable.js"></script>
<script src="../../inc/js/admin_clientes.js"></script>
 <?php }else{
      $redirec= "../../" ;
      header('Location:'.$redirec );
 }