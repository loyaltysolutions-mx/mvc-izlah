<?php
    include_once '../../inc/parametros.php';
    include_once '../../controller/administracion/c_admin_segmento.php';
 
    $instanceSegmento=new C_admin_segmento($ser,$usu,$pas,$bd);
    $id = $_POST['idSegmento'];
    $segmento= $instanceSegmento->getInfoSegmento($id);
    $categorias = $instanceSegmento->getCategoriasSegmento(NULL);
    session_start();
    if(isset($_SESSION["usuario"]) && count($segmento) > 0){     
?>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="javascript:;" class="btn btn-info btnBack"><i class="fas fa-angle-double-left"></i>&nbsp;Volver a lista</a><br><br></div>
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div id="form_add_segmento" class="card">
            <div class="header">                        
                <h2 class="text_color"><i class="fas fa-pen"></i> Editar Segmento</h2>
            </div>
            <div class="body">
                <form id="formEditSegmento">
                    <fieldset class="fieldset" style="padding: 8px; border: 1px solid lightgray;">
                        <legend style="border-bottom:none; padding:5px; width:auto">Información Básica</legend>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">spellcheck</i>
                            </span>
                            <div class="form-line">
                                <input type="text" class="form-control" name='nombre_segmento' placeholder="Nombre del Segmento (Obligatorio)" id='nombreSegmento' required value="<?php echo $segmento[0]['nombre_segmento'] ?>">
                            </div>
                            <span id="labelNombre" style="color:red; font-weight:bold"></span>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">subject</i>
                            </span>
                            <div class="form-line">
                                <input type="text" class="form-control" name='descripcion_segmento' placeholder="Descripción del Segmento (Opcional)" id='descripcionSegmento' value="<?php echo $segmento[0]['descripcion_segmento'] ?>">                                    
                            </div>                            
                        </div>
                    </fieldset>
                    <fieldset class="fieldset" style="padding: 8px; border: 1px solid lightgray;">
                        <legend style="border-bottom:none; padding:5px; width:auto">Configuración del Segmento</legend>
                        <div class="card" style="background-color: #eaeaea; border: 1px solid #c7c7c7;">
                            <div class="body">
                                <div id="boxSegmento">                                    
                                </div>
                                <div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right"><br><a href="javascript:;" class="btn btn_color btnANDCondition">Agregar condición AND</a></div></div>
                            </div>
                        </div>
                    </fieldset>
                    <div class="input-group">                        
                        <button type="submit" class="btn m-t-15 waves-effect btn_color"> Actualizar Segmento </button>
                    </div>
                </form>
            </div>                  
        </div>
    </div>                
</div>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="javascript:;" class="btn btn-info btnBack"><i class="fas fa-angle-double-left"></i>&nbsp;Volver a lista</a><br><br></div>
</div>
<script src='../../inc/js/box_segmento.js'></script>
<script src='../../inc/js/categoria.js'></script>
<script type="text/javascript">
    var configuracionSegmento = <?php echo json_encode($segmento)?>;
    var arraySegmento = [];
    var categorias = <?php echo json_encode($categorias)?>;
    var flagRowOR = true;
    var flagBtnDelete = true;

    //console.log(configuracionSegmento);

    $.each(configuracionSegmento, function(index, segmento){        
        $.each(segmento.configuracion, function(nivel,categoria){            

            var boxSegmento = new BoxSegmento(nivel+1);
            let indexCategoria = 0;
            $.each(categoria, function(indexObjeto, objetos){  
                flagRowOR = indexObjeto == 0 ? false : true;
                flagBtnDelete = indexObjeto == 0 ? false : true;
                
                let categoriaObjeto = new Categoria(categorias, flagRowOR, flagBtnDelete, indexCategoria);    
                categoriaObjeto.setIDCategoria(objetos.id_categoria-0);
                //console.log(objetos.objetos);
                $.each(objetos.objetos, function(indexObjeto, contentObjetos){
                    
                    switch(contentObjetos.id_objeto){                        
                        case "1":
                        case "2":
                        case "3":
                            var aux = {};
                            aux["value"] = contentObjetos.valores[0];
                            aux["name"] = contentObjetos.name;
                            aux["id_objeto"] = contentObjetos.id_objeto;
                            aux["tipo"] = contentObjetos.tipo;
                            aux["label"] = contentObjetos.label;
                            aux["id_relacion"] = contentObjetos.id_rel-0;
                            categoriaObjeto.insertInput(aux);
                            categoriaObjeto.setIDRelacion(contentObjetos.id_rel-0);
    
                            
                        break;                        
                        case "4":
                        case "5":
                            let container = categoriaObjeto.getInputs().filter(function(v) {                                
                                return v.id_relacion == categoriaObjeto.getIDRelacion(); // Filter out the appropriate one
                            });
                            var aux = {};
                            aux["valores"] = contentObjetos.valores;
                            aux["name"] = contentObjetos.name;
                            aux["id_objeto"] = contentObjetos.id_objeto;
                            aux["tipo"] = contentObjetos.tipo;
                            aux["label"] = contentObjetos.label;
                            aux["id_relacion"] = contentObjetos.id_rel-0;
                            categoriaObjeto.insertInput(aux);
                            categoriaObjeto.setIDRelacion(contentObjetos.id_rel-0);
    
                        break;

                        case "6":
                        case "7":
                        case "8":
                        case "9":
                        case "10":
                            var aux = {};
                            //console.log(contentObjetos.valores);
                            let valores = contentObjetos.valores[0].split(",");
                            
                            let minValue = valores[0].split("=");
                            let maxValue = valores[1].split("=");
                            
                            aux["value"] = {min: minValue[1], max: maxValue[1]};
                            aux["name"] = contentObjetos.name;
                            aux["id_objeto"] = contentObjetos.id_objeto;
                            aux["tipo"] = contentObjetos.tipo;
                            aux["label"] = contentObjetos.label;
                            aux["id_relacion"] = contentObjetos.id_rel-0;
                            categoriaObjeto.insertInput(aux);
                            categoriaObjeto.setIDRelacion(contentObjetos.id_rel-0);
                        break;
                    }
                       
                }); 
                flagRowOR = indexObjeto == 1 ? true : false;

                boxSegmento.insertCategoria(categoriaObjeto, true, 2, 1);
                indexCategoria++;
            });
            arraySegmento.push(boxSegmento);
            
            //console.log(arraySegmento);
        });        
    });

    //console.log(arraySegmento);
    var boxContainer = $("#boxSegmento");

    $.each(arraySegmento, function(index, boxSegmento){
        let divContainer = $("<div>");      
        let divRowAND = $("<div>",{'class':'row', style : "margin-top:8px"}).append( $("<div>",{'class':'col-lg-5 col-md-5 col-sm-5 col-xs-5'}).append($("<hr>",{style : "border: 1px solid gray"})) ).append($("<div>",{'class':'col-lg-2 col-md-2 col-sm-2 col-xs-2'}).append($("<a>",{class:'btn btn-block primary-back', style: "color: white"}).text("AND")) ).append($("<div>",{'class':'col-lg-5 col-md-5 col-sm-5 col-xs-5'}).append($("<hr>",{style : "border: 1px solid gray"})));
        let btnDeleteSegmento = $("<a>",{'href' : 'javascript:;', class : 'btn text_color', style : "box-shadow: none; font-weight:bold"}).append($("<i>",{class:"fas fa-trash-alt"}));
        let divRowBtnSegmento = $("<div>",{class:"row"}).append($("<div>",{'class' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right'}).append(btnDeleteSegmento));

        index > 0 && divContainer.append(divRowAND);
        index > 0 && divContainer.append(divRowBtnSegmento);
        
        divContainer.append(boxSegmento.getHTML());

        btnDeleteSegmento.click(function(){
            divContainer.remove();
            console.log(boxSegmento.getIndex());        
            arraySegmento.splice(boxSegmento.getIndex()-1,1);
            $.each(arraySegmento, function(key,value){
                value.setIndex(key);
            });
            console.log(arraySegmento);
        });

        boxContainer.append(divContainer);

    });

    //console.log(configuracionSegmento);

    console.log(arraySegmento);
    function backLink(){
        var link = "../../view/administracion/admin_segmento.php";
        var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
        var beforeFunction = function(){
        $("#card-content").html(div);
        };
        var successFunction = function(response){    
        $("#card-content").html(response);
        };
        var failFunction = function(data){

        };
        loadPage(link,null,beforeFunction,successFunction,failFunction);
    }
    
    $(".btnBack").click(function(){
        backLink();        
    });

    function loadPage(link, data, beforeFunction, successFunction, failFunction){
      $.ajax({
        type: "POST",
        timeout: 15000,
        url: link,
        data: data,    
        beforeSend : function(){
            beforeFunction && beforeFunction();
        }
      }).done(function(data){
          successFunction && successFunction(data);
          //console.log(data);
      }).fail(function(data){
          failFunction && failFunction(data);
          //console.log(data);
      });
    }

    $(".btnANDCondition").click(function(){
      initSegmento(true, false, true, true);      
    });

    function initSegmento(showBtnDeleteBox, showBtnDeleteCategoria, showRowAND, showBtnAdd){
      var linkCategoria = "../../controller/administracion/c_llamadas_ajax.php";
      var data = { 'op' : 3, 'action' : 9 };
      var successFunction = function(data){
        //$.each( data, function(key,value){      
          let categoria = new Categoria(data, false, showBtnDeleteCategoria, 0);
          //console.log(categoria);
          let boxSegmento = new BoxSegmento(arraySegmento.length);
          boxSegmento.insertCategoria(categoria,showBtnAdd,2,1);

          /* INICIO EDICION */
          let divContainer = $("<div>");      
          let divRowAND = $("<div>",{'class':'row', style : "margin-top:8px"}).append( $("<div>",{'class':'col-lg-5 col-md-5 col-sm-5 col-xs-5'}).append($("<hr>",{style : "border: 1px solid gray"})) ).append($("<div>",{'class':'col-lg-2 col-md-2 col-sm-2 col-xs-2'}).append($("<a>",{class:'btn btn-block primary-back', style: "color: white"}).text("AND")) ).append($("<div>",{'class':'col-lg-5 col-md-5 col-sm-5 col-xs-5'}).append($("<hr>",{style : "border: 1px solid gray"})));
          let btnDeleteSegmento = $("<a>",{'href' : 'javascript:;', class : 'btn text_color', style : "box-shadow: none; font-weight:bold"}).append($("<i>",{class:"fas fa-trash-alt"}));
          let divRowBtnSegmento = $("<div>",{class:"row"}).append($("<div>",{'class' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right'}).append(btnDeleteSegmento));

          showRowAND && divContainer.append(divRowAND);
          showBtnDeleteBox && divContainer.append(divRowBtnSegmento);
          //console.log("paso");
          divContainer.append(boxSegmento.getHTML());

          btnDeleteSegmento.click(function(){
            divContainer.remove();

            arraySegmento.splice(boxSegmento.getIndex(),1);
            $.each(arraySegmento, function(key,value){
              value.setIndex(key);
            });
            
          });

          /* FIN EDICION */

          $("#boxSegmento").append(divContainer);
          arraySegmento.push(boxSegmento);
          
        console.log(arraySegmento);
        //});
      };
      connectServer(linkCategoria, data, null, successFunction, null);
    }

    $("#formEditSegmento").on("submit", function(e){
      e.preventDefault();
      let datos = [];

      $.each(arraySegmento, function(key, value){
        let inputs = value.getCategorias();    
        var aux = [];
        $.each(inputs, function(key1, value1){
            console.log(value1.getInputs());
          aux.push({idCat:value1.idCategoria,idRel:value1.idRelacion, data:value1.getInputs()});
        });
        datos.push(aux);
      });
      
      var link = "../../controller/administracion/c_llamadas_ajax.php";
      var sendData = {'op' : 3, 'action' : 13, 'idSegmento':<?php echo isset($id) ? $id : null ?>, 'nombre': $("#nombreSegmento").val(), 'descripcion': $("#descripcionSegmento").val(), 'datos': datos };
      var beforeFunction = function(){
          
      };
      var successFunction = function(data){
        if(data){
          if(data.result == 1){        
            showNotification("bg-green", "¡Segmento Actualizado!", "bottom", "center", null, null);
            backLink();
          }else{
            showNotification("bg-red", "Error de actualización", "bottom", "center", null, null);
          }
        }else{
          showNotification("bg-red", "¡Error de Conexión. Intenta nuevamente!", "bottom", "center", null, null);
        }
      };
      var failFunction = function(data){
        showNotification("bg-red", "¡Ocurrio un problema de conexion, intenta nuevamente!", "bottom", "center", null, null);
      };
      connectServer(link,sendData,beforeFunction,successFunction,failFunction);
      
    });

    function showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
      //console.log("click");
      if (colorName === null || colorName === '') { colorName = 'bg-black'; }
      if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
      if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
      if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
      var allowDismiss = true;

      $.notify({
          message: text
      },{
        type: colorName,
        allow_dismiss: allowDismiss,
        newest_on_top: true,
        timer: 1000,
        placement: {
            from: placementFrom,
            align: placementAlign
        },
        animate: {
            enter: animateEnter,
            exit: animateExit
        },
        template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
        '<span data-notify="icon"></span> ' +
        '<span data-notify="title">{1}</span> ' +
        '<span data-notify="message">{2}</span>' +
        '<div class="progress" data-notify="progressbar">' +
        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
        '</div>' +
        '<a href="{3}" target="{4}" data-notify="url"></a>' +
        '</div>'
      });
    }

    function connectServer(link, data, beforeFunction, successFunction, failFunction){
      $.ajax({
        type: "POST",
        timeout: 15000,
        url: link,
        data: data,
        dataType: "json",
        beforeSend : function(){
            beforeFunction && beforeFunction();
        }
      }).done(function(data){
          successFunction && successFunction(data);
          console.log(data);
      }).fail(function(data){
          failFunction && failFunction(data);
          console.log(data);
      });
    }


</script>

<?php }?>