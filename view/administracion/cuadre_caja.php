
<?php 
/*************************************************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#- Vista de Registro -#
##################################################################################### 
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
$ins_funciones=new Funciones_Basicas();
session_start();
if(isset($_SESSION["usuario"])){ 
//VALIDAMOS SI ES ADMINISTRADOR O PV
$id_sess=$_SESSION["id_usuario"];
$qr_rol_usu=$ins_funciones->consulta_generica_all('select * from tbl_usuario  where  id_usuario='.$id_sess);
$res_rol_usu=$qr_rol_usu->fetch_assoc();
?>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Cuadre de Caja</h2>
                        </div>
                        <div class="body">
						 <div class="row clearfix">
								<div class="col-sm-6"> 			
									<div class="input-group">                        
										<span class="input-group-addon">
											<i class="material-icons">date_range</i>
										</span>
										<div class="form-line">
											<input id="desde" type="text" class="form-control date datetimepicker" placeholder="Desde" /><span id="textDesde" style="color:red"></span>
										</div>
									</div>
								</div>
								<div class="col-sm-6"> 
									<div class="input-group"> <br>                      
										<span class="input-group-addon">
											<i class="material-icons">date_range</i>
										</span>
										<div class="form-line">
											<input id="hasta" type="text" class="form-control date datetimepicker" placeholder="Hasta" /><span id="textHasta" style="color:red"></span>
										</div>
									</div>
								</div>
							</div>		
                            <div class="row clearfix">
                                <div class="col-sm-6">
								<h4><i class="fas fa-store-alt"></i> Establecimiento</h4>
                                   <select id="establecimientos" class="form-control show-tick" data-live-search="true">
                                        <option value="">-- Selecciona el establecimiento --</option>
										<?php  
										if($res_rol_usu['rol']==1){//ADMINISTRADOR
											$id_usua='';
											$cad='<option value="*">Todos</option>';
												}else{
												$id_usua=' and id_usuario='.$res_rol_usu['id_usuario'];		
												$cad='';
												}
											$qr_usu=$ins_funciones->consulta_generica_all('select * from tbl_usuario  where rol=2 '.$id_usua.' and activo=0 order by usuario asc');
											while($fila = $qr_usu->fetch_assoc()){ ?>
											 <option value="<?php echo  utf8_encode($fila['id_usuario']); ?>"><?php echo  utf8_encode($fila['usuario']); ?></option>
										<?php } ?>
                                    </select>
                                </div>
                            </div>
							 <div class="row clearfix">
                                <div class="col-sm-6">
									<button type="button" class="previous-form btn btn-info " id="genera_cuadre"> Generar Reporte</button>
								</div>
							</div>
							<div class="load">
								<div class="preloader">
									<div class="spinner-layer pl-red">
										<div class="circle-clipper left">
											<div class="circle"></div>
										</div>
										<div class="circle-clipper right">
											<div class="circle"></div>
										</div>
									</div>
								</div>
								<p>Procesando...</p>
							</div>
							<div class="row clearfix" id='resp_cuadre' ></div>
                        </div>
                    </div>
                </div>
            </div>
<script src="../../inc/plugins/node-waves/waves.js"></script>
<script src="../../inc/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="../../inc/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<script src="../../inc/js/pages/tables/jquery-datatable.js"></script>
<script src="../../inc/plugins/momentjs/moment.js"></script>
<script src="../../inc/plugins/momentjs/moment-with-locale.js"></script>
<script src="../../inc/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src="../../inc/js/cuadre_caja.js"></script>
 <?php 
 }else{
      $redirec= "../../" ;
      header('Location:'.$redirec );
 }