
<?php 
/*************************************************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#- Vista de Registro -#
##################################################################################### 
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
include_once '../../controller/administracion/c_admin_usu.php';
$ins_control_admin_usu=new C_admin_usu($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
session_start();
if(isset($_SESSION["usuario"])){ 
?>
<script src="../../inc/js/admin_usuarios.js"></script>
 <button onclick="toggles();" type="button" name="previous" class="previous-form btn btn-info " id='muestra_form_add_usu'><i class="fas fa-user-plus"></i> Agregar Usuario </button><br><br>
<div class="card" id='form_add_usuario'>    
    <div class="body">
	<h4 class="text_color"><i class="fas fa-users"></i> Administración de Usuarios<br><small>Ingrese los campos necesarios para dar de alta a un nuevo usuario</small></h4>
           
        <form  onsubmit="addusers(); return false;">
             <hr>
            <div class="input-group" style="padding:10px;">
                <div class="form-line">
                    <input type="text" class="form-control"  name='nom_usu' placeholder="Nombre de Usuario" id='usuario'>
                </div><br>
                <div class="form-line">
                     &nbsp;<input type="password" class="form-control"  name='pass_usu' placeholder="Password" id='password'>
                </div><br>
                    Tipo de Usuario:
                <div class="demo-radio-button">
                        <input name="tipo_usu" type="radio" id="tipo_usu_1"  value='1'/>
                        <label for="tipo_usu_1">Administrador</label>
                        <input name="tipo_usu" type="radio" id="tipo_usu_2" value='2'/>
                        <label for="tipo_usu_2">Punto de Venta</label>
                </div>
				<div id='tipo_pv'>
					<select id='tip_pv' class="form-control show-tick">
						<option value="">Selecciona el tipo de Venta</option>           
						<?php  
							$res=$ins_funciones->consulta_generica_all('select * from tbl_tipo_punto_venta');
							while ($reg= mysqli_fetch_assoc($res)) { ?>
								<option value="<?php  echo $reg['id_tipo_punto_venta'];?>"><?php  echo $reg['nombre'];?></option>
							<?php } ?>
					</select>
				</div>
            </div>
            <div id='loading'></div>
            <button class="btn btn-block btn-lg btn_color waves-effect" onclick="sendform();" name="submit-add">Registrar Usuario</button>
        </form>
	</div>
</div>
 <div class="card">    
    <div class="body">
        <div>
            <h3>Tabla de Usuarios</h3>
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                    <thead>
                        <tr>
                            
                            <th>Id </th>
                            <th>Nombre </th>
                            <th>Rol</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                <?php 
                    //GENERAMOS CONTENIDO DE TABLA
                   $res=$ins_control_admin_usu->trae_usuarios();
                        while($fila = $res->fetch_assoc()){ ?>
                           <tr>
                                <td><?php echo $fila['id_usuario']; ?></td>
                                <td><?php  echo utf8_encode($fila['usuario']); ?></td>
                                <td><?php echo utf8_encode($fila['nombre']); ?></td>
                                <td><button type="button" class="btn btn-primary " onclick="editusu('<?php echo $fila['id_usuario']; ?>')"><i class="far fa-edit"></i> </button></td>                                   
                                <td>
                                <button id="<?php echo $fila['id_usuario']; ?>" type="button" class="btn btn-danger elimina_usu" value='' onclick="delusu(this.id);"><i class="far fa-trash-alt"></i></button></td>                                   
                            </tr>
                       <?php  } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
function sendform(){
    var form = $('#form_add_usuario');
    $('#form_add_usuario').find('[type="submit"]').trigger('click');
}


</script>     

 <?php }else{
      $redirec= "../../" ;
      header('Location:'.$redirec );
 }