<?php 
include_once '../../controller/administracion/c_admin_menu.php';
$select = new C_admin_menu($ser,$usu,$pas,$bd);
?>
<script src="../../inc/js/admin_menu.js"></script>

    <div class="card">
        <div class="body">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="header">
                        <h2 class="text_color"><i class="fas fa-utensils"></i> Registro de Elemento de Menú <small>Agrega un Elemento a Menú</small></h2>
                    </div>
                </div>    
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <button class="btn btn-block btn-lg btn_color waves-effect" onclick="menu('administracion/edit_menu')">Editar Menú</button>
                </div>
            </div>
            <form id="addmenu" onsubmit="addmenu(); return false;">
                <div class="row">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <!--i class="material-icons">person</i-->
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" id="element" name="element" placeholder="* Nombre del Elemento del Menú" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <!--i class="material-icons">person</i-->
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" id="description" name="description" placeholder="* Descripción" required>
                        </div>
                    </div>
                    <div class="input-group">
                        <div class="form-line">
                            <select id="cat" name="cat" onchange="" class="form-control show-tick" required>
                                <option value="">Elige una Categoria</option>
                                <?php echo $select->selectcat(); ?>
                            </select>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <!--i class="material-icons">person</i-->
                        </span>
                        <div class="form-line">
                            <input type="number" class="form-control" id="price" step="0.01" name="price" placeholder="* Precio" required>
                        </div>
                    </div>
                </div>
                    

                
                <input type="submit" value="Enviar" class="hide" />
            </form>

            <form action="/" id="frmFileUpload" class="dropzone needsclick dz-clickable" method="post" enctype="multipart/form-data">
                <div class="dz-message needsclick">
                    <div class="drag-icon-cph">
                        <i class="material-icons">touch_app</i>
                    </div>
                    <div>
                        Arrastra tu imagen o haz click para agregarla
                    </div>
                </div>
                <div class="fallback">
                    <input id="file" name="file" type="file" />
                </div>
            </form>
            <input id="filename" type="hidden" name="filename">

            <button class="btn btn-block btn-lg btn_color waves-effect" onclick="sendform();" id="submit-lead" name="submit-lead" disabled="true">Agregar</button>
        </div>
    </div>
<!-- IMPORTANT CONTENT-->
<!-- Dropzone Plugin Js -->
<script src="../../inc/plugins/dropzone/dropzone.js"></script>
<!-- END IMPORTANT -->
<script>

    Dropzone.options.frmFileUpload = {
        maxFilesize: 2,
        acceptedFiles: "image/jpeg,image/png",

        accept: function(file, done) {
            console.log("uploaded");
            done();
            var filename = file.name;
            $('#filename').val(filename);
            $('#submit-lead').prop( 'disabled',false);
            
            
        },
        init: function() {
            this.on("addedfile", function() {
              if (this.files[1]!=null){
                this.removeFile(this.files[0]);
              }
            });
        }
    };

    $("form#frmFileUpload").dropzone({
        url: "../administracion/uploadmenu.php"
    });

        function sendform(){
            var form = $('#addmenu');
            $('#addmenu').find('[type="submit"]').trigger('click');
        }
    </script>