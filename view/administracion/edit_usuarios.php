<?php
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
include_once '../../controller/administracion/c_admin_usu.php';
$ins_control_admin_usu=new C_admin_usu($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
$id = $_POST['id'];
session_start();
if(isset($_SESSION["usuario"])){ 
?>
<script src="../../inc/js/edit_usuarios.js"></script>
<!--button type="button" name="" class="previous-form btn btn-info " onclick="shownew('newpromo')"><i class="fas fa-plus"></i> Agregar Nuevo Nivel </button><br><br-->
<button class="btn btn-info" onclick="menu('administracion/admin_usuarios')"><i class="fas fa-angle-double-left"></i> Volver a lista</button>
<br>
<br>
<div class="card" id='form_add_usuario'>    
    <div class="body">
	<h4 class="text_color"><i class="fas fa-users"></i> Edición de Usuarios<br><small>Edite un Nuevo Usuario</small></h4>
        <form  onsubmit="editusers('<?php echo $id; ?>'); return false;">
            <hr>
            <div class="input-group" style="padding:10px;">
                <?php 
                    echo $ins_control_admin_usu->grideditable($id);
                ?>
                <div id='tipo_pv'>
                    <select id='tip_pv' class="form-control show-tick">
                        <option value="">Selecciona el tipo de Venta</option>           
                        <?php  
                            $res=$ins_funciones->consulta_generica_all('select * from tbl_tipo_punto_venta');
                            while ($reg= mysqli_fetch_assoc($res)) { ?>
                                <option value="<?php  echo $reg['id_tipo_punto_venta'];?>"><?php  echo $reg['nombre'];?></option>
                            <?php } ?>
                    </select>
                </div>
            </div>
            <div id='loading'></div>
            <button class="btn btn-block btn-lg btn_color waves-effect" onclick="sendform();" name="submit-add">Editar Usuario</button>
        </form>    
	</div>
</div>
<script>
function sendform(){
    var form = $('#form_add_usuario');
    $('#form_add_usuario').find('[type="submit"]').trigger('click');
}


</script>     

 <?php }else{
      $redirec= "../../" ;
      header('Location:'.$redirec );
 }