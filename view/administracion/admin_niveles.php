<?php
include_once '../../controller/administracion/c_admin_nivel.php';
$type = new C_admin_nivel($ser,$usu,$pas,$bd);
$grid = new C_admin_nivel($ser,$usu,$pas,$bd);
?>
<script src="../../inc/js/admin_niveles.js"></script>
<script src="../../inc/js/edit_niveles.js"></script>
<button type="button" name="" class="previous-form btn btn-info " onclick="shownew('newlvl')"><i class="fas fa-plus"></i> Agregar Nuevo Nivel </button><br><br>
<input id="range" type="hidden" value="<?php echo $type->minrange();?>">
<div class="card" id="newlvl" style="display: none;">
    <div class="body">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="header">
                    <h2 class="text_color"><i class="fas fa-star"></i> Registro de Nuevo Nivel <small>Agrega un Nivel</small></h2>
                </div>
            </div>
            <!--div class="col-sm-12 col-md-6 col-lg-6">
                <button class="btn btn-block btn-lg btn_color waves-effect" onclick="menu('administracion/edit_niveles')">Editar Nivel</button>
            </div-->
        </div>
        <form id="adduser" onsubmit="addlvl(); return false;">
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">person</i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control" id="name" name="name" placeholder="* Nombre del Nivel" required autofocus>
                </div>
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons">person</i>
                </span>
                <h5>* Tipo</h5>
                <div class="form-line">
                    <select id="type" name="type" class="form-control show-tick" required>
                        <?php
                            $type->selectype();
                        ?>    
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <h5>* De:</h5>
                        <div class="form-line">
                            <input type="text" value="" class="form-control" id="de" name="de" pattern="[0-9]+" placeholder="De: " disabled required>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <h5>* Hasta</h5>
                        <div class="form-line">
                            <input type="text" class="form-control" id="hasta" name="hasta" pattern="[0-9]+" placeholder="Hasta:" required>
                        </div>
                    </div>
                </div>
            </div>
            
            <button class="btn btn-block btn-lg btn_color waves-effect" onclick="sendform();" name="submit-add">Agregar</button>

        </form>
        <hr/>
    </div>
</div>
<script>
    var minrange = $('#range').val();
    minrange = parseInt(minrange);
    if(isNaN(minrange)) {
        minrange = 1;
    }else{
        minrange = minrange+1;
    }
    
    $('#de').val(minrange);
    $('#de').attr('min',minrange);
    $('#de').attr('max',minrange);

    function sendform(){
        var form = $('#addlvl');
        $('#addlvl').find('[type="submit"]').trigger('click');
    }
</script>
<!-- Editable -->    
<div class="card">
    <div class="body">
    <!-- Edit Menù table -->
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="header">
                    <h2 class="text_color"><i class="fas fa-star"></i> Edición de Niveles <small>Edita un Nivel</small></h2>
                </div>
            </div>
            <!--div class="col-sm-12 col-md-6 col-lg-6">
                <button class="btn btn-block btn-lg btn_color waves-effect" onclick="menu('administracion/admin_niveles')">Nuevo Nivel</button>
            </div-->
        </div>
        <div class="table-responsive">
            <table id="mainTable" class="table table-bordered table-striped table-hover dataTable js-exportable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>De</th>
                        <th>Hasta</th>
                        <th></th>
                        <th>Inactivo/Activo</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        echo $grid->grideditl('tr');
                    ?>
                </tbody>
            </table>
        </div>
        <!-- #END# Exportable Table -->
    </div>   
</div>
<?php
 include '../../inc/datatables.php';
?>
<script>
    $('.ifcheck').change(function() {
        if($(this).is(":checked")) {
            var idx = $(this).attr('id');
            var id = idx.replace("x", "");
            activelvl(id);
        }else{
            var idx = $(this).attr('id');
            var id = idx.replace("x", "");
            inactivelvl(id);
        }        
    });
</script>