<?php 

?>
<script src="../../inc/js/admin_categoria.js"></script>

    <div class="card">
        <div class="body">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="header">
                        <h2 class="text_color"><i class="fas fa-project-diagram"></i> Registro de Nueva Categoría <small>Agrega un Elemento a Categoría</small></h2>
                    </div>
                </div>    
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <button class="btn btn-block btn-lg btn_color waves-effect" onclick="menu('administracion/edit_categorias')">Editar Categoría</button>
                </div>
            </div>
            <form id="addcat" onsubmit="addcat(); return false;">
                <div class="row">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <!--i class="material-icons">person</i-->
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" id="cat" name="cat" placeholder="* Nombre del Elemento de la Categoría" required autofocus>
                        </div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <!--i class="material-icons">person</i-->
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" id="descript" name="description" placeholder="* Descripción" required>
                        </div>
                    </div>
                </div>
                    

                
                <input type="submit" value="Enviar" class="hide" />
            </form>

            <button class="btn btn-block btn-lg btn_color waves-effect" onclick="sendform();" id="submit-lead" name="submit-lead">Agregar</button>
        </div>
    </div>
<!-- IMPORTANT CONTENT-->
<script>
    function sendform(){
        var form = $('#addcat');
        $('#addcat').find('[type="submit"]').trigger('click');
    }
</script>