<?php
    include_once '../../inc/parametros.php';
    include_once '../../controller/administracion/c_admin_segmento.php';
 
    $instanceSegmento=new C_admin_segmento($ser,$usu,$pas,$bd);
    $segmentos = $instanceSegmento->getArraySegmentos(NULL);
    $categorias = $instanceSegmento->getCategoriasSegmento(NULL);
    
    //print_r($categorias);
    //die();
    session_start();
    if(isset($_SESSION["usuario"])){ 
?>


<div class="content">
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>                
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h4 class="text_color"><i class="fas fa-chart-pie"></i> Administración de Segmentos</h4>
    </div>
</div>
<div class="row clearfix listSegmentos">                
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <button type="button" name="previous" class="previous-form btn btn-info " id='muestra_form_add_usu'><i class="fas fa-plus"></i> Agregar Nuevo Segmento </button><br><br>
    </div>
    <div class="w-100"></div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div id="form_add_segmento" class="card">
            <div class="header">                        
                <h2 class="text_color"><i class="fas fa-plus-circle"></i> Nuevo Segmento</h2>
            </div>
            <div class="body">
                <form id="formSegmento">
                    <fieldset class="fieldset" style="padding: 8px; border: 1px solid lightgray;">
                        <legend style="border-bottom:none; padding:5px; width:auto">Información Básica</legend>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">spellcheck</i>
                            </span>
                            <div class="form-line">
                                <input type="text" class="form-control" name='nombre_segmento' placeholder="Nombre del Segmento (Obligatorio)" id='nombreSegmento' required>                                    
                            </div>
                            <span id="labelNombre" style="color:red; font-weight:bold"></span>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">subject</i>
                            </span>
                            <div class="form-line">
                                <input type="text" class="form-control" name='descripcion_segmento' placeholder="Descripción del Segmento (Opcional)" id='descripcionSegmento'>                                    
                            </div>                            
                        </div>
                    </fieldset>
                    <fieldset class="fieldset" style="padding: 8px; border: 1px solid lightgray;">
                        <legend style="border-bottom:none; padding:5px; width:auto">Configuración del Segmento</legend>
                        <div class="card" style="background-color: #eaeaea; border: 1px solid #c7c7c7;">
                            <div class="body">
                                <div id="boxSegmento">                                                                        
                                </div>
                                <div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right"><br><a href="javascript:;" class="btn btn_color btnANDCondition">Agregar condición AND</a></div></div>
                            </div>
                        </div>                    
                    </fieldset>
                    <div class="input-group">
                        <div id='loading'><img src="../../inc/imagenes/load.gif"></div>
                        <button type="submit" class="btn m-t-15 waves-effect btn_color"> Crear Segmento </button>
                        <!--button id='btnNewSegmento' type="button" class="btn m-t-15 waves-effect btn_color" >Crear Segmento</button-->
                    </div>
                </form>
            </div>                  
        </div>
    </div>                
</div>

<!-- GRID -->
<div class="row listSegmentos">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2 class="text_color"><i class="fas fa-list-ul"></i> Lista de Segmentos</h2>
            </div>
            <div class="body">
                <div class="table-responsive">                
                    <table id="tableSegmentos" class="table table-bordered table-striped table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Descripción</th>
                                <th>Clientes Inscritos</th>
                                <th>Status</th>
                                <th>Acciones</th>
                            </tr>                                    
                        </thead>
                        <tbody>
                            <?php foreach ($segmentos as $key) { 
                                $conteo = (int)$key['countClients'][0]['countClients'];
                            ?>
                            <tr>
                                <td><?php echo $key['idSegmento'];  ?></td>
                                <td><?php echo utf8_encode($key['nombreSegmento']);  ?></td>
                                <td><?php echo utf8_encode($key['descripcionSegmento']);  ?></td>
                                <td><?php echo $conteo; ?></td>
                                <td><?php echo $key['activo'] == 0 ? "Activo" : "Inactivo";  ?></td>
                                <td><a href="javascript:;" class="btn btnRefreshSegmento" style="background-color:green; color:white" data-action="<?php echo $key['idSegmento'] ?>" data-toggle="tooltip" data-placement="top" data-placement-from="bottom" title="Actualizar Segmento" data-placement-align="center" data-animate-enter="" data-animate-exit="" data-color-name="bg-green"><div><i class="fas fa-sync-alt"></i></div></a> <a href="javascript:;" class="btn btn-primary btnViewSegmento" data-action="<?php echo $key['idSegmento'] ?>" data-toggle="tooltip" data-placement="top" title="Editar Segmento"><i class="fas fa-pen"></i></a>&nbsp;<a class="btn btn-danger btnDeleteSegmento" href="javascript:;" data-value="<?php echo $key['idSegmento'] ?>" data-toggle="tooltip" data-placement="top" title="Eliminar Segmento"><i class="fas fa-trash-alt"></i></a><?php if($conteo > 0){?>&nbsp;<a class="btn btn_color btnUsersSegmento" data-toggle="tooltip" data-placement="top" title="Ver Usuarios" style="color:white; width:auto" href="javascript:;" data-seg="<?php echo $key['idSegmento'] ?>" ><i class="fas fa-eye"></i></a><?php } ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>                                
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="../../inc/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="../../inc/js/pages/tables/jquery-datatable.js"></script>


<!-- Moment Plugin Js -->
<script src="../../inc/plugins/momentjs/moment.js"></script>
<script src="../../inc/plugins/momentjs/moment-with-locale.js"></script>

<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="../../inc/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src="../../inc/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<script src="../../inc/plugins/bootstrap-notify/bootstrap-notify.js"></script>

<!-- noUISlider Plugin Js -->
<script src="../../inc/plugins/nouislider/nouislider.js"></script>

<!--script src='../../inc/js/edit_segmento.js'></script-->

<script src='../../inc/js/box_segmento.js'></script>
<script src='../../inc/js/categoria.js'></script>
<script src='../../inc/js/admin_segmento.js'></script>


<?php }?>