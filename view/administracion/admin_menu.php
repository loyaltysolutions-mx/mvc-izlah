<?php
include_once '../../controller/administracion/c_admin_menu.php';
$select = new C_admin_menu($ser,$usu,$pas,$bd);
$grid = new C_admin_menu($ser,$usu,$pas,$bd);
?>
<script src="../../inc/js/admin_menu.js"></script>
<script src="../../inc/js/edit_menu.js"></script>
<div class="row">    
    <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="pull-left">
            <button type="button" name="" class="previous-form btn btn-info " onclick="shownew('newmenu')"><i class="fas fa-plus"></i> Agregar Nuevo a Menú </button><br><br>
        </div>
        <div class="pull-right">
            <a href="javascript:;" class="btn btn-info" id="btnLoadMasive"><i class="fas fa-file-excel"></i> Importar Menú desde Archivo </a>
        </div>
    </div>
</div>

<div id="loadMasivoMenu" class="card" style="display: none;">
    <div class="body">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="header">
                    <h2 class="text_color"><i class="fas fa-utensils"></i> Carga Masiva de Menú <small>Agrega elementos al Menú</small></h2>
                </div>
            </div>    
            <!--div class="col-sm-12 col-md-6 col-lg-6">
                <button class="btn btn-block btn-lg btn_color waves-effect" onclick="menu('administracion/edit_menu')">Editar Menú</button>
            </div-->
        </div>
        <form id="formLoadMenu" enctype="multipart/form-data">            
            <div id="dropzoneLoad" class="dropzone needsclick dz-clickable">
                <div class="dz-message needsclick">
                    <div class="drag-icon-cph">
                        <i class="material-icons">attach_file</i>
                    </div>
                    <h5>
                        Arrastra tu archivo aquí o click para seleccionar
                    </h5>
                </div>
                <div class="fallback">
                    <input id="img_menu" name="img_menu" type="file" required />
                </div>
            </div>                
            <button class="btn btn-block btn-lg btn_color waves-effect" type="submit">Agregar</button>
        </form>
    </div>
</div>


<div id="newmenu" class="card" style="display: none;">
    <div class="body">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="header">
                    <h2 class="text_color"><i class="fas fa-utensils"></i> Registro de Elemento de Menú <small>Agrega un Elemento a Menú</small></h2>
                </div>
            </div>
            <!--div class="col-sm-12 col-md-6 col-lg-6">
                <button class="btn btn-block btn-lg btn_color waves-effect" onclick="menu('administracion/edit_menu')">Editar Menú</button>
            </div-->
        </div>
        <form id="addmenu" enctype="multipart/form-data">
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <!--i class="material-icons">person</i-->
                    </span>
                    <div class="form-line">
                        <input type="text" class="form-control" id="element" name="element" placeholder="* Nombre del Elemento del Menú" required autofocus>
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <!--i class="material-icons">person</i-->
                    </span>
                    <div class="form-line">
                        <input type="text" class="form-control" id="description" name="description" placeholder="* Descripción" required>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <select id="cat" name="cat" class="form-control show-tick" required>
                            <option value=""> -- Elige una Categoría --</option>
                            <?php echo $select->selectcat(); ?>
                        </select>
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <!--i class="material-icons">person</i-->
                    </span>
                    <div class="form-line">
                        <input type="number" class="form-control" id="price" step="0.01" name="price" placeholder="* Precio" required>
                    </div>
                </div>
            </div>
            <div id="dropzoneMenu" class="dropzone needsclick dz-clickable">
                <div class="dz-message needsclick">
                    <div class="drag-icon-cph">
                        <i class="material-icons">attach_file</i>
                    </div>
                    <h5>
                        Arrastra tu archivo aquí o click para seleccionar
                    </h5>
                </div>
                <div class="fallback">
                    <input id="img_menu" name="img_menu" type="file" required />
                </div>
            </div>                
            <button class="btn btn-block btn-lg btn_color waves-effect" type="submit">Agregar</button>
        </form>
    </div>
</div>
<!-- IMPORTANT CONTENT-->
<!-- Dropzone Plugin Js -->
<script src="../../inc/plugins/dropzone/dropzone.js"></script>
<!-- END IMPORTANT -->
<!--script>

    Dropzone.options.frmFileUpload2 = {
        maxFilesize: 2,
        acceptedFiles: "image/jpeg,image/png",

        accept: function(file, done) {
            console.log("uploaded");
            done();
            var filename = file.name;
            $('#filename2').val(filename);
            $('#submit-lead2').prop( 'disabled',false);
            
            
        },
        init: function() {
            this.on("addedfile", function() {
              if (this.files[1]!=null){
                this.removeFile(this.files[0]);
              }
            });
        }
    };

    $("form#frmFileUpload2").dropzone({
        url: "../administracion/uploadmenu.php"
    });

    function sendform(){
        var form = $('#addmenu');
        $('#addmenu').find('[type="submit"]').trigger('click');
    }
</script-->
<script>
    var dropzoneRecompensa = new Dropzone("#dropzoneMenu",{
        url: "../administracion/uploadmenu.php",
        autoProcessQueue: false,
        paramName: "img_menu", // The name that will be used to transfer the file
        maxFilesize: 1, // MB
        maxFiles: 1, 
        addRemoveLinks: true,
        acceptedFiles: ".jpg,.jpeg,.png,.gif",
        init: function(){
            var _this = this;
            $("#addmenu").on("submit", function(e){
                e.preventDefault();
                _this.processQueue();            
            });
        },
        success: function(data){
            console.log(data);
        }
    });

    dropzoneRecompensa.on("sending", function(file, xhr, data){
        data.append("element", $("#element").val());
        data.append("description", $("#description").val());
        data.append("category", $("#cat").val());
        data.append("price", $("#price").val());
        data.append("op",1);
        //console.log("sending");
    });

    dropzoneRecompensa.on("error", function(file, data){
        dropzoneRecompensa.removeFile(file);
        //showNotification("bg-red", "Archivo inválido", "bottom", "center", null, null);
    });

    dropzoneRecompensa.on("success", function(file, data){
        var data = JSON.parse(data);
        
        if(data.result){
            swal('Éxito', 'Se ha subido correctamente el registro', 'success');
            $("#card-content").load("../../view/administracion/admin_menu.php");
        }else{
            swal('Error', 'Error al subir el registro', 'error');
        }
    });

    $("#btnLoadMasive").click(function(){
        $("#loadMasivoMenu").toggle(600);
    });
</script>

<script>
    var dropzoneFileMenu = new Dropzone("#dropzoneLoad",{
        url: "../administracion/uploadmenu.php",
        autoProcessQueue: false,
        paramName: "file_menu", // The name that will be used to transfer the file
        maxFilesize: 1, // MB
        maxFiles: 3, // MB
        addRemoveLinks: true,
        acceptedFiles: ".xls,.xlsx",
        init: function(){
            var _this = this;
            $("#formLoadMenu").on("submit", function(e){
                e.preventDefault();
                _this.processQueue();            
            });
        },
        success: function(data){
            console.log(data);
        }
    });

    dropzoneFileMenu.on("sending", function(file, xhr, data) {        
        data.append("op", 2);
        //console.log("sending");
    });

    dropzoneFileMenu.on("error", function(file, data){
        dropzoneFileMenu.removeFile(file);
        //showNotification("bg-red", "Archivo inválido", "bottom", "center", null, null);
    });

    dropzoneFileMenu.on("success", function(file, data){
        var data = JSON.parse(data);
        
        if(data.result){
            swal('Éxito', 'Se ha subido correctamente el archivo', 'success');
            $("#card-content").load("../../view/administracion/admin_menu.php");
        }else{
            swal('Error', 'Error al subir el archivo', 'error');
        }
        
    });


</script>
<div class="card">
    <div class="body">
    <!-- Edit Menù table -->
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="header">
                    <h2 class="text_color"><i class="fas fa-utensils"></i> Edición de Menú <small>Edita el Menú</small></h2>
                </div>
            </div>
            <!--div class="col-sm-12 col-md-6 col-lg-6">
                <button class="btn btn-block btn-lg btn_color waves-effect" onclick="menu('administracion/admin_menu')">Nuevo Menú</button>
            </div-->
        </div>
        <div class="table-responsive">
            <table id="mainTable" class="table table-bordered table-striped table-hover dataTable js-exportable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Categoría</th>
                        <th>Precio</th>
                        <th>Imagen</th>
                        <th>Imagen</th>
                        <th>Editar</th>
                        <th>Inactivo/Activo</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        echo $grid->grideditm('tr');
                    ?>
                </tbody>
            </table>
        </div>
        <!-- #END# Exportable Table -->
        <!-- #Modal -->
        <div class="modal fade" id="editimg" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Edición de imagen</h4>
                    </div>
                    <div class="modal-body">
                        <!--form action="/" id="frmFileUpload" class="dropzone needsclick dz-clickable" method="post" enctype="multipart/form-data">
                            <div class="dz-message needsclick">
                                <div class="drag-icon-cph">
                                    <i class="material-icons">touch_app</i>
                                </div>
                                <div>
                                    Arrastra tu imagen o haz click para agregarla
                                </div>
                            </div>
                            <div class="fallback">
                                <input id="files" name="files" type="file" />
                            </div>
                        </form-->
                        <form id="formUpdateImg">
                            <div id="dropzoneUpdateImg" class="dropzone needsclick dz-clickable">
                                <div class="dz-message needsclick">
                                    <div class="drag-icon-cph">
                                        <i class="material-icons">attach_file</i>
                                    </div>
                                    <h5>
                                        Arrastra tu archivo aquí o click para seleccionar
                                    </h5>
                                </div>
                                <div class="fallback">
                                    <input id="img_update" name="img_update" type="file" required />
                                </div>
                            </div>
                            <input id="idMenu" type="hidden" name="idMenu" />
                            <button class="btn btn-block btn-lg btn_color waves-effect" type="submit">Actualizar Imagen</button>
                        </form>
                    <!--input id="filename" type="hidden" name="filename"-->  
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- #End Modal -->
    </div>   
</div>
<?php
 include '../../inc/datatables.php';
?>
<script src="../../inc/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="../../inc/plugins/dropzone/dropzone.js"></script>


<!--script>
    //Dropzone config by Allan M.
    Dropzone.options.frmFileUpload = {
        maxFilesize: 2,
        acceptedFiles: "image/jpeg,image/png",

        removedfile: function(file) {
            this.on("dragstart", function(file) { 
               this.removeAllFiles(true); 
            });
        },

        accept: function(file, done) {
            
            console.log("uploaded");
            done();
            var sendid = $('#filename').attr('my');
            setfile(sendid,file.name,1);
            var filename = file.name;
            $('#filename').val(filename);
            
        },
        init: function() {
            this.on("addedfile", function() {
              if (this.files[1]!=null){
                this.removeFile(this.files[0]);
              }
            });
        }
    };
    
    $("form#frmFileUpload").dropzone({ 
        url: "../administracion/uploadmenu.php"
    });

    /*$(function () {

        $('input').colorpicker({
            format: 'hex'
        });

        $('.colorpicker-with-alpha').remove();
        $('.colorpicker-alpha').remove();
    });
    

    $('.ifcheck').change(function() {
        if($(this).is(":checked")) {
            var idx = $(this).attr('id');
            var id = idx.replace("x", "");
            activemnu(id);
        }else{
            var idx = $(this).attr('id');
            var id = idx.replace("x", "");
            inactivemnu(id);
        }        
    });*/
</script-->
<script>
    var dropzoneUpdateImg = new Dropzone("#dropzoneUpdateImg",{
        url: "../administracion/uploadmenu.php",
        autoProcessQueue: false,
        paramName: "img_menu", // The name that will be used to transfer the file
        maxFilesize: 1, // MB
        maxFiles: 1, // MB
        addRemoveLinks: true,
        acceptedFiles: ".jpeg, .jpg, .png, .gif",
        init: function(){
            var _this = this;
            $("#formUpdateImg").on("submit", function(e){
                e.preventDefault();
                _this.processQueue();            
            });
        },
        success: function(data){
            
        }
    });

    dropzoneUpdateImg.on("sending", function(file, xhr, data) {     
        $("#editimg").modal("hide");   
        data.append("idMenu", $("#idMenu").val());
        data.append("op", 3);

        //console.log("sending");
    });

    dropzoneUpdateImg.on("error", function(file, data){
        dropzoneUpdateImg.removeFile(file);
        //showNotification("bg-red", "Archivo inválido", "bottom", "center", null, null);
    });

    dropzoneUpdateImg.on("success", function(file, data){
        
        var data = JSON.parse(data);
        
        if(data.result){
            $("#card-content").html("");
            swal('Éxito', 'Se ha actualizado correctamente la imagen', 'success');
            $("#card-content").load("../../view/administracion/admin_menu.php");
        }else{
            swal('Error', 'Error al subir el archivo', 'error');
        }        
    });

    $(".btnModal").click(function(){
        id = $(this).attr("data-action");
        $("#idMenu").val(id);
        $("#editimg").modal("show");
    });

</script>