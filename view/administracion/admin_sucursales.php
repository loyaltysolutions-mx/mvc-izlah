<?php
include_once '../../controller/administracion/c_admin_sucursales.php';
$grid = new C_admin_sucursales($ser,$usu,$pas,$bd);
?>
<script src="../../inc/js/admin_sucursales.js"></script>
<script src="../../inc/js/edit_sucursales.js"></script>
<?php
    $dif = $grid->revsuc();
    if($dif==1 OR $dif=='1'){
        $bloq = '';
    }else if($dif==0){
        $bloq = 'disabled';
    }  
?>
<button type="button" name="" class="previous-form btn btn-info " onclick="shownew('newsuc')" <?php echo $bloq; ?> ><i class="fas fa-plus"></i> Agregar Nueva Sucursal</button><br><br>
<div class="card" id="newsuc" style="display: none;">
    <div class="body">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="header">
                <h2 class="text_color"><i class="fas fa-store"></i> Registro de Nueva sucursal <small>Agrega una Sucursal</small></h2>
            </div>
            </div>
            <!--div class="col-sm-12 col-md-6 col-lg-6">
                <button class="btn btn-block btn-lg btn_color waves-effect" onclick="menu('administracion/edit_sucursales')">Editar Sucursal</button>
            </div-->
        </div>
        <form id="addsuc" onsubmit="addsuc(); return false;">
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <!--i class="material-icons">person</i-->
                    </span>
                    <div class="form-line">
                        <input type="text" class="form-control" id="suc" name="suc" placeholder="* Nombre de la Sucursal" required autofocus>
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <!--i class="material-icons">person</i-->
                    </span>
                    <div class="form-line">
                        <input type="text" class="form-control" id="address" name="address" placeholder="* Dirección" required>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">
                    <div class="input-group">
                        <span class="input-group-addon">
                            <!--i class="material-icons">person</i-->
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" id="latitud" name="latitud" placeholder="* Latitud" required>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-6">    
                    <div class="input-group">
                        <span class="input-group-addon">
                            <!--i class="material-icons">person</i-->
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" id="longitud" name="longitud" placeholder="* Longitud" required>
                        </div>
                    </div>
                </div>    
                <div class="input-group">
                    <span class="input-group-addon">
                        <!--i class="material-icons">person</i-->
                    </span>
                    <div class="form-line">
                        <input type="text" class="form-control" id="phone" name="phone" placeholder="* Teléfono" required>
                    </div>
                </div>
                <div class="demo-masked-input">
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <b>De:</b>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">access_time</i>
                            </span>
                            <div class="form-line">
                                <input id="open" name="open" type="text" class="form-control time12" placeholder="Ex: 08:00 am">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-6">
                        <b>Hasta:</b>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">access_time</i>
                            </span>
                            <div class="form-line">
                                <input id="close" name="close" type="text" class="form-control time12" placeholder="Ex: 08:00 pm">
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
                
            <input type="submit" value="Enviar" class="hide" />
        </form>

        <button class="btn btn-block btn-lg btn_color waves-effect" onclick="sendform();" id="submit-lead" name="submit-lead">Agregar</button>
    </div>
</div> 
<!-- Edit content -->
<div class="card">
    <div class="body">
    <!-- Edit Menù table -->
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="header">
                    <h2 class="text_color"><i class="fas fa-store"></i> Edición de Sucursales <small>Edita una Sucursal</small></h2>
                </div>
            </div>
            <!--div class="col-sm-12 col-md-6 col-lg-6">
                <button class="btn btn-block btn-lg btn_color waves-effect" onclick="menu('administracion/admin_sucursales')">Nueva Sucursal</button>
            </div-->
        </div>
        <div class="table-responsive">
            <table id="mainTable" class="table table-bordered table-striped table-hover dataTable js-exportable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Categoria</th>
                        <th>Dirección</th>
                        <th>Latitud</th>
                        <th>Longitud</th>
                        <th>Teléfono</th>
                        <th>Apertura</th>
                        <th>Cierre</th>
                        <th>Editar</th>
                        <th>Inactivo/Activo</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        echo $grid->gridedits('tr');
                    ?>
                </tbody>
            </table>
        </div>
        <!-- #END# Exportable Table -->
    </div>   
</div>
<?php
 include '../../inc/datatables.php';
?>
<!-- IMPORTANT CONTENT-->

<!-- Bootstrap Core Js -->
<script src="../../inc/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="../../inc/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Bootstrap Colorpicker Js -->
<script src="../../inc/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>

<!-- Input Mask Plugin Js -->
<script src="../../inc/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>

<!-- Custom Js -->
<script src="../../inc/js/pages/forms/advanced-form-elements.js"></script>
<script>
    $('.mask').inputmask({
        mask: "h:s t\\m - h:s t\\m",
        placeholder: "hh:mm xm - hh:mm xm",
        alias: "datetime",
        hourFormat: "12"
    });
</script>
<script>
function sendform(){
    var form = $('#addsuc');
    $('#addsuc').find('[type="submit"]').trigger('click');
}

$(document).ready(function(){
  $("#open").inputmask();
  $("#close").inputmask();
});

$('.ifcheck').change(function() {
    if($(this).is(":checked")) {
        var idx = $(this).attr('id');
        var id = idx.replace("x", "");
        activesuc(id);
    }else{
        var idx = $(this).attr('id');
        var id = idx.replace("x", "");
        inactivesuc(id);
    }        
});
</script>  