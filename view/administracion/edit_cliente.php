<?php
//LLAMAMOS ESTILOS DE CONFIGURACION
    include_once '../../inc/funciones.php';
    include_once '../../inc/parametros.php';
    include_once '../../controller/administracion/c_admin_segmento.php';
    $instanceSegmento=new C_admin_segmento($ser,$usu,$pas,$bd);
    session_start();
    
    $idUsuario = isset($_POST['idUsuario']) && ( $_SESSION['rol'] == 1 || $_SESSION['id_usuario'] == $_POST['idUsuario'] ) ? trim(addslashes(htmlspecialchars($_POST['idUsuario']))) : null;
    if($idUsuario != null){
        $usuario= $instanceSegmento->getInfoUser($idUsuario);
        if(isset($_SESSION["usuario"])){     
?>
    
<!-- CONTENIDO -->
<?php if(isset($_SESSION["rol"]) && $_SESSION["rol"] == 1){ ?>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="javascript:;" class="btn btn-info btnBack"><i class="fas fa-angle-double-left"></i>&nbsp;Volver a lista</a></div>
</div>
<br>
<?php } ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2><i class="fas fa-user-circle"></i> Información General del Usuario <small>Editar la información del cliente </small></h2>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <form id="formUpdateCliente">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" id='nombreUser' name="nombre" placeholder="Nombre del Cliente" value="<?php echo $usuario['info'][0]['nombre'] ?>">
                                </div>                    
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">wc</i>
                                </span>
                                <div class="form-line">
                                    <select id="selectGenero" name="genero" required class="form-control show-tick">
                                        <option value=""> -- Elige un género -- </option>
                                        <option value="1" <?php echo $usuario['info'][0]['genero'] == 1 ? "selected" : "" ?>> Hombre </option>
                                        <option value="2" <?php echo $usuario['info'][0]['genero'] == 2 ? "selected" : "" ?> > Mujer </option>
                                    </select>
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">mail</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" id='email' value="<?php echo $usuario['info'][0]['email'] ?>"  placeholder="Email" readonly>
                                </div>                    
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">phone</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" id='telefono' value="<?php echo $usuario['info'][0]['telefono'] ?>" placeholder="Celular a 10 digitos" readonly>
                                </div>                    
                            </div>                        
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">cake</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" id='fechaNacimiento' name="fecha_nacimiento" placeholder="Fecha Nacimiento" value="<?php echo $usuario['info'][0]['fecha_nacimiento'] ?>">
                                </div>                    
                            </div>
                            <div id="loadAnimation" style="display:none; width:100%; text-align:center"><div class="lds-dual-ring"></div></div>
                            <input type="hidden" name='idUsuario' value="<?php echo $idUsuario ?>">
                            <button id="btnSubmit" type="submit" class="btn btn_color"> Actualizar información </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="../../inc/plugins/momentjs/moment.js"></script>
<script src="../../inc/plugins/momentjs/moment-with-locale.js"></script>

<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="../../inc/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script type="text/javascript">
    $(".btnBack").click(function(){
        var link = "../../view/administracion/admin_clientes.php";
        var div = '<div class="loader text-center"><div class="preloader"><div class="spinner-layer pl-red"><div class="circle-clipper left"><div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div><p>Please wait...</p></div>';
        var beforeFunction = function(){
            $("#card-content").html(div);
        };
        var successFunction = function(response){    
            $("#card-content").html(response);
        };
        var failFunction = function(data){

        };
        loadPage(link,null,beforeFunction,successFunction,failFunction);
    });

    $("#formUpdateCliente").on("submit", function(e){
        e.preventDefault();
        $("#btnSubmit").fadeOut(300, function(){
            let link = '../../controller/administracion/c_llamadas_ajax.php';
            let data = $("#formUpdateCliente").serialize()+"&op=30";
            let beforeFunction = function(){
                $("#loadAnimation").fadeIn();
            };
            let successFunction = function(data){
                if(data.result == 1){
                    swal("Exito","¡Se ha actualizado correctamente el usuario!","success");
                    <?php if(isset($_SESSION["rol"]) && $_SESSION["rol"] == 1){ ?>
                        $("#card-content").load('../../view/administracion/admin_clientes.php');
                    <?php }else { ?>
                        $("#loadAnimation").fadeOut(500, function(){
                            $("#btnSubmit").fadeIn();
                        });
                    <?php } ?>
                }else if(data.result == 2){
                    $("#loadAnimation").hide();
                    $("#btnSubmit").show();
                    swal("", 'Ocurrio un problema al actualizar al usuario.','error');
                }<?php if(isset($_SESSION["rol"]) && $_SESSION["rol"] == 1){ ?> else{
                    $("#loadAnimation").hide();
                    $("#btnSubmit").show();
                    swal("", 'El usuario ingresado ya existe en la plataforma.','error');
                }<?php } ?>
            };
            let failFunction = function(){
                $("#loadAnimation").hide();
                    $("#btnSubmit").show();
                swal("", 'Ocurrio un problema de conexión.','error');
            };

            connectServer(link, data, beforeFunction, successFunction, failFunction);
        });
    });

    function connectServer(link, data, beforeFunction, successFunction, failFunction){
        $.ajax({
            type: "POST",
            timeout: 8000,
            url: link,
            data: data,
            dataType: "json",
            beforeSend : function(){
                beforeFunction && beforeFunction();
            }
        }).done(function(data){
            successFunction && successFunction(data);
            console.log(data);
        }).fail(function(data){
            failFunction && failFunction(data);
            console.log(data);
        });
    }

    $("#fechaNacimiento").bootstrapMaterialDatePicker({ 
        format : 'YYYY-MM-DD',
        time : false,         
        lang : 'es', 
        weekStart : 1,
        okText : 'Aceptar',  
        cancelText : 'Cancelar'
    });

</script>
<?php } } ?>
 