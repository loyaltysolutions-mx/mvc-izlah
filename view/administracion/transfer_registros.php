
<?php 
/*************************************************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#- Vista de Registro -#
##################################################################################### 
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
$ins_funciones=new Funciones_Basicas();
session_start();
if(isset($_SESSION["usuario"])){ 
?>
   
 <!-- Select -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>Transfererncia de Registros</h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-6">
								<input type='hidden' id='id_usu_session' value='<?php  echo $_SESSION["id_usuario"]?>'>
								<h4><i class="fas fa-user"></i> Usuario Origen</h4>
                                   <select id="usuario_origen" class="form-control show-tick" data-live-search="true">
                                        <option value="">-- Selecciona el Usuario --</option>
                                        <?php 
											$qr_usu=$ins_funciones->consulta_generica_all(' select * from tbl_usuario  where rol=3 and activo=0 order by usuario asc');
										    while($fila = $qr_usu->fetch_assoc()){ ?>
											 <option value="<?php echo  utf8_encode($fila['id_usuario']); ?>"><?php echo  utf8_encode($fila['usuario']); ?></option>
										<?php } ?>
                                    </select>
                                </div>
								 <div class="col-sm-6">
								<h4> <i class="fas fa-user"></i> Usuario Destino</h4>
                                    <select id="usuario_destino" class="form-control show-tick" data-live-search="true">
                                        <option value="">-- Selecciona el Usuario --</option>
                                        <?php 
											$qr_usu=$ins_funciones->consulta_generica_all(' select * from tbl_usuario where rol=3 and activo=0 order by usuario asc');                                            
										    while($fila = $qr_usu->fetch_assoc()){
                                                //echo '<script>console.log('.json_encode($fila).');</script>'
                                             ?>
											 <option value="<?php echo  utf8_encode($fila['id_usuario']); ?>"><?php echo  utf8_encode($fila['usuario']); ?></option>
										<?php } ?>
                                    </select>
                                </div>
                            </div>
							 <div class="row clearfix">
                                <div class="col-sm-6">
									<button type="button" class="previous-form btn btn-info " id="tranfer_info"> Transferir Registros del Usuario</button>
								</div>
							</div>
							<div class="row clearfix" id='resp_transfer'></div>
                        </div>
                    </div>
                </div>
            </div>
<!--script src="../../inc/plugins/bootstrap/js/bootstrap.js"></script-->
<script src="../../inc/js/transfer_registros.js"></script>
 <?php }else{
      $redirec= "../../" ;
      header('Location:'.$redirec );
 }