<?php
    include_once '../../inc/parametros.php';
    include_once '../../controller/administracion/c_admin_segmento.php';
 
    $instanceSegmento=new C_admin_segmento($ser,$usu,$pas,$bd);
    $idSegmento = $_POST['idSegmento'];
    $usuarios = $instanceSegmento->getClientesSegmento($idSegmento, NULL);
    
    session_start();
    if(isset($_SESSION["usuario"]) && count($usuarios) > 0){ 
?>
<!-- GRID -->            
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="javascript:;" class="btn btn-info btnBack"><i class="fas fa-angle-double-left"></i>&nbsp;Volver a lista</a></div>
</div>
<br>
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">                                                    
                <h2 class="text_color"><i class="fas fa-th-list"></i> Clientes registrados en el Segmento</h2>                                    
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table id="tableUsers" class="table table-bordered table-striped table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nombre</th>
                                <th>Segmento</th>
                                <th>Fecha Registro</th>
                                <th>Status</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php while ($row = mysqli_fetch_assoc($usuarios) ){ ?>
                            <tr>
                                <td><?php echo $row['id_usuario']; ?></td>
                                <td><?php echo utf8_encode($row['nombre']); ?></td>
                                <td><?php echo utf8_encode($row['nombre_segmento']); ?></td>
                                <td><?php echo $row['fecha_registro_plataforma']; ?></td>
                                <td><?php echo $row['activo'] == 0 ? 'Activo' : 'Inactivo'; ?></td>
                                <td><a class="btn btn-primary" href="../../view/administracion/view_info_user_segmento.php?id=<?php echo $row['id_usuario']; ?>" target="_blank" onClick="window.open(this.href, this.target, 'width=1000,height=700'); return false;"><i class="far fa-user-circle"></i><span> Ver más</span></a></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>    
</div>
<!-- GRID -->            
<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><a href="javascript:;" class="btn btn-info btnBack"><i class="fas fa-angle-double-left"></i>&nbsp;Volver a lista</a><br><br><br><br></div>
</div>

<script src="../../inc/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="../../inc/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="../../inc/js/pages/tables/jquery-datatable.js"></script>

<script src='../../inc/js/edit_segmento.js'></script>
<?php }?>