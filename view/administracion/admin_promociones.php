<?php 
include_once '../../controller/administracion/c_admin_promociones.php';
$select = new C_admin_promociones($ser,$usu,$pas,$bd);
$grid = new C_admin_promociones($ser,$usu,$pas,$bd);

$type = $select->projectype();

if($type=='1' OR $type=='2'){
    $tipo = 'puntos';
}elseif($type=='3'){
    $tipo = 'visitas';
}else{
    $tipo= 'error';
}

?>
<script src="../../inc/js/admin_promociones.js"></script>
<script src="../../inc/js/edit_promociones.js"></script>
<button type="button" name="" class="previous-form btn btn-info " onclick="shownew('newpromo')"><i class="fas fa-plus"></i> Agregar Nueva Promoción </button><br><br>  
<div class="card" id="newpromo" style="display: none;">
    <div class="body">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="header">
                    <h2 class="text_color"><i class="fas fa-tags"></i> Registro de Nueva Promoción <small>Agrega una Promoción</small></h2>
                </div>
            </div>
            <!--div class="col-sm-12 col-md-6 col-lg-6">
                <button class="btn btn-block btn-lg btn_color waves-effect" onclick="menu('administracion/edit_promociones')">Editar Promociones</button>
            </div-->
        </div>
        <form id="addpromo" onsubmit="addpromo(); return false;">
            <!-- Init Accordion -->
            <div class="panel-group" id="accordion_1" role="tablist" aria-multiselectable="true">
                <!-- init form content -->
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons"></i>
                    </span>
                    <h5>* Tipo</h5>
                    <div class="form-line">
                        <select id="type" name="type" onchange="changesel(this.value)" class="form-control show-tick" required>
                            <option value="" selected>Selecciona el tipo de Campaña</option>
                            <?php
                                $select->selectype();
                            ?>    
                        </select>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control" id="promo" name="promo" placeholder="* Nombre Promoción" required>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control" id="detail" name="detail" placeholder="* Detalle" required>
                    </div>
                </div>
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control" id="smalltxt" name="smalltxt" placeholder="* Texto Corto" required>
                    </div>
                </div>
                <div id="cashbackd" class="row" style="display: none;">
                    <div class="col-lg-5 col-md-5 col-xs-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="number" class="form-control" id="cashback" name="cashback" placeholder="$ Valor a Devolver" value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1 col-md-1 col-xs-12">
                        <div class="input-group">
                            <div align="center">
                                Ó
                            </div>
                        </div>    
                    </div>    
                    <div class="col-lg-6 col-md-6 col-xs-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="number" class="form-control" id="cashbackperc" name="cashbackperc" placeholder="% Porcentaje a Devolver" value="">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="customd" class="input-group" style="display: none;">
                    <div class="form-line">
                        <input type="number" class="form-control" id="custom" name="custom" placeholder="" value="">
                    </div>
                </div>
                <div id="codigod" class="input-group" style="display: none;">
                    <div class="form-line">
                        <input type="text" class="form-control" id="codigo" name="codigo" placeholder="* Código" value="">
                    </div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading" role="tab" id="headingOne_1">
                        <h4 class="panel-title primary-back">
                            <a onclick="togglemode('step1');" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#step1">
                                <i class="fas fa-plus"></i> Condiciones, Ver más, Uso...
                            </a>
                        </h4>
                    </div>
                    <div id="step1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_1" style="display: none;">
                        <div class="panel-body">
                            <!-- Init Accordion content 1 -->
                            <div class="input-group">
                                <div class="form-line">
                                    <textarea rows="4" class="form-control no-resize" id="conditions" name="conditions" placeholder=" Condiciones"></textarea>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="form-line">
                                    <textarea rows="4" class="form-control no-resize" id="howuse" name="howuse" placeholder=" ¿Cómo usar cupones?"></textarea>
                                </div>
                            </div>
                            <div class="input-group">
                                <div class="form-line">
                                    <input type="text" class="form-control" id="infolink" name="infolink" placeholder=" Link de más información">
                                </div>
                            </div>
                            <!-- End Accordion content -->
                        </div>
                    </div>
                </div>
                <br/>
                <div class="panel panel-primary">
                    <div class="panel-heading" role="tab" id="headingTwo_1">
                        <h4 class="panel-title primary-back">
                            <a onclick="togglemode('step2');" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#step2">
                                <i class="fas fa-plus"></i> Bonus (si es que aplica)...
                            </a>
                        </h4>
                    </div>
                    <div id="step2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo_1" style="display: none;">
                        <div class="panel-body">
                            <!-- Init Accordion content 1 -->
                                
                                <div class="input-group">
                                    <div class="form-line">
                                        <select id="bonustype" name="bonustype" class="form-control show-tick">
                                            <option value="0" selected>Selecciona el tipo de Retribución</option>
                                            <!--option value="1">Excluyente(solo <?php //echo $tipo; ?> Bonus, Reemplazando la Configuración General)</option-->
                                            <option value="2">Extra(Configuración General + <?php echo $tipo; ?> Bonus)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="input-group">
                                            <div class="form-line">
                                                <input type="hidden" class="form-control" id="projectype" name="projectype" value="<?php echo $tipo; ?>">
                                                <input type="number" class="form-control" id="rewardto" name="rewardto" value="" placeholder=" Valor en <?php echo $tipo; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="input-group">
                                            <!--div id="ejemplo1">Sólo los puntos a introducir</div-->
                                            <!--div id="ejemplo2">Puntos de Configuración + Puntos Extra </div-->
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon">%</span>
                                            <div class="form-line">
                                                <input type="number" class="form-control" id="rewardperc" name="rewardperc" value="" placeholder=" Valor en Multiplos Ejem. <?php echo $tipo; ?> triples: 300%">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-xs-12">
                                        <div class="input-group">
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                            <!-- End Accordion content -->
                        </div>
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-xs-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="number" min="1" class="form-control" id="npromos" name="npromos" placeholder="* Límite de Promociones" required="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="checkbox" class="ilimitado" id="ilimitadop" class="filled-in">
                                <label for="ilimitadop">Promociones Ilimitadas</label>
                            </div>
                        </div>
                    </div>
                    <!--div class="col-md-4 col-lg-4 col-xs-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="number" min="0" class="form-control" id="halfdays" name="halfdays" placeholder="* No. Días para Activación" required="">
                            </div>
                        </div>
                    </div-->
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-xs-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="number" min="1" class="form-control" id="nredention" name="nredention" placeholder="* Limite de uso por Usuario" required="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6 col-xs-12">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="checkbox" class="ilimitado" id="ilimitador" class="filled-in">
                                <label for="ilimitador">Ilimitadas</label>
                            </div>
                        </div>
                    </div>
                </div>    
                <!-- Input´s with mask Date -->
                <div class="demo-masked-input">
                    <div class="row">
                        <div class="col-md-4 col-lg-4 col-xs-12">
                            <b>* Inicio de Promoción</b>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">date_range</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" id="start" name="start" class="form-control date datepicker" placeholder="Ejem: 9999-01-01" required>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 col-xs-12">
                            <b>* Fin de Promoción</b>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">date_range</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" id="ending" name="ending" class="form-control date datepicker" placeholder="Ejem: 9999-01-01" required>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Input´s with mask Date -->
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-xs-6">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="checkbox" id="push" class="filled-in">
                                <label for="push">¿Envíar Push?</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-6">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="checkbox" id="sms" class="filled-in" minlength="5" maxlength="160">
                                <label for="sms">¿Envíar Sms?</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-6">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="checkbox" id="whats" class="filled-in">
                                <label for="whats">¿Envíar WhatsApp?</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3 col-xs-6">
                        <div class="input-group">
                            <div class="form-line">
                                <input type="checkbox" id="email" class="filled-in">
                                <label for="email">¿Envíar Email?</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-lg-3 col-xs-12">
                        <b>Fecha y Hora de Envío</b>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">date_range</i>
                            </span>
                            <div class="form-line">
                                <input mdc-datetime-picker="" date="true" time="true" type="text" id="sendtime" placeholder="Time" minutes="true" minute-steps="30" min-date="minDate" auto-ok="true" format="hh" short-time="true" ng-model="time" class="md-input datetimepicker">
                            </div>
                        </div>
                    </div>
                </div>
                <div id="pushdiv" class="input-group" style="display: none;">
                    <div class="form-line">
                        <input type="text" class="form-control" id="pushtxt" name="pushtxt" placeholder=" Agregar Notificación para App">
                    </div>
                </div>
                <div id="smsdiv" class="input-group" style="display: none;">
                    <div class="form-line">
                        <input type="text" class="form-control" id="smstxt" name="smstxt" placeholder=" Agregar Notificación para Sms">
                    </div>
                </div>
                <div id="whatsdiv" class="input-group" style="display: none;">
                    <div class="form-line">
                        <input type="text" class="form-control" id="whatstxt" name="whatstxt" placeholder=" Agregar Notificación para WhatsApp">
                    </div>
                </div>
                <!-- Init send sms -->
                <div id="emaildiv" class="panel panel-primary" style="display: none;">
                    <div class="panel-heading" role="tab" id="heading_4">
                        <h4 class="panel-title primary-back">
                            <a onclick="togglemode('step4');" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#step4">
                                <i class="fas fa-plus"></i> Texto Email...
                            </a>
                        </h4>
                    </div>
                    <div id="step4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_4" style="display: none;">
                        <div class="panel-body">
                            <!-- Init Accordion content 1 -->
                            <textarea name="emailtxt" id="emailtxt" rows="10" cols="80" placeholder="Message Email">
                                
                            </textarea>
                            <!-- End Accordion content -->
                        </div>
                    </div>
                </div>
                <br/>
                            
                <!-- End send sms -->
                <div class="panel panel-primary">
                    <div class="panel-heading" role="tab" id="heading3_1">
                        <h4 class="panel-title primary-back">
                            <a onclick="togglemode('step3');" role="button" data-toggle="collapse" data-parent="#accordion_1" href="#step3">
                                <i class="fas fa-plus"></i> * Selección de Sucursales, Segementos y/o Niveles...
                            </a>
                        </h4>
                    </div>
                    <div id="step3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3_1" style="display: none;">
                        <div class="panel-body">
                            <!-- Init Accordion content 1 -->
                                <div class="input-group">
                                    <h5>* Segmento(s)</h5>
                                    <div class="body">
                                        <div id='select-all' class="btn btn-primary btn-block waves-effect" >Todos los Segementos</div>
                                        <div id='deselect-all' class="btn btn-primary btn-block waves-effect" >Quitar Segementos</div>
                                        <select id="segment" name="segment[]" class="ms"  multiple="multiple">
                                            <optgroup label="Elige uno o varios Segmentos">
                                                <?php echo $select->selectsegment(); ?>
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <div align="center">
                                        <h2>Y/O</h2>
                                    </div>
                                </div>    
                                <div class="input-group">
                                    <h5>* Sucursal(es)</h5>
                                    <div class="body">
                                        <div id='select-all2' class="btn btn-primary btn-block waves-effect" >Todas las Sucursales</div>
                                        <div id='deselect-all2' class="btn btn-primary btn-block waves-effect" >Quitar Sucursales</div>
                                        <select id="suc" name="suc[]" class="ms"  multiple="multiple" >
                                            <optgroup label="Elige uno o varias sucursales">
                                                <?php echo $select->selectsuc(); ?>
                                            </optgroup>    
                                        </select>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <div align="center">
                                        <h2>Y/O</h2>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <h5>* Nivel(es)</h5>
                                    <div class="body">
                                        <div id='select-all3' class="btn btn-primary btn-block waves-effect" >Todos los Niveles</div>
                                        <div id='deselect-all3' class="btn btn-primary btn-block waves-effect" >Quitar Niveles</div>
                                        <select id="lvl" name="lvl[]" class="ms"  multiple="multiple">
                                            <optgroup label="Elige uno o varios Niveles">
                                                <?php echo $select->selectlvl(); ?>
                                            </optgroup>    
                                        </select>
                                    </div>
                                </div>
                            <!-- End Accordion content -->
                        </div>
                    </div>
                </div>
                <br/>
                <input type="submit" value="Enviar" class="hide" />
            </div>    
        </form>

        <button class="btn btn-block btn-lg btn_color waves-effect" onclick="sendform();" id="submit-lead" name="submit-lead">Agregar</button>
    </div>
</div>
<!-- Init Edit content -->
<div class="card">
    <div class="body">
    <!-- Edit Menù table -->
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="header">
                    <h2 class="text_color"><i class="fas fa-tags"></i> Edición de Promociones <small>Edita una Promoción</small></h2>
                </div>
                <i class="fas fa-info-circle" rel="popover" title="En la parte de 'Arriba' o 'Izquierda' se encuentran las Sucursales o Segmentos sin seleccionar" data-content="En la parte de 'Abajo' o 'Derecha' se encuentran las sucursales o Segmentos seleccionados"></i>
            </div>
            <!--div class="col-sm-12 col-md-6 col-lg-6">
                <button class="btn btn-block btn-lg btn_color waves-effect" onclick="menu('administracion/admin_promociones')">Nueva Promoción</button>
            </div-->
        </div>
        <div class="table-responsive">
            <table id="mainTable" class="table table-bordered table-striped table-hover dataTable js-exportable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Detalle</th>
                        <th>Texto Corto</th>
                        <th>Segmento(s)</th>
                        <th>Sucursal(es)</th>
                        <th>Nivel(es)</th>
                        <th>Comienza</th>
                        <th>Termina</th>
                        <th>Utilizadas</th>
                        <th>Desactivar/Activar</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        echo $grid->grideditp('tr');
                    ?>
                </tbody>
            </table>
        </div>
        <!-- #END# Exportable Table -->
    </div>   
</div>

<!-- IMPORTANT CONTENT-->

<!-- Jquery Core Js -->
<!--script src="../../inc/plugins/jquery/jquery.min.js"></script-->

<!-- Moment Plugin Js -->
<script src="../../inc/plugins/momentjs/moment.js"></script>
<script src="../../inc/plugins/momentjs/moment-with-locale.js"></script>

<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="../../inc/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<!-- Ckeditor -->
<script src="../../inc/plugins/ckeditor/ckeditor.js"></script>
<!-- Custom Js -->
<!--script src="../../inc/js/pages/forms/editors.js"></script-->
<!-- Multi Select Plugin Js -->
<script src="../../inc/plugins/multi-select/js/jquery.multi-select.js"></script>

<script src="../../inc/js/general_promociones.js"></script>
<!-- END IMPORTANT -->
<script>

    //multi select initiate
    $('#suc').multiSelect();
    $('#segment').multiSelect();
    $('#lvl').multiSelect();

    var segm = $('#segment');
    var suc = $('#suc');
    var lvl = $('#lvl');

    $('#select-all').on('click', function(){
      segments(segm,1);
    });

    $('#deselect-all').on('click', function(){
      segments(segm,2);
    });

    $('#select-all2').on('click', function(){
      sucs(suc,1);
    });

    $('#deselect-all2').on('click', function(){
      sucs(suc,2);
    });

    $('#select-all3').on('click', function(){
      lvls(lvl,1);
    });

    $('#deselect-all3').on('click', function(){
      lvls(lvl,2);
    });

    function segments(varsegm,opt){
        if(opt==1){
            varsegm.multiSelect('select_all');
        }else{
            varsegm.multiSelect('deselect_all');
        }
        
    }

    function sucs(varsuc,opt){
        if(opt==1){
            varsuc.multiSelect('select_all');
        }else{
            varsuc.multiSelect('deselect_all');
        }
        
    }

    function lvls(varlvl,opt){
        if(opt==1){
            varlvl.multiSelect('select_all');
        }else{
            varlvl.multiSelect('deselect_all');
        }
        
    }

    function sendform(){
        var form = $('#addpromo');
        $('#addpromo').find('[type="submit"]').trigger('click');
    }

    $('.datepicker').bootstrapMaterialDatePicker(
        { 
            format : 'YYYY-MM-DD', 
            lang : 'es',
            time : false, 
            weekStart : 1,
            okText : 'Aceptar',  
            cancelText : 'Cancelar',
            minDate : new Date() 
        }
    );

    $('.ifcheck').change(function() {
        if($(this).is(":checked")) {
            var idx = $(this).attr('id');
            var id = idx.replace("x", "");
            activepromo(id);
        }else{
            var idx = $(this).attr('id');
            var id = idx.replace("x", "");
            inactivepromo(id);
        }        
    });

    $('#ilimitadop').change(function() {
        if($(this).is(":checked")) {
            $('#npromos').hide();
            $('#npromos').val(99999999999);
        }else{
            $('#npromos').show();
            $('#npromos').val(1);
        }        
    });

    $('#ilimitador').change(function() {
        if($(this).is(":checked")) {
            $('#nredention').hide();
            $('#nredention').val(99999999999);
        }else{
            $('#nredention').show();
            $('#nredention').val(1);
        }        
    });
    
</script>
<?php
 //include '../../inc/datatables.php';
?>