
<?php
//LLAMAMOS ESTILOS DE CONFIGURACION
    include_once '../../inc/funciones.php';
    include_once '../../inc/parametros.php';
    include_once '../../controller/administracion/c_admin_segmento.php';
    $id=$_GET['id'];
    //die($id);
    if($id!=''){ 
        //die($id);
        $instanceSegmento=new C_admin_segmento($ser,$usu,$pas,$bd);
        $segmento = $instanceSegmento->getSegmentos($id); //Obtener la informacion del segmento actual
        $clientes = $instanceSegmento->getClientesSegmento($id, null); //Obtenemos los clientes del segmento actual
        $clientsAdd = $instanceSegmento->getClientsAdd($id); //Obtenemos los clientes que se pueden agregar al segmento actual        
        if( count($segmento) > 0 ){ 

    echo '<script>console.log("'.json_decode($segmento).'");</script>'
?>
    
<!-- CONTENIDO -->
    
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="row">                    
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div style="padding:10px 0 0 10px">
                                    <h5 style="color:#46b8da;">INFORMACION DEL SEGMENTO</h5>
                                </div>                                
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <form class="form-group" style="padding:10px">
                                    <?php while ($fila = $segmento->fetch_assoc()) { ?>
                                    <div class="form-group">
                                        <label for="nombreSegmento">Nombre Segmento: *</label>
                                        <input type="text" class="form-control"  name='nombre_segmento' placeholder="Nombre del Segmento" id='nombreSegmento' value="<?php echo $fila['nombre_segmento'] ?>">
                                        
                                    </div>                            
                                    <div class="form-group" style="margin-top:8px">
                                        <label for="descripcionSegmento">Descripción:</label>
                                        <textarea class="form-control" name='descripcion' placeholder="Campo opcional de 256 caracteres" id='descripcionSegmento'><?php echo $fila['descripcion_segmento'] ?></textarea>
                                    </div>
                                    <div class="divSelect" style="margin-top:8px">
                                        <label for="statusSegmento">Status:</label>
                                        <select id="statusSegmento" name="statusSegmento" style="width:100%">
                                            <option value="1" <?php echo $fila['activo'] == 1 ? "selected" : "" ?>>Activo</option>
                                            <option value="0" <?php echo $fila['activo'] == 0 ? "selected" : "" ?>>Inactivo</option>
                                        </select>
                                    </div>
                                    <div class="form-group" style="margin-top:8px">
                                        <div id='loading2'><img src="../../inc/imagenes/load.gif"></div>
                                        <button id='btnEditSegmento' type="button" class="btn  m-t-15 waves-effect btn-primary" >Actualizar Información</button>
                                    </div>
                                    <?php } ?>
                                </form>
                            </div>                            
                        </div>                
                    </div>
                </div>                
            </div>
            <hr style="border: 1px solid white"/>
            <div class="row">                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <button type="button" name="previous" class="previous-form btn btn-info " id='muestra_form_add_usu'><i class="fas fa-plus"></i> Agregar Nuevo Cliente Al Segmento </button><br><br>
                </div>
                <div class="w-100"></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="form_add_cliente" class="card">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div style="padding:10px 0 0 10px">
                                    <h5 style="color:#46b8da;">AGREGAR USUARIO</h5>
                                </div>                                
                            </div>
                        </div>
                        <div class="row">                                                
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <form id="formAddClient" class="form-group" style="padding:10px">
                                    <div class="divSelect">
                                        <label for="idUsuario">Elige al usuario: *</label><br>
                                        <select id="idUsuario" name="idUsuario" style="width:100%">
                                            <option value=""> -- Selecciona un usuario -- </option>
                                            <?php while ($client = mysqli_fetch_assoc($clientsAdd)) { ?>
                                                <option value="<?php echo $client['id_usuario']?>"><?php echo $client['nombre']?></option>
                                            <?php } ?>
                                        </select>
                                        <br>
                                        <span id="labelUser" style="color:red; font-weight:bold"></span>
                                        <input id="idSegmento" type="hidden" value="<?php echo $id ?>" />
                                    </div>
                                    <div class="form-group" style="margin-top:8px">
                                        <div id='loading1'><img src="../../inc/imagenes/load.gif"></div>
                                        <button id='btnNewUserSegmento' type="button" class="btn  m-t-15 waves-effect btn-primary" >Agregar Usuario</button>
                                    </div>
                                </form>
                            </div>                            
                        </div>                
                    </div>
                </div>                
            </div>
            
            <!-- GRID -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2 style="color:#46b8da;">
                                Lista de clientes registrados en el Segmento
                            </h2>
                        </div>
                        <div class="body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                    <thead>
                                        <tr>                                            
                                            <th>ID </th>
                                            <th>Nombre Cliente</th>
                                            <th>Email</th>
                                            <th>Status</th>
                                            <th>Acciones</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php 
                                    //GENERAMOS CONTENIDO DE TABLA                               
                                    while($fila = $clientes->fetch_assoc()){ ?>
                                        <tr>
                                            <td><?php echo $fila['id_rel_segmento_usuario']; ?></td>                                                
                                            <td><?php echo utf8_encode($fila['nombre']); ?></td>
                                            <td><?php echo utf8_encode($fila['email']); ?></td>
                                            <td><?php echo $fila['activo'] == 0 ? 'Activo' : 'Inactivo' ; ?></td>
                                            <td>
                                                <a href="javascript:;" data-action="<?php echo $fila['id_usuario']; ?>" class="btn btn-primary btnViewUser"><i class="far fa-user-circle"></i></a>
                                                <a href="javascript:;" class="btn btn-danger btnDeleteUserSegmento" data-action="<?php echo $fila['id_usuario']; ?>"><i class="far fa-trash-alt"></i></a>
                                            </td>
                                        </tr>
                                    <?php  } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
    <script src='../../inc/js/jquery-1.11.2.min.js'></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <!-- Bootstrap Select Css -->
    <link href="../../inc/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <!-- Select Plugin Js -->
    <script src="../../inc/plugins/bootstrap-select/js/bootstrap-select.js"></script>



    <script src="../../inc/plugins/bootstrap-select/js/bootstrap-select.js"></script>

    <!-- Autosize Plugin Js -->
    <script src="../../inc/plugins/autosize/autosize.js"></script>

    <!-- Moment Plugin Js -->
    <script src="../../inc/plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="../../inc/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <script src="../../inc/js/pages/forms/basic-form-elements.js"></script>    
    <script type="text/javascript" src='../../inc/js/edit_segmento.js'></script>

<?php } }?>
 