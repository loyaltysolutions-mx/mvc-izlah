<?php
include_once '../../controller/administracion/c_admin_categoria.php';
$grid = new C_admin_categoria($ser,$usu,$pas,$bd);
?>
<script src="../../inc/js/admin_categoria.js"></script>
<script src="../../inc/js/edit_categoria.js"></script>
<button type="button" name="" class="previous-form btn btn-info " onclick="shownew('newcat')"><i class="fas fa-plus"></i> Agregar Nueva Categoría </button><br><br>
<div id="newcat" class="card" style="display: none;">
    <div class="body">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="header">
                    <h2 class="text_color"><i class="fas fa-project-diagram"></i> Registro de Nueva Categoría <small>Agrega un Elemento a Categoría</small></h2>
                </div>
            </div>    
            <!--div class="col-sm-12 col-md-6 col-lg-6">
                <button class="btn btn-block btn-lg btn_color waves-effect" onclick="menu('administracion/edit_categorias')">Editar Categoría</button>
            </div-->
        </div>
        <form id="addcat" onsubmit="addcat(); return false;">
            <div class="row">
                <div class="input-group">
                    <span class="input-group-addon">
                        <!--i class="material-icons">person</i-->
                    </span>
                    <div class="form-line">
                        <input type="text" class="form-control" id="cat" name="cat" placeholder="* Nombre del Elemento de la Categoría" required autofocus>
                    </div>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <!--i class="material-icons">person</i-->
                    </span>
                    <div class="form-line">
                        <input type="text" class="form-control" id="descript" name="description" placeholder="* Descripción" required>
                    </div>
                </div>
            </div>
                

            
            <input type="submit" value="Enviar" class="hide" />
        </form>

        <button class="btn btn-block btn-lg btn_color waves-effect" onclick="sendform();" id="submit-lead" name="submit-lead">Agregar</button>
    </div>
</div>
<!-- IMPORTANT CONTENT-->
<script>
    function sendform(){
        var form = $('#addcat');
        $('#addcat').find('[type="submit"]').trigger('click');
    }
</script>
<!-- Editable -->
<div class="card">
    <div class="body">
    <!-- Edit Menù table -->
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="header">
                    <h2 class="text_color"><i class="fas fa-project-diagram"></i> Edición de Categorías <small>Agrega un Elemento a Categoría</small></h2>
                </div>
            </div>
        </div>
        <div class="table-responsive">
            <table id="mainTable" class="table table-bordered table-striped table-hover dataTable js-exportable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Categoria</th>
                        <th>Descripción</th>
                        <th></th>
                        <th>Inactivo/Activo</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        echo $grid->grideditc('tr');
                    ?>
                </tbody>
            </table>
        </div>
        <!-- #END# Exportable Table -->
    </div>   
</div>
<?php
 include '../../inc/datatables.php';
?>
<script>
    $('.ifcheck').change(function() {
        if($(this).is(":checked")) {
            var idx = $(this).attr('id');
            var id = idx.replace("x", "");
            activecat(id);
        }else{
            var idx = $(this).attr('id');
            var id = idx.replace("x", "");
            inactivecat(id);
        }        
    });
</script>