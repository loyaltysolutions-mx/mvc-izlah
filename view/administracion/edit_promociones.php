<?php 
include_once '../../controller/administracion/c_admin_promociones.php';
$select = new C_admin_promociones($ser,$usu,$pas,$bd);
$id = $_POST['id'];
?>
<script src="../../inc/js/admin_promociones.js"></script>
<script src="../../inc/js/edit_promociones.js"></script>
<!--button type="button" name="" class="previous-form btn btn-info " onclick="shownew('newpromo')"><i class="fas fa-plus"></i> Agregar Nuevo Nivel </button><br><br-->
<button class="btn btn-info" onclick="menu('administracion/admin_promociones')"><i class="fas fa-angle-double-left"></i> Volver a lista</button>
<br>
<br> 
<div class="card" id="editablepromo">
    <div class="body">
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="header">
                    <h2 class="text_color"><i class="fas fa-tags"></i> Edición de Promoción <small>Edita una Promoción</small></h2>
                </div>
            </div>
            <!--div class="col-sm-12 col-md-6 col-lg-6">
                <button class="btn btn-block btn-lg btn_color waves-effect" onclick="menu('administracion/edit_promociones')">Editar Promociones</button>
            </div-->
        </div>
		<?php 
			echo $select->grideditable($id);
		?>
	</div>
</div>	
<!-- IMPORTANT CONTENT-->
<!-- Jquery Core Js -->
<script src="../../inc/plugins/jquery/jquery.min.js"></script>
<script src="../../inc/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="../../inc/plugins/bootstrap-select/js/bootstrap-select.js"></script>
<!-- Moment Plugin Js -->
<script src="../../inc/plugins/momentjs/moment.js"></script>
<script src="../../inc/plugins/momentjs/moment-with-locale.js"></script>

<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="../../inc/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<!-- Ckeditor -->
<script src="../../inc/plugins/ckeditor/ckeditor.js"></script>
<!-- Custom Js -->
<!--script src="../../inc/js/pages/forms/editors.js"></script-->
<!-- Multi Select Plugin Js -->
<script src="../../inc/plugins/multi-select/js/jquery.multi-select.js"></script>

<!--script src="../../inc/js/pages/ui/tooltips-popovers.js"></script-->

<!-- END IMPORTANT -->
<script>
    function sendform(){
        var form = $('#addpromo');
        $('#addpromo').find('[type="submit"]').trigger('click');
    }
    
    //multi select initiate
    $('#suc').multiSelect();
    $('#segment').multiSelect();
    $('#lvl').multiSelect();

    var segm = $('#segment');
    var suc = $('#suc');
    var lvl = $('#lvl');

    $('#select-all').on('click', function(){
      segments(segm,1);
    });

    $('#deselect-all').on('click', function(){
      segments(segm,2);
    });

    $('#select-all2').on('click', function(){
      sucs(suc,1);
    });

    $('#deselect-all2').on('click', function(){
      sucs(suc,2);
    });

    $('#select-all3').on('click', function(){
      lvls(lvl,1);
    });

    $('#deselect-all3').on('click', function(){
      lvls(lvl,2);
    });

    function segments(varsegm,opt){
        if(opt==1){
            varsegm.multiSelect('select_all');
        }else{
            varsegm.multiSelect('deselect_all');
        }
        
    }

    function sucs(varsuc,opt){
        if(opt==1){
            varsuc.multiSelect('select_all');
        }else{
            varsuc.multiSelect('deselect_all');
        }
        
    }

    function lvls(varlvl,opt){
        if(opt==1){
            varlvl.multiSelect('select_all');
        }else{
            varlvl.multiSelect('deselect_all');
        }
        
    }

    $('.datepicker').bootstrapMaterialDatePicker(
        { 
            format : 'YYYY-MM-DD', 
            lang : 'es',
            time : false, 
            weekStart : 1,
            okText : 'Aceptar',  
            cancelText : 'Cancelar',
            minDate : new Date() 
        }
    );

function changesel(type){
    if(type==1){
        $('#cashbackd').show();
        customhide();
    }else if(type==3){
        $('#customd').show();
        $('#codigod').show();
        $('#custom').attr('placeholder','* $ de Descuento');
        $('#custom').attr('required','required');
        cashbackhide();
    }else if(type==6){
        $('#customd').show();
        $('#codigod').show();
        $('#custom').attr('placeholder','* % de Descuento');
        $('#custom').attr('required','required');
        cashbackhide();
    }else{
        customhide();
        cashbackhide();
    }
}

function customhide(){
    $('#custom').removeAttr('required');
    $('#customd').hide();
    $('#codigod').hide();
    $('#custom').val('');
    $('#codigo').val('');
}

function cashbackhide(){
    $('#cashbackd').hide();
    $('#cashback').val('');
    $('#cashbackperc').val('');
}

$("#cashback").on('keyup', function(){
    var cashback = $(this).val().length;
    if(cashback>0){
        $('#cashbackperc').val('');
    }
}).keyup();

$("#cashbackperc").on('keyup', function(){
    var cashbackperc = $(this).val().length;
    if(cashbackperc>0){
        $('#cashback').val('');
    }
}).keyup();


    if($('#push').is(':checked')){
        $('#pushdiv').show();
    }

    if($('#sms').is(':checked')){
        $('#smsdiv').show();
    }

    if($('#whats').is(':checked')){
        $('#whatsdiv').show();
    }

    if($('#email').is(':checked')){
        $('#emaildiv').show();
    }

    $('#push').change(function() {
        if(this.checked) {
            $('#pushdiv').show();
        }else{
            $('#pushdiv').hide();
            $('#pushtxt').val('');
        }
    });

    $('#sms').change(function() {
        if(this.checked) {
            $('#smsdiv').show();
        }else{
            $('#smsdiv').hide();
            $('#smstxt').val('');
        }
    });

    $('#whats').change(function() {
        if(this.checked) {
            $('#whatsdiv').show();
        }else{
            $('#whatsdiv').hide();
            $('#whatstxt').val('');
        }
    });

    $('#email').change(function() {
        if(this.checked) {
            $('#emaildiv').show();
        }else{
            $('#emaildiv').hide();
            $('#emailtxt').val('');
            $("#emailtxt") != undefined && CKEDITOR.instances.emailtxt.setData('');
        }
    });

    $("#rewardto").on('keyup', function(){
        var rewardto = $(this).val().length;
        if(rewardto>0){
            $('#rewardperc').val('');
            $('#rewardval').val('');
        }else{
            
        }
    }).keyup();

    $("#rewardperc").on('keyup', function(){
        var rewardto = $(this).val().length;
        if(rewardto>0){
            $('#rewardto').val('');
            $('#rewardval').val('');
        }
    }).keyup();

    $("#rewardval").on('keyup', function(){
        var rewardto = $(this).val().length;
        if(rewardto>0){
            $('#rewardto').val('');
            $('#rewardperc').val('');
        }
    }).keyup();

    $("#emailtxt") != undefined && CKEDITOR.replace('emailtxt');

    $('.datetimepicker').bootstrapMaterialDatePicker(
        { 
            format : 'YYYY-MM-DD HH:mm:ss', 
            lang : 'es', 
            weekStart : 1,
            okText : 'Aceptar',  
            cancelText : 'Cancelar',
            minDate : new Date() 
        }
    );

    $('#ilimitadop').change(function() {
        if($(this).is(":checked")) {
            $('#npromos').hide();
            $('#npromos').val(999999999);
        }else{
            $('#npromos').show();
            $('#npromos').val(1);
        }        
    });

    $('#ilimitador').change(function() {
        if($(this).is(":checked")) {
            $('#nredention').hide();
            $('#nredention').val(999999999);
        }else{
            $('#nredention').show();
            $('#nredention').val(1);
        }        
    });
    //edit content
    

</script>	