<?php
include_once '../../controller/administracion/c_admin_menu.php';
$select = new C_admin_menu($ser,$usu,$pas,$bd);
include('../../inc/import/dbconect.php');
require_once('../../inc/import/vendor/php-excel-reader/excel_reader2.php');
require_once('../../inc/import/vendor/SpreadsheetReader.php');

$saludo = "";
if (isset($_POST["import"]))
{
	$allowedFileType = array('application/vnd.ms-excel','text/xls','text/csv','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet','application/octet-stream');
	$cat = $_POST['cat'];
   	if(in_array($_FILES["file"]["type"],$allowedFileType)){
	 	$targetPath = '../../inc/xls/'.$_FILES['file']['name'];
	 	$saludo = "aqui";
		move_uploaded_file($_FILES['file']['tmp_name'], $targetPath);

		$Reader = new SpreadsheetReader($targetPath);

		echo "entro"; 

		$sheetCount = count($Reader->sheets());
	        for($i=0;$i<$sheetCount;$i++){
	            
	            $Reader->ChangeSheet($i);
	            
	            foreach ($Reader as $Row){
					
	                $menu = "";
	                if(isset($Row[0])) {
	                    $menu = mysqli_real_escape_string($connect,$Row[0]);
	                }

	                $descript = "";
	                if(isset($Row[1])) {
	                    $descript = mysqli_real_escape_string($connect,$Row[1]);    
	                }

	                $price = "";
	                if(isset($Row[2])) {
	                    $price = mysqli_real_escape_string($connect,$Row[2]);   
	                }

					

	                //echo "<script>console.log('".$menu."');</script>";
	                //echo "<script>console.log('".$descript."');</script>";
	                //echo "<script>console.log('".$cat."');</script>";
	                //echo "<script>console.log('".$price."');</script>";

	                $filename ='';
	                
	                if (!empty($menu) AND !empty($cat) AND !empty($descript) AND !empty($price)){

	                	//if($i==1){

	                	//}elseif($i>1){
	                		$filename='';
	                		$select->addmenu($menu,$cat,$descript,$price,$filename);
                    		
	                	//}
     
	                } 
	            }
	        }
	       header('Location: https://desclub.com.mx/plataforma_lealtad/?mn=1');
    } 
    else{ 
        $type = "error";
        $message = "El archivo enviado es invalido. Por favor vuelva a intentarlo";
    }               
}else{
	$saludo = " * Archivo aún no cargado";
}
?>

<link rel="icon" href="favicon.ico">
<title>Importar archivo de Excel</title>
<!-- Begin page content -->

<div class="card">
	<div class="row">
	    <div class="col-sm-12 col-md-6 col-lg-6">
	        <div class="header">
	            <h2 class="text_color"><i class="fas fa-upload"></i> Carga Multiple de Servicios <small>Agrega Varios Servicios por MEdio de un XLS</small></h2>
	        </div>
	    </div>
	    <!--div class="col-sm-12 col-md-6 col-lg-6">
	        <button class="btn btn-block btn-lg btn_color waves-effect" onclick="menu('administracion/edit_promociones')">Editar Promociones</button>
	    </div-->
	</div>
  <div class="row" style="padding: 10px;">
    <div class="col-sm-12 col-md-12"> 
      <!-- Contenido -->
    	<h3  id="hello" class="mt-5">Importar archivo de Excel <?php echo $saludo; ?></h3>
		<hr>
		<div style="color: blue"><b>NOTA: El archivo va sin encabezados, Formato aceptado (.XLS)</div>
	    <div class="outer-container">
	        <form action="../administracion/upload_menu.php" method="post" name="frmExcelImport" id="frmExcelImport" enctype="multipart/form-data">
	            <div class="input-group">
                    <div class="form-line">
                        <select id="cat" name="cat" onchange="" class="form-control show-tick" required>
                            <option value="">Elige una Categoria</option>
                            <?php echo $select->selectcat(); ?>
                        </select>
                    </div>
                </div>
	            <div>
	                <label>Elija Archivo Excel</label> 
	                <input class="uploadxls dropzone needsclick dz-clickable col-12 col-md-12" type="file" name="file" id="filein" accept=".xls,.xlsx">
	                <button type="submit" id="import" name="import" class="btn btn-block btn-lg bg-deep-purple waves-effect btn_color" disabled>Importar Registros</button>
	            </div>
	        
	        </form>
	        
	    </div>
	        
	    <div class="body">
	    <!-- Edit Users table -->
	        <h3>Códigos Cargados Recientemente</h3>
	        <div class="table-responsive">
	            <table id="mainTable" class="table table-bordered table-striped table-hover dataTable js-exportable">
	                <thead>
	                    <tr>
	                        <th>ID</th>
			                <th>Menú Item</th>
			                <th>Descrición</th>
			                <th>Categoría</th>
			                <th>Precio</th>
			                <!--th>Imagen</th>
			                <th>Eliminar</th-->
	                    </tr>
	                </thead>
	                <tbody>
	                    <?php echo $select->uploadgrid('tr'); ?>
	                </tbody>
	            </table>
	        </div>
	    </div>
	    <!-- #Modal -->
        <div class="modal fade" id="editimg" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Edición de imagen</h4>
                    </div>
                    <div class="modal-body">
                        <form action="/" id="frmFileUpload" class="dropzone needsclick dz-clickable" method="post" enctype="multipart/form-data">
                            <div class="dz-message needsclick">
                                <div class="drag-icon-cph">
                                    <i class="material-icons">touch_app</i>
                                </div>
                                <div>
                                    Arrastra tu imagen o haz click para agregarla
                                </div>
                            </div>
                            <div class="fallback">
                                <input id="files" name="files" type="file" />
                            </div>
                        </form>
                    <input id="filename" type="hidden" name="filename">  
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- #End Modal -->   
      <!-- Fin Contenido --> 
    </div>
  </div>
<?php
	//require_once('../../inc/datatables.php');
?>
<!--script src="../../inc/plugins/bootstrap/js/bootstrap.js"></script-->
<!-- Dropzone Plugin Js -->
<script src="../../inc/plugins/dropzone/dropzone.js"></script>  
<script>
	//Dropzone config by Allan M.
    Dropzone.options.frmFileUpload = {
        maxFilesize: 2,
        acceptedFiles: "image/jpeg,image/png",

        removedfile: function(file) {
            this.on("dragstart", function(file) { 
               this.removeAllFiles(true); 
            });
        },

        accept: function(file, done) {
            
            console.log("uploaded");
            done();
            var sendid = $('#filename').attr('my');
            setfile(sendid,file.name,1);
            var filename = file.name;
            $('#filename').val(filename);
            
        },
        init: function() {
            this.on("addedfile", function() {
              if (this.files[1]!=null){
                this.removeFile(this.files[0]);
              }
            });
        }
    };
    
    $("form#frmFileUpload").dropzone({ 
        url: "../administracion/uploadmenu.php"
 	});
	
	$("#filein").change(function(){
	    if ($('#filein').get(0).files.length === 0) {
	        $('#import').attr('disabled', true);
	        $('#hello').html('Importar archivo de Excel');
	    }else{
	        $('#import').removeAttr('disabled');
	        $('#hello').html('Archivo Excel Seleccionado');
	    }
	});

</script>  
  <!-- Fin row --> 
  
</div>

