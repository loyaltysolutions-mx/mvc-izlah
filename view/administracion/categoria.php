<?php
include_once '../../inc/parametros.php';
include_once '../../model/administracion/m_admin_segmento.php';
class Categoria{
	
	private $ser;
	private $usu;
	private $pas;
	private $bd;
	private $model;

	function __construct($ser, $usu, $pas, $bd, $flagRow, $flagBtnDelete) {
		
		$this->ser=$ser;
	    $this->usu=$usu;
	    $this->pas=$pas;
	    $this->bd=$bd;
	    	    
	    $this->model = new M_admin_segmento($this->ser, $this->usu, $this->pas, $this->bd);

	    //die("entro");
		$cat = $this->model->getCategorias(NULL);
	    $this->elementos = array();
	    foreach ($cat as $key){
	      $res=$this->model->getObjetosCategorias($key['id_categoria']);
	      array_push($this->elementos, array('id_categoria' => $key['id_categoria'], 'nombre_categoria' => $key['nombre_categoria'], 'descripcion_categoria' => $key['descripcion_categoria'], 'inputs' => $res));
	    }

	    $this->tagHTML = $this->createHTML($this->elementos, $flagRowOR, $flagBtnDelete);
	}
    
    function createHTML($objeto, $flagRow, $flagBtnDelete){

    	$html = '<div>';
    		if($flagRow){
	    		$html.= '<div class="row" style="margin-top:8px">';
	    			$html.= '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">';
	    				$html.= '<hr style="border: 1px solid gray">';
	    			$html.= '</div>';
	    			$html.= '<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">';
	    				$html.= '<a class="btn btn-block primary-back" style="color: white">OR</a>';
	    			$html.= '</div>';
	    			$html.= '<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">';
	    				$html.= '<hr style="border: 1px solid gray">';
	    			$html.= '</div>';
	    		$html.= '</div>';
	    	}
    		$html.= '<div class="row containerSegmento align-items-center">';
    			if($flagBtnDelete){
	    			$html.='<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-right">';
	    				$html.='<a href="javascript:;" class="btn" style="box-shadow: none; color:gray">';
	    					$html.='<i class="fas fa-trash-alt"></i>';
	    				$html.='</a>';
	    			$html.='</div>';
	    		}
	    		$html.='<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">';
	    			$html.='<p><b>Elige una categoría:</b></p>';
					$html.='<select name="categoria[]" required="required" class="form-control show-tick" title=" -- Elige una categoría -- " data-show-subtext="true">';
						foreach ($objeto as $key) {
							$html.='<option value="'.$key['id_categoria'].'" data-subtext="'.$key['descripcion_categoria'].'">'.$key['nombre_categoria'].'</option>';
	    				}	    				
	    			$html.='</select>';
	    		$html.='</div>';
	    	$html.='</div>';
    	$html.='</div>';			
		
		return $html;
	}
}

?>