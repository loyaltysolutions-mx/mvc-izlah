<?php
    session_start();

    include('../../inc/parametros.php');
    require("../../controller/administracion/c_admin_registros.php");

    $inst = new C_admin_registros($ser,$usu,$pas,$bd);
    $idSucursal = isset($_SESSION['id_usuario']) ? $_SESSION['id_usuario'] : NULL;

    $registros = $inst->getRegistros($idSucursal);
    
?>
<div class="modal fade margin-fix" id="modalPush" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-notify modal-success" role="document">
        <!--Content-->
        <div class="modal-content">
            <form id="formDelete">
                <!--Header-->
                <div class="modal-header primary-back">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">&times;</span>
                    </button>
                    <h4 class="second-color">Eliminar registro</h4>                    
                </div>
                <!--Body-->
                <div class="modal-body" id="bodyModalPasswd">                
                    <div class="text-center">
                        <i class="fas fa-trash fa-4x mb-3 animated rotateIn"></i><br><br>
                        <p class="text_color" style="text-align:center"><b>Nota:</b> <u>Al eliminar el registro se le descontarán al cliente los puntos obtenidos.</u><br>¿Deseas continuar?</p>
                        <small class="second-color"><b><u>Ésta operación no se puede deshacer</u></b></small>
                        <input type="hidden" name="idRegistro" required />
                    </div>
                </div>
                <!--Footer-->
                <div class="modal-footer justify-content-center">
                    <button type="submit" id="btnYes" data-action="1" class="btn btn_color"><i class="glyphicon glyphicon-ok"></i> Eliminar </button>
                    <button type="button" class="btn btn_color btn-outline-success waves-effect" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Cancelar</button>
                </div>
            </form>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- Central Modal Medium Success -->
<div class="modal fade margin-fix" id="modalPromocion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-notify modal-success" role="document">
        <!--Content-->
        <div class="modal-content">
            <form id="formPromocion">
                <!--Header-->
                <div class="modal-header primary-back">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">&times;</span>
                    </button>
                    <h4 class="second-color">Editar Registro</h4>
                </div>
                <!--Body-->
                <div class="modal-body">
                    <div class="container-fluid">                        
                        <div class="row" id="rowModalBody"></div>                        
                        <p class="second-color" style="text-align:center"><b>Importante:</b> <u>Al editar un registro también se modificarán los puntos/visitas acumulados.</u></p>
                    </div>
                </div>
                <!--Footer-->
                <div class="modal-footer justify-content-center">                    
                    <button type="submit" id="btnYes1" data-action="1" class="btn btn_color"><i class="glyphicon glyphicon-ok"></i> Confirmar</button>
                    <button type="button" class="btn btn_color btn-outline-success waves-effect" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Cancelar</button>
                </div>
            </form>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- Editable -->
<div class="card">            
    <div class="header">
        <h2 class="text_color"><i class="fas fa-clipboard-list"></i> Historial de Registros <small> Lista de transacciones realizadas en la plataforma. </small></h2>
    </div>
    <div class="body">
        <div class="table-responsive">
            <table id="tableRegistros" class="table table-bordered table-striped table-hover">
                <thead>
                    <tr>                        
                        <th>ID Registro</th>                        
                        <th>Usuario</th>
                        <th>Monto</th>
                        <th>Ticket</th>
                        <th>Tipo</th>
                        <th>Producto</th>
                        <th>Promoción</th>
                        <?php if($registros['tipo_proyecto'] != 2 ){ ?> 
                        <th>Puntos</th> 
                        <?php }else{ ?>
                        <th>Visitas</th>
                        <?php } ?>
                        <th>Punto de Venta</th>
                        <th>Fecha/Hora Registro</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($registros['registros'] as $index => $registro) { ?>
                    <tr>                        
                        <td><?php echo $registro['id_registro_acumulacion'] ?></td>                        
                        <td><?php echo $registro['nombre'] ?></td>
                        <td><?php echo ($registro['monto_ticket'] != "" ? "$" : "").$registro['monto_ticket'] ?></td>
                        <td><?php echo $registro['ticket'] ?></td>
                        <td><?php echo $registro['nombreTipo'] ?></td>
                        <td><?php echo isset($registro['nombreProducto']) ? $registro['nombreProducto'] : "" ?></td>
                        <td><?php echo isset($registro['nombrePromo']) ? $registro['nombrePromo'] : "" ?></td>
                        <?php if(isset($registros['tipo_proyecto']) && $registros['tipo_proyecto'] != 2 ){ ?> 
                            <td><?php echo $registro['id_tipo_registro'] == "2" ? "-".$registro['puntos'] : ($registro['puntos'] != 0 ? "+" : "").$registro['puntos'] ?></td> 
                        <?php }else{ ?>
                            <td><?php echo $registro['num_visitas'] ?></td>
                        <?php } ?>
                        <td><?php echo $registro['pos'] ?></td>
                        <td><?php echo $registro['fecha_registro'] ?></td>
                        <td><a href="javascript:;" class="btn btn-primary btnEdit" data-toggle="tooltip" data-id="<?php echo $registro['id_registro_acumulacion'] ?>" data-placement="top" title="Editar"><i class="fas fa-pen"></i></a>&nbsp;<a class="btn btn-danger btnDelete" href="javascript:;" data-id="<?php echo $registro['id_registro_acumulacion'] ?>" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fas fa-trash-alt"></i></a></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>            
        </div>        
    </div>   
</div>


<script src="../../inc/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="../../inc/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<!-- Bootstrap Notify Plugin Js -->
<script src="../../inc/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<!-- Editable Table Plugin Js -->
<!--script src="../../inc/plugins/editable-table/mindmup-editabletable.js"></script-->



<script src="../../inc/js/admin_registros.js"></script>




