<?php
include_once '../../inc/parametros.php';
include_once '../../model/administracion/m_admin_menu.php';

$model = new M_admin_menu();

$file = $_FILES;

if(isset($_POST)){
	$post = $_POST;

	switch ($post['op']) {
		case '1':
		case 1:
			$nombre = $post['element'];
			$detalle = $post['description'];
			$category = $post['category'];
			$price = $post['price'];
			//$menu,$cat,$descript,$price,$filename
			$idMenu = $model->addmenu($ser, $usu, $pas, $bd, $nombre, $category, $detalle, $price, null);
			//$idMenu = $idMenu->insert_id;

			if(isset($file['img_menu'])){
				$errors= array();
				$file = $file['img_menu'];
				$path = '../../inc/imagenes/img_menu/';

				$file_size = $file['size'];
				$file_tmp = $file['tmp_name'];
				$file_type= $file['type'];
				$file_ext=strtolower(end(explode('.',$file['name'])));
				$file_name = "menu_".$idMenu."_".date("Ymd_His").".".$file_ext;
				$routeFile = $path.$file_name;
				$extensions= array("jpg","jpeg","png","gif");

				//print_r($errors);
				//print_r($errors);

				if(in_array($file_ext,$extensions)=== false){
					$errors[]="Extension no válida, por favor elige un archivo con extension válida";
				}

				$array = array('imagen' => $file_name);

				if(empty($errors)==true){
					if(move_uploaded_file($file_tmp,$routeFile)){
						//die("Entro");
						$updateImg = $model->updateMenu($ser, $usu, $pas, $bd, $idMenu, $array);
						echo json_encode(array('result' => $updateImg ? true : false));
						die();
					}else{
						die("No se pudo mover la img");
					}				
				}

				
			}

			echo json_encode(array('result' => $idMenu > 0 ? true : false));
			die();	
		break;
		
		case 2:
		case '2':

			if(isset($file['file_menu'])){
				$errors= array();
				$file = $file['file_menu'];
				$path = "../../inc/xls/";

				$file_size = $file['size'];
				$file_tmp = $file['tmp_name'];
				$file_type= $file['type'];
				$file_ext=strtolower(end(explode('.',$file['name'])));
				$file_name = "upload_menu_".date("Ymd_His").".".$file_ext;
				$routeFile = $path.$file_name;
				$extensions= array("xls","xlsx");

				if(in_array($file_ext,$extensions)=== false){
					$errors[]="Extension no válida, por favor elige un archivo con extension .xls o .xlsx";
				}

				if(empty($errors)==true){
					if(move_uploaded_file($file_tmp,$routeFile)){
			  			include '../../inc/vendor/excel/PHPExcel/IOFactory.php';
						$inputFileName = $routeFile;
						try {
							$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
						} catch(PHPExcel_Reader_Exception $e) {
							die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
						}
						//die("Entro");

						$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

						$drawing1 = $objPHPExcel->getActiveSheet()->getDrawingCollection();
						foreach ($sheetData as $key => $value) {
							if(isset($drawing1[$key-1])){				      
								$drawing = $drawing1[$key-1];
								if ($drawing instanceof PHPExcel_Worksheet_MemoryDrawing){
									ob_start();
									call_user_func(
										$drawing->getRenderingFunction(),
										$drawing->getImageResource()
									);

							  		$imageContents = ob_get_contents();
							  		ob_end_clean();
							  		$extension = 'png';
								}else{
							  		$zipReader = fopen($drawing->getPath(),'r');
								  	$imageContents = '';

								  	while (!feof($zipReader)){
										$imageContents .= fread($zipReader,1024);
								  	}
								  	fclose($zipReader);
								  	$extension = $drawing->getExtension();
								}
								//die("exist");

								$pathImg = '../../inc/imagenes/img_menu/';								
								$myFileName = 'menu_'.uniqid().'.'.$extension;
								//echo "Name:".$drawing->getName();
								//echo "Description:".$drawing->getDescription();
								file_put_contents($pathImg.$myFileName,$imageContents);
							}else{
								$myFileName = "no_image.png";
							}

						    //$tipoProyecto = $this->projectype();
						    $nombre = addslashes(utf8_decode($value['A']));
						    $description = addslashes(utf8_decode($value['B']));
						    $category = addslashes(utf8_decode($value['C']));
						    $price = addslashes(utf8_decode($value['D']));
						    $imagen = $myFileName;
						    
						    $result = $model->addmenu($ser,$usu,$pas,$bd,$nombre, $category, $description, $price, $imagen);
						    if($result < 1){break;}
				  		}
				  
				  		echo json_encode(array('result' => $result > 0 ? true : false));
				  		die();
				  		//die("Result: ".$result);
					}
				    
				}else{
					print_r($errors);
				}
			}

		break;

		case 3:
			//die("Entro");
			//print_r($file);
			//die();

			if(isset($file['img_menu']) && isset($post['idMenu']) ){
				
				$idMenu = $post['idMenu'];
				$errors= array();
				$file = $file['img_menu'];
				$path = '../../inc/imagenes/img_menu/';

				$file_size = $file['size'];
				$file_tmp = $file['tmp_name'];
				$file_type= $file['type'];
				$file_ext=strtolower(end(explode('.',$file['name'])));
				$file_name = "menu_".$idMenu."_".date("Ymd_His").".".$file_ext;
				$routeFile = $path.$file_name;
				$extensions= array("jpg","jpeg","png","gif");

				//print_r($errors);
				//print_r($errors);

				if(in_array($file_ext,$extensions)=== false){
					$errors[]="Extension no válida, por favor elige un archivo con extension válida";
				}

				$array = array('imagen' => $file_name);

				if(empty($errors)==true){
					if(move_uploaded_file($file_tmp,$routeFile)){
						//die("Entro");
						$updateImg = $model->updateMenu($ser, $usu, $pas, $bd, $idMenu, $array);
						echo json_encode(array('result' => $updateImg ? true : false));
						die();
					}
				}else{
					print_r($errors);
					die();
				}

			}
			
		break;
	}
}

echo json_encode(array('result' => false));
die();


