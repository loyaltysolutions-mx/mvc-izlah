<?php
    include_once '../../inc/parametros.php';
    include_once '../../controller/administracion/c_admin_segmento.php';
 
    $instanceSegmento=new C_admin_segmento($ser,$usu,$pas,$bd);
    $id = $_GET['id'];
    $usuario= $instanceSegmento->getInfoUser($id);        
    //print_r($usuario);
    //die();
    session_start();
    if(isset($_SESSION["usuario"])){ 
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Configuración - Plataforma de Lealtad</title>
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../../inc/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Custom Css -->
    <link href="../../inc/css/style.css" rel="stylesheet"/>   

    
    <link rel="stylesheet" href="../../inc/css/sweet-alert2.css" />
     
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link href="../../inc/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet"/>
</head>
<body>
    <div class="container">
        <br>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2 style="color:#0095f5;"><i class="fas fa-user-circle"></i> Información General</h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">person</i>
                                    </span>
                                    <div class="form-line">                                
                                        <input type="text" class="form-control" readonly id='nombreUser' value="<?php echo $usuario['info'][0]['nombre'] ?>">
                                    </div>                    
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">wc</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" readonly id='genero' value="<?php echo $usuario['info'][0]['genero'] == 0 ? "Mujer" : ($usuario['info'][0]['genero'] == 1 ? "Hombre" : "") ?>">
                                    </div>                    
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">mail</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" readonly id='email' value="<?php echo $usuario['info'][0]['email'] ?>">
                                    </div>                    
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">phone</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" readonly id='telefono' value="<?php echo $usuario['info'][0]['telefono'] ?>">
                                    </div>                    
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="fas fa-credit-card"></i>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" readonly id='tarjeta' value="<?php echo $usuario['info'][0]['tarjeta'] ?>">
                                    </div>                    
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">cake</i>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" readonly id='fechaNacimiento' value="<?php echo $usuario['info'][0]['fecha_nacimiento'] ?>">
                                    </div>                    
                                </div>                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="card" style="height:100%">
                    <div class="header">
                        <h2 style="color:#0095f5;"><i class="fas fa-chart-line"></i> Actividad del cliente </h2>
                    </div>
                    <div class="body">                                              
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <?php if( isset($usuario['tipo_proyecto'][0]['id_tipo_proyecto']) && $usuario['tipo_proyecto'][0]['id_tipo_proyecto'] != 3 ){ ?>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span><i class="fas fa-file-invoice-dollar"></i> Puntos Totales:</span>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" readonly value="<?php echo $usuario['puntos'][0]['puntos_totales'] ?>">
                                    </div>                    
                                </div>
                                <?php } ?>
                                <?php if( isset($usuario['tipo_proyecto'][0]['id_tipo_proyecto']) && $usuario['tipo_proyecto'][0]['id_tipo_proyecto'] == 3 ){ ?>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span><i class="fas fa-street-view"></i> Visitas Totales:</span>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" readonly value="<?php echo $usuario['puntos'][0]['visitas_totales'] ?>">
                                    </div>                    
                                </div>
                                <?php } ?>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span><i class="fas fa-donate"></i> Promedio de compra:</span>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" readonly value="$<?php echo $usuario['promedio'][0]['promedio'] ? $usuario['promedio'][0]['promedio'] : "0.00" ?>">
                                    </div>                    
                                </div>                                
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span><i class="fas fa-shopping-cart"></i> Compras realizadas los últimos 6 meses:</span>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" readonly value="<?php echo count($usuario['actividad']) ?>">
                                    </div>                    
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span><i class="fas fa-calendar-day"></i> Días transcurridos de la última compra:</span>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" readonly value="<?php echo $usuario['diasUltimaCompra'][0]['dias'] ? $usuario['diasUltimaCompra'][0]['dias']." día(s)" : " No Aplica " ?>">
                                    </div>                    
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2 style="color:#0095f5;"><i class="fas fa-arrow-up"></i> Información de Nivel</h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span><i class="fas fa-signature"></i> Nombre Nivel:</span>
                                    </span>
                                    <div class="form-line">                                
                                        <input type="text" class="form-control" readonly id='nombreNivel' value="<?php echo $usuario['info'][0]['nombreNivel'] ?>">
                                    </div>                    
                                </div>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <span><i class="fas fa-signature"></i> Descripción del nivel:</span>
                                    </span>
                                    <div class="form-line">
                                        <input type="text" class="form-control" readonly id='inicioNivel' value="<?php echo $usuario['info'][0]['ini'] ?> - <?php echo $usuario['info'][0]['fin'] ?> <?php echo $usuario['info'][0]['nombreProyecto'] ?> ">
                                    </div>                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2 style="color:#0095f5;"><i class="fas fa-chart-pie"></i> Segmentos Asignados</h2>
                    </div>
                    <div class="body">
                        <?php foreach ($usuario['segmentos'] as $key) { ?>
                            <div class="card">
                                <div class="header">
                                    <h2 style="color:#0095f5;"><i class="fas fa-info-circle"></i> Información</h2>
                                </div>
                                <div class="body">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <span><i class="fas fa-signature"></i> Nombre Segmento:</span>
                                                </span>
                                                <div class="form-line">
                                                    <input type="text" class="form-control" readonly id='nombreSegmento' value="<?php echo $key['nombre_segmento'] ?>">
                                                </div>                    
                                            </div>
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <span><i class="fas fa-signature"></i> Descripción:</span>
                                                </span>
                                                <div class="form-line">
                                                    <input type="text" class="form-control" readonly id='descripcionSegmento' value="<?php echo $key['descripcion_segmento'] ?>">
                                                </div>                    
                                            </div>                                                   
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>            
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">                
                <div class="card">
                    <div class="header">
                        <h2 style="color:#0095f5;"><i class="fas fa-cash-register"></i> Registro de Actividades</h2>
                    </div>
                    <div class="body">                
                        <div class="table-responsive">
                            <table id="tableActividades" class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nombre Punto Venta</th>                                
                                        <th>Ticket</th>
                                        <th>Monto Ticket</th>
                                        <th>Puntos</th>
                                        <th>Tipo Registro</th>
                                        <th>Dia Registro</th>
                                        <th>Fecha/Hora Registro</th>
                                    </tr>                                    
                                </thead>
                                <tbody>
                                    <?php foreach ($usuario['registros'] as $registro) { ?>
                                        <tr>
                                            <td><?php echo $registro['id_registro_acumulacion'];  ?></td>
                                            <td><?php echo $registro['usuario'];  ?></td>
                                            <td><?php echo $registro['ticket'];  ?></td>
                                            <td><?php echo $registro['monto_ticket'] != "" ? "$".$registro['monto_ticket'] : "";  ?></td>                                    
                                            <td><?php echo $registro['id_tipo_registro'] && $registro['puntos'] != "" ?($registro['id_tipo_registro'] == 2 ? "-":"+").$registro['puntos'] : "" ;  ?></td>                                    
                                            <td><?php echo $registro['nombreTipo']?></td>
                                            <td><?php echo $registro['dia_registro']?></td>
                                            <td><?php echo $registro['fecha_registro']?></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        <div class="row justify-content-center">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                <a href="javascript:;" class="btn btn-primary" onclick="window.close();" style="width:100%"> Cerrar Ventana </a>
            </div>
        </div>
        <br>
        

    <script src="../../inc/js/jquery.js"></script>
    <script src="../../inc/plugins/jquery-datatable/jquery.dataTables.js"></script>
    <script src="../../inc/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
    <script src="../../inc/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
    <script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
    <script src="../../inc/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
    <script src="../../inc/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
    <script src="../../inc/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
    <script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
    <script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
    

    <!-- Moment Plugin Js -->
    <script src="../../inc/plugins/momentjs/moment.js"></script>
    <script src="../../inc/plugins/momentjs/moment-with-locale.js"></script>    

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="../../inc/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>    
    <script>
        if ( $.fn.dataTable.isDataTable( '#tableActividades' ) ) {
            let table = $('#tableActividades').DataTable();
            table.destroy();
        }
        $("#tableActividades").DataTable({
            //'searching': false
        });
    </script>
    <!--script src="../../inc/js/instascan.min.js"></script>    
    <script type="text/javascript">      
      let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
      //console.log(Instascan.Camera.getCameras());
      scanner.addListener('scan', function (content, image) {
        //console.log(content);
        alert(content);
      });

      Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
            console.log(cameras);        
            scanner.start(cameras[1], function(){
                console.log("inicio");
            });
        }
      });
    </script-->
</body>
</html>

<?php }?>