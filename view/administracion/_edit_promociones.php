<?php
include_once '../../controller/administracion/c_admin_promociones.php';
$grid = new C_admin_promociones($ser,$usu,$pas,$bd);
?>
<script src="../../inc/js/edit_promociones.js"></script>
<div class="card">
    <div class="body">
    <!-- Edit Menù table -->
        <div class="row">
            <div class="col-sm-12 col-md-6 col-lg-6">
                <div class="header">
                    <h2 class="text_color"><i class="fas fa-tags"></i> Edición de Promociones <small>Edita una Promoción</small></h2>
                </div>
                <i class="fas fa-info-circle" rel="popover" title="En la parte de 'Arriba' o 'Izquierda' se encuentran las Sucursales o Segmentos sin seleccionar" data-content="En la parte de 'Abajo' o 'Derecha' se encuentran las sucursales o Segmentos seleccionados"></i>
            </div>
            <div class="col-sm-12 col-md-6 col-lg-6">
                <button class="btn btn-block btn-lg btn_color waves-effect" onclick="menu('administracion/admin_promociones')">Nueva Promoción</button>
            </div>
        </div>
        <div class="table-responsive">
            <table id="mainTable" class="table table-bordered table-striped table-hover dataTable js-exportable">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Detalle</th>
                        <th>Texto Corto</th>
                        <th>Sucursal(es)</th>
                        <th>Segmento(s)</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                        echo $grid->grideditp('tr');
                    ?>
                </tbody>
            </table>
        </div>
        <!-- #END# Exportable Table -->
    </div>   
</div>
<?php
 include '../../inc/datatables.php';
?>
<script src="../../inc/plugins/multi-select/js/jquery.multi-select.js"></script>
<script src="../../inc/js/pages/ui/tooltips-popovers.js"></script>
<script>
    //multi select initiate
    $('.sucursal').multiSelect();
    $('.segment').multiSelect();

    $(document).ready(function() {
        $("i.fas").popover({'trigger':'hover'});
    });

</script>