<?php 
/*************************************************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#- Vista de Login  -#
##################################################################################### 
include_once '../../inc/cont_fijos.php';
include_once '../../inc/funciones.php';
$ins_cont_fijos=new Contenidos_Fijos($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <?php  $ins_cont_fijos->head(); ?>
</head>
<style>
 <?php
 $res_con=$ins_funciones->consulta_generica('tbl_estilo', ' ');
 $registro= mysqli_fetch_assoc($res_con);
 //VALIDAMOS LOGO
if($registro['logo_cte']==''){
    $img_logo='imagenes/logo.png';
}else{
    $img_logo='imagenes/img_configuracion/'.$registro['logo_cte'];
}
 //VALIDAMOS IMAGEN FONDO
if($registro['fondo_cte']==''){
     $img_background='imagenes/background_default.jpg';
}else{
    $img_background='imagenes/img_configuracion/'.$registro['fondo_cte'];
}


  
    
    ?>
.home{
    background: url("../../inc/<?php echo $img_background; ?>")no-repeat fixed!important;
    background-size:cover!important;    
      }
    
</style>
<body class='home'> 
    <div class="row">
        <div class="col-md-12 text-center ">
            <img style='width: 90px; height: 90px;' class="img_logo" src="../../inc/<?php echo $img_logo; ?>">
            <h4 class='azul ' style="  color:black!important;">Adminsitrador Plataforma de Lealtad</h4>
	</div>
    </div>
    <div class="row">
	<div class="col-md-4 text-center "></div>
	<div class="well col-md-4  login-box transparencia">
            <form  action="../../controller/login/c_login.php?op=1" method="post">
		<fieldset>
                    <legend class='text-left '><h6>Ingrese los datos para el Acceso</h6></legend>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon  glyphicon-user"></i></span>
			<input name="usuario" type="text" class="form-control" placeholder="Usuario" required>
                    </div><br>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
			<input name="password" type="password" class="form-control" placeholder="Password" required>
                    </div><br>
                    <div class="input-group " ><br>
                        <button  name="iniciar"   type="submit" class="btn btn-primary  btn-sm"><i class="glyphicon glyphicon-log-in"></i> &nbsp;Ingresar</button>
                    </div>
                    <div class="input-group " ><br>
                        <?php  
                            $res=$_REQUEST['msg'];
                            if(isset($res) && $res==1){ ?>
                        <div class="alert alert-danger" role="alert" style='width: 100%;margin-top: 8px;'>
                                     Usuario y/o Password incorrecto
                                 </div>
                                                      <?php  }
                        ?>
                    </div>  
		</fieldset>
            </form>
	</div>
	<div class="col-md-4 text-center "></div>
    </div>
</body>
</html>

