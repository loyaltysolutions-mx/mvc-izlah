<!-- Developed by Allan Ayrton -->
<!-- view/login.php -->
<?php
require_once('../../controller/login/c_login.php');
$confirm = new Acceso($ser,$usu,$pas,$bd);
?> 
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Admin of</title>
    <!-- Favicon-->
    <link rel="icon" href="../../inc/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../../inc/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../../inc/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../../inc/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../../inc/css/style.css" rel="stylesheet">
</head>
<body class="login-page">
    <div class="menu-top">
        
    </div>
    <div class="login-container">
        <div id="login-box" class="login-box">
            <div id="login-card" class="card">
                <div class="logo">
                    <a href="javascript:void(0);"></a>
                </div>
                <div class="align-center p-t-10">
                    <h3 class="msg"><b>Bienvenido</b></h3>
                </div>
                <div class="body">
                    <form id="sign_in" action="../../controller/login/c_login.php?op=1" method="POST">
                        <?php  
                            $res=$_REQUEST['msg'];
                            if(isset($res) && $res==10){
                                echo "<div class='msg'>Registro Correcto, por favor active su cuenta</div>";
                            }else if($res==11){
                                echo "<div class='msg'>Hubo un error en el registro</div>";
                            }else if($res==12){
                                echo "<div class='msg'>El Usuario ya existe, por favor ingrese o pruebe con otro correo</div>";
                            }else if($res==13){
                                echo "<div class='msg'>Se ha enviado un correo para recuperar tu cuenta</div>";
                            }else if($res==14){
                                echo "<div class='msg'>Error o correo inexistente</div>";
                            }
                        ?>
                        <div class="msg">Ingrese los datos para el Acceso...</div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">person</i>
                            </span>
                            <div class="form-line">
                                <input type="text" class="form-control" name="user" placeholder="Usuario" required autofocus>
                            </div>
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="material-icons">lock</i>
                            </span>
                            <div class="form-line">
                                <input type="password" class="form-control" name="password" placeholder="Password" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8 p-t-5">
                               
                            </div>
                            <div class="col-xs-12 col-md-4">
                                <button class="btn btn-block bg-black waves-effect" type="submit">Ingresar</button>
                            </div>
                        </div>
                        <div class="input-group " ><br>
    							<?php  
                                    $res=$_REQUEST['msg'];
                                    if(isset($res) && $res==1){
                                        echo "<label class='red'>Usuario y/o Password incorrecto</label>";
                                    }
                                    $user = $_REQUEST['us'];
                                    $key = $_REQUEST['key'];
                                    if(isset($key)){
                                        $confirm->confirm($user,$key);
                                    }
                                ?>
    					</div>
                        <!--div class="row m-t-15 m-b--20">
                            <div class="col-xs-6">
                                <a href="#" onclick="gotpage('sign-up');">¡Registrate Ahora!</a>
                            </div>
                            <div class="col-xs-6 align-right">
                                <a href="#" onclick="gotpage('recovery')">¿Olvidaste tu Password?</a>
                            </div>
                        </div-->
                    </form>
                </div>
            </div>
        </div>
    </div>  
    <!-- Jquery Core Js -->
    <script src="../../inc/plugins/jquery/jquery.min.js"></script>

    <!-- Custom Js -->
    <script src="../../inc/js/custom.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="../../inc/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="../../inc/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="../../inc/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="../../inc/js/admin.js"></script>
    <script src="../../inc/js/pages/examples/sign-in.js"></script>
</body>

</html>