<?php 
/*************************************************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#- Vista de Home -#
##################################################################################### 
include_once '../../inc/cont_fijos.php';
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
include_once '../../controller/recupera/c_recupera.php';
$ins_control_registro=new C_recupera($ser,$usu,$pas,$bd);
$ins_cont_fijos=new Contenidos_Fijos($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
  
<?php  
 $ins_cont_fijos->head();
 //TRAEMOS CONFIGURACION DE ESTILOS
 $res_con=$ins_funciones->consulta_generica('tbl_estilo', ' ');
 $registro= mysqli_fetch_assoc($res_con);
 //VALIDAMOS LOGO
if($registro['logo_cte']==''){
    $img_logo='imagenes/logo.png';
}else{
    $img_logo='imagenes/img_configuracion/'.$registro['logo_cte'];
}
 //VALIDAMOS IMAGEN FONDO
if($registro['fondo_cte']==''){
     $img_background='imagenes/background_default.jpg';
}else{
    $img_background='imagenes/img_configuracion/'.$registro['fondo_cte'];
}
 //VALIDAMOS COLOR PRIMARIO
if($registro['color_primario']==''){
     $color_primario='#006AA9';
}else{
    $color_primario=$registro['color_primario'];
}
 //VALIDAMOS COLOR SECUNDARIO
if($registro['color_secundario']==''){
     $color_secundario='#006AA9';
}else{
    $color_secundario=$registro['color_secundario'];
}
  ?>
 <script type="text/javascript" src='../../inc/js/recovery.js'></script>
 <style>
 section.content {
    margin: 100px 15px 0 0px;
    -moz-transition: 0.5s;
    -o-transition: 0.5s;
    -webkit-transition: 0.5s;
    transition: 0.5s;
}
</style>
</head>
<?php
//LLAMAMOS ESTILOS DE CONFIGURACION
$ins_cont_fijos->estilos($img_logo,$img_background,$color_primario,$color_secundario);
$email=$_GET['email'];
$token=$_GET['token'];
if($email!=''){ 
 $res_token=$ins_funciones->consulta_generica("tbl_usuario", ' where email = "'.$email.'" AND token = "'.$token.'"'); 
 $recovery= mysqli_fetch_assoc($res_token);
 if(count($recovery)>0){ ?>
<body class="theme-red">
    <!-- Pag. Loading-->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Espera un poco...</p>
        </div>
    </div>
    <!-- Barra Ariba -->
    <nav class="navbar" style='height: 70px;'> 
        <div class="container-fluid">
            <div class="navbar-header">
                <span class="navbar-brand" >Plataforma Lealtad </span>
            </div>
        </div>
    </nav>
<!-- CONTENIDO -->
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2><i class="far fa-id-badge"></i> RESTABLECER CONTRASEÑA </h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            Ingresa la nueva contraseña para reestablecer tu cuenta<br>
                        </div>
                        <div class="body">
                            <form id='form_confirma_registro' method="POST">
                                    <div class="row clearfix">
                                        <div class="col-sm-12">
                                            <h2 class="card-inside-title">Correo:<span style='font-size: 10px;' ></span></h2> 
                                            <div class="form-group">
                                                <div>
                                                    <input  type='hidden' id='id_usu' value="<?php  echo $recovery['id_usuario']; ?>">
                                                    <input type="text"  class="form-control" readonly value="<?php  echo $recovery['email']; ?>">
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <h2 class="card-inside-title">Ingrese Nueva Contraseña:<span style='font-size: 10px;'></span></h2> 
                                            <div class="form-group">
                                                <div>
                                                    <input type="password" id='pass' class="form-control"  name='password' required /><br>
                                                    <span id="textPass" style="color:red"></span>
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <h2 class="card-inside-title">Confirme Nueva Contraseña:<span style='font-size: 10px;'></span></h2> 
                                            <div class="form-group">
                                                <div>
                                                    <input type="password" id='retype_pass' class="form-control"  name='retype_password' required /><br>
                                                    <span id="textRetype" style="color:red"></span>
                                                    <hr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                 <input type="hidden" class="form-control "  name='clave' value="<?php  echo  $clave ?>">
                                 <div id='loading'><img src="../../inc/imagenes/load.gif"></div>
                                <button id='confirma_contrasena' type="button" class="btn btn-primary m-t-15 waves-effect " >Actualizar Password</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </section>
</body>
</html> 
    <?php 
 }else{ ?>
   <body class="four-zero-four">
        <div class="four-zero-four-container">
            <div class="error-code"><i class="fas fa-bomb"></i></div><br><br>
            <div class="error-message">Token inexistente</div>
        </div>
    </body>
<?php  } 
 }else{ ?>
    <body class="four-zero-four">
        <div class="four-zero-four-container">
            <div class="error-code"><i class="fas fa-bomb"></i></div><br><br>
            <div class="error-message">Faltan Parametros</div>
        </div>
    </body>
<?php }
?>