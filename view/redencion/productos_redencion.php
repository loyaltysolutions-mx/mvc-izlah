
<?php 
/*************************************************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#- Vista de Redencion -#
##################################################################################### 
include_once '../../inc/cont_fijos.php';
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
include_once '../../controller/redencion/c_redencion.php';
$ins_control_redencion=new C_redencion($ser,$usu,$pas,$bd);
$ins_cont_fijos=new Contenidos_Fijos($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
$id_usua=$_REQUEST["id_usu"];
 //TRAEMOS CONFIGURACION DEL PROYECTO
 $qr_config_proy=$ins_funciones->consulta_generica_all(' select * from tbl_configuracion_proyecto');
 $reg_config= mysqli_fetch_assoc($qr_config_proy);
 if($reg_config['id_tipo_proyecto']==3){//PROYECTO POR VISITAS
	 //TRAEMOS INFORMACION DE USUARIO 
 $qr_visitas_acumuladas=$ins_funciones->consulta_generica_all(' select * from tbl_puntos_totales where id_usuario='.$id_usua.' and id_tipo_puntos=1');
 $visitas_totales_acumuladas= mysqli_fetch_assoc($qr_visitas_acumuladas);
 
 $qr_ptos_redimidos=$ins_funciones->consulta_generica_all(' select * from tbl_puntos_totales where id_usuario='.$id_usua.' and id_tipo_puntos=2');
 $ptos_totales_redimidos= mysqli_fetch_assoc($qr_ptos_redimidos);
 $res_con=$ins_funciones->consulta_generica_all('select SUM(R.puntos) as puntos_acumulados,U.usuario,U.id_usuario  from tbl_registros as R
                                                inner join tbl_usuario as U on R.id_usuario=U.id_usuario
                                                 where R.id_tipo_registro=1 and R.id_usuario='.$id_usua);
 $ptos_acumu= mysqli_fetch_assoc($res_con);
 $res_con2=$ins_funciones->consulta_generica_all('select SUM(puntos) as puntos_redimidos from tbl_registros where id_tipo_registro=2 and id_usuario='.$id_usua);
 $ptos_redim= mysqli_fetch_assoc($res_con2);
	 
	 
 }else{//PROYECTO POR PUNTOS
	//TRAEMOS INFORMACION DE USUARIO 
 $qr_ptos_acumulados=$ins_funciones->consulta_generica_all(' select * from tbl_puntos_totales where id_usuario='.$id_usua.' and id_tipo_puntos=1');
 $ptos_totales_acumulado= mysqli_fetch_assoc($qr_ptos_acumulados);
 $qr_ptos_redimidos=$ins_funciones->consulta_generica_all(' select * from tbl_puntos_totales where id_usuario='.$id_usua.' and id_tipo_puntos=2');
 $ptos_totales_redimidos= mysqli_fetch_assoc($qr_ptos_redimidos);
 $res_con=$ins_funciones->consulta_generica_all('select SUM(R.puntos) as puntos_acumulados,U.usuario,U.id_usuario  from tbl_registros as R
                                                inner join tbl_usuario as U on R.id_usuario=U.id_usuario
                                                 where R.id_tipo_registro=1 and R.id_usuario='.$id_usua);
 $ptos_acumu= mysqli_fetch_assoc($res_con);
 $res_con2=$ins_funciones->consulta_generica_all('select SUM(puntos) as puntos_redimidos from tbl_registros where id_tipo_registro=2 and id_usuario='.$id_usua);
 $ptos_redim= mysqli_fetch_assoc($res_con2);

 }
 
session_start();
if(isset($_SESSION["usuario"])){ 
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?php  
 $ins_cont_fijos->head();
 //TRAEMOS CONFIGURACION DE ESTILOS
 $res_con=$ins_funciones->consulta_generica('tbl_estilo', ' ');
 $registro= mysqli_fetch_assoc($res_con);
 //VALIDAMOS LOGO
if($registro['logo_cte']==''){
    $img_logo='imagenes/logo.png';
}else{
    $img_logo='imagenes/img_configuracion/'.$registro['logo_cte'];
}
 //VALIDAMOS IMAGEN FONDO
if($registro['fondo_cte']==''){
     $img_background='imagenes/background_default.jpg';
}else{
    $img_background='imagenes/img_configuracion/'.$registro['fondo_cte'];
}
 //VALIDAMOS COLOR PRIMARIO
if($registro['color_primario']==''){
     $color_primario='#006AA9';
}else{
    $color_primario=$registro['color_primario'];
}
 //VALIDAMOS COLOR SECUNDARIO
if($registro['color_secundario']==''){
     $color_secundario='#006AA9';
}else{
    $color_secundario=$registro['color_secundario'];
}
  ?>
      <script type="text/javascript" src='../../inc/js/productos_redencion.js'></script>
</head>
<?php
//LLAMAMOS ESTILOS DE CONFIGURACION
$ins_cont_fijos->estilos($img_logo,$img_background,$color_primario,$color_secundario);
?>

<body class="theme-red">
    <!-- Pag. Loading-->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Espera un poco...</p>
        </div>
    </div>
    <!-- Barra Ariba -->
    <nav class="navbar" style='height: 70px;'> 
        <div class="container-fluid">
            <div class="navbar-header">
                 <a href="javascript:void(0);" class="bars"></a>
                <span class="navbar-brand" >Plataforma Lealtad </span>
            </div>
        </div>
    </nav>
    <section>
        <!-- MeNU IZQUIERDO -->
        <aside id="leftsidebar" class="sidebar">
            <!-- Usuario-->
            <div class="user-info">
                <div class="image">
                    <img src="../../inc/imagenes/user.png" width="48" height="48" alt="User" />
                    <img src="../../inc/<?php echo $img_logo; ?>" style='    margin-left: 130px;' width="60" height="60" alt="Logo" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION["usuario"];?></div>
                    <!--<div class="email">john.doe@example.com</div>-->
                </div>
            </div>
            <!-- MENU -->
            <div class="menu">
                <ul class="list">
                    <?php echo $ins_cont_fijos->menu($_SESSION["id_usuario"]);?>
                </ul>
            </div>
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                        <?php 
                        $ins_cont_fijos->footer();
                        ?>
                </div>
            </div>
        </aside>
    </section>
<!-- CONTENIDO -->
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                    <h2><i class="fas fa-gift"></i> CANJE DE PRODUCTO</h2>
            </div>
            <div class="row clearfix">
                 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> 
                    <div class="info-box hover-zoom-effect">
                            <div class="icon bg-indigo">
                                <i class="material-icons">face</i>
                            </div>
                            <div class="content">
                                <div class="text">Usuario</div>
                                <div class="text "><b><?php echo utf8_encode(ucwords($ptos_acumu['usuario'])); ?></b></div>
                            </div>
                    </div>
                 </div>
				 <?php 
				 if($reg_config['id_tipo_proyecto']==3){//PROYECTO POR VISITAS  ?>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="info-box hover-zoom-effect">
							<div class="icon bg-pink">
							   <i class="fas fa-donate"></i>
							</div>
							<div class="content">
								<div class="text">Visitas Acumuladas</div>
								<div class="number count-to" data-from="0" data-to=" <?php  if($visitas_totales_acumuladas['visitas_totales']==''){ echo '0'; }else{ echo  $visitas_totales_acumuladas['visitas_totales']; }?>" data-speed="1000" data-fresh-interval="20"></div>
							</div>
						</div>
					</div>

				 
				 <?php }else{//PROYECTO POR PUNTOS  ?>
					 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="info-box hover-zoom-effect">
							<div class="icon bg-pink">
							   <i class="fas fa-donate"></i>
							</div>
							<div class="content">
								<div class="text">Puntos Acumulados </div>
								<div class="number count-to" data-from="0" data-to=" <?php  if($ptos_totales_acumulado['puntos_totales']==''){ echo '0'; }else{ echo  $ptos_totales_acumulado['puntos_totales']; }?>" data-speed="1000" data-fresh-interval="20"></div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="info-box hover-zoom-effect">
							<div class="icon bg-blue">
								<i class="fas fa-gift"></i>
							</div> 
							<div class="content">
								<div class="text">Puntos Redimidos</div>
								<div class="number count-to" data-from="0"  data-to="<?php  if($ptos_totales_redimidos['puntos_totales']==''){ echo '0'; }else{ echo  $ptos_totales_redimidos['puntos_totales']; }?>" data-fresh-interval="20"> </div>
							</div>
						</div>
					</div>
					 
					 
				 <?php } ?>
                
                
               
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id='loading' class="load_pantalla"><div class="preloader pl-size-xl">
                                    <div class="spinner-layer">
                                        <div class="circle-clipper left">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="circle-clipper right">
                                            <div class="circle"></div>
                                        </div>
                                    </div>
                                </div></div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="card">
                                <div class="header">
                                    <h2>
                                        Catalogo de Productos para Redimir
                                    </h2>

                                </div>
                                <br>
                                    <div class="row pd-20">
                                        <?php 
										//EVALUAMOS TIPO DE PROYECTO 
										if($reg_config['id_tipo_proyecto']==3){//PROYETO POR VISITAS
											$rest=$ins_control_redencion->trae_productos_visitas();?>
                                            <input type="hidden" id="id_cte" value='<?php echo $ptos_acumu['id_usuario']; ?>'>
                                            <input type="hidden" id="id_usu" value='<?php echo $_SESSION["id_usuario"] ?>'>
                                            <?php while($fila = $rest->fetch_assoc()){  
                                                if($visitas_totales_acumuladas['visitas_totales']>=$fila['valor_visitas']){
                                                    $estatus='';
                                                }else{
                                                    $estatus='disabled';
                                                }
                                                
                                                ?>
                                                <div class="col-sm-6 col-md-3">
                                                    <div class="thumbnail shadow " style='min-height: 390px'>
                                                        <img src="../../inc/imagenes/img_catalogo/<?php echo utf8_encode($fila['imagen']); ?>" style='  max-height: 150px; min-height: 150px;  min-height: 140px!important;'>
                                                        <div class="caption">
                                                            <h3><?php echo utf8_encode(ucwords($fila['nombre'])) ?></h3>
                                                            <h6><?php echo utf8_encode($fila['valor_visitas']) ?> Visitas</h6>
                                                            <div class="detalle_prod">
                                                                <small ><?php echo utf8_encode($fila['detalle']) ?> </small><br><br>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer">
                                                            <button class="btn btn-primary waves-effect  redimir_puntos" type="submit" <?php echo $estatus; ?> value='<?php echo $fila['id_cat_premios_productos_servicios']; ?>'><i class="fas fa-gift" ></i> Obtener</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                   <?php   } 
										
										
										
											
										}else{//PROYECTO POR PUNTO  
										    $rest=$ins_control_redencion->trae_productos();?>
                                            <input type="hidden" id="id_cte" value='<?php echo $ptos_acumu['id_usuario']; ?>'>
                                            <input type="hidden" id="id_usu" value='<?php echo $_SESSION["id_usuario"] ?>'>
                                            <?php while($fila = $rest->fetch_assoc()){  
                                                if($ptos_totales_acumulado['puntos_totales']>=$fila['valor_puntos']){
                                                    $estatus='';
                                                }else{
                                                    $estatus='disabled';
                                                }
                                                
                                                ?>
                                                <div class="col-sm-6 col-md-3">
                                                    <div class="thumbnail shadow " style='min-height: 390px'>
                                                        <img src="../../inc/imagenes/img_catalogo/<?php echo utf8_encode($fila['imagen']); ?>" style='  max-height: 150px; min-height: 150px;  min-height: 140px!important;'>
                                                        <div class="caption">
                                                            <h3><?php echo utf8_encode(ucwords($fila['nombre'])) ?></h3>
                                                            <h6><?php echo utf8_encode($fila['valor_puntos']) ?> Ptos</h6>
                                                            <div class="detalle_prod">
                                                                <small ><?php echo utf8_encode($fila['detalle']) ?> </small><br><br>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer">
                                                            <button class="btn btn-primary waves-effect  redimir_puntos" type="submit" <?php echo $estatus; ?> value='<?php echo $fila['id_cat_premios_productos_servicios']; ?>'><i class="fas fa-gift" ></i> Obtener</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                   <?php   } 	
										}?>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </section>
</body>
</html>
 <?php }else{ 
 
 
 
 //VISTA MERSION MOVIL 
?>
 <!DOCTYPE html>
<html lang="es" class="form_front chrome">
<head>
<?php  
 $ins_cont_fijos->head();
 //TRAEMOS CONFIGURACION DE ESTILOS
 $res_con=$ins_funciones->consulta_generica('tbl_estilo', ' ');
 $registro= mysqli_fetch_assoc($res_con);
 //VALIDAMOS LOGO
if($registro['logo_cte']==''){
    $img_logo='imagenes/logo.png';
}else{
    $img_logo='imagenes/img_configuracion/'.$registro['logo_cte'];
}
 //VALIDAMOS IMAGEN FONDO
if($registro['fondo_cte']==''){
     $img_background='imagenes/background_default.jpg';
}else{
    $img_background='imagenes/img_configuracion/'.$registro['fondo_cte'];
}
 //VALIDAMOS COLOR PRIMARIO
if($registro['color_primario']==''){
     $color_primario='#006AA9';
}else{
    $color_primario=$registro['color_primario'];
}
 //VALIDAMOS COLOR SECUNDARIO
if($registro['color_secundario']==''){
     $color_secundario='#006AA9';
}else{
    $color_secundario=$registro['color_secundario'];
}
  ?>
      <script type="text/javascript" src='../../inc/js/productos_redencion.js'></script>
      
      
</head>
<?php
//LLAMAMOS ESTILOS DE CONFIGURACION
$ins_cont_fijos->estilos_front($img_logo,$img_background,$color_primario,$color_secundario);
?>

<body class="theme-red">
    <!-- Pag. Loading-->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Espera un poco...</p>
        </div>
    </div>
<!-- CONTENIDO -->
  <br><br>
        <div class="container-fluid">
            <div class="block-header">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <a href='../registro/registrate.php' type="button" class="btn btn-rounded btn-default btn-circle waves-effect waves-circle waves-float">
                        <i class="fa fa-arrow-left"></i>
                    </a>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                    <img class="img_form_reg" src="../../inc/<?php echo $img_logo ;?>">
                </div>
                <h3 class="text-white"><i class="fas fa-gift text-white"></i> CANJE DE PRODUCTO</h3>
            </div>
            <div class="row clearfix">
                 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> 
                    <div class="info-box black-box hover-zoom-effect">
                            <div class="icon bg-indigo">
                                <i class="material-icons">face</i>
                            </div>
                            <div class="content">
                                <div class="text text-white">Usuario</div>
                                <div class="text text-white"><b><?php echo utf8_encode(ucwords($ptos_acumu['usuario'])); ?></b></div>
                            </div>
                    </div>
                 </div>
                <?php 
				 if($reg_config['id_tipo_proyecto']==3){//PROYECTO POR VISITAS  ?>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="info-box hover-zoom-effect">
							<div class="icon bg-pink">
							   <i class="fas fa-donate"></i>
							</div>
							<div class="content">
								<div class="text">Visitas Acumuladas</div>
								<div class="number count-to" data-from="0" data-to=" <?php  if($visitas_totales_acumuladas['visitas_totales']==''){ echo '0'; }else{ echo  $visitas_totales_acumuladas['visitas_totales']; }?>" data-speed="1000" data-fresh-interval="20"></div>
							</div>
						</div>
					</div>
				 <?php }else{//PROYECTO POR PUNTOS  ?>
					 <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="info-box hover-zoom-effect">
							<div class="icon bg-pink">
							   <i class="fas fa-donate"></i>
							</div>
							<div class="content">
								<div class="text">Puntos Acumulados </div>
								<div class="number count-to" data-from="0" data-to=" <?php  if($ptos_totales_acumulado['puntos_totales']==''){ echo '0'; }else{ echo  $ptos_totales_acumulado['puntos_totales']; }?>" data-speed="1000" data-fresh-interval="20"></div>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
						<div class="info-box hover-zoom-effect">
							<div class="icon bg-blue">
								<i class="fas fa-gift"></i>
							</div> 
							<div class="content">
								<div class="text">Puntos Redimidos</div>
								<div class="number count-to" data-from="0"  data-to="<?php  if($ptos_totales_redimidos['puntos_totales']==''){ echo '0'; }else{ echo  $ptos_totales_redimidos['puntos_totales']; }?>" data-fresh-interval="20"> </div>
							</div>
						</div>
					</div>
				 <?php } ?>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id='loading' class="load_pantalla"><div class="preloader pl-size-xl">
                                    <div class="spinner-layer">
                                        <div class="circle-clipper left">
                                            <div class="circle"></div>
                                        </div>
                                        <div class="circle-clipper right">
                                            <div class="circle"></div>
                                        </div>
                                    </div>
                                </div></div>
                    <div class="row clearfix">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="cabecera">
                                    
                                        Catalogo de Productos para Redimir
                                    <br>
                            </div>
                            <div class="card card2">
                                <div class="row clearfix text-center margin-auto" style='width: 500px;'>
                                      <h4 class="text-white"><i class="fas fa-user-lock"></i> Datos de Autorización</h4>
										<input  type="password" class="form-control text-center" id="pass_usu_autoriza" placeholder="*Password Usuario Autoriza">                    
                                  </div>
                                <br>
                                    <div class="row pd-20">
                                        <?php 
										//EVALUAMOS TIPO DE PROYECTO 
										if($reg_config['id_tipo_proyecto']==3){//PROYETO POR VISITAS
										   $rest=$ins_control_redencion->trae_productos();?>
                                            <input type="hidden" id="id_cte" value='<?php echo $ptos_acumu['id_usuario']; ?>'>
                                            <?php while($fila = $rest->fetch_assoc()){  
                                                if($visitas_totales_acumuladas['visitas_totales']>=$fila['valor_visitas']){
                                                    $estatus='';
                                                }else{
                                                    $estatus='disabled';
                                                }
                                                
                                                ?>
                                                <div class="col-sm-6 col-md-3">
                                                    <div class="thumbnail shadow round" style='min-height: 390px; border: 4px solid <?php echo $color_primario; ?> !important'>
                                                        <img src="../../inc/imagenes/img_catalogo/<?php echo utf8_encode($fila['imagen']); ?>" style='  max-height: 140px; min-height: 140px;  min-height: 140px!important;'>
                                                        <div class="caption">
                                                            <h3><?php echo utf8_encode(ucwords($fila['nombre'])) ?></h3>
                                                            <h6><?php echo utf8_encode($fila['valor_visitas']) ?> Ptos</h6>
                                                            <div class="detalle_prod">
                                                                <small ><?php echo utf8_encode($fila['detalle']) ?> </small><br><br>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer text-center">
                                                            <button class="btn waves-effect redimir_puntos_sin_sesion" style="background:<?php echo $color_primario; ?> !important" type="submit" <?php echo $estatus; ?> value='<?php echo $fila['id_cat_premios_productos_servicios']; ?>'><i class="fas fa-gift" ></i> Obtener</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                   <?php   } 
										
										
										
										
										}else{//PROYECTO POR PUNTO 
										     $rest=$ins_control_redencion->trae_productos();?>
                                            <input type="hidden" id="id_cte" value='<?php echo $ptos_acumu['id_usuario']; ?>'>
                                            <?php while($fila = $rest->fetch_assoc()){  
                                                if($ptos_totales_acumulado['puntos_totales']>=$fila['valor_puntos']){
                                                    $estatus='';
                                                }else{
                                                    $estatus='disabled';
                                                }
                                                
                                                ?>
                                                <div class="col-sm-6 col-md-3">
                                                    <div class="thumbnail shadow round" style='min-height: 390px; border: 4px solid <?php echo $color_primario; ?> !important'>
                                                        <img src="../../inc/imagenes/img_catalogo/<?php echo utf8_encode($fila['imagen']); ?>" style='  max-height: 140px; min-height: 140px;  min-height: 140px!important;'>
                                                        <div class="caption">
                                                            <h3><?php echo utf8_encode(ucwords($fila['nombre'])) ?></h3>
                                                            <h6><?php echo utf8_encode($fila['valor_puntos']) ?> Ptos</h6>
                                                            <div class="detalle_prod">
                                                                <small ><?php echo utf8_encode($fila['detalle']) ?> </small><br><br>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer text-center">
                                                            <button class="btn waves-effect redimir_puntos_sin_sesion" style="background:<?php echo $color_primario; ?> !important" type="submit" <?php echo $estatus; ?> value='<?php echo $fila['id_cat_premios_productos_servicios']; ?>'><i class="fas fa-gift" ></i> Obtener</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                   <?php   } 
										
											
											
											
										}
										
										
										/*
                                            $rest=$ins_control_redencion->trae_productos();?>
                                            <input type="hidden" id="id_cte" value='<?php echo $ptos_acumu['id_usuario']; ?>'>
                                            <?php while($fila = $rest->fetch_assoc()){  
                                                if($ptos_totales_acumulado['puntos_totales']>=$fila['valor_puntos']){
                                                    $estatus='';
                                                }else{
                                                    $estatus='disabled';
                                                }
                                                
                                                ?>
                                                <div class="col-sm-6 col-md-3">
                                                    <div class="thumbnail shadow round" style='min-height: 390px; border: 4px solid <?php echo $color_primario; ?> !important'>
                                                        <img src="../../inc/imagenes/img_catalogo/<?php echo utf8_encode($fila['imagen']); ?>" style='  max-height: 140px; min-height: 140px;  min-height: 140px!important;'>
                                                        <div class="caption">
                                                            <h3><?php echo utf8_encode(ucwords($fila['nombre'])) ?></h3>
                                                            <h6><?php echo utf8_encode($fila['valor_puntos']) ?> Ptos</h6>
                                                            <div class="detalle_prod">
                                                                <small ><?php echo utf8_encode($fila['detalle']) ?> </small><br><br>
                                                            </div>
                                                        </div>
                                                        <div class="card-footer text-center">
                                                            <button class="btn waves-effect redimir_puntos_sin_sesion" style="background:<?php echo $color_primario; ?> !important" type="submit" <?php echo $estatus; ?> value='<?php echo $fila['id_cat_premios_productos_servicios']; ?>'><i class="fas fa-gift" ></i> Obtener</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                   <?php   } */?>
                                    </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
   
</body>
</html>
 
 
 <?php
 }
