
<?php 
/*************************************************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#- Vista de Redencion -#
##################################################################################### 
include_once '../../inc/cont_fijos.php';
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
include_once '../../controller/redencion/c_redencion.php';
$ins_control_redencion=new C_redencion($ser,$usu,$pas,$bd);
$ins_cont_fijos=new Contenidos_Fijos($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
session_start();
if(isset($_SESSION["usuario"])){ 
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?php  
 $ins_cont_fijos->head();
 //TRAEMOS CONFIGURACION DE ESTILOS
 $res_con=$ins_funciones->consulta_generica('tbl_estilo', ' ');
 $registro= mysqli_fetch_assoc($res_con);
 //VALIDAMOS LOGO
if($registro['logo_cte']==''){
    $img_logo='imagenes/logo.png';
}else{
    $img_logo='imagenes/img_configuracion/'.$registro['logo_cte'];
}
 //VALIDAMOS IMAGEN FONDO
if($registro['fondo_cte']==''){
     $img_background='imagenes/background_default.jpg';
}else{
    $img_background='imagenes/img_configuracion/'.$registro['fondo_cte'];
}
 //VALIDAMOS COLOR PRIMARIO
if($registro['color_primario']==''){
     $color_primario='#006AA9';
}else{
    $color_primario=$registro['color_primario'];
}
 //VALIDAMOS COLOR SECUNDARIO
if($registro['color_secundario']==''){
     $color_secundario='#006AA9';
}else{
    $color_secundario=$registro['color_secundario'];
}
  ?>
      <script type="text/javascript" src='../../inc/js/redencion.js'></script>
      
      
</head>
<?php
//LLAMAMOS ESTILOS DE CONFIGURACION
$ins_cont_fijos->estilos($img_logo,$img_background,$color_primario,$color_secundario);
?>

<body class="theme-red">
    <!-- Pag. Loading-->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Espera un poco...</p>
        </div>
    </div>
    <!-- Barra Ariba -->
    <nav class="navbar" style='height: 70px;'> 
        <div class="container-fluid">
            <div class="navbar-header">
                 <a href="javascript:void(0);" class="bars"></a>
                <span class="navbar-brand" >Plataforma Lealtad </span>
            </div>
        </div>
    </nav>
    <section>
        <!-- MeNU IZQUIERDO -->
        <aside id="leftsidebar" class="sidebar">
            <!-- Usuario-->
            <div class="user-info">
                <div class="image">
                    <img src="../../inc/imagenes/user.png" width="48" height="48" alt="User" />
                    <img src="../../inc/<?php echo $img_logo; ?>" style='    margin-left: 130px;' width="60" height="60" alt="Logo" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION["usuario"];?></div>
                    <!--<div class="email">john.doe@example.com</div>-->
                </div>
            </div>
            <!-- MENU -->
            <div class="menu">
                <ul class="list">
                    <?php echo $ins_cont_fijos->menu($_SESSION["id_usuario"]);?>
                </ul>
            </div>
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                        <?php 
                        $ins_cont_fijos->footer();
                        ?>
                </div>
            </div>
        </aside>
    </section>
<!-- CONTENIDO -->
    <section class="content">
        <div class="container-fluid">
            <div class="block-header">
                <h2><i class="fas fa-gift"></i> REGISTRO DE REDENCION</h2>
            </div>
            <!-- Input -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-sm-12">
                                    <form id='form_redencion' method="POST">
                                         <?php 
                                            $rest=$ins_control_redencion->trae_campos();
                                             while($fila = $rest->fetch_assoc()){ 
                                               //EVALUAMOS SI ES TELEFONO     
                                                 if(utf8_encode($fila['alias'])=='telefono')
                                                {
                                                     $coment_tel=' Lada+Número Telefonico';
                                                     $type='number';
                                                     $campo0=utf8_encode(ucwords($fila['alias'])).' Celular';
                                                }else{
                                                    $coment_tel='';
                                                    $type='email';
                                                    $campo0=utf8_encode(ucwords($fila['alias']));
                                                }  
                                                 ?> 
                                                <div class="row clearfix">
                                                    <div class="col-sm-12">
                                                        Ingresa por lo menos un campo:<br>
                                                        <h2 class="card-inside-title"><?php echo $campo0;?><span style='font-size: 10px; color:<?php  echo $color_primario; ?>'> <?php  echo $coment_tel; ?></span></h2> 
                                                        <div class="form-group">
                                                            <div class="form-line">
                                                                <input type="<?php  echo $type; ?>" class="form-control"  name='<?php echo utf8_encode($fila['alias'])?>' placeholder="<?php  echo $placeholder; ?> ">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php }  ?>
                                             <div id='loading'><img src="../../inc/imagenes/load.gif"></div>
                                            <button id='guarda_redencion' type="button" class="btn btn_color m-t-15 waves-effect " >Mostrar Catalogo para Cliente</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </section>
</body>
</html>
<?php }else{ 
     //MOSTRAMOS FORMULARIO DE REGISTRO SIN SESION INICIADA
     ?>


<?php
    //VISTA PARA UN USUARIO FINAL 
    //MOSTRAMOS FORMULARIO DE REGISTRO SIN SESION INICIADA
    $ins_cont_fijos->head();
    //TRAEMOS CONFIGURACION DE ESTILOS
    $res_con=$ins_funciones->consulta_generica('tbl_estilo', ' ');
    $registro= mysqli_fetch_assoc($res_con);
    //VALIDAMOS LOGO
    if($registro['logo_cte']==''){
        $img_logo='imagenes/logo.png';
    }else{
        $img_logo='imagenes/img_configuracion/'.$registro['logo_cte'];
    }
    //VALIDAMOS IMAGEN FONDO
    if($registro['fondo_cte']==''){
        $img_background='imagenes/background_default.jpg';
    }else{
        $img_background='imagenes/img_configuracion/'.$registro['fondo_cte'];
    }
     //VALIDAMOS COLOR PRIMARIO
    if($registro['color_primario']==''){
         $color_primario='#006AA9';
    }else{
        $color_primario=$registro['color_primario'];
    }
     //VALIDAMOS COLOR SECUNDARIO
    if($registro['color_secundario']==''){
         $color_secundario='#006AA9';
    }else{
        $color_secundario=$registro['color_secundario'];
    }
    //LLAMAMOS ESTILOS DE CONFIGURACION
    $ins_cont_fijos->estilos_front($img_logo,$img_background,$color_primario,$color_secundario);
?>
<!DOCTYPE html>
<html lang="es" class='form_front'>
<head>
    <script type="text/javascript" src='../../inc/js/redencion.js'></script>
    <style>
        section.content {
            margin: 100px 15px 0 5px;
            -moz-transition: 0.5s;
            -o-transition: 0.5s;
            -webkit-transition: 0.5s;
            transition: 0.5s;
            max-width: 500px;
            margin: 7% auto !important;
        }
    </style>
</head>

<body class="theme-red">
    <!-- Pag. Loading-->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Espera un poco...</p>
        </div>
    </div> 
    <!-- CONTENIDO -->
    <div class="block-header" style="padding-top:5px;">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <a href='../registro/registrate.php' type="button" class="btn btn-rounded btn-default btn-circle waves-effect waves-circle waves-float">
                <i class="fa fa-arrow-left"></i>
            </a>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
            <img class="img_form_reg" src="../../inc/<?php echo $img_logo ;?>">
        </div>
    </div>

    <section class="content">
        <div class="container-fluid">
            <div class="block-header text-center">
                <span class="tit_form_reg" >Bienvenido</span>
            </div>
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class=" cabecera">
                        Registro de Redención<br>
                    </div>
                    <div class="card card2" >
                        <div class="body">
                            <form id='form_redencion' method="POST">
                                <div class="row clearfix">
                            <?php 
                                $rest=$ins_control_redencion->trae_campos();
                                while($fila = $rest->fetch_assoc()){ 
                                //EVALUAMOS SI ES TELEFONO     
                                    if(utf8_encode($fila['alias'])=='telefono')
                                    {
                                         $coment_tel=' Lada+Número Telefonico';
                                         $type='number';
                                         $campo0=utf8_encode(ucwords($fila['alias'])).' Celular';
                                    }else{
                                        $coment_tel='';
                                        $type='email';
                                        $campo0=utf8_encode(ucwords($fila['alias']));
                                    }  
                            ?> 

                                        <div class="col-sm-12">
                                            Ingresa por lo menos un campo:<br>
                                            <h2 class="card-inside-title"><?php echo $campo0;?><span style='font-size: 10px; color:<?php  echo $color_primario; ?>'> <?php  echo $coment_tel; ?></span></h2> 
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="<?php  echo $type; ?>" class="form-control"  name='<?php echo utf8_encode($fila['alias'])?>' placeholder="<?php  echo $placeholder; ?> ">
                                                </div>
                                            </div>
                                        </div>
                            <?php 
                                }  
                            ?>
                                    <div id='loading'><img src="../../inc/imagenes/load.gif"></div>
                                    <button id='guarda_redencion' type="button" class="btn btn_color m-t-15 waves-effect " >Mostrar Catalogo para Cliente</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </section>
</body>
</html>
<?php    
}