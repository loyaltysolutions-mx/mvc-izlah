
<?php 

include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
include_once '../../controller/redencion/c_redencion.php';
$ins_control_redencion=new C_redencion($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
$id_usua=$_REQUEST["id_usu"];
 //TRAEMOS CONFIGURACION DEL PROYECTO
 $qr_config_proy=$ins_funciones->consulta_generica_all(' select * from tbl_configuracion_proyecto');
 $reg_config= mysqli_fetch_assoc($qr_config_proy);
 if($reg_config['id_tipo_proyecto']==3){//PROYECTO POR VISITAS
	 //TRAEMOS INFORMACION DE USUARIO 
 $qr_visitas_acumuladas=$ins_funciones->consulta_generica_all(' select * from tbl_puntos_totales where id_usuario='.$id_usua.' and id_tipo_puntos=1');
 $visitas_totales_acumuladas= mysqli_fetch_assoc($qr_visitas_acumuladas);

 $qr_visitas_acumuladasc=$ins_funciones->consulta_generica_all(' select COUNT(*) as n from tbl_puntos_totales where id_usuario='.$id_usua.' and id_tipo_puntos=1 AND visitas_totales IS NOT NULL');
 $visitas_totales_acumuladasc= mysqli_fetch_assoc($qr_visitas_acumuladasc);
 
 $qr_ptos_redimidos=$ins_funciones->consulta_generica_all(' select * from tbl_puntos_totales where id_usuario='.$id_usua.' and id_tipo_puntos=2');
 $ptos_totales_redimidos= mysqli_fetch_assoc($qr_ptos_redimidos);
 $res_con=$ins_funciones->consulta_generica_all('select SUM(R.puntos) as puntos_acumulados,U.usuario,U.id_usuario  from tbl_registros as R
                                                inner join tbl_usuario as U on R.id_usuario=U.id_usuario
                                                 where R.id_tipo_registro=1 and R.id_usuario='.$id_usua);
 $ptos_acumu= mysqli_fetch_assoc($res_con);
 $res_con2=$ins_funciones->consulta_generica_all('select SUM(puntos) as puntos_redimidos from tbl_registros where id_tipo_registro=2 and id_usuario='.$id_usua);
 $ptos_redim= mysqli_fetch_assoc($res_con2);
	 
	 
 }else{//PROYECTO POR PUNTOS
	//TRAEMOS INFORMACION DE USUARIO 
 $qr_ptos_acumulados=$ins_funciones->consulta_generica_all(' select * from tbl_puntos_totales where id_usuario='.$id_usua.' and id_tipo_puntos=1');
 $ptos_totales_acumulado= mysqli_fetch_assoc($qr_ptos_acumulados);
 $qr_ptos_redimidos=$ins_funciones->consulta_generica_all(' select * from tbl_puntos_totales where id_usuario='.$id_usua.' and id_tipo_puntos=2');
 $ptos_totales_redimidos= mysqli_fetch_assoc($qr_ptos_redimidos);
 $res_con=$ins_funciones->consulta_generica_all('select SUM(R.puntos) as puntos_acumulados,U.usuario,U.id_usuario  from tbl_registros as R
                                                inner join tbl_usuario as U on R.id_usuario=U.id_usuario
                                                 where R.id_tipo_registro=1 and R.id_usuario='.$id_usua);
 $ptos_acumu= mysqli_fetch_assoc($res_con);
 $res_con2=$ins_funciones->consulta_generica_all('select SUM(puntos) as puntos_redimidos from tbl_registros where id_tipo_registro=2 and id_usuario='.$id_usua);
 $ptos_redim= mysqli_fetch_assoc($res_con2);

 }
 
session_start();
if(isset($_SESSION["usuario"])){ 
?>

            <!-- Input -->
<script type="text/javascript" src='../../inc/js/productos_redencion2.js'></script>            
<div class="card">
    <div class="body">
        <div class="header">
            <h2><i class="fas fa-gift"></i> CANJE DE PRODUCTO</h2>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12"> 
            <div class="info-box hover-zoom-effect">
                <div class="icon bg-indigo">
                    <i class="material-icons">face</i>
                </div>
                <div class="content">
                    <div class="text">Usuario</div>
                    <div class="text "><b><?php echo utf8_encode(ucwords($ptos_acumu['usuario'])); ?></b>
                    </div>
                </div>
            </div>
        </div>
        <?php 
            if($reg_config['id_tipo_proyecto']==3){//PROYECTO POR VISITAS  
        ?>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box hover-zoom-effect">
                    <div class="icon bg-pink">
                       <i class="fas fa-donate"></i>
                    </div>
                    <div class="content">
                        <div class="text">Visitas Acumuladas</div>
                        <?php
                            if($visitas_totales_acumuladas['visitas_totales']==''){ 
                                $vta = '0'; }
                            else{ 
                                $vta = $visitas_totales_acumuladas['visitas_totales']; 
                            }
                        ?>
                        <div class="number count-to" data-from="0" data-to=" <?php echo $vta; ?>" data-speed="1000" data-fresh-interval="20"><?php echo $vta; ?></div>
                    </div>
                </div>
            </div>

         
        <?php 
            }else{//PROYECTO POR PUNTOS  ?>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box hover-zoom-effect">
                    <div class="icon bg-pink">
                       <i class="fas fa-donate"></i>
                    </div>
                    <div class="content">
                        <div class="text">Puntos Acumulados </div>
                        <?php 
                            if($ptos_totales_acumulado['puntos_totales']=='')
                                { 
                                    $pta = '0';
                                }else{ 
                                    $pta = $ptos_totales_acumulado['puntos_totales']; 
                                }
                        ?>
                        <div class="number count-to" data-from="0" data-to="<?php echo $pta; ?>" data-speed="1000" data-fresh-interval="20"><?php echo $pta; ?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <div class="info-box hover-zoom-effect">
                    <div class="icon bg-blue">
                        <i class="fas fa-gift"></i>
                    </div> 
                    <div class="content">
                        <div class="text">Puntos Redimidos</div>
						<?php 
							if($ptos_totales_redimidos['puntos_totales']==''){
								$pr='0'; 
								}else{ 
									$pr=$ptos_totales_redimidos['puntos_totales']; 
									}
						?>
                        <div class="number count-to" data-from="0"  data-to="<?php  echo $pr; ?>" data-fresh-interval="20"><?php  echo $pr; ?> </div>
                    </div>
                </div>
            </div>
             
             
        <?php 
            } 
        ?>
        <div class="header col-xs-12 col-md-12 col-lg-12">
            <h2>
                Catalogo de Productos para Redimir
            </h2>
        </div>
        <br>
        <div class="row pd-20">
        <?php 
			//EVALUAMOS TIPO DE PROYECTO 
			if($reg_config['id_tipo_proyecto']==3){
            //PROYETO POR VISITAS
				$rest=$ins_control_redencion->trae_productos_visitas();
            ?>
                <input type="hidden" id="id_cte" value='<?php echo $ptos_acumu['id_usuario']; ?>'>
                <input type="hidden" id="id_usu" value='<?php echo $_SESSION["id_usuario"] ?>'>
            <?php 
                while($fila = $rest->fetch_assoc()){
                    $id = $fila['id_cat_premios_productos_servicios'];  
                    if($visitas_totales_acumuladasc['n']>=$fila['valor_visitas']){
                        $estatus='';
                    }else{
                        $estatus='disabled';
                    }
                    
                    ?>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail shadow " style='min-height: 350px; border:2px dashed orange'>
                            <img src="../../inc/imagenes/img_catalogo/<?php echo utf8_encode($fila['imagen']); ?>" style='max-height: 150px; min-height: 150px;  min-height: 140px!important;'>
                            <div class="caption text-center">
                                <h3><?php echo utf8_encode(ucwords($fila['nombre'])) ?></h3>
                                <h6><?php echo utf8_encode($fila['valor_visitas']) ?> Visitas</h6>
                                <div class="detalle_prod">
                                    <p><?php echo utf8_encode($fila['detalle']) ?> </p>
                                </div>
                            </div>
                            <div class="card-footer text-center">
                                <button id="<?php echo $id; ?>" onclick="buyit(this.id)" class="btn btn-primary waves-effect  redimir_puntos" type="submit" <?php echo $estatus; ?> value='<?php echo $fila['id_cat_premios_productos_servicios']; ?>'><i class="fas fa-gift" ></i> Obtener</button>
                            </div>
                        </div>
                    </div>
            <?php   
                }//end while 
				
			}else{//PROYECTO POR PUNTO  
			    $rest=$ins_control_redencion->trae_productos();?>
                <input type="hidden" id="id_cte" value='<?php echo $ptos_acumu['id_usuario']; ?>'>
                <input type="hidden" id="id_usu" value='<?php echo $_SESSION["id_usuario"] ?>'>
                <?php 
                while($fila = $rest->fetch_assoc()){ 
                    $id = $fila['id_cat_premios_productos_servicios']; 
                    if($ptos_totales_acumulado['puntos_totales']>=$fila['valor_puntos']){
                        $estatus='';
                    }else{
                        $estatus='disabled';
                    }
                    
                    ?>
                    <div class="col-sm-6 col-md-3">
                        <div class="thumbnail shadow " style='min-height: 350px; border:2px dashed orange'>
                            <img src="../../inc/imagenes/img_catalogo/<?php echo utf8_encode($fila['imagen']); ?>" style='  max-height: 150px; min-height: 150px;  min-height: 140px!important;'>
                            <div class="caption text-center">
                                <h3><?php echo utf8_encode(ucwords($fila['nombre'])) ?></h3>
                                <h6><?php echo utf8_encode($fila['valor_puntos']) ?> Ptos</h6>
                                <div class="detalle_prod">
                                    <p><?php echo utf8_encode($fila['detalle']) ?> </p>
                                </div>
                            </div>
                            <div class="card-footer text-center">
                                <button id="<?php echo $id; ?>" onclick="buyit(this.id)" class="btn btn-primary waves-effect  redimir_puntos" type="submit" <?php echo $estatus; ?> value='<?php echo $fila['id_cat_premios_productos_servicios']; ?>'><i class="fas fa-gift" ></i> Obtener</button>
                            </div>
                        </div>
                    </div>
                    <?php   
                }//end while 	
            }//end else
            ?>
        </div>
    </div>
</div>    
                        
 
 
 <?php
 }
