
<?php 
//By Allan
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
include_once '../../controller/redencion/c_redencion.php';
$ins_control_redencion=new C_redencion($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
session_start();
if(isset($_SESSION["usuario"])){ 
?>
<script type="text/javascript" src='../../inc/js/redencion2.js'></script>

<div class="card">
    <div id="here" class="body">
        <form id="form_redencion" onsubmit="redencion(); return false;">
            <h3>Registro de Redención</h3>
            <hr/>
        <?php 
            $rest=$ins_control_redencion->trae_campos();
             while($fila = $rest->fetch_assoc()){ 
               //EVALUAMOS SI ES TELEFONO     
                 if(utf8_encode($fila['alias'])=='telefono')
                {
                     $coment_tel=' Lada+Número Telefonico';
                     $type='text';
                     $campo0=utf8_encode(ucwords($fila['alias'])).' Celular';
                }else{
                    $coment_tel='';
                    $type='text';
                    $campo0=utf8_encode(ucwords($fila['alias']));
                    if($campo0=='Mail'){
                        $type='email';
                    }
                }  
        ?> 
                <div class="input-group">
                    <div class="form-group">
                        <div class="form-line">
                            <input type="<?php  echo $type; ?>" class="form-control"  name='<?php echo utf8_encode($fila['alias'])?>' placeholder="<?php echo $campo0;?> <?php  echo $coment_tel; ?> ">
                        </div>
                    </div>
                </div>    
        <?php 
                }  
        ?>
        <div id='loading'></div>
        <button class="btn btn-block btn-lg btn_color waves-effect" onclick="sendform();" name="submit-add">Mostrar Catalogo para Cliente</button>

        </form>
    </div>
</div>
<script>
    function sendform(){
        var form = $('#form_redencion');
        $('#form_redencion').find('[type="submit"]').trigger('click');
    }
</script>

<?php
}
?>
