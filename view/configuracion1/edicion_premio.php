<?php
include_once '../../inc/parametros.php';
include_once '../../inc/cont_fijos.php';
$ins_cont_fijos=new Contenidos_Fijos($ser,$usu,$pas,$bd);
include_once '../../controller/configuracion/c_configuracion.php';
$grid = new C_configuracion($ser,$usu,$pas,$bd);
?>
<!DOCTYPE html>
<html lang="es">
  <head>
    <?php  $ins_cont_fijos->headx(); ?>
    <!-- JS  DE CONFIGURACION-->
    <script src="../../inc/js/configuracion.js"></script>
    <script src="../../inc/js/custom.js"></script>   
    <script src="../../inc/plugins/bootstrap/js/bootstrap.min.js"></script>
  </head>
<body id="config" class="">
    <?php
        $ins_cont_fijos->header_config();
      ?>
    <div class="container-fluid">  
        <div class="card" style="padding: 20px;">
        <!-- rewards table -->
            <div class="table-responsive">
                <table id="mainTable" class="table table-bordered table-striped table-hover dataTable js-exportable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Puntos</th>
                            <th>Visitas</th>
                            <th>Imagen</th>
                            <th>Detalle</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            echo $grid->gridedit('tr');
                        ?>
                    </tbody>
                </table>
            </div>  
        </div>
    </div>

    <!-- #END# Exportable Table -->
<!-- #Modal -->
<div class="modal fade" id="editimg" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="defaultModalLabel">Elija a quien se Asignara</h4>
            </div>
            <div class="modal-body">
                <form action="/" id="frmFileUpload" class="dropzone needsclick dz-clickable" method="post" enctype="multipart/form-data">
                    <div class="dz-message needsclick">
                        <div class="drag-icon-cph">
                            <i class="material-icons">touch_app</i>
                        </div>
                        <div>
                            Arrastra tu imagen o haz click para agregarla
                        </div>
                    </div>
                    <div class="fallback">
                        <input id="files" name="files" type="file" />
                    </div>
                </form>
            <input id="filename" type="hidden" name="filename">  
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CERRAR</button>
            </div>
        </div>
    </div>
</div>
<!-- #End Modal -->
<?php
 require_once('../../inc/datatables.php');
?>
<script>
    //Dropzone config by Allan M.
    Dropzone.options.frmFileUpload = {
        maxFilesize: 2,
        acceptedFiles: "image/jpeg,image/png",

        removedfile: function(file) {
            this.on("dragstart", function(file) { 
               this.removeAllFiles(true); 
            });
        },

        accept: function(file, done) {
            
            console.log("uploaded");
            done();
            var sendid = $('#filename').attr('my');
            setfile(sendid,file.name);
            var filename = file.name;
            $('#filename').val(filename);
            
        },
        init: function() {
            this.on("addedfile", function() {
              if (this.files[1]!=null){
                this.removeFile(this.files[0]);
              }
            });
        }
    };
    $("form#frmFileUpload").dropzone({ 
        url: "upload.php"
     });
</script>
</body>                
   