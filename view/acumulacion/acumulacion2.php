
<?php 

include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
include_once '../../controller/acumulacion/c_acumulacion.php';
$ins_control_acumulacion=new C_acumulacion($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
session_start();
if(isset($_SESSION["usuario"])){ 
?>
<script type="text/javascript" src='../../inc/js/acumulacion2.js'></script>
<!-- CONTENIDO -->
    <div class="card">
        <div class="body">
            <form id="form_valida_usu" onsubmit="addacumula(); return false;">
            <h3>Registro de Acumulación</h3>
            <hr/>
            <?php 
                $res=$ins_control_acumulacion->trae_campos_acumulacion();
                $clave='';
                while($fila = $res->fetch_assoc()){ 
            ?> 
 
                            
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control"  name='<?php echo utf8_encode($fila['alias'])?>' placeholder="<?php  echo $caracter; ?> <?php echo utf8_encode(ucwords($fila['nombre'])) ?>">
                    </div>
                </div>
                        
            <?php
                }  
            ?>
                <br>
                <h5> <i class="fas fa-money-check-alt"></i> Datos de consumo</h5><br>
 
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" class="form-control" name='num_ticket' placeholder="Número de Ticket de Venta">
                    </div>
                </div>
           
                <div class="input-group">
                    <div class="form-line">
                        <input type="text" id='monto_ticket' class="form-control"  name='monto_ticket' placeholder="Monto de Venta">
                    </div>
                </div>
                           
                <input type='hidden' id='id_usuario' value='<?php  echo $_SESSION["id_usuario"];?>'>
                <div id='loading'></div>
                <button class="btn btn-block btn-lg btn_color waves-effect" onclick="sendform();" name="submit-add">Acumular</button>
            </form>
        </div>    
    </div>
    <script>
        function sendform(){
            var form = $('#form_valida_usu');
            $('#form_valida_usu').find('[type="submit"]').trigger('click');
        }
    </script>                    

<?php    
}