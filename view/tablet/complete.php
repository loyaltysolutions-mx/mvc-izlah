<?php 
	
	
	require_once '../../inc/funciones.php';
	require_once '../../inc/parametros.php';
	//$inst_control=new C_home($ser,$usu,$pas,$bd);	
	$ins_funciones=new Funciones_Basicas();
	/*session_start();
	if(isset($_SESSION["usuario"])){ 
	    $catalogue = 1;
	    //echo "<script>console.log('".$catalogue." estas logueado');</script>";
	}else{*/
    $catalogue = 0;

      //TRAEMOS CONFIGURACION DE ESTILOS
	$res_con1=$ins_funciones->consulta_generica('tbl_estilo', ' ');
	$registro1= mysqli_fetch_assoc($res_con1);

	$res_con2=$ins_funciones->consulta_generica('tbl_configuracion_proyecto', ' ');
	$config = mysqli_fetch_assoc($res_con2);

	//VALIDAMOS LOGO
	if($registro1['logo_cte']==''){
	  $img_logo1='imagenes/logo.png';
	}else{
	  $img_logo1='imagenes/img_configuracion/'.$registro1['logo_cte'];
	}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name='viewport' content='width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1'/>
	<meta content="yes" name="mobile-web-app-capable">
	<link rel="icon" sizes="192x192" href="../../inc/<?php echo $img_logo1 ?>">
	<title>:: <?php echo $config['titulo_home'] ?> :: <?php echo $config['detalle_home'] ?> </title>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">	
	
	<!-- Bootstrap Select Css -->
    <link href="../../inc/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <link href="../../inc/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />	
	<link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/4742d4c7f3.js"></script>	
	<style type="text/css">
		body{
			font-family: 'Oswald', sans-serif;
			color: white;
		}

		
		body{
			background:url('../../inc/imagenes/tablet/complete/cat-chocolateas-hover.jpg') no-repeat;
			background-size: cover;
			background-position: top center;
			/*background-attachment: fixed;*/
		}

		.btn-primary{
		    background-color: #EF2967;
		    /* -webkit-box-shadow: 6px 11px 5px rgba(0,0,0,0.7); */
		    /* box-shadow: 6px 11px 5px rgba(0,0,0,0.7); */
		    -o-box-shadow: 6px 11px 5px rgba(0,0,0,0.7);
		    -moz-box-shadow: 6px 11px 5px rgba(0,0,0,0.7);
		    border-color: #EF2967 !important;
		    padding: 15px;
		    border-radius: 8px;
		    font-size: 16pt;
		    /* font-weight: bold; */
		}

		.btn:hover{
			color: #EF2967;			
			background-color: white;

		}

		.btn:active{
			color: #EF2967;
			background-color: white !important;
			border-color: #EF2967;
		}

		.title{
			font-size: 35pt;
			color: white;
		}

		.textBienvenida{
			font-size: 18pt;
			color: white;
		}

		.labelTag{
			font-size: 12pt;
		}

		.labelText{
			font-size: 14pt;
			font-family: "Open-sans";
			margin: 5px 0;
		}

		input{
			width: 100% !important;
			border-radius: 5px;
			-webkit-border-radius: 5px;
			-o-border-radius: 5px;
			-moz-border-radius: 5px;
			border: 1px solid gray;
			padding: 8px;			
	    	font-family: Helvetica;
	    	font-size: 21pt;
	    	height: 35pt !important;
		}

		select{
	    	width: 100% !important;
	    	font-family: Helvetica;	    	
	    	height: 35pt !important;
	    	color: black !important;
	    	border-radius: 5px;
	    }

	    .lds-dual-ring {
	        display: inline-block;
	        width: 64px;
	        height: 64px;
	    }
	    .lds-dual-ring:after {
	        content: " ";
	        display: block;
	        width: 46px;
	        height: 46px;
	        margin: 1px;
	        border-radius: 50%;
	        border: 5px solid #fff;
	        border-color: #EF2967 transparent #EF2967 transparent;
	        animation: lds-dual-ring 1.2s linear infinite;
	    }
	    @keyframes lds-dual-ring {
	        0% {
	            transform: rotate(0deg);
	        }
	        100% {
	            transform: rotate(360deg);
	        }
	    }
	</style>	
</head>
<body>
<?php
	
	$token=$_GET['token'];	

	if(isset($token) && $token != null ){
		$token = trim(addslashes(htmlspecialchars($token)));
		//die($token);
		$res_token=$ins_funciones->consulta_generica("tbl_usuario", " where token='$token'");
		
		$registro_token = mysqli_fetch_assoc($res_token);
		if(count($registro_token)>0){

			function mask ( $str, $start = 0, $length = null ) {
			    $mask = preg_replace ( "/\S/", "*", $str );
			    if( is_null ( $length )) {
			        $mask = substr ( $mask, $start );
			        $str = substr_replace ( $str, $mask, $start );
			    }else{
			        $mask = substr ( $mask, $start, $length );
			        $str = substr_replace ( $str, $mask, $start, $length );
			    }
			    return $str;
			}
			 
			// Usage:
			$string = $registro_token['telefono'];
			$telefono = mask($string,null,strlen($string)-4);
?>
	<div class="modal fade" id="modalResult" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header" style="background-color: rgba(255, 255, 255, 0.5)">
					<h5 class="modal-title text-center" id="exampleModalLongTitle" style="color:black"> Izlah dice: </h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  		<span aria-hidden="true">&times;</span>
					</button>
				</div>
			  	<div class="modal-body" style="padding:20px">
			    	<p id="textResult" style="color:black"></p>
			  	</div>
			  	<div class="modal-footer">			    	
			    	<button id="btnOkModal" type="button" class="btn btn-primary" data-dismiss="modal" style="padding:10px 30px">Aceptar</button>
			  	</div>
			</div>
		</div>
	</div>
	<div id="mainContainer" class="container-fluid">
		<div class="row fixed-top">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="background-color: black; padding: 35px 0">
				<img class="imgFix" src="../../inc/imagenes/tablet/logo-izlah.png"/>
			</div>
		</div>
		<div class="row justify-content-center" style="margin-top:95px">			
			<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9" style="background-color: rgba(0,0,0,0.5)">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="margin:25px 0">
						<span class="title">Hola de nuevo</span>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: black">
						<div class="row justify-content-center">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 text-justify">
								<p class="textBienvenida">Bienvenido, gracias por ser parte de nuestra familia, por favor completa tu registro con los siguientes datos.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="row justify-content-center">
							<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9" style="margin: 15px 0">
								<form id="formConfirmRegistro">
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
											<label class="labelTag">Teléfono: </label><br>
											<label class="labelText"><?php echo $telefono ?> </label>
											<input  type='hidden' id='telefono' name="telefono" value="<?php  echo $registro_token['telefono']; ?>">
											<input  type='hidden' id='usuario' name="usuario" value="<?php  echo $registro_token['telefono']; ?>">
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
											<label class="labelTag">Email: </label><br>
											<label class="labelText"><?php echo $registro_token['email'] ?> </label>
											<input  type='hidden' id='email' name="email" value="<?php  echo $registro_token['email']; ?>">
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
											<label class="labelTag">Nombre Completo: </label><br>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
											<div class="input-group">
												<input type="text" class="form-control" aria-describedby="basic-addon1" name="nombre" placeholder="Paterno, Materno, Nombre(s)" required pattern="^([A-Za-zñáéíóúÑÁÉÍÓÚ]{1,24}[\s]?)+" />
												<input  type='hidden' id='id_usu' name="id_usu" value="<?php  echo $registro_token['id_usuario']; ?>">
											</div>											
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">											
											<select name="genero" required>
												<option value=""> Género</option>
												<option value="1"> Hombre </option>
												<option value="2"> Mujer </option>
											</select>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
											<div class="input-group">												
												<label class="labelTag">Fecha de Nacimiento:</label>												
												<input id="fechaNacimiento" type="text" class="form-control" aria-describedby="basic-addon1" name="fecha_nacimiento" required autocomplete="off" placeholder="Elige una fecha">
											</div>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 text-left">
											<label class="labelTag">Asignar Contraseña (Mínimo 8 caracteres):</label>
											<div class="input-group-append">
												<input id="password" type="password" class="form-control" aria-describedby="basic-addon1" name="password" required autocomplete="off" placeholder="Sólo números y letras" pattern="[A-Za-z0-9ñáéíóúÑÁÉÍÓÚ]{8,}">
												<i class="btnShow input-group-text fa fa-eye" style="padding:14px"></i>
											</div>
										</div>
										<div class="col-lg-6 col-md-6 col-sm-12 col-xs-6 text-left">
											<label class="labelTag">Confirmar Contraseña:</label>
											<div class="input-group-append">												
												<input id="repassword" type="password" class="form-control" aria-describedby="basic-addon1" required autocomplete="off" placeholder="Repite tu contraseña">
												<i class="btnShow input-group-text fa fa-eye" style="padding:14px"></i>
											</div>
										</div>
									</div>
									<br>
									<div class="row">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
											<div class="input-group">
												<label class="labelTag">¿Cuál es tu bebida favorita?:</label>												
												<select id="encuesta" name="encuesta" required>
													<option value=""> -- Elige una opción -- </option>
													<option value="Café Matcha"> Café Matcha </option>
												</select>
											</div>
										</div>										
									</div>
									<br>
									<div class="row justify-content-center" style="margin-bottom:135px">
										<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
											<br>
											<button id="btnSubmitConfirm" type="submit" class="btn btn-primary"> Confirmar Registro </button>
											<div id="loadAnimation" style="display:none; width:100%; text-align:center"><div class="lds-dual-ring"></div></div>
										</div>										
									</div>									
								</form>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row fixed-bottom">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="background-color: #333333; padding: 15px 0">				
				<div class="row justify-content-center">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
						<br>
						<img src="../../inc/imagenes/tablet/mailing/Grupo_16_A18_Group_8_pattern.png">
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">					
						<label>Consulta nuestro aviso de privacidad <u><a href="https://desclub.com.mx/izlah">aquí</a></u>.</label>
					</div>
				</div>
			</div>
		</div>		
	</div>
	<?php 
		}else{ 
		
	?>
	
			<div id="mainContainer" class="container-fluid">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="background-color: black; padding: 25px 0">
						<img class="imgFix" src="../../inc/imagenes/tablet/logo-izlah.png"/>
					</div>
				</div>
				<div class="row justify-content-center">			
					<div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 align-self-center" style="background-color: rgba(0,0,0,0.5);">
						<p style="text-align: center; font-size: 2.1em"> El link ingresado no existe o ha caducado. Si ya completaste tu registro te invitamos a iniciar sesión <a href="../../index.php" style="text-decoration: underline; color: white; font-size: 1em; cursor: pointer; ">aquí</a></p>
					</div>
				</div>
			</div>
	<?php 		
		}
	}else{ 
		//die("Else");
		header("location:../../");
	}
	?>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<!-- Moment Plugin Js -->
<script src="../../inc/plugins/momentjs/moment.js"></script>
<script src="../../inc/plugins/momentjs/moment-with-locale.js"></script>
<!-- DateTimePicker  -->
<script src="../../inc/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
<script src="../../inc/js/jquery_redirect.js"></script>
<script type="text/javascript">    
    $('#fechaNacimiento').bootstrapMaterialDatePicker({ 
		format : 'YYYY-MM-DD',
		time : false,         
		lang : 'es', 
		weekStart : 1,
		okText : 'Aceptar',
		maxDate : '<?php echo date("Y-m-d") ?>',
		cancelText : 'Cancelar'
	});
    function initScreen(){				
    	if($("#mainContainer").height() < $(window).height() )
			$("#mainContainer").height($(window).height());
		else
			$("#mainContainer").height("auto");
	}

	$(window).on("resize", function(){		
		initScreen();
	});

	initScreen();

	$(document).ready(function(){
		//GUARDA DATOS DE REGISTRO   
		

		$('#formConfirmRegistro').on("submit", function(e){    
			e.preventDefault();
			if($("#password").val() == $("#repassword").val()){
				$("#btnSubmitConfirm").fadeOut(300, function(){
					$.ajax({
						url: '../../controller/valida/c_llamadas_ajax.php',
						type: "POST",
						data: $("#formConfirmRegistro").serialize()+"&op=2",
						dataType: "json",
						beforeSend: function(){
							$("#loadAnimation").fadeIn();
						},
						success: function(resp){  
							$('#loading').hide();
							//console.log(resp);
							if(resp == 1){
							    $("#textResult").text("Felicidades, tu registro se ha completado, ahora ya podrás ver todas las promociones que tenemos para ti");
							    $("#modalResult").on("hidden.bs.modal", function () {
								    $.redirect("../../controller/login/c_login.php?op=1", {usuario: $("#usuario").val(), password: $("#password").val()}, "POST"); 
								});
							    	
							}else{
								$("#textResult").text("Hubo un error al finalizar tu registro, intenta nuevamente. Si el problema persiste contacta al administrador.");
								$("#loadAnimation").fadeOut(300, function(){
									$("#btnSubmitConfirm").fadeIn();
								});	
							}

							$("#modalResult").modal({
								backdrop: 'static',
								keyboard: false
							});

						}
					}).fail(function(data){
					  console.log(data);
					});
				});				
			}else{
				$("#textResult").text("Las contraseñas no coinciden, intenta nuevamente.");
				$("#modalResult").modal("show");
			}
			
		});

		$(".btnShow").click(function() {
			var input = $(this).siblings("input");
			if (input.attr("type") === "password") {
				
				input.attr("type", "text");
				$(this).removeClass("fa fa-eye");
				$(this).addClass("fa fa-eye-slash");
			} else {
				
				input.attr("type", "password");
				$(this).removeClass("fa fa-eye-slash");
				$(this).addClass("fa fa-eye");
			}
		});
	});
    
</script>
</body>
</html>