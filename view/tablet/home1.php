<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta content="yes" name="mobile-web-app-capable">
<title>Home</title>
<style id="applicationStylesheet" type="text/css">
	body {
		margin: 0;
		padding: 0;

	}
	:root {
		--web-view-ids: Panel_Transaccional;
	}
	#Panel_Transaccional * {
		margin: 0;
		padding: 0;
	}
	.Panel_Transaccional_Class {
		position: absolute;
		box-sizing: border-box;
		background: #E5E5E5;
		width: 1024px;
		height: 768px;
		background-color: rgba(255,255,255,1);
		overflow: hidden;
		margin: 0;
		padding: 0;
		--web-view-name: Panel Transaccional;
		--web-view-id: Panel_Transaccional;
		--web-enable-deep-linking: true;
	}
	.cat_teasorientexpresscollectio_Class {
		fill: url(#cat_teasorientexpresscollectio_A0_Rectangle_2_pattern);
	}
	.cat_teasorientexpresscollectio {
		position: absolute;
		overflow: visible;
		width: 1024.5px;
		height: 1024px;
		left: 0px;
		top: -256px;
	}
	.Rect_ngulo_253_Class {
		fill: url(#LinearGradientFill1);
	}
	.Rect_ngulo_253 {
		position: absolute;
		overflow: visible;
		width: 1025px;
		height: 768px;
		left: 0px;
		top: 0px;
	}
	.Rect_ngulo_260_Class {
		filter: drop-shadow(1px 0px 30px rgba(0, 0, 0, 1));
		fill: rgba(0,0,0,0.502);
	}
	.Rect_ngulo_260 {
		position: absolute;
		overflow: visible;
		width: 459px;
		height: 858px;
		left: 0px;
		top: 0px;
	}
	.Grupo_20_Class {
		position: absolute;
		width: 125.421px;
		height: 19.012px;
		left: 28.99px;
		top: 729.988px;
		overflow: visible;
	}
	.Grupo_17_Class {
		position: absolute;
		width: 125.421px;
		height: 19.012px;
		left: 0px;
		top: 0px;
		overflow: visible;
	}
	.Grupo_16_Class {
		position: absolute;
		width: 125.421px;
		height: 19.012px;
		left: 0px;
		top: 0px;
		overflow: visible;
	}
	.Rect_ngulo_103_Class {
		filter: drop-shadow(6px 11px 8px rgba(0, 0, 0, 0.329));
		fill: rgba(239,41,103,1);
	}
	.Rect_ngulo_103 {
		position: absolute;
		overflow: visible;
		width: 181px;
		height: 64px;
		left: 109px;
		top: 540px;
	}
	.Ganar_puntos_Class {
		position: absolute;
		left: 129px;
		top: 549px;
		overflow: visible;
		width: 117px;
		white-space: nowrap;
		line-height: 30px;
		margin-top: -5.5px;
		text-align: center;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 19px;
		color: rgba(255,255,255,1);
	}
	._Gana_con_tu_compra__Class {
		position: absolute;
		left: 28.135px;
		top: 387.688px;
		overflow: visible;
		width: 313px;
		white-space: nowrap;
		text-align: center;
		font-family: Oswald;
		font-style: normal;
		font-weight: bold;
		font-size: 36px;
		color: rgba(239,41,103,1);
	}
	.Tus_promociones_Class {
		position: absolute;
		left: 401px;
		top: 445px;
		overflow: visible;
		width: 241px;
		white-space: nowrap;
		text-align: left;
		font-family: Oswald;
		font-style: normal;
		font-weight: bold;
		font-size: 35px;
		color: rgba(255,255,255,1);
	}
	.Redime_tus_puntos_Class {
		position: absolute;
		left: 399px;
		top: 32px;
		overflow: visible;
		width: 269px;
		white-space: nowrap;
		text-align: center;
		font-family: Oswald;
		font-style: normal;
		font-weight: bold;
		font-size: 35px;
		color: rgba(255,255,255,1);
	}
	.Frapp_Teas_Class {
		position: absolute;
		left: 396px;
		top: 88px;
		overflow: visible;
		width: 63px;
		white-space: nowrap;
		text-align: center;
		font-family: Oswald;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(255,255,255,1);
	}
	.Hola_Jairo__tienes_2300pts_Class {
		position: absolute;
		left: 81px;
		top: 346px;
		overflow: visible;
		width: 199px;
		white-space: nowrap;
		text-align: center;
		font-family: Oswald;
		font-style: normal;
		font-weight: bold;
		font-size: 19px;
		color: rgba(255,255,255,1);
	}
	.ID1800pts_para_el_sig__nivel_Class {
		position: absolute;
		left: 100px;
		top: 254px;
		overflow: visible;
		width: 161px;
		white-space: nowrap;
		text-align: center;
		font-family: Oswald;
		font-style: normal;
		font-weight: bold;
		font-size: 17px;
		color: rgba(255,255,255,1);
	}
	.Grupo_30_Class {
		position: absolute;
		width: 189.429px;
		height: 4.371px;
		left: 85px;
		top: 245.857px;
		overflow: visible;
	}
	.Rect_ngulo_256_Class {
		opacity: 0.4;
		fill: rgba(255,255,255,1);
	}
	.Rect_ngulo_256 {
		position: absolute;
		overflow: visible;
		width: 189.428px;
		height: 4.371px;
		left: 0px;
		top: 0px;
	}
	.Rect_ngulo_257_Class {
		fill: rgba(239,41,103,1);
	}
	.Rect_ngulo_257 {
		position: absolute;
		overflow: visible;
		width: 103.181px;
		height: 4.371px;
		left: 0px;
		top: 0px;
	}
	.Elipse_9_Class {
		opacity: 0.57;
		fill: transparent;
		stroke: rgb(255, 255, 255);
		stroke-width: 10px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Elipse_9 {
		position: absolute;
		overflow: visible;
		width: 118.285px;
		height: 118.285px;
		left: 120.786px;
		top: 65px;
	}
	.Silver_Class {
		position: absolute;
		left: 152px;
		top: 206px;
		overflow: visible;
		width: 57px;
		white-space: nowrap;
		text-align: center;
		font-family: Oswald;
		font-style: normal;
		font-weight: bold;
		font-size: 24px;
		color: rgba(239,41,103,1);
	}
	.L_nea_4_Class {
		opacity: 0.8;
		fill: transparent;
		stroke: rgb(239, 41, 103);
		stroke-width: 1px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.L_nea_4 {
		overflow: visible;
		position: absolute;
		top: 452.5px;
		left: 25.5px;
		width: 327.746px;
		height: 1px;
	}
	.L_nea_5_Class {
		opacity: 0.8;
		fill: transparent;
		stroke: rgb(239, 41, 103);
		stroke-width: 1px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.L_nea_5 {
		overflow: visible;
		position: absolute;
		top: 384px;
		left: 25.5px;
		width: 327.746px;
		height: 1px;
	}
	.logo_izlah_Class {
		fill: url(#logo_izlah_A0_Rectangle_18_pattern);
	}
	.logo_izlah {
		position: absolute;
		overflow: visible;
		width: 102px;
		height: 16px;
		left: 29px;
		top: 29px;
	}
	.Sustracci_n_3_Class {
		fill: transparent;
		stroke: rgb(239, 41, 103);
		stroke-width: 7px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Sustracci_n_3 {
		transform: matrix(1,0,0,1,0,0);
		overflow: visible;
		position: absolute;
		top: 67.388px;
		left: 179.724px;
		width: 63.96px;
		height: 120.51px;
	}
	.Enmascarar_grupo_1_Class {
		position: absolute;
		width: 113.509px;
		height: 113.509px;
		left: 124.174px;
		top: 67.389px;
		overflow: visible;
	}
	.Rect_ngulo_195_Class {
		fill: rgba(255,255,255,1);
		stroke: rgb(221, 221, 221);
		stroke-width: 1px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Rect_ngulo_195 {
		position: absolute;
		overflow: visible;
		width: 303px;
		height: 43.731px;
		left: 36px;
		top: 470.734px;
	}
	.Ingresa_el_monto_de_tu_compra__Class {
		position: absolute;
		left: 45px;
		top: 484px;
		overflow: visible;
		width: 287px;
		white-space: nowrap;
		text-align: center;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 17px;
		color: rgba(129,138,145,1);
	}
	.Grupo_35_Class {
		position: absolute;
		width: 815px;
		height: 359px;
		left: 399px;
		top: 63px;
		overflow: visible;
	}
	.Rect_ngulo_398_Class {
		filter: drop-shadow(8px 8px 16px rgba(0, 0, 0, 0.4));
		fill: rgba(0,0,0,0.702);
	}
	.Rect_ngulo_398 {
		position: absolute;
		overflow: visible;
		width: 303px;
		height: 348px;
		left: 0px;
		top: 59px;
	}
	.Rect_ngulo_407_Class {
		filter: drop-shadow(8px 8px 16px rgba(0, 0, 0, 0.4));
		fill: rgba(0,0,0,0.702);
	}
	.Rect_ngulo_407 {
		position: absolute;
		overflow: visible;
		width: 303px;
		height: 348px;
		left: 280px;
		top: 59px;
	}
	.Rect_ngulo_410_Class {
		filter: drop-shadow(8px 8px 16px rgba(0, 0, 0, 0.4));
		fill: rgba(0,0,0,0.702);
	}
	.Rect_ngulo_410 {
		position: absolute;
		overflow: visible;
		width: 303px;
		height: 348px;
		left: 560px;
		top: 59px;
	}
	.Rect_ngulo_397_Class {
		fill: rgba(239,41,103,1);
	}
	.Rect_ngulo_397 {
		position: absolute;
		overflow: visible;
		width: 140px;
		height: 40px;
		left: 58px;
		top: 301px;
	}
	.Trazado_47_Class {
		fill: rgba(57,57,57,1);
	}
	.Trazado_47 {
		transform: matrix(1,0,0,1,0,0);
		overflow: visible;
		position: absolute;
		top: 301px;
		left: 304px;
		width: 208px;
		height: 40px;
	}
	.Rect_ngulo_409_Class {
		fill: rgba(239,41,103,1);
	}
	.Rect_ngulo_409 {
		position: absolute;
		overflow: visible;
		width: 140px;
		height: 40px;
		left: 618px;
		top: 301px;
	}
	.Lo_quiero_Class {
		position: absolute;
		left: 95px;
		top: 313px;
		overflow: visible;
		width: 67px;
		white-space: nowrap;
		line-height: 24px;
		margin-top: -4px;
		text-align: center;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(255,255,255,1);
	}
	.Acumula_3000_pts__m_s_Class {
		position: absolute;
		left: 324px;
		top: 313px;
		overflow: visible;
		width: 169px;
		white-space: nowrap;
		line-height: 24px;
		margin-top: -4px;
		text-align: center;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(255,255,255,1);
	}
	.ID2_300_pts__Class {
		position: absolute;
		left: 91px;
		top: 273.008px;
		overflow: hidden;
		width: 75px;
		height: 20px;
		line-height: 24px;
		margin-top: -4px;
		text-align: left;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(255,255,255,1);
	}
	.ID5_100pts__Class {
		position: absolute;
		left: 371px;
		top: 273.008px;
		overflow: hidden;
		width: 75px;
		height: 20px;
		line-height: 24px;
		margin-top: -4px;
		text-align: center;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(255,0,0,1);
	}
	.Matcha_Frapptea_Class {
		position: absolute;
		left: 52px;
		top: 243.037px;
		overflow: visible;
		width: 152px;
		white-space: nowrap;
		text-align: left;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 20px;
		color: rgba(255,255,255,1);
	}
	.Chai_Walla_Class {
		position: absolute;
		left: 360px;
		top: 243.037px;
		overflow: visible;
		width: 97px;
		white-space: nowrap;
		text-align: left;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 20px;
		color: rgba(255,255,255,1);
	}
	.Enmascarar_grupo_2_Class {
		position: absolute;
		width: 293px;
		height: 218px;
		left: 0px;
		top: 0px;
		overflow: visible;
	}
	.Enmascarar_grupo_3_Class {
		position: absolute;
		width: 255px;
		height: 204.5px;
		left: 0px;
		top: 59px;
		overflow: visible;
	}
	.matcha_Class {
		fill: url(#matcha_A0_Rectangle_32_pattern);
	}
	.matcha {
		position: absolute;
		overflow: visible;
		width: 255px;
		height: 163px;
		left: 0px;
		top: 59px;
	}
	.cat_pateasserie_hover_Class {
		fill: url(#cat_pateasserie_hover_A0_Rectangle_34_pattern);
	}
	.cat_pateasserie_hover {
		position: absolute;
		overflow: visible;
		width: 255px;
		height: 163px;
		left: 280px;
		top: 59px;
	}
	.cat_beachclubteas_hover_Class {
		fill: url(#cat_beachclubteas_hover_A0_Rectangle_36_pattern);
	}
	.cat_beachclubteas_hover {
		position: absolute;
		overflow: visible;
		width: 255px;
		height: 163px;
		left: 560px;
		top: 59px;
	}
	.Lo_quiero_A0_Text_16_Class {
		position: absolute;
		left: 655px;
		top: 313px;
		overflow: visible;
		width: 67px;
		white-space: nowrap;
		line-height: 24px;
		margin-top: -4px;
		text-align: center;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(255,255,255,1);
	}
	.ID1_200pts_Class {
		position: absolute;
		left: 651px;
		top: 273.008px;
		overflow: hidden;
		width: 75px;
		height: 20px;
		line-height: 24px;
		margin-top: -4px;
		text-align: center;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(255,255,255,1);
	}
	.Club_Tea_Class {
		position: absolute;
		left: 648px;
		top: 243.037px;
		overflow: visible;
		width: 80px;
		white-space: nowrap;
		text-align: left;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 20px;
		color: rgba(255,255,255,1);
	}
	.Grupo_36_Class {
		position: absolute;
		width: 535px;
		height: 284px;
		left: 399px;
		top: 452px;
		overflow: visible;
	}
	.Rect_ngulo_398_A0_Rectangle_37_Class {
		filter: drop-shadow(8px 8px 16px rgba(0, 0, 0, 0.4));
		fill: rgba(0,0,0,0.702);
	}
	.Rect_ngulo_398_A0_Rectangle_37 {
		position: absolute;
		overflow: visible;
		width: 583px;
		height: 273px;
		left: 0px;
		top: 59px;
	}
	.Rect_ngulo_397_A0_Rectangle_38_Class {
		fill: rgba(239,41,103,1);
	}
	.Rect_ngulo_397_A0_Rectangle_38 {
		position: absolute;
		overflow: visible;
		width: 140px;
		height: 40px;
		left: 325px;
		top: 198px;
	}
	.Aplicar_Promo_Class {
		position: absolute;
		left: 345px;
		top: 210px;
		overflow: visible;
		width: 101px;
		white-space: nowrap;
		line-height: 24px;
		margin-top: -4px;
		text-align: center;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(255,255,255,1);
	}
	.ID2x1_en_4Ever_Berrys_Class {
		position: absolute;
		left: 304px;
		top: 136.037px;
		overflow: visible;
		width: 183px;
		white-space: nowrap;
		text-align: center;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 20px;
		color: rgba(255,255,255,1);
	}
	.Enmascarar_grupo_2_A0_Group_18_Class {
		position: absolute;
		width: 293px;
		height: 218px;
		left: 0px;
		top: 0px;
		overflow: visible;
	}
	.Enmascarar_grupo_3_A0_Group_23_Class {
		position: absolute;
		width: 255px;
		height: 204.5px;
		left: 0px;
		top: 59px;
		overflow: visible;
	}
	.cat_teas4everyberry_hover_Class {
		fill: url(#cat_teas4everyberry_hover_A0_Rectangle_44_pattern);
	}
	.cat_teas4everyberry_hover {
		position: absolute;
		overflow: visible;
		width: 255px;
		height: 225px;
		left: 0px;
		top: 59px;
	}
	.V_lido_al_30_de_Agosto_de_2019_Class {
		position: absolute;
		left: 255px;
		top: 166.008px;
		overflow: hidden;
		width: 281px;
		height: 20px;
		line-height: 24px;
		margin-top: -6px;
		text-align: center;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 12px;
		color: rgba(255,255,255,1);
	}
	.Grupo_37_Class {
		position: absolute;
		width: 535px;
		height: 284px;
		left: 960px;
		top: 452px;
		overflow: visible;
	}
	.Rect_ngulo_398_A0_Rectangle_45_Class {
		filter: drop-shadow(8px 8px 16px rgba(0, 0, 0, 0.4));
		fill: rgba(0,0,0,0.702);
	}
	.Rect_ngulo_398_A0_Rectangle_45 {
		position: absolute;
		overflow: visible;
		width: 583px;
		height: 273px;
		left: 0px;
		top: 59px;
	}
	.Rect_ngulo_397_A0_Rectangle_46_Class {
		fill: rgba(239,41,103,1);
	}
	.Rect_ngulo_397_A0_Rectangle_46 {
		position: absolute;
		overflow: visible;
		width: 140px;
		height: 40px;
		left: 325px;
		top: 198px;
	}
	.Aplicar_Promo_A0_Text_22_Class {
		position: absolute;
		left: 345px;
		top: 210px;
		overflow: visible;
		width: 101px;
		white-space: nowrap;
		line-height: 24px;
		margin-top: -4px;
		text-align: center;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(255,255,255,1);
	}
	.ID2x1_en_4Ever_Berrys_A0_Text_23_Class {
		position: absolute;
		left: 304px;
		top: 136.037px;
		overflow: visible;
		width: 183px;
		white-space: nowrap;
		text-align: center;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 20px;
		color: rgba(255,255,255,1);
	}
	.Enmascarar_grupo_2_A0_Group_25_Class {
		position: absolute;
		width: 293px;
		height: 218px;
		left: 0px;
		top: 0px;
		overflow: visible;
	}
	.Enmascarar_grupo_3_A0_Group_30_Class {
		position: absolute;
		width: 255px;
		height: 204.5px;
		left: 0px;
		top: 59px;
		overflow: visible;
	}
	.blossom_Class {
		fill: url(#blossom_A0_Rectangle_52_pattern);
	}
	.blossom {
		position: absolute;
		overflow: visible;
		width: 255px;
		height: 225px;
		left: 0px;
		top: 59px;
	}
	.V_lido_al_30_de_Agosto_de_2019_A0_Text_24_Class {
		position: absolute;
		left: 255px;
		top: 166.008px;
		overflow: hidden;
		width: 281px;
		height: 20px;
		line-height: 24px;
		margin-top: -7px;
		text-align: center;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 10px;
		color: rgba(255,255,255,1);
	}
	.Rect_ngulo_414_Class {
		filter: drop-shadow(1px 0px 30px rgba(0, 0, 0, 1));
		fill: url(#LinearGradientFill2);
	}
	.Rect_ngulo_414 {
		position: absolute;
		overflow: visible;
		width: 450px;
		height: 858px;
		left: 900px;
		top: 0px;
	}
	.Trazado_45_Class {
		fill: rgba(255,255,255,1);
	}
	.Trazado_45 {
		transform: matrix(1,0,0,1,0,0);
		overflow: visible;
		position: absolute;
		top: 275px;
		left: 984px;
		width: 14px;
		height: 20px;
	}
	.Trazado_46_Class {
		fill: rgba(255,255,255,1);
	}
	.Trazado_46 {
		transform: matrix(1,0,0,1,0,0);
		overflow: visible;
		position: absolute;
		top: 613px;
		left: 984px;
		width: 14px;
		height: 20px;
	}*/

	/*body{
		background: url('../../inc/imagenes/tablet/panel/panel_Transaccional.png') no-repeat;
		background-size: cover;
		background-position: top center;
	}
</style>
</head>
<body>
<div id="Panel_Transaccional" class="Panel_Transaccional_Class" style="max-width: 100vw; overflow-x: hidden;">
	<svg class="cat_teasorientexpresscollectio">
		<pattern elementId="cat_teasorientexpresscollectio_A0_Rectangle_2" id="cat_teasorientexpresscollectio_A0_Rectangle_2_pattern" x="0" y="0" width="100%" height="100%">
			<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/panel/cat_teasorientexpresscollectio_A0_Rectangle_2_pattern.png" xlink:href="../../inc/imagenes/tablet/panel/cat_teasorientexpresscollectio_A0_Rectangle_2_pattern.png"></image>
		</pattern>
		<rect class="cat_teasorientexpresscollectio_Class" rx="0" ry="0" x="0" y="0" width="1024.5" height="1024">
		</rect>
	</svg>
	<svg class="Rect_ngulo_253">
		<linearGradient spreadMethod="pad" id="LinearGradientFill1" x1="0.5" x2="0.5" y1="0" y2="1">
			<stop offset="0" stop-color="#000" stop-opacity="1"></stop>
			<stop offset="1" stop-color="#666" stop-opacity="1"></stop>
		</linearGradient>
		<rect class="Rect_ngulo_253_Class" rx="0" ry="0" x="0" y="0" width="1025" height="768">
		</rect>
	</svg>
	<svg class="Rect_ngulo_260">
		<rect class="Rect_ngulo_260_Class" rx="0" ry="0" x="0" y="0" width="369" height="768">
		</rect>
	</svg>
	<div class="Grupo_20_Class">
		<div class="Grupo_17_Class">
			<div class="Grupo_16_Class">
			<svg style="width:inherit;height:inherit;overflow:visible;">
				<rect fill="url(#Grupo_16_A0_Group_31_pattern)" width="100%" height="100%"></rect>
					<pattern elementId="Grupo_16_A0_Group_31" id="Grupo_16_A0_Group_31_pattern" x="0" y="0" width="100%" height="100%">
						<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/panel/Grupo_16_A0_Group_31_pattern.png" xlink:href="../../inc/imagenes/tablet/panel/Grupo_16_A0_Group_31_pattern.png"></image>
					</pattern>
					</svg>
				</div>
		</div>
	</div>
	
		<svg class="Rect_ngulo_103">
			<a href="mensaje.html">
			<rect class="Rect_ngulo_103_Class" rx="4" ry="4" x="0" y="0" width="157" height="40">
			</rect>
			</a>
		</svg>
	
	<a href="mensaje.html">
		<div class="Ganar_puntos_Class">
			<span>Ganar puntos</span>
		</div>
	</a>
	<div class="_Gana_con_tu_compra__Class">
		<span>¡Gana </span><span style="color:rgba(255,255,255,1);">con tu compra!</span>
	</div>
	<div class="Tus_promociones_Class">
		<span>Tus promociones</span>
	</div>
	<div class="Redime_tus_puntos_Class">
		<span>Redime tus puntos</span>
	</div>
	<div class="Frapp_Teas_Class">
		<span>Frapp Teas</span>
	</div>
	<div class="Hola_Jairo__tienes_2300pts_Class">
		<span>Hola Jairo,</span><span style="font-style:normal;font-weight:normal;"> tienes</span><span style="color:rgba(239,41,103,1);"> 2300pts</span>
	</div>
	<div class="ID1800pts_para_el_sig__nivel_Class">
		<span></span><span style="color:rgba(239,41,103,1);">1800pts </span><span style="font-style:normal;font-weight:normal;">para el sig. nivel</span>
	</div>
	<div class="Grupo_30_Class">
		<svg class="Rect_ngulo_256">
			<rect class="Rect_ngulo_256_Class" rx="2.1854770183563232" ry="2.1854770183563232" x="0" y="0" width="189.429" height="4.371">
			</rect>
		</svg>
		<svg class="Rect_ngulo_257">
			<rect class="Rect_ngulo_257_Class" rx="2.1854770183563232" ry="2.1854770183563232" x="0" y="0" width="103.181" height="4.371">
			</rect>
		</svg>
	</div>
	<svg class="Elipse_9">
		<ellipse class="Elipse_9_Class" rx="59.14244842529297" ry="59.142578125" cx="59.14244842529297" cy="59.142578125">
		</ellipse>
	</svg>
	<div class="Silver_Class">
		<span>Silver</span>
	</div>
	<svg class="L_nea_4">
		<path class="L_nea_4_Class" d="M 0 0 L 327.7461547851563 0">
		</path>
	</svg>
	<svg class="L_nea_5">
		<path class="L_nea_5_Class" d="M 0 0 L 327.7461547851563 0">
		</path>
	</svg>
	<svg class="logo_izlah">
		<pattern elementId="logo_izlah_A0_Rectangle_18" id="logo_izlah_A0_Rectangle_18_pattern" x="0" y="0" width="100%" height="100%">
			<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/panel/logo_izlah_A0_Rectangle_18_pattern.png" xlink:href="../../inc/imagenes/tablet/panel/logo_izlah_A0_Rectangle_18_pattern.png"></image>
		</pattern>
		<rect class="logo_izlah_Class" rx="0" ry="0" x="0" y="0" width="102" height="16">
		</rect>
	</svg>
	<svg class="Sustracci_n_3" viewBox="0 0 56.96 113.51">
		<path class="Sustracci_n_3_Class" d="M 0.2052065581083298 113.5098114013672 C 0.1363208293914795 113.5098114013672 0.06882083415985107 113.5096664428711 -7.734897735645063e-06 113.5093994140625 L -7.734897735645063e-06 0.0003967102093156427 C 0.03828327730298042 4.8412213800475e-05 0.07587811350822449 6.978062174312072e-06 0.113527812063694 6.978062174312072e-06 C 0.1252935379743576 6.978062174312072e-06 0.1370642334222794 1.099591918318765e-05 0.1488636881113052 1.099591918318765e-05 L 0.2052065581083298 1.099591918318765e-05 C 7.866949558258057 1.099591918318765e-05 15.2995491027832 1.500582456588745 22.29653549194336 4.46005392074585 C 29.05487823486328 7.318568229675293 35.12447738647461 11.41076850891113 40.33676528930664 16.62299728393555 C 45.54912185668945 21.83531188964844 49.64140701293945 27.90491104125977 52.49993515014648 34.66321182250977 C 55.45949172973633 41.66024017333984 56.96010589599609 49.09295272827148 56.96010589599609 56.75489807128906 C 56.96010589599609 64.41685485839844 55.45949172973633 71.84957122802734 52.49993515014648 78.84659576416016 C 49.64137649536133 85.60494232177734 45.54909133911133 91.67453765869141 40.33676528930664 96.88679504394531 C 35.12446212768555 102.0990524291992 29.05486297607422 106.1912536621094 22.29653549194336 109.0497512817383 C 15.29956340789795 112.0092391967773 7.866963863372803 113.5098114013672 0.2052065581083298 113.5098114013672 Z">
		</path>
	</svg>
	<div class="Enmascarar_grupo_1_Class">
	<svg style="width:inherit;height:inherit;overflow:visible;">
		<rect fill="url(#Enmascarar_grupo_1_A0_Group_32_pattern)" width="100%" height="100%"></rect>
			<pattern elementId="Enmascarar_grupo_1_A0_Group_32" id="Enmascarar_grupo_1_A0_Group_32_pattern" x="0" y="0" width="100%" height="100%">
				<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/panel/Enmascarar_grupo_1_A0_Group_32_pattern.png" xlink:href="../../inc/imagenes/tablet/panel/Enmascarar_grupo_1_A0_Group_32_pattern.png"></image>
			</pattern>
			</svg>
		</div>
	<svg class="Rect_ngulo_195">
		<rect class="Rect_ngulo_195_Class" rx="4" ry="4" x="0" y="0" width="303" height="43.732">
		</rect>
	</svg>
	<div class="Ingresa_el_monto_de_tu_compra__Class">
		<span>Ingresa el monto de tu compra y gana</span>
	</div>
	<div class="Grupo_35_Class">
		<svg class="Rect_ngulo_398">
			<rect class="Rect_ngulo_398_Class" rx="4" ry="4" x="0" y="0" width="255" height="300">
			</rect>
		</svg>
		<svg class="Rect_ngulo_407">
			<rect class="Rect_ngulo_407_Class" rx="4" ry="4" x="0" y="0" width="255" height="300">
			</rect>
		</svg>
		<svg class="Rect_ngulo_410">
			<rect class="Rect_ngulo_410_Class" rx="4" ry="4" x="0" y="0" width="255" height="300">
			</rect>
		</svg>
		<a href="mensaje.html">
		<svg class="Rect_ngulo_397">
			<rect class="Rect_ngulo_397_Class" rx="4" ry="4" x="0" y="0" width="140" height="40">
			</rect>
		</svg>
		</a>
		<svg class="Trazado_47" viewBox="0 0 208 40">
			<path class="Trazado_47_Class" d="M 5.942857265472412 0 L 202.0571441650391 0 C 205.3392944335938 0 208 1.790860891342163 208 4 L 208 36 C 208 38.20914077758789 205.3392944335938 40 202.0571441650391 40 L 5.942857265472412 40 C 2.660707712173462 40 0 38.20914077758789 0 36 L 0 4 C 0 1.790860891342163 2.660707712173462 0 5.942857265472412 0 Z">
			</path>
		</svg>
		<svg class="Rect_ngulo_409">
			<rect class="Rect_ngulo_409_Class" rx="4" ry="4" x="0" y="0" width="140" height="40">
			</rect>
		</svg>
		<a href="mensaje.html">
		<div class="Lo_quiero_Class">
			<span>Lo quiero</span>
		</div>
		</a>
		<div class="Acumula_3000_pts__m_s_Class">
			<span>Acumula 3000 pts. más</span>
		</div>
		<div class="ID2_300_pts__Class">
			<span>2,300 pts.</span>
		</div>
		<div class="ID5_100pts__Class">
			<span>5,100pts.</span>
		</div>
		<div class="Matcha_Frapptea_Class">
			<span>Matcha Frapptea</span>
		</div>
		<div class="Chai_Walla_Class">
			<span>Chai Walla</span>
		</div>
		<div class="Enmascarar_grupo_2_Class">
		<svg style="width:inherit;height:inherit;overflow:visible;">
			<rect fill="url(#Enmascarar_grupo_2_A0_Group_33_pattern)" width="100%" height="100%"></rect>
				<pattern elementId="Enmascarar_grupo_2_A0_Group_33" id="Enmascarar_grupo_2_A0_Group_33_pattern" x="0" y="0" width="100%" height="100%">
					<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/panel/Enmascarar_grupo_2_A0_Group_33_pattern.png" xlink:href="../../inc/imagenes/tablet/panel/Enmascarar_grupo_2_A0_Group_33_pattern.png"></image>
				</pattern>
				</svg>
			</div>
		<div class="Enmascarar_grupo_3_Class">
		<svg style="width:inherit;height:inherit;overflow:visible;">
			<rect fill="url(#Enmascarar_grupo_3_A0_Group_34_pattern)" width="100%" height="100%"></rect>
				<pattern elementId="Enmascarar_grupo_3_A0_Group_34" id="Enmascarar_grupo_3_A0_Group_34_pattern" x="0" y="0" width="100%" height="100%">
					<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/panel/Enmascarar_grupo_3_A0_Group_34_pattern.png" xlink:href="../../inc/imagenes/tablet/panel/Enmascarar_grupo_3_A0_Group_34_pattern.png"></image>
				</pattern>
				</svg>
			</div>
		<svg class="matcha">
			<pattern elementId="matcha_A0_Rectangle_32" id="matcha_A0_Rectangle_32_pattern" x="0" y="0" width="100%" height="100%">
				<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/panel/matcha_A0_Rectangle_32_pattern.png" xlink:href="../../inc/imagenes/tablet/panel/matcha_A0_Rectangle_32_pattern.png"></image>
			</pattern>
			<rect class="matcha_Class" rx="4" ry="4" x="0" y="0" width="255" height="163">
			</rect>
		</svg>
		<svg class="cat_pateasserie_hover">
			<pattern elementId="cat_pateasserie_hover_A0_Rectangle_34" id="cat_pateasserie_hover_A0_Rectangle_34_pattern" x="0" y="0" width="100%" height="100%">
				<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/panel/cat_pateasserie_hover_A0_Rectangle_34_pattern.png" xlink:href="../../inc/imagenes/tablet/panel/cat_pateasserie_hover_A0_Rectangle_34_pattern.png"></image>
			</pattern>
			<rect class="cat_pateasserie_hover_Class" rx="4" ry="4" x="0" y="0" width="255" height="163">
			</rect>
		</svg>
		<svg class="cat_beachclubteas_hover">
			<pattern elementId="cat_beachclubteas_hover_A0_Rectangle_36" id="cat_beachclubteas_hover_A0_Rectangle_36_pattern" x="0" y="0" width="100%" height="100%">
				<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/panel/cat_beachclubteas_hover_A0_Rectangle_36_pattern.png" xlink:href="../../inc/imagenes/tablet/panel/cat_beachclubteas_hover_A0_Rectangle_36_pattern.png"></image>
			</pattern>
			<rect class="cat_beachclubteas_hover_Class" rx="4" ry="4" x="0" y="0" width="255" height="163">
			</rect>
		</svg>
		<div class="Lo_quiero_A0_Text_16_Class">
			<span>Lo quiero</span>
		</div>
		<div class="ID1_200pts_Class">
			<span>1,200pts</span>
		</div>
		<div class="Club_Tea_Class">
			<span>Club Tea</span>
		</div>
	</div>
	<div class="Grupo_36_Class">
		<svg class="Rect_ngulo_398_A0_Rectangle_37">
			<rect class="Rect_ngulo_398_A0_Rectangle_37_Class" rx="4" ry="4" x="0" y="0" width="535" height="225">
			</rect>
		</svg>
		<a href="mensaje.html">
		<svg class="Rect_ngulo_397_A0_Rectangle_38">
			<rect class="Rect_ngulo_397_A0_Rectangle_38_Class" rx="4" ry="4" x="0" y="0" width="140" height="40">
			</rect>
		</svg>
		<div class="Aplicar_Promo_Class">
			<span>Aplicar Promo</span>
		</div>
		</a>
		<div class="ID2x1_en_4Ever_Berrys_Class">
			<span>2x1 en 4Ever Berrys</span>
		</div>
		<div class="Enmascarar_grupo_2_A0_Group_18_Class">
		<svg style="width:inherit;height:inherit;overflow:visible;">
			<rect fill="url(#Enmascarar_grupo_2_A0_Group_35_pattern)" width="100%" height="100%"></rect>
				<pattern elementId="Enmascarar_grupo_2_A0_Group_35" id="Enmascarar_grupo_2_A0_Group_35_pattern" x="0" y="0" width="100%" height="100%">
					<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/panel/Enmascarar_grupo_2_A0_Group_35_pattern.png" xlink:href="../../inc/imagenes/tablet/panel/Enmascarar_grupo_2_A0_Group_35_pattern.png"></image>
				</pattern>
				</svg>
			</div>
		<div class="Enmascarar_grupo_3_A0_Group_23_Class">
		<svg style="width:inherit;height:inherit;overflow:visible;">
			<rect fill="url(#Enmascarar_grupo_3_A0_Group_36_pattern)" width="100%" height="100%"></rect>
				<pattern elementId="Enmascarar_grupo_3_A0_Group_36" id="Enmascarar_grupo_3_A0_Group_36_pattern" x="0" y="0" width="100%" height="100%">
					<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/panel/Enmascarar_grupo_3_A0_Group_36_pattern.png" xlink:href="../../inc/imagenes/tablet/panel/Enmascarar_grupo_3_A0_Group_36_pattern.png"></image>
				</pattern>
				</svg>
			</div>
		<svg class="cat_teas4everyberry_hover">
			<pattern elementId="cat_teas4everyberry_hover_A0_Rectangle_44" id="cat_teas4everyberry_hover_A0_Rectangle_44_pattern" x="0" y="0" width="100%" height="100%">
				<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/panel/cat_teas4everyberry_hover_A0_Rectangle_44_pattern.png" xlink:href="../../inc/imagenes/tablet/panel/cat_teas4everyberry_hover_A0_Rectangle_44_pattern.png"></image>
			</pattern>
			<rect class="cat_teas4everyberry_hover_Class" rx="4" ry="4" x="0" y="0" width="255" height="225">
			</rect>
		</svg>
		<div class="V_lido_al_30_de_Agosto_de_2019_Class">
			<span>Válido al 30 de Agosto de 2019</span>
		</div>
	</div>
	<div class="Grupo_37_Class">
		<svg class="Rect_ngulo_398_A0_Rectangle_45">
			<rect class="Rect_ngulo_398_A0_Rectangle_45_Class" rx="4" ry="4" x="0" y="0" width="535" height="225">
			</rect>
		</svg>
		<svg class="Rect_ngulo_397_A0_Rectangle_46">
			<rect class="Rect_ngulo_397_A0_Rectangle_46_Class" rx="4" ry="4" x="0" y="0" width="140" height="40">
			</rect>
		</svg>
		<div class="Aplicar_Promo_A0_Text_22_Class">
			<span>Aplicar Promo</span>
		</div>
		<div class="ID2x1_en_4Ever_Berrys_A0_Text_23_Class">
			<span>2x1 en 4Ever Berrys</span>
		</div>
		<div class="Enmascarar_grupo_2_A0_Group_25_Class">
		<svg style="width:inherit;height:inherit;overflow:visible;">
			<rect fill="url(#Enmascarar_grupo_2_A0_Group_37_pattern)" width="100%" height="100%"></rect>
				<pattern elementId="Enmascarar_grupo_2_A0_Group_37" id="Enmascarar_grupo_2_A0_Group_37_pattern" x="0" y="0" width="100%" height="100%">
					<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/panel/Enmascarar_grupo_2_A0_Group_37_pattern.png" xlink:href="../../inc/imagenes/tablet/panel/Enmascarar_grupo_2_A0_Group_37_pattern.png"></image>
				</pattern>
				</svg>
			</div>
		<div class="Enmascarar_grupo_3_A0_Group_30_Class">
		<svg style="width:inherit;height:inherit;overflow:visible;">
			<rect fill="url(#Enmascarar_grupo_3_A0_Group_38_pattern)" width="100%" height="100%"></rect>
				<pattern elementId="Enmascarar_grupo_3_A0_Group_38" id="Enmascarar_grupo_3_A0_Group_38_pattern" x="0" y="0" width="100%" height="100%">
					<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/panel/Enmascarar_grupo_3_A0_Group_38_pattern.png" xlink:href="../../inc/imagenes/tablet/panel/Enmascarar_grupo_3_A0_Group_38_pattern.png"></image>
				</pattern>
				</svg>
			</div>
		<svg class="blossom">
			<pattern elementId="blossom_A0_Rectangle_52" id="blossom_A0_Rectangle_52_pattern" x="0" y="0" width="100%" height="100%">
				<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/panel/blossom_A0_Rectangle_52_pattern.png" xlink:href="../../inc/imagenes/tablet/panel/blossom_A0_Rectangle_52_pattern.png"></image>
			</pattern>
			<rect class="blossom_Class" rx="4" ry="4" x="0" y="0" width="255" height="225">
			</rect>
		</svg>
		<div class="V_lido_al_30_de_Agosto_de_2019_A0_Text_24_Class">
			<span>Válido al 30 de Agosto de 2019</span>
		</div>
	</div>
	<svg class="Rect_ngulo_414">
		<linearGradient spreadMethod="pad" id="LinearGradientFill2" x1="0" x2="0.5" y1="1" y2="1">
			<stop offset="0" stop-color="#000" stop-opacity="0"></stop>
			<stop offset="0.2425989955663681" stop-color="#0b0b0b" stop-opacity="0.06274509803921569"></stop>
			<stop offset="1" stop-color="#545454" stop-opacity="0.5019607843137255"></stop>
		</linearGradient>
		<rect class="Rect_ngulo_414_Class" rx="0" ry="0" x="0" y="0" width="360" height="768">
		</rect>
	</svg>
	<svg class="Trazado_45" viewBox="0 0 14 20">
		<path class="Trazado_45_Class" d="M 3.818182468414307 0 L 0 3.749999523162842 L 6.363637447357178 10 L 0 16.24999618530273 L 3.818182468414307 20 L 14 10 L 3.818182468414307 0 Z">
		</path>
	</svg>
	<svg class="Trazado_46" viewBox="0 0 14 20">
		<path class="Trazado_46_Class" d="M 3.818182468414307 0 L 0 3.749999523162842 L 6.363637447357178 10 L 0 16.24999618530273 L 3.818182468414307 20 L 14 10 L 3.818182468414307 0 Z">
		</path>
	</svg>
</div>
</body>
</html>
