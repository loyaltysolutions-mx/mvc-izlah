<?php 
	
	
	include '../../inc/funciones.php';
	include '../../inc/parametros.php';
	//$inst_control=new C_home($ser,$usu,$pas,$bd);	
	$ins_funciones=new Funciones_Basicas();
	/*session_start();
	if(isset($_SESSION["usuario"])){ 
	    $catalogue = 1;
	    //echo "<script>console.log('".$catalogue." estas logueado');</script>";
	}else{*/
    $catalogue = 0;

      //TRAEMOS CONFIGURACION DE ESTILOS
   $res_con1=$ins_funciones->consulta_generica('tbl_estilo', ' ');
   $registro1= mysqli_fetch_assoc($res_con1);

   $res_con2=$ins_funciones->consulta_generica('tbl_configuracion_proyecto', ' ');
   $config = mysqli_fetch_assoc($res_con2);
   
   //VALIDAMOS LOGO
  if($registro1['logo_cte']==''){
      $img_logo1='imagenes/logo.png';
  }else{
      $img_logo1='imagenes/img_configuracion/'.$registro1['logo_cte'];
  }

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta content="yes" name="mobile-web-app-capable">
	<link rel="icon" sizes="192x192" href="../../inc/<?php echo $img_logo1 ?>">
	<title>:: <?php echo $config['titulo_home'] ?> :: <?php echo $config['detalle_home'] ?> </title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<link href="https://fonts.googleapis.com/css?family=Oswald&display=swap" rel="stylesheet">
	<style type="text/css">
		*{
			font-family: 'Oswald', sans-serif;
		}

		/* Safari 4.0 - 8.0 */
        @-webkit-keyframes btnAnimation {
          0%   {-webkit-box-shadow: 5px 5px 8px 3px black;}
          50%  {-webkit-box-shadow: 5px 5px 8px 3px rgba(255,255,255,0.5);}
          100% {-webkit-box-shadow: 5px 5px 8px 3px black;}
        }

        /* Standard syntax */
        @keyframes btnAnimation {
          0%   {box-shadow: 5px 5px 8px 3px inherit}
          50%  {box-shadow: 5px 5px 8px 3px rgba(255,255,255,0.5);}
          100% {box-shadow: 5px 5px 8px 3px inherit}
        }

        .btnHome {
            -webkit-animation-name: btnAnimation; /* Safari 4.0 - 8.0 */
            -webkit-animation-duration: 2s; /* Safari 4.0 - 8.0 */
            -webkit-animation-iteration-count: infinite; /* Safari 4.0 - 8.0 */
            -webkit-animation-timing-function: ease-out;
            animation-name: btnAnimation;
            animation-duration: 2s;
            animation-iteration-count: infinite;
            animation-timing-function: ease-out;          
        }

		#mainContainer{
			background:url('../../inc/imagenes/tablet/index/toca_1_A0_Rectangle_2_pattern.png') no-repeat;
			background-size: cover;
			background-position: center center;
			/*background-attachment: fixed;*/
		}

		.btn-primary{
			background-color: #EF2967;
			-webkit-box-shadow: 6px 11px 5px rgba(0,0,0,0.7);
			box-shadow: 6px 11px 5px rgba(0,0,0,0.7);
			-o-box-shadow: 6px 11px 5px rgba(0,0,0,0.7);
			-moz-box-shadow: 6px 11px 5px rgba(0,0,0,0.7);
			border-color: #EF2967 !important;			
			padding: 15px;
			border-radius: 20px;
			font-size: 16pt;
			font-weight: bold;
		}

		.btn-primary:focus{
			background-color: #EF2967;
			-webkit-box-shadow: 6px 11px 5px rgba(0,0,0,0.7);
			box-shadow: 6px 11px 5px rgba(0,0,0,0.7);
			-o-box-shadow: 6px 11px 5px rgba(0,0,0,0.7);
			-moz-box-shadow: 6px 11px 5px rgba(0,0,0,0.7);
			border-color: #EF2967 !important;			
			padding: 15px;
			border-radius: 20px;
			font-size: 16pt;
			font-weight: bold;
		}

		.btn-primary:active{
			background-color: #EF2967;
			-webkit-box-shadow: 6px 11px 5px rgba(0,0,0,0.7);
			box-shadow: 6px 11px 5px rgba(0,0,0,0.7);
			-o-box-shadow: 6px 11px 5px rgba(0,0,0,0.7);
			-moz-box-shadow: 6px 11px 5px rgba(0,0,0,0.7);
			border-color: #EF2967 !important;			
			padding: 15px;
			border-radius: 20px;
			font-size: 16pt;
			font-weight: bold;
		}

		.btn:hover{
			color: #EF2967;
			background-color: white;

		}

		.btn:active{
			color: #EF2967;
			background-color: white !important;
			border-color: #EF2967 !important;
		}

		.btn:focus{
			color: #EF2967;
			background-color: white !important;
			border-color: #EF2967 !important;
		}

	</style>	
</head>
<body>
<?php 
	//if(isset($config['puntosInicio']) && $config['puntosInicio'] == 1 ){ 
?>
	<div id="mainContainer" class="container-fluid">
		<div class="row justify-content-end">
			<div class="col-lg-6 col-md-6 col-sm-9 col-xs-9 text-right">
				<img class="imgFix" src="../../inc/imagenes/tablet/index/Grupo_16_A0_Group_8_pattern.png" />
			</div>
		</div>
		<div class="row justify-content-center">			
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
				<br>
				<img class="imgFix" src="../../inc/imagenes/tablet/index/logo_izlah_A0_Rectangle_13_pattern.png"/>
			</div>
		</div>
		<div class="row justify-content-center">
			<div class="col-lg-8 col-md-8 col-sm-9 col-xs-9">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
						<br>
						<span style="font-size:3.1rem; font-weight: 500; color:white;" class="text-center">¿Quieres ganar?</span>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
						<br>
						<a class="btn btn-primary btnHome" href="login.php" style="width:100%"> TOCA AQUI </a>
						<!--a id="btnScreen" class="btn btn-primary" onclick="start()" style="width:100%"> START </a-->
					</div>
				</div>
			</div>
		</div>
	</div>
<?php 
	/*}else{ 
		header("Location:login.php");
	}*/
?>
<script type="text/javascript">	
	$(window).on("resize", function(){
		$("#mainContainer").height($(window).height());		
	});	
	$("#mainContainer").height($(window).height());

	
	function ready(){
		const { type } = screen.orientation;
		console.log(`Fullscreen and locked to ${type}. Ready!`);
	}

	async function start(){
		try{
			await document.body.requestFullscreen();
			await screen.orientation.lock("landscape");	
		}catch(err){
			console.log(err);
		}
		
		ready();
	}

	$(document).ready(function(){
		window.setTimeout(function(){
			$("#btnScreen").trigger("click");
		},100);
		
	});
	
</script>
</body>
</html>