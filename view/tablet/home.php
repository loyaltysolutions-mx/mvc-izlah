<?php 
	
	include('../../inc/parametros.php');
    require("../../controller/registro/c_registro.php");
    require('../../inc/funciones.php');
	//$inst_control=new C_home($ser,$usu,$pas,$bd);	
	$ins_funciones=new Funciones_Basicas();
	/*session_start();
	if(isset($_SESSION["usuario"])){ 
	    $catalogue = 1;
	    //echo "<script>console.log('".$catalogue." estas logueado');</script>";
	}else{*/
    $catalogue = 0;

      //TRAEMOS CONFIGURACION DE ESTILOS
   $res_con1=$ins_funciones->consulta_generica('tbl_estilo', ' ');
   $registro1= mysqli_fetch_assoc($res_con1);

   $res_con2=$ins_funciones->consulta_generica('tbl_configuracion_proyecto', ' ');
   $config = mysqli_fetch_assoc($res_con2);
   
   //VALIDAMOS LOGO
	if($registro1['logo_cte']==''){
		$img_logo1='imagenes/logo.png';
  	}else{
		$img_logo1='imagenes/img_configuracion/'.$registro1['logo_cte'];
  	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta content="yes" name="mobile-web-app-capable">
	<link rel="icon" sizes="192x192" href="../../inc/<?php echo $img_logo1 ?>">
	<title>:: <?php echo $config['titulo_home'] ?> :: <?php echo $config['detalle_home'] ?> </title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<link href="https://fonts.googleapis.com/css?family=Oswald:300,400&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/4742d4c7f3.js"></script>	
	<style type="text/css">
		*{
			font-family: Oswald, sans-serif;
			color: white;
		}

		.spanText{
			font-weight: 500; 
			color:#EF2967;
		}

		body{
			/*background:url('../../inc/imagenes/tablet/cat_chocolateas_hover_A19_Rectangle_2_pattern@2x.png') no-repeat;*/
			/*background: url('../../inc/imagenes/tablet/panel/dark-black-gradient.jpg') no-repeat;
			background-size: cover;
			background-position: top center;*/
			background-color:#1f1f1f;
			line-height: 4vh;
		}

		.btn-disabled{
			background-color: gray;			
			border-color: white !important;			
			padding: 8px;
			color: white;
			font-family: Helvetica;
			font-size: 0.5em !important;
		}

		.btn-primary{
			background-color: #EF2967;
			-webkit-box-shadow: 4px 5px 20px 0px rgba(62, 61, 61, 0.85);
			box-shadow: 4px 5px 20px 0px rgba(62, 61, 61, 0.85);
			-o-box-shadow: 4px 5px 20px 0px rgba(62, 61, 61, 0.85);
			-moz-box-shadow: 4px 5px 20px 0px rgba(62, 61, 61, 0.85);
			border-color: #EF2967 !important;			
			padding: 2vh;
			font-family: Helvetica;
			font-size: 0.5em !important;
		}

		.btn-primary:hover{
			color: #EF2967;			
			background-color: white;
			-webkit-box-shadow: 4px 5px 20px 0px #ef2967;
			box-shadow: 4px 5px 20px 0px #ef2967;
			-o-box-shadow: 4px 5px 20px 0px #ef2967;
			-moz-box-shadow: 4px 5px 20px 0px #ef2967;
			
		}

		.btn-primary:active{
			color: #EF2967;			
			background-color: white;
			-webkit-box-shadow: 4px 5px 20px 0px #ef2967;
			box-shadow: 4px 5px 20px 0px #ef2967;
			-o-box-shadow: 4px 5px 20px 0px #ef2967;
			-moz-box-shadow: 4px 5px 20px 0px #ef2967;
		}

		input{			
			border: transparent;
			padding-left: 5px;

		}

		.lds-dual-ring {
	        display: inline-block;
	        width: 64px;
	        height: 64px;
	    }
	    .lds-dual-ring:after {
	        content: " ";
	        display: block;
	        width: 46px;
	        height: 46px;
	        margin: 1px;
	        border-radius: 50%;
	        border: 5px solid #fff;
	        border-color: #EF2967 transparent #EF2967 transparent;
	        animation: lds-dual-ring 1.2s linear infinite;
	    }
	    @keyframes lds-dual-ring {
	        0% {
	            transform: rotate(0deg);
	        }
	        100% {
	            transform: rotate(360deg);
	        }
	    }

	    .input-group-text{
	    	background-color: #EF2967;
	    	color: white;
	    	border-color: #EF2967;
	    }

	    input{
    	    width: 20vw;
		    font-family: Helvetica;
		    font-size: 2vh !important;
		    font-weight: bold;
		    height: 6vh !important;
		    padding: 1vh !important;
		    border-radius: 5px;
		    color: black;
	    }

	    select{
	    	width: auto;
	    	font-family: Helvetica;
	    	font-size: 21pt;
	    	height: 35pt !important;
	    }

	    .progress{
	    	height: 0.5em;
	    }

	    .progress-bar{
	    	background-color: #EF2967;
	    }

	    /*.scrolling-wrapper {
			overflow-x: scroll;
			overflow-y: hidden;
			white-space: nowrap;
			-webkit-overflow-scrolling: touch;		
		}


		
		.card {
			background-color: black;
			display: inline-block;
		}*/

		.scrolling-wrapper-flexbox {
			display: flex;
			flex-wrap: nowrap;
			overflow-x: auto;
		}

		.card {
			flex: 0 0 auto;
			background-color: black;
			margin-right: 8px;
			border-radius: 8px;
		}

		::-webkit-scrollbar {
			display: none;
		}	

		.homePage {
			position: absolute;
			top: 2vh;
			right: 2vh;
			height: 4vh;
			width: 4vh;
			z-index: 1;
		}

		.homePage img {
			display: block;
			width: 100%;
		}

		p{
			margin-bottom: 0 !important;
		}

		.progress{
			border: 1px solid #ef2967;
		}
	</style>	
</head>
<body>
	<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-center" id="exampleModalLongTitle">Error</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  		<span aria-hidden="true">&times;</span>
					</button>
				</div>
			  	<div class="modal-body" style="padding:20px">
			    	<center id="textError">¡Felicidades, tu registro se ha completado correctamente!<br>
			    	Ya puedes empezar a ganar puntos.</center>
			  	</div>
			  	<div class="modal-footer">			    	
			    	<button type="button" class="btn btn-primary" data-dismiss="modal" style="padding:10px 30px">Ok</button>
			  	</div>
			</div>
		</div>
	</div>
<?php 
	$tel = isset($_POST['tel']) ? $_POST['tel'] : NULL;
    $mail = isset($_POST['mail']) ? $_POST['mail'] : NULL;
    $id = isset($_POST['id']) ? $_POST['id'] : NULL;
    $qr = isset($_POST['idUsuario']) ? $_POST['idUsuario'] : NULL;

    //print_r($_POST);
    //die();

    if($tel != NULL || $mail != NULL || $id != NULL || $qr != NULL){
        

        $ins_fun_basicas = new Funciones_Basicas();
        $configuracion=$ins_fun_basicas->consulta_generica('tbl_configuracion_proyecto', '');
        $configuracion = mysqli_fetch_assoc($configuracion);
        $inst = new C_registro($ser,$usu,$pas,$bd);
        $usuario = $inst->get_usuario($qr, $tel, $mail, $id);

        //print_r($usuario);
        //die();
        if(count($usuario) > 0){
            $redenciones = $inst->get_productos_promociones($usuario[0]['idUsuario']);
            //print_r($redenciones);
            //die();

            $barProgress = (int)(($usuario[0]['puntos_disponibles'][0]['puntos_totales'] / $usuario[0]['finNivel']) *100);
?>
	<div id="mainContainer" class="container-fluid">
		<div class="row" style="height: 100vh; display: -webkit-inline-box">
			<div id="colPerfil" style="background-color: black; height: 100vh; width: 30vw">				
				<div style="height: 95vh;">
					<div style="padding: 1vh; text-align: center">
						<img src="../../inc/imagenes/tablet/panel/logo_izlah_A0_Rectangle_18_pattern@2x.png" style="height: 3vh" />
					</div>
					<div style="padding-top: 1vh; text-align: center">
						<img src="../../inc/imagenes/user.png" class="rounded-circle" style="height: 20vh">	
					</div>
					<div style="text-align: center">
						<span class="spanText" style="font-size: 3vh; font-weight: bold"><?php echo isset($usuario[0]['nombreNivel']) ? "Nivel Actual: ".$usuario[0]['nombreNivel'] : "" ?></span>
					</div>
					<div style="padding:1vh 7vw">
						<div class="progress" style="height: 1vh; width: 100%; text-align: center">
							<div class="progress-bar" role="progressbar" style="width: <?php echo isset($barProgress) ? $barProgress  : 0; ?>%; height:1vh" aria-valuenow="100" aria-valuemin="100" aria-valuemax="100"></div>
						</div>
					</div>
					<div style="text-align: center;">
						<label style="font-size: 3vh"><span class="spanText"><?php echo isset( $usuario[0]['puntos_disponibles'][0]['puntos_totales']) ? $usuario[0]['finNivel'] - $usuario[0]['puntos_disponibles'][0]['puntos_totales'] : $usuario[0]['finNivel'] ?> pts.</span> para el sig. nivel</label>
					</div>
					<div style="text-align: center; margin-top: 3vh; padding-top: 1vh">
						<h5 style="font-size: 3vh">Hola <?php echo $usuario[0]['nombreUsuario'] ?>, tienes <span class="spanText"><?php echo $usuario[0]['puntos_disponibles'][0]['puntos_totales'] ? $usuario[0]['puntos_disponibles'][0]['puntos_totales'] : 360;?> pts.</span></h5>
					</div>
					<div style="text-align: center; padding:0.5vh 2.5vw">
						<div style="width: 25vw; border-top: 0.5vh solid #EF2967; border-bottom: 0.5vh solid #EF2967; text-align: center">
							<h4 style="font-size: 4vh"><span class="spanText">¡Gana</span> con tu compra!</h4>
						</div>							
					</div>
					<div style="text-align: center;">
						<form id="formAcumular">
							<div style="margin-bottom: 2vh; margin-top: 1vh">
								<input id="monto" type="number" name="monto" required autocomplete="off" placeholder="Ingresa el monto de compra" step="0.01" pattern="^[0-9]">
							</div>
							<div style="margin-bottom: 2vh">
								<input id="ticket" type="tel" name="ticket" maxlength="6" minlength="2" required autocomplete="off" placeholder="Ingresa el ticket de compra" pattern="^[A-Za-z0-9]{2,6}">
							</div>
							<input type="hidden" name="user" required value="<?php echo $usuario[0]['idUsuario']?>"/>
							<div style="text-align: center">
								<button type="submit" class="btn btn-primary" style="width: 16vw; font-size: 2vh !important;"> Ganar Puntos </button>
							</div>								
						</form>
					</div>
				</div>
				<div style="height: 5vh; text-align: center;">
					<img src="../../inc/imagenes/tablet/panel/Grupo_16_A0_Group_31_pattern.png" style="height: 3vh;">
				</div>
			</div>
			<div id="colRedencion" style="height: 100vh; width: 70vw">				
				<div style="height: 100vh">
					<div style="height: 60vh;">
						<div style="padding: 1vh 2vw;">
							<span style="font-size: 5vh">Redime tus puntos</span>
							<a class="homePage" href="../../view/tablet/index.php"><img src="../../inc/imagenes/tablet/closeBtn.png" style="width: 100%"></a>
						</div>
						<div class="scrolling-wrapper-flexbox" style="margin: 1vh 1vw;">
							<?php if ( isset($redenciones['productos']) && count($redenciones['productos']) > 0 ) { ?>
								<?php foreach ($redenciones['productos'] as $key) { ?>
									<?php $enable =  $key['valor_puntos'] > $usuario[0]['puntos_disponibles'][0]['puntos_totales'] ?>
									<div class="card" style="width: 38vh; height: 50vh">
										<img src="<?php echo $key['imagen'] != null && $key['imagen'] != "" && file_exists("../../inc/imagenes/img_catalogo/".$key['imagen']) ? "../../inc/imagenes/img_catalogo/".$key['imagen'] : "http://www.culturalwellnesscenter.org/wp-content/uploads/2015/11/no-image-available.png" ?>" style="width: 38vh; height: 28vh; border-radius: 8px 8px 0 0" />
										<div class="text-center" style="padding-top: 1vh">
											<span style="font-size:3vh"><?php echo mb_strtoupper($key['nombre'],"utf-8") ?></span>
											<p style="font-size:3vh"><?php echo $key['valor_puntos'] ?> pts.</p>
											<a href="javascript:;" class="btn <?php echo !$enable ? 'btn-primary' : 'btn-disabled' ?> openRedencion" style="margin-top:1vh; width: 35vh; <?php echo !$enable ? 'font-size: 2.5vh !important;' : 'font-size: 2.5vh !important;' ?>"><?php echo $enable ? "Acumula ".($key['valor_puntos'] - $usuario[0]['puntos_disponibles'][0]['puntos_totales'])." puntos más" : "Lo quiero" ?> </a>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>	
					<div style="height: 40vh">
						<div style="padding: 0vh 2vw;">
							<h4 style="font-size: 5vh">Tus promociones</h4>
						</div>
						<div class="scrolling-wrapper-flexbox" style="margin: 0vh 1vw; height: 32vh">
							<?php if ( (isset($redenciones['promociones']) && count($redenciones['promociones']) > 0 ) || (isset($redenciones['cashback']) && count($redenciones['cashback']) > 0 )  ){ ?>
                        		<?php foreach ($redenciones['cashback'] as $key) { ?>
									<div class="card" style="width: 60vh; height: 30vh">
										<div class="row no-gutters" style="height: 100%">
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
												<img src="../../inc/imagenes/tablet/panel/blossom_A0_Rectangle_52_pattern.png" style="width: 30vh; height: 30vh; border-top-left-radius: 8px; border-bottom-left-radius: 8px" >
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
												<div style="text-align: center;">
													<div style="height: 4vh">
														<label style="font-size:3vh;"><?php echo mb_strtoupper($key['nombre'],'utf-8') ?></label>	
													</div>
													<div style="padding-top:2vh; height: 14vh">
														<p style="font-size: 2vh; text-align: justify; padding: 0 1vw;"><?php echo $key['detalle'] ?></p>	
													</div>
													<div style="padding-top:2vh; height: 4vh">
														<a href="javascript:;" class="btn btn-primary openRedencion" style="width: 28vh; font-size: 2vh !important; "> Lo quiero </a>
													</div>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>                        
                        		<?php foreach ($redenciones['promociones'] as $key) { ?>
                        			<div class="card" style="width: 60vh; height: 30vh">
										<div class="row no-gutters" style="height: 100%; display: -webkit-inline-box">
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
												<img src="../../inc/imagenes/tablet/panel/blossom_A0_Rectangle_52_pattern.png" style="width: 30vh; height: 30vh; border-top-left-radius: 8px; border-bottom-left-radius: 8px" >
											</div>
											<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
												<div style="text-align: center;">
													<div style="height: 4vh">
														<label style="font-size:3vh;"><?php echo mb_strtoupper($key['nombre'],'utf-8') ?></label>	
													</div>
													<div style="padding-top:2vh; height: 14vh">
														<p style="font-size: 2vh; text-align: center; padding: 0 1vw;"><?php echo $key['detalle'] ?></p>	
													</div>
													<div style="padding-top:2vh; height: 4vh">
														<a href="javascript:;" class="btn btn-primary openRedencion" style="width: 26vh; font-size: 3vh !important; "> Lo quiero </a>
													</div>
												</div>
											</div>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<style type="text/css">
		.overlay-acumulacion {
			position: fixed;
			top: 0;
			left: 0;
			width: 0;
			height: 100vh;
			width: 100vw;
			background-color: rgba(0,0,0,1);
			z-index: 100;
			visibility: hidden;
			opacity: 0;
			transition: visibility 0s linear 300ms, opacity 300ms;
		}
		.overlay-acumulacion.open {
			visibility: visible;
			opacity: 1;
			transition: visibility 0s linear 0s, opacity 300ms;
		}
		.logoOverlay {
			position: absolute;
			top: 30px;
			left: 50%;
			transform: translateX(-50%);
		}
		.closeOverlay {
			position: absolute;
			top: 20px;
			right: 30px;
			height: 40px;
			width: 40px;
		}
		.containerOverlay {
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%,-50%);
			width: 1000px;
			display: none;
		}
		.titleContainerOverlay {
			font-family: Oswald, sans-serif;
			display: inline-block;
			width: 100%;
			margin: 0 0 40px 0;
			text-align: center;
			font-weight: 300;
		}
		.nameOverlay {
			font-weight: 400;
		}
		.montoOverlay {
			font-weight: 400;
			color:#ef2967;
		}

		.ticketOverlay {
			font-weight: 400;
			color:#ef2967;
		}
		.textOverlay {
			font-family: Oswald, sans-serif;
			display: inline-block;
			width: 100%;
			margin: 0 0 20px 0;
			text-align: center;
			font-weight: 300;
			font-size: 20px;
		}
		.formCheckAcumulacion {
			display: block;
			width: 300px;
			margin: 0 auto;
		}
		#idCajero {
			text-align: center;
		}
		.btnOverlaySubmit {
			display: block;
			margin: 30px auto 0 auto;
			font-size: 14px !important;
			width: 100%;
		}
		.containerOverlayMesaje {
			position: absolute;
			top: 50%;
			left: 50%;
			transform: translate(-50%,-50%);
			width: 1000px;
			display: none;
		}
		.puntosOverlay {
			font-weight: 400;
			color:#ef2967;
		}
		.titleContainerOverlayMesaje {
			font-family: Oswald, sans-serif;
			display: inline-block;
			width: 100%;
			text-align: center;
			font-weight: 400;
			font-size: 40px;
		}

		.lds-dual-ring {
	        display: inline-block;
	        width: 64px;
	        height: 64px;
	    }
	    .lds-dual-ring:after {
	        content: " ";
	        display: block;
	        width: 46px;
	        height: 46px;
	        margin: 1px;
	        border-radius: 50%;
	        border: 5px solid #fff;
	        border-color: #EF2967 transparent #EF2967 transparent;
	        animation: lds-dual-ring 1.2s linear infinite;
	    }
	    @keyframes lds-dual-ring {
	        0% {
	            transform: rotate(0deg);
	        }
	        100% {
	            transform: rotate(360deg);
	        }
	    }
	</style>
	<script src="../../inc/js/confetti.js"></script>
	<div class="overlay-acumulacion" >
		<img class="logoOverlay" src="../../inc/imagenes/tablet/panel/logo_izlah_A0_Rectangle_18_pattern@2x.png">
		<img class="closeOverlay" src="../../inc/imagenes/tablet/closeBtn.png">
		<div class="containerOverlay">
			<h2 class="titleContainerOverlay">
				<span class="nameOverlay">Hola <?php echo $usuario[0]['nombreUsuario'] ?>, ingresaste una compra por</span>
				<span class="montoOverlay"></span>
				<span class="nameOverlay">y ticket </span>
				<span id="tagTicket" class="ticketOverlay"></span>
			</h2>
			<p class="textOverlay">Pide al cajero que ingrese su código</p>
			<form id="formPassword">
				<div class="formCheckAcumulacion">
					<input id="idCajero" type="password" class="form-control" name="password" required autocomplete="off" placeholder="Ingresa codigo de cajero" pattern="^[0-9]{2,6}">
					<button id="btnPassword" class="btn btn-primary btnOverlaySubmit">Aceptar</button>
					<br>
					<div id="loadAnimation" style="display:none; width:100%; text-align:center"><div class="lds-dual-ring"></div></div>
				</div>
			</form>
		</div>
		<div class="containerOverlayMesaje">
			<h2 class="titleContainerOverlayMesaje">¡Felicidades, ganaste <span id="txtPuntos" class="puntosOverlay"></span>!</h2>
		</div>
	</div>

	<style type="text/css">
		.overlay-redencion {
			position: fixed;
			top: 0;
			left: 0;
			width: 0;
			height: 100vh;
			width: 100vw;
			background-color: rgba(0,0,0,1);
			z-index: 100;
			visibility: hidden;
			opacity: 0;
			transition: visibility 0s linear 300ms, opacity 300ms;
		}
		.overlay-redencion.open {
			visibility: visible;
			opacity: 1;
			transition: visibility 0s linear 0s, opacity 300ms;
		}
		.listCode {
			display: block;
			width: 550px;
			margin: 0 auto;
		}
		.exampleCode {
			display: block;
			width: 100%;
		}
		.colorOverlay {
			font-weight: 400;
			color:#ef2967;
		}
		.closeOverlayRedencion {
			position: absolute;
			top: 20px;
			right: 30px;
			height: 40px;
			width: 40px;
		}
		.btnOverlaySubmitRedencion {
			display: block;
			margin: 30px auto 0 auto;
			font-size: 14px !important;
			width: 100%;
		}
	</style>

	<div class="overlay-redencion" >
		<img class="logoOverlay" src="../../inc/imagenes/tablet/panel/logo_izlah_A0_Rectangle_18_pattern@2x.png">
		<img class="closeOverlayRedencion" src="../../inc/imagenes/tablet/closeBtn.png">
		<div class="containerOverlay">
			<h2 class="titleContainerOverlay"><span class="nameOverlay">Muestra esta pantalla <span class="colorOverlay">a tu cajero<span></h2>
			<p class="textOverlay">Captura en el POS cualquiera de los siguientes códigos</p>
			<div class="listCode">
				<img class="exampleCode" src="../../inc/imagenes/tablet/exampleCode.png">
			</div>
			<div class="formCheckAcumulacion">
				<button class="btn btn-primary btnOverlaySubmitRedencion">Aceptar</button>
			</div>
		</div>
	</div>
<script type="text/javascript">


	/*var idleTime = 0; 
	$(document).ready(function () { 
		//Increment the idle time counter every minute. 
		var idleInterval = setInterval(timerIncrement, 60000); 
		// 1 minute //Zero the idle timer on mouse movement. 
		$(this).mousemove(function (e) { idleTime = 0; console.log(idleTime); }); 
		$(this).keypress(function (e) { idleTime = 0; console.log(idleTime); }); 
	}); 

	function timerIncrement(){ 
		idleTime = idleTime + 1; 
		if (idleTime >= 1) { // 20 minutes 
			window.location = "login.php"; 
		} 
	};*/
	
	let colPerfil = $("#colPerfil").height();
	let colRedencion = $("#colRedencion").height();

	$("#formPassword").on("submit", function(e){
		e.preventDefault();
		var link = "../../controller/registro/c_llamadas_ajax.php";
        var data = $("#formPassword").serialize()+"&"+$("#formAcumular").serialize()+"&op=2";
        var beforeFunction = function(){
           $("#loadAnimation").show();
        };
        var successFunction = function(data){
            switch(data.result){
                case 1:
                	$('.containerOverlay').hide();
                   	$("#txtPuntos").text(data.puntos+" puntos");
                   	$(".containerOverlayMesaje").show();
                   	startConfetti();
					/*setTimeout(function(){ 
						$('.overlay-acumulacion').removeClass('open');
						$('#monto').val('');
						$('#ticket').val('');
					}, 10000);*/
                break;

                case 2:
                	$("#textError").text("Contraseña incorrecta, verifica que sea correcta e intenta nuevamente!");
                	$("#modalError").modal({
						backdrop: 'static',
						keyboard: false
					});
                break;

                case 3:
                   	$("#textError").text("El ticket ingresado ya se encuentra registrado, verifica que la información del ticket sea correcta e intenta nuevamente!");
                	$("#modalError").modal({
						backdrop: 'static',
						keyboard: false
					});
                break;

                default:
                	$("#textError").text("Hubo un error al registrar la acumulacion, intenta nuevamente. Si el problema persiste, contacta al administrador");
                	$("#modalError").modal({
						backdrop: 'static',
						keyboard: false
					});
                break;
            }
        }

        var failFunction = function(){
        	$("#textError").text("No se pudo procesar la solicitud. Verifica tu conexión a internet e intenta nuevamente!");
        	$("#modalError").modal({
				backdrop: 'static',
				keyboard: false
			});
        }

        $("#btnPassword").fadeOut(300, function(){
        	connectServer(link, data, beforeFunction, successFunction, failFunction);
        });
	});


	function connectServer(link, data, beforeFunction, successFunction, failFunction){
	    $.ajax({
	        type: "POST",
	        timeout: 8000,
	        url: link,
	        data: data,
	        dataType: "json",
	        beforeSend : function(){
	            beforeFunction && beforeFunction();
	        }
	    }).done(function(data){
	        console.log(data);
	        successFunction && successFunction(data);
	    }).fail(function(data){
	        failFunction && failFunction(data);
	        console.log(data);
	    });
	}

	$(document).ready(function(){
		
		$('#formAcumular').submit(function( event ) {
		  event.preventDefault();
		  var monto = $('#monto').val();
		  var ticket = $('#ticket').val();
		  $('.montoOverlay').empty();
		  $('.montoOverlay').append('$'+monto);
		  $('.overlay-acumulacion').addClass('open');
		  $("#tagTicket").append(ticket)
		  $('.containerOverlay').show();
		  $('.containerOverlayMesaje').hide();
		});

		$('.closeOverlay').click(function( event ) {
			$('.overlay-acumulacion').removeClass('open');
			$('#monto').val('');
			$('#ticket').val('');
			stopConfetti();
		});

		/*$('.btnOverlaySubmit').click(function( event ) {
		  $('.containerOverlay').hide();
		  $('.containerOverlayMesaje').show();
		  setTimeout(function(){ 
		  	$('.overlay-acumulacion').removeClass('open');
		  	$('#monto').val('');
		  	$('#ticket').val('');
		  }, 10000);

		});*/

		$('.openRedencion').click(function( event ) {
			event.preventDefault();
			$('.overlay-redencion').addClass('open');
			$('.overlay-redencion .containerOverlay').show();
		});	

		$('.closeOverlayRedencion').click(function( event ) {
		  $('.overlay-redencion').removeClass('open');
		});	
		$('.btnOverlaySubmitRedencion').click(function( event ) {
		  $('.overlay-redencion').removeClass('open');
		});		

	});	

</script>
<?php }} ?>
</body>
</html>