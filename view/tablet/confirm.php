<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta content="yes" name="mobile-web-app-capable">
<title>Confirmar Registro</title>
<style id="applicationStylesheet" type="text/css">
	body {
		margin: 0;
		padding: 0;
	}
	:root {
		--web-view-ids: Web_1280___1;
	}
	#Web_1280___1 * {
		margin: 0;
		padding: 0;
	}
	#Web_1280___1 {
		position: absolute;
		box-sizing: border-box;
		background: #E5E5E5;
		width: 1280px;
		height: 1084px;
		background-color: rgba(255,255,255,1);
		overflow: hidden;
		margin: 0;
		padding: 0;
		--web-view-name: Web 1280 – 1;
		--web-view-id: Web_1280___1;
		--web-enable-deep-linking: true;
	}
	#cat_chocolateas_hover {
		fill: url(#cat_chocolateas_hover_A19_Rectangle_2_pattern);
	}
	.cat_chocolateas_hover {
		position: absolute;
		overflow: visible;
		width: 1280px;
		height: 1281.5px;
		left: 0px;
		top: 80px;
	}
	#Rect_ngulo_245 {
		opacity: 0.5;
		fill: rgba(32,32,32,1);
	}
	.Rect_ngulo_245 {
		position: absolute;
		overflow: visible;
		width: 1280px;
		height: 1084px;
		left: 0px;
		top: 0px;
	}
	#Rect_ngulo_246 {
		opacity: 0.5;
		fill: rgba(32,32,32,1);
	}
	.Rect_ngulo_246 {
		position: absolute;
		overflow: visible;
		width: 698px;
		height: 1084px;
		left: 291px;
		top: 0px;
	}
	#Rect_ngulo_244 {
		fill: rgba(0,0,0,1);
	}
	.Rect_ngulo_244 {
		position: absolute;
		overflow: visible;
		width: 1280px;
		height: 80px;
		left: 0px;
		top: 0px;
	}
	#Rect_ngulo_250 {
		fill: rgba(0,0,0,1);
	}
	.Rect_ngulo_250 {
		position: absolute;
		overflow: visible;
		width: 698px;
		height: 146px;
		left: 291px;
		top: 274px;
	}
	#logo_izlah {
		fill: url(#logo_izlah_A19_Rectangle_8_pattern);
	}
	.logo_izlah {
		position: absolute;
		overflow: visible;
		width: 156px;
		height: 24px;
		left: 562px;
		top: 28px;
	}
	#Rect_ngulo_194 {
		fill: rgba(255,255,255,1);
		stroke: rgb(221, 221, 221);
		stroke-width: 1px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Rect_ngulo_194 {
		position: absolute;
		overflow: visible;
		width: 304.743px;
		height: 43.732px;
		left: 407px;
		top: 673.391px;
	}
	#Rect_ngulo_249 {
		fill: rgba(255,255,255,1);
		stroke: rgb(221, 221, 221);
		stroke-width: 1px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Rect_ngulo_249 {
		position: absolute;
		overflow: visible;
		width: 465.743px;
		height: 43.732px;
		left: 407px;
		top: 769.391px;
	}
	#Nombre_completo {
		position: absolute;
		left: 407px;
		top: 646.059px;
		overflow: visible;
		width: 127px;
		white-space: nowrap;
		text-align: left;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(255,255,255,1);
	}
	#Tel_fono {
		position: absolute;
		left: 407px;
		top: 454.059px;
		overflow: visible;
		width: 61px;
		white-space: nowrap;
		text-align: left;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(255,255,255,1);
	}
	#________56 {
		position: absolute;
		left: 407.453px;
		top: 486.059px;
		overflow: visible;
		width: 122px;
		white-space: nowrap;
		text-align: left;
		font-family: Open Sans;
		font-style: normal;
		font-weight: bold;
		font-size: 22px;
		color: rgba(255,255,255,1);
	}
	#Jairo_guantes_desclub_com_mx {
		position: absolute;
		left: 406px;
		top: 581.58px;
		overflow: visible;
		width: 346px;
		white-space: nowrap;
		text-align: left;
		font-family: Open Sans;
		font-style: normal;
		font-weight: bold;
		font-size: 22px;
		color: rgba(255,255,255,1);
	}
	#E_Mail {
		position: absolute;
		left: 407px;
		top: 550.059px;
		overflow: visible;
		width: 46px;
		white-space: nowrap;
		text-align: left;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(255,255,255,1);
	}
	#Ingresa_una_contrase_a {
		position: absolute;
		left: 407px;
		top: 742.059px;
		overflow: visible;
		width: 169px;
		white-space: nowrap;
		text-align: left;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(255,255,255,1);
	}
	#Bienvenido_Jairo__gracias_por_ {
		position: absolute;
		left: 399.743px;
		top: 316.808px;
		overflow: visible;
		width: 481px;
		white-space: nowrap;
		text-align: center;
		font-family: Oswald;
		font-style: normal;
		font-weight: bold;
		font-size: 20px;
		color: rgba(255,255,255,1);
	}
	#Jairo_Guantes_Ruiz {
		position: absolute;
		left: 420.12px;
		top: 686.51px;
		overflow: visible;
		width: 137px;
		white-space: nowrap;
		text-align: left;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(129,138,145,1);
	}
	#Contrase_a {
		position: absolute;
		left: 420.12px;
		top: 782.51px;
		overflow: visible;
		width: 84px;
		white-space: nowrap;
		text-align: left;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(129,138,145,1);
	}
	#Rect_ngulo_171 {
		fill: rgba(230,230,230,1);
		stroke: rgb(221, 221, 221);
		stroke-width: 1px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Rect_ngulo_171 {
		position: absolute;
		overflow: visible;
		width: 34.195px;
		height: 44.123px;
		left: 838.805px;
		top: 673px;
	}
	#Rect_ngulo_179 {
		fill: rgba(255,255,255,1);
		stroke: rgb(221, 221, 221);
		stroke-width: 1px;
		stroke-linejoin: miter;
		stroke-linecap: butt;
		stroke-miterlimit: 4;
		shape-rendering: auto;
	}
	.Rect_ngulo_179 {
		position: absolute;
		overflow: visible;
		width: 107.786px;
		height: 44.123px;
		left: 732.911px;
		top: 673px;
	}
	#G_nero {
		position: absolute;
		left: 742.838px;
		top: 686.237px;
		overflow: visible;
		width: 54px;
		white-space: nowrap;
		line-height: 24px;
		margin-top: -4px;
		text-align: left;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(55,58,60,1);
	}
	#S_mbolo_1___1 {
		position: absolute;
		width: 9.011px;
		height: 4.35px;
		left: 850.938px;
		top: 692.855px;
		overflow: visible;
	}
	#Trazado_1 {
		fill: rgba(55,58,60,1);
	}
	.Trazado_1 {
		transform: matrix(1,0,0,1,0,0);
		overflow: visible;
		position: absolute;
		top: 0px;
		left: 0px;
		width: 9.011px;
		height: 4.35px;
	}
	#Trazado_36 {
		fill: rgba(239,41,103,1);
	}
	.Trazado_36 {
		transform: matrix(1,0,0,1,0,0);
		overflow: visible;
		position: absolute;
		top: 893.268px;
		left: 519px;
		width: 243px;
		height: 43.732px;
	}
	#Completar_registro {
		position: absolute;
		left: 575px;
		top: 906.388px;
		overflow: visible;
		width: 133px;
		white-space: nowrap;
		line-height: 25px;
		margin-top: -4.5px;
		text-align: left;
		font-family: Helvetica Neue;
		font-style: normal;
		font-weight: normal;
		font-size: 16px;
		color: rgba(255,255,255,1);
	}
	#Hola_de_nuevo {
		position: absolute;
		left: 459.356px;
		top: 132.151px;
		overflow: visible;
		width: 363px;
		white-space: nowrap;
		text-align: center;
		font-family: Oswald;
		font-style: normal;
		font-weight: bold;
		font-size: 65px;
		color: rgba(255,255,255,1);
	}
	#Rect_ngulo_251 {
		fill: rgba(51,51,51,1);
	}
	.Rect_ngulo_251 {
		position: absolute;
		overflow: visible;
		width: 1280px;
		height: 91px;
		left: 0px;
		top: 993px;
	}
	#Loyalty_Solutions_2019__Consul {
		position: absolute;
		left: 521.982px;
		top: 1046.151px;
		overflow: visible;
		width: 239px;
		white-space: nowrap;
		text-align: center;
		font-family: Oswald;
		font-style: normal;
		font-weight: normal;
		font-size: 10px;
		color: rgba(255,255,255,1);
	}
	#Grupo_23 {
		position: absolute;
		width: 120.944px;
		height: 18.333px;
		left: 580.67px;
		top: 1016px;
		overflow: visible;
	}
	#Grupo_17 {
		position: absolute;
		width: 120.944px;
		height: 18.333px;
		left: 0px;
		top: 0px;
		overflow: visible;
	}
	#Grupo_16 {
		position: absolute;
		width: 120.944px;
		height: 18.333px;
		left: 0px;
		top: 0px;
		overflow: visible;
	}
</style>
</head>
<body>
<div id="Web_1280___1" >
	<svg class="cat_chocolateas_hover">
		<pattern elementId="cat_chocolateas_hover_A19_Rectangle_2" id="cat_chocolateas_hover_A19_Rectangle_2_pattern" x="0" y="0" width="100%" height="100%">
			<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/cat_chocolateas_hover_A19_Rectangle_2_pattern.png" xlink:href="../../inc/imagenes/tablet/cat_chocolateas_hover_A19_Rectangle_2_pattern.png"></image>
		</pattern>
		<rect id="cat_chocolateas_hover" rx="0" ry="0" x="0" y="0" width="1280" height="1281.5">
		</rect>
	</svg>
	<svg class="Rect_ngulo_245">
		<rect id="Rect_ngulo_245" rx="0" ry="0" x="0" y="0" width="1280" height="1084">
		</rect>
	</svg>
	<svg class="Rect_ngulo_246">
		<rect id="Rect_ngulo_246" rx="0" ry="0" x="0" y="0" width="698" height="1084">
		</rect>
	</svg>
	<svg class="Rect_ngulo_244">
		<rect id="Rect_ngulo_244" rx="0" ry="0" x="0" y="0" width="1280" height="80">
		</rect>
	</svg>
	<svg class="Rect_ngulo_250">
		<rect id="Rect_ngulo_250" rx="0" ry="0" x="0" y="0" width="698" height="146">
		</rect>
	</svg>
	<svg class="logo_izlah">
		<pattern elementId="logo_izlah_A19_Rectangle_8" id="logo_izlah_A19_Rectangle_8_pattern" x="0" y="0" width="100%" height="100%">
			<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/logo_izlah_A19_Rectangle_8_pattern.png" xlink:href="../../inc/imagenes/tablet/logo_izlah_A19_Rectangle_8_pattern.png"></image>
		</pattern>
		<rect id="logo_izlah" rx="0" ry="0" x="0" y="0" width="156" height="24">
		</rect>
	</svg>
	<svg class="Rect_ngulo_194">
		<rect id="Rect_ngulo_194" rx="4" ry="4" x="0" y="0" width="304.743" height="43.732">
		</rect>
	</svg>
	<svg class="Rect_ngulo_249">
		<rect id="Rect_ngulo_249" rx="4" ry="4" x="0" y="0" width="465.743" height="43.732">
		</rect>
	</svg>
	<div id="Nombre_completo">
		<span>Nombre completo</span>
	</div>
	<div id="Tel_fono">
		<span>Teléfono</span>
	</div>
	<div id="________56">
		<span>********56</span>
	</div>
	<div id="Jairo_guantes_desclub_com_mx">
		<span>Jairo.guantes@desclub.com.mx</span>
	</div>
	<div id="E_Mail">
		<span>E-Mail</span>
	</div>
	<div id="Ingresa_una_contrase_a">
		<span>Ingresa una contraseña</span>
	</div>
	<div id="Bienvenido_Jairo__gracias_por_">
		<span>Bienvenido Jairo, gracias por ser parte de nuestra familia, <br/>por favor completa tu registro con los siguientes datos.</span>
	</div>
	<div id="Jairo_Guantes_Ruiz">
		<span>Jairo Guantes Ruiz</span>
	</div>
	<div id="Contrase_a">
		<span>Contraseña</span>
	</div>
	<svg class="Rect_ngulo_171">
		<rect id="Rect_ngulo_171" rx="0" ry="0" x="0" y="0" width="34.195" height="44.123">
		</rect>
	</svg>
	<svg class="Rect_ngulo_179">
		<rect id="Rect_ngulo_179" rx="4" ry="4" x="0" y="0" width="107.786" height="44.123">
		</rect>
	</svg>
	<div id="G_nero">
		<span>Género</span>
	</div>
	<div id="S_mbolo_1___1">
		<svg class="Trazado_1" viewBox="3106.946 2498 9.01 4.35">
			<path id="Trazado_1" d="M 3106.9462890625 2497.999755859375 C 3106.9462890625 2497.999755859375 3111.82275390625 2497.999755859375 3111.82275390625 2497.999755859375 C 3111.82275390625 2497.999755859375 3115.956787109375 2497.999755859375 3115.956787109375 2497.999755859375 C 3115.956787109375 2497.999755859375 3111.24365234375 2502.349365234375 3111.24365234375 2502.349365234375 C 3111.24365234375 2502.349365234375 3106.9462890625 2497.999755859375 3106.9462890625 2497.999755859375 Z">
			</path>
		</svg>
	</div>
	<svg class="Trazado_36" viewBox="0 0 243 43.732">
		<path id="Trazado_36" d="M 4 0 L 239 0 C 241.2091369628906 0 243 1.790860891342163 243 4 L 243 39.73177719116211 C 243 41.94091796875 241.2091369628906 43.73177719116211 239 43.73177719116211 L 4 43.73177719116211 C 1.790860891342163 43.73177719116211 0 41.94091796875 0 39.73177719116211 L 0 4 C 0 1.790860891342163 1.790860891342163 0 4 0 Z">
		</path>
	</svg>
	<div id="Completar_registro">
		<span>Completar registro</span>
	</div>
	<div id="Hola_de_nuevo">
		<span>Hola de nuevo</span>
	</div>
	<svg class="Rect_ngulo_251">
		<rect id="Rect_ngulo_251" rx="0" ry="0" x="0" y="0" width="1280" height="91">
		</rect>
	</svg>
	<div id="Loyalty_Solutions_2019__Consul">
		<span>Loyalty Solutions 2019. Consulta nuestro aviso de privacidad aquí.</span>
	</div>
	<div id="Grupo_23">
		<div id="Grupo_17">
			<div id="Grupo_16">
			<svg style="width:inherit;height:inherit;overflow:visible;">
				<rect fill="url(#Grupo_16_A19_Group_8_pattern)" width="100%" height="100%"></rect>
					<pattern elementId="Grupo_16_A19_Group_8" id="Grupo_16_A19_Group_8_pattern" x="0" y="0" width="100%" height="100%">
						<image x="0" y="0" width="100%" height="100%" href="../../inc/imagenes/tablet/Grupo_16_A19_Group_8_pattern.png" xlink:href="../../inc/imagenes/tablet/Grupo_16_A19_Group_8_pattern.png"></image>
					</pattern>
					</svg>
				</div>
		</div>
	</div>
</div>
</body>
</html>