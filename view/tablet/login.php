<?php 
	
	include '../../inc/funciones.php';
	include '../../inc/parametros.php';
	//$inst_control=new C_home($ser,$usu,$pas,$bd);	
	$ins_funciones=new Funciones_Basicas();
	/*session_start();
	if(isset($_SESSION["usuario"])){ 
	    $catalogue = 1;
	    //echo "<script>console.log('".$catalogue." estas logueado');</script>";
	}else{*/
    $catalogue = 0;

      //TRAEMOS CONFIGURACION DE ESTILOS
   $res_con1=$ins_funciones->consulta_generica('tbl_estilo', ' ');
   $registro1= mysqli_fetch_assoc($res_con1);

   $res_con2=$ins_funciones->consulta_generica('tbl_configuracion_proyecto', ' ');
   $config = mysqli_fetch_assoc($res_con2);
   
   //VALIDAMOS LOGO
  if($registro1['logo_cte']==''){
      $img_logo1='imagenes/logo.png';
  }else{
      $img_logo1='imagenes/img_configuracion/'.$registro1['logo_cte'];
  }

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta content="yes" name="mobile-web-app-capable">
	<link rel="icon" sizes="192x192" href="../../inc/<?php echo $img_logo1 ?>">
	<title>:: <?php echo $config['titulo_home'] ?> :: <?php echo $config['detalle_home'] ?> </title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<link href="https://fonts.googleapis.com/css?family=Oswald:300,400&display=swap" rel="stylesheet">
	<script src="https://kit.fontawesome.com/4742d4c7f3.js"></script>	
	<style type="text/css">
		*{
			font-family: Oswald, sans-serif;
		}

		.spanText{
			font-family: Helvetica;
			font-size:20pt; 
			font-weight: 500; 
			color:white;
		}

		body{
			/*background:url('../../inc/imagenes/tablet/cat_chocolateas_hover_A19_Rectangle_2_pattern@2x.png') no-repeat;*/
			background: url('../../inc/imagenes/tablet/login/rectangulo252.png') no-repeat;
			background-size: 100% auto;
			background-position: bottom;
			
		}

		#back-top{
			background:url('../../inc/imagenes/tablet/login/rectangulo_253.png') no-repeat;
			background-size: cover;
			background-position: top center;
			
			-webkit-box-shadow: 0px 7px 8px rgba(0,0,0,0.5);
			box-shadow: 0px 7px 8px rgba(0,0,0,0.5);
			-o-box-shadow: 0px 7px 8px rgba(0,0,0,0.5);
			-moz-box-shadow: 0px 7px 8px rgba(0,0,0,0.5);
		}

		.btn-primary{
			background-color: #EF2967;
			-webkit-box-shadow: 4px 5px 20px 0px rgba(62, 61, 61, 0.85);
			box-shadow: 4px 5px 20px 0px rgba(62, 61, 61, 0.85);
			-o-box-shadow: 4px 5px 20px 0px rgba(62, 61, 61, 0.85);
			-moz-box-shadow: 4px 5px 20px 0px rgba(62, 61, 61, 0.85);
			border-color: #EF2967 !important;			
			padding: 8px;
			font-family: Helvetica;
		}

		.btn-primary:hover{
			color: #EF2967;			
			background-color: white;
			-webkit-box-shadow: 4px 5px 20px 0px #ef2967;
			box-shadow: 4px 5px 20px 0px #ef2967;
			-o-box-shadow: 4px 5px 20px 0px #ef2967;
			-moz-box-shadow: 4px 5px 20px 0px #ef2967;
			
		}

		.btn-primary:active{
			color: #EF2967;			
			background-color: white;
			-webkit-box-shadow: 4px 5px 20px 0px #ef2967;
			box-shadow: 4px 5px 20px 0px #ef2967;
			-o-box-shadow: 4px 5px 20px 0px #ef2967;
			-moz-box-shadow: 4px 5px 20px 0px #ef2967;
		}

		input{			
			border: transparent;
			padding-left: 5px;

		}

		.lds-dual-ring {
	        display: inline-block;
	        width: 64px;
	        height: 64px;
	    }
	    .lds-dual-ring:after {
	        content: " ";
	        display: block;
	        width: 46px;
	        height: 46px;
	        margin: 1px;
	        border-radius: 50%;
	        border: 5px solid #fff;
	        border-color: #EF2967 transparent #EF2967 transparent;
	        animation: lds-dual-ring 1.2s linear infinite;
	    }
	    @keyframes lds-dual-ring {
	        0% {
	            transform: rotate(0deg);
	        }
	        100% {
	            transform: rotate(360deg);
	        }
	    }

	    .input-group-text{
	    	background-color: #EF2967;
	    	color: white;
	    	border-color: #EF2967;
	    }

	    input{
	    	width: auto;
	    	font-family: Helvetica;
	    	/*font-size: 19pt;*/
	    	font-weight: bold;
	    	height: 35pt !important;
	    }

	    select{
	    	width: auto;
	    	font-family: Helvetica;
	    	font-size: 21pt;
	    	height: 35pt !important;
	    }

	    .backModal{
	    	background: url('../../inc/imagenes/tablet/scanQR/QR_Background_A2_Rectangle_2_pattern.png') no-repeat; 
	    	background-size: cover; 
	    	background-position: top center;
	    }
	</style>	
</head>
<body>
	<div class="modal fade" id="modalRegistro" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-center" id="exampleModalLongTitle">Registro correcto</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  		<span aria-hidden="true">&times;</span>
					</button>
				</div>
			  	<div class="modal-body" style="padding:20px">
			    	<center>¡Felicidades, tu registro se ha completado correctamente!<br>
			    	Ya puedes empezar a ganar puntos.</center>
			  	</div>
			  	<div class="modal-footer">			    	
			    	<button type="button" class="btn btn-primary" data-dismiss="modal" style="padding:10px 30px">Ok</button>
			  	</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalError" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header" style="background-color:#EF2967;">
					<h5 class="modal-title text-center" id="exampleModalLongTitle">Error</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				  		<span aria-hidden="true">&times;</span>
					</button>
				</div>
			  	<div class="modal-body" style="padding:20px">
			    	<center id="textError"></center>
			  	</div>
			  	<div class="modal-footer">			    	
			    	<button type="button" class="btn btn-primary" data-dismiss="modal" style="padding:10px 30px">Aceptar</button>
			  	</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modalScan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content backModal">
				
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
							<img src="../../inc/imagenes/tablet/scanQR/logo_izlah_A2_Rectangle_13_pattern.png" />
							<div class="pull-right">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  		<span aria-hidden="true" style="font-size:2.25em; color:red">&times;</span>
								</button>
							</div>
						</div>
					</div>	
				
			  	<div class="modal-body" style="padding:20px">
			    	
			  	</div>
			  	<div class="modal-footer" style="border-top: none">			    	
			    	<button type="button" class="btn btn-primary" data-dismiss="modal" style="padding:10px 30px">Cancelar</button>
			  	</div>
			</div>
		</div>
	</div>
	<div id="mainContainer" class="container-fluid">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="back-top" >
				<form id="formVerifyUser">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left">
							<img src="../../inc/imagenes/tablet/login/loyalty_20.png"/>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
							<br>
							<img class="imgFix" src="../../inc/imagenes/tablet/login/logo-izlah@2x.png"/>
						</div>
					</div>					
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
							<br>							
							<span id="textResponse" style="font-size:34pt; font-weight: 300; color:white;" class="text-center">
								<?php 
									//if(isset($config['puntosInicio']) && $config['puntosInicio'] == 1 ){ 
								?>
									
									¡Felicidades, ganaste 200 puntos!
								<?php //} ?>
							</span>
						</div>
					</div>
					<div class="row justify-content-center">
						<div class="col-lg-9 col-md-10 col-sm-12 col-xs-12 text-center">
							<div class="row">								
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<br>
									<span class="text-center spanText">Regálanos algunos datos para enviarte los puntos</span>
								</div>								
							</div>
						</div>
					</div>
					<div class="row justify-content-center">
						<div class="col-lg-8 col-md-8 col-sm-9 col-xs-9 text-center">
							<div class="row justify-content-center">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
									<br>
									<div class="input-group">
										<div class="input-group-prepend">
											<i class="input-group-text fa fa-mobile-alt" style="padding:14px"></i>
										</div>
										<input id="tel" type="tel" class="form-control" aria-describedby="basic-addon1" name="telefono" maxlength="10" minlength="10" required autocomplete="off" placeholder="Celular a 10 dígitos" pattern="^[0-9]{10}">
									</div>
								</div>
								<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 text-center">
									<br>
									<span class="spanText"> ó </span>
								</div>
								<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
									<br>
									<a id="btnQR" class="btn btn-primary" href="javascript:;" style="width: 100%"> Escanear QR </a>
								</div>
							</div>
						</div>
					</div>
					<div class="row justify-content-center" id="rowEmail">
						<div class="col-lg-8 col-md-8 col-sm-9 col-xs-9">
							<div class="row justify-content-center">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
									<br>
									<label class="spanText" for="email">Por último ingresa tu correo electrónico</label><br>
								</div>
							</div>
							<div class="row justify-content-center">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="input-group">
										<input type="text" class="form-control" name="email" id="email" disabled required autocomplete="off" pattern="^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+" placeholder="usuario.izlah" />
										<div id="divSelect" class="input-group-append">
											<span class="input-group-text">@</span>											
											<select id="domainEmailSelect" name="domainEmail" class="input-group-text" disabled>
												<option value="1">hotmail.com</option>
												<option value="2">gmail.com</option>
												<option value="3">outlook.com</option>
												<option value="5">icloud.com</option>
												<option value="4"> -- Otro -- </option>
											</select>											
										</div>
										<div id="divDomain" class="input-group-append">
											<span class="input-group-text">@</span>
											<div class="input-group">
												<input id="inputDomainText" type="text" name="domainEmailOther" autocomplete="off" placeholder="example.com" pattern="[a-z0-9.-]+\.[a-z]{2,4}$">
												<div class="input-group-append"><span id="btnCancel" class="input-group-text">Cancelar</span></div>
											</div>											
										</div>										
									</div>									
								</div>
							</div>
						</div>
					</div>
					<div class="row justify-content-center">
						<div class="col-lg-5 col-md-6 col-sm-8 col-xs-8 text-center" style="margin-bottom:25px;">
							<br>
							<button id="btnSubmitVerify" class="btn btn-primary" type="submit" style="width:100%"> Continuar </button>
						</div>
					</div>
					<div id="loadAnimation" style="display:none; width:100%; text-align:center"><div class="lds-dual-ring"></div></div>
				</form>
			</div>
			
		</div>
	</div>
<script src="../../inc/js/jquery_redirect.js"></script>
<script type="text/javascript">	
	$("#rowEmail").hide();
	$("#divDomain").hide();

	$("#domainEmailSelect").change(function(){
		var value = $(this).val();
		if( value == 4){
			$("#divSelect").fadeOut(300, function(){
				$("#divDomain").fadeIn();
				$("#inputDomainText").removeAttr("required");
			});			
		}
	});

	$("#btnCancel").click(function(){		
		$("#divDomain").fadeOut(300, function(){
			$("#divSelect").fadeIn();
			$("#domainEmailSelect").val(1);
			$("#inputDomainText").attr("required", true);
		});
	});

	$("#tel").on("change",function(){
		$("#rowEmail").fadeOut(300);
		$("#email").attr("disabled", true);
	});
	
	let heightTop = $("#back-top").height();
	function initScreen(){				
		$("#mainContainer").height($(window).height());
		
		var windowHeight = $(window).height();

		var defaultSize = $(window).height()-115;
		if(defaultSize < heightTop )
			$("#back-top").css("height", "auto");
		else
			$("#back-top").css("height",defaultSize);

		
	}

	$(window).on("resize", function(){		
		initScreen();
	});

	$("#btnQR").click(function(){
		$("#modalScan").modal({
			backdrop: 'static',
			keyboard: false
		});
		//$("#modalScan").modal("show");
	});

	$("#formVerifyUser").on("submit", function(e){
		e.preventDefault();
		var link = "../../controller/registro/c_llamadas_ajax.php";
		var data = $("#formVerifyUser").serialize()+"&op=7";
		var beforeFunction = function(){			
			$("#loadAnimation").fadeIn();
		};
		var successFunction = function(response){
			if(response.result){
				switch(response.result){
					case 1:
						$("#loadAnimation").fadeOut(300, function(){
							$.redirect("home.php", {tel: $("#tel").val()}, "POST"); 
						});
					break;

					case 2:
						$("#loadAnimation").fadeOut(300, function(){
							$("#textResponse").text("¿Eres usuario nuevo?");
							$("#rowEmail").fadeIn(300);
							$("#btnSubmitVerify").fadeIn(300);
							$("#email").removeAttr("disabled");
							$("#domainEmailSelect").removeAttr("disabled");
							
						});
					break;

					case 3:
						$("#loadAnimation").fadeOut(300, function(){
							//$("#modalRegistro").modal("show");
							$("#modalRegistro").modal({
								backdrop: 'static',
								keyboard: false
							});
							/*var data = {'telefono' : $("#tel").val(), 'op' : 7};
							connectServer(link, data, beforeFunction, successFunction, failFunction);*/
							setTimeout(function(){
								callbackFunction(function(){
									$("#modalRegistro").modal("hide");
								}, function(){
									console.log("start");
									$.redirect("home.php", {tel: $("#tel").val()}, "POST"); 
								});
							}, 4000);
						});
					break;

					case 4:
						$("#loadAnimation").fadeOut(300, function(){
							$("#textError").html("Hubo un error al realizar el registro en la plataforma.<br>Intenta nuevamente");
							$("#modalError").modal({
								backdrop: 'static',
								keyboard: false
							});
							//$("#modalError").modal("show");
							$("#btnSubmitVerify").fadeIn();
						});
					break;

					case 5:
						$("#loadAnimation").fadeOut(300, function(){
							$("#textError").html("El correo electrónico ingresado ya existe en la plataforma.<br>Ingresa otro e intenta nuevamente.");
							$("#modalError").modal({
								backdrop: 'static',
								keyboard: false
							});
							//$("#modalError").modal("show");
							$("#btnSubmitVerify").fadeIn();
						});
					break;
				}
			}
		};
		var failFunction = function(data){

		};

		function callbackFunction(callback1, callback2){
			callback1 && callback1();
			callback2 && callback2();
		}
		$("#btnSubmitVerify").fadeOut(300, function(){
			connectServer(link, data, beforeFunction, successFunction, failFunction);
		});
	});

	function connectServer(link, data, beforeFunction, successFunction, failFunction){
	    $.ajax({
	        type: "POST",
	        timeout: 8000,
	        url: link,
	        data: data,
	        dataType: "json",
	        beforeSend : function(){
	            beforeFunction && beforeFunction();
	        }
	    }).done(function(data){
	        console.log(data);
	        successFunction && successFunction(data);
	    }).fail(function(data){
	        failFunction && failFunction(data);
	        console.log(data);
	    });
	}

	initScreen();

</script>
</body>
</html>