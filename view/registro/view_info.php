<?php    
    $img_logo = $_POST['logo'];
    $tel = isset($_POST['tel']) ? $_POST['tel'] : NULL;
    $mail = isset($_POST['mail']) ? $_POST['mail'] : NULL;
    $id = isset($_POST['id']) ? $_POST['id'] : NULL;
    $qr = isset($_POST['idUsuario']) ? $_POST['idUsuario'] : NULL;

    //print_r($_POST);
    //die();

    if($tel != NULL || $mail != NULL || $id != NULL || $qr != NULL){
        include('../../inc/parametros.php');
        require("../../controller/registro/c_registro.php");
        require('../../inc/funciones.php');

        $ins_fun_basicas = new Funciones_Basicas();
        $configuracion=$ins_fun_basicas->consulta_generica('tbl_configuracion_proyecto', '');
        $configuracion = mysqli_fetch_assoc($configuracion);
        $inst = new C_registro($ser,$usu,$pas,$bd);
        $usuario = $inst->get_usuario($qr, $tel, $mail, $id);

        //print_r($usuario);
        //die();
        if(count($usuario) > 0){
            $redenciones = $inst->get_productos_promociones($usuario[0]['idUsuario']);

?>
<!-- Central Modal Medium Success -->
<div class="modal fade margin-fix" id="modalPush" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-notify modal-success" role="document">
        <!--Content-->
        <div class="modal-content">
            <form id="formPasswd">
                <!--Header-->
                <div class="modal-header primary-back">
                    <p class="heading lead second-color">Confirmar Operación por el Punto de Venta</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">&times;</span>
                    </button>
                </div>
                <!--Body-->
                <div class="modal-body divHide" id="bodyModalPasswd"> 
                    <?php if($configuracion['tipo_confirm_operacion'] == 1){ ?>               
                    <div class="text-center">
                        <i class="fas fa-check fa-4x mb-3 animated rotateIn"></i>
                        <p>Ingresa la contraseña para confirmar la operación:</p>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input type="password" name="password" style="width:100%" required/>
                        </div>
                    </div>
                    <?php }else{ ?>
                    <div class="row" id="rowPasswd" >                            
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="text-center">                                    
                                <p>Muestre su tarjeta de identificacion para continuar:</p>
                                <div style="padding:5%">
                                    <canvas id="canvas" hidden style="width:100%; border-radius: 10px" ></canvas>
                                    <p id="loadingMessage" style="color:white; text-align:center"> No se detectó alguna cámara activa </p>                            
                                    <div id="output">
                                        <p id="outputMessage" style=" text-align:center; color:white">No se ha detectado ningún código QR.</p>
                                    </div>
                                </div>
                                <div class="input-group">
                                    <!--span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span-->
                                    <input id="inputPasswd" type="hidden" name="password" style="width:100%;" required/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div id="divLoad">
                    <div class="text-center">
                        <div class="load">
                            <div class="preloader">
                                <div class="spinner-layer pl-red">
                                    <div class="circle-clipper left">
                                        <div class="circle"></div>
                                    </div>
                                    <div class="circle-clipper right">
                                        <div class="circle"></div>
                                    </div>
                                </div>
                            </div>
                            <p class="text-center">Procesando...</p>
                        </div>
                    </div>
                </div>
                <!--Footer-->
                <div class="modal-footer justify-content-center divHide">
                    <button type="submit" id="btnYes" data-action="1" class="btn btn_color"><i class="glyphicon glyphicon-ok"></i> Confirmar</button>
                    <button type="button" class="btn btn_color btn-outline-success waves-effect" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Cancelar</button>
                </div>
            </form>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- Central Modal Medium Success-->

<!-- Central Modal Medium Success -->
<div class="modal fade margin-fix" id="modalPromocion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-notify modal-success" role="document">
        <!--Content-->
        <div class="modal-content">
            <form id="formPromocion">
                <!--Header-->
                <div class="modal-header primary-back">
                    <p class="heading lead">Aplicar Promoción</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="white-text">&times;</span>
                    </button>
                </div>
                <!--Body-->
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row"><div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center"><i class="fas fa-check fa-4x mb-3 animated rotateIn"></i></div></div>
                        <div class="row"><div id="bodyModalPromocion" class="col-lg-12 col-md-12 col-sm-12 col-xs-12"></div></div>
                        <?php if($configuracion['tipo_confirm_operacion'] == 1){ ?>
                        <div class="row" id="rowPasswd" >                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="text-center">                                    
                                    <p>Ingresa la contraseña para continuar:</p>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                        <input type="password" name="password" style="width:100%;" required/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php }else{ ?>
                        <div class="row" id="rowPasswd" >                            
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="text-center">                                    
                                    <p>Muestre su tarjeta de identificacion para continuar:</p>
                                    <div style="padding:5%">
                                        <canvas id="canvas" hidden style="width:100%; border-radius: 10px" ></canvas>
                                        <p id="loadingMessage" style="color:white; text-align:center"> No se detectó alguna cámara activa </p>                            
                                        <div id="output">
                                            <p id="outputMessage" style=" text-align:center; color:white">No se ha detectado ningún código QR.</p>
                                        </div>
                                    </div>
                                    <div class="input-group">
                                        <!--span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span-->
                                        <input id="inputPasswdPromo" type="hidden" name="password" style="width:100%;" required/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <div id="divLoad1">
                            <div class="text-center">
                                <div class="load">
                                    <div class="preloader">
                                        <div class="spinner-layer pl-red">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="text-center">Procesando...</p>
                                </div>
                            </div>
                        </div>
                    </div>                
                </div>
                <!--Footer-->
                <div class="modal-footer justify-content-center">
                    <input id="inputIDPromo" type="hidden" name="idPromo" />
                    <input id="inputTipoPromo" type="hidden" name="tipoPromo" />
                    <input id="inputIDUser" type="hidden" name="idUser" value="<?php echo $usuario[0]['idUsuario'] ?>" />
                    <button type="submit" id="btnYes1" data-action="1" class="btn btn_color"><i class="glyphicon glyphicon-ok"></i> Confirmar</button>
                    <button type="button" class="btn btn_color btn-outline-success waves-effect" data-dismiss="modal"><i class="glyphicon glyphicon-remove"></i> Cancelar</button>
                </div>
            </form>
        </div>
        <!--/.Content-->
    </div>
</div>
<!-- Central Modal Medium Success-->
<!-- Central Modal Medium Success-->
<script type="text/javascript">

    $(window).on("resize", function(){
        resizeWindow();
    });

    let heightAcumulacion = $("#contentAcumulacion").height();
    let heightRedencion = $("#contentRedencion").height();
   
    var resizeWindow = function(){
        var windowHeight = $(window).height();
        var heightHeader = $("#headView").height();
        
        var heightHeaderAcumulacion = $("#headerAcumulacion").height();
        var heightHeaderRedencion = $("#headerRedencion").height();
        var contentAcumulacion = $("#contentAcumulacion");
        var contentRedencion = $("#contentRedencion");
        var rowAcumulacion = $("#rowAcumulacion");
        var rowRedencion = $("#rowRedencion");

        var result = windowHeight - heightHeader;

        
        contentAcumulacion.height(heightAcumulacion);
        contentRedencion.height(heightRedencion);

        rowAcumulacion.height("auto");
        rowRedencion.height("auto");

        if( (heightHeader + rowAcumulacion.height()) <= windowHeight && (heightHeader + rowRedencion.height()) <= windowHeight){
            rowAcumulacion.css("height", result);
            rowRedencion.css("height", result);
            contentAcumulacion.css("height", result - heightHeaderAcumulacion);
            contentRedencion.css("height", result - heightHeaderRedencion);
            
        }else{

            
            if( contentAcumulacion.height() < result - heightHeaderRedencion){
                rowAcumulacion.css("height", result);
                rowRedencion.css("height", result);
                contentAcumulacion.css("height", result - heightHeaderRedencion);
                contentRedencion.css("height", result - heightHeaderRedencion);
                $("#listRedencion").css("max-height",(result - heightHeaderRedencion)/ 2);
                $("#listPromocion").css("max-height",(result - heightHeaderRedencion)/2 - $("#tagPromocion").height());
                console.log("IF");
            }else{             
                
                contentRedencion.css("height", contentAcumulacion.height());
                $("#listRedencion").css("max-height",(contentAcumulacion.height())/ 2);
                $("#listPromocion").css("max-height",(contentAcumulacion.height()/ 2) - $("#tagPromocion").height());
                console.log("Else");
            }
        }
    }

    resizeWindow();

</script>
<script src="../../inc/plugins/jquery/jquery.min.js"></script>
<script src="../../inc/plugins/bootstrap/js/bootstrap.js"></script>
<div class="fullcard" >
    <div id="headView" class="row align-self-center">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="media">
                <div class="media-left align-self-center">
                    <a href="registrate2.php">
                        <img src="../../inc/imagenes/back_icon.svg" style="width:35px" />
                    </a>
                    <!--h1 style="color:white; font-size:2em"><i class="material-icons">keyboard_arrow_left</i></h1-->
                </div>
                <div class="media-body align-self-center">                    
                    <h4 class="text-right" style="color:red"> <?php echo $usuario[0]['nombreUsuario'] ?></h4>                    
                    <h4 class="text-right text_color" style="color:white !important" ><?php echo $configuracion['id_tipo_proyecto'] == 1 || $configuracion['id_tipo_proyecto'] == 2 ? $usuario[0]['puntos_disponibles'][0]['puntos_totales']." Pts." : $usuario[0]['puntos_disponibles'][0]['visitas_totales']." Visitas" ?></h4>
                    <?php if($configuracion['niveles'] == 1){ ?><h4 class="text-right text_color" style="color:white !important" ><?php echo isset($usuario[0]['nombreNivel']) ? "Nivel: ".$usuario[0]['nombreNivel'] : "" ?></h4><?php } ?>
                </div>
                <div class="media-right align-self-center">                    
                    <img class="media-object" src="<?php echo  $usuario[0]['foto'] != null && file_exists("../../inc/imagenes/img_usuarios/".$usuario[0]['foto']) ? '../../inc/imagenes/img_usuarios/'.$usuario[0]['foto'] : ( $usuario[0]['genero'] == 1 ? '../../inc/imagenes/user.png' : ( $usuario[0]['genero'] == 2 ? '../../inc/imagenes/avatar2.png' : '../../inc/imagenes/user.png' ) );  ?>" style="margin:0 5px; height:70px">
                </div>
            </div>            
        </div>
    </div>
    <div class="row margin-fix">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="rowAcumulacion">
            <div class="row" id="headerAcumulacion">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 primary-back text-center" style=" border:1px solid gray;">
                    <h3 style="color:white">Acumular Puntos</h3>
                </div>
            </div>
            <div class="row" id="contentAcumulacion">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="border-right: 1px solid gray; border-left: 1px solid gray; border-bottom: 1px solid gray; background-color: white;">
                    <form id="formAcumulacion" style="padding:10px; margin-bottom:20px">
                        <div class="row justify-content-center" style="margin-top:20px">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 text-center" >
                                <img id="logo" data-name="<?php echo $img_logo ?>" src="../../inc/<?php echo $img_logo ?>" style="max-width:150px"/>
                            </div>
                        </div>
                        <div class="row justify-content-center" style="margin-top:20px">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <h5 class="text_color">Ingresa el Monto</h5>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-usd"></i></span>
                                    <input type="number" min="1" name="monto" step="0.01" required style="width:100%; height:50px"/>
                                </div>
                            </div>
                        </div>
                        <div class="row justify-content-center" style="margin-top:15px">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                <h5 class="text_color">Ingresa el ID del Ticket</h5>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-barcode"></i></span>                                    
                                    <div class="form-line"><input type="text" name="ticket" required style="width:100%; height:50px" /></div>                                    
                                    <input type="hidden" name="user" required value="<?php echo $usuario[0]['idUsuario']?>"/>
                                </div>                                
                            </div>
                        </div>
                        <div class="row justify-content-center" style="margin-top:20px">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <button type="submit" class="btn btn_color btn-block" style="border-radius:10px">Enviar acumulación</button>                                
                            </div>
                        </div>                        
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" id="rowRedencion">
            <div class="row" id="headerRedencion">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="border: 1px solid gray; background-color: gray">
                    <h3 style="color:white">Usar Puntos</h3>
                </div>
            </div>
            <div class="row" id="contentRedencion">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="background-color: #c5c5c5;">                    
                    <div class="row">                        
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="listRedencion" style="overflow-y: scroll; background-color: #f3f3f3;">
                            <?php if ( isset($redenciones['productos']) && count($redenciones['productos']) > 0 ) { ?>
                                <?php foreach ($redenciones['productos'] as $key) { ?>
                                <div class="row" style="border-bottom: 1px solid gray">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rowMedia <?php if( ( ($configuracion['id_tipo_proyecto'] == 1 || $configuracion['id_tipo_proyecto'] == 2) && $key['valor_puntos'] <= $usuario[0]['puntos_disponibles'][0]['puntos_totales'] ) || ($configuracion['id_tipo_proyecto'] == 3 && $key['valor_visitas'] <= $usuario[0]['puntos_disponibles'][0]['visitas_totales']) ){ ?> btnRedencion<?php } ?>" data-id-user="<?php echo $usuario[0]['idUsuario'] ?>" data-id-redencion="<?php echo $key['id_cat_premios_productos_servicios'] ?>">
                                        <div class="row">
                                            <!--div class="media-left">
                                                <img class="media-object" src="<?php echo file_exists("../../inc/imagenes/img_catalogo/".$key['imagen']) ? '../../inc/imagenes/img_catalogo/'.$key['imagen'] : '../../inc/imagenes/img_catalogo/no_img.svg' ?>" width="64" height="64" style="margin-right:5px">
                                            </div-->
                                            <div class="col-lg-10 col-md-10 col-sm-9 col-xs-9 align-self-center">
                                                <h3 class="text_secondary"><?php echo mb_strtoupper($key['nombre'],'utf-8') ?></h3>
                                                <p style="font-weight:900; font-size:1.6em; text-align:justify"><?php echo $key['detalle'] ?></p>
                                                <?php if( ( ($configuracion['id_tipo_proyecto'] == 1 || $configuracion['id_tipo_proyecto'] == 2) && $key['valor_puntos'] <= $usuario[0]['puntos_disponibles'][0]['puntos_totales'] ) || ($configuracion['id_tipo_proyecto'] == 3 && $key['valor_visitas'] <= $usuario[0]['puntos_disponibles'][0]['visitas_totales']) ){ ?><h5 class="text_color"><b>(<?php echo $key['restantes'] ?> Restantes)</b></h5><?php }else{ ?><h5 class="text_color"><b><u>No dispones de <?php echo ($configuracion['id_tipo_proyecto'] == 1 || $configuracion['id_tipo_proyecto'] == 2 ? 'puntos' : 'visitas'); ?> suficientes para redimir este producto. </u></b></h5><?php } ?>
                                                <br>
                                            </div>
                                            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 align-self-center">                                                
                                                <a class="btn btn-block btn_color" style="width:100%; border-radius:8px; padding:8px; <?php if( !( ( ($configuracion['id_tipo_proyecto'] == 1 || $configuracion['id_tipo_proyecto'] == 2) && $key['valor_puntos'] <= $usuario[0]['puntos_disponibles'][0]['puntos_totales'] ) || ($configuracion['id_tipo_proyecto'] == 3 && $key['valor_visitas'] <= $usuario[0]['puntos_disponibles'][0]['visitas_totales']) ) ){ ?> background-color: gray !important; color: white !important;  <?php } ?> " href="javascript:;"><b><?php echo $configuracion['id_tipo_proyecto'] == 1 || $configuracion['id_tipo_proyecto'] == 2 ? $key['valor_puntos']."<br>puntos" : $key['valor_visitas']."<br>visitas." ?></b></a>
                                            </div>                                            
                                        </div>

                                    </div>
                                </div>
                                <?php } ?>
                            <?php }else{ ?> 
                            <div class="row" style="border-bottom: 1px solid gray">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rowMedia">
                                    <div class="media">                            
                                        <div class="media-body">
                                            <h4 style="color:black">Sin Redenciones</h4>
                                            <h5 style="color:black"><u>No hay redenciones disponibles</u></h5>
                                        </div>                            
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>                    
                    <div class="row d-flex align-items-stretch">
                        <div id="tagPromocion" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 primary-back">
                            <span style="color:white">PROMOCIÓN</span>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="listPromocion" style="overflow-y: scroll; background-color: #f3f3f3;">
                            <?php if ( (isset($redenciones['promociones']) && count($redenciones['promociones']) > 0 ) || (isset($redenciones['cashback']) && count($redenciones['cashback']) > 0 )  ){ ?>
                                <?php foreach ($redenciones['cashback'] as $key) { ?>
                                <div class="row" style="border-bottom: 1px solid gray">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rowMedia btnPromo" data-id-user="<?php echo $usuario[0]['idUsuario'] ?>" data-id-promo="<?php echo $key['id_promociones'] ?>" data-tipo-promo="<?php echo $key['tipo' ]?>" <?php echo $key['tipo'] == 1 ? (!empty($key['cashback']) ? 'tipo-cashback="1" data-cashback="'.$key["cashback"].'"' :  'tipo-cashback="2" data-cashback="'.$key['cashbackperc'].'"') : "" ?> <?php echo $key['tipo'] == 3 || $key['tipo'] == 6 ? 'data-desc="'.$key["descuento"].'"' : "" ?>>
                                        <div class="row">
                                            <!--div class="media-left">
                                                <img class="media-object" src="../../inc/imagenes/gift-flat.svg" width="64" height="64" style="margin-right:5px">
                                            </div-->
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-self-center">
                                                <h3 class="text_secondary"><?php echo mb_strtoupper($key['nombre'],'utf-8') ?></h3>
                                                <h3><b>Tipo Promoción: <u><?php echo $key['nombreCampana'] ?></u></b></h3>
                                                <h4 style="color:black"><?php echo $key['detalle'] ?></h4>
                                                <h5><b>(<?php echo isset($key['restantes']) ? $key['restantes'] : $key['limite_por_usuario'] ?> Restantes)</b></h5>
                                            </div>
                                            <!--div class="media-right">
                                                <a class="btn btn-block btn_color" href="javascript:;"><small><?php echo isset($key['restantes']) ? $key['restantes'] : $key['limite_por_usuario'] ?><br>Restantes</small></b></a>
                                            </div-->
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>                        
                                <?php foreach ($redenciones['promociones'] as $key) { ?>
                                <div class="row" style="border-bottom: 1px solid gray">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rowMedia btnPromo" data-id-user="<?php echo $usuario[0]['idUsuario'] ?>" data-id-promo="<?php echo $key['id_promociones'] ?>" data-tipo-promo="<?php echo $key['tipo' ]?>" <?php echo $key['tipo'] == 1 ? (!empty($key['cashback']) ? 'tipo-cashback="1" data-cashback="'.$key["cashback"].'"' :  'tipo-cashback="2" data-cashback="'.$key['cashbackperc'].'"') : "" ?> <?php echo $key['tipo'] == 3 || $key['tipo'] == 6 ? 'data-desc="'.$key["descuento"].'"' : "" ?>>
                                        <div class="row">
                                            <!--div class="media-left">
                                                <img class="media-object" src="../../inc/imagenes/gift-flat.svg" width="64" height="64" style="margin-right:5px">
                                            </div-->
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-self-center">
                                                <h3 class="text_secondary"><?php echo mb_strtoupper($key['nombre'],'utf-8') ?></h3>
                                                <h4><b>Tipo Promoción: <u><?php echo $key['nombreCampana'] ?></u></b></h4>
                                                <h5 style="color:black"><?php echo $key['detalle'] ?></h5>
                                                <small><b>(<?php echo isset($key['restantes']) ? $key['restantes'] : $key['limite_por_usuario'] ?> Restantes)</b></small>
                                                <br>
                                                <br>
                                            </div>
                                            <!--div class="media-right">
                                                <a class="btn btn-block btn_color" href="javascript:;"><small><?php echo isset($key['restantes']) ? $key['restantes'] : $key['limite_por_usuario'] ?><br>Restantes</small></b></a>
                                            </div-->
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            <?php }else{ ?>
                                <div class="row" style="border-bottom: 1px solid gray">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rowMedia">
                                        <div class="media">                            
                                            <div class="media-body">
                                                <h4 style="color:black">Sin Promociones</h4>
                                                <h5><u>No hay promociones disponibles</u></h5>
                                            </div>                            
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</div>

<script src="../../inc/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="../../inc/js/movil_view_info.js"></script>
    <?php if($configuracion['tipo_confirm_operacion'] == 2){ ?>
        <script src="../../inc/js/jsQR.js"></script>        
    <?php }?>
<?php
}else{?>
    <div class="fullcard" >
        <div class="row margin-fix align-items-center">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="media">
                    <div class="media-left">
                        <a href="registrate2.php">
                            <img src="../../inc/imagenes/back_icon.svg" style="width:35px" />
                        </a>
                    </div>
                    <div class="media-body">
                        <h3 class="text-right text_color text-center" style="color:white !important"><u>No se encontró ningún cliente</u></h3>
                    </div>
                    <div class="media-right"></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        setTimeout(function(){ location.reload() }, 3000);
    </script>
<?php } 
    }
?>