<div class="fullcard">
    <div class="row align-items-center margin-fix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left primary-back">
            <a href="registrate2.php"><h2 style="color:white"><i class="material-icons">keyboard_arrow_left</i></h2></a>            
        </div>    
    </div>
    <div class="row align-items-center">        
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">                
            <form id="formID">
                <div class="row justify-content-center">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 text-center">
                        <br><br>
                        <!--div style="margin:auto; text-align:center"-->
                            <img id="logo" src="<?php echo '../../inc/'.$_POST['logo'] ?>" style="width:25%"/>
                        <!--/div-->
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                        <input type="hidden" name="logo" value="<?php echo $_POST['logo']?>" />
                        <div class="input-group">
                            <label for="idTarjeta">Ingresa el ID:</label>
                            <div class="input-group-prepend">
                                <span class="input-group-text"><strong>#</strong></span><input id="idTarjeta" type="text" name="id" style="width:100%" required/>
                            </div>                                
                        </div>
                        <button type="submit" class="btn btn-block btn_color" >Consultar</button>
                    </div>
                </div>                
            </form>
        </div>        
    </div>
</div>
<script src="../../inc/js/movil_scan_id.js"></script>