<?php
include '../../controller/registro/c_registro.php';
$fields = new C_registro($ser,$usu,$pas,$bd);
?>
<!-- Custom Js -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="../../inc/js/movil_registro.js"></script>

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="background-color: <?php echo $color_primario; ?>;">    
    <div class="row" style="height:100%">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-self-center">
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                    <br>
                    <img src="../../inc/<?php echo $img_logo; ?>" style="width:100%; border:2px solid white; border-radius:10px">
                    <br><br>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-9">
                    <button id="registro" class="btn-custom btn btn-block btn-sm btn_color_txt_w waves-effect btnHome" onclick="btns(this.id,'<?php echo $img_logo; ?>')">Regístrate</button>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 text-center">
                    <h3 style="color:white">¡Empieza a acumular puntos!</h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="background-color: rgba(0,0,0,0.6)">
    <div class="row" style="height:100%">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 align-self-lg-center align-self-md-center">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-6 col-sm-9 col-sm-9 text-center">
                    <h3 style="color:white">Inicia sesión</h3>
                </div>
            </div>
        <?php 
            $rest=$fields->trae_campos_reg();
            while($fila = $rest->fetch_assoc()){
                $id=$fila['alias']; 
                //alias    
                if(utf8_encode($fila['alias'])=='qr'){
                    $alias = 'Escanéa tu código QR';
                }elseif(utf8_encode($fila['alias'])!='' AND utf8_encode($fila['alias'])!='qr'){
                    $alias = utf8_encode($alias);
                    $alias = 'Ingresa tu '.$fila['alias'];
                }else{
                    $alias = '';
                }    
        ?>
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-6 col-sm-9 col-xs-9" style="margin: 10px 0;">
                    <button id="<?php echo $id; ?>" onclick="btns(this.id,'<?php echo $img_logo; ?>')" class="btn btn_color waves-effect btnHome" style=" font-size: 1.2em !important; color:white !important; border:1px solid white"><?php echo $alias; ?></button>                
                </div>
            </div>
        <?php 
            }  
        ?> 
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-6 col-sm-4 col-xs-4 align-self-end">
                    <br><br>
                    <img src="../../inc/imagenes/powered.png" style="width:100%">
                    <br><br>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End Custom Js -->  
<!--div class="col-lg-6 col-md-6 col-sm-12" height="100%">
    <div id="menu_top" class="menu-top" style="background-color: <?php echo $color_primario; ?>">
        <div class="logotype text-center">
            <br>
            <br>
            <img src="../../inc/<?php echo $img_logo; ?>" width="30%">
        </div>
        <div class="menu-button">
            <button id="registro1" class="btn-custom btn btn-block btn-sm btn_color_txt_w waves-effect btnHome" onclick="btns(this.id,'<?php echo $img_logo; ?>')">Regístrate</button>
        </div>
        <div class="menu-txt">
            ¡Empieza a acumular puntos!
        </div>
    </div>
    <div id="menu_bottom" class="menu-bottom menu-h">
        <div class="menu-txt2">
            Inicia sesión
        </div>
        <div class="menu-items">
            <?php 
            $rest=$fields->trae_campos_reg();
             while($fila = $rest->fetch_assoc()){
                $id=$fila['alias']; 
                //alias    
                if(utf8_encode($fila['alias'])=='qr'){
                    $alias = 'Escanéa tu código QR';
                }elseif(utf8_encode($fila['alias'])!='' AND utf8_encode($fila['alias'])!='qr'){
                    $alias = utf8_encode($alias);
                    $alias = 'Ingresa tu '.$fila['alias'];
                }else{
                    $alias = '';
                }   
            ?> 
            <div class="pt10" style="margin-bottom:10px">
                <button id="<?php echo $id; ?>" onclick="btns(this.id,'<?php echo $img_logo; ?>')" class="btn btn_color waves-effect btnHome" style=" font-size: 1.2em !important; color:white !important; border:1px solid white"><?php echo $alias; ?></button>                
            </div>
            <?php 
                }  
            ?>
        </div>
        <div class="powered">
            <img src="../../inc/imagenes/powered.png" width="100%">
        </div>
    </div>
</div>
<section class="col-lg-6 col-md-6 col-sm-12">
    <div>
      <div id="card-inner" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div>

        </div>
        <?php 
            
        ?>
      </div>       
    </div>
</section-->
<!--script>
    $(window).on("resize", function(){resizeWindow()});

    let heightTop = $("#menu_top").height();
    let heightBottom = $("#menu_bottom").height();

    var resizeWindow = function(){
        //console.log("resize");
        $("#menu_top").height("auto");
        $("#menu_bottom").height("auto");

        if($(window).height() > ($("#menu_top").height() + $("#menu_bottom").height())){
            $("#menu_bottom").css("height", $(window).height() - $("#menu_top").height());            
        }
    }

    resizeWindow();
</script-->