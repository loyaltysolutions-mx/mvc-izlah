<?php    
    $img_logo = $_POST['logo'];
?>
<div class="fullcard" >
    <div class="row margin-fix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left primary-back" >                
            <a href="registrate2.php"><h2 style="color:white"><i class="material-icons">keyboard_arrow_left</i></h2></a>
        </div>            
    </div>
    <div class="row align-items-center margin-fix">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="border-right: 1px solid gray">            
            <div class="row justify-content-center">
                <div class="col-md-10 col-lg-10 col-sm-10 col-xs-10">
                    <div style="padding:5%">
                        <canvas id="canvas" hidden style="width:100%; border-radius: 10px" ></canvas>
                        <p id="loadingMessage" style="color:white; text-align:center"> No se detectó alguna cámara activa </p>                            
                        <div id="output">
                            <p id="outputMessage" style=" text-align:center; color:white">No se ha detectado ningún código QR.</p>
                        </div>
                    </div>
                </div>                    
            </div>                
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="row justify-content-center"><div class="col-md-9 col-lg-9 col-sm-9 col-xs-9 text-center"><br><br><img src="<?php echo "../../inc/".$img_logo ?>" style="max-width:50%"/></div></div>
            <div class="row justify-content-center"><div class="col-md-6 col-lg-6 col-sm-6 col-xs-6"><br><img style="width:100%" src="../../inc/imagenes/mano_tarjeta.svg" /></div></div>
            <div class="row justify-content-center"><div class="col-md-10 col-lg-10 col-sm-10 col-xs-10"><h5 class="text-center text_color"> Coloca tu tarjeta a 10 cm de distancia y escanea el código QR </h5></div></div>
        </div>
    </div>    
</div>
<script src="../../inc/js/jsQR.js"></script>
<script>
    //$(document).ready(function(){
        var mediaStream = null;
        var video = document.createElement("video");
        var canvasElement = document.getElementById("canvas");
        var canvas = canvasElement.getContext("2d");
        var loadingMessage = document.getElementById("loadingMessage");            
        function drawLine(begin, end, color) {
          canvas.beginPath();
          canvas.moveTo(begin.x, begin.y);
          canvas.lineTo(end.x, end.y);
          canvas.lineWidth = 4;
          canvas.strokeStyle = color;
          canvas.stroke();
        }
        // Use facingMode: environment to attemt to get the back camera on phones, user to get the front camera

        navigator.mediaDevices.getUserMedia({ 
            video: { 
                facingMode: "user",                    
            } 
        }).then(function(stream) {            
            
            mediaStream = stream;
            mediaStream.stop = function () {
                this.getAudioTracks().forEach(function (track) {
                    track.stop();
                });
                this.getVideoTracks().forEach(function (track) { //in case... :)
                    track.stop();
                });
            };
            video.srcObject = stream;
            video.setAttribute("playsinline", true); // required to tell iOS safari we don't want fullscreen
            video.play();
            requestAnimationFrame(tick);
            
        });
        
        function tick() {            
            if (video.readyState === video.HAVE_ENOUGH_DATA) {
                loadingMessage.hidden = true;
                canvasElement.hidden = false;                    
                canvasElement.height = video.videoHeight;
                canvasElement.width = video.videoWidth;
                canvas.drawImage(video, 0, 0, canvasElement.width, canvasElement.height);
                var imageData = canvas.getImageData(0, 0, canvasElement.width, canvasElement.height);
                var code = jsQR(imageData.data, imageData.width, imageData.height, {
                  inversionAttempts: "invert",
                });
                if (code) {                        
                    $.ajax({
                        data:{ idUsuario: code.data, logo : '<?php echo $img_logo?>' },
                        url: 'view_info.php',
                        method:'post',
                        beforeSend: function(response){
                            mediaStream.stop(); 
                        },
                        success: function(response){                                                               
                            $('#card-content').html(response);
                        }
                    });
                }
            }
            requestAnimationFrame(tick);
        }
    //});        
</script>
