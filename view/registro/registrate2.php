<?php 
//By Allan M.
include '../../inc/cont_fijos3.php';
include '../../inc/funciones.php';
include '../../inc/parametros.php';
//$inst_control=new C_home($ser,$usu,$pas,$bd);
$ins_cont_fijos=new Cont_fijos($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
/*session_start();
if(isset($_SESSION["usuario"])){ 
    $catalogue = 1;
    //echo "<script>console.log('".$catalogue." estas logueado');</script>";
}else{*/
    $catalogue = 0;

      //TRAEMOS CONFIGURACION DE ESTILOS
   $res_con1=$ins_funciones->consulta_generica('tbl_estilo', ' ');
   $registro1= mysqli_fetch_assoc($res_con1);

   $res_con2=$ins_funciones->consulta_generica('tbl_configuracion_proyecto', ' ');
   $config = mysqli_fetch_assoc($res_con2);
   
   //VALIDAMOS LOGO
  if($registro1['logo_cte']==''){
      $img_logo1='imagenes/logo.png';
  }else{
      $img_logo1='imagenes/img_configuracion/'.$registro1['logo_cte'];
  }

?>
<!DOCTYPE html>
<html lang="es" style="overflow: auto;">
    <head>
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
      <meta content="yes" name="mobile-web-app-capable">
      <link rel="icon" sizes="192x192" href="../../inc/<?php echo $img_logo1 ?>">
      <title>:: <?php echo $config['titulo_home'] ?> :: <?php echo $config['detalle_home'] ?> </title>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">    
      <?php  
        $ins_cont_fijos->head();

        //TRAEMOS CONFIGURACION DE ESTILOS
        $res_con = $ins_funciones->consulta_generica('tbl_estilo', ' ');
        $registro = mysqli_fetch_assoc($res_con);
        
        //VALIDAMOS LOGO
        if($registro['logo_cte']==''){
          $img_logo='imagenes/logo.png';
        }else{
          $img_logo='imagenes/img_configuracion/'.$registro['logo_cte'];
        }

        //VALIDAMOS IMAGEN FONDO
        if($registro['fondo_cte']==''){
          $img_background='imagenes/background_default.jpg';
          $img_back = 'background_default.jpg';
        }else{
          $img_background='imagenes/img_configuracion/'.$registro['fondo_cte'];
          $img_back = $registro['fondo_cte'];
        }
        
        //VALIDAMOS COLOR PRIMARIO
        if($registro['color_primario']==''){
         $color_primario='#006AA9';
        }else{
         $color_primario=$registro['color_primario'];
        }
        
        //VALIDAMOS COLOR SECUNDARIO
        if($registro['color_secundario']==''){
         $color_secundario='#006AA9';
        }else{
         $color_secundario=$registro['color_secundario'];
        }
      ?>
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
      
        <!-- Custom Css -->
        <link href="../../inc/css/registro.css" rel="stylesheet"/>
        <style type="text/css">
        .registro {
          padding-left: 0;
          background: url(../../inc/imagenes/img_configuracion/<?php echo $img_back; ?>) no-repeat;
          background-repeat: no-repeat;
          background-size: cover;
          height: 100%;
          background-attachment: fixed;
        }

        .text_secondary{
          color: <?php echo $color_secundario; ?> !important;
        }

        label{
            color:orange;
        }
        .scrollable {
            overflow-x: hidden;
            overflow-y: auto;
        }

        input[type="text"], input[type="number"], input[type="tel"]{
            border:1px solid #ced4da;
            border-radius:0 5px 5px 0 ;
        }

        .btn-xs{
            padding: 5px 2px;
            font-size: 11px;
            line-height: 1.5;
            border-radius: 3px;
        }

        .modal-header{
            background-color:orange;
            color:white;
        }

        .btn-info, .btn-info:active, .btn-info:focus, .btn-info:visited{
            background-color:orange !important;
            color:white;
        }

        .btn-info:hover{
            border: 1px solid orange !important;
            background-color:white !important;
            color:orange;
        }

        .btn-outline-info{
            border: 1px solid orange;
        }

        .rowRender{
            background-color: #d0d0d0;
        }

        .rowRedencion:hover, .rowPromocion:hover, .rowRedencion:hover, .rowPromocion:hover {
            background-color: rgba(0,0,0,0.3);
        }        
        
      </style>
        
    </head>
<?php
  //LLAMAMOS ESTILOS DE CONFIGURACION
  $ins_cont_fijos->estilos($img_logo,$img_background,$color_primario,$color_secundario);
?>  
    <body class="registro">
        <div id="card-content" class="row margin-fix" style="height:100%;">
            <?php
                include 'registro.php';
            ?>
        </div>
        <?php  
    $ins_cont_fijos->footer();
  ?>
    </body>
  
</html>
<?php    
//}
?>