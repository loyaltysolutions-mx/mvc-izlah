
<?php 

/*************************************************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#- Vista de Registrate -#
##################################################################################### 
include_once '../../inc/cont_fijos.php';
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
include_once '../../controller/registro/c_registro.php';
$ins_control_registro=new C_registro($ser,$usu,$pas,$bd);
$ins_cont_fijos=new Contenidos_Fijos($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
session_start();
if(isset($_SESSION["usuario"])){ 
    $catalogue = 1;
    //echo "<script>console.log('".$catalogue." estas logueado');</script>";
}else{
    $catalogue = 0;
    //echo "<script>console.log('".$catalogue." No estas logueado');</script>";
}
?>
    
    

     <?php
 $ins_cont_fijos->head();
 //TRAEMOS CONFIGURACION DE ESTILOS
 $res_con=$ins_funciones->consulta_generica('tbl_estilo', ' ');
 $registro= mysqli_fetch_assoc($res_con);
 //VALIDAMOS LOGO
if($registro['logo_cte']==''){
    $img_logo='imagenes/logo.png';
}else{
    $img_logo='imagenes/img_configuracion/'.$registro['logo_cte'];
}
 //VALIDAMOS IMAGEN FONDO
if($registro['fondo_cte']==''){
    $img_background='imagenes/background_default.jpg';
}else{
    $img_background='imagenes/img_configuracion/'.$registro['fondo_cte'];
}
 //VALIDAMOS COLOR PRIMARIO
if($registro['color_primario']==''){
     $color_primario='#006AA9';
}else{
    $color_primario=$registro['color_primario'];
}
 //VALIDAMOS COLOR SECUNDARIO
if($registro['color_secundario']==''){
     $color_secundario='#006AA9';
}else{
    $color_secundario=$registro['color_secundario'];
}
//LLAMAMOS ESTILOS DE CONFIGURACION
$ins_cont_fijos->estilos_front($img_logo,$img_background,$color_primario,$color_secundario);
  ?>
<!DOCTYPE html>
<html lang="es" class='form_front'>
 <body class="theme-red">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        
   
        <div id="carousel" class="carousel slide" data-ride="carousel" style='position: initial!important;padding: 4%'>
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel" data-slide-to="1"></li>
                                </ol>
                                <div class="carousel-inner" role="listbox">
                                    <div class="item active">
                                        <div class="inicial text-center">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center img-responsive">
                                                    <img src="../../inc/<?php echo $img_logo ;?>" class="imagen">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center letras_carrusel">
                                                    ¡Disfruta la mejor experiencia y gana grandes recompensas!
                                                </div>
                                            </div>
                                            <div class="row margin-auto btn-cotainer text-center" style='padding-top: 4%;'>
                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center ">
                                                    <a href='./registro.php' class="submit btn btn-success  btn_regist" >Registro</a>
                                                </div>
                                                  <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center">
                                                    <a href='../acumulacion/acumulacion.php' class="submit btn btn-success  btn_regist" >Ya soy Miembro!</a>
                                          
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="productos text-center">
                                         <?php 
                                         $ins_cont_fijos->catalogo_productos($catalogue);
                                         ?>  
                                        </div>
                                    </div>
                                </div>

                                <!-- Controls -->
                                <a class="left carousel-control" href="#carousel" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="right carousel-control" href="#carousel" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        
                    
                </div>
    <script src="../../inc/plugins/jquery/jquery.js"></script>
    <script src="../../inc/plugins/bootstrap/js/bootstrap.js"></script>            
    <script src="../../inc/js/jquery.touchSwipe.js"></script> 
    <script type="text/javascript">

    $(function() {


      //end swipe tu_busca

    $("#carousel").carousel({
		interval: 9000
	});
    $("#carousel").swipe({

      swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
        
        if (direction == 'left') {
            
            $("#carousel").carousel('next');
        }
        else if (direction == 'right'){
            
            $("#carousel").carousel('prev');
        } 

      },
      allowPageScroll:"vertical"

    });

    

});

</script>
</body>

</html>
 