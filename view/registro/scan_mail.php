<div class="fullcard">
    <div class="row align-items-center margin-fix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left primary-back">
            <a href="registrate2.php"><h2 style="color:white"><i class="material-icons">keyboard_arrow_left</i></h2></a>
        </div>    
    </div>
    <div class="row align-items-center margin-fix" style="height: auto">
        <form id="formMail" style="width:100%; margin: 40px 0">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                <div class="row justify-content-center">
                    <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9 text-center">
                        <img id="logo" src="<?php echo '../../inc/'.$_POST['logo'] ?>" style="width:100%; max-width:300px" />
                        <input type="hidden" name="logo" value="<?php echo $_POST['logo']?>" />
                    </div>
                </div>
                <br>
                <div class="row justify-content-center">
                    <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9">
                        <div class="input-group">
                            <label for="mail">Ingresa el email:</label>
                            <div class="input-group-prepend">
                                <span class="input-group-text"><strong>@</strong></span><input id="mail" type="email" name="mail" style="width:100%" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$" required/>
                            </div>                                
                        </div>                            
                    </div>
                </div>            
                <div class="row justify-content-center">
                    <div class="col-md-9 col-lg-9 col-sm-9 col-xs-9"><br>
                        <button type="submit" class="btn btn-block" style="background-color:orange; color:white">Consultar</button>
                    </div>
                </div>                    
            </div>
        </form>
    </div>
</div>
<script src="../../inc/js/movil_scan_mail.js"></script>