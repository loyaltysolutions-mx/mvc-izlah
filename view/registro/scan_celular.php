<?php
	$img_logo = $_POST['logo'];
?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left nop" style="background-color: rgba(0,0,0,0.6)">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">			
			<div class="row justify-content-start">
				<div class="col-lg-1 col-md-1 col-sm-2 col-xs-2">			
					<a href="registrate2.php"><img src="../../inc/imagenes/back_icon.svg" style="width:100%" /></a>
				</div>
			</div>			
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">    
		    <div class="row">
		        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		            <div class="row justify-content-center">
		                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
		                    <br>
		                    <img src="../../inc/<?php echo $img_logo; ?>" style="width:100%; border:2px solid white; border-radius:10px">
		                    <br><br>
		                </div>
		            </div>
		            <div class="row justify-content-center">
		                <form>
			        		<input class="form-control-b" maxlength="10" minlength="10" type="text" id="cel" name="cel" required="true" disabled>
			        	</form>
		            </div>            
		        </div>
		    </div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
		    <div class="row">
		        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		            <div class="row justify-content-center">
		                <div class="col-lg-6 col-md-6 col-sm-9 col-sm-9 text-center">		                	
		                	<br>
		                    <h3 style="color:red;">Por favor digita tu número</h3>
		                </div>
		            </div>
		            <div class="row justify-content-center">
		            	<div class="col-lg-9 col-md-9 col-sm-6 col-xs-6">
		            		<div class="row">
	            				<div onclick="sendnum(1)" class="cellNumber col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">1</div>
	            				<div onclick="sendnum(2)" class="cellNumber col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">2</div>
	            				<div onclick="sendnum(3)" class="cellNumber col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">3</div>
		            		</div>
		            		<div class="row">
	            				<div onclick="sendnum(4)" class="cellNumber col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">4</div>
	            				<div onclick="sendnum(5)" class="cellNumber col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">5</div>
	            				<div onclick="sendnum(6)" class="cellNumber col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">6</div>	            				
		            		</div>
		            		<div class="row">
	            				<div onclick="sendnum(7)" class="cellNumber col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">7</div>
	            				<div onclick="sendnum(8)" class="cellNumber col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">8</div>
	            				<div onclick="sendnum(9)" class="cellNumber col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">9</div>
		            		</div>
		            		<div class="row">
	            				<div class="cellNumber col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center"></div>
	            				<div onclick="sendnum(0)" class="cellNumber col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center">0</div>
	            				<div onclick="sendnum('r')" class="cellNumber col-lg-4 col-md-4 col-sm-4 col-xs-4 text-center"><i class="fas fa-backspace"></i></div>
		            		</div>
		            	</div>
		            </div>
		            <div class="row justify-content-center">
		            	<div class="col-lg-9 col-md-9 col-sm-6 col-xs-6">
		            		<br>
		        			<button onclick="sendtel('<?php echo $img_logo; ?>')" class="btn btn_color waves-effect btnHome" style="width:100%">Confirmar</button>
		            	</div>
		            </div>
		        </div>
		    </div>
		</div>
	</div>
</div>



<!--div class="fullcard">
	<div class="row margin-fix">
		<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 nop">
			<div class="back">
				<a href="registrate2.php">
					<i class="fas fa-angle-left white"></i>
				</a>
			</div>
			<div class="card-left">
				<div class="logotype-cel">
		            <img src="../../inc/<?php echo $img_logo; ?>" width="50%">
		        </div>
		        <div class="black-txt">
		        	<form>
		        		<input class="form-control-b" maxlength="10" minlength="10" type="text" id="cel" name="cel" required="true" disabled>
		        	</form>
		        </div>
		        <h2 class="gray-txt">
	        		Ej. 5512345678
	        	</h2>
			</div>
		</div>
		<div class="col-md-6 col-lg-6 col-sm-12 col-xs-12 nop nopm">
			<h2 class="text-center" style="color:red;">Por favor digita tu número</h2>
	        <div class="divtable">
	        	<table id="numeric" class="border-color">
	        		<tr>
	        			<td onclick="sendnum(1)">1</td>
	        			<td onclick="sendnum(2)">2</td>
	        			<td onclick="sendnum(3)">3</td>
	        		</tr>
	        		<tr>
	        			<td onclick="sendnum(4)">4</td>
	        			<td onclick="sendnum(5)">5</td>
	        			<td onclick="sendnum(6)">6</td>
	        		</tr>
	        		<tr>
	        			<td onclick="sendnum(7)">7</td>
	        			<td onclick="sendnum(8)">8</td>
	        			<td onclick="sendnum(9)">9</td>
	        		</tr>
	        		<tr>
	        			<!--td class="" onclick="sendtel('<?php //echo $img_logo; ?>')"><i class="fas fa-paper-plane"></i></td-->
	        			<!--td></td>
	        			<td onclick="sendnum(0)">0</td>
	        			<td onclick="sendnum('r')"><i class="fas fa-backspace"></i></td>
	        		</tr>
	        	</table>
	        	<div class="button-cel">
		        	<button onclick="sendtel('<?php echo $img_logo; ?>')" class="btn-custom2 btn btn-block btn-sm btn_color waves-effect">Confirmar</button>
		        </div>
	        </div>
		</div>
	</div>
</div-->	