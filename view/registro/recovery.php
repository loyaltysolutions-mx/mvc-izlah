<!DOCTYPE html>
<html lang="es">
<head>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<?php  
include_once '../../inc/cont_fijos.php';
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
include_once '../../controller/registro/c_registro.php';
$ins_control_registro=new C_registro($ser,$usu,$pas,$bd);
$ins_cont_fijos=new Contenidos_Fijos($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
 
 $ins_cont_fijos->head();
 //TRAEMOS CONFIGURACION DE ESTILOS
 $res_con=$ins_funciones->consulta_generica('tbl_estilo', ' ');
 $registro= mysqli_fetch_assoc($res_con);

 $campoClave=$ins_funciones->consulta_generica('tbl_rel_config_campos_registro AS A INNER JOIN tbl_campos_registro AS B ON (B.id_campos_registro = A.id_campo)', 'WHERE A.clave = 1 ');

 

 //VALIDAMOS LOGO
if($registro['logo_cte']==''){
    $img_logo='imagenes/logo.png';
}else{
    $img_logo='imagenes/img_configuracion/'.$registro['logo_cte'];
}
 //VALIDAMOS IMAGEN FONDO
if($registro['fondo_cte']==''){
     $img_background='imagenes/background_default.jpg';
}else{
    $img_background='imagenes/img_configuracion/'.$registro['fondo_cte'];
}
 //VALIDAMOS COLOR PRIMARIO
if($registro['color_primario']==''){
     $color_primario='#006AA9';
}else{
    $color_primario=$registro['color_primario'];
}
 //VALIDAMOS COLOR SECUNDARIO
if($registro['color_secundario']==''){
    $color_secundario='#006AA9';
}else{
    $color_secundario=$registro['color_secundario'];
}
  ?>
<!--script src="https://code.jquery.com/jquery-3.4.0.min.js" integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg=" crossorigin="anonymous"></script-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css" >

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" >
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" >

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.9/css/bootstrap-select.css" >

<script src="../../inc/js/sweet-alert2.js"></script>

<script src="../../inc/plugins/momentjs/moment.js"></script>
<script src="../../inc/plugins/momentjs/moment-with-locale.js"></script>    

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<!--script type="text/javascript" src="../../inc/js/valida.js"></script-->
<?php
//LLAMAMOS ESTILOS DE CONFIGURACION
$ins_cont_fijos->estilos($img_logo,$img_background,$color_primario,$color_secundario);

?>
<style>
    .preloader{
        width: 100px !important;
        height: 100px !important;
    }

    .lds-dual-ring {
        display: inline-block;
        width: 64px;
        height: 64px;
    }
    .lds-dual-ring:after {
        content: " ";
        display: block;
        width: 46px;
        height: 46px;
        margin: 1px;
        border-radius: 50%;
        border: 5px solid #fff;
        border-color: <?php echo $color_secundario ?> transparent <?php echo $color_secundario ?> transparent;
        animation: lds-dual-ring 1.2s linear infinite;
    }
    @keyframes lds-dual-ring {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }

</style>
</head>

<body class="theme-red">
    <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Espere por favor...</p>
            </div>
        </div>
    <!-- Barra Ariba -->
    <nav class="navbar" style='height: 70px;'> 
        <div class="container-fluid">
            <div class="navbar-header">
                <span class="navbar-brand" >Plataforma de Lealtad </span>
            </div>
        </div>
    </nav>
<!-- CONTENIDO -->
    <div class="container-fluid" style="margin-top:85px">        
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2><i class="far fa-id-badge"></i> Recuperar Usuario <small> Por favor llena los campos solicitados</small></h2>                            
                    </div>
                    <div class="body">
                        <form id="formRecovery">
                            <?php while ($row = mysqli_fetch_assoc($campoClave)) { 
                                switch ($row['id_campo']) {
                                    case 1:
                                      $type = "text";
                                      $min = '';
                                      $max = '';
                                      $pattern = "[A-Za-z{3,}]";
                                      $icon = "person";
                                    break;
                                    case 3:
                                        $type = "tel";
                                        $min = 'minlength="10"';
                                        $max = 'maxlength="10"';
                                        $pattern = "[1-9{1}]\d{9}";
                                        $icon = "phone_android";
                                    break;
                                    case 5:
                                        $type = "email";
                                        $min = 'minlength="10"';
                                        $max = '';
                                        $pattern = "";
                                        $icon = "email";
                                    break;

                                    case 6:
                                        $type = "text";
                                        $min = '';
                                        $max = '';
                                        $pattern = "[A-Z{4}][0-9{6}][A-Z{2}][0-9{1}]";
                                        $icon = "keyboard";
                                    break;

                                    case 7:
                                        $type = "number";
                                        $min = 'min"1"';
                                        $max = '';
                                        $pattern = "[0-9{5,}]";
                                        $icon = "keyboard";
                                    break;


                                    default:
                                        $type = "text";
                                        $min = '';
                                        $max = '';
                                        $pattern = "";
                                        $icon = "keyboard";
                                    break;
                                }
                            ?>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons"><?php echo $icon ?></i>
                                    </span>
                                    <div class="form-line">
                                        <input type="<?php echo $type ?>" id='username' class="form-control" <?php echo $min ?> <?php echo $max ?> pattern="<?php echo $pattern ?>" name='username' style="font-size:14px" required placeholder="Ingresa tu <?php  echo $row['id_campo'] == 3 ? "Celular a 10 dígitos" : ucwords($row['nombre']) ?>">
                                    </div>
                                </div>
                                <button id="btnSubmit" type="submit" class="btn btn_color m-t-15 waves-effect"> Recuperar Usuario </button>

                                <div id="loadAnimation" style="display:none; width:100%; text-align:center"><div class="lds-dual-ring"></div></div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $("#formRecovery").on("submit", function(e){
            e.preventDefault();
            //console.log("click");
            $("#btnSubmit").fadeOut(500, function(){
                var link = "../../controller/valida/c_llamadas_ajax.php";
                var data = {'op' : 1, 'username' : $("#username").val()};
                var beforeFunction = function(){
                    $("#loadAnimation").fadeIn();
                };
                var successFunction = function(data){
                    if(data.result){
                        switch(data.result){
                            case 1:                                
                                Swal("Éxito", "Se ha enviado un mail/sms a tu correo o teléfono con el proceso para recuperar tu contraseña", "success").then((value) => {window.location="../../"});
                            break;

                            case 2:
                                Swal("Error", "Los datos ingresados no corresponden a un usuario registrado. Intenta nuevamente", "error");
                                $("#loadAnimation").hide();
                                $("#btnSubmit").show();
                            break;

                            case 3:
                                Swal("Error", "Hubo un error al procesar la solicitud, intenta nuevamente, si el problema persiste contacta al administrador", "error");
                                $("#loadAnimation").hide();
                                $("#btnSubmit").show();
                            break;

                            case 4:
                                Swal("Error", "Hubo un error al procesar la solicitud, intenta nuevamente, si el problema persiste contacta al administrador", "error");
                                $("#loadAnimation").hide();
                                $("#btnSubmit").show();
                            break;
                        }
                        
                    }                    
                };

                var failFunction = function(data){
                    Swal("Error de conexión", "No se pudo conectar al sistema, verifica tu conexión e intenta nuevamente", "error");
                    $("#loadAnimation").hide();
                    $("#btnSubmit").show();
                };

                connectServer(link, data, beforeFunction, successFunction, failFunction);
            });
        });

        function connectServer(link, data, beforeFunction, successFunction, failFunction){
            $.ajax({
                type: "POST",
                timeout: 7000,
                url: link,
                data: data,
                dataType: "json",
                beforeSend : function(){
                    beforeFunction && beforeFunction();
                }
            }).done(function(data){
                successFunction && successFunction(data);
                console.log(data);
            }).fail(function(data){
                failFunction && failFunction(data);
                console.log(data);
            });
        }

    </script>
</body>
</html>    