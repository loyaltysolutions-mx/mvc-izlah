
<?php 
/*************************************************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#- Vista de Registro -#
##################################################################################### 
require '../../inc/funciones.php';
require '../../inc/parametros.php';
include_once '../../controller/registro/c_registro.php';
session_start();
$ins_control_registro=new C_registro($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
$id_usu_session=$_SESSION["id_usuario"];
//$id_usu_session=$_SESSION["id_usuario"];
//session_start();
//if(isset($_SESSION["usuario"])){ 
    //VISTA PARA UN PUNTO DE VENTA
?>

<?php
//include_once '../../controller/user/c_user.php';
//$parks = new user($ser,$usu,$pas,$bd);
?>
<div class="card">
    <div class="body">
        <form id="form_registro" onsubmit="registro(); return false;">
            <h3>Registro de Nuevo Usuario</h3>
            <hr/>
            <?php 
            $res=$ins_control_registro->trae_campos();
            $clave='';
            while($fila = $res->fetch_assoc()){ 
                //EVALUAMOS SI ES CAMPO CLAVE O NO
                if($fila['clave']==0)
                {
                    $placeholder='';
                    $caracter='';
                    $clave.='';
                }else{
                    $placeholder='*Este campo sera el usuario de la cuenta';
                    $caracter='<i class="fas fa-user"></i>';
                    $clave.=$fila['campo'];
                }
                //EVALUAMOS SI ES TELEFONO     
                if(utf8_encode(ucwords($fila['campo']))=='Teléfono')
                {
                    $coment_tel='/ Lada+Número Celular';
                    $campo0=utf8_encode(ucwords($fila['campo'])).' Celular' ;
                }else{
                    $coment_tel='';
                    $campo0=utf8_encode(ucwords($fila['campo']));
                } 
                $bono_cumple=$fila['bono_cumple'];
                ?> 
                <div class="col-sm-12 demo-masked-input">
                    <b><?php echo $campo0; ?>:</b>
                    <div class="input-group">
                        <span class="input-group-addon" style="color: red !important;">
                            <i class="material-icons"><?php echo $caracter; ?></i>                            
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control"  name='<?php echo utf8_encode($fila['campo'])?>'  required>
                        </div>
                    </div>
                </div>
                <?php 
            }

            if($bono_cumple==1){?>
               <!-- <div class="row clearfix">
                    <div class="col-sm-12">
                        <h2 class="card-inside-title">Fecha de Nacimiento</h2>
                        <div class="form-group">
                            <input type="text" class='datepicker'  name='fecha_cumple'>
                        </div>
                    </div>
                </div>-->		
                <div class="col-sm-12 demo-masked-input">
                    <b>Fecha de Nacimiento:</b>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">date_range</i>
                        </span>
                        <div class="form-line">
                            <input id="fecha_cumple" name="fecha_cumple" type="text" class="form-control date" placeholder="Ej: 30/07/2000">
                        </div>
                    </div>
                </div>
                <!--div class="col-sm-4">
                    <input type="text" name='fecha_cumple'  id='fecha_cumple' class="datepicker form-control" placeholder="Fecha de Nacimiento">
                </div-->	   
            <?php } ?>
            <?php if($ins_control_registro->get_status_nivel() == 1) { 
                $niveles = $ins_control_registro->get_niveles()
            ?>
            <div class="col-sm-12 demo-masked-input">
                <b>Nivel:</b>
                <div class="input-group">
                    <span class="input-group-addon">
                      <i class="material-icons">keyboard_arrow_up</i>
                    </span>
                    <div class="form-line">
                        <select id="nivel" name="nivel" onchange="" class="form-control show-tick" required>
                            <option value="">Elige un Nivel</option>
                            <?php foreach ($niveles as $index => $nivel) { ?>
                                <option value="<?php echo $nivel['id_nivel']; ?>"><?php echo $nivel['nombre']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>  
            <?php } ?>            
            <div class="col-sm-12">
                <b>Genero:</b>
    			<div class="input-group">                    
                    <span class="input-group-addon">
                        <i class="material-icons">wc</i>
                    </span>
    				<div class="form-line">
    					<select id="genero" name="genero" class="form-control show-tick" required>
    						<option value="">Elige una opción</option>
    						<option value="0">Mujer</option>
    						<option value="1">Hombre</option>
    					</select>
    				</div>
    			</div>
            </div>
            <input type="hidden" class="form-control "  name='id_usu_session' value="<?php  echo  $id_usu_session; ?>">
            <input type="hidden" class="form-control "  name='clave' value="<?php  echo  $clave ?>">
            <div id='loading'></div>
            <button class="btn btn-block btn-lg btn_color waves-effect" onclick="sendform();" name="submit-add">Registrar</button>
        </form>
    </div>
</div>

<!-- Moment Plugin Js -->
<script src="../../inc/plugins/momentjs/moment.js"></script>
<!-- Input Mask Plugin Js -->
<script src="../../inc/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script>
<!-- Bootstrap Tags Input Plugin Js -->
<script src="../../inc/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script src="../../inc/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<script>

    function sendform(){
        var form = $('#form_registro');
        $('#form_registro').find('[type="submit"]').trigger('click');
    }
	

</script>  
<script type="text/javascript" src='../../inc/js/registro2.js'></script>

                    