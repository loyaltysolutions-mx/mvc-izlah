<?php   
    session_start();
    if(isset($_SESSION["usuario"])){ 
?>
<div class="card">
    <div class="header">
        <h2 class="text_color"><i class="fas fa-chart-pie"></i></i> Reporte de Segmentos <small>Por favor elige uno o varios filtros (opcional)</small></h2>
    </div>
    <div class="body">
        <form id="formSearch">            
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <input id="inputStatus" type="checkbox" value="1" class="filter filled-in"><label for="inputStatus"><b>Status</b></label>
                    </div>                    
                </div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 divFilter">
                    <div class="input-group">
                        <div class="form-line">
                            <select id="selectStatus" class="form-control show-tick">
                                <option value="0"> Activo </option>
                                <option value="1"> Inactivo </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">                        
                        <input id="inputFechaCreacion" type="checkbox" value="1" class="filter filled-in"><label for="inputFechaCreacion"><b>Fecha de Creacion</b></label>
                    </div>                    
                </div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 divFilter">                    
                    <div class="input-group">                        
                        <span class="input-group-addon">
                            <i class="material-icons">date_range</i>
                        </span>
                        <div class="form-line">
                            <input id="desde" type="text" class="form-control date datetimepicker" placeholder="Desde" /><span id="textDesde" style="color:red"></span>
                        </div>
                    </div>
                    <div class="input-group">                        
                        <span class="input-group-addon">
                            <i class="material-icons">date_range</i>
                        </span>
                        <div class="form-line">
                            <input id="hasta" type="text" class="form-control date datetimepicker" placeholder="Hasta" /><span id="textHasta" style="color:red"></span>
                        </div>
                    </div>
                </div>
            </div>            
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <a id="btnConsultar" href="javascript:;" class="btn btn_color">Consultar</a>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="divTable" class="card">
    <div class="header clearfix">
        <div class="pull-left"><h2>Tabla de Resultados</h2></div>        
    </div>
    <div class="body divTable">        
        <div class="table-responsive">
            <table id="tableResult" class="table table-bordered table-striped table-hover" style="width:100%"></table>
        </div>                    
    </div>
</div>
<script src="../../inc/plugins/bootstrap-select/js/bootstrap-select.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.33.1/dist/sweetalert2.min.js"></script>
<!--script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script-->
<script src="../../inc/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="../../inc/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<script src="../../inc/js/pages/tables/jquery-datatable.js"></script>

<!-- Moment Plugin Js -->
<script src="../../inc/plugins/momentjs/moment.js"></script>
<script src="../../inc/plugins/multi-select/js/jquery.multi-select.js"></script>

<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="../../inc/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<script src='../../inc/js/reporte_segmentos.js'></script>
<?php } ?>