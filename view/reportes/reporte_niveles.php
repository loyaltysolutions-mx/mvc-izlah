<?php
    include_once '../../inc/parametros.php';
    include_once '../../controller/reportes/c_reporte_niveles.php';    
    $instanceReport=new C_reporte_niveles($ser,$usu,$pas,$bd);    
    $result = $instanceReport->getFiltros();    
    session_start();
    if(isset($_SESSION["usuario"])){ 
?>

<div class="card">
    <div class="header">
        <h2 class="text_color"><i class="fas fa-arrow-up"></i></i> Reporte Niveles <small>Por favor elige uno o varios filtros</small></h2>
    </div>
    <div class="body">
        <form id="formSearch">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">                        
                        <input id="inputTipo" type="checkbox" value="1" class="filter filled-in"><label for="inputTipo"><b>Nivel</b></label>
                    </div>                    
                </div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 divFilter">
                    <div class="input-group">
                        <div class="form-line">
                            <select id="selectTipo" title=" -- Todos los niveles -- " class="form-control show-tick" multiple data-live-search="true">
                                <?php if(isset($result['niveles'])){ ?>
                                    <?php foreach ($result['niveles'] as $key) { ?>
                                    <option value="<?php echo $key['id_nivel'] ?>"><?php echo $key['nombre'] ?></option>
                                    <?php }?>
                                <?php }?>
                            </select>                    
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <a id="btnConsultar" href="javascript:;" class="btn btn_color">Consultar</a>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="divTable" class="card">
    <div class="header clearfix">
        <div class="pull-left"><h2>Tabla de Resultados</h2></div>
    </div>
    <div class="body divTable">        
        <div class="table-responsive">
            <table id="tableResult" class="table table-bordered table-striped table-hover" style="width:100%"></table>
        </div>                    
    </div>
</div>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="../../inc/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<script src="../../inc/js/pages/tables/jquery-datatable.js"></script>

<!-- Moment Plugin Js -->
<script src="../../inc/plugins/momentjs/moment.js"></script>
<script src="../../inc/plugins/momentjs/moment-with-locale.js"></script>

<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="../../inc/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<script src='../../inc/js/reporte_niveles.js'></script>
<?php } ?>