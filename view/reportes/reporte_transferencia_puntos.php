<?php
    include_once '../../inc/parametros.php';
    include_once '../../controller/reportes/c_reporte_transferencia_puntos.php';    
    $instanceReport=new C_reporte_transferencia_puntos($ser,$usu,$pas,$bd);    
    $result = $instanceReport->getFiltros();    
    session_start();
    if(isset($_SESSION["usuario"])){ 
?>

<div class="card">
    <div class="header">
        <h2 class="text_color"><i class="fas fa-exchange-alt"></i></i> Reporte de Transferencia de Puntos <small>Por favor elige uno o varios filtros</small></h2>
    </div>
    <div class="body">
        <form id="formSearch">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">                        
                        <input id="inputSegmento" type="checkbox" value="1" class="filter filled-in"><label for="inputSegmento"><b>Segmento</b></label>
                    </div>                    
                </div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 divFilter">
                    <div class="input-group">
                        <div class="form-line">
                            <select id="selectSegmento" title=" -- Elige uno o más segmentos -- " class="form-control show-tick" data-live-search="true" multiple>
                                <?php 
                                if(isset($result['segmentos'])){
                                    foreach ($result['segmentos'] as $key) { ?>
                                        <option value="<?php echo $key['id_segmento'] ?>"><?php echo $key['nombre_segmento'] ?></option>
                                <?php } }
                                ?>
                            </select>                    
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <input id="inputNivel" type="checkbox" value="1" class="filter filled-in"><label for="inputNivel"><b>Nivel</b></label>
                    </div>                    
                </div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 divFilter">
                    <div class="input-group">
                        <div class="form-line">
                            <select id="selectNivel" title=" -- Elige uno o más niveles -- " multiple class="form-control show-tick" data-live-search="true">
                                <?php if(isset($result['niveles']) ){ ?>
                                    <?php foreach ($result['niveles'] as $key) { ?>
                                    <option value="<?php echo $key['id_nivel'] ?>"><?php echo $key['nombre'] ?></option>
                                    <?php }?>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <input id="inputPV" type="checkbox" value="1" class="filter filled-in"><label for="inputPV"><b>Punto de Venta</b></label>
                    </div>                    
                </div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 divFilter">
                    <div class="input-group">
                        <div class="form-line">
                            <select id="selectPV" title=" -- Elige uno o más Puntos de Venta -- " multiple class="form-control show-tick" data-live-search="true">
                                <?php if(isset($result['puntos_venta'])){ ?>
                                    <?php foreach ($result['puntos_venta'] as $key) { ?>
                                    <option value="<?php echo $key['id_usuario'] ?>"><?php echo $key['usuario'] ?></option>
                                    <?php }?>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <input id="inputUsuario" type="checkbox" value="1" class="filter filled-in"><label for="inputUsuario"><b>Usuario</b></label>
                    </div>                    
                </div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 divFilter">
                    <div class="input-group">
                        <div class="form-line">
                            <select id="selectUsuario" title=" -- Elige uno o más usuarios -- " multiple class="form-control show-tick" data-live-search="true">
                                <?php if(isset($result['usuarios'])){ ?>
                                    <?php foreach ($result['usuarios'] as $key) { ?>
                                    <option value="<?php echo $key['id_usuario'] ?>"><?php echo $key['nombre'] ?></option>
                                    <?php }?>
                                <?php }?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">                        
                        <input id="inputFechaRegistro" type="checkbox" value="1" class="filter filled-in"><label for="inputFechaRegistro"><b>Fecha de Transferencia</b></label>
                    </div>                    
                </div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 divFilter">                    
                    <div class="input-group">                        
                        <span class="input-group-addon">
                            <i class="material-icons">date_range</i>
                        </span>
                        <div class="form-line">
                            <input id="desde" type="text" class="form-control date datetimepicker" placeholder="Desde" /><span id="textDesde" style="color:red"></span>
                        </div>
                    </div>
                    <div class="input-group">                        
                        <span class="input-group-addon">
                            <i class="material-icons">date_range</i>
                        </span>
                        <div class="form-line">
                            <input id="hasta" type="text" class="form-control date datetimepicker" placeholder="Hasta" /><span id="textHasta" style="color:red"></span>
                        </div>
                    </div>
                </div>
            </div>            
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <a id="btnConsultar" href="javascript:;" class="btn btn_color">Consultar</a>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="divTable" class="card">
    <div class="header clearfix">
        <div class="pull-left"><h2>Tabla de Resultados</h2></div>        
    </div>
    <div class="body divTable">        
        <div class="table-responsive">
            <table id="tableResult" class="table table-bordered table-striped table-hover" style="width:100%"></table>
        </div>                    
    </div>
</div>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="../../inc/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<script src="../../inc/js/pages/tables/jquery-datatable.js"></script>
<script src="../../inc/js/pages/tables/jquery-datatable.js"></script>

<!-- Moment Plugin Js -->
<script src="../../inc/plugins/momentjs/moment.js"></script>
<script src="../../inc/plugins/multi-select/js/jquery.multi-select.js"></script>

<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="../../inc/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

<script src='../../inc/js/reporte_transferencia_puntos.js'></script>
<script type="text/javascript">

    $('.openBtn').on('click',function(){
        $('.modal-body').load('../../view/administracion/admin_segmento.php',function(){
            $('#myModal').modal({show:true});
        });
    });
</script>
<?php } ?>