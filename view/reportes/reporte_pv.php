<?php
    include_once '../../inc/parametros.php';
    include_once '../../controller/reportes/c_reporte.php';
    $instanceReport=new C_reporte($ser,$usu,$pas,$bd);
    $result = $instanceReport->getFiltros(2);    
    session_start();
    if(isset($_SESSION["usuario"])){ 
?>

<div class="card">
    <div class="header">
        <h2 class="text_color"><i class="fas fa-store-alt"></i></i> Reporte - Punto de Venta <small>Por favor elige uno o varios filtros</small></h2>
    </div>
    <div class="body">
        <form id="formSearch">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">                        
                        <input id="inputPV" type="checkbox" value="1" class="filter filled-in"><label for="inputPV"><b>Punto de Venta</b></label>
                    </div>                    
                </div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 divFilter">
                    <div class="input-group">
                        <div class="form-line">
                            <select id="selectPV" title=" -- Todos los puntos de venta -- " class="form-control show-tick" multiple>                                
                                <?php foreach ($result['puntosVenta'] as $key) { ?>
                                <option value="<?php echo $key['id_usuario'] ?>"><?php echo $key['usuario'] ?></option>
                                <?php }?>
                            </select>                    
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="row" id="divSegmento">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">                        
                        <input id="inputSegmento" type="checkbox" value="1" class="filter filled-in"><label for="inputSegmento"><b>Segmento</b></label>
                    </div>                    
                </div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 divFilter">
                    <div class="input-group">
                        <div class="form-line">
                            <select id="selectSegmento" title=" -- Elige uno o más segmentos --" class="form-control show-tick" data-live-search="true" multiple>                                
                                <?php foreach ($result['segmentos'] as $key) { ?>
                                <option value="<?php echo $key['idSegmento'] ?>"><?php echo $key['nombreSegmento'] ?></option>
                                <?php }?>
                            </select>                    
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">                        
                        <input id="inputTipoPV" type="checkbox" value="1" class="filter filled-in"><label for="inputTipoPV"><b>Tipo</b></label>
                    </div>                    
                </div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 divFilter">
                    <div class="input-group">
                        <div class="form-line">
                            <select id="selectTipo" class="form-control show-tick" >
                                <option value=""> -- Todos los tipos -- </option>
                                <?php foreach ($result['tipos'] as $key) { ?>
                                <option value="<?php echo $key['idTipo'] ?>"><?php echo $key['nombreTipo'] ?></option>
                                <?php }?>
                            </select>                    
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">                        
                        <input id="inputFechaRegistro" type="checkbox" value="1" class="filter filled-in"><label for="inputFechaRegistro"><b>Fecha de Registro</b></label>
                    </div>                    
                </div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 divFilter">                    
                    <div class="input-group">                        
                        <span class="input-group-addon">
                            <i class="material-icons">date_range</i>
                        </span>
                        <div class="form-line">
                            <input id="desde" type="text" class="form-control date datetimepicker" placeholder="Desde" /><span id="textDesde" style="color:red"></span>
                        </div>
                    </div>
                    <div class="input-group">                        
                        <span class="input-group-addon">
                            <i class="material-icons">date_range</i>
                        </span>
                        <div class="form-line">
                            <input id="hasta" type="text" class="form-control date datetimepicker" placeholder="Hasta" /><span id="textHasta" style="color:red"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">                        
                        <input id="inputSemana" type="checkbox" value="1" class="filter filled-in"><label for="inputSemana"><b>Dia(s) de Registro</b></label>
                    </div>                    
                </div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 divFilter">
                    <div class="input-group">
                        <div class="form-line">                            
                            <select title=" -- Todos los días --"  id="selectSemana" class="selectpicker form-control show-tick" multiple>
                                <option value="Lunes">Lunes</option>
                                <option value="Martes">Martes</option>
                                <option value="Miercoles">Miercoles</option>
                                <option value="Jueves">Jueves</option>
                                <option value="Viernes">Viernes</option>
                                <option value="Sabado">Sabado</option>
                                <option value="Domingo">Domingo</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">                        
                        <input id="inputTicketPromedio" type="checkbox" value="1" class="filter filled-in"><label for="inputTicketPromedio"><b>Monto Ticket</b></label>
                    </div>                    
                </div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 divFilter">
                    <div class="input-group">
                        <div class="form-line">
                            <p><b>Monto Minimo - Monto Máximo</b></p>
                            <div id="slideTicketPromedio"></div>
                            <div class="m-t-20 font-12"><b>Valores: </b><span class="js-nouislider-value"></span></div>
                        </div>
                    </div>
                </div>
            </div>
            <?php if( isset($result['tipoProyecto']) && $result['tipoProyecto'] != 3){ ?>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">                        
                        <input id="inputPuntos" type="checkbox" value="1" class="filter filled-in"><label for="inputPuntos"><b>Puntos</b></label>
                    </div>                    
                </div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 divFilter">
                    <div class="input-group">
                        <div class="form-line">
                            <p><b>Pts Mínimos - Pts Máximos</b></p>
                            <div id="slidePuntos"></div>
                            <div class="m-t-20 font-12"><b>Valores: </b><span class="js-nouislider-value"></span></div>
                        </div>
                    </div>
                </div>
            </div>
            <?php }else {?>
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">                        
                        <input id="inputVisitas" type="checkbox" value="1" class="filter filled-in"><label for="inputVisitas"><b>Visitas</b></label>
                    </div>                    
                </div>
                <div class="col-lg-9 col-md-6 col-sm-12 col-xs-12 divFilter">
                    <div class="input-group">
                        <div class="form-line">
                            <p><b>Visita Mínima - Visita Máxima</b></p>
                            <div id="slideVisitas"></div>
                            <div class="m-t-20 font-12"><b>Valores: </b><span class="js-nouislider-value"></span></div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <a id="btnConsultar" href="javascript:;" class="btn btn_color">Consultar</a>
                </div>
            </div>
        </form>
    </div>
</div>
<div id="ajaxResult"></div>


<script src="../../inc/plugins/bootstrap-select/js/bootstrap-select.js"></script>
<script src="../../inc/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="../../inc/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="../../inc/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>
<script src="../../inc/js/pages/tables/jquery-datatable.js"></script>

<!-- Moment Plugin Js -->
<script src="../../inc/plugins/momentjs/moment.js"></script>
<script src="../../inc/plugins/momentjs/moment-with-locale.js"></script>

<!-- Bootstrap Material Datetime Picker Plugin Js -->
<script src="../../inc/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>


<script src='../../inc/js/reporte_pv.js'></script>

<?php } ?>