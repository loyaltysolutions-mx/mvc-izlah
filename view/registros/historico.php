
<?php 
include_once '../../inc/cont_fijos3.php';
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
include_once '../../controller/registros/c_registros.php';
$ins_cont_fijos=new Cont_fijos($ser,$usu,$pas,$bd);
$ins_control_registros=new C_registros($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
session_start();
if(isset($_SESSION["usuario"])){ 
 $res_config_proy=$ins_funciones->consulta_generica('tbl_configuracion_proyecto', ' ');
 $reg_config= mysqli_fetch_assoc($res_config_proy);    
?>

<script type="text/javascript" src='../../inc/js/registro.js'></script>
<!-- CONTENIDO -->
    <div class="card">
        <div class="header">
            <h2><i class="fas fa-history"></i> Historial de movimientos <small>Muestra una descripción de las transacciones realizadas por el usuario </small></h2>
        </div>
        <div class="body">
            <div class="table-responsive">
                <?php 
                    //GENERAMOS CONTENIDO DE TABLA
                    $res=$ins_control_registros->trae_historial($_SESSION["id_usuario"]);

                    $arr_reg=array();
                    $arr_ptos=array();
                    $x=0;
                    while($fila = $res->fetch_assoc()){
						
						//VERIFICAMOS SI ES PUNTOS O VISITAS
						if($reg_config['id_tipo_proyecto']==3){//VISITAS
							//VERIFICAMOS SI ES + - 
                              if($fila['id_tipo_registro']==2){
                                    $arr_ptos[$x]='-'.$fila['num_visita'];
                                    $signo='-';
                                   }else{
                                    $arr_ptos[$x]='+'.$fila['num_visita'];
                                    $signo='+';
                                        }
                            $arr_reg[$x]=array(
                                date('d-m-Y',strtotime( $fila['fecha_registro'])),
                                utf8_encode(ucwords($fila['nombre'])).'  '.utf8_encode(ucwords($fila['nombre_premio'])).' - '. utf8_encode(ucwords($fila['usuario'])),
                                $fila['num_visita'],
                                $signo,
                                $ins_cont_fijos->saldo($arr_ptos)
                                        );
							
						}else{//PUNTOS
						//VERIFICAMOS SI ES + - 
						  if($fila['id_tipo_registro']==2){
								$arr_ptos[$x]='-'.$fila['puntos'];
								$signo='-';
							   }else{
								$arr_ptos[$x]='+'.$fila['puntos'];
								$signo='+';
									}
							$arr_reg[$x]=array(
								date('d-m-Y',strtotime( $fila['fecha_registro'])),
								utf8_encode(ucwords($fila['nombre'])).'  '.utf8_encode(ucwords($fila['nombre_premio'])).' - '. utf8_encode(ucwords($fila['usuario'])),
								$fila['puntos'],
								$signo,
								$ins_cont_fijos->saldo($arr_ptos)
                                        );
						}
                        $x++; 
                    }
                ?>
                <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                    <thead>
                        <tr>
                            <th>Nro.</th>
                            <th>Fecha de Actividad</th>
                            <th>Actividad</th>
                            <?php 
							if($reg_config['id_tipo_proyecto']==3){
								//MOSTRAMOS CAMPOS PARA VISITAS ?>
								<th>Visitas Realizadas/Utilizadas</th>
								<th>Visitas Totales</th>
							<?php }else{
								//MOSTRAMOS CAMPOS PARA PUNTOS ?>
								<th>Puntos Recibidos/Redimidos</th>
								<th>Saldo de Puntos </th>
							<?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        //TRAEMOS HISTORIAL DE USUARIO                        
                        for($i=0; $i < count($arr_reg); $i++){ ?>
                            <tr>                               
                                <td><?php echo $i+1; ?></td>
                                <td><?php echo $arr_reg[$i][0]; ?></td>
                                <td><?php  echo $arr_reg[$i][1]; ?></td>
                                <td><?php  
                                    if($arr_reg[$i][3]=='+'){
                                         echo $arr_reg[$i][3].$arr_reg[$i][2];
                                        }else{
                                                echo '<span style="color:red">'.$arr_reg[$i][3].$arr_reg[$i][2].'</span>';
                                             }
                                ?></td>
                                <td><?php echo  '<b>'.$arr_reg[$i][4].'<b>'; ?></td>                                   
                            </tr>
                        <?php }  ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

<?php
include_once '../../inc/datatables.php';
?>
 <?php }else{
      $redirec= "../../" ;
      header('Location:'.$redirec );
 }