<?php 
/*************************************************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#- Vista de Home -#
##################################################################################### 
include_once '../../inc/cont_fijos.php';
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
include_once '../../controller/registro/c_registro.php';
$ins_control_registro=new C_registro($ser,$usu,$pas,$bd);
$ins_cont_fijos=new Contenidos_Fijos($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
  
<?php  
 $ins_cont_fijos->head();
 //TRAEMOS CONFIGURACION DE ESTILOS
 $res_con=$ins_funciones->consulta_generica('tbl_estilo', ' ');
 $registro= mysqli_fetch_assoc($res_con);
 //VALIDAMOS LOGO
if($registro['logo_cte']==''){
    $img_logo='imagenes/logo.png';
}else{
    $img_logo='imagenes/img_configuracion/'.$registro['logo_cte'];
}
 //VALIDAMOS IMAGEN FONDO
if($registro['fondo_cte']==''){
     $img_background='imagenes/background_default.jpg';
}else{
    $img_background='imagenes/img_configuracion/'.$registro['fondo_cte'];
}
 //VALIDAMOS COLOR PRIMARIO
if($registro['color_primario']==''){
     $color_primario='#006AA9';
}else{
    $color_primario=$registro['color_primario'];
}
 //VALIDAMOS COLOR SECUNDARIO
if($registro['color_secundario']==''){
     $color_secundario='#006AA9';
}else{
    $color_secundario=$registro['color_secundario'];
}
  ?>
<!--script src="https://code.jquery.com/jquery-3.4.0.min.js" integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg=" crossorigin="anonymous"></script-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css" >
<!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css.map"-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" >
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" >

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.9/css/bootstrap-select.css" >

<script src="../../inc/js/sweet-alert2.js"></script>


<style>
    .preloader{
        width: 100px !important;
        height: 100px !important;
    }

    .lds-dual-ring {
        display: inline-block;
        width: 64px;
        height: 64px;
    }
    .lds-dual-ring:after {
        content: " ";
        display: block;
        width: 46px;
        height: 46px;
        margin: 1px;
        border-radius: 50%;
        border: 5px solid #fff;
        border-color: <?php echo $color_secundario ?> transparent <?php echo $color_secundario ?> transparent;
        animation: lds-dual-ring 1.2s linear infinite;
    }
    @keyframes lds-dual-ring {
        0% {
            transform: rotate(0deg);
        }
        100% {
            transform: rotate(360deg);
        }
    }

</style>
</head>
<?php
//LLAMAMOS ESTILOS DE CONFIGURACION
$ins_cont_fijos->estilos($img_logo,$img_background,$color_primario,$color_secundario);
$token= isset($_GET['token']) ? $_GET['token'] : null;
if($token!=''){ 
    $token = trim(addslashes(htmlspecialchars($token)));
    $res_token=$ins_funciones->consulta_generica("tbl_usuario", " where token='".$token."'");
    $registro_token= mysqli_fetch_assoc($res_token);
    
 if(count($registro_token)>0){ ?>
<body class="theme-red">
    <!-- Pag. Loading-->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Espera un poco...</p>
        </div>
    </div>
    <!-- Barra Ariba -->
    <nav class="navbar" style='height: 70px;'> 
        <div class="container-fluid">
            <div class="navbar-header">
                <span class="navbar-brand" >Plataforma Lealtad </span>
            </div>
        </div>
    </nav>
<!-- CONTENIDO -->
    <div class="container-fluid" style="margin-top:85px">        
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2><i class="far fa-id-badge"></i> RECUPERACION DE CONTRASEÑA <small> Por favor llena los siguientes campos</small></h2>                            
                    </div>
                    <div class="body">
                        <form id="formRecovery">                            
                            <input  type='hidden' id='idUsuario' name="idUsuario" value="<?php  echo $registro_token['id_usuario']; ?>">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">                                
                                    <input type="text" class="form-control" name="nombre" readonly value="<?php  echo $registro_token['nombre']; ?>" />
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">phone_android</i>
                                </span>
                                <div class="form-line">                                
                                    <input type="text" class="form-control" readonly value="<?php  echo $registro_token['telefono']; ?>" />
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">                                
                                    <input type="password" class="form-control" id='pass' name="password" style="font-size:14px" placeholder="Ingresa una nueva contraseña" required />
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">                                
                                    <input type="password" class="form-control" id='repass' name="repassword" style="font-size:14px" placeholder="Escribe nuevamente la nueva contraseña" required />
                                </div>
                            </div>
                            <button id="btnSubmit" type="submit" class="btn btn_color m-t-15 waves-effect"> Confirmar </button>
                            <div id="loadAnimation" style="display:none; width:100%; text-align:center"><div class="lds-dual-ring"></div></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
	$("#formRecovery").on("submit", function(e){
		e.preventDefault();
		let pass = $("#pass").val();
		let repass = $("#repass").val();
		if(pass == repass){
			 $("#btnSubmit").fadeOut(500, function(){
			
				var link = "../../controller/valida/c_llamadas_ajax.php";
	            var data = $("#formRecovery").serialize()+"&op=3";
	            var beforeFunction = function(){
	                $("#loadAnimation").fadeIn();
	            };
	            var successFunction = function(data){
	                if(data.result){
                        switch(data.result){
                            case 1:                                
                                Swal("Éxito", "Se ha restablecido correctamente la contraseña, en breve recibirás una notificacion de confirmación", "success").then((value) => {window.location="../../"});                                
                            break;

                            case 2:
                                Swal("Error", "Los datos ingresados no corresponden a un usuario registrado. Intenta nuevamente", "error");
                                $("#loadAnimation").hide();
                                $("#btnSubmit").show();
                            break;

                            case 3:
                                Swal("Error", "Se ha restablecido la contraseña correctamente pero no se pudo enviar notificación", "error").then((value) => {window.location="../../"});
                                $("#loadAnimation").hide();
                                $("#btnSubmit").show();
                            break;

                            case 4:
                                Swal("Error", "Datos incompletos, llena todos los campos solicitados e intenta nuevamente", "error");
                                $("#loadAnimation").hide();
                                $("#btnSubmit").show();
                            break;
                        }
                    }
	            };

	            var failFunction = function(data){
	                $("#loadAnimation").hide();
	                $("#btnSubmit").show();
	                Swal("Error de Conexión", "Ocurrio un error al guardar tu información", "error");
	            };

	            connectServer(link, data, beforeFunction, successFunction, failFunction);
	        });
		}else{
			Swal("Error", "Las contraseñas no coinciden. Intenta nuevamente", "error");
		}
	});

	function connectServer(link, data, beforeFunction, successFunction, failFunction){
        $.ajax({
            type: "POST",
            timeout: 7000,
            url: link,
            data: data,
            dataType: "json",
            beforeSend : function(){
                beforeFunction && beforeFunction();
            }
        }).done(function(data){
            successFunction && successFunction(data);
            console.log(data);
        }).fail(function(data){
            failFunction && failFunction(data);
            console.log(data);
        });
    }
</script>
</body>
</html> 
    <?php 
 }else{ ?>
   <body class="four-zero-four">
        <div class="four-zero-four-container">
            <div class="error-code"><i class="fas fa-bomb"></i></div><br>
            <div class="error-message">Token inexistente</div>
        </div>
    </body>
<?php  } 
 }else{ ?>
    <body class="four-zero-four">
        <div class="four-zero-four-container">
            <div class="error-code"><i class="fas fa-bomb"></i></div><br>
            <div class="error-message">Faltan Parametros</div>
        </div>
    </body>
<?php }
?>
