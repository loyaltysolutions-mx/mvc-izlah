<?php 
/*************************************************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#- Vista de Home -#
##################################################################################### 
include_once '../../inc/cont_fijos.php';
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
include_once '../../controller/registro/c_registro.php';
$ins_control_registro=new C_registro($ser,$usu,$pas,$bd);
$ins_cont_fijos=new Contenidos_Fijos($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
session_start();
?>
<!DOCTYPE html>
<html lang="es">
<head>
  
<?php  
 $ins_cont_fijos->head();
 //TRAEMOS CONFIGURACION DE ESTILOS
 $res_con=$ins_funciones->consulta_generica('tbl_estilo', ' ');
 $registro= mysqli_fetch_assoc($res_con);
 //VALIDAMOS LOGO
if($registro['logo_cte']==''){
    $img_logo='imagenes/logo.png';
}else{
    $img_logo='imagenes/img_configuracion/'.$registro['logo_cte'];
}
 //VALIDAMOS IMAGEN FONDO
if($registro['fondo_cte']==''){
     $img_background='imagenes/background_default.jpg';
}else{
    $img_background='imagenes/img_configuracion/'.$registro['fondo_cte'];
}
 //VALIDAMOS COLOR PRIMARIO
if($registro['color_primario']==''){
     $color_primario='#006AA9';
}else{
    $color_primario=$registro['color_primario'];
}
 //VALIDAMOS COLOR SECUNDARIO
if($registro['color_secundario']==''){
     $color_secundario='#006AA9';
}else{
    $color_secundario=$registro['color_secundario'];
}
  ?>
<!--script src="https://code.jquery.com/jquery-3.4.0.min.js" integrity="sha256-BJeo0qm959uMBGb65z40ejJYGSgR7REI4+CW1fNKwOg=" crossorigin="anonymous"></script-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css" >
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker-standalone.min.css.map">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css" >
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" >

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.9/css/bootstrap-select.css" >

<script src="../../inc/js/sweet-alert2.js"></script>

<script src="../../inc/plugins/momentjs/moment.js"></script>
<script src="../../inc/plugins/momentjs/moment-with-locale.js"></script>    

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" src="../../inc/js/valida.js"></script>

<?php
//LLAMAMOS ESTILOS DE CONFIGURACION
$ins_cont_fijos->estilos($img_logo,$img_background,$color_primario,$color_secundario);
$token=$_GET['token'];
if($token!=''){ 
    $token = trim(addslashes(htmlspecialchars($token)));
    $res_token=$ins_funciones->consulta_generica("tbl_usuario", " where token='".$token."'");
    $registro_token= mysqli_fetch_assoc($res_token);

    $campos_registro=$ins_control_registro->trae_campos();
    $ultima_tarjeta=$ins_funciones->consulta_generica("tbl_rel_usuario_tarjeta"," WHERE id_usuario = ".$registro_token['id_usuario']." ORDER BY id_tarjeta DESC ");
    //print_r($ultima_tarjeta);
    //die();
    $rowTarjeta = mysqli_fetch_assoc($ultima_tarjeta);


 if(count($registro_token)>0){ ?>
</head>
<body class="theme-red">
    <!-- Pag. Loading-->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Espera un poco...</p>
        </div>
    </div>
    <!-- Barra Ariba -->
    <nav class="navbar" style='height: 70px;'> 
        <div class="container-fluid">
            <div class="navbar-header">
                <span class="navbar-brand" >Plataforma Lealtad </span>
            </div>
        </div>
    </nav>
<!-- CONTENIDO -->
    <div class="container-fluid" style="margin-top:85px">        
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2><i class="far fa-id-badge"></i> CONFIRMACION DE REGISTRO <small>Llena los campos solicitados</small></h2>                            
                    </div>
                    <div class="body">
                        <form id="form_confirma_registro">
                            <!--div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-group"-->                                
                                    <input type="hidden" readonly class="form-control" id='usuario' name="usuario" placeholder="Ingresa un usuario" value="<?php echo $registro_token['usuario'] ?>"  required>
                                    <input  type='hidden' id='id_usu' name="id_usu" value="<?php  echo $registro_token['id_usuario']; ?>">
                                <!--/div>                    
                            </div-->
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-group">                                
                                    <input type="password" class="form-control" id='pass' name="password" style="font-size:14px" placeholder="Ingresa una contraseña para tu cuenta" required>                                    
                                </div>                    
                            </div>
                        <?php while ($row = mysqli_fetch_assoc($campos_registro)) { ?>
                            <?php if($row['id_campo'] == 1){ ?>                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-group">                                
                                    <input type="text" id='nombre' class="form-control"  name='nombre' style="font-size:14px" required value="<?php  echo $registro_token['nombre']; ?>" <?php  echo !empty($registro_token['nombre']) ? 'readonly' : ''; ?> placeholder="Ingresa tu nombre" autocomplete="off">
                                </div>                    
                            </div>
                            <?php } ?>

                            <?php if($row['id_campo'] == 3){ ?>                            
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">phone</i>
                                </span>
                                <div class="form-group">
                                    <input type="tel" id='telefono' class="form-control"  name='telefono' style="font-size:14px" placeholder="Ingresa tu celular" value="<?php  echo $registro_token['telefono']; ?>" <?php  echo !empty($registro_token['telefono']) ? 'readonly' : ''; ?> autocomplete="off" required >
                                </div>                    
                            </div>                            
                            <?php } ?>
                            
                            <?php if($row['id_campo'] == 6){ ?>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">text_format</i>
                                </span>
                                <div class="form-group">
                                    <input type="text" id='rfc' class="form-control"  name='rfc' style="font-size:14px" value="<?php  echo $registro_token['rfc']; ?>" <?php  echo !empty($registro_token['rfc']) ? 'readonly' : ''; ?> required placeholder="Ingresa tu RFC" autocomplete="off">
                                </div>                    
                            </div>
                            <?php } ?>
                            
                            <?php if($row['id_campo'] == 7){ ?>
                            <!--div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">credit_card</i>
                                </span>
                                <div class="form-group">
                                    <input type="tarjeta" id='tarjeta' class="form-control" style="font-size:14px" value="<?php  echo isset($rowTarjeta['tarjeta']) ? $rowTarjeta['tarjeta'] : '' ; ?>" <?php  echo !empty($registro_token['tarjeta']) ? 'readonly' : ''; ?> name='tarjeta' required placeholder="Ingresa tu ID de tarjeta">
                                </div>                    
                            </div-->
                            <?php } ?>
                        <?php } ?>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">mail</i>
                                </span>
                                <div class="form-group">
                                    <input type="email" id='email' class="form-control" name='email' style="font-size:14px" value="<?php  echo $registro_token['email']; ?>" <?php  echo !empty($registro_token['email']) ? 'readonly' : ''; ?> required placeholder="Ingresa un correo electrónico" autocomplete="off">
                                </div>                    
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">wc</i>
                                </span>
                                <div class="form-group">
                                    <select id="selectGenero" name="genero" required data-width="100%">
                                        <option value=""> -- Elige un género -- </option>
                                        <option value="1" <?php  echo $registro_token['genero'] == 1 ? 'selected' : ''; ?> >Hombre</option>
                                        <option value="2" <?php  echo $registro_token['genero'] == 2 ? 'selected' : ''; ?> >Mujer</option>
                                    </select>
                                </div>                    
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">cake</i>
                                </span>
                                <div class="form-group">
                                    <input type="text" placeholder="Fecha de Nacimiento" name="fecha_nacimiento" style="font-size:14px" class="form-control" id='fechaNacimiento' value="<?php  echo $registro_token['fecha_nacimiento']; ?>" autocomplete="off">
                                </div>                    
                            </div>
                            <div class="row clearfix">
                                <div class="col-xs-12 col-md-12 col-lg-12 col-sm-12">
                                    <div id='loading'><img src="../../inc/imagenes/load.gif" style="display: none;" /></div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn_color m-t-15 waves-effect"> Finalizar Registro </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>        
    </div>    
    <!-- Moment Plugin Js -->
    <script type="text/javascript">        
        //$("#selectGenero").selectpicker();
        $("#fechaNacimiento").datetimepicker({
            locale : 'es',
            format : 'YYYY-MM-DD'
        });
        //console.log("ho");
    </script>
</body>
</html> 
    <?php 
 }else{ ?>
   <body class="four-zero-four">
        <div class="four-zero-four-container">
            <div class="error-code"><i class="fas fa-bomb"></i></div><br>
            <div class="error-message">Token inexistente</div>
        </div>
    </body>
<?php  } 
 }else{ ?>
    <body class="four-zero-four">
        <div class="four-zero-four-container">
            <div class="error-code"><i class="fas fa-bomb"></i></div><br>
            <div class="error-message">Faltan Parametros</div>
        </div>
    </body>
<?php }
?>
