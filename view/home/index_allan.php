<?php

include_once '../../inc/cont_fijos3.php';
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
//$inst_control=new C_home($ser,$usu,$pas,$bd);
$ins_cont_fijos=new Cont_fijos($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
session_start();
if(isset($_SESSION["usuario"])){ 
?>
<!DOCTYPE html>
<html lang="es">
    <head>
      <?php  
        $ins_cont_fijos->head();

        //TRAEMOS CONFIGURACION DE ESTILOS
        $res_con = $ins_funciones->consulta_generica('tbl_estilo', ' ');
        $registro = mysqli_fetch_assoc($res_con);
        //VALIDAMOS LOGO
        if($registro['logo_cte']==''){
          $img_logo='imagenes/logo.png';
        }else{
          $img_logo='imagenes/img_configuracion/'.$registro['logo_cte'];
        }
        //VALIDAMOS IMAGEN FONDO
        if($registro['fondo_cte']==''){
          $img_background='imagenes/background_default.jpg';
        }else{
          $img_background='imagenes/img_configuracion/'.$registro['fondo_cte'];
        }
        //VALIDAMOS COLOR PRIMARIO
        if($registro['color_primario']==''){
         $color_primario='#006AA9';
        }else{
         $color_primario=$registro['color_primario'];
        }
        //VALIDAMOS COLOR SECUNDARIO
        if($registro['color_secundario']==''){
         $color_secundario='#006AA9';
        }else{
         $color_secundario=$registro['color_secundario'];
        }
        $ins_cont_fijos->estilos($img_logo,$img_background,$color_primario,$color_secundario);
      ?>
    </head>
  <body id="body" class="theme-blue"> 
      <?php 
        $ins_cont_fijos->header();
      ?>
    <div class="container-fluid">
      <div class="row">
        <?php  
             $id_usu=$_SESSION["idusuario"];
             $ins_cont_fijos->menu($_SESSION["id_usuario"]);
        ?>
      </div>
    </div>
    <section class="content">
        <div id="all-content" class="row container-fluid">
          <div id="card-content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          </div>  
            <?php 
            switch ($_SESSION["rol"]){
            //VISTA ADMINISTRADOR  
                case 1:
                    include './admin.php';
                break;
            //VISTA PUNTO DE VENTA
                case 2:
                    echo 'vista  inicio punto de venta';
                break;
            //VISTA USUARIO
                case 3:
                   include './usuario.php';
                break;
                default:
                    echo ':(';
                break;
                       } 
            ?>     
        </div>
    </section>
  </body>
  <?php  
    $ins_cont_fijos->footer();
  ?>
</html>

<?php 
    }else{
      $redirec= "../../" ;
      header('Location:'.$redirec );
    }