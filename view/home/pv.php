<?php 
session_start();
if(isset($_SESSION["usuario"])){ 
$ins_funciones=new Funciones_Basicas();
$id_usu_session=$_SESSION["id_usuario"];
//CONSULTAMOS TIPO DE PROYECTO PUNTOS O VISITAS
$qr_tipo_proyecto=$ins_funciones->consulta_generica_all('select id_tipo_proyecto  from  tbl_configuracion_proyecto');
$config_proy= mysqli_fetch_assoc($qr_tipo_proyecto);
//EVALUAMOS SI EL PROYECTO ES POR PUNTOS O POR VISITAS
switch($config_proy['id_tipo_proyecto']){
	case 1://PTOS
	case 2:
	$tit_acum='Puntos Acumulados';
	$tit_red='Puntos Redimidos';
		 //TRAEMOS INFORMACION DE USUARIO 
		 //CTES REGISTRADOS
		 $qr_ctes_registrados=$ins_funciones->consulta_generica_all('select count(*) as n from tbl_usuario  where rol=3 and activo=0  and id_usuario_registro='.$id_usu_session);
		 $res_qr_ctes_reg= mysqli_fetch_assoc($qr_ctes_registrados);
		 //PUNTOS ACUMULADOS
		 $qr_ptos_acumulados=$ins_funciones->consulta_generica_all('select sum(puntos)  as n  from tbl_registros where  id_tipo_registro=1 and  id_usuario_registro='.$id_usu_session );
		 $res_qr_acumula= mysqli_fetch_assoc($qr_ptos_acumulados);
		 //PUNTOS REDIMIDOS
		 $qr_ptos_redimidos=$ins_funciones->consulta_generica_all('select SUM(puntos) as n  from  tbl_registros where id_tipo_registro=2 and id_usuario_registro='.$id_usu_session);
		 $res_qr_redimidos= mysqli_fetch_assoc($qr_ptos_redimidos);
		  //TICKETS REGISTRADOS
		 $qr_ticketes_registrados=$ins_funciones->consulta_generica_all('select count(*) as n from tbl_registros where id_tipo_registro=1 and id_usuario_registro='.$id_usu_session);
		 $tickets_qr_registrados= mysqli_fetch_assoc($qr_ticketes_registrados);
	break;
	
	case 3://VISITAS
		$tit_acum='Visitas Acumulación';
		$tit_red='Visitas  de Redención';
	     //TRAEMOS INFORMACION DE USUARIO 
		 //CTES REGISTRADOS
		 $qr_ctes_registrados=$ins_funciones->consulta_generica_all('select count(*) as n from tbl_usuario  where rol=3 and activo=0 and id_usuario_registro='.$id_usu_session );
		 $res_qr_ctes_reg= mysqli_fetch_assoc($qr_ctes_registrados);
		 //VISITAS ACUMULADAS
		 $qr_visitas_acumuladas=$ins_funciones->consulta_generica_all('select count(*) as n from  tbl_registros where id_tipo_registro=1 and id_usuario_registro='.$id_usu_session );
		 $res_qr_acumula= mysqli_fetch_assoc($qr_visitas_acumuladas);
		 //VISITAS REDIMIDAS
		 $qr_visitas_redimidas=$ins_funciones->consulta_generica_all('select count(*) as n from  tbl_registros where id_tipo_registro=2 and id_usuario_registro='.$id_usu_session);
		 $res_qr_redimidos= mysqli_fetch_assoc($qr_visitas_redimidas);
		  //TICKETS REGISTRADOS
		 $qr_ticketes_registrados=$ins_funciones->consulta_generica_all('select count(*) as n from tbl_registros where id_tipo_registro=1 and id_usuario_registro='.$id_usu_session);
		 $tickets_qr_registrados= mysqli_fetch_assoc($qr_ticketes_registrados);
	break;
	default:
		echo 'ocurrio un problema';
	break;
	
}
?>
<div class="row clearfix">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box hover-zoom-effect">
            <div class="icon bg-pink">
               <i class="fas fa-users"></i>
            </div>
            <div class="content">
                <div class="text">Clientes  Registrados</div>
                <?php 
                    if($res_qr_ctes_reg['n']==''){
                        $resqrc = '0'; 
                    }else{ 
                        $resqrc = $res_qr_ctes_reg['n']; 
                    }
                ?>
                <div class="number count-to" data-from="0" data-to="<?php echo $resqrc ?>" data-speed="1000" data-fresh-interval="20"><?php echo $resqrc ?></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box hover-zoom-effect">
            <div class="icon bg-blue">
                <i class="fas fa-boxes"></i>
            </div>
            <div class="content">
                <div class="text"><?php  echo $tit_acum; ?></div>
                <?php 
                    if($res_qr_acumula['n']==''){
                        $resqr = '0'; 
                    }else{ 
                        $resqr = $res_qr_acumula['n']; 
                    }
                ?>
                <div class="number count-to" data-from="0"  data-to="<?php echo $resqr; ?>" data-fresh-interval="20">
                <?php echo $resqr; ?></div>
                 
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box hover-zoom-effect">
            <div class="icon bg-light-green">
                <i class="fas fa-gift"></i>
            </div>
            <div class="content">
                <div class="text"><?php  echo $tit_red; ?>  </div>
                <?php 
                    if($res_qr_redimidos['n']=='')
                        { 
                            $ptsqr = '0'; 
                        }else{ 
                            $ptsqr = $res_qr_redimidos['n']; 
                        }
                ?>
                <div class="number count-to" data-from="0"  data-to="<?php echo $ptsqr; ?>" data-fresh-interval="20"><?php echo $ptsqr; ?></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box hover-zoom-effect">
            <div class="icon bg-yellow">
                <i class="fas fa-tags"></i>
            </div>
            <div class="content">
                <div class="text">Tickets  Registrados</div>
               <?php 
                    if($tickets_qr_registrados['n']=='')
                        { 
                            $tksqr = '0'; 
                        }else{ 
                            $tksqr = $tickets_qr_registrados['n']; 
                        }
                ?>
                <div class="number count-to" data-from="0"  data-to="<?php echo $tksqr; ?>" data-fresh-interval="20"><?php echo $tksqr; ?></div>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="body">
    <!-- Edit Menù table -->
        <div class="row">
            
                <h5>Promociones Existentes</h5>
            
        </div>
        <hr>
        <div class="table-responsive">
            <table id="" class="table table-bordered table-striped table-hover dataTable js-exportable">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Detalle</th>
                        <th>Sucursal</th>
                    </tr>
                </thead>
                <tbody>
                        <?php 
						//CONSULTAMOS REGISTROS PARA GRID DE  PROMOS
						$qr_consulta_promos=$ins_funciones->consulta_generica_all('SELECT P.*,S.nombre as nombre_sucursal, SC.nombre as nombre_sucursal FROM tbl_rel_promo_suc_segmento as P 
                            inner join tbl_promociones as S on S.id_promociones=P.id_promo
                            inner join tbl_sucursales as SC on SC.id_sucursales=P.id_sucursal');
							while($fila = $qr_consulta_promos->fetch_assoc()){ ?>
								<tr>
									<td><?php echo utf8_encode($fila['nombre']); ?></td>
									<td><?php echo  utf8_encode($fila['detalle']); ?></td>
									<td><?php echo  utf8_encode($fila['nombre_sucursal']); ?></td>
								</tr>
							<?php }
						?>
                </tbody>
            </table>
        </div>
    </div>
</div>        
<?php 
 }else{
      $redirec= "../../" ;
      header('Location:'.$redirec );
 }
