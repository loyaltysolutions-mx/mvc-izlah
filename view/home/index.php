<?php
include_once '../../inc/cont_fijos3.php';
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
//$inst_control=new C_home($ser,$usu,$pas,$bd);
$ins_cont_fijos=new Cont_fijos($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
session_start();
if(isset($_SESSION["usuario"])){ 
?>
<!DOCTYPE html>
<html lang="es">
    <head>
      <?php  
        $ins_cont_fijos->head();

        //TRAEMOS CONFIGURACION DE ESTILOS
        $res_con = $ins_funciones->consulta_generica('tbl_estilo', ' ');
        $registro = mysqli_fetch_assoc($res_con);
        //VALIDAMOS LOGO
        if($registro['logo_cte']==''){
          $img_logo='imagenes/logo.png';
        }else{
          $img_logo='imagenes/img_configuracion/'.$registro['logo_cte'];
        }
        //VALIDAMOS IMAGEN FONDO
        if($registro['fondo_cte']==''){
          $img_background='imagenes/background_default.jpg';
        }else{
          $img_background='imagenes/img_configuracion/'.$registro['fondo_cte'];
        }
        //VALIDAMOS COLOR PRIMARIO
        if($registro['color_primario']==''){
         $color_primario='#006AA9';
        }else{
         $color_primario=$registro['color_primario'];
        }
        //VALIDAMOS COLOR SECUNDARIO
        if($registro['color_secundario']==''){
         $color_secundario='#006AA9';
        }else{
         $color_secundario=$registro['color_secundario'];
        }
      ?>

      <style type="text/css">
         .lds-dual-ring {
            display: inline-block;
            width: 64px;
            height: 64px;
        }
        .lds-dual-ring:after {
            content: " ";
            display: block;
            width: 46px;
            height: 46px;
            margin: 1px;
            border-radius: 50%;
            border: 5px solid #fff;
            border-color: <?php echo $color_secundario ?> transparent <?php echo $color_secundario ?> transparent;
            animation: lds-dual-ring 1.2s linear infinite;
        }
        @keyframes lds-dual-ring {
            0% {
                transform: rotate(0deg);
            }
            100% {
                transform: rotate(360deg);
            }
        }
      </style>
   
<?php
  //LLAMAMOS ESTILOS DE CONFIGURACION
  $ins_cont_fijos->estilos($img_logo,$img_background,$color_primario,$color_secundario);
?>  
 </head>
  <body id="body" class="theme-red"> 
      <?php 
        $ins_cont_fijos->header();
      ?>
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>                
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <?php  
             $id_usu=$_SESSION["idusuario"];
             $ins_cont_fijos->menu($_SESSION["id_usuario"],$img_logo);
        ?>
      </div>
    </div>
    <section class="content">
        <div id="all-content" class="row container-fluid">
          <div id="card-content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <?php 
            switch ($_SESSION["rol"]){
            //VISTA ADMINISTRADOR  
                case 1: ?>
                   <?php 
				   include './admin.php';
                break;
            //VISTA PUNTO DE VENTA
                case 2: 
                include './pv.php';
                break;
            //VISTA USUARIO
                case 3:
                    include './usuario.php';
                break;
                default:
                    echo ':(';
                break;
                       } 
            ?>
          </div>       
        </div>
    </section>
    <?php  
      $ins_cont_fijos->footer();
    ?>
  </body>
  
</html>

<?php 
    }else{
      $redirec= "../../" ;
      header('Location:'.$redirec );
    }