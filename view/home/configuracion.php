<?php
/**
 *  * ##+> ################################# <+##
 * EDICIÓN DE CONFIGURACION INICIAL DE LA PLATAFORMA DE LEALTAD
 * Desarrolador : Allan M.
 *  * ##+> ################################# <+##
 */
include_once '../../controller/configuracion/c_configuracion.php';
include_once '../../inc/parametros.php';
include_once '../../inc/funciones.php';
$ins_fun_basicas=new Funciones_Basicas();
$ins_control=new C_configuracion($ser,$usu,$pas,$bd);

?>
  <div id="config" class="card">
    <div class="container">
      <input type="hidden" name="data" id="data" value='<?php echo $ins_control->checks(); ?>'>
      <div id="register_form" method="post">

        <div id="5m" class="menus">
          <br>
          <h2> 
            <i class="far fa-id-badge"></i>¿Donde vivira el Programa?
          </h2>
          <hr>
          <div class="row">
            <div class="form-check customradio col-md-4">
              <input class="form-check-input" type="radio" id="movil" >
              <label class="form-check-label" for="movil">
                <!-- In Label -->
                <div class="info-box bg-cyan hover-expand-effect">
                  <div class="icon">
                    <i class="material-icons">phonelink_ring</i>
                  </div>
                  <div class="content">
                    <h1 class="text">Móvil/Tablet</h1>
                  </div>
                </div>
              </label> 
            </div>

            <div class="form-check customradio col-md-4">
              <input class="form-check-input" type="radio" id="pc" >
              <label class="form-check-label" for="pc">
                <!-- In Label -->
                <div class="info-box bg-cyan hover-expand-effect">
                  <div class="icon">
                    <i class="material-icons">computer</i>
                  </div>
                  <div class="content">
                    <h1 class="text">Escritorio/PC</h1>
                  </div>
                </div>
              </label> 
            </div>
          </div>

          <br/>

          <button type="button" class="next-form btn btn-info" value="Siguiente"><i class="far fa-arrow-alt-circle-right"></i> Siguiente </button><br><br>
        </div>

        <!-- REGISTRO -->
        <div id="1m" class="menus">
          <br>
          <h2> 
            <i class="far fa-id-badge"></i> ¿Cómo se hará el registro?
          </h2>
          <hr>
          <label><b>Forma del Registro</b></label>
          <br>
          <div class="row">
            <?php 
              $res=$ins_fun_basicas->consulta_generica('tbl_tipo_registro', '');
                while($fila = $res->fetch_assoc()){ ?>
                  <div class="form-check customradio col-md-4">
                    <input class=" tipo_registro form-check-input" type="radio" name="registro" id="registro_<?php echo $fila['id_tipo_registro']; ?>" value="<?php echo $fila['id_tipo_registro']; ?>" >
                    <label class="form-check-label" for="registro_<?php echo $fila['id_tipo_registro']; ?>">
                              <!--?php echo utf8_encode($fila['nombre']); ?-->
                              <!-- In Label -->
                      <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i id="ico_<?php echo $fila['id_tipo_registro']; ?>" class="material-icons"></i>
                        </div>
                        <div class="content">
                            <h1 class="text"><?php echo utf8_encode($fila['nombre']); ?></h1>
                        </div>
                      </div>

                    </label> 
                  </div>
                <?php 
                  } 
                ?>
              <br>
          </div>
            
            <!--<button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
            <button type="button" class="next-form btn btn-info" >Siguiente <i class="far fa-arrow-alt-circle-right"></i></button><br><br>
            -->
          <br>
          <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
          <button type="button" class="next-form btn btn-info" value="Siguiente"><i class="far fa-arrow-alt-circle-right"></i> Siguiente </button>
          <br>
          <br>
                  
        </div>
        <!-- CAMPOS DE REGISTRO -->
        <div id="2m" class="menus">
          <div class="form-group">
            <br>
            <h2> <i class="far fa-id-badge"></i> ¿Qué campos se capturan para el Registro?</h2>
            <hr>
            <table border='0'>
              <tr>
                  <td style='text-align:center;'><b>Campo  </b></td>
                  <td><b>| Posición |</b></td>
                  <td><b>Clave para Acceso</b></td>
              </tr>
              
              <?php 
                $res=$ins_fun_basicas->consulta_generica('tbl_campos_registro', '');
                while($fila = $res->fetch_assoc()){ ?>
                  <tr>
                    <td class="customcheck">
                      <input name="campos_registro[]" class="styled" type="checkbox" value="<?php echo utf8_encode($fila['id_campos_registro']); ?>" id="camposregistro_<?php echo utf8_encode($fila['id_campos_registro']); ?>">
                       
                      <label class="styled" for="camposregistro_<?php echo utf8_encode($fila['id_campos_registro']); ?>">
                        <?php echo utf8_encode(ucwords ($fila['nombre'])); ?>
                      </label>
                    </td>
                    <td style=" text-align: center">
                      <input  id="pos_<?php echo utf8_encode($fila['id_campos_registro']); ?>" type='number'  value=" " style='width:40px;height:25px' min="1"  disabled>
                    </td>
                    <td style=" text-align: center">
                      <?php 
                        if(utf8_encode(ucwords ($fila['nombre']))=='Nombre' or utf8_encode(ucwords ($fila['nombre']))=='Edad' ){ }else{ ?>
                          <input class=" campo_clave form-check-input" type="radio" name="campo_clave" id="campoclave_<?php echo utf8_encode($fila['id_campos_registro']); ?>" disabled >
                          <label class="form-check-label" for="campoclave_<?php echo utf8_encode($fila['id_campos_registro']); ?>"></label>
                      <?php } ?>
                    </td>
                  </tr>
                     <?php  }  ?>
              </table>
          </div>
            <!--<button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
            <button type="button" class="next-form btn btn-info" >Siguiente <i class="far fa-arrow-alt-circle-right"></i></button><br><br>
            -->
            <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
            <button type="button" class="next-form btn btn-info" value="Siguiente"><i class="far fa-arrow-alt-circle-right"></i> Siguiente </button><br><br>
                
        </div>
        <!-- ACUMULACION -->
        <div id="3m" class="menus">
          <br/>
          <h2><i class="fas fa-boxes"></i> ¿Cúal será mécanica la de Acumulación?</h2>
          <hr/>
          <div class="row">
            <div class="col-md-6 col-xs-12">
              <br/>
              <label><b>Forma de Acumulación </b></label>
              <?php 
              $res=$ins_fun_basicas->consulta_generica('tbl_forma_acumulacion', '');
                while($fila = $res->fetch_assoc()){ ?>
                  <div class="customcheck">
                      <input name="form_acum[]" class="styled" type="checkbox" value="<?php echo utf8_encode($fila['id_forma_acumulacion']); ?>" id="tipoacumilacion_<?php echo utf8_encode($fila['id_forma_acumulacion']); ?>">
                      <label class="styled" for="tipoacumilacion_<?php echo utf8_encode($fila['id_forma_acumulacion']); ?>">
                          <?php echo utf8_encode($fila['nombre']); ?>
                      </label>
                  </div>
                <?php  }  ?>
                <br>     
            </div>

            <div class="col-md-6 col-xs-12">
              <br>
              <label><b> ¿Cúal será el Tipo de Acumulación?</b></label>
              <?php 
                $res=$ins_fun_basicas->consulta_generica('tbl_tipo_proyecto', '');
                while($fila = $res->fetch_assoc()){ ?>
                  <div class="form-check miniradio">
                    <input class=" tipo_acumulacion_proy form-check-input" type="radio" name="tipo_acumulacion" id="tipoproyecto_<?php echo utf8_encode($fila['id_tipo_proyecto']); ?>" value="<?php echo utf8_encode($fila['id_tipo_proyecto']); ?>" >
                    <label class="form-check-label" for="tipoproyecto_<?php echo utf8_encode($fila['id_tipo_proyecto']); ?>">
                      <!--?php echo utf8_encode($fila['nombre']); ?-->
                      <!-- In Label -->
                      <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i id="2ico_<?php echo $fila['id_tipo_proyecto']; ?>" class="material-icons small-icons"></i>
                        </div>
                        <div class="content">
                            <h1 class="text"><?php echo utf8_encode($fila['nombre']); ?></h1>
                        </div>
                      </div>
                    </label>
                  </div>              
              <?php  }  ?>
                <!-- DETALLE DE PUNTOS COMO % DE CONSUMO -->
                <section id='tipoproyecto_1_detalle'>
                  <hr>
                  <i class="fas fa-chevron-right"></i> % de la cantidad vendida a otorgar para el programa <input id="por_cant_vendida" type="number" value=" " style="width:80px;height:25px" min="1" ><br>
                  <i class="fas fa-chevron-right"></i> N° de puntos por cada peso gastado <input id="num_puntos_peso" type="number" value=" " style="width:80px;height:25px" min="1" ><br>
                </section>
                <!-- DETALLE DE PUNTOS COMO VISITAS-->
                <section id='tipoproyecto_2_detalle'>
                  <hr>
                    <!--<i class="fas fa-chevron-right"></i> N° mínimo visitas para premio <input id="visitas_premio" type="number" value=" " style="width:80px;height:25px" min="1" ><br>-->
                  <i class="fas fa-chevron-right"></i> N° de puntos por visita <input id="puntos_por_visita" type="number" value=" " style="width:80px;height:25px" min="1" ><br>
                </section>
              <br>
            </div>
          </div>
          
            <br/>
            <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
            <button type="button" class="next-form btn btn-info" value="Siguiente" />Siguente <i class="far fa-arrow-alt-circle-right"></i></button>
            <br/>
            <br/>
        </div>

        <div id="15m" class="menus">
          <div class="header">
            <h2> 
              <i class="fa fa-lock"></i> ¿Cómo confirmará la operación el Usuario?
            </h2>
          </div>
          <?php 
          $res=$ins_fun_basicas->consulta_generica('tbl_configuracion_proyecto', '');
          while($fila = $res->fetch_assoc()){ ?>
          <div class="body">
            <div class="row">
              <div class="form-check customradio col-md-4">
                <input class="form-check-input" type="radio" name="confirm_operation" id="qrcode" value="2" <?php echo $fila['tipo_confirm_operacion'] == 2 ? 'checked="true"' : '' ?>>
                <label class="form-check-label" for="qrcode">
                  <!-- In Label -->
                  <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                      <i class="fas fa-qrcode"></i>
                    </div>
                    <div class="content">
                      <h1 class="text">Por Código QR</h1>
                    </div>
                  </div>
                </label> 
              </div>

              <div class="form-check customradio col-md-4">
                <input class="form-check-input" type="radio" name="confirm_operation" id="contrasena" value="1" <?php echo $fila['tipo_confirm_operacion'] == 1 ? 'checked="true"' : '' ?>>
                <label class="form-check-label" for="contrasena">
                  <!-- In Label -->
                  <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                      <i class="material-icons">lock</i>
                    </div>
                    <div class="content">
                      <h1 class="text">Por Contraseña</h1>
                    </div>
                  </div>
                </label> 
              </div>
            </div>
          </div>
          <?php  }  ?>
          <br/>
          <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
          <button type="button" class="next-form btn btn-info" value="Siguiente"><i class="far fa-arrow-alt-circle-right"></i> Siguiente </button><br><br>
        </div>


        <!-- REDENCION -->
        <div id="4m" class="menus">
          <br>
          <h2><i class="fas fa-gift"></i> ¿Cuál será el Tipo de Redención?</h2>
          <hr>
          <div class="form-group">
            <br>
            <label><b> ¿Cuál será la forma de redención?</b></label>
            <hr>
            <?php
                $res=$ins_fun_basicas->consulta_generica('tbl_tipo_redencion', '');
                while($fila = $res->fetch_assoc()){ 
            ?>
                  <div class="customcheck" id="redencion_<?php echo utf8_encode($fila['id_tipo_redencion']); ?>">
                    <input name='tipo_reden[]' class="styled" type="checkbox" value="<?php echo utf8_encode($fila['id_tipo_redencion']); ?>" id="tiporedencion_<?php echo utf8_encode($fila['id_tipo_redencion']); ?>">
                    <label  class="styled" for="tiporedencion_<?php echo utf8_encode($fila['id_tipo_redencion']); ?>" style="height:auto">
                    <?php echo utf8_encode($fila['nombre']); ?>
                    </label>
            <?php 
                      if($fila['link_detalle']!=''){ 
            ?>
              
            <?php 
                      }
                      
                               
            ?>
                  </div>
            <?php
                }  
            ?>
          </div>
          <div class="form-group">
            <label><b>Seleccione la Mecánica de Redención</b></label>
            <hr>
            <?php
                $res=$ins_fun_basicas->consulta_generica('tbl_mecanica_redencion', '');
                while($fila = $res->fetch_assoc()){ ?>
                    <div class="customcheck">
                       <input name='mecanica_reden[]' class="styled" type="checkbox" value="<?php echo utf8_encode($fila['id_mecanica_redencion']); ?>" id="mecanicaredencion_<?php echo utf8_encode($fila['id_mecanica_redencion']); ?>">
                       <label  class="styled" for="mecanicaredencion_<?php echo utf8_encode($fila['id_mecanica_redencion']); ?>">
                         <?php echo utf8_encode($fila['nombre']); ?>
                       </label>
                    </div>
            <?php  }  ?>
          </div>
          <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
          <button type="button" class="next-form btn btn-info" value="Siguiente" />Siguente <i class="far fa-arrow-alt-circle-right"></i></button>
          <br>
          <br>
        </div>
        <!-- COMUNICACION -->
        <div id="6m" class="menus">
          <br>
          <h2><i class="far fa-comments"></i> ¿Cuál será la forma de Comunicación?</h2>
          <h6>Configura la Forma de Comunicación</h6>

          <div class="row">

              <div class="col-xs-12 col-md-3">
                <label><b>Bono de Bienvenida</b></label>
                <div class="form-check">
                    <input class=" bono_bienvenida form-check-input"  type="radio" id="si_bono" name="bono"  value='1' />
                    <label for="si_bono">Si</label><br>
                    <input class=" bono_bienvenida form-check-input"  type="radio" id="no_bono" name="bono"  value='0' />
                    <label for="no_bono">No</label>
                    <section id='div_ptos_bienvenida'>
                        Puntos de Bienvenida:<input type="number" id='ptos_bienvenida' style='width:80px;height:25px' min="1" value="" disabled >
                    </section>
                </div>
              </div>

              <div class="col-xs-12 col-md-3">
                <label><b>Bono por Cumpleaños</b></label>
                <div class="form-check">
                    <input class=" bono_cumpleanios form-check-input"  type="radio" id="si_cumple_anios" name="cumple"  value='1' />
                    <label for="si_cumple_anios">Si</label><br>
                    <input class=" bono_cumpleanios form-check-input"  type="radio" id="no_cumple_anios" name="cumple"  value='0' />
                    <label for="no_cumple_anios">No</label>
                    <section id='div_ptos_cumple'>
                        Puntos de Cumpleaños:<input type="number" id='ptos_cumple' style='width:80px;height:25px' min="1" value=""  disabled >
                    </section>
                </div>
              </div>
              <br>
              <br>

          </div>
            
          <hr>

          <div class="row">

              <div class="col-xs-12 col-md-3">
                <label><b>Tipo de Comunicación al Registrarse</b></label>
                <?php
                //class: form-check-label
                // input class: tipo_comunicacion_registro form-check-input
                    $res=$ins_fun_basicas->consulta_generica('tbl_tipo_comunicacion', '');
                    while($fila = $res->fetch_assoc()){ ?>
                        <div class="customcheck form-check" id="comunicacion_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                           <input name='tipo_comunicacion_registro[]' class="styled" type="checkbox" value="<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>" id="tipo_comunicacion_registro<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                           <label class="styled" for="tipo_comunicacion_registro<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                             <?php echo utf8_encode($fila['nombre']); ?>
                           </label>
                        </div>
                <?php  }  ?>
              </div>

              <div class="col-xs-12 col-md-3">
                <label><b>Tipo de Comunicación al Acumular</b></label>
                  <?php
                    $res=$ins_fun_basicas->consulta_generica('tbl_tipo_comunicacion', '');
                    while($fila = $res->fetch_assoc()){ 
                  ?>
                        <div class="customcheck form-check" id="comunicacion_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                           <input name='tipo_comunicacion_acumular[]' class="styled" type="checkbox" value="<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>" id="tipocomunicacion_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                           <label class="styled" for="tipocomunicacion_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                             <?php echo utf8_encode($fila['nombre']); ?>
                           </label>
                        </div>
                    <?php  }  ?>
              </div>

              <div class="col-xs-12 col-md-3">
                <label><b>Tipo de Comunicación al Redimir</b></label>
                  <?php
                    $res=$ins_fun_basicas->consulta_generica('tbl_tipo_comunicacion', '');
                    while($fila = $res->fetch_assoc()){ ?>
                        <div class="customcheck form-check" id="comunicacionredimir_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                           <input name='tipo_comunicacion_redimir[]' class="styled" type="checkbox" value="<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>" id="tipocomunicacionredimir_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                           <label class="styled" for="tipocomunicacionredimir_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                             <?php echo utf8_encode($fila['nombre']); ?>
                           </label>
                        </div>
                  <?php  }  ?>
              </div>

              <div class="col-xs-12 col-md-3">
                <label><b>Comunicación Periódica</b></label>
                  <?php
                    $res=$ins_fun_basicas->consulta_generica('tbl_tipo_comunicacion', '');
                    while($fila = $res->fetch_assoc()){ ?>
                        <div class="customcheck form-check" id="comunicacionperiodica_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                           <input name='tipo_comun_periodica[]' class="styled" type="checkbox" value="<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>" id="tipocomunicacionperiodica_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                           <label class="styled" for="tipocomunicacionperiodica_<?php echo utf8_encode($fila['id_tipo_comunicacion']); ?>">
                             <?php echo utf8_encode($fila['nombre']); ?>
                           </label>
                        </div>
                  <?php  }  ?>
               
              </div> 
              <div class="col-xs-12 col-md-3">
                <label>Lapso de Comunicación Periódica</label>
                <?php
                  $res=$ins_fun_basicas->consulta_generica('tbl_periodo_comunicacion', '');
                  while($fila = $res->fetch_assoc()){ 
                ?>
                    <div class="miniradio form-check">
                      <input class="lapso_comun form-check-input" type="radio" name="mediocomunicacionperiodica" id="mediocomunicacionperiodica_<?php echo utf8_encode($fila['id_periodo_comunicacion']); ?>" value="<?php echo utf8_encode($fila['id_periodo_comunicacion']); ?>" >
                      <label class="form-check-label" for="mediocomunicacionperiodica_<?php echo utf8_encode($fila['id_periodo_comunicacion']); ?>">
                            <!--?php echo utf8_encode($fila['nombre']); ?-->
                        <div class="info-box bg-cyan hover-expand-effect">
                          <div class="icon">
                              <i id="2ico_<?php echo $fila['id_tipo_proyecto']; ?>" class="far fa-calendar-alt small-icons"></i>
                          </div>
                          <div class="content">
                              <h1 class="text"><?php echo utf8_encode($fila['nombre']); ?></h1>
                          </div>
                        </div>
                      </label>
                    </div>
                <?php 
                  } 
                ?>
                  <br>
              </div>
          </div>

          <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
          <button type="button" class="next-form btn btn-info" value="Siguiente" />Siguente <i class="far fa-arrow-alt-circle-right"></i></button><br><br>
            
        </div>
        <!-- END COMUNICACION -->
     
        <!-- ESTILO -->
        <div id="7m" class="menus">
            <br>
            <h2><i class="far fa-id-card"></i>  Configura el Estilo de la Plataforma del Cliente</h2>
            <input name="estilos_default" class="estilos_default form-check-input" type="checkbox" value="1" id="estilos_default">
            <label class="form-check-label" for="estilos_default"> 
              <h6>Tomar estilos Default </h6>
            </label>
            <br>  
            <section id='conten_estilo'>
              <h6>Configura los Estilos Generales del Cliente</h6>
              <!-- LOGO-->
              <form method="post" id="formulario" enctype="multipart/form-data">
               <b>Selecciona Logo:</b> <input type="file" name="file" id='file' value="">
              </form>
              <input type="hidden" id='nombre_logo' value="">
              <div><img id="logo" src="" width="250px"></div>
              <div id='respuesta_logo'></div><br>
              <!--IMAGEN FONDO -->
              <form method="post" id="formulario2" enctype="multipart/form-data">
               <b>Selecciona Imagen de Fondo:</b> <input type="file" name="file2" id='file2' value="">
              </form>
              <input type="hidden" id='nombre_fondo' value="">
              <div><img id="fondo" src="" width="250px"></div>
              <div id='respuesta_imagen_fondo'></div><br>
              <div class="form-group">
                <label for="color_prim"><b>Color Primario</b></label>
                <input type="color" class="form-control select_color" name="color_prim" id="color_prim" value="#000">
              </div>
              <div class="form-group">
                <label for="color_sec"><b>Color Secundario</b></label>
                <input type="color" class="form-control select_color" name="color_sec" id="color_sec" value="#000">
              </div>
              <br>
              <br>
            </section>
            <br>
                    
            <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
            <button type="button" class="next-form btn btn-info" value="Siguiente"><i class="far fa-arrow-alt-circle-right"></i> Siguiente </button>
            <br>
            <br>
            <!--button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
            <input type="submit" name="submit" class="submit btn btn-success guarda_config" value="Generar Configuración" />
            <div id='loading'> <img src="../../inc/imagenes/load.gif"></div><br><br-->
        </div>
        <!-- SUCURSALES -->
        <div id="8m" class="menus">
          <br>
          <h2> 
            <i class="far fa-id-badge"></i> ¿Tendrá Sucursales?
          </h2>
          <hr>
          <label><b>Activar Sucursales</b></label>
          <br>
          <div class="row">
              <div class="form-check customradio col-md-6">
                <input class="sucs form-check-input" type="radio" name="sucursales" id="suc1" value="1" >
                <label class="form-check-label" for="suc1">

                  <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">done</i>
                    </div>
                    <div class="content">
                        <h1 class="text">Si</h1>
                    </div>
                  </div>

                </label> 
              </div>

              <div class="form-check customradio col-md-6">
                <input class="sucs form-check-input" type="radio" name="sucursales" id="suc0" value="0" >
                <label class="form-check-label" for="suc0">

                  <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">block</i>
                    </div>
                    <div class="content">
                        <h1 class="text">No</h1>
                    </div>
                  </div>

                </label> 
              </div>

              <div>
                <div class="input-group col-md-6" style="padding: 10px;">
                  <label><b>Número de Sucursales</b></label>
                  <br>
                  <input type="number" min="1" name="nsucursales" id="nsucursales">
                </div>  
              </div>
          </div>
          <br>  
          <br>
          <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
          <button type="button" class="next-form btn btn-info" value="Siguiente"><i class="far fa-arrow-alt-circle-right"></i> Siguiente </button>
          <br/>
          <br>
        </div>
        <!-- MENUS -->
        <div id="9m" class="menus">
          <br>
          <h2> 
            <i class="far fa-id-badge"></i> ¿Tendrá Menú?
          </h2>
          <hr>
          <label><b>Activar Menú</b></label>
          <br>
          <div class="row">
              <div class="form-check customradio col-md-4">
                <input class="mnus form-check-input" type="radio" name="mnu" id="mnu1" value="1" >
                <label class="form-check-label" for="mnu1">

                  <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">done</i>
                    </div>
                    <div class="content">
                        <h1 class="text">Si</h1>
                    </div>
                  </div>

                </label> 
              </div>

              <div class="form-check customradio col-md-4">
                <input class="mnus form-check-input" type="radio" name="mnu" id="mnu0" value="0" >
                <label class="form-check-label" for="mnu0">

                  <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">block</i>
                    </div>
                    <div class="content">
                        <h1 class="text">No</h1>
                    </div>
                  </div>

                </label> 
              </div>

            </div>  
          <br>
          <br>
          <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
          <button type="button" class="next-form btn btn-info" value="Siguiente"><i class="far fa-arrow-alt-circle-right"></i> Siguiente </button>
          <br>
          <br>
        </div>
        <!-- PROMOCIONES -->
        <div id="10m" class="menus">
          <br>
          <h2> 
            <i class="far fa-id-badge"></i> ¿Tendrá Promociones?
          </h2>
          <hr>
          <label><b>Activar Promociones</b></label>
          <br>
          <div class="row">
              <div class="form-check customradio col-md-4">
                <input class="prom form-check-input" type="radio" name="promos" id="promo1" value="1" >
                <label class="form-check-label" for="promo1">

                  <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">done</i>
                    </div>
                    <div class="content">
                        <h1 class="text">Si</h1>
                    </div>
                  </div>

                </label> 
              </div>

              <div class="form-check customradio col-md-4">
                <input class="prom form-check-input" type="radio" name="promos" id="promo0" value="0" >
                <label class="form-check-label" for="promo0">

                  <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">block</i>
                    </div>
                    <div class="content">
                        <h1 class="text">No</h1>
                    </div>
                  </div>

                </label> 
              </div>
          </div>  
          <br>
          <br>
          <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
          <button type="button" class="next-form btn btn-info" value="Siguiente"><i class="far fa-arrow-alt-circle-right"></i> Siguiente </button>
          <br/>
          <br>
        </div>
        <!-- PREMIOS DE REDENCIONES -->
        <div id="11m" class="menus">
          <h3>Configura los Premios de Redención</h3>
          <div class="form-group">
            <button id="modalr" type="button" onclick="" class="btn btn-primary" data-toggle="modal" data-target="#premios" disabled>
              Configurar
            </button>

            <button id="editmodal" type="button" onclick="" class="btn btn-primary" disabled>
              <a class="text-white" href="../configuracion/edicion_premio.php" target="_blank">Editar Premios</a> 
            </button>
            
            <!-- Modal -->
            <div class="modal fade " id="premios1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document" style="width: 80%;">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Configuración de Productos por Puntos</h5><hr>
                  </div>
                  <div class="modal-body">
                   <?php  include '../../view/configuracion/premios_puntos.php'; ?>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
                  </div>
                </div>
              </div>
            </div>

            <!-- Modal -->
            <div class="modal fade " id="premios2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document" style="width: 80%;">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Configuración de Productos por Visitas</h5><hr>
                  </div>
                  <div class="modal-body">
                   <?php  include '../../view/configuracion/premios_visitas.php'; ?>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
                  </div>
                </div>
              </div>
            </div>

            <!-- Modal Edit -->
            <div class="modal fade " id="editconf" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document" style="width: 80%;">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edición de Configuración de Productos por Puntos</h5><hr>
                  </div>
                  <div class="modal-body">
                   <?php  //include '../../view/configuracion/edicion_premio.php'; ?>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Salir</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <br>
          <br>
          <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
          <button type="button" class="next-form btn btn-info" value="Siguiente"><i class="far fa-arrow-alt-circle-right"></i> Siguiente </button>
          <br/>
          <br>   
        </div>  
        <!-- NIVEL DE USUARIOS -->
        <div id="12m" class="menus">
          <br>
          <h2> 
            <i class="far fa-id-badge"></i> ¿Tendrá Niveles de Usuario?
          </h2>
          <hr>
          <label><b>Activar Nivel de Usuarios</b></label>
          <br>
          <div class="row">
              <div class="form-check customradio col-md-4">
                <input class="lvl form-check-input" type="radio" name="nivel" id="lvl1" value="1" >
                <label class="form-check-label" for="lvl1">

                  <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">done</i>
                    </div>
                    <div class="content">
                        <h1 class="text">Si</h1>
                    </div>
                  </div>

                </label> 
              </div>

              <div class="form-check customradio col-md-4">
                <input class="lvl form-check-input" type="radio" name="nivel" id="lvl0" value="0" >
                <label class="form-check-label" for="lvl0">

                  <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">block</i>
                    </div>
                    <div class="content">
                        <h1 class="text">No</h1>
                    </div>
                  </div>

                </label> 
              </div>
          </div>  
          <br>
          <br>
          <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
          <button type="button" class="next-form btn btn-info" value="Siguiente"><i class="far fa-arrow-alt-circle-right"></i> Siguiente</button>
          <br/>
          <br>
        </div>
        <!-- INFO APP -->
        <div id="13m" class="menus">
            <h3>Información de APP</h3>
            <hr>
            <h3> 
              <i class="far fa-id-badge"></i> ¿Tendrá App?
            </h3>
            <label><b>Activar App</b></label>
            <br>
            <div class="row">
                <div class="form-check customradio col-md-6">
                  <input class="apps form-check-input" type="radio" name="app" id="app1" value="1" >
                  <label class="form-check-label" for="app1">

                    <div class="info-box bg-cyan hover-expand-effect">
                      <div class="icon">
                          <i class="material-icons">done</i>
                      </div>
                      <div class="content">
                          <h1 class="text">Si</h1>
                      </div>
                    </div>

                  </label> 
                </div>

                <div class="form-check customradio col-md-6">
                  <input class="apps form-check-input" type="radio" name="app" id="app0" value="0" >
                  <label class="form-check-label" for="app0">

                    <div class="info-box bg-cyan hover-expand-effect">
                      <div class="icon">
                          <i class="material-icons">block</i>
                      </div>
                      <div class="content">
                          <h1 class="text">No</h1>
                      </div>
                    </div>

                  </label> 
                </div>
            </div>
            <br>  
            <br>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons"></i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control" id="title" name="title" placeholder="* Titulo Principal" required>
                </div>
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons"></i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control" id="detail" name="detail" placeholder="* Detalle" required>
                </div>
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons"></i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control" id="tel" name="tel" placeholder="* Teléfono de Marca" required>
                </div>
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons"></i>
                </span>
                <div class="form-line">
                    <input type="email" class="form-control" id="email" name="email" placeholder="* E-Mail de Marca" required>
                </div>
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons"></i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control" id="face" name="face" placeholder="* Cuenta de Facebook" required>
                </div>
            </div>
            <div class="input-group">
                <span class="input-group-addon">
                    <i class="material-icons"></i>
                </span>
                <div class="form-line">
                    <input type="text" class="form-control" id="twit" name="twit" placeholder="* Cuenta de Twitter" required>
                </div>
            </div>
            
                <br/>
                <br/>
                <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
                <button type="button" class="next-form btn btn-info" value="Siguiente"> <i class="far fa-arrow-alt-circle-right"></i> Siguiente</button>
                <br/>
                <br/>
            
        </div>  

        <!-- TIPO DE PUNTO DE VENTA -->
        <div id="14m" class="menus">
          
          <div class="row">
            <div class="col-md-12 col-12 col-sm-12">
              <br>
              <h2> 
                <i class="far fa-id-badge"></i> Tipo de Punto de Venta
              </h2>
              <hr>
              <?php include '../../view/configuracion/t_punto_venta.php'; ?>
            </div>
          </div>
          <br>
          <br>
          <button type="button" name="previous" class="previous-form btn btn-default" ><i class="far fa-arrow-alt-circle-left"></i> Regresar </button>
          <input type="submit" name="submit" class="submit btn btn-success guarda_config" value="Generar Configuración" />
          <div id='loading'> <img src="../../inc/imagenes/load.gif"></div>
          <br>
          <br>
          <br>
          <br>
        </div>



      </div><!-- end form div -->
    </div><!-- end div class container -->    
  </div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="../../inc/js/configuracion2.js"></script>