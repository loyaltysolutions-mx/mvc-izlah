<?php

include_once '../../inc/cont_fijos2.php';
include_once '../../inc/parametros.php';
//$inst_control=new C_home($ser,$usu,$pas,$bd);
$ins_cont_fijos=new Cont_fijos($ser,$usu,$pas,$bd);
 session_start();
 $email = $_SESSION["email"];
 $usuario = $_SESSION["usuario"];
?>
<!DOCTYPE html>
<html lang="es">
    <head>
      <?php  
        $ins_cont_fijos->head();
      ?>
      <!--script src="../../inc/js/home.js"></script-->
    </head>
  <body id="body" class="theme-blue"> 
      <?php 
        $ins_cont_fijos->header();
      ?>
    <div class="container-fluid">
      <div class="row">
        <?php  
             $id_usu=$_SESSION["idusuario"];
             $ins_cont_fijos->menu($id_usu,$usuario,$email);
        ?>
      </div>
    </div>
    <section class="content">
        <div id="all-content" class="row container-fluid">
          <div id="card-content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          </div>  
            <?php 
            include ('configuracion.php');
            //include('widgets.php');
            ?>
        </div>
    </section>
  </body>
  <?php  
    $ins_cont_fijos->footer();
  ?>
</html>

