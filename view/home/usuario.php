<?php 
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
session_start();
if(isset($_SESSION["usuario"])){ 
$catalogue=1;
$ins_funciones=new Funciones_Basicas();
$id_usu=$_SESSION["id_usuario"];
$qr_config=$ins_funciones->consulta_generica_all(' select * from tbl_configuracion_proyecto ');
$reg_config= mysqli_fetch_assoc($qr_config);
 
?>
<div class="row clearfix">
<?php  
if($reg_config['id_tipo_proyecto']==3){ 
//MARCADORES PARA PROYECTO VISITAS
 //CATALOGO DE PREMIOS
 $qr_catalogo_premios=$ins_funciones->consulta_generica_all(' select * from tbl_cat_premios_productos_servicios where activo=0');
 //VISITAS ACUMULADAS

 $qr_visitas_acumulados=$ins_funciones->consulta_generica_all('select count(*) as visitas_totales from  tbl_registros where id_tipo_registro=1 and id_usuario='.$id_usu );
 $visitas_totales_acumulado= mysqli_fetch_assoc($qr_visitas_acumulados);
 //VISITAS REDIMIDAS
 $qr_premios_redimidos=$ins_funciones->consulta_generica_all('select count(*) as visitas_totales from  tbl_registros where id_tipo_registro=2 and id_usuario='.$id_usu);
 $premios_totales_redimidos= mysqli_fetch_assoc($qr_premios_redimidos);
 /* INFORMACION CUANDO PV ENTRA A REDIMIR  DE USUARIO
 $res_con=$ins_funciones->consulta_generica_all('select SUM(R.puntos) as puntos_acumulados,U.usuario,U.id_usuario  from tbl_registros as R
                                                inner join tbl_usuario as U on R.id_usuario=U.id_usuario
                                                 where R.id_tipo_registro=1 and R.id_usuario='.$id_usua);
 $ptos_acumu= mysqli_fetch_assoc($res_con)											 
 $res_con=$ins_funciones->consulta_generica_all('select SUM(puntos) as puntos_acumulados from tbl_registros where id_tipo_registro=1 and id_usuario='.$id_usu);
 $ptos_acumu= mysqli_fetch_assoc($res_con);
 
 $res_con2=$ins_funciones->consulta_generica_all('select SUM(puntos) as puntos_redimidos from tbl_registros where id_tipo_registro=2 and id_usuario='.$id_usu);
 $ptos_redim= mysqli_fetch_assoc($res_con2);*/

?>

	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box hover-zoom-effect">
            <div class="icon bg-pink">
               <i class="fas fa-donate"></i>
            </div>
            <div class="content">
                <?php 
                    if($visitas_totales_acumulado['visitas_totales']==''){ 
                        $vta = '0'; 
                    }else{ 
                        $vta = $visitas_totales_acumulado['visitas_totales']; 
                    }
                ?>
                <div class="text">Visitas Acumulados </div>
                <div class="number count-to" data-from="0" data-to="<?php echo $vta; ?>" data-speed="1000" data-fresh-interval="20"><?php echo $vta; ?></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box hover-zoom-effect">
            <div class="icon bg-blue">
                <i class="fas fa-gift"></i>
            </div>
            <div class="content">
                <?php
                    if($premios_totales_redimidos['visitas_totales']==''){ 
                        $ptr = '0'; 
                    }else{
                        $ptr = $premios_totales_redimidos['visitas_totales']; 
                    }
                ?>
                <div class="text">Premios Redimidos</div>
                <div class="number count-to" data-from="0"  data-to="<?php echo $ptr; ?>" data-fresh-interval="20"><?php echo $ptr; ?></div>
            </div>
        </div>
    </div>
</div>

<?php }else{ 
//MARCADORES PARA PROYECTOS DE PUNTOS 
 //CATALOGO DE PREMIOS
 $qr_catalogo_premios=$ins_funciones->consulta_generica_all(' select * from tbl_cat_premios_productos_servicios where activo=0');
 //PUNTOS ACUMULADOS
 $qr_ptos_acumulados=$ins_funciones->consulta_generica_all('select sum(puntos_totales) as puntos_totales  from tbl_puntos_totales where id_tipo_puntos=1 and id_usuario='.$id_usu);
 $ptos_totales_acumulado= mysqli_fetch_assoc($qr_ptos_acumulados);
 //PUNTOS REDIMIDOS
 $qr_ptos_redimidos=$ins_funciones->consulta_generica_all('select SUM(puntos) as puntos_totales  from  tbl_registros where id_tipo_registro=2 and id_usuario='.$id_usu);
 $ptos_totales_redimidos= mysqli_fetch_assoc($qr_ptos_redimidos);
 $res_con=$ins_funciones->consulta_generica_all('select SUM(R.puntos) as puntos_acumulados,U.usuario,U.id_usuario  from tbl_registros as R
                                                inner join tbl_usuario as U on R.id_usuario=U.id_usuario
                                                 where R.id_tipo_registro=1 and R.id_usuario='.$id_usua);
 $res_con=$ins_funciones->consulta_generica_all('select SUM(puntos) as puntos_acumulados from tbl_registros where id_tipo_registro=1 and id_usuario='.$id_usu);
 $ptos_acumu= mysqli_fetch_assoc($res_con);
 $res_con2=$ins_funciones->consulta_generica_all('select SUM(puntos) as puntos_redimidos from tbl_registros where id_tipo_registro=2 and id_usuario='.$id_usu);
 $ptos_redim= mysqli_fetch_assoc($res_con2);
?>
<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box hover-zoom-effect">
            <div class="icon bg-pink">
               <i class="fas fa-donate"></i>
            </div>
            <div class="content">
                <div class="text">Puntos Acumulados </div>
				<?php 
					if($ptos_totales_acumulado['puntos_totales']==''){
						$rpa='0'; 
						}else{
						$rpa=$ptos_totales_acumulado['puntos_totales']; 
						}
				?>
                <div class="number count-to" data-from="0" data-to="<?php echo $rpa ?>"  data-fresh-interval="20"><?php  echo $rpa; ?></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box hover-zoom-effect">
            <div class="icon bg-blue">
                <i class="fas fa-gift"></i>
            </div>
            <div class="content">
                <div class="text">Puntos Redimidos</div>
				<?php 
					if($ptos_totales_redimidos['puntos_totales']==''){
						$rpr='0'; 
					}else{ 
						$rpr=$ptos_totales_redimidos['puntos_totales']; }
				?>
				
                <div class="number count-to" data-from="0"  data-to="<?php  echo $rpr; ?>" data-fresh-interval="20"> <?php  echo $rpr;?></div>
                       </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">		
        <?php
            //die("Usuario: ".$id_usu); 
            $ins_cont_fijos->catalogo_productos($catalogue,$reg_config['id_tipo_proyecto'],$id_usu);
        ?>        
	</div>
</div>
<?php  }else{
      $catalogue=0;  
      $redirec= "../../" ;
      header('Location:'.$redirec);
 }
