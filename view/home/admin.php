<?php 
include '../inc/funciones.php';
include '../inc/parametros.php';
session_start();
if(isset($_SESSION["usuario"])){ 
$ins_funciones=new Funciones_Basicas();
$id_usu=$_SESSION["id_usuario"];
//CONSULTAMOS TIPO DE PROYECTO PUNTOS O VISITAS
$qr_tipo_proyecto=$ins_funciones->consulta_generica_all('select id_tipo_proyecto  from  tbl_configuracion_proyecto');
$config_proy= mysqli_fetch_assoc($qr_tipo_proyecto);
//EVALUAMOS SI EL PROYECTO ES POR PUNTOS O POR VISITAS
switch($config_proy['id_tipo_proyecto']){
	case 1://PTOS
	case 2:
	$tit_acum='Puntos Acumulados';
	$tit_red='Puntos Redimidos';
		 //TRAEMOS INFORMACION DE USUARIO 
		 //CTES REGISTRADOS
		 $qr_ctes_registrados=$ins_funciones->consulta_generica_all('select count(*) as n from tbl_usuario  where rol=3 and activo=0');
		 $res_qr_ctes_reg= mysqli_fetch_assoc($qr_ctes_registrados);
		 //PUNTOS ACUMULADOS
		 $qr_ptos_acumulados=$ins_funciones->consulta_generica_all('select sum(puntos_totales) as n  from tbl_puntos_totales where id_tipo_puntos=1');
		 $res_qr_acumula= mysqli_fetch_assoc($qr_ptos_acumulados);
		 //PUNTOS REDIMIDOS
		 $qr_ptos_redimidos=$ins_funciones->consulta_generica_all('select sum(puntos_totales) as n from tbl_puntos_totales where  id_tipo_puntos=2');
		 $res_qr_redimidos= mysqli_fetch_assoc($qr_ptos_redimidos);
		  //TICKETS REGISTRADOS
		 $qr_ticketes_registrados=$ins_funciones->consulta_generica_all('select count(*) as n from tbl_registros where id_tipo_registro=1');
		 $tickets_qr_registrados= mysqli_fetch_assoc($qr_ticketes_registrados);
	break;
	
	case 3://VISITAS
		$tit_acum='Visitas Acumulación';
		$tit_red='Visitas  de Redención';
	     //TRAEMOS INFORMACION DE USUARIO 
		 //CTES REGISTRADOS
		 $qr_ctes_registrados=$ins_funciones->consulta_generica_all('select count(*) as n from tbl_usuario  where rol=3 and activo=0');
		 $res_qr_ctes_reg= mysqli_fetch_assoc($qr_ctes_registrados);
		 //VISITAS ACUMULADAS
		 $qr_visitas_acumuladas=$ins_funciones->consulta_generica_all('select count(*) as n from  tbl_registros where id_tipo_registro=1');
		 $res_qr_acumula= mysqli_fetch_assoc($qr_visitas_acumuladas);
		 //VISITAS REDIMIDAS
		 $qr_visitas_redimidas=$ins_funciones->consulta_generica_all('select count(*) as n from  tbl_registros where id_tipo_registro=2');
		 $res_qr_redimidos= mysqli_fetch_assoc($qr_visitas_redimidas);
		  //TICKETS REGISTRADOS
		 $qr_ticketes_registrados=$ins_funciones->consulta_generica_all('select count(*) as n from tbl_registros where id_tipo_registro=1');
		 $tickets_qr_registrados= mysqli_fetch_assoc($qr_ticketes_registrados);
	break;
	default:
		echo 'ocurrio un problema';
	break;
}

$sqllvl=$ins_funciones->consulta_generica_all('SELECT niveles FROM tbl_configuracion_proyecto');
$haynivel=mysqli_fetch_assoc($sqllvl);
//LLAMAMOS A STORE PARA TRAER ARR DE DATOS DE HISTORIAL DEL MES
$n_dia=date('d');
$n_mes=date('m');
$n_anio=date('Y');

if($haynivel['niveles']=='0' OR $haynivel['niveles']==0){
	$cols = 'col-lg-12 col-md-12 col-sm-12 col-xs-12';
	$displaylvl = 'display:none;';
}else{
	$cols = 'col-lg-6 col-md-6 col-sm-12 col-xs-12';
}

?>
<div class="row clearfix">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box hover-zoom-effect">
            <div class="icon bg-pink">
               <i class="fas fa-users"></i>
            </div>
            <div class="content">
                <div class="text">Clientes  Registrados</div>
                <?php 
                    if($res_qr_ctes_reg['n']==''){
                        $resqrc = '0'; 
                    }else{ 
                        $resqrc = $res_qr_ctes_reg['n']; 
                    }
                ?>
                <div class="number count-to" data-from="0" data-to="<?php echo $resqrc ?>" data-speed="1000" data-fresh-interval="20"><?php echo $resqrc ?></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box hover-zoom-effect">
            <div class="icon bg-blue">
                <i class="fas fa-boxes"></i>
            </div>
            <div class="content">
                <div class="text"><?php  echo $tit_acum; ?></div>
                <?php 
                    if($res_qr_acumula['n']==''){
                        $resqr = '0'; 
                    }else{ 
                        $resqr = $res_qr_acumula['n']; 
                    }
                ?>
                <div class="number count-to" data-from="0"  data-to="<?php echo $resqr; ?>" data-fresh-interval="20">
                <?php echo $resqr; ?></div>
                 
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box hover-zoom-effect">
            <div class="icon bg-light-green">
                <i class="fas fa-gift"></i>
            </div>
            <div class="content">
                <div class="text"><?php  echo $tit_red; ?>  </div>
                <?php 
                    if($res_qr_redimidos['n']=='')
                        { 
                            $ptsqr = '0'; 
                        }else{ 
                            $ptsqr = $res_qr_redimidos['n']; 
                        }
                ?>
                <div class="number count-to" data-from="0"  data-to="<?php echo $ptsqr; ?>" data-fresh-interval="20"><?php echo $ptsqr; ?></div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="info-box hover-zoom-effect">
            <div class="icon bg-yellow">
                <i class="fas fa-tags"></i>
            </div>
            <div class="content">
                <div class="text">Tickets  Registrados</div>
               <?php 
                    if($tickets_qr_registrados['n']=='')
                        { 
                            $tksqr = '0'; 
                        }else{ 
                            $tksqr = $tickets_qr_registrados['n']; 
                        }
                ?>
                <div class="number count-to" data-from="0"  data-to="<?php echo $tksqr; ?>" data-fresh-interval="20"><?php echo $tksqr; ?></div>
            </div>
        </div>
    </div>
</div>
<!-- GRAFICAS -->
<div class="container-fluid">
    <div class="row clearfix">
        <div class="<?php echo $cols;?>">
            <div class="card">
                <div class="header">
                    <h2>Historial de Clientes del Mes Actual </h2>
					<?php
						//CLIENTES REGISTRADOS
						$arr_res_ctes= $ins_funciones->consulta_generica_st('CALL historial_ctes_mes('.$n_dia.','.$n_mes.','.$n_anio.')');
						$arr_dat_por_dia=$arr_res_ctes;
						$string_dat_por_dia = implode(",", $arr_dat_por_dia);
						//VISITAS ACUMULADAS
						$arr_res_v_acum= $ins_funciones->consulta_generica_st('CALL historial_visitas_mes('.$n_dia.','.$n_mes.','.$n_anio.',1)');
						$arr_dat_por_dia_ac=$arr_res_v_acum;
						$string_dat_por_dia_ac = implode(",", $arr_dat_por_dia_ac);
						//VISITAS REDENCION
						$arr_res_v_redim= $ins_funciones->consulta_generica_st('CALL historial_visitas_mes('.$n_dia.','.$n_mes.','.$n_anio.',2)');
						$arr_dat_por_dia_re=$arr_res_v_redim;
						$string_dat_por_dia_re = implode(",", $arr_dat_por_dia_re);
					?>
                </div>
                <div class="body">
                    <canvas  id="line_chart" height="150"></canvas>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="card" style="<?php echo $displaylvl; ?>" >
                <div class="header">
                    <h2>Usuarios por Nivel</h2>
					<?php
						///DATOS DE  NIVELES
						$sql_nom_niv= $ins_funciones->consulta_generica_all('SELECT * FROM tbl_niveles ');
						$str_nom_niv='';
						$str_num_usu_nivel='';
						$color_niv='';
						while($fila = $sql_nom_niv->fetch_assoc()){
								$str_nom_niv.= '"'.utf8_encode($fila['nombre']).'",';
								$sql_num_usu_nuv= $ins_funciones->consulta_generica_all('select count(*) as n  from tbl_usuario where id_nivel='.$fila['id_nivel']);
								$resp=$sql_num_usu_nuv->fetch_assoc();
								$str_num_usu_nivel.= '"'.$resp['n'].'",';
								$color_niv.= '"rgb('.rand(0,255).','.rand(0,255).','.rand(0,255).')",';
							}
					    $str_nom_niv=trim($str_nom_niv,',');
					    $str_num_usu_nivel=trim($str_num_usu_nivel,',');
					    $color_niv=trim($color_niv,',');
					?>
                </div>
                <div class="body">
                    <canvas id="bar_chart" height="150"></canvas>
                </div>
            </div>
        </div>               
    </div>          
    <div class="row clearfix">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="card" style="height:auto">
                <div class="header">
                	<?php 
                		$sql_tipo_proyecto= $ins_funciones->consulta_generica_all(" SELECT id_tipo_proyecto FROM tbl_configuracion_proyecto ");
						$row_tipo_proyecto = $sql_tipo_proyecto->fetch_assoc();
						
						$order = $row_tipo_proyecto['id_tipo_proyecto'] != 3 ? "puntos" : "num_visitas";
                	?>
                    <h2><?php echo $row_tipo_proyecto['id_tipo_proyecto'] != 3 ? "Puntos" : "Visitas" ?> acumulados/redimidos en el mes</h2>
					<?php
						//DATOS DE USUARIOS
						//$arrayPOS = array();
						
						$query = ' SELECT DISTINCT A.id_usuario_registro, B.usuario FROM tbl_registros AS A INNER JOIN tbl_usuario AS B ON (B.id_usuario = A.id_usuario_registro) WHERE A.fecha_registro BETWEEN DATE_FORMAT(NOW(), "%Y-%m-01 %H:%i:%s") AND NOW() AND A.id_tipo_registro != 3 ORDER BY A.fecha_registro ASC ';
						//echo($query);
						$sql_nom_us= $ins_funciones->consulta_generica_all($query);
						
						$arrayPOS = array('labels' => array(),'datasets' => array());
						//while($fila = $sql_nom_us->fetch_assoc()){
							$arrayAcumulado = array();
							$arrayRedimido = array();
							for ($i=0; $i < date("d"); $i++){
				                $fecha = date("Y-m-01");
				                $day = date('Y-m-d', strtotime($fecha.' + '.$i.' day'));
				                
				                //$filter = "id_registro_acumulacion IS NOT NULL AND fecha_registro BETWEEN '$day 00:00:00' AND '$day 23:59:59' AND id_tipo_registro = 1 AND id_usuario_registro = ".$fila['id_usuario_registro'];
				                $filter = "id_registro_acumulacion IS NOT NULL AND fecha_registro BETWEEN '$day 00:00:00' AND '$day 23:59:59' AND id_tipo_registro = 1 ";
				                
				                $arr_acumulados= $ins_funciones->consulta_generica_all(" SELECT SUM($order) AS total_day FROM tbl_registros WHERE $filter ");
				                $rowAcumulado = $arr_acumulados->fetch_assoc();				                
				                array_push($arrayAcumulado, $rowAcumulado['total_day'] != NULL ? (int)$rowAcumulado['total_day'] : 0);

				                //$filter = "id_registro_acumulacion IS NOT NULL AND fecha_registro BETWEEN '$day 00:00:00' AND '$day 23:59:59' AND id_tipo_registro = 2 AND id_usuario_registro = ".$fila['id_usuario_registro'];
				                $filter = "id_registro_acumulacion IS NOT NULL AND fecha_registro BETWEEN '$day 00:00:00' AND '$day 23:59:59' AND id_tipo_registro = 2 ";
				                $arr_redimidos= $ins_funciones->consulta_generica_all(" SELECT SUM($order) AS total_day FROM tbl_registros WHERE $filter ");
				                $rowRedimido = $arr_redimidos->fetch_assoc();
				                array_push($arrayRedimido, $rowRedimido['total_day'] != NULL ? (int)$rowRedimido['total_day'] : 0);

				                !in_array($day, $arrayPOS['labels']) && array_push($arrayPOS['labels'], $day);
							}

			                $color = rand(0,255).", ".rand(0,255).", ".rand(0,255);
			        		$border = "rgba(".$color.", 1)";
			        		$back = "rgba(".$color.", 0.3)" ;

							//$aux['label'] = $fila['usuario'];
							$aux['label'] = "Acumulacion";
							$aux['backgroundColor'] = $back;
							$aux['borderColor'] = $border;
							$aux['data'] = $arrayAcumulado;
							$aux['stack'] = 'Stack 0';

							array_push($arrayPOS['datasets'], $aux);

							$color = rand(0,255).", ".rand(0,255).", ".rand(0,255);
							$border = "rgba(".$color.", 1)";
			        		$back = "rgba(".$color.", 0.3)" ;

							//$aux1['label'] = $fila['usuario'];
							$aux1['label'] = "Redencion";
							$aux1['backgroundColor'] = $back;							
							$aux1['borderColor'] = $border;
							$aux1['data'] = $arrayRedimido;
							$aux1['stack'] = 'Stack 1';
							array_push($arrayPOS['datasets'], $aux1);							
						//}

						
						//print_r(json_encode($arrayPOS));
					?>
                </div>
                <div class="body">
                    <canvas id="bar_chart2" height="150"></canvas>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>Usuarios por Segmentos</h2>
					<?php
						///DATOS DE  SEGMENTOS
						$sql_nom_seg= $ins_funciones->consulta_generica_all('SELECT * FROM tbl_segmento WHERE activo=0;');
						$str_nom_seg='';
						$str_num_seg_usu='';
						$color_seg='';
						while ($fila = $sql_nom_seg->fetch_assoc()){
								$str_nom_seg.= '"'.utf8_encode($fila['nombre_segmento']).'",';
								$sql_num_seg= $ins_funciones->consulta_generica_all('select count(*) as n  from tbl_rel_segmento_usuario where id_segmento='.$fila['id_segmento'].' and activo=0;');
								$resp=$sql_num_seg->fetch_assoc();
								$str_num_seg_usu.= '"'.$resp['n'].'",';
								$color_seg.= '"rgb('.rand(0,255).','.rand(0,255).','.rand(0,255).')",';
						}
						$str_nom_seg=trim($str_nom_seg,',');
						$str_num_seg_usu=trim($str_num_seg_usu,',');
						$color_seg=trim($color_seg,',');
					?>
                </div>
                <div class="body">
                    <canvas id="pie_chart" height="150"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="../../inc/plugins/chartjs/Chart.bundle.js"></script>
<script src="../../inc/js/pages/charts/chartjs.js"></script>
<script src="https://www.chartjs.org/dist/2.8.0/Chart.min.js"></script>
<script>
	var ins_fecha = new Date();
	var dia_actual=ins_fecha.getDate();
	var x=[];
	var nom_segmentos=[<?php  echo $str_nom_seg; ?>];
	var nom_niveles=[<?php  echo $str_nom_niv; ?>];
	//var nom_usu_top=[<?php  echo $str_num_usu_acum; ?>];

	var datos_h_ctes=[<?php  echo $string_dat_por_dia; ?>];
	//console.log(datos_h_ctes);
	var datos_segmentos=[<?php  echo $str_num_seg_usu; ?>];
	var datos_niveles=[<?php  echo $str_num_usu_nivel; ?>];
	var colores_segmentos=[<?php  echo $color_seg; ?>];
	var colores_niveles=[<?php  echo $color_niv; ?>];
	var datos_h_acumula=[<?php  echo $string_dat_por_dia_ac; ?>];
	var datos_h_redime=[<?php  echo $string_dat_por_dia_re; ?>];
		for(var i=1;i<=dia_actual;i++){
			x.push(i);
					}
	//HISTORIAL CTES MES ACTUAL				
	var line = document.getElementById("line_chart").getContext('2d');
	var myLineChart = new Chart(line,{
		type: 'line',
		data: {
			labels:x,
			datasets: [{
					label: '# Cts. Registrados ',
					data:datos_h_ctes,
					<?php $color = rand(0,255).", ".rand(0,255).", ".rand(0,255); ?>
			        <?php $border = "rgba(".$color.", 1)" ;?>
			        <?php $back = "rgba(".$color.", 0.2)" ;?>
			        backgroundColor: ['<?php echo $back; ?>'],
			        borderColor: ['<?php echo $border; ?>'],
			        borderWidth: 1
				},{
					label: '# Visitas Acumulación ',
					data:datos_h_acumula,
					<?php $color = rand(0,255).", ".rand(0,255).", ".rand(0,255); ?>
			        <?php $border = "rgba(".$color.", 1)" ;?>
			        <?php $back = "rgba(".$color.", 0.2)" ;?>
			        backgroundColor: ['<?php echo $back; ?>'],
			        borderColor: ['<?php echo $border; ?>'],
			        borderWidth: 1
				}
				,{
					label: '# Visitas Redención ',
					data:datos_h_redime,
					<?php $color = rand(0,255).", ".rand(0,255).", ".rand(0,255); ?>
			        <?php $border = "rgba(".$color.", 1)" ;?>
			        <?php $back = "rgba(".$color.", 0.2)" ;?>
			        backgroundColor: ['<?php echo $back; ?>'],
			        borderColor: ['<?php echo $border; ?>'],
			        borderWidth: 1
				}
			]
		}
	});
	//SEGMENTOS
	var pie = document.getElementById("pie_chart").getContext('2d');
	var myPieChart = new Chart(pie, {
		type: 'pie',
				data: {
					datasets: [{
						data:datos_segmentos,
						backgroundColor:colores_segmentos,
					}],
					labels:nom_segmentos
				},
				options: {
					responsive: true,
					legend: false
				}
		
	});
	//USUARIOS POR NIVEL
	var bar = document.getElementById("bar_chart").getContext('2d');
	var myBarChart = new Chart(bar, {
		type: 'bar',
		data: {
			labels: nom_niveles,
			datasets: [{
				label: '# Usuarios en Nivel',
				data: datos_niveles,
				backgroundColor: colores_niveles,
				borderColor: colores_niveles,
				borderWidth: 1
			}]
		},
		options: {
			barValueSpacing: 20,
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
					}
				}]
			}
		}
	});
	//TOP 5 ACTIVIDADES
	var bar = document.getElementById("bar_chart2").getContext('2d');
	var myBarChart = new Chart(bar, {
		type: 'bar',
		data: <?php echo json_encode($arrayPOS);?>,
		//data: {"labels":["2019-05-01","2019-05-02","2019-05-03","2019-05-04","2019-05-05","2019-05-06","2019-05-07","2019-05-08","2019-05-09"],"datasets":[{"label":"Allan","backgroundColor":"rgba(176, 97, 163, 0.2)","borderColor":"rgba(176, 97, 163, 1)","data":[0,3800,16780,0,0,0,0,0,0],stack:"Stack 0"},{"label":"Allan","backgroundColor":"rgba(176, 97, 163, 0.2)","borderColor":"rgba(176, 97, 163, 1)","data":[0,4200,4000,0,0,4000,3200,0,0],stack:"Stack 1"},{"label":"Rogelio","backgroundColor":"rgba(24, 1, 227, 0.2)","borderColor":"rgba(24, 1, 227, 1)","data":[0,0,0,0,0,1000,0,0,0],stack:"Stack 0"},{"label":"Rogelio","backgroundColor":"rgba(24, 1, 227, 0.2)","borderColor":"rgba(24, 1, 227, 1)","data":[0,0,0,0,0,0,1200,0,0],stack:"Stack 1"}]},
		options: {			
			tooltips: {
				mode: 'index',
				intersect: true
			},

			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
				}],
				yAxes: [{
					stacked: true
				}]
			}
		}
	});
	</script>
						
						
<?php  }else{
      $redirec= "../../" ;
      header('Location:'.$redirec );
 }
