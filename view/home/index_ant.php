
<?php 
/*************************************************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#- Vista de Home -#
##################################################################################### 
include_once '../../inc/cont_fijos.php';
include_once '../../inc/funciones.php';
include_once '../../inc/parametros.php';
$ins_cont_fijos=new Contenidos_Fijos($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
session_start();
if(isset($_SESSION["usuario"])){ 
?>
<!DOCTYPE html>
<html lang="es">
<head>
<?php  
 $ins_cont_fijos->head();
 //TRAEMOS CONFIGURACION DE ESTILOS
 $res_con=$ins_funciones->consulta_generica('tbl_estilo', ' ');
 $registro= mysqli_fetch_assoc($res_con);
 //VALIDAMOS LOGO
if($registro['logo_cte']==''){
    $img_logo='imagenes/logo.png';
}else{
    $img_logo='imagenes/img_configuracion/'.$registro['logo_cte'];
}
 //VALIDAMOS IMAGEN FONDO
if($registro['fondo_cte']==''){
     $img_background='imagenes/background_default.jpg';
}else{
    $img_background='imagenes/img_configuracion/'.$registro['fondo_cte'];
}
 //VALIDAMOS COLOR PRIMARIO
if($registro['color_primario']==''){
     $color_primario='#006AA9';
}else{
    $color_primario=$registro['color_primario'];
}
 //VALIDAMOS COLOR SECUNDARIO
if($registro['color_secundario']==''){
     $color_secundario='#006AA9';
}else{
    $color_secundario=$registro['color_secundario'];
}
  ?>
</head>
<?php
//LLAMAMOS ESTILOS DE CONFIGURACION
$ins_cont_fijos->estilos($img_logo,$img_background,$color_primario,$color_secundario);
?>
<body class="theme-red">
    <!-- Pag. Loading-->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Espera un poco...</p>
        </div>
    </div>
    <!-- Barra Ariba -->
    <nav class="navbar" style='height: 70px;'> 
        <div class="container-fluid">
            <div class="navbar-header">
                 <a href="javascript:void(0);" class="bars"></a>
                <span class="navbar-brand" >Plataforma Lealtad </span>
            </div>
        </div>
    </nav>
    <section>
        <!-- MeNU IZQUIERDO -->
        <aside id="leftsidebar" class="sidebar">
            <!-- Usuario-->
            <div class="user-info">
                <div class="image">
                    <img src="../../inc/imagenes/user.png" width="48" height="48" alt="User" />
                    <img src="../../inc/<?php echo $img_logo; ?>" style='    margin-left: 130px;' width="60" height="60" alt="Logo" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION["usuario"];?></div>
                    <!--<div class="email">john.doe@example.com</div>-->
                </div>
            </div>
            <!-- MENU -->
            <div class="menu">
                <ul class="list">
                    <?php echo $ins_cont_fijos->menu($_SESSION["id_usuario"]);?>
                </ul>
            </div>
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                        <?php 
                        $ins_cont_fijos->footer();
                        ?>
                </div>
            </div>
        </aside>
    </section>
<!-- CONTENIDO -->
    <section class="content">
        <div id="all-content" class="row container-fluid">
          <div id="card-content" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
          </div>  
            <?php 
            //include('widgets.php');
            ?>
        </div>
        <?php 
            switch ($_SESSION["rol"]){
            //VISTA ADMINISTRADOR  
                case 1:
                    include './admin.php';
                break;
            //VISTA PUNTO DE VENTA
                case 2:
                    echo 'vista  inicio punto de venta';
                break;
            //VISTA USUARIO
                case 3:
                   include './usuario.php';
                break;
                default:
                    echo ':(';
                break;
                       } 
        ?>     
    </section>
</body>
</html>
 <?php }else{
      $redirec= "../../" ;
      header('Location:'.$redirec );
 }
