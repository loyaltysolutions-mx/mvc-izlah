<?php 
/*************************************************************************************************************************/
#- Developed By:Miguel Ruiz -#
##+> ################################# <+##
#- Vista de Home -#
##################################################################################### 
include_once './inc/cont_fijos.php';
include_once './inc/funciones.php';
include_once './inc/parametros.php';

$ins_cont_fijos=new Contenidos_Fijos($ser,$usu,$pas,$bd);
$ins_funciones=new Funciones_Basicas();
$configuracion = $ins_funciones->consulta_generica2(' tbl_rel_config_campos_registro AS A INNER JOIN tbl_campos_registro AS B ON (A.id_campo = B.id_campos_registro) ', ' WHERE A.clave = 1 ');
$configuracion = mysqli_fetch_assoc($configuracion);
session_start();

//TRAEMOS CONFIGURACION DE ESTILOS
 $res_con1=$ins_funciones->consulta_generica2('tbl_estilo', ' ');
 $registro1= mysqli_fetch_assoc($res_con1);

 $res_config=$ins_funciones->consulta_generica2('tbl_configuracion_proyecto', ' ');
 $registro_config= mysqli_fetch_assoc($res_config);
 //VALIDAMOS LOGO
if($registro1['logo_cte']==''){
    $img_logo1='imagenes/logo.png';
}else{
    $img_logo1='imagenes/img_configuracion/'.$registro1['logo_cte'];
}


?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta content="yes" name="mobile-web-app-capable">
    <link rel="icon" sizes="192x192" href="./inc/<?php echo $img_logo1 ?>">
    <title><?php echo $registro_config['titulo_home'] ?></title>
    <!-- Favicon-->
    
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="./inc/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="./inc/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="./inc/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="./inc/css/style.css" rel="stylesheet">
</head>
<style>
    .msg{
        font-weight: bold;
        text-decoration: underline;
    }
    /*
 .login-page {
    padding-left: 0;
    max-width: 360px;
    margin: 10% auto;
    overflow-x: hidden;
    width: 100%;
    height: 400px;
    background-image: url('./inc/imagenes/fondo.jpg');
    background-repeat: no-repeat;
    background-size: cover;
   
}   
.navbar {
    background-color: #95542E!important;
}
a{
        color: white!important;
}
    */
</style>
<?php  

 //TRAEMOS CONFIGURACION DE ESTILOS
 $res_con=$ins_funciones->consulta_generica2('tbl_estilo', ' ');
 $registro= mysqli_fetch_assoc($res_con);
 //VALIDAMOS LOGO
if($registro['logo_cte']==''){
    $img_logo='imagenes/logo.png';
}else{
    $img_logo='imagenes/img_configuracion/'.$registro['logo_cte'];
}
 //VALIDAMOS IMAGEN FONDO
if($registro['fondo_cte']==''){
     $img_background='imagenes/background_default.jpg';
}else{
    $img_background='imagenes/img_configuracion/'.$registro['fondo_cte'];
}
 //VALIDAMOS COLOR PRIMARIO
if($registro['color_primario']==''){
     $color_primario='#006AA9';
}else{
    $color_primario=$registro['color_primario'];
}
 //VALIDAMOS COLOR SECUNDARIO
if($registro['color_secundario']==''){
     $color_secundario='#006AA9';
}else{
    $color_secundario=$registro['color_secundario'];
}
  ?>

<style type="text/css">
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus {        
        background-color: rgba(255,255,255,0) !important;        
    }

    .nav-tabs > li > a:before {
        border-bottom: 2px solid black !important;
    }

    #btnRecovery{
        color:<?php echo $color_primario ?> !important; 
        font-weight:bold; 
        font-size:1.5rem !important; 
        text-decoration:underline;
    }

    #btnRecovery:hover{
        color:white !important;        
    }
</style>
</head>
<?php
//LLAMAMOS ESTILOS DE CONFIGURACION
$ins_cont_fijos->estilos_home($img_logo,$img_background,$color_primario,$color_secundario);
?>


<body class="login-page">
    <nav class="navbar" style="height: 70px;"> 
        <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
           <button type="button" class="navbar-toggle " data-toggle="collapse" data-target="#navbar" ></button>
            <a class="navbar-brand" href="#">Sitio web de Cliente Registrate</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse" aria-expanded="false" style="height: 1px;">
            <ul class="nav navbar-nav navbar-right">
              <li><a href="./">Acceso <span class="sr-only">(current)</span></a></li>
              <li><a href="#">Terminos y Condiciones</a></li>
              <li><a href="#">FAQ</a></li>
              
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
    </nav>
    <div class="login-box">
        <div class="card" style="background: rgba(255,255,255,0.4) !important;">
            <div class="body">                
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#home_with_icon_title" data-toggle="tab">
                            <i class="fa fa-user"></i> Cliente
                        </a>
                    </li>
                    <li role="presentation">
                        <a href="#profile_with_icon_title" data-toggle="tab">
                            <i class="fa fa-store"></i> Punto Venta / Admin
                        </a>
                    </li>                        
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade in active" id="home_with_icon_title">
                        <form id="sign_in" action="./controller/login/c_login.php?op=1" method="post">
                            <h4 style="text-align:center; text-decoration:underline; color:black">Iniciar sesión</h4>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">phone_android</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="usuario" placeholder="Ingresa tu <?php echo utf8_encode($configuracion['nombre']) ?>" style="padding:8px;">
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">
                                    <input type="password" class="form-control" name="password" placeholder="Ingresa tu contraseña" style="padding:8px">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12" style="text-align:center">
                                    <button name="iniciar" type="submit" class="btn btn_color  btn-sm" style="color:white !important;"></i> &nbsp;Ingresar</button>
                                </div>
                            </div>
                            <div class="row m-t-15 m-b--20">
                                <div class="col-xs-12">
                                    <a id="btnRecovery" href="./view/registro/recovery.php"><small><b>Olvidé mi contraseña</b></small></a>
                                </div>
                            </div>
                            <?php  
                                $res=$_REQUEST['msg'];
                                if(isset($res) && $res==1){ 
                            ?>
                            <div class="row">
                                <div class="col-xs-12"> 
                                    <br>
                                    <div class="alert alert-danger" role="alert" style='margin-top:-30px;'>
                                        Usuario y/o Password incorrecto
                                    </div>
                                </div>
                            </div>
                            <?php  } ?>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="profile_with_icon_title">
                        <form id="sign_in1" action="./controller/login/c_login.php?op=1" method="post">
                            <h4 style="text-align:center; text-decoration:underline; color:black">Iniciar sesión</h4>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="usuario" placeholder="Usuario" style="padding:3px">
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">
                                    <input type="password" class="form-control" name="password" placeholder="Contraseña" style="padding:3px">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12" style="text-align:center">
                                    <button name="iniciar" type="submit" class="btn btn_color  btn-sm" style="color:white !important;"></i> &nbsp;Ingresar</button>
                                </div>
                            </div>
                            <?php  
                                $res=$_REQUEST['msg'];
                                if(isset($res) && $res==1){ 
                            ?>
                            <div class="row">
                                <div class="col-xs-12"> 
                                    <br>
                                    <div class="alert alert-danger" role="alert" style='margin-top:-30px;'>
                                        Usuario y/o Password incorrecto
                                    </div>
                                </div>
                            </div>
                            <?php  } ?>
                        </form>
                    </div>
                </div>                
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="./inc/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="./inc/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="./inc/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="./inc/plugins/jquery-validation/jquery.validate.js"></script>

    <!-- Custom Js -->
    <script src="./inc/js/admin.js"></script>
    <script src="./inc/js/pages/examples/sign-in.js"></script>
    <script type="text/javascript">
        sessionStorage.clear();
    </script>
</body>

</html>