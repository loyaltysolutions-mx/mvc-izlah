<?php

class M_reporte_clientes{

    private $ser;
    private $usu;
    private $pas;
    private $bd;    

    public function __construct($ser,$usu,$pas,$bd) {
        //mb_internal_encoding("UTF-8");
        $this->ser=$ser;
        $this->usu=$usu;
        $this->pas=$pas;
        $this->bd=$bd;
        $this->res_con=$this->conecta_bd($ser,$usu,$pas,$bd);         
    }

    private function cleanEspacio($txt){
        return preg_replace("/(\s*)/i","",trim($txt));
    }

    private function cleanSQL($query){
        return preg_replace("/\s(AND|OR|>|<|=|IN|NOT|BETWEEN|LIKE|XOR|UNION|JOIN|INNER)\s/i"," ",$query);
    }


    //FUNCION OBTIENE CLIENTES SEGMENTO
    public function getClientsSegmento($idSegmento = NULL, $idUsuario = NULL){
        $filter=" B.id_rel_segmento_usuario IS NOT NULL AND C.rol = 3 ";
        $this->cleanEspacio($idSegmento) && $filter.= " AND B.id_segmento = ".$this->cleanSQL($idSegmento);
        $this->cleanEspacio($idUsuario) && $filter.= " AND B.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT * FROM tbl_segmento AS A INNER JOIN tbl_rel_segmento_usuario AS B ON (A.id_segmento = B.id_segmento) INNER JOIN tbl_usuario AS C ON (B.id_usuario = C.id_usuario) WHERE $filter ";        
        return $this->res_con->query($sql);        
    }

    //FUNCION OBTIENE CLIENTES SEGMENTO
    public function getClientsSegmento1($idSegmento = NULL, $idUsuario = NULL){
        $filter=" B.id_rel_segmento_usuario IS NOT NULL AND C.rol = 3 ";
        $this->cleanEspacio($idSegmento) && $filter.= " AND B.id_segmento = ".$this->cleanSQL($idSegmento);
        $this->cleanEspacio($idUsuario) && $filter.= " AND B.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT * FROM tbl_segmento AS A INNER JOIN tbl_rel_segmento_usuario AS B ON (A.id_segmento = B.id_segmento) INNER JOIN tbl_usuario AS C ON (B.id_usuario = C.id_usuario) WHERE $filter ";        
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    function getSegmentos($idSegmento = NULL, $idUsuario = NULL){
        $filter=" A.id_segmento IS NOT NULL ";
        $this->cleanEspacio($idSegmento) && $filter.= " AND A.id_segmento = ".$this->cleanSQL($idSegmento);
        $this->cleanEspacio($idUsuario) && $filter.= " AND B.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT DISTINCT A.id_segmento, A.nombre_segmento, A.descripcion_segmento FROM tbl_segmento AS A LEFT JOIN tbl_rel_segmento_usuario AS B ON (A.id_segmento = B.id_segmento) WHERE $filter ";        
        $result = $this->res_con->query($sql);
        return $this->arrayToAssoc($result);
    }

    function getNiveles($idNivel = NULL, $idUsuario = NULL){
        $filter=" A.id_nivel IS NOT NULL ";
        $this->cleanEspacio($idNivel) && $filter.= " AND A.id_nivel = ".$this->cleanSQL($idNivel);
        $this->cleanEspacio($idUsuario) && $filter.= " AND B.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT DISTINCT A.nombre, A.id_nivel FROM tbl_niveles AS A LEFT JOIN tbl_usuario AS B ON (A.id_nivel = B.id_nivel) WHERE $filter ";

        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    function getTarjetas($idUsuario = NULL){
        $filter=" A.id_tarjeta IS NOT NULL ";        
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT * FROM tbl_rel_usuario_tarjeta AS A WHERE $filter ";

        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    function getUsers($idUser = NULL){
        $filter=" A.id_usuario IS NOT NULL ";
        $this->cleanEspacio($idUser) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUser);

        $sql = " SELECT A.*, B.nombre AS nombreNivel FROM tbl_usuario AS A INNER JOIN tbl_niveles AS B ON (A.id_nivel = B.id_nivel) WHERE $filter ";

        return $this->arrayToAssoc($this->res_con->query($sql));
    }
        
    public function getReporteClientes($idUsuario = NULL, $idSegmento = NULL, $fechaRegistro = NULL, $edad = NULL, $genero = NULL, $status = NULL, $idNivel = NULL){
        $filter=" A.id_usuario IS NOT NULL AND A.rol = 3";
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);
        $this->cleanEspacio($idSegmento) && $filter.= " AND (B.id_segmento = ".$idSegmento.")";
        $this->cleanEspacio($fechaRegistro) && $filter.= " AND A.fecha_registro_plataforma BETWEEN '".$fechaRegistro;        
        $this->cleanEspacio($edad) && $filter.= " AND A.fecha_nacimiento  ".$edad;
        $genero != NULL && $filter.= ' AND A.genero = '.$genero;
        $status != NULL && $filter.= ' AND A.activo = '.$status;
        $this->cleanEspacio($idNivel) && $filter.= ' AND (A.id_nivel = '.$idNivel.")";

        $sql = " SELECT DISTINCT(A.id_usuario) FROM tbl_usuario AS A INNER JOIN tbl_rel_segmento_usuario AS B ON (B.id_usuario = A.id_usuario) INNER JOIN tbl_segmento AS C ON (C.id_segmento = B.id_segmento) LEFT JOIN tbl_niveles AS D ON (A.id_nivel = D.id_nivel) WHERE $filter ";
        //die($sql);
        $result = $this->res_con->query($sql);
        return $this->arrayToAssoc($result);
    }

    public function getPromedioMontoTicket($idUsuario = NULL){
        $filter=" A.id_registro_acumulacion IS NOT NULL ";
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);        

        $sql = " SELECT ROUND(AVG(A.monto_ticket),2) AS promedio FROM tbl_registros AS A WHERE $filter ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }


    private function arrayToAssoc($result){
        $auxArray = array();
        while ($fila = mysqli_fetch_assoc($result)) {
            foreach ($fila as $key => $value) {
                $fila[$key] = utf8_encode($value);
            }
            array_push($auxArray, $fila);
        }
        return $auxArray;
    }

    //FUNCION CONECTA A BASE DE DATOS
    public function conecta_bd($ser,$usu,$pas,$bd){
        $con = mysqli_connect($ser,$usu,$pas,$bd);
        if ($con){
            return $con;
            mysqli_close($con);
        }else{
       echo("Error description: " . mysqli_error($con));
             exit(); 
        }
    }
    
}
