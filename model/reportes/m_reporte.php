<?php

class M_reporte{

    private $ser;
    private $usu;
    private $pas;
    private $bd;    

    public function __construct($ser,$usu,$pas,$bd) {
        //mb_internal_encoding("UTF-8");
        $this->ser=$ser;
        $this->usu=$usu;
        $this->pas=$pas;
        $this->bd=$bd;
        $this->res_con=$this->conecta_bd($ser,$usu,$pas,$bd);         
    }

    private function cleanEspacio($txt){
        return preg_replace("/(\s*)/i","",trim($txt));
    }

    private function cleanSQL($query){
        return preg_replace("/\s(AND|OR|>|<|=|IN|NOT|BETWEEN|LIKE|XOR|UNION|JOIN|INNER)\s/i"," ",$query);
    }

    public function getMin($table = NULL, $field = NULL){
        if($field != NULL && $table != NULL){
            $sql = " SELECT MIN($field) AS min FROM $table WHERE rol = 3 ";
            //die($sql);
            return $this->res_con->query($sql); 
        }
        return false;
    }

    public function getMax($table = NULL, $field = NULL){
        if($field != NULL && $table != NULL){
            $sql = " SELECT MAX($field) AS max FROM $table WHERE rol = 3 ";
            //die($sql);
            return $this->res_con->query($sql); 
        }
        return false;
    }

    public function getMinPuntos($field = NULL){
        if($field != NULL){
            $sql = " SELECT MIN($field) AS min FROM tbl_puntos_totales AS A INNER JOIN tbl_usuario AS B ON (A.id_usuario = B.id_usuario) WHERE B.rol = 3 ";
            //die($sql);
            return $this->res_con->query($sql); 
        }
        return false;
    }

    public function getMaxPuntos($field = NULL){
        if($field != NULL){
            $sql = " SELECT MAX($field) AS max FROM tbl_puntos_totales AS A INNER JOIN tbl_usuario AS B ON (A.id_usuario = B.id_usuario) WHERE B.rol = 3 ";
            //die($sql);
            return $this->res_con->query($sql); 
        }
        return false;
    }   

    //FUNCION OBTIENE CLIENTES SEGMENTO
    public function getClientsSegmento($idSegmento = NULL, $idUsuario = NULL){
        $filter=" B.id_rel_segmento_usuario IS NOT NULL AND C.rol = 3 ";
        $this->cleanEspacio($idSegmento) && $filter.= " AND B.id_segmento = ".$this->cleanSQL($idSegmento);
        $this->cleanEspacio($idUsuario) && $filter.= " AND B.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT * FROM tbl_segmento AS A INNER JOIN tbl_rel_segmento_usuario AS B ON (A.id_segmento = B.id_segmento) INNER JOIN tbl_usuario AS C ON (B.id_usuario = C.id_usuario) WHERE $filter ";        
        return $this->res_con->query($sql);        
    }

    function getSegmentos($idSegmento = NULL){
        $filter=" A.id_segmento IS NOT NULL ";
        $this->cleanEspacio($idSegmento) && $filter.= " AND A.id_segmento = ".$this->cleanSQL($idSegmento);

        $sql = " SELECT * FROM tbl_segmento AS A WHERE $filter ";        
        return $this->res_con->query($sql);
    }

    function getTipoPV($idTipo = NULL){
        $filter="A.id_tipo_punto_venta IS NOT NULL ";
        $this->cleanEspacio($idTipo) && $filter.= " AND id_tipo_punto_venta = ".$this->cleanSQL($idTipo);

        $sql = " SELECT A.id_tipo_punto_venta AS idTipo, A.nombre AS nombreTipo FROM tbl_tipo_punto_venta AS A WHERE $filter ";        
        $tipos = $this->res_con->query($sql);
        
        return $this->arrayToAssoc($tipos);
    }

    function getUsers($idUsuario = NULL){
        $filter="A.id_usuario IS NOT NULL AND A.rol = 3";
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT * FROM tbl_usuario AS A WHERE $filter ";        
        $tipos = $this->res_con->query($sql);
        
        return $this->arrayToAssoc($tipos);   
    }

    function getNivel($idNivel = NULL){
        $filter=" A.id_nivel IS NOT NULL ";
        $this->cleanEspacio($idNivel) && $filter.= " AND A.id_nivel = ".$this->cleanSQL($idNivel);

        $sql = " SELECT * FROM tbl_niveles AS A WHERE $filter ";        
        $tipos = $this->res_con->query($sql);
        
        return $this->arrayToAssoc($tipos);
    }

    function getPuntosVenta($idPuntoVenta = NULL){
        $filter="A.id_usuario IS NOT NULL AND A.rol = 2";
        $this->cleanEspacio($idPuntoVenta) && $filter.= " AND id_usuario = ".$this->cleanSQL($idPuntoVenta);

        $sql = " SELECT * FROM tbl_usuario AS A WHERE $filter ";
        $tipos = $this->res_con->query($sql);
        
        return $this->arrayToAssoc($tipos);
    }
        
    //FUNCION CONECTA A BASE DE DATOS
    public function conecta_bd($ser,$usu,$pas,$bd){
        $con = mysqli_connect($ser,$usu,$pas,$bd);
        if ($con){
            return $con;
            mysqli_close($con);
        }else{
       echo("Error description: " . mysqli_error($con));
             exit(); 
        }
    }

    public function getReporteClientes($idSegmento = NULL, $fechaRegistro = NULL, $edad = NULL, $genero = NULL, $status = NULL){
        $filter=" A.id_usuario IS NOT NULL AND A.rol = 3";
        $this->cleanEspacio($idSegmento) && $filter.= " AND B.id_segmento = ".$this->cleanSQL($idSegmento);
        $this->cleanEspacio($fechaRegistro) && $filter.= " AND A.fecha_registro_plataforma BETWEEN '".$fechaRegistro;        
        $this->cleanEspacio($edad) && $filter.= " AND A.fecha_nacimiento  ".$edad;
        $genero != NULL && $filter.= ' AND A.genero = '.$genero;
        $status != NULL && $filter.= ' AND A.activo = '.$status;        

        $sql = " SELECT A.*".($idSegmento != NULL ? ",C.nombre_segmento, C.activo AS activoSegmento" : "")." FROM tbl_usuario AS A ".($idSegmento != NULL ? "INNER JOIN tbl_rel_segmento_usuario AS B ON (B.id_usuario = A.id_usuario) INNER JOIN tbl_segmento AS C ON (C.id_segmento = B.id_segmento)" : "")." WHERE $filter ";
        //die($sql);
        $result = $this->res_con->query($sql);
        $arrayResult = array();
        while ($fila = mysqli_fetch_assoc($result)){
            array_push($arrayResult, $fila);
        }

        return $arrayResult;        
    }

    public function getReporteActividadesPV($idPuntoVenta = NULL, $tipo = NULL, $fechaRegistro = NULL, $diasSemana = NULL, $ticketPromedio = NULL, $totalPuntos = NULL, $totalVisitas = NULL){
        $filter="A.id_registro_acumulacion IS NOT NULL";
        $this->cleanEspacio($idPuntoVenta) && $filter.= " AND (A.id_usuario_registro = ".$idPuntoVenta.")";
        $this->cleanEspacio($tipo) && $filter.= " AND (D.id_tipo_punto_venta = ".$tipo.")";
        $this->cleanEspacio($fechaRegistro) && $filter.= " AND A.fecha_registro BETWEEN '".$fechaRegistro;
        $this->cleanEspacio($totalPuntos) && $filter.= " AND A.puntos BETWEEN ".$totalPuntos;
        $this->cleanEspacio($ticketPromedio) && $filter.= " AND A.monto_ticket BETWEEN ".$ticketPromedio;
        $this->cleanEspacio($diasSemana) && $filter.= ' AND (A.dia_registro = "'.$diasSemana.")";
        $this->cleanEspacio($totalVisitas) && $filter.= ' AND A.num_visita BETWEEN '.$totalVisitas;

        //$sql = " SELECT * FROM tbl_usuario AS A ".($idSegmento != NULL ? "INNER JOIN tbl_rel_segmento_usuario AS B ON (B.id_usuario = A.id_usuario) INNER JOIN tbl_segmento AS C ON (C.id_segmento = B.id_segmento)" : "").($fechaRegistro == NULL && $totalPuntos == NULL ? "" :"INNER JOIN tbl_puntos_totales AS D ON (D.id_usuario = A.id_usuario)").($ticketPromedio == NULL && $diasSemana == NULL ? "" :" INNER JOIN tbl_registros AS E ON (E.id_usuario = A.id_usuario)")." WHERE $filter ";
        $sql = " SELECT A.*, D.id_tipo_punto_venta AS idTipo, C.nombre AS nombreTipo, D.id_usuario AS idPuntoVenta, D.usuario AS nombrePV, B.nombre AS nombreCliente, B.email AS emailCliente, B.rfc AS RFC_Cliente, E.nombre AS nombreTipoRegistro, F.nombre AS nombreProductoRedencion, G.nombre AS nombrePromo FROM tbl_registros AS A INNER JOIN tbl_usuario AS B ON (A.id_usuario = B.id_usuario) LEFT JOIN tbl_usuario AS D ON (A.id_usuario_registro = D.id_usuario) LEFT JOIN tbl_tipo_punto_venta AS C ON (D.id_tipo_punto_venta = C.id_tipo_punto_venta) LEFT JOIN tbl_tipo AS E ON (E.id_tipo = A.id_tipo_registro) LEFT JOIN tbl_cat_premios_productos_servicios AS F ON (F.id_cat_premios_productos_servicios = A.id_cat_premio) LEFT JOIN tbl_promociones AS G ON (G.id_promociones = A.id_promo) WHERE $filter ";
        //die($sql);
        $result = $this->res_con->query($sql);
        
        return $this->arrayToAssoc($result);
    }

    public function getReporteHistorialClientes($idUsuario = NULL, $idSegmento = NULL, $idNivel = NULL, $fechaRegistro = NULL){
        $filter="A.id_registro_acumulacion IS NOT NULL";
        $this->cleanEspacio($idUsuario) && $filter.= " AND (A.id_usuario = ".$idUsuario.")";
        $this->cleanEspacio($idSegmento) && $filter.= " AND (B.id_segmento = ".$idSegmento.")";
        $this->cleanEspacio($fechaRegistro) && $filter.= " AND A.fecha_registro BETWEEN '".$fechaRegistro;
        $this->cleanEspacio($idNivel) && $filter.= " AND (E.id_nivel = ".$idNivel.")";        

        //$sql = " SELECT * FROM tbl_usuario AS A ".($idSegmento != NULL ? "INNER JOIN tbl_rel_segmento_usuario AS B ON (B.id_usuario = A.id_usuario) INNER JOIN tbl_segmento AS C ON (C.id_segmento = B.id_segmento)" : "").($fechaRegistro == NULL && $totalPuntos == NULL ? "" :"INNER JOIN tbl_puntos_totales AS D ON (D.id_usuario = A.id_usuario)").($ticketPromedio == NULL && $diasSemana == NULL ? "" :" INNER JOIN tbl_registros AS E ON (E.id_usuario = A.id_usuario)")." WHERE $filter ";
        $sql = " SELECT DISTINCT A.*, D.nombre AS nombreCliente, E.nombre AS nombreNivel, F.nombre AS nombreTipo, G.nombre AS nombrePromocion, G.cashback, G.cashbackperc, G.descuento, G.tipo as tipoPromo  FROM tbl_registros AS A LEFT JOIN tbl_rel_segmento_usuario AS B ON (A.id_usuario = B.id_usuario) LEFT JOIN tbl_segmento AS C ON (C.id_segmento = B.id_segmento) INNER JOIN tbl_usuario AS D ON (A.id_usuario = D.id_usuario) LEFT JOIN tbl_niveles AS E ON (E.id_nivel = D.id_nivel) LEFT JOIN tbl_tipo AS F ON (F.id_tipo = A.id_tipo_registro) LEFT JOIN tbl_promociones AS G ON (A.id_promo = G.id_promociones) WHERE $filter ";
        //die($sql);
        $result = $this->res_con->query($sql);
        
        return $this->arrayToAssoc($result);
    }

    public function getUsersHistorialClientes($idUsuario = NULL, $idSegmento = NULL, $idNivel = NULL, $fechaRegistro = NULL){
        $filter="A.id_registro_acumulacion IS NOT NULL";
        $this->cleanEspacio($idUsuario) && $filter.= " AND (A.id_usuario = ".$idUsuario.")";
        $this->cleanEspacio($idSegmento) && $filter.= " AND (B.id_segmento = ".$idSegmento.")";
        $this->cleanEspacio($fechaRegistro) && $filter.= " AND A.fecha_registro BETWEEN '".$fechaRegistro;
        $this->cleanEspacio($idNivel) && $filter.= " AND (E.id_nivel = ".$idNivel.")";        

        //$sql = " SELECT * FROM tbl_usuario AS A ".($idSegmento != NULL ? "INNER JOIN tbl_rel_segmento_usuario AS B ON (B.id_usuario = A.id_usuario) INNER JOIN tbl_segmento AS C ON (C.id_segmento = B.id_segmento)" : "").($fechaRegistro == NULL && $totalPuntos == NULL ? "" :"INNER JOIN tbl_puntos_totales AS D ON (D.id_usuario = A.id_usuario)").($ticketPromedio == NULL && $diasSemana == NULL ? "" :" INNER JOIN tbl_registros AS E ON (E.id_usuario = A.id_usuario)")." WHERE $filter ";
        $sql = " SELECT DISTINCT(A.id_usuario) FROM tbl_registros AS A LEFT JOIN tbl_rel_segmento_usuario AS B ON (A.id_usuario = B.id_usuario) LEFT JOIN tbl_segmento AS C ON (C.id_segmento = B.id_segmento) INNER JOIN tbl_usuario AS D ON (A.id_usuario = D.id_usuario) LEFT JOIN tbl_niveles AS E ON (E.id_nivel = D.id_nivel) INNER JOIN tbl_tipo AS F ON (F.id_tipo = A.id_tipo_registro) WHERE $filter ";
        //die($sql);
        $result = $this->res_con->query($sql);
        
        return $this->arrayToAssoc($result);
    }

    function getSegmentos1($idSegmento = NULL, $idUsuario = NULL){
        $filter=" A.id_segmento IS NOT NULL ";
        $this->cleanEspacio($idSegmento) && $filter.= " AND A.id_segmento = ".$this->cleanSQL($idSegmento);
        $this->cleanEspacio($idUsuario) && $filter.= " AND B.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT DISTINCT A.id_segmento, A.nombre_segmento, A.descripcion_segmento FROM tbl_segmento AS A LEFT JOIN tbl_rel_segmento_usuario AS B ON (A.id_segmento = B.id_segmento) WHERE $filter ";        
        $result = $this->res_con->query($sql);
        return $this->arrayToAssoc($result);
    }

    public function getDistinctPromociones($idSucursal = NULL, $idSegmento = NULL, $idNivel = NULL){
        $filter="A.id_rel IS NOT NULL";
        $this->cleanEspacio($idSucursal) && $filter.= ' AND (A.id_sucursal = '.$idSucursal.')';
        $this->cleanEspacio($idSegmento) != NULL && $filter.= ' AND (A.id_segmento = '.$idSegmento.')';
        $this->cleanEspacio($idNivel) != NULL && $filter.= ' AND (A.id_nivel = '.$idNivel.')';
        
        $sql = " SELECT DISTINCT A.id_promo, B.nombre AS nombrePromocion, B.detalle, B.texto_corto, B.imagen_qr, B.activo FROM tbl_rel_promo_suc_segmento AS A INNER JOIN tbl_promociones AS B ON (A.id_promo = B.id_promociones) LEFT JOIN tbl_sucursales AS C ON (C.id_sucursales = A.id_sucursal) LEFT JOIN tbl_segmento AS D ON (D.id_segmento = A.id_segmento) WHERE $filter ";
        //die($sql);
        $result = $this->res_con->query($sql);
        
        return $this->arrayToAssoc($result);
    }

    public function getReportePromociones($idSucursal = NULL, $idSegmento = NULL){
        $filter="A.id_rel IS NOT NULL";
        $this->cleanEspacio($idSucursal) && $filter.= ' AND (A.id_sucursal = '.$idSucursal.')';
        $this->cleanEspacio($idSegmento) != NULL && $filter.= ' AND (A.id_segmento = '.$idSegmento.')';
        
        $sql = " SELECT DISTINCT A.*, B.id_promociones, B.nombre AS nombrePromocion, B.detalle, B.texto_corto, B.imagen_qr, B.activo, C.nombre AS nombreSucursal, D.nombre_segmento AS nombreSegmento FROM tbl_rel_promo_suc_segmento AS A INNER JOIN tbl_promociones AS B ON (A.id_promo = B.id_promociones) LEFT JOIN tbl_sucursales AS C ON (C.id_sucursales = A.id_sucursal) LEFT JOIN tbl_segmento AS D ON (D.id_segmento = A.id_segmento) WHERE $filter ";
        die($sql);
        $result = $this->res_con->query($sql);
        
        return $this->arrayToAssoc($result);
    }

    public function getCategoriasPromociones($idPromocion = NULL, $flag = 0){
        
        $filter="";
        $this->cleanEspacio($idPromocion) && $filter.= ' B.id_promo = '.$idPromocion;

        switch ($flag) {
            case 1:
                $sql = " SELECT DISTINCT A.* FROM tbl_segmento AS A INNER JOIN tbl_rel_promo_suc_segmento AS B ON (A.id_segmento = B.id_segmento) WHERE $filter ";
            break;
            case 2:
                $sql = " SELECT DISTINCT A.* FROM tbl_niveles AS A INNER JOIN tbl_rel_promo_suc_segmento AS B ON (A.id_nivel = B.id_nivel) WHERE $filter ";
            break;
            case 3:
                $sql = " SELECT DISTINCT A.nombre AS nombreSucursal FROM tbl_sucursales AS A INNER JOIN tbl_rel_promo_suc_segmento AS B ON (A.id_sucursales = B.id_sucursal) WHERE $filter ";
            break;
            
            default:
                $sql = " SELECT DISTINCT A.*, B.nombre AS nombrePromocion, B.detalle, B.texto_corto, B.imagen_qr, B.activo, C.nombre AS nombreSucursal, D.nombre_segmento AS nombreSegmento FROM tbl_rel_promo_suc_segmento AS A INNER JOIN tbl_promociones AS B ON (A.id_promo = B.id_promociones) LEFT JOIN tbl_sucursales AS C ON (C.id_sucursales = A.id_sucursal) LEFT JOIN tbl_segmento AS D ON (D.id_segmento = A.id_segmento) WHERE $filter ";
            break;
        }

        //die($sql);
        $result = $this->res_con->query($sql);        
        return $this->arrayToAssoc($result);
    }

    public function getConfigProyecto(){
        
        $sql = " SELECT * FROM tbl_configuracion_proyecto ";        
        $result = $this->res_con->query($sql);        
        return $this->arrayToAssoc($result);
    }

    public function getSucursales($idSucursal = NULL){
        $filter="A.id_sucursales IS NOT NULL";
        $this->cleanEspacio($idSucursal) && $filter.= ' AND A.id_sucursales = '.$this->cleanSQL($idSucursal);
        
        $sql = " SELECT * FROM tbl_sucursales AS A WHERE $filter ";        
        $result = $this->res_con->query($sql);
        
        return $this->arrayToAssoc($result);
    }

    private function arrayToAssoc($result){
        $auxArray = array();
        while ($fila = mysqli_fetch_assoc($result)) {
            foreach ($fila as $key => $value) {
                $fila[$key] = utf8_encode($value);
            }
            array_push($auxArray, $fila);
        }
        return $auxArray;
    }
    
}
