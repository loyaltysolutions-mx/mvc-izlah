<?php
class M_reporte_niveles{

    private $ser;
    private $usu;
    private $pas;
    private $bd;    

    public function __construct($ser,$usu,$pas,$bd) {
        //mb_internal_encoding("UTF-8");
        $this->ser=$ser;
        $this->usu=$usu;
        $this->pas=$pas;
        $this->bd=$bd;
        $this->res_con=$this->conecta_bd($ser,$usu,$pas,$bd);         
    }

    private function cleanEspacio($txt){
        return preg_replace("/(\s*)/i","",trim($txt));
    }

    private function cleanSQL($query){
        return preg_replace("/\s(AND|OR|>|<|=|IN|NOT|BETWEEN|LIKE|XOR|UNION|JOIN|INNER)\s/i"," ",$query);
    }

    public function getReporteNiveles($idNivel = NULL, $tipoNivel = NULL){
        $filter="A.id_nivel IS NOT NULL";
        $this->cleanEspacio($idNivel) && $filter.= ' AND (A.id_nivel = '.$idNivel.")";
        $tipoNivel != NULL && $filter.= ' AND A.tipo = '.$tipoNivel;        
        
        $sql = " SELECT A.*, B.nombre AS nombreTipo FROM tbl_niveles AS A INNER JOIN tbl_tipo_proyecto AS B ON (A.tipo = B.id_tipo_proyecto) WHERE $filter ORDER BY A.nombre ";
        //die($sql);
        $result = $this->res_con->query($sql);
        
        return $this->arrayToAssoc($result);
    }

    public function getUsersLevel($idNivel = NULL){
        $filter="A.id_usuario IS NOT NULL";
        $this->cleanEspacio($idNivel) && $filter.= ' AND (A.id_nivel = '.$idNivel.")";
                
        $sql = " SELECT * FROM tbl_usuario AS A WHERE $filter ORDER BY nombre ";        
        $result = $this->res_con->query($sql);
        
        return $this->arrayToAssoc($result);
    }

    private function arrayToAssoc($result){
        $auxArray = array();
        while ($fila = mysqli_fetch_assoc($result)) {
            foreach ($fila as $key => $value) {
                $fila[$key] = utf8_encode($value);
            }
            array_push($auxArray, $fila);
        }
        return $auxArray;
    }

    //FUNCION CONECTA A BASE DE DATOS
    public function conecta_bd($ser,$usu,$pas,$bd){
        $con = mysqli_connect($ser,$usu,$pas,$bd);
        if ($con){
            return $con;
            mysqli_close($con);
        }else{
       echo("Error description: " . mysqli_error($con));
             exit(); 
        }
    }
    
}
