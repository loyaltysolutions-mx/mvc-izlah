<?php

class M_reporte_transferencia_puntos{

    private $ser;
    private $usu;
    private $pas;
    private $bd;    

    public function __construct($ser,$usu,$pas,$bd) {
        //mb_internal_encoding("UTF-8");
        $this->ser=$ser;
        $this->usu=$usu;
        $this->pas=$pas;
        $this->bd=$bd;
        $this->res_con=$this->conecta_bd($ser,$usu,$pas,$bd);         
    }

    private function cleanEspacio($txt){
        return preg_replace("/(\s*)/i","",trim($txt));
    }

    private function cleanSQL($query){
        return preg_replace("/\s(AND|OR|>|<|=|IN|NOT|BETWEEN|LIKE|XOR|UNION|JOIN|INNER)\s/i"," ",$query);
    }


    //FUNCION OBTIENE CLIENTES SEGMENTO
    public function getClientsSegmento($idSegmento = NULL, $idUsuario = NULL){
        $filter=" B.id_rel_segmento_usuario IS NOT NULL AND C.rol = 3 ";
        $this->cleanEspacio($idSegmento) && $filter.= " AND B.id_segmento = ".$this->cleanSQL($idSegmento);
        $this->cleanEspacio($idUsuario) && $filter.= " AND B.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT * FROM tbl_segmento AS A INNER JOIN tbl_rel_segmento_usuario AS B ON (A.id_segmento = B.id_segmento) INNER JOIN tbl_usuario AS C ON (B.id_usuario = C.id_usuario) WHERE $filter ";        
        return $this->res_con->query($sql);        
    }

    function getSegmentos($idSegmento = NULL){
        $filter=" A.id_segmento IS NOT NULL ";
        $this->cleanEspacio($idSegmento) && $filter.= " AND A.id_segmento = ".$this->cleanSQL($idSegmento);

        $sql = " SELECT * FROM tbl_segmento AS A WHERE $filter ";        
        $result = $this->res_con->query($sql);
        return $this->arrayToAssoc($result);
    }

    function getUsuarios($idUsuario = NULL){
        $filter=" A.id_usuario IS NOT NULL AND A.rol = 3 ";
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT * FROM tbl_usuario AS A WHERE $filter ";
        $result = $this->res_con->query($sql);
        return $this->arrayToAssoc($result);
    }

    function getPuntoVenta($idPuntoVenta = NULL){
        $filter=" A.id_usuario IS NOT NULL AND A.rol = 2 ";
        $this->cleanEspacio($idPuntoVenta) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idPuntoVenta);

        $sql = " SELECT * FROM tbl_usuario AS A WHERE $filter ";
        $result = $this->res_con->query($sql);
        return $this->arrayToAssoc($result);
    }

    function getNiveles($idNivel = NULL){
        $filter=" A.id_nivel IS NOT NULL ";
        $this->cleanEspacio($idNivel) && $filter.= " AND A.id_nivel = ".$this->cleanSQL($idNivel);

        $sql = " SELECT * FROM tbl_niveles AS A WHERE $filter ";

        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    function getReporte($idLog = NULL, $idSegmento = NULL, $idNivel = NULL, $idPuntoVenta = NULL, $fechaRegistro = NULL, $idUsuario = NULL){
        $filter=" A.id_log IS NOT NULL ";        

        $this->cleanEspacio($idLog) && $filter.= " AND A.id_log = ".$this->cleanSQL($idLog);
        $this->cleanEspacio($idSegmento) && $filter.= " AND (E.id_segmento = ".$idSegmento.")";
        $this->cleanEspacio($idNivel) && $filter.= " AND (B.id_nivel = ".$idNivel.")";
        $this->cleanEspacio($idPuntoVenta) && $filter.= " AND (A.id_usuario_registro = ".$idPuntoVenta.")";
        $this->cleanEspacio($fechaRegistro) && $filter.= " AND A.fecha_registro BETWEEN '".$fechaRegistro;
        $this->cleanEspacio($idUsuario) && $filter.= " AND (A.id_usuario_origen = ".$idUsuario.")";

        $sql = " SELECT A.*, B.nombre AS nombreOrigen, C.nombre AS nombreDestino, D.usuario AS nombrePV, G.nombre_segmento, F.nombre AS nombreNivel FROM tbl_log_transfer_registros AS A LEFT JOIN tbl_usuario AS B ON (B.id_usuario = A.id_usuario_origen) LEFT JOIN tbl_usuario AS C ON (C.id_usuario = A.id_usuario_destino) LEFT JOIN tbl_usuario AS D ON (D.id_usuario = A.id_usuario_registro) LEFT JOIN tbl_rel_segmento_usuario AS E ON (B.id_usuario = E.id_usuario) LEFT JOIN tbl_segmento AS G ON (G.id_segmento = E.id_segmento) LEFT JOIN tbl_niveles AS F ON (F.id_nivel = B.id_nivel) WHERE $filter ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    private function arrayToAssoc($result){
        $auxArray = array();
        while ($fila = mysqli_fetch_assoc($result)) {
            foreach ($fila as $key => $value) {
                $fila[$key] = utf8_encode($value);
            }
            array_push($auxArray, $fila);
        }
        return $auxArray;
    }

    //FUNCION CONECTA A BASE DE DATOS
    public function conecta_bd($ser,$usu,$pas,$bd){
        $con = mysqli_connect($ser,$usu,$pas,$bd);
        if ($con){
            return $con;
            mysqli_close($con);
        }else{
       echo("Error description: " . mysqli_error($con));
             exit(); 
        }
    }

    public function getSegmentosUser($idUsuario = NULL, $idSegmento = NULL){
        $filter=" A.id_rel_segmento_usuario IS NOT NULL ";
        $this->cleanEspacio($idUsuario) && $filter.= " AND (A.id_usuario = ".$idUsuario.")";
        $this->cleanEspacio($idSegmento) && $filter.= " AND (A.id_segmento = ".$idSegmento.")";

        $sql = " SELECT DISTINCT A.id_usuario, A.id_segmento, B.nombre AS nombreCliente, C.nombre_segmento FROM tbl_rel_segmento_usuario AS A INNER JOIN tbl_usuario AS B ON (A.id_usuario = B.id_usuario) INNER JOIN tbl_segmento AS C ON (C.id_segmento = A.id_segmento) WHERE $filter ";
        
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    /*public function getDistinctUserSegmento($idUsuario = NULL){
        $filter=" A.id_rel_segmento_usuario IS NOT NULL ";
        $this->cleanEspacio($idUsuario) && $filter.= " AND (A.id_usuario = ".$idUsuario.")";        

        $sql = " SELECT DISTINCT A.id_usuario, B.nombre AS nombreCliente FROM tbl_rel_segmento_usuario AS A INNER JOIN tbl_usuario AS B ON (A.id_usuario = B.id_usuario) INNER JOIN tbl_segmento AS C ON (C.id_segmento = A.id_segmento) WHERE $filter ";
        
        return $this->arrayToAssoc($this->res_con->query($sql));
    }*/

    public function getDistinctUserSegmento($idUsuario = NULL, $idNivel = NULL, $idPuntoVenta = NULL, $idSegmento){
        $filter=" A.id_log IS NOT NULL ";
        $this->cleanEspacio($idUsuario) && $filter.= " AND (A.id_usuario_origen = ".$idUsuario.")";
        $this->cleanEspacio($idNivel) && $filter.= " AND (B.id_nivel = ".$idNivel.")";
        $this->cleanEspacio($idPuntoVenta) && $filter.= " AND (A.id_usuario_registro = ".$idPuntoVenta.")";
        $this->cleanEspacio($idSegmento) && $filter.= " AND (C.id_segmento = ".$idSegmento.")";

        $sql = " SELECT DISTINCT A.id_usuario_origen, B.nombre AS nombreCliente FROM tbl_log_transfer_registros AS A INNER JOIN tbl_usuario AS B ON (A.id_usuario_origen = B.id_usuario) INNER JOIN tbl_rel_segmento_usuario AS C ON (C.id_usuario = A.id_usuario_origen) INNER JOIN tbl_segmento AS D ON (D.id_segmento = C.id_segmento) INNER JOIN tbl_niveles AS E ON (E.id_nivel = B.id_nivel) WHERE $filter ";
        die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }



}