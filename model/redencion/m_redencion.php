<?php
 /*  * ##+> ################################# <+##
 * MODELO DE REDENCION
 * Desarrolado ->Miguel Ruiz
 *  * ##+> ################################# <+##
 */
class M_redencion{

//FUNCION REEMPLAZA CARACTERES
public function reemplaza_caracteres($campo){
    $arr_caracteres=array('%40','%C3%91','%C3%B1','%C3%81','%C3%A1','%C3%89','%C3%A9','%C3%8D','%C3%AD','%C3%93','%C3%B3','%C3%9A','%C3%BA','%20');
    $arr_remplazo=array('@','Ñ','ñ','Á','á','É','é','Í','í','Ó','ó','Ú','ú',' ');
    $campo_limpio= str_replace($arr_caracteres,$arr_remplazo,$campo);
    return  $campo_limpio;
}
//FUNCION RESTA LOS PUNTOS Y GUARDA REGISTRO DE REDENCION
public function resta_redencion($ser,$usu,$pas,$bd,$id_cte,$id_prod,$id_usu){
    include_once '../../inc/funciones.php';
    require '../../inc/parametros.php';
     $inst_basicas=new Funciones_Basicas();
     $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    //OBTENEMOS INFORMACION DE CLIENTE PARA ENVIO DE INFORMACION AL REDIMIR
    $qr_consulta_info_cte='select * from tbl_usuario  where id_usuario='.$id_cte;
    $resp_consulta_info_cte=$res_con->query($qr_consulta_info_cte);
    $res_info_cte=mysqli_fetch_assoc($resp_consulta_info_cte);
    $mail_cte=$res_info_cte['email'];
    $cel_cte=$res_info_cte['telefono'];
    //OBTENEMOS INFORMACION PARA GUARDAR REGISTRO DE REDENCION
    $qr_consulta_val_ptos='select CAT.valor_puntos,CAT.valor_visitas,CAT.nombre from tbl_cat_premios_productos_servicios as CAT 
                        where CAT.id_cat_premios_productos_servicios='.$id_prod;
    $resp_consulta_val_ptos=$res_con->query($qr_consulta_val_ptos);
    $res=mysqli_fetch_assoc($resp_consulta_val_ptos);
    $val_puntos=$res['valor_puntos'];
	$val_visitas=$res['valor_visitas'];
    $nombre_prod=$res['nombre'];
    $fecha =date("d-m-Y"); 
    $nombre_dia=$inst_basicas->nombre_dia($fecha); 
	//OBTENEMOS TIPO DE PROYECTO PUNTOS O VISITAS
	$qr_cons_tipo_proy=$inst_basicas->consulta_generica_all('select * from tbl_configuracion_proyecto');
    $res_tipo_pro=mysqli_fetch_assoc($qr_cons_tipo_proy);
    $id_tipo_proyecto=$res_tipo_pro['id_tipo_proyecto'];
	//EVALUAMOS  TIPO DE PROYECTO PUNTOS O VISITAS
	switch($id_tipo_proyecto){
		case 1:
		case 2://PUNTOS
			//INSERTAMOS LA REDENCION
		$qr_inserta_redencion='insert into tbl_registros(id_usuario,puntos,fecha_registro,dia_registro,id_tipo_registro,id_usuario_registro,id_cat_premio) '
				. 'values('.$id_cte.',"'.$val_puntos.'",now(),"'.$nombre_dia.'",2,'.$id_usu.','.$id_prod.')';
		$resp_inserta_redencion=$res_con->query($qr_inserta_redencion);
		//CONSULTAMOS PUNTOS DE REDENCION
		$qri_consulta_pts_redencion=' select * from tbl_puntos_totales where id_usuario='.$id_cte.' and id_tipo_puntos=2';
		$resp_consulta_pts_redencion=$res_con->query($qri_consulta_pts_redencion); 
		$reg= mysqli_fetch_assoc($resp_consulta_pts_redencion);
		$ptos_actuales=$reg['puntos_totales'];
		$nuevos_ptos=$ptos_actuales+$val_puntos; 
		if($ptos_actuales==''){
			//INSERTA REDENCION
			$qri_insert_pts_redencion='insert tbl_puntos_totales(id_usuario,puntos_totales,id_tipo_puntos,fecha_registro)values('.$id_cte.',"'.$nuevos_ptos.'",2,now())';
			$res_con->query($qri_insert_pts_redencion);
			//DESCUENTA ACUMULADOS
			$qri_consulta_pts_acumulacion=' select * from tbl_puntos_totales where id_usuario='.$id_cte.' and id_tipo_puntos=1';
			$resp_consulta_pts_acumulacion=$res_con->query($qri_consulta_pts_acumulacion); 
			$reg= mysqli_fetch_assoc($resp_consulta_pts_acumulacion);
			$id_reg=$reg['id_puntos_totales'];  
			$ptos_actuales_acum=$reg['puntos_totales']-$val_puntos; 
			$qri_actualiza_pts_acum='update tbl_puntos_totales set puntos_totales='.$ptos_actuales_acum.',fecha_registro=now() where id_puntos_totales='.$id_reg;
			$res_con->query($qri_actualiza_pts_acum); 
			//EVALUAMOS TIPO DE COMUNICACION AL REDIMIR
			$qri_consulta_config_proy='select * from  tbl_configuracion_proyecto';
			$resp_consulta_config_proy=$res_con->query($qri_consulta_config_proy); 
			$reg_config= mysqli_fetch_assoc($resp_consulta_config_proy);
			$tipo_comunicacion=$reg_config['id_tipo_comunicacion_redimir'];
			//print_r($reg_config);
			$tipo_comunicado=explode('-',$tipo_comunicacion);
			 for($i=0;$i<=count($tipo_comunicado)-1;$i++){
				switch ( $tipo_comunicado[$i]) {
					case 1:
						//ENVIA SMS
						$msg='Felicidades canjeaste  '.$val_puntos .' puntos por '.$nombre_prod .' ¡Disfrutalo! ';
						$cel=$cel_cte;
						//$cel=5527278290;
						$inst_basicas->envia_sms($msg, $cel, $usu_sms, $pas_sms, $url_sms);
					break;
					case 2:
						//ENVIA MAIL
						$mail=$mail_cte;
						$mensa='<h3>Notificaci&oacute;n Redención  </h3> Felicidades canjeaste  <b>'. $val_puntos .'</b> puntos por <b>'. $nombre_prod.'</b> ¡Disfrútalo! <br><br>
								<hr><h5><font color="#8c8c8c" size="1">* Este correo es generado de forma automatica por la Plataforma de Lealtad, no es necesario que lo respondas</font></h5>';
						$inst_basicas->envia_mail($usu_mail, $pas_mail, $from_mail,$mail , 'Redencion',$mensa);
					break;
					default:
					break;
								} 
							}
			
			echo '1';
		 }else{
			//ACTUALIZA REDENCIONES
			$id_reg=$reg['id_puntos_totales']; 
			$qri_actualiza_pts_redencion='update tbl_puntos_totales set puntos_totales='.$nuevos_ptos.',fecha_registro=now() where id_puntos_totales='.$id_reg;
			$res_con->query($qri_actualiza_pts_redencion);
			//DESCUENTA ACUMULADOS
			$qri_consulta_pts_acumulacion=' select * from tbl_puntos_totales where id_usuario='.$id_cte.' and id_tipo_puntos=1';
			$resp_consulta_pts_acumulacion=$res_con->query($qri_consulta_pts_acumulacion);
			$reg_ptos_acum= mysqli_fetch_assoc($resp_consulta_pts_acumulacion);
			$id_reg_acum=$reg_ptos_acum['id_puntos_totales'];
			$ptos_actuales_acum=$reg_ptos_acum['puntos_totales']-$val_puntos; 
			$qri_actualiza_pts_acum='update tbl_puntos_totales set puntos_totales='.$ptos_actuales_acum.',fecha_registro=now() where id_puntos_totales='.$id_reg_acum;
			$res_con->query($qri_actualiza_pts_acum);  
			//EVALUAMOS TIPO DE COMUNICACION AL REDIMIR
			$qri_consulta_config_proy='select * from  tbl_configuracion_proyecto';
			$resp_consulta_config_proy=$res_con->query($qri_consulta_config_proy); 
			$reg_config= mysqli_fetch_assoc($resp_consulta_config_proy);
			$tipo_comunicacion=$reg_config['id_tipo_comunicacion_redimir'];
			//print_r($reg_config);
			$tipo_comunicado=explode('-',$tipo_comunicacion);
			 for($i=0;$i<=count($tipo_comunicado)-1;$i++){
				switch ( $tipo_comunicado[$i]) {
					case 1:
						//ENVIA SMS
						$msg='Felicidades canjeaste  '.$val_puntos .' puntos por '.$nombre_prod .' ¡Disfrutalo! ';
						$cel=$cel_cte;
						//$cel=5527278290;
						$inst_basicas->envia_sms($msg, $cel, $usu_sms, $pas_sms, $url_sms);
					break;
					case 2:
						//ENVIA MAIL
						$mail=$mail_cte;
						$mensa='<h3>Notificaci&oacute;n Redención  </h3> Felicidades canjeaste  <b>'. $val_puntos .'</b> puntos por <b>'. $nombre_prod.'</b> ¡Disfrútalo! <br><br>
							  <hr><h5><font color="#8c8c8c" size="1">* Este correo es generado de forma automatica por la Plataforma de Lealtad, no es necesario que lo respondas</font></h5>';
						$inst_basicas->envia_mail($usu_mail, $pas_mail, $from_mail,$mail , 'Redencion',$mensa);
					break;
					default:
					break;
								} 
							}
		  echo '1';
		 }
		break;
		case 3://VISITAS
			//INSERTAMOS LA REDENCION
		$qr_inserta_redencion='insert into tbl_registros(id_usuario,num_visita,fecha_registro,dia_registro,id_tipo_registro,id_usuario_registro,id_cat_premio) '
				. 'values('.$id_cte.',"'.$val_visitas.'",now(),"'.$nombre_dia.'",2,'.$id_usu.','.$id_prod.')';
		$resp_inserta_redencion=$res_con->query($qr_inserta_redencion);
		//CONSULTAMOS VISITAS DE REDENCION
		$qri_consulta_visitas_redencion=' select * from tbl_puntos_totales where id_usuario='.$id_cte.' and id_tipo_puntos=2';
		$resp_consulta_visitas_redencion=$res_con->query($qri_consulta_visitas_redencion); 
		$reg= mysqli_fetch_assoc($resp_consulta_visitas_redencion);
		$visitas_actuales=$reg['visitas_totales'];
		$nuevos_visit=$visitas_actuales+$val_visitas; 
		if($visitas_actuales==''){
			//INSERTA REDENCION
			$qri_insert_visitas_redencion='insert tbl_puntos_totales(id_usuario,visitas_totales,id_tipo_puntos,fecha_registro)values('.$id_cte.',"'.$nuevos_visit.'",2,now())';
			$res_con->query($qri_insert_visitas_redencion);
			//DESCUENTA ACUMULADOS
			$qri_consulta_visitas_acumulacion=' select * from tbl_puntos_totales where id_usuario='.$id_cte.' and id_tipo_puntos=1';
			$resp_consulta_pts_acumulacion=$res_con->query($qri_consulta_visitas_acumulacion);
			$reg= mysqli_fetch_assoc($resp_consulta_pts_acumulacion);
			$id_reg=$reg['id_puntos_totales'];
			$visit_actuales_acum=$reg['visitas_totales']-$val_visitas; 
			$qri_actualiza_visit_acum='update tbl_puntos_totales set visitas_totales='.$visit_actuales_acum.',fecha_registro=now() where id_puntos_totales='.$id_reg;
			$res_con->query($qri_actualiza_visit_acum); 
			//EVALUAMOS TIPO DE COMUNICACION AL REDIMIR
			$qri_consulta_config_proy='select * from  tbl_configuracion_proyecto';
			$resp_consulta_config_proy=$res_con->query($qri_consulta_config_proy); 
			$reg_config= mysqli_fetch_assoc($resp_consulta_config_proy);
			$tipo_comunicacion=$reg_config['id_tipo_comunicacion_redimir'];
			//print_r($reg_config);
			$tipo_comunicado=explode('-',$tipo_comunicacion);
			 for($i=0;$i<count($tipo_comunicado);$i++){
				switch ( $tipo_comunicado[$i]) {
					case 1:
						//ENVIA SMS
						$msg='Felicidades canjeaste  '.$val_visitas .' visitas por '.$nombre_prod .' ¡Disfrutalo! ';
						$cel=$cel_cte;
						//$cel=5527278290;
						$inst_basicas->envia_sms($msg, $cel, $usu_sms, $pas_sms, $url_sms);
					break;
					case 2:
						//ENVIA MAIL
						$mail=$mail_cte;
						$mensa='<h3>Notificaci&oacute;n Redención  </h3> Felicidades canjeaste  <b>'. $val_visitas .'</b> visitas por <b>'. $nombre_prod.'</b> ¡Disfrútalo! <br><br>
								<hr><h5><font color="#8c8c8c" size="1">* Este correo es generado de forma automatica por la Plataforma de Lealtad, no es necesario que lo respondas</font></h5>';
						$inst_basicas->envia_mail($usu_mail, $pas_mail, $from_mail,$mail , 'Redencion',$mensa);
					break;
					default:
					break;
				} 
			}
			
			echo '1';
		 }else{
			//ACTUALIZA REDENCIONES
			$id_reg=$reg['id_puntos_totales']; 
			$qri_actualiza_visitas_redencion='update tbl_puntos_totales set visitas_totales='.$nuevos_visit.',fecha_registro=now() where id_puntos_totales='.$id_reg;
			$res_con->query($qri_actualiza_visitas_redencion);
			//DESCUENTA ACUMULADOS
			$qri_consulta_visitas_acumulacion=' select * from tbl_puntos_totales where id_usuario='.$id_cte.' and id_tipo_puntos=1';
			$resp_consulta_visitas_acumulacion=$res_con->query($qri_consulta_visitas_acumulacion); 
			$reg_visit_acum= mysqli_fetch_assoc($resp_consulta_visitas_acumulacion);
			$id_reg_acum=$reg_visit_acum['id_puntos_totales'];   
			$visitas_actuales_acum=$reg_visit_acum['visitas_totales']-$val_visitas; 
			$qri_actualiza_visit_acum='update tbl_puntos_totales set visitas_totales='.$visitas_actuales_acum.',fecha_registro=now() where id_puntos_totales='.$id_reg_acum;
			$res_con->query($qri_actualiza_visit_acum);  
			//EVALUAMOS TIPO DE COMUNICACION AL REDIMIR
			$qri_consulta_config_proy='select * from  tbl_configuracion_proyecto';
			$resp_consulta_config_proy=$res_con->query($qri_consulta_config_proy); 
			$reg_config= mysqli_fetch_assoc($resp_consulta_config_proy);
			//print_r($reg_config);
			$tipo_comunicacion=$reg_config['id_tipo_comunicacion_redimir'];
			$tipo_comunicado=explode('-',$tipo_comunicacion);
			 for($i=0;$i<=count($tipo_comunicado)-1;$i++){
				switch ( $tipo_comunicado[$i]) {
					case 1:
						//ENVIA SMS
						$msg='Felicidades canjeaste  '.$val_visitas .' visitas por '.$nombre_prod .' ¡Disfrutalo! ';
						$cel=$cel_cte;
						//$cel=5527278290;
						$inst_basicas->envia_sms($msg, $cel, $usu_sms, $pas_sms, $url_sms);
					break;
					case 2:
						//ENVIA MAIL
						$mail=$mail_cte;
						$mensa='<h3>Notificaci&oacute;n Redención  </h3> Felicidades canjeaste  <b>'. $val_visitas .'</b> visitas por <b>'. $nombre_prod.'</b> ¡Disfrútalo! <br><br>
							  <hr><h5><font color="#8c8c8c" size="1">* Este correo es generado de forma automatica por la Plataforma de Lealtad, no es necesario que lo respondas</font></h5>';
						$inst_basicas->envia_mail($usu_mail, $pas_mail, $from_mail,$mail , 'Redencion',$mensa);
					break;
					default:
					break;
								} 
							}
		  echo '1';
		 }
		break;
		default:
		 echo 'ocurrio un error :(';
		break;
		
		
		
	}	
}
//FUNCION BUSCA REGISTRO DE CLIENTE
public function inserta_redencion($ser,$usu,$pas,$bd,$telefono,$id_tarjeta,$mail){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    
    //important bacause some rows have the same values empty
    //die("Tarjeta:".$id_tarjeta.",Mail:".$mail);

    //if($id_tarjeta==''){
    $query_tarjeta = $id_tarjeta != '' ? "AND (usuario = '".utf8_decode($this->reemplaza_caracteres(trim($id_tarjeta)))."' OR tarjeta = '".utf8_decode($this->reemplaza_caracteres(trim($id_tarjeta)))."')" : "" ;
    //}else if($mail==''){
    $query_mail = $mail != '' ? "AND (usuario = '".utf8_decode($this->reemplaza_caracteres(trim($mail)))."' OR email = '".utf8_decode($this->reemplaza_caracteres(trim($mail)))."')" : "" ;
    //}
    
    //VALIDAMOS SI EL USUARIO EXISTE
    $qr_consulta_usuario=" SELECT count(*) AS n FROM tbl_usuario WHERE id_usuario IS NOT NULL $query_tarjeta $query_mail ";
    //die($qr_consulta_usuario);
    $resp_consulta_usuario=$res_con->query($qr_consulta_usuario);
    $r=mysqli_fetch_assoc($resp_consulta_usuario);
    if($r['n']>0){
      	//echo 'muestra productos';
		//OBTENEMOS EL ID DEL USUARIO PARA REGRESARLO
        $qr_consulta_id_usuario=" SELECT * FROM tbl_usuario WHERE id_usuario IS NOT NULL $query_tarjeta $query_mail ";
        //die($qr_consulta_id_usuario);
        $resp_consulta_id_usuario=$res_con->query($qr_consulta_id_usuario);
        $reg=mysqli_fetch_assoc($resp_consulta_id_usuario);        
        //print_r($reg);
        //die();
		echo $reg['id_usuario'];
    }else{
        
       // echo 'No se encontro Registro con la información proporcionada';
        echo 3;
    }
    
}
//FUNCION TRAE LOS CAMPOS DEL REGISTRO    
 public function campos_registro($ser,$usu,$pas,$bd){
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $sql="select MR.*,CP.*,MRn.* from tbl_rel_config_mecanica_redencion as MR
            inner join tbl_configuracion_proyecto as CP on MR.id_configuracion_proyecto=CP.id_configuracion_proyecto
            inner join tbl_mecanica_redencion as MRn on MRn.id_mecanica_redencion=MR.id_mecanica_redencion";
    $resultado=$res_con->query($sql);
   return $resultado;
        
    }
//FUNCION TRAE LOS PRODUCTOS DEL CATALOGO PARA VISITAS
 public function productos_catalogo_visitas($ser,$usu,$pas,$bd){
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $sql="select * from  tbl_cat_premios_productos_servicios where activo=0 order by valor_visitas desc";
    $resultado=$res_con->query($sql);
   return $resultado;
        
    }
//FUNCION TRAE LOS PRODUCTOS DEL CATALOGO
 public function productos_catalogo($ser,$usu,$pas,$bd){
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $sql="select * from  tbl_cat_premios_productos_servicios where activo=0 order by valor_puntos desc";
    $resultado=$res_con->query($sql);
   return $resultado;
        
    }
//FUNCION CONECTA A BASE DE DATOS
  public function conecta_bd($ser,$usu,$pas,$bd,$con){
        $con = mysqli_connect($ser,$usu,$pas,$bd);
    if ($con)
    {
        return $con;
         mysqli_close($con);
    }else{
       echo("Error description: " . mysqli_error($con));
             exit(); 
        }
    }


    
    
}

?>