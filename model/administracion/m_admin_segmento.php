<?php

class M_admin_segmento{

    private $ser;
    private $usu;
    private $pas;
    private $bd;    

    public function __construct($ser,$usu,$pas,$bd){
        $this->ser=$ser;
        $this->usu=$usu;
        $this->pas=$pas;
        $this->bd=$bd;
        $this->res_con=$this->conecta_bd($ser,$usu,$pas,$bd);         
    }

    //FUNCION LIMPIA ESPACIOS 
    function cleanEspacio($txt){
        return preg_replace("/(\s*)/i","",trim($txt));
    }

    //FUNCION QUE LIMPIA EL SQL INGRESADO
    function cleanSQL($query){
        return preg_replace("/\s(AND|OR|>|<|=|IN|NOT|BETWEEN|LIKE|XOR|UNION|JOIN|INNER)\s/i"," ",$query);
    }
    
    //FUNCION TRAE HISTORIAL USUARIO
    public function registros_usuarios($ser,$usu,$pas,$bd){
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $sql="select U.*,R.* from tbl_usuario as U
                inner join tbl_rol as R on R.id_tbl_rol=U.rol
                where U.activo=0";
        
        //echo $sql;
        $resultado=$res_con->query($sql);
        return $resultado;
        
    }

    //FUNCION INSERTA SEGMENTO
    public function newSegmento($nombreSegmento = NULL, $descripcionSegmento = NULL, $statusSegmento = NULL, $fechaCreacion = NULL){

        if($nombreSegmento != NULL){
            $nombre = addslashes($this->cleanSQL($nombreSegmento));
            $descripcion = addslashes($this->cleanSQL($descripcionSegmento));
            $status = $statusSegmento;
            
            $sql=" INSERT INTO tbl_segmento (nombre_segmento, descripcion_segmento, activo, fecha_creacion) VALUES ('$nombreSegmento', '$descripcionSegmento', $status, '$fechaCreacion') ";
            //die($sql);
            return $this->res_con->query($sql);
        }
    
        return 0;        
    }

    public function getInfoUser($idUsuario = NULL){
        $filter=" A.id_usuario IS NOT NULL AND A.rol = 3";
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT A.*, B.nombre AS nombreNivel, C.nombre AS nombreProyecto, B.ini, B.fin FROM tbl_usuario AS A LEFT JOIN tbl_niveles AS B ON (B.id_nivel = A.id_nivel) LEFT JOIN tbl_tipo_proyecto AS C ON (C.id_tipo_proyecto = B.tipo) WHERE $filter ";        
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getSegmentosUser($idUsuario = NULL){
        $filter=" A.id_usuario IS NOT NULL ";
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT * FROM tbl_rel_segmento_usuario AS A INNER JOIN tbl_usuario AS B ON (B.id_usuario = A.id_usuario) INNER JOIN tbl_segmento AS C ON (C.id_segmento = A.id_segmento) WHERE $filter ";        
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getRegistrosUser($idUsuario = NULL){
        $filter=" A.id_registro_acumulacion IS NOT NULL ";
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT A.*, C.usuario, D.nombre AS nombreTipo FROM tbl_registros AS A LEFT JOIN tbl_usuario AS C ON (A.id_usuario_registro = C.id_usuario) INNER JOIN tbl_tipo AS D ON (A.id_tipo_registro = D.id_tipo) WHERE $filter ORDER BY id_registro_acumulacion ASC ";                
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getRegistrosUserFecha($idUsuario = NULL, $meses = NULL){
        $filter=" A.id_registro_acumulacion IS NOT NULL ";
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);
        $this->cleanEspacio($meses) && $filter.= " AND A.fecha_registro BETWEEN date_sub(NOW(), INTERVAL ".$this->cleanSQL($meses)." MONTH) AND NOW() ";

        $sql = " SELECT A.*, C.usuario, D.nombre AS nombreTipo FROM tbl_registros AS A LEFT JOIN tbl_usuario AS C ON (A.id_usuario_registro = C.id_usuario) INNER JOIN tbl_tipo AS D ON (A.id_tipo_registro = D.id_tipo) WHERE $filter ORDER BY id_registro_acumulacion ASC ";                
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getDiasUltimaCompra($idUsuario = NULL){
        $filter=" A.id_registro_acumulacion IS NOT NULL ";
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);        

        $sql = " SELECT DATEDIFF(NOW(), A.fecha_registro) AS dias FROM (SELECT B.id_registro_acumulacion, B.id_usuario, B.fecha_registro from tbl_registros AS B WHERE B.id_usuario = $idUsuario ORDER BY B.fecha_registro DESC LIMIT 1 ) AS A WHERE $filter ORDER BY id_registro_acumulacion ASC ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getPromedioMontoTicket($idUsuario = NULL){
        $filter=" A.id_registro_acumulacion IS NOT NULL ";
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);        

        $sql = " SELECT ROUND(AVG(A.monto_ticket),2) AS promedio FROM tbl_registros AS A WHERE $filter ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }


    public function getPuntosUser($idUsuario = NULL){
        $filter=" A.id_puntos_totales IS NOT NULL ";
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT A.*, B.nombre AS nombreTipo FROM tbl_puntos_totales AS A INNER JOIN tbl_tipo AS B ON (A.id_tipo_puntos = B.id_tipo) WHERE $filter ";                
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getTransferenciasUser($idUsuario = NULL){
        $filter=" A.id_log IS NOT NULL ";
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario_origen = ".$this->cleanSQL($idUsuario)." OR A.id_usuario_destino = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT A.*, B.*, C.nombre AS nombreOrigen, D.nombre AS nombreDestino, E.nombre AS nombrePV FROM tbl_log_transfer_registros AS A INNER JOIN tbl_tipo AS B ON (B.id_tipo_puntos = A.id_tipo) INNER JOIN tbl_usuario AS C ON (A.id_usuario_origen = C.id_usuario) INNER JOIN tbl_usuario AS D ON (D.id_usuario = A.id_usuario_destino) INNER JOIN tbl_usuario AS E ON (A.id_usuario_registro = E.id_usuario_destino) WHERE $filter ";
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getCountSegmento($idSegmento = NULL){
        $filter=" A.id_segmento IS NOT NULL ";
        $this->cleanEspacio($idSegmento) && $filter.= " AND A.id_segmento = ".$this->cleanSQL($idSegmento);

        $sql=" SELECT count(*) AS countClients FROM tbl_rel_segmento_usuario AS A WHERE $filter ";
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getTipoProyecto(){
        $sql=" SELECT A.id_tipo_proyecto FROM tbl_configuracion_proyecto AS A";
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    //FUNCION OBTIENE LOS SEGMENTOS DEPENDIENDO LOS FILTROS INGRESADOS
    public function getSegmentos($idSegmento = NULL, $status = NULL, $fechaCreacion = NULL){
        $filter=" A.id_segmento IS NOT NULL ";
        $this->cleanEspacio($idSegmento) && $filter.= " AND A.id_segmento = ".$this->cleanSQL($idSegmento);
        $status != NULL && $filter.= " AND A.activo = ".$this->cleanSQL($status);
        $this->cleanEspacio($fechaCreacion) && $filter.= " AND A.fecha_creacion BETWEEN '".$fechaCreacion;
        $sql = " SELECT * FROM tbl_segmento AS A WHERE $filter ORDER BY A.id_segmento ";
        //die($sql);
        return $this->res_con->query($sql);
    }

    //FUNCION ELIMINA SEGMENTO 
    public function removeSegmento($idSegmento = NULL){         
        $sql = " DELETE FROM tbl_segmento WHERE id_segmento = $idSegmento ";
        //die($sql);
        return $this->res_con->query($sql);        
    }

    //FUNCION ELIMINA LAS CATEORIAS LIGADAS AL SEGMENTO
    public function removeCategoriasSegmento($idSegmento = NULL){
        $sql = " DELETE FROM tbl_rel_segmento_categorias WHERE id_segmento = $idSegmento ";
        //die($sql);
        return $this->res_con->query($sql);        
    }

    //FUNCION OBTIENE CLIENTES SEGMENTO
    public function getClientsSegmento($idSegmento = NULL, $idUsuario = NULL){
        $filter=" B.id_rel_segmento_usuario IS NOT NULL AND C.rol = 3 ";
        $this->cleanEspacio($idSegmento) && $filter.= " AND B.id_segmento = ".$this->cleanSQL($idSegmento);
        $this->cleanEspacio($idUsuario) && $filter.= " AND B.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT * FROM tbl_segmento AS A INNER JOIN tbl_rel_segmento_usuario AS B ON (A.id_segmento = B.id_segmento) INNER JOIN tbl_usuario AS C ON (B.id_usuario = C.id_usuario) WHERE $filter ";        
        return $this->res_con->query($sql);        
    }

    public function getUsuario($idUsuario = NULL){
        $filter=" A.id_usuario IS NOT NULL ";
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);        
        $sql = " SELECT A.id_usuario, A.nombre, A.email, A.rfc, A.fecha_nacimiento, A.telefono, A.genero, A.activo FROM tbl_usuario AS A WHERE $filter ORDER BY A.id_usuario ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    //FUNCION QUE PERMITE ELIMINAR LA RELACION DE CLIENTE/SEGMENTO
    public function removeClientsSegmento($idUsuario = NULL, $idSegmento = NULL){
        $filter="id_usuario IS NOT NULL";
        $this->cleanEspacio($idSegmento) && $filter.= " AND id_segmento = ".$this->cleanSQL($idSegmento);
        $this->cleanEspacio($idUsuario) && $filter.= " AND id_usuario = ".$this->cleanSQL($idUsuario);
        $sql = " DELETE FROM tbl_rel_segmento_usuario WHERE $filter ";
        //die($sql);
        return $this->res_con->query($sql);         
    }

    //FUNCION QUE PERMITE OBTENER LOS CLIENTES QUE NO PERTENECEN AL SEGMENTO INGRESADO
    public function getClientsAdd($idSegmento = NULL){        
        $sql = " SELECT A.id_usuario, A.nombre, A.usuario, A.rfc, A.email, A.telefono, A.tarjeta, A.fecha_nacimiento, A.genero FROM tbl_usuario AS A WHERE A.rol = 3 AND A.id_usuario NOT IN ( SELECT DISTINCT(id_usuario) FROM tbl_rel_segmento_usuario AS B WHERE B.id_segmento = $idSegmento ) ";        
        //die($sql);
        return $this->res_con->query($sql);
    }

    //FUNCION QUE PERMITE OBTENER LAS CATEGORIAS QUE SE PUEDEN APLICAR AL SEGMENTO
    public function getCategorias($idCategoria = NULL){        
        $filter=" A.id_categoria IS NOT NULL ";        
        $this->cleanEspacio($idCategoria) && $filter.= " AND A.id_categoria = ".$this->cleanSQL($idCategoria);
        $sql = " SELECT * FROM tbl_categorias_segmento AS A WHERE $filter ORDER BY A.id_categoria ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql)); 
    }

    //FUNCION QUE PERMITE OBTENER LOS OBJETOS DE LAS CATEGORIAS QUE SE PUEDEN APLICAR AL SEGMENTO
    public function getObjetosCategorias($idCategoria = NULL){        
        $filter=" A.id_rel IS NOT NULL ";        
        $this->cleanEspacio($idCategoria) && $filter.= " AND A.id_categoria = ".$this->cleanSQL($idCategoria);
        $sql = " SELECT * FROM tbl_rel_objetos_categorias AS A INNER JOIN tbl_objeto_html AS B ON (A.id_objeto = B.id_objeto_html) WHERE $filter ORDER BY A.id_categoria ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql)); 
    }

    public function getUsuarios($idUsuario = NULL, $tipoUsuario = NULL){        
        if($tipoUsuario != NULL){
            $filter=" A.id_usuario IS NOT NULL AND A.rol = $tipoUsuario ";        
            $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);        
            $sql = " SELECT A.id_usuario, A.usuario, A.nombre, A.email, A.rfc, A.fecha_nacimiento, A.telefono, A.genero, A.activo FROM tbl_usuario AS A WHERE $filter ORDER BY A.id_usuario ";
            //die($sql);
            return $this->arrayToAssoc($this->res_con->query($sql));
        }
        return false;
    }

    public function getQuerySegmentos($idSegmento = NULL){        
        $filter=" A.id_rel IS NOT NULL ";        
        $this->cleanEspacio($idSegmento) && $filter.= " AND A.id_segmento = ".$this->cleanSQL($idSegmento);

        $sql = " SELECT * FROM tbl_rel_segmento_categorias AS A INNER JOIN tbl_categorias_segmento AS B ON (A.id_categoria = B.id_categoria) INNER JOIN tbl_rel_objetos_categorias AS C ON (A.id_rel_objeto_categoria = C.id_rel) INNER JOIN tbl_objeto_html AS D ON (D.id_objeto_html = C.id_objeto) WHERE $filter ORDER BY A.nivel ASC ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    //FUNCION QUE PERMITE INGRESAR UN USUARIO AL DETERMINADO SEGMENTO
    public function addClientsSegmento($idUsuario = NULL, $idSegmento = NULL){
        if($idUsuario != NULL && $idSegmento != NULL){
            $sql=" INSERT INTO tbl_rel_segmento_usuario (id_usuario, id_segmento, activo) VALUES ($this->cleanEspacio($idUsuario), $this->cleanEspacio($idSegmento), 0) ";            
            //die($sql);
            return $this->res_con->query($sql);
        }else{
            return false;
        }
    }

    //FUNCION QUE PERMITE INGRESAR UN USUARIO AL DETERMINADO SEGMENTO
    public function addSegmentoCategoria($idSegmento = NULL, $idCategoria = NULL, $idRelObjetoCategoria = NULL, $nivel = NULL, $valueObjeto = NULL){
        if($idSegmento != NULL && $idCategoria != NULL && $idRelObjetoCategoria != NULL && $nivel != NULL && $valueObjeto != NULL){
            $sql=" INSERT INTO tbl_rel_segmento_categorias (id_segmento,id_categoria, id_rel_objeto_categoria, nivel, valores) VALUES ($this->cleanSQL($idSegmento), $this->cleanSQL($idCategoria), $this->cleanSQL($idRelObjetoCategoria), $this->SQL($nivel), '$valueObjeto') ";
            //die($sql);
            return $this->res_con->query($sql);
        }else{
            return false;
        }
    }

    //FUNCION QUE PERMITE INGRESAR UN USUARIO AL DETERMINADO SEGMENTO
    public function existRegistro($idSegmento = NULL, $idCategoria = NULL, $nivel = NULL){
        if($idSegmento != NULL && $idCategoria != NULL && $nivel != NULL){
            $sql=" SELECT * FROM tbl_rel_segmento_categorias WHERE id_segmento = $this->cleanEspacio($idSegmento) AND id_categoria = $this->cleanEspacio($idCategoria) AND nivel = $this->cleanEspacio($nivel) ";
            //die($sql);
            return $this->res_con->query($sql);
        }else{
            return false;
        }
    }

    //FUNCION QUE PERMITE INGRESAR UN USUARIO AL DETERMINADO SEGMENTO
    public function setValuesObjetosCategoria($idCategoria = NULL, $idObjeto = NULL, $nameObjeto = NULL, $valueObjeto = NULL){
        if($idCategoria != NULL && $idObjeto != NULL && $nameObjeto != NULL && $valueObjeto != NULL){            
            $sql=" UPDATE tbl_rel_objetos_categorias SET valores = '$valueObjeto' WHERE id_categoria = $idCategoria AND id_objeto = $idObjeto AND name = '$nameObjeto' ";            
            //die($sql);
            return $this->res_con->query($sql);
        }else{
            return false;
        }
    }

    public function getSegmento1($idSegmento){
        $filter=" A.id_segmento IS NOT NULL ";
        $this->cleanEspacio($idSegmento) && $filter.= " AND A.id_segmento = ".$this->cleanSQL($idSegmento);

        $sql = " SELECT A.*, MAX(B.nivel) AS maxNivel FROM tbl_segmento AS A INNER JOIN tbl_rel_segmento_categorias AS B ON (A.id_segmento = B.id_segmento) WHERE $filter ORDER BY A.id_segmento ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getCategoriasSegmento($idSegmento = NULL, $idCategoria = NULL, $nivel = NULL){
        $filter=" A.id_rel IS NOT NULL ";        
        $this->cleanEspacio($idSegmento) && $filter.= " AND A.id_segmento = ".$this->cleanSQL($idSegmento);
        $this->cleanEspacio($idCategoria) && $filter.= " AND A.id_categoria = ".$this->cleanSQL($idCategoria);
        $this->cleanEspacio($nivel) && $filter.= " AND A.nivel = ".$this->cleanSQL($nivel);

        $sql = " SELECT * FROM tbl_rel_segmento_categorias AS A INNER JOIN tbl_categorias_segmento AS B ON (A.id_categoria = B.id_categoria) INNER JOIN tbl_rel_objetos_categorias AS C ON (A.id_rel_objeto_categoria = C.id_rel) INNER JOIN tbl_objeto_html AS D ON (D.id_objeto_html = C.id_objeto) WHERE $filter ORDER BY A.id_categoria ASC ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function deleteCategoriasSegmento($idSegmento = NULL, $idCategoria = NULL, $nivel = NULL){
        $filter=" id_rel IS NOT NULL ";        
        $this->cleanEspacio($idSegmento) && $filter.= " AND id_segmento = ".$this->cleanSQL($idSegmento);
        $this->cleanEspacio($idCategoria) && $filter.= " AND id_categoria = ".$this->cleanSQL($idCategoria);
        $this->cleanEspacio($nivel) && $filter.= " AND nivel = ".$this->cleanSQL($nivel);

        $sql = " DELETE FROM tbl_rel_segmento_categorias WHERE $filter ";
        //die($sql);
        return $this->res_con->query($sql);
    }


    public function getElementsCategoriasSegmento($idSegmento = NULL, $idCategoria = NULL, $nivel = NULL){
        $filter=" A.id_rel IS NOT NULL ";        
        $this->cleanEspacio($idSegmento) && $filter.= " AND A.id_segmento = ".$this->cleanSQL($idSegmento);
        $this->cleanEspacio($idCategoria) && $filter.= " AND A.id_categoria = ".$this->cleanSQL($idCategoria);
        $this->cleanEspacio($nivel) && $filter.= " AND A.nivel = ".$this->cleanSQL($nivel);

        $sql = " SELECT * FROM tbl_rel_segmento_categorias AS A INNER JOIN tbl_categorias_segmento AS B ON (A.id_categoria = B.id_categoria) INNER JOIN tbl_rel_objetos_categorias AS C ON (A.id_rel_objeto_categoria = C.id_rel) INNER JOIN tbl_objeto_html AS D ON (D.id_objeto_html = C.id_objeto) WHERE $filter ORDER BY A.id_categoria ASC ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getDistinctCategoriasSegmento($idSegmento = NULL, $idCategoria = NULL, $nivel = NULL){
        $filter=" A.id_rel IS NOT NULL ";        
        $this->cleanEspacio($idSegmento) && $filter.= " AND A.id_segmento = ".$this->cleanSQL($idSegmento);
        $this->cleanEspacio($idCategoria) && $filter.= " AND A.id_categoria = ".$this->cleanSQL($idCategoria);
        $this->cleanEspacio($nivel) && $filter.= " AND A.nivel = ".$this->cleanSQL($nivel);

        $sql = " SELECT DISTINCT B.id_categoria, B.nombre_categoria, B.descripcion_categoria FROM tbl_rel_segmento_categorias AS A INNER JOIN tbl_categorias_segmento AS B ON (A.id_categoria = B.id_categoria) INNER JOIN tbl_rel_objetos_categorias AS C ON (A.id_rel_objeto_categoria = C.id_rel) INNER JOIN tbl_objeto_html AS D ON (D.id_objeto_html = C.id_objeto) WHERE $filter ORDER BY A.id_categoria ASC ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getUsuariosSegmento($objects){        
        $filter = "";
        $inner = "";
        $flag = true;
        $nivel = 1;

        foreach ($objects as $key){            
            $resultFilter = $this->getFilter($key['id_categoria'], $key['valores']);
            //echo "Before: ".$nivel."->".$key['nivel']."->".(int)$flag."\n";
            $filter.= $nivel == $key['nivel'] ? (!$flag ? "OR" : "").$resultFilter['filter'] : ") AND ( ".$resultFilter['filter'];
            $inner.= strpos($inner, $resultFilter['inner']) === FALSE ? $resultFilter['inner'] : ""; 
            //print_r(strpos($inner, $resultFilter['inner']));
            $nivel != $key['nivel'] && $nivel++;
            $flag = $nivel == $key['nivel'] ? false : true;          
            //echo "After: ".$nivel."->".$key['nivel']."->".(int)$flag."\n";
          
        }
        //die();
        $sql = " SELECT DISTINCT(A.id_usuario) FROM tbl_usuario AS A $inner WHERE A.id_usuario IS NOT NULL AND ($filter) AND A.rol = 3 ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    //FUNCION QUE PERMITE ORGANIZAR LOS DATOS DEPENDIENDO LA CATEGORIA
    private function getFilter($categoria, $valor){
        if($categoria == 1){            
            $filter = " CAST((DATE_FORMAT(A.fecha_nacimiento, CONCAT( YEAR(NOW()),'-%m-%d' ) ) ) AS DATE) BETWEEN date_sub(NOW(), INTERVAL $valor DAY) AND NOW() ";            
            $inner = "";
        }

        if($categoria == 2){            
            $filter = " CAST((DATE_FORMAT(A.fecha_nacimiento, CONCAT( YEAR(NOW()),'-%m-%d' ) ) ) AS DATE) BETWEEN date_sub(NOW(), INTERVAL $valor DAY) AND NOW() ";            
            $inner = "";
        }

        if($categoria == 3){
          $fecha = date('Y-m-d');
          $compra = strtotime ( "-$valor day" , strtotime ( $fecha ) ) ;
          $compra = date ( 'Y-m-d 00:00:00' , $compra );
          $hoy = date("Y-m-d")." 23:59:59";
          $filter = " B.fecha_registro BETWEEN '$compra' AND '$hoy' ";
          $inner = " INNER JOIN tbl_registros AS B ON (B.id_usuario = A.id_usuario) ";
        }

        if($categoria == 4){
          $rango = explode(",", $valor);
          $min = explode("=", $rango[0]);
          $max = explode("=", $rango[1]);
          $filter = " B.fecha_registro BETWEEN '$min[1] 00:00:00' AND '$max[1] 23:59:59' ";
          $inner = " INNER JOIN tbl_registros AS B ON (B.id_usuario = A.id_usuario) ";
        }

        if($categoria == 5){
          $filter = " B.id_usuario_registro = $valor ";
          $inner = " INNER JOIN tbl_registros AS B ON (B.id_usuario = A.id_usuario) ";
        }

        if($categoria == 6){          
          $filter = " A.id_usuario = $valor ";
          $inner = "";
        }

        if($categoria == 7){
          $rango = explode(",", $valor);
          $min = explode("=", $rango[0]);
          $max = explode("=", $rango[1]);
          $filter = " B.monto_ticket BETWEEN $min[1] AND $max[1] ";
          $inner = " INNER JOIN tbl_registros AS B ON (B.id_usuario = A.id_usuario) ";
        }

        if($categoria == 8){
          $filter = " A.genero = $valor ";
          $inner = "";
        }

        if($categoria == 9){
            $rango = explode(",", $valor);
            $min = explode("=", $rango[0]);
            $max = explode("=", $rango[1]);
            $filter = " A.fecha_nacimiento BETWEEN date_sub(NOW(), INTERVAL $max[1] YEAR) AND date_sub(NOW(), INTERVAL $min[1] YEAR) ";
            $inner = "";
        }

        if($categoria == 10){
            $filter = " B.dia_registro = '$valor' ";
            $inner = " INNER JOIN tbl_registros AS B ON (B.id_usuario = A.id_usuario) ";
        }

        if($categoria == 11){
            $rango = explode(",", $valor);
            $min = explode("=", $rango[0]);
            $max = explode("=", $rango[1]);
            $filter = " CAST((DATE_FORMAT(B.fecha_registro, CONCAT( YEAR(NOW()),'-',MONTH(NOW()),'-',DAY(NOW()),' %H:%i:%s' ) ) ) AS DATETIME) BETWEEN CONCAT( YEAR(NOW()),'-',MONTH(NOW()),'-',DAY(NOW()),' $min[1]:00' ) AND CONCAT( YEAR(NOW()),'-',MONTH(NOW()),'-',DAY(NOW()),' $max[1]:00' ) ";                        
            $inner = " INNER JOIN tbl_registros AS B ON (B.id_usuario = A.id_usuario) ";
        }

        if($categoria == 12){
            
            $sql = " SELECT A.id_usuario, COUNT(A.id_usuario) FROM tbl_usuario AS A INNER JOIN tbl_registros AS B ON (A.id_usuario = B.id_usuario) GROUP BY A.id_usuario HAVING COUNT(A.id_usuario) >= $valor ORDER BY A.id_usuario ASC";
            $result = $this->arrayToAssoc($this->res_con->query($sql));

            $arrayIn = null;
            foreach ($result as $key) {
                $arrayIn.= $arrayIn != null ? ','.$key['id_usuario'] : $key['id_usuario'] ;
            }

            $filter = " A.id_usuario IN ($arrayIn) ";
            $inner = "";
        }

        return array('filter'=> $filter, 'inner' => $inner);
    }

    //FUNCION ACTUALIZA DATOS DE SEGMENTO
    public function updateSegmento($idSegmento = NULL, $arrayUpdate = NULL){
        if($idSegmento != NULL && $arrayUpdate != NULL){
            $update = "";
            foreach ($arrayUpdate as $key => $value){
                $update .= ($update == "" ? "": ", ").$key.'="'.mb_strtoupper(utf8_decode($this->cleanSQL($value))).'"';
            }
            $sqlUpdate = " UPDATE tbl_segmento SET $update WHERE id_segmento = $idSegmento ";
            //die($sqlUpdate);            
            return $this->res_con->query($sqlUpdate);
        }else{
            return false;
        }
    }
    
    //FUNCION CONECTA A BASE DE DATOS
    public function conecta_bd($ser,$usu,$pas,$bd){
        $con = mysqli_connect($ser,$usu,$pas,$bd);
        if ($con){
            return $con;
            mysqli_close($con);
        }else{
       echo("Error description: " . mysqli_error($con));
             exit(); 
        }
    }

    //FUNCION QUE INSERTA SEGMENTO Y REGRESA EL ID
    public function insertSegmentoID($nombreSegmento = NULL, $descripcionSegmento = NULL, $statusSegmento = NULL){

        if($nombreSegmento != NULL){
            $fecha = date("Y-m-d H:i:s");
            $nombre = addslashes($this->cleanSQL($nombreSegmento));
            $descripcion = addslashes($this->cleanSQL($descripcionSegmento));
            $status = $statusSegmento;
            
            $sql=" INSERT INTO tbl_segmento (nombre_segmento, descripcion_segmento, activo, fecha_creacion) VALUES ('$nombreSegmento', '$descripcionSegmento', $status, '$fecha') ";            
            //die($sql);
            $this->res_con->query($sql);

            return $this->res_con->insert_id;
        }
    
        return 0;   
    }

    private function arrayToAssoc($result){
        $auxArray = array();
        while ($fila = mysqli_fetch_assoc($result)) {
            foreach ($fila as $key => $value) {
                $fila[$key] = utf8_encode($value);
            }
            array_push($auxArray, $fila);
        }
        return $auxArray;
    }
    
}
