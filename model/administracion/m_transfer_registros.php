<?php
class M_transfer_registros{
	//FUNCION REGRESA NUMERO DE REGISTROS 
     public function num_reg($ser,$usu,$pas,$bd,$id_usu_origen,$tipo){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
		//OBTENEMOS EL NUMERO DE REGISTROS QUE SE  VAN A CAMBIAR 
        $qr_n='select count(*) as n from  tbl_registros where   id_usuario='.$id_usu_origen. ' and   id_tipo_registro='.$tipo;
        $resp_qr= mysqli_query($conn,$qr_n);
     return $resp_qr;
    }
	//FUNCION MIGRA LOS REGISTROS DE UN USUARIO A OTRO
	public function cambia_registros($ser,$usu,$pas,$bd,$id_usu_origen,$id_usu_destino,$n_acum,$n_red,$id_usu_session){
		$conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
		//MIGRAMOS DATOS
		$qr_migrainfo='UPDATE tbl_registros set id_usuario='.$id_usu_destino.' where id_usuario='.$id_usu_origen;
        $resp_qrmigra= mysqli_query($conn,$qr_migrainfo);
		if($resp_qrmigra){
			//EVALUAMOS TIPO DE PROYECTO
			$qr_proy='select id_tipo_proyecto from tbl_configuracion_proyecto';
			$resp_qr_proy= mysqli_query($conn,$qr_proy);
			$arr=$resp_qr_proy->fetch_assoc();
			$tipo_proy=$arr['id_tipo_proyecto'];
			$anio=date('Y');
			$mes=date('m');
			switch($tipo_proy){
				case 1://PUNTOS
				case 2:
					//$qr_sum_ac='SELECT sum(puntos) as sum  FROM tbl_registros where id_usuario='.$id_usu_origen. ' and id_tipo_registro=1  and date(fecha_registro) between  '.$anio.'-'.$mes.'-01 and now()';
					$qr_sum_ac='SELECT sum(puntos) as sum  FROM tbl_registros where id_usuario='.$id_usu_destino. ' and id_tipo_registro=1 ';
					$qr_sum_re='SELECT sum(puntos) as sum  FROM tbl_registros where id_usuario='.$id_usu_destino. ' and id_tipo_registro=2 ';
				break;
				
				case 3://VISITAS
					 $qr_sum_ac='SELECT sum(num_visita) as sum  FROM tbl_registros where id_usuario='.$id_usu_destino. ' and id_tipo_registro=1  ';
					 $qr_sum_re='SELECT sum(num_visita) as sum  FROM tbl_registros where id_usuario='.$id_usu_destino. ' and id_tipo_registro=2 ';
				break;
				
			}
			//OBTENEMOS SUMATORIA DE REGISTROS DE ACUMULACION Y REDENCION PARA LOG
				$resp_qr_ac= mysqli_query($conn,$qr_sum_ac);
				$reg_ac=$resp_qr_ac->fetch_assoc();
				$sum_ac=$reg_ac['sum'];
				if($sum_ac==''){
					$sum_ac=0;
				}
				$resp_qr_re= mysqli_query($conn,$qr_sum_re);
				$reg_re=$resp_qr_re->fetch_assoc();
			    $sum_re=$reg_re['sum'];
				if($sum_re==''){
					$sum_re=0;
				}
			//GUARDAMOS REGISTRO DE MIGRACION
			 $qr_guarda_log='insert into tbl_log_transfer_registros(id_usuario_origen,id_usuario_destino,num_acumu_transfer,num_reden_transfer,fecha_registro,id_usuario_registro,cant_p_v_transferidas_ac,cant_p_v_transferidas_re)
					values('.$id_usu_origen.','.$id_usu_destino.','.$n_acum.','.$n_red.',now(),'.$id_usu_session.','.$sum_ac.','.$sum_re.')';
			$resp_qrmigra= mysqli_query($conn,$qr_guarda_log);
			
			return true;
		}
		
		
	}

    //FUNCION CONECTA A BASE DE DATOS
    public function conecta_bd($ser,$usu,$pas,$bd,$con){
        $con = mysqli_connect($ser,$usu,$pas,$bd);
        if ($con){
            return $con;
            mysqli_close($con);
        }else{
            echo("Error description: " . mysqli_error($con));
            exit(); 
        }
    }//end function to DB connect
    
}//end principal function

?>