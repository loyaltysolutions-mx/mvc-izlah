<?php
 /*  * ##+> ################################# <+##
 * MODELO DE CONFIGURACION  REGISTRO
 * Desarrolado ->Miguel Ruiz AND Allan Ayrton
 *  * ##+> ################################# <+##
 */
class M_enviar{

    public function revisa_promo($ser,$usu,$pas,$bd){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT * FROM tbl_promociones WHERE sms=1 AND ( fecha_envio >= NOW() AND fecha_envio <= NOW() + interval 30 minute) AND activo=0";

        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function rel_promo_usuarios($ser,$usu,$pas,$bd,$id_promo){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT * FROM tbl_rel_promo_suc_segmento WHERE id_promo='$id_promo' GROUP BY id_segmento ";

        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function usuario_segmento($ser,$usu,$pas,$bd,$idseg){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT * FROM tbl_rel_segmento_usuario WHERE id_segmento='$idseg' AND activo=0 ";

        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function info_promo($ser,$usu,$pas,$bd,$idusr){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT * FROM tbl_usuario WHERE id_usuario='$idusr' AND activo=0 ";

        $query= mysqli_query($conn,$sql);
        return $query;
    }

    
//FUNCION CONECTA A BASE DE DATOS
  public function conecta_bd($ser,$usu,$pas,$bd,$con){
        $con = mysqli_connect($ser,$usu,$pas,$bd);
    if ($con)
    {
        return $con;
         mysqli_close($con);
    }else{
       echo("Error description: " . mysqli_error($con));
             exit(); 
        }
    }


    
    
}

?>