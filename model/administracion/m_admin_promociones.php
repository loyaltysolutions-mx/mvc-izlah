<?php
 /*By Allan*/
class M_admin_promociones{

    public function projectype($ser,$usu,$pas,$bd){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT id_tipo_proyecto FROM tbl_configuracion_proyecto";
        //echo $sql;
        $query= mysqli_query($conn,$sql);
        return $query;
        
    }

    public function addpromo($ser,$usu,$pas,$bd,$promo,$descript,$smalltxt,$type,$conditions,$howuse,$infolink,$bonustype,$rewardto,$rewarperc,$npromos,$nredention,$start,$ending,$push,$sms,$whats,$email,$pushtxt,$smstxt,$whatstxt,$emailtxt,$sendtime,$cashback,$cashbackperc,$descuento,$codigo){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="INSERT INTO tbl_promociones (nombre,detalle,texto_corto,cashback,cashbackperc,descuento,codigo,tipo,condiciones,uso,link_info,tipo_bonus,recompensa_directa,recompensa_porcentaje,limite,limite_por_usuario,comienza,vigencia,push,sms,whatsapp,email,pushtxt,smstxt,whatstxt,emailtxt,fecha_envio) VALUES ('".$promo."','".$descript."','".$smalltxt."','".$cashback."','".$cashbackperc."','".$descuento."','".$codigo."','".$type."','".$conditions."','".$howuse."','".$infolink."','".$bonustype."','".$rewardto."','".$rewarperc."','".$npromos."','".$nredention."','".$start."','".$ending."','".$push."','".$sms."','".$whats."','".$email."','".$pushtxt."','".$smstxt."','".$whatstxt."','".$emailtxt."','".$sendtime."') ";
        //echo $sql;
        $query= mysqli_query($conn,$sql);

        $id_promo = mysqli_insert_id($conn);

        return $id_promo;
    }

    public function addpromorel($ser,$usu,$pas,$bd,$id,$suc,$segment,$lvl){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sucs = sizeof($suc);
        $segments = sizeof($segment);
        $lvls = sizeof($lvl);

        //echo "<script>console.log('".$sucs."')<script>";
        //echo "<script>console.log('".$segments."')<script>";
        //echo "<script>console.log('".$lvls."')<script>";

        if($sucs>0){
            for($i=0;$i<$sucs;$i++){

                if($sucs>0 AND $segments==0 AND $lvls==0){
                    $sql="INSERT INTO tbl_rel_promo_suc_segmento (id_promo,id_sucursal) VALUES ('".$id."','".$suc[$i]."') ";
                    $query = mysqli_query($conn,$sql);
                }

                if($sucs>0 AND $segments>0){

                    for($j=0;$j<$segments;$j++){

                        if($sucs>0 AND $segments>0 AND $lvls==0){
                            $sql="INSERT INTO tbl_rel_promo_suc_segmento (id_promo,id_sucursal,id_segmento) VALUES ('".$id."','".$suc[$i]."','".$segment[$j]."') ";
                            $query = mysqli_query($conn,$sql);
                        }

                        for($k=0;$k<$lvls;$k++){

                            if($sucs>0 AND $segments>0 AND $lvls>0){
                                $sql="INSERT INTO tbl_rel_promo_suc_segmento (id_promo,id_sucursal,id_segmento,id_nivel) VALUES ('".$id."','".$suc[$i]."','".$segment[$j]."','".$lvl[$k]."') ";
                                $query = mysqli_query($conn,$sql);
                            } 
                             
                        }//end last for

                    }//end secondary for

                }elseif($sucs>0 AND $segments==0 AND $lvls>0){

                    for($k=0;$k<$lvls;$k++){

                        $sql="INSERT INTO tbl_rel_promo_suc_segmento (id_promo,id_sucursal,id_nivel) VALUES ('".$id."','".$suc[$i]."','".$lvl[$k]."') ";
                        $query = mysqli_query($conn,$sql);
                        
                         
                    }//end last for
                }

                    
            }//end principal for
        }elseif($sucs==0 AND $segments>0){

            for($j=0;$j<$segments;$j++){

                if($segments>0 AND $lvls==0){
                    $sql="INSERT INTO tbl_rel_promo_suc_segmento (id_promo,id_segmento) VALUES ('".$id."','".$segment[$j]."') ";
                    $query = mysqli_query($conn,$sql);
                }

                for($k=0;$k<$lvls;$k++){

                    if($segments>0 AND $lvls>0){
                        $sql="INSERT INTO tbl_rel_promo_suc_segmento (id_promo,id_segmento,id_nivel) VALUES ('".$id."','".$segment[$j]."','".$lvl[$k]."') ";
                        $query = mysqli_query($conn,$sql);
                    } 
                     
                }//end last for

            }//end principal for
        }elseif($sucs==0 AND $segments==0 AND $lvls>0){

            for($k=0;$k<$lvls;$k++){

                    $sql="INSERT INTO tbl_rel_promo_suc_segmento (id_promo,id_nivel) VALUES ('".$id."','".$lvl[$k]."') ";
                    $query = mysqli_query($conn,$sql);
                 
            }//end for
        }
        

    }//end function

    public function addimg($ser,$usu,$pas,$bd,$id,$img){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="UPDATE tbl_promociones SET imagen_qr='$img' WHERE id_promociones='$id' ";
        
        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function selectype($ser,$usu,$pas,$bd){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT * FROM tbl_campanias WHERE activo=0 ";
        
        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function selectedtype($ser,$usu,$pas,$bd,$selected){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT tipo FROM tbl_promociones WHERE id_promociones='$selected' AND activo=0 ";
        
        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function selectsuc($ser,$usu,$pas,$bd){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT * FROM tbl_sucursales WHERE activo=0";
        
        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function selectedsuc($ser,$usu,$pas,$bd,$selected){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT distinct tbl_promociones.*, tbl_rel_promo_suc_segmento.id_sucursal FROM tbl_promociones INNER JOIN tbl_rel_promo_suc_segmento ON tbl_rel_promo_suc_segmento.id_promo =  tbl_promociones.id_promociones WHERE tbl_rel_promo_suc_segmento.id_promo='$selected' ORDER BY tbl_rel_promo_suc_segmento.id_sucursal ASC";
        
        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function selectsegment($ser,$usu,$pas,$bd){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT * FROM tbl_segmento WHERE activo=0";
        
        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function selectedsegm($ser,$usu,$pas,$bd,$selected){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT distinct tbl_promociones.*, tbl_rel_promo_suc_segmento.id_segmento FROM tbl_promociones INNER JOIN tbl_rel_promo_suc_segmento ON tbl_rel_promo_suc_segmento.id_promo =  tbl_promociones.id_promociones WHERE tbl_rel_promo_suc_segmento.id_promo='$selected' ORDER BY tbl_rel_promo_suc_segmento.id_segmento ASC";
        
        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function selectlvl($ser,$usu,$pas,$bd){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT * FROM tbl_niveles WHERE activo=0";
        
        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function selectedlvl($ser,$usu,$pas,$bd,$selected){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT distinct tbl_promociones.*, tbl_rel_promo_suc_segmento.id_nivel FROM tbl_promociones INNER JOIN tbl_rel_promo_suc_segmento ON tbl_rel_promo_suc_segmento.id_promo =  tbl_promociones.id_promociones WHERE tbl_rel_promo_suc_segmento.id_promo='$selected' ORDER BY tbl_rel_promo_suc_segmento.id_nivel ASC";
        
        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function editpromo($ser,$usu,$pas,$bd,$id,$promo,$descript,$smalltxt,$type,$conditions,$howuse,$infolink,$bonustype,$rewardto,$rewarperc,$npromos,$nredention,$start,$ending,$push,$sms,$whats,$email,$pushtxt,$smstxt,$whatstxt,$emailtxt,$sendtime,$cashback,$cashbackperc,$descuento,$codigo){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="UPDATE tbl_promociones SET nombre='$promo',detalle='$descript',texto_corto='$smalltxt',cashback='$cashback',cashbackperc='$cashbackperc',descuento='$descuento',codigo='$codigo',tipo='$type',condiciones='$conditions',uso='$howuse',link_info='$infolink',tipo_bonus='$bonustype',recompensa_directa='$rewardto',recompensa_porcentaje='$rewarperc',limite='$npromos',limite_por_usuario='$nredention',comienza='$start',vigencia='$ending',push='$push',sms='$sms',whatsapp='$whats',email='$email',pushtxt='$pushtxt',smstxt='$smstxt',whatstxt='$whatstxt',emailtxt='$emailtxt',fecha_envio='$sendtime' WHERE id_promociones='$id' ";
        
        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function duplicate($ser,$usu,$pas,$bd,$id){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="INSERT INTO tbl_promociones (nombre,detalle,texto_corto,cashback,cashbackperc,descuento,codigo,tipo,condiciones,uso,link_info,recompensa_directa,recompensa_porcentaje,limite,limite_por_usuario,comienza,vigencia,push,sms,whatsapp,email,pushtxt,smstxt,whatstxt,emailtxt,fecha_envio,activo) SELECT nombre,detalle,texto_corto,cashback,cashbackperc,descuento,codigo,tipo,condiciones,uso,link_info,recompensa_directa,recompensa_porcentaje,limite,limite_por_usuario,comienza,vigencia,push,sms,whatsapp,email,pushtxt,smstxt,whatstxt,emailtxt,fecha_envio,activo FROM tbl_promociones WHERE id_promociones='$id' ";
        
        $query= mysqli_query($conn,$sql);

        $idnew = mysqli_insert_id($conn);
        return $idnew;
    }

    public function duplicaterel($ser,$usu,$pas,$bd,$id,$idnew){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="INSERT INTO tbl_rel_promo_suc_segmento (id_promo,id_sucursal,id_segmento,id_nivel) SELECT  '$idnew',id_sucursal,id_segmento,id_nivel FROM tbl_rel_promo_suc_segmento WHERE id_promo='$id' ";
        
        echo $sql;
        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function delrow($ser,$usu,$pas,$bd,$id,$table,$field){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="DELETE FROM $table WHERE $field = '$id' ";
        //echo "<script>console.log('".$sql."');</script>";
        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function usedpromo($ser,$usu,$pas,$bd,$id){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT COUNT(id_promo) as usedpromos FROM tbl_registros WHERE id_tipo_registro=5 AND id_promo='$id' ";
        //echo "<script>console.log('".$sql."');</script>";
        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function grideditp($ser,$usu,$pas,$bd){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT * FROM tbl_promociones";

        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function grideditable($ser,$usu,$pas,$bd,$id){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT * FROM tbl_promociones WHERE id_promociones='$id'";

        $query= mysqli_query($conn,$sql);
        return $query;
    }
    
    //FUNCION CONECTA A BASE DE DATOS
    public function conecta_bd($ser,$usu,$pas,$bd,$con){
        $con = mysqli_connect($ser,$usu,$pas,$bd);
        if ($con){
            return $con;
            mysqli_close($con);
        }else{
            echo("Error description: " . mysqli_error($con));
            exit(); 
        }
    }//end function to DB connect
    
}//end principal function

?>