<?php
 /*  * ##+> ################################# <+##
 * MODELO DE CONFIGURACION  REGISTRO
 * Desarrolado ->Miguel Ruiz AND Allan Ayrton
 *  * ##+> ################################# <+##
 */
class M_admin_usu{

    //FUNCION TRAE HISTORIAL USUARIO
    public function registros_usuarios($ser,$usu,$pas,$bd){
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $sql="SELECT U.*,R.* FROM tbl_usuario as U
                inner join tbl_rol as R on R.id_tbl_rol=U.rol
                where U.activo=0 AND U.rol!=3";
        
        //echo $sql;
        $resultado=$res_con->query($sql);
        return $resultado;        
    }

    public function get_clientes($idUsuario = NULL){
        $filter=" A.id_usuario IS NOT NULL AND A.rol = 3";
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT A.*, B.nombre AS nombreNivel, C.nombre AS nombreProyecto, B.ini, B.fin FROM tbl_usuario AS A LEFT JOIN tbl_niveles AS B ON (B.id_nivel = A.id_nivel) LEFT JOIN tbl_tipo_proyecto AS C ON (C.id_tipo_proyecto = B.tipo) WHERE $filter ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }


    //FUNCION INSERTA USUARIO 
    public function registra_usuario($ser,$usu,$pas,$bd,$usua,$pass,$tipo_usu,$tipo_punt_vta){ 
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $usuario=trim(utf8_decode($usua));
        $password=md5(trim(utf8_decode($pass)));
        $tipo_usuario=trim($tipo_usu);
        $sql='insert into tbl_usuario(usuario,password,rol,id_tipo_punto_venta)values("'.$usuario.'","'.$password.'","'.$tipo_usu.'","'.$tipo_punt_vta.'")';
        $res_con->query($sql);
        $id_usuario=mysqli_insert_id($res_con);
        //ASIGNAMOS PANTALLAS SEGUN TIPO DE USUARIO
        switch ($tipo_usu){
         //USUARIO ADMINISTRADOR
            case 1:
            $qr_inserta_rel1='insert into tbl_rel_usuario_area(id_usuario,id_area,activo)values("'.$id_usuario.'",1,0)';
            $res_con->query($qr_inserta_rel1);
            $qr_inserta_rel2='insert into tbl_rel_usuario_area(id_usuario,id_area)values("'.$id_usuario.'",2,0)';
            $res_con->query($qr_inserta_rel2);
            break;
         //USUARIO PV
            case 2:
            $qr_inserta_rel3='insert into tbl_rel_usuario_area(id_usuario,id_area,activo)values("'.$id_usuario.'",3,0)';
            $res_con->query($qr_inserta_rel3);
            break;
            default:
                //echo '-1';
              break;
        }
        //echo $id_usuario; 
        //echo $sql;
    }

    //FUNCION TRAE HISTORIAL USUARIO
    public function passrev($ser,$usu,$pas,$bd,$pass){
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $password=md5($pass);
        $sql="SELECT COUNT(password) as countpass FROM tbl_usuario WHERE password='$password'";
        //echo $sql;
        $resultado=$res_con->query($sql);
        return $resultado;  
    }

    public function grideditable($ser,$usu,$pas,$bd,$id){
        $conn=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="SELECT * FROM tbl_usuario WHERE id_usuario='$id'";

        $query= mysqli_query($conn,$sql);
        return $query;
    }

    public function edita_usuario($ser,$usu,$pas,$bd,$id,$usua,$pass,$tipo_usu,$tipo_punt_vta){ 
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $usuario=trim(utf8_decode($usua));
        $password=md5(trim(utf8_decode($pass)));
        $tipo_usuario=trim($tipo_usu);
        $sql='UPDATE tbl_usuario SET usuario="'.$usuario.'",password="'.$password.'",rol="'.$tipo_usu.'",id_tipo_punto_venta="'.$tipo_punt_vta.'" WHERE id_usuario="'.$id.'" ';
        $res_con->query($sql);
        $id_usuario=mysqli_insert_id($res_con);
        //ASIGNAMOS PANTALLAS SEGUN TIPO DE USUARIO
        switch ($tipo_usu){
         //USUARIO ADMINISTRADOR
            case 1:
            $qr_inserta_rel1='UPDATE tbl_rel_usuario_area SET id_area=1,activo=0 WHERE id_usuario="'.$id.'" ';
            $res_con->query($qr_inserta_rel1);
            $qr_inserta_rel2='UPDATE tbl_rel_usuario_area SET id_area=2,activo=0 WHERE id_usuario="'.$id.'" ';
            $res_con->query($qr_inserta_rel2);
            break;
         //USUARIO PV
            case 2:
            $qr_inserta_rel3='UPDATE tbl_rel_usuario_area SET id_area=3,activo=0 WHERE id_usuario="'.$id.'" ';
            $res_con->query($qr_inserta_rel3);
            break;
            default:
                //echo '-1';
              break;
        }
   //echo $id_usuario; 
        //echo $sql;
    }

//FUNCION ELIMINA USUARIO 
 public function elimina_usuario($ser,$usu,$pas,$bd,$id_usu){ 
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
     
        $sql='update  tbl_usuario set activo=1 where id_usuario='.$id_usu;
        $res_con->query($sql);
        $res_qr=mysqli_insert_id($res_con);

echo '1';
    }
    
//FUNCION CONECTA A BASE DE DATOS
  public function conecta_bd($ser,$usu,$pas,$bd,$con){
        $con = mysqli_connect($ser,$usu,$pas,$bd);
    if ($con)
    {
        return $con;
         mysqli_close($con);
    }else{
       echo("Error description: " . mysqli_error($con));
             exit(); 
        }
    }


    
    
}

?>