<?php

class M_admin_registros{

    private $ser;
    private $usu;
    private $pas;
    private $bd;    

    public function __construct($ser,$usu,$pas,$bd){
        $this->ser=$ser;
        $this->usu=$usu;
        $this->pas=$pas;
        $this->bd=$bd;
        $this->res_con=$this->conecta_bd($ser,$usu,$pas,$bd);         
    }

    //FUNCION LIMPIA ESPACIOS 
    function cleanEspacio($txt){
        return preg_replace("/(\s*)/i","",trim($txt));
    }

    //FUNCION QUE LIMPIA EL SQL INGRESADO
    function cleanSQL($query){
        return preg_replace("/\s(AND|OR|>|<|=|IN|NOT|BETWEEN|LIKE|XOR|UNION|JOIN|INNER)\s/i"," ",$query);
    }
    
    public function getInfoUser($idUsuario = NULL){
        $filter=" A.id_usuario IS NOT NULL AND A.rol = 3";
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);

        $sql = " SELECT A.*, B.nombre AS nombreNivel, C.nombre AS nombreProyecto, B.ini, B.fin FROM tbl_usuario AS A LEFT JOIN tbl_niveles AS B ON (B.id_nivel = A.id_nivel) LEFT JOIN tbl_tipo_proyecto AS C ON (C.id_tipo_proyecto = B.tipo) WHERE $filter ";        
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getRegistros($idSucursal = NULL, $idRegistro = NULL){
        $filter=" A.id_registro_acumulacion IS NOT NULL ";
        $this->cleanEspacio($idRegistro) && $filter.= " AND A.id_registro_acumulacion = ".$this->cleanSQL($idRegistro);
        $this->cleanEspacio($idSucursal) && $filter.= " AND A.id_usuario_registro = ".$this->cleanSQL($idSucursal);
        $sql = " SELECT A.*, B.*, C.nombre AS nombreTipo, D.usuario AS pos, E.id_cat_premios_productos_servicios AS idPremio, E.nombre AS nombreProducto, F.nombre AS nombrePromo FROM tbl_registros AS A INNER JOIN tbl_usuario AS B ON (A.id_usuario = B.id_usuario) INNER JOIN tbl_tipo AS C ON (A.id_tipo_registro = C.id_tipo) INNER JOIN tbl_usuario AS D ON (D.id_usuario = A.id_usuario_registro) LEFT JOIN tbl_cat_premios_productos_servicios AS E ON ( E.id_cat_premios_productos_servicios = A.id_cat_premio ) LEFT JOIN tbl_promociones AS F ON (F.id_promociones = A.id_promo) WHERE $filter ";        
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getUser($idUser = NULL){
        $filter=" A.id_usuario IS NOT NULL AND A.rol = 3 ";
        $this->cleanEspacio($idUser) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUser);
        $sql = " SELECT A.* FROM tbl_usuario AS A WHERE $filter ";
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getTipoProyecto(){
        $sql=" SELECT A.id_tipo_proyecto FROM tbl_configuracion_proyecto AS A";
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getPremios($idPremio = NULL){
        $filter=" A.id_cat_premios_productos_servicios IS NOT NULL ";
        $this->cleanEspacio($idPremio) && $filter.= " AND A.id_cat_premios_productos_servicios = ".$this->cleanSQL($idPremio);
        $sql = " SELECT A.* FROM tbl_cat_premios_productos_servicios AS A WHERE $filter ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getPromos($idPromo = NULL){
        $filter=" A.id_promociones IS NOT NULL ";
        $this->cleanEspacio($idPromo) && $filter.= " AND A.id_promociones = ".$this->cleanSQL($idPromo);
        $sql = " SELECT A.*, A.nombre AS nombrePromo, B.* FROM tbl_promociones AS A INNER JOIN tbl_campanias AS B ON (A.tipo = B.id_camp) WHERE $filter ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function getPuntosTotales($idUsuario = NULL, $idTipoPuntos = NULL){
        $filter=" A.id_puntos_totales IS NOT NULL AND A.id_tipo_puntos = $idTipoPuntos ";
        $this->cleanEspacio($idUsuario) && $filter.= " AND A.id_usuario = ".$this->cleanSQL($idUsuario);
        $sql = " SELECT A.* FROM tbl_puntos_totales AS A WHERE $filter ";
        //die($sql);
        return $this->arrayToAssoc($this->res_con->query($sql));
    }

    public function deleteRegistro($idRegistro = NULL){        
        if($idRegistro != NULL){
            $sql = " DELETE FROM tbl_registros WHERE id_registro_acumulacion = $idRegistro ";
            //die($sql);
            return $this->res_con->query($sql);
        }
        return false;
    }

    public function updatePts($idUsuario = NULL, $pts = NULL, $tipo = NULL, $idTipoPuntos = NULL){
        
        if($tipo != 2)
            $sql = " UPDATE tbl_puntos_totales SET puntos_totales = $pts WHERE id_usuario = $idUsuario AND id_tipo_puntos = $idTipoPuntos ";
        else
            $sql = " UPDATE tbl_puntos_totales SET visitas_totales = $pts WHERE id_usuario = $idUsuario AND id_tipo_puntos = $idTipoPuntos ";
        //die($sql);
        return $this->res_con->query($sql);
    }

    public function update_acumulacion($idRegistro, $id_usuario_plataforma, $monto_ticket, $num_ticket){
        
        include_once '../../inc/funciones.php';
        $inst_basicas=new Funciones_Basicas();
        
        $result = 2;
        //die($id_usuario_plataforma);
        //****REGLA DE NEGOCIO  PARA OBTENER PUNTOS DEL TICKET ****
        $qri_config=" SELECT * FROM tbl_configuracion_proyecto ";
        $resp_consulta_qri_config=$this->res_con->query($qri_config);
        $reg_config= mysqli_fetch_assoc($resp_consulta_qri_config);
        //print_r($reg_config);
        //die();
        $porcentaje_otorgado=$reg_config['porcentaje_margen'];
        $puntos_por_peso=$reg_config['puntos_por_peso'];
        $porcentaje_venta=($porcentaje_otorgado*$monto_ticket)/100;
        $puntos_generados=$puntos_por_peso*$porcentaje_venta;
        $fecha =date("d-m-Y");
        $nombre_dia=$inst_basicas->nombre_dia($fecha);

        //ACTUALIZAMOS REGISTO DE ACUMULACION
        $qri_acumulacion=" UPDATE tbl_registros SET id_usuario = $id_usuario_plataforma, ticket = '$num_ticket', monto_ticket = $monto_ticket, puntos = $puntos_generados WHERE id_registro_acumulacion = $idRegistro ";
        $resp_consulta_qri_acumnulacion=$this->res_con->query($qri_acumulacion);

        //ACTUALIZAMOS REGISTRO DE PUNTOS
        $qri_consulta_pts_acumulacion=" SELECT * FROM tbl_puntos_totales WHERE id_usuario=$id_usuario_plataforma AND id_tipo_puntos = 1 ";
        $resp_consulta_pts_acumulacion=$this->res_con->query($qri_consulta_pts_acumulacion); 
        $reg= mysqli_fetch_assoc($resp_consulta_pts_acumulacion);
        $ptos_actuales=$reg['puntos_totales'];
        $nuevos_ptos=$ptos_actuales+$puntos_generados; 
        //die("N:".$nuevos_ptos);
        if($ptos_actuales==''){
             //INSERTA
            $qri_insert_pts_acumulacion=" INSERT INTO tbl_puntos_totales (id_usuario, puntos_totales, id_tipo_puntos, fecha_registro) VALUES ( $id_usuario_plataforma, '$nuevos_ptos', 1, NOW() ) ";
            //die($qri_insert_pts_acumulacion);
            $this->res_con->query($qri_insert_pts_acumulacion); 
            $result = 1;
        }else{
             //ACTUALIZA
            $id_reg=$reg['id_puntos_totales']; 
            $qri_actualiza_pts_acumulacion=" UPDATE tbl_puntos_totales SET puntos_totales = '$nuevos_ptos', fecha_registro = now() WHERE id_usuario = $id_usuario_plataforma AND id_tipo_puntos = 1";
            //die($qri_actualiza_pts_acumulacion);
            $this->res_con->query($qri_actualiza_pts_acumulacion);
            $result = 1;
        }

        return $result;
    }

    public function update_redencion($idRegistro, $id_usuario_plataforma, $id_producto, $id_proyecto, $tipoProyecto){
        
        include_once '../../inc/funciones.php';
        $inst_basicas=new Funciones_Basicas();
        
        $result = 2;        

        //****REGLA DE NEGOCIO  PARA OBTENER PUNTOS DEL TICKET ****

        $qri_config=" SELECT * FROM tbl_cat_premios_productos_servicios WHERE id_cat_premios_productos_servicios = $id_producto ";
        $resp_consulta_qri_config=$this->res_con->query($qri_config);
        $reg_config= mysqli_fetch_assoc($resp_consulta_qri_config);    
        $puntos_generados=$reg_config['valor_puntos'];
        //die($puntos_generados);
        $fecha =date("d-m-Y");
        $nombre_dia=$inst_basicas->nombre_dia($fecha);

        //ACTUALIZAMOS REGISTO DE REDENCION
        $qri_acumulacion=" UPDATE tbl_registros SET id_usuario = $id_usuario_plataforma, puntos = $puntos_generados, id_cat_premio = $id_producto WHERE id_registro_acumulacion = $idRegistro ";
        $resp_consulta_qri_acumnulacion=$this->res_con->query($qri_acumulacion);

        //ACTUALIZAMOS REGISTRO DE PUNTOS
        $qri_consulta_pts_acumulacion=" SELECT * FROM tbl_puntos_totales WHERE id_usuario=$id_usuario_plataforma AND id_tipo_puntos=2 ";
        $resp_consulta_pts_acumulacion=$this->res_con->query($qri_consulta_pts_acumulacion); 
        $reg= mysqli_fetch_assoc($resp_consulta_pts_acumulacion);

        $qri_consulta_pts_acumulacion1=" SELECT * FROM tbl_puntos_totales WHERE id_usuario=$id_usuario_plataforma AND id_tipo_puntos=1 ";
        $resp_consulta_pts_acumulacion1=$this->res_con->query($qri_consulta_pts_acumulacion1); 
        $reg1= mysqli_fetch_assoc($resp_consulta_pts_acumulacion1);

        $ptos_actuales=$reg['puntos_totales'];
        $nuevos_ptos=$ptos_actuales+$puntos_generados; 
        $field = $tipoProyecto != 2 ? "puntos_totales" : "visitas_totales";
        if($ptos_actuales==''){
             //INSERTA

            $qri_insert_pts_acumulacion=" INSERT INTO tbl_puntos_totales(id_usuario, $field, id_tipo_puntos, fecha_registro) values ( $id_usuario_plataforma, '$nuevos_ptos', 2, now() ) ";
            $res_con->query($qri_insert_pts_acumulacion); 
            $result = 1;
        }else{
            //ACTUALIZA
            $ptos_actuales_acum1=$reg1['puntos_totales']-$puntos_generados;
            //print_r($reg1['puntos_totales']);
            //die("Pts:".$ptos_actuales_acum1);
            $qri_actualiza_pts_acum=" UPDATE tbl_puntos_totales SET $field = '$ptos_actuales_acum1', fecha_registro=now() WHERE id_usuario = $id_usuario_plataforma AND id_tipo_puntos = 1 ";
            //die($qri_actualiza_pts_acum);
            $this->res_con->query($qri_actualiza_pts_acum);

            //$ptos_actuales_acum1=$reg['puntos_totales']+$puntos;            
            $qri_actualiza_pts_acumulacion=" UPDATE tbl_puntos_totales SET $field = '$nuevos_ptos', fecha_registro=now() WHERE id_usuario=$id_usuario_plataforma AND id_tipo_puntos=2 ";
            $this->res_con->query($qri_actualiza_pts_acumulacion);
            $result = 1;
        }

        return $result;
    }

    public function update_promocion($id_usuario_plataforma, $monto_ticket, $num_ticket, $montoCashback = 0, $montoDescuento = 0, $idPromocion, $idRegistro){
        
        require "../../inc/parametros.php";
        include_once "../../inc/funciones.php";    

        $result = 2;
      
        $inst_basicas=new Funciones_Basicas();
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);        
        
        //****REGLA DE NEGOCIO  PARA OBTENER PUNTOS DEL TICKET ****
        $qri_config=" SELECT * FROM tbl_configuracion_proyecto ";
        $resp_consulta_qri_config=$res_con->query($qri_config);
        $reg_config= mysqli_fetch_assoc($resp_consulta_qri_config);
        $porcentaje_otorgado=$reg_config['porcentaje_margen'];
        $puntos_por_peso=$reg_config['puntos_por_peso'];

        //die($puntos_por_peso);
        //die("Monto: ".$montoDescuento);
        $nuevo_monto = $monto_ticket;

        if($montoDescuento != 0 || $montoCashback != 0){
            if($montoDescuento != 0){
                $nuevo_monto = $montoDescuento;            
            }else{
                $nuevo_monto = $monto_ticket - $montoCashback;            
            }
        }

        $porcentaje_venta=($porcentaje_otorgado*$nuevo_monto)/100;
        $puntos_generados=$puntos_por_peso*$porcentaje_venta;
        
        //SE ACTUALIZA REGISTO DE ACUMULACION
        $qri_acumulacion=" UPDATE tbl_registros SET id_usuario = $id_usuario_plataforma, ticket = '$num_ticket', monto_ticket = $monto_ticket, puntos = $puntos_generados, monto_cashback = $montoCashback, monto_descuento = $montoDescuento, id_promo = $idPromocion WHERE id_registro_acumulacion = $idRegistro ";
        //die($qri_acumulacion);
        $resp_consulta_qri_acumnulacion=$res_con->query($qri_acumulacion); 
        //INSERTAMOS REGISTRO DE PUNTOS
        $qri_consulta_pts_acumulacion=" SELECT * FROM tbl_puntos_totales WHERE id_usuario = $id_usuario_plataforma AND id_tipo_puntos = 1 ";
        $resp_consulta_pts_acumulacion=$res_con->query($qri_consulta_pts_acumulacion); 
        $reg= mysqli_fetch_assoc($resp_consulta_pts_acumulacion);
        $ptos_actuales=$reg['puntos_totales'];
        $nuevos_ptos=$ptos_actuales+$puntos_generados; 
        if($ptos_actuales==''){
             //INSERTA
            $qri_insert_pts_acumulacion=" INSERT INTO tbl_puntos_totales (id_usuario,puntos_totales,id_tipo_puntos,fecha_registro) VALUES ($id_usuario_plataforma, '$nuevos_ptos', 1, NOW()) ";
            //die($qri_insert_pts_acumulacion);
            $res_con->query($qri_insert_pts_acumulacion); 
            $result = 1;
        }else{
             //ACTUALIZA
            $id_reg=$reg['id_puntos_totales']; 
            $qri_actualiza_pts_acumulacion=" UPDATE tbl_puntos_totales SET puntos_totales = $nuevos_ptos, fecha_registro = NOW() WHERE id_puntos_totales=$id_reg ";
            //die($qri_actualiza_pts_acumulacion);
            $res_con->query($qri_actualiza_pts_acumulacion);
            $result = 1;
        }

        return $result;
    }

    //FUNCION CONECTA A BASE DE DATOS
    public function conecta_bd($ser,$usu,$pas,$bd){
        $con = mysqli_connect($ser,$usu,$pas,$bd);
        if ($con){
            return $con;
            mysqli_close($con);
        }else{
       echo("Error description: " . mysqli_error($con));
             exit(); 
        }
    }

    private function arrayToAssoc($result){
        $auxArray = array();
        while ($fila = mysqli_fetch_assoc($result)) {
            foreach ($fila as $key => $value) {
                $fila[$key] = utf8_encode($value);
            }
            array_push($auxArray, $fila);
        }
        return $auxArray;
    }
}
