<?php
 /*  * ##+> ################################# <+##
 * MODELO DE CONFIGURACION  REGISTRO
 * Desarrolado ->Miguel Ruiz
 *  * ##+> ################################# <+##
 */
include_once '../../model/registro/m_registro.php';
class M_acumulacion{
//FUNCION REEMPLAZA CARACTERES
public function reemplaza_caracteres($campo){
    $arr_caracteres=array('%40','%C3%91','%C3%B1','%C3%81','%C3%A1','%C3%89','%C3%A9','%C3%8D','%C3%AD','%C3%93','%C3%B3','%C3%9A','%C3%BA','%20');
    $arr_remplazo=array('@','Ñ','ñ','Á','á','É','é','Í','í','Ó','ó','Ú','ú',' ');
    $campo_limpio= str_replace($arr_caracteres,$arr_remplazo,$campo);
    return  $campo_limpio;
}

//FUNCION VALIDA GUARDA REGISTRO DE ACUMULACION
public function guarda_registro_acumulacion($ser,$usu,$pas,$bd,$celular,$qr,$id,$mail,$num_ticket,$monto_ticket,$id_usuario){
    include_once '../../inc/funciones.php';
    include '../../inc/parametros.php';
    $inst_basicas=new Funciones_Basicas();
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    //OBTENEMOS LOS CAMPOS DE CONFIGURACION PARA ACUMULACION
    $qr_consulta_campos_forma_acumular='select CFA.*,FA.* from tbl_rel_config_forma_acumulacion as CFA
                          inner join tbl_forma_acumulacion as FA on FA.id_forma_acumulacion=CFA.id_forma_acumulacion';
    $resp_consulta_campos_forma_acumular=$res_con->query($qr_consulta_campos_forma_acumular);
    while($fila = $resp_consulta_campos_forma_acumular->fetch_assoc()){ 
         switch ($fila['alias']) {
            case 'celular':
                if($celular==''){
                    $celular ='x';
                }
                $telefono='  A.telefono="'.$celular.'" or ';
            break;
            case 'qr':
                if($qr==''){
                    $qr ='x';
                }
                $tarjeta='  B.tarjeta="'.$qr.'" or ';
            break;
            case 'id':
                if($id==''){
                    $id ='x';
                }
                $id='  B.tarjeta="'.$id.'" or ';
            break;
            case 'mail':
                 $email ='  A.email="'.utf8_decode($this->reemplaza_caracteres(trim($mail))).'" or ';
            break;
            default:
            break;
         }
     }

     //die("Entro");
    
    $qri='SELECT count(*) as n from tbl_usuario AS A LEFT JOIN tbl_rel_usuario_tarjeta AS B ON (A.id_usuario = B.id_usuario) where '.$telefono.$tarjeta.$id.$email;
    $qr_us=rtrim($qri,'or ');
    $qr_us= $qr_us.' AND rol=3';
    
    //echo $qr_us;
    $resp_consulta_usu=$res_con->query($qr_us);
    $r= mysqli_fetch_assoc($resp_consulta_usu);
    if($r['n']>0){
        //die("rn:".$r['n']);
        //VALIDAMOS SI NUM TIKET Y MONTO NO ESTAN EN BLANCO
        if($num_ticket!='' and $monto_ticket!=''){
            //OBTENEMOS EL ID DEL USUARIO PARA LA ACUMULACION
            $qri_all=' SELECT A.* from tbl_usuario AS A LEFT JOIN tbl_rel_usuario_tarjeta AS B ON (A.id_usuario = B.id_usuario) where '.$telefono.$tarjeta.$id.$email;
            $qr_id_us=rtrim($qri_all,'or ');
            //echo $qr_id_us;
            $resp_consulta_id_us=$res_con->query($qr_id_us);
            $reg= mysqli_fetch_assoc($resp_consulta_id_us);
            $id_usuario_plataforma=$reg['id_usuario'];
            $num_celular_cte=$reg['telefono'];
			$mail_cte=$reg['email'];
            //OBTENEMOS LA CONFIGURACION GENERAL DEL PROYECTO
            $qri_config='select * from  tbl_configuracion_proyecto';
            $resp_consulta_qri_config=$res_con->query($qri_config);
            $reg_config= mysqli_fetch_assoc($resp_consulta_qri_config);
            //REALIZAMOS PROCESO SEGUN EL TIPO DE ACUMULACION PAS 3-2 DE LA CONFIGURACION
            switch ($reg_config['id_tipo_proyecto']){
            //PUNTOS COMO % DE CONSUMO
                case 1:
                    //****REGLA DE NEGOCIO  PARA OBTENER PUNTOS DEL TICKET****
                    $porcentaje_otorgado=$reg_config['porcentaje_margen'];
                    $puntos_por_peso=$reg_config['puntos_por_peso'];
                    $porcentaje_venta=($porcentaje_otorgado*$monto_ticket)/100;
                    $puntos_generados=$puntos_por_peso*$porcentaje_venta;
                    $fecha =date("d-m-Y");
                    $nombre_dia=$inst_basicas->nombre_dia($fecha);
                    //INSERTAMOS REGISTO DE ACUMULACION
                    $qri_acumulacion='insert into tbl_registros(id_usuario,ticket,monto_ticket,id_configuracion,puntos,fecha_registro,dia_registro,id_tipo_registro,id_usuario_registro)'
                            . 'values('.$id_usuario_plataforma.',"'.$num_ticket.'","'.$monto_ticket.'",'.$reg_config['id_configuracion_proyecto'].',"'.$puntos_generados.'",now(),"'.$nombre_dia.'",1,'.$id_usuario.')';
					$resp_consulta_qri_acumnulacion=$res_con->query($qri_acumulacion); 
                   //INSERTAMOS REGISTRO DE PUNTOS
                     $qri_consulta_pts_acumulacion=' select * from tbl_puntos_totales where id_usuario='.$id_usuario_plataforma.' and id_tipo_puntos=1';
                     $resp_consulta_pts_acumulacion=$res_con->query($qri_consulta_pts_acumulacion); 
                     $reg= mysqli_fetch_assoc($resp_consulta_pts_acumulacion);
                     $ptos_actuales=$reg['puntos_totales'];
                     $nuevos_ptos=$ptos_actuales+$puntos_generados; 
                     if($ptos_actuales==''){
                         //INSERTA
                        $qri_insert_pts_acumulacion='insert tbl_puntos_totales(id_usuario,puntos_totales,id_tipo_puntos,fecha_registro)values('.$id_usuario_plataforma.',"'.$nuevos_ptos.'",1,now())';
                        $res_con->query($qri_insert_pts_acumulacion); 
                          }else{
                         //ACTUALIZA
                        $id_reg=$reg['id_puntos_totales']; 
                        $qri_actualiza_pts_acumulacion='update tbl_puntos_totales set puntos_totales='.$nuevos_ptos.',fecha_registro=now() where id_puntos_totales='.$id_reg;
                        $res_con->query($qri_actualiza_pts_acumulacion);
                    }
                    $this->model_registro=new M_registro();
                    //die("llego");
                    $removeCashBackUser = $this->model_registro->removeCashbackUser($ser,$usu,$pas,$bd, $id_usuario_plataforma);
                    $verifyActiveCashback = $this->model_registro->getActiveCashback($ser,$usu,$pas,$bd);
                    foreach ($verifyActiveCashback as $cashback) {
                        $resultInsertActiveCashback = $this->model_registro->insertUserCashback($ser,$usu,$pas,$bd, $id_usuario_plataforma, $cashback['id_promociones']);
                    }
                    if($resp_consulta_qri_acumnulacion){
                        $tipo_comuni_acumular=$reg_config['id_tipo_comunicacion_acumular'];
                        $tipo_comunicado=explode('-',$tipo_comuni_acumular);
                        for($i=0;$i<=count($tipo_comunicado)-1;$i++){
                            switch ( $tipo_comunicado[$i]){
                                case 1:
                                    //ENVIA SMS
                                   $msg='Felicidades Acumulaste  '.$puntos_generados .' puntos, por tu compra ve tu historial de puntos en www.sitio.com.mx/puntos';
                                   $cel=$num_celular_cte;
                                    //$cel=5527278290;
                                    $inst_basicas->envia_sms($msg, $cel, $usu_sms, $pas_sms, $url_sms);
                                 break;
                                 case 2:
                                     //ENVIA MAIL
									 $mail=$mail_cte;
                                    $mensa='<h3>Notificaci&oacute;n Acumulación  </h3> Felicidades Acumulaste <b>'. $puntos_generados .'</b> puntos por tu compra<br><br>
                                    <hr><h5><font color="#8c8c8c" size="1">* Este correo es generado de forma automatica por la Plataforma de Lealtad, no es necesario que lo respondas</font></h5>';
                                   $inst_basicas->envia_mail($usu_mail, $pas_mail, $from_mail,$mail , 'Acumulación',$mensa);
                                 break;

                                default:
                                    break;
                            }
                            
                        }
                        echo '3';
                    }else{
                        echo 'error al guardar registro';
                    }
				break;
			//PUNTOS POR VISITAS
                case 2:
					//****REGLA DE NEGOCIO ****
					$puntos_generados=$reg_config['puntos_por_visita'];
					$fecha =date("d-m-Y");
                    $nombre_dia=$inst_basicas->nombre_dia($fecha);
					//INSERTAMOS REGISTO DE ACUMULACION
                    $qri_acumulacion='insert into tbl_registros(id_usuario,ticket,monto_ticket,id_configuracion,puntos,fecha_registro,dia_registro,id_tipo_registro,id_usuario_registro)'
                            . 'values('.$id_usuario_plataforma.',"'.$num_ticket.'","'.$monto_ticket.'",'.$reg_config['id_configuracion_proyecto'].',"'.$puntos_generados.'",now(),"'.$nombre_dia.'",1,'.$id_usuario.')';
					$resp_consulta_qri_acumnulacion=$res_con->query($qri_acumulacion); 
                    //INSERTAMOS REGISTRO DE PUNTOS
                    $qri_consulta_pts_acumulacion=' select * from tbl_puntos_totales where id_usuario='.$id_usuario_plataforma.' and id_tipo_puntos=1';
                    $resp_consulta_pts_acumulacion=$res_con->query($qri_consulta_pts_acumulacion); 
                    $reg= mysqli_fetch_assoc($resp_consulta_pts_acumulacion);
                    $ptos_actuales=$reg['puntos_totales'];
                    $nuevos_ptos=$ptos_actuales+$puntos_generados; 
					if($ptos_actuales==''){
                         //INSERTA
                        $qri_insert_pts_acumulacion='insert tbl_puntos_totales(id_usuario,puntos_totales,id_tipo_puntos,fecha_registro)values('.$id_usuario_plataforma.',"'.$nuevos_ptos.'",1,now())';
                        $res_con->query($qri_insert_pts_acumulacion); 
                          }else{
                         //ACTUALIZA
                        $id_reg=$reg['id_puntos_totales']; 
                        $qri_actualiza_pts_acumulacion='update tbl_puntos_totales set puntos_totales='.$nuevos_ptos.',fecha_registro=now() where id_puntos_totales='.$id_reg;
                        $res_con->query($qri_actualiza_pts_acumulacion);
                     }
                    $this->model_registro=new M_registro();
                    //die("llego");
                    $removeCashBackUser = $this->model_registro->removeCashbackUser($ser,$usu,$pas,$bd, $id_usuario_plataforma);
                    $verifyActiveCashback = $this->model_registro->getActiveCashback($ser,$usu,$pas,$bd);
                    foreach ($verifyActiveCashback as $cashback) {
                        $resultInsertActiveCashback = $this->model_registro->insertUserCashback($ser,$usu,$pas,$bd, $id_usuario_plataforma, $cashback['id_promociones']);
                    }
					if($resp_consulta_qri_acumnulacion){
                        $tipo_comuni_acumular=$reg_config['id_tipo_comunicacion_acumular'];
                        $tipo_comunicado=explode('-',$tipo_comuni_acumular);
                        for($i=0;$i<=count($tipo_comunicado)-1;$i++){
                            switch ( $tipo_comunicado[$i]){
                                case 1:
                                    //ENVIA SMS
                                   $msg='Felicidades Acumulaste  '.$puntos_generados .' puntos, por tu compra ve tu historial de puntos en www.sitio.com.mx/puntos';
                                   $cel=$num_celular_cte;
                                    //$cel=5527278290;
                                    $inst_basicas->envia_sms($msg, $cel, $usu_sms, $pas_sms, $url_sms);
                                 break;
                                 case 2:
                                     //ENVIA MAIL
									 $mail=$mail_cte;
                                    $mensa='<h3>Notificaci&oacute;n Acumulación  </h3> Felicidades Acumulaste <b>'. $puntos_generados .'</b> puntos por tu compra<br><br>
                                    <hr><h5><font color="#8c8c8c" size="1">* Este correo es generado de forma automatica por la Plataforma de Lealtad, no es necesario que lo respondas</font></h5>';
                                   $inst_basicas->envia_mail($usu_mail, $pas_mail, $from_mail,$mail , 'Acumulación',$mensa);
                                 break;

                                default:
                                    break;
                            }
                            
                        }
                        echo '3';
                        echo $qr_id_us;
                    }else{
                        echo 'error al guardar registro';
                    }
				break;
			// POR VISITAS
                case 3:
				    //echo ' DEBE GUARDAR VISITA';
					//****REGLA DE NEGOCIO ****
					$fecha =date("d-m-Y");
                    $nombre_dia=$inst_basicas->nombre_dia($fecha);
					//INSERTAMOS REGISTO DE ACUMULACION
                    $qri_acumulacion='insert into tbl_registros(id_usuario,ticket,monto_ticket,id_configuracion,num_visita,fecha_registro,dia_registro,id_tipo_registro,id_usuario_registro)'
                            . 'values('.$id_usuario_plataforma.',"'.$num_ticket.'","'.$monto_ticket.'",'.$reg_config['id_configuracion_proyecto'].',"1",now(),"'.$nombre_dia.'",1,'.$id_usuario.')';
					$resp_consulta_qri_acumnulacion=$res_con->query($qri_acumulacion); 
					 //INSERTAMOS REGISTRO DE VISITAS
                    $qri_consulta_visitas_acumulacion=' select * from tbl_puntos_totales where id_usuario='.$id_usuario_plataforma.' and id_tipo_puntos=1';
                    $resp_consulta_visitas_acumulacion=$res_con->query($qri_consulta_visitas_acumulacion); 
                    $reg= mysqli_fetch_assoc($resp_consulta_visitas_acumulacion);
                    $visitas_actuales=$reg['visitas_totales'];
                    $nuevos_num_visitas=$visitas_actuales+1; 
					if($visitas_actuales==''){
                         //INSERTA
                        $qri_insert_vis_acumulacion='insert tbl_puntos_totales(id_usuario,visitas_totales,id_tipo_puntos,fecha_registro)values('.$id_usuario_plataforma.',"'.$nuevos_num_visitas.'",1,now())';
                        $res_con->query($qri_insert_vis_acumulacion); 
                          }else{
                         //ACTUALIZA
                        $id_reg=$reg['id_puntos_totales']; 
                        $qri_actualiza_vis_acumulacion='update tbl_puntos_totales set visitas_totales='.$nuevos_num_visitas.',fecha_registro=now() where id_puntos_totales='.$id_reg;
                        $res_con->query($qri_actualiza_vis_acumulacion);
                    }
                    $this->model_registro=new M_registro();
                    //die("llego");
                    $removeCashBackUser = $this->model_registro->removeCashbackUser($ser,$usu,$pas,$bd, $id_usuario_plataforma);
                    $verifyActiveCashback = $this->model_registro->getActiveCashback($ser,$usu,$pas,$bd);
                    foreach ($verifyActiveCashback as $cashback) {
                        $resultInsertActiveCashback = $this->model_registro->insertUserCashback($ser,$usu,$pas,$bd, $id_usuario_plataforma, $cashback['id_promociones']);
                    }
					if($resp_consulta_qri_acumnulacion){
                        $tipo_comuni_acumular=$reg_config['id_tipo_comunicacion_acumular'];
                        $tipo_comunicado=explode('-',$tipo_comuni_acumular);
                        for($i=0;$i<=count($tipo_comunicado)-1;$i++){
                            switch ( $tipo_comunicado[$i]){
                                case 1:
                                    //ENVIA SMS
                                   $msg='Felicidades Acumulaste  1 visita al programa de Lealtad, ve tu historial de puntos en www.sitio.com.mx/puntos';
                                   $cel=$num_celular_cte;
                                    //$cel=5527278290;
                                    $inst_basicas->envia_sms($msg, $cel, $usu_sms, $pas_sms, $url_sms);
                                 break;
                                 case 2:
                                     //ENVIA MAIL
									 $mail=$mail_cte;
                                    $mensa='<h3>Notificaci&oacute;n Acumulación  </h3> Felicidades Acumulaste <b> una visita </b> al programa de lealtad<br><br>
                                    <hr><h5><font color="#8c8c8c" size="1">* Este correo es generado de forma automatica por la Plataforma de Lealtad, no es necesario que lo respondas</font></h5>';
                                   $inst_basicas->envia_mail($usu_mail, $pas_mail, $from_mail,$mail , 'Acumulación',$mensa);
                                 break;
                                default:
                                    break;
                            }
                        }
                        echo '3';
                    }else{
                        echo 'error al guardar registro';
                    }
				break;
				
			//%INCREMENTAL
				 case 4:
				  echo 'PROCESO DE %INCREMENTAL';
				 break;
				 

            default:
                echo 'ERROR EN TIPO DE ACUMULACION :(';
            break;
             }
             
        }else{
             echo '2';
        }
  
        
    }else{
     echo '1';
    }
    
}

//FUNCION TRAE LOS CAMPOS PARA ACUMULACION
 public function campos_acumulacion($ser,$usu,$pas,$bd){
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $sql="select CFA.*,FA.* from tbl_rel_config_forma_acumulacion as CFA
                inner join tbl_forma_acumulacion as FA on FA.id_forma_acumulacion=CFA.id_forma_acumulacion";
    $resultado=$res_con->query($sql);
   return $resultado;
        
    }
//FUNCION CONECTA A BASE DE DATOS
  public function conecta_bd($ser,$usu,$pas,$bd,$con){
        $con = mysqli_connect($ser,$usu,$pas,$bd);
    if ($con)
    {
        return $con;
         mysqli_close($con);
    }else{
       echo("Error description: " . mysqli_error($con));
             exit(); 
        }
    }


    
    
}

?>