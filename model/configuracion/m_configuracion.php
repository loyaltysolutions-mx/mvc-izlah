<?php
 /*  * ##+> ################################# <+##
 * MODELO DE CONFIGURACION INICIAL DE LA PLATAFORMA DE LEALTAD
 * Desarrolado ->Miguel Ruiz & Allan M.
 *  * ##+> ################################# <+##
 */
class M_configuracion{

    public function addptovta($ser,$usu,$pas,$bd,$t_punto_venta){
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $sql="INSERT INTO tbl_tipo_punto_venta (nombre) values('$t_punto_venta')";

        $query=$res_con->query($sql);
        return $query;
    }

    public function upreward($ser,$usu,$pas,$bd,$option,$reward,$filename,$description,$cant,$points){
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        
        if($option==1){
            $visits = $points;
            $pts = 0;
        }else if($option==2){
            $visits = 0;
            $pts = $points;
        }

        $sql="INSERT INTO tbl_cat_premios_productos_servicios (nombre,valor_puntos,valor_visitas,id_tipo,imagen,detalle,cantidad,activo) values('".$reward."','".$pts."','".$visits."','1','".$filename."','".$description."','".$cant."','0')";

        $query=$res_con->query($sql);
        return $query;
    }
     
    public function inserta_configuracion($ser,$usu,$pas,$bd,$img_logo,$img_fondo,$color_primario,$color_secundario,$autoregistro,$forma_acumulacion,$tipo_acumulacion,$tipo_redencion,$mecanica_redencion,$bono_bienvenida,$bono_cumpleanios,$tipo_comunicacion_registro,$tipo_comunicacion_acumular,$tipo_comunicacion_redimir,$tipo_comun_periodica,$lapso_comun,$reg,$por_margen,$puntos_por_peso,$num_min_visitas,$puntos_por_visita,$ptos_bienvenida,$ptos_cumpleanios,$sucursales,$menus,$promociones,$niveles,$title,$detail,$tel,$email,$face,$twit,$nsucursales,$app, $tipoConfirmation)
    {
        if(empty($nsucursales)){
            $nsucursales = 5;
        }else{
            $nsucursales;
        }
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        //CONSULTAMOS SIYA EXISTE CONFIGURACION GUARDADA ANTERIORMENTE
        $qr_cuenta_reg='select count(*) as num from tbl_configuracion_proyecto';
        $resp_cuenta_reg=$res_con->query($qr_cuenta_reg);
        $num_reg=mysqli_fetch_assoc($resp_cuenta_reg);

        $sqlverify = "SELECT * FROM tbl_configuracion_proyecto";
        $responsev = $res_con->query($sqlverify);
        while ($row = $responsev->fetch_assoc()) {
            $idestilo = $row['id_estilo'];
            $idconf = $row['id_configuracion_proyecto'];
        }

        if($num_reg['num']>=1){//<------------------------CAMBIAR A 1 PARA QUE SOLO DEJE INSERTAR UNA CONFIGURACION
            //INSERTAMOS EN TABLA ESTILOS
            $qr_inserta_estilo='UPDATE tbl_estilo SET logo_cte="'.$img_logo.'",fondo_cte="'.$img_fondo.'",color_primario="'.$color_primario.'",color_secundario="'.$color_secundario.'" WHERE id_estilo="'.$idestilo.'" ';
            $resp_estilo=$res_con->query($qr_inserta_estilo);
             //OBTIENE EL ID DEL ESTILO
            $id_estilo = mysqli_insert_id($res_con);
            //INSERTA DATOS EN TABLA DE CONFIGURACION
            $qr_inserta_config_proyecto='UPDATE tbl_configuracion_proyecto SET id_tipo_registro="'.$autoregistro.'",id_tipo_proyecto="'.$tipo_acumulacion.'",bono_bienvenida="'.$bono_bienvenida.'",bono_cumple="'.$bono_cumpleanios.'",id_tipo_comunicacion_registro="'.rtrim($tipo_comunicacion_registro,'-').'",id_tipo_comunicacion_acumular="'.rtrim($tipo_comunicacion_acumular,'-').'",id_tipo_comunicacion_redimir="'.rtrim($tipo_comunicacion_redimir,'-').'",id_tipo_comunicacion_periodica="'.rtrim($tipo_comun_periodica,'-').'",id_lapso_comunicacion_periodica="'.$lapso_comun.'",porcentaje_margen="'.$por_margen.'",puntos_por_peso="'.$puntos_por_peso.'",num_min_visitas="'.$num_min_visitas.'",puntos_por_visita="'.$puntos_por_visita.'",puntos_bienvenida="'.$ptos_bienvenida.'",puntos_cumple="'.$ptos_cumpleanios.'",sucursales="'.$sucursales.'",menus="'.$menus.'",promociones="'.$promociones.'",niveles="'.$niveles.'",titulo_home="'.$title.'",detalle_home="'.$detail.'",tel_marca="'.$tel.'",mail_marca="'.$email.'",cta_face="'.$face.'",cta_twit="'.$twit.'",nsucursales="'.$nsucursales.'",app="'.$app.'", tipo_confirm_operacion = '.$tipoConfirmation.' WHERE id_configuracion_proyecto="'.$idconf.'"';
            $resp_config=$res_con->query($qr_inserta_config_proyecto);
            //OBTIENE EL ID DE LA CONFIGURACION
            $id_configuracion = mysqli_insert_id($res_con);
            //INSERTAMOS LA RELACION CONFIGURACION->FOMRA ACUMULACION
            $sqlfa ="TRUNCATE TABLE tbl_rel_config_forma_acumulacion";
            $res_con->query($sqlfa);
            
            $elementos_forma_acumulacion = explode("-", rtrim($forma_acumulacion,'-')); 
             for($i=0;$i<count($elementos_forma_acumulacion);$i++){
                $qr_inserta_rel_conf_forma_acumulacion='INSERT INTO tbl_rel_config_forma_acumulacion(id_configuracion_proyecto,id_forma_acumulacion) values("'.$idconf.'","'.$elementos_forma_acumulacion[$i].'")';
                $res_con->query($qr_inserta_rel_conf_forma_acumulacion);
             }
            //INSERTAMOS LA RELACION CONFIGURACION->TIPO REDENCION
            $sqltr ="TRUNCATE TABLE tbl_rel_config_tipo_redencion";
            $res_con->query($sqltr); 
             
            $elementos_tipo_redencion = explode("-", rtrim($tipo_redencion,'-')); 
            for($i=0;$i<count($elementos_tipo_redencion);$i++){
                $qr_inserta_rel_conf_tipo_redencion='insert into tbl_rel_config_tipo_redencion(id_configuracion_proyecto,id_tipo_redencion) values("'.$idconf.'","'.$elementos_tipo_redencion[$i].'")';
                $res_con->query($qr_inserta_rel_conf_tipo_redencion);
            }
            //INSERTAMOS LA RELACION CONFIGURACION->MECANICA REDENCION
            $sqlmr ="TRUNCATE TABLE tbl_rel_config_mecanica_redencion";
            $res_con->query($sqlmr);
            $elementos_mecanica_redencion = explode("-", rtrim($mecanica_redencion,'-')); 
            for($i=0;$i<count($elementos_mecanica_redencion);$i++){
                $qr_inserta_rel_conf_mecanica_redencion='INSERT INTO tbl_rel_config_mecanica_redencion(id_configuracion_proyecto,id_mecanica_redencion) values("'.$idconf.'","'.$elementos_mecanica_redencion[$i].'")';
                $res_con->query($qr_inserta_rel_conf_mecanica_redencion);
            }
             //INSERTA LA RELACION CONFIGURACION->CAMPOS REGISTRO
            $sqlcr ="TRUNCATE TABLE tbl_rel_config_campos_registro";
            $res_con->query($sqlcr);

            foreach($reg as $posicion=>$dato)
            {
                $ex=explode('|',$dato);
                $qr_inserta_rel_conf_campo_reg='insert into tbl_rel_config_campos_registro(id_config,id_campo,posicion,clave) values('.$idconf.','.$posicion.','.$ex[0].','.$ex[1].')';
                $res_con->query($qr_inserta_rel_conf_campo_reg);
                
            }
             //CREA USUARIO DE ADMINISTRACION AUTOMATICAMENTE
             /*$qr_crea_admin='UPDATE tbl_usuario SET usuario="Admin",password="'.md5(admin).'",activo="0",rol="1" ';
             $resp_crea_admin=$res_con->query($qr_crea_admin);
             //OBTENEMOS ID DE ADMIN CREADO
             $id_admin = mysqli_insert_id($res_con);
             //INSERTAMOS RELACION USUARIO AREA ADMIN
             $qr_insert_rel_usuario_area='UPDATE tbl_rel_usuario_area SET id_usuario="'.$id_admin.'",id_area="1",activo="0" ';
             $resp_insert_rel_usuario_area=$res_con->query($qr_insert_rel_usuario_area);
              //INSERTAMOS RELACION USUARIO AREA REPORTES
             $qr_insert_rel_usuario_rep='UPDATE tbl_rel_usuario_area SET id_usuario="'.$id_admin.'",id_area="2",activo="0" ';
             $resp_insert_rel_usuario_rep=$res_con->query($qr_insert_rel_usuario_rep);*/
             //ASIGNA PANTALLAS  POR DEFAULT AL ADMINISTRADOR CREADO
             //$qr_asigna_pantalla='insert into tbl_pantalla(nombre,ruta,activo,id_area,icono) values("Usuarios","../usuarios/usuarios.php",0,1,"users")';
            // $resp_asigna_pantalla=$res_con->query($qr_asigna_pantalla);
             
            echo '1';
            //echo $qr_inserta_config_proyecto;
        }else{
            //INSERTAMOS EN TABLA ESTILOS
            $qr_inserta_estilo='insert into tbl_estilo(logo_cte,fondo_cte,color_primario,color_secundario) values("'.$img_logo.'","'.$img_fondo.'","'.$color_primario.'","'.$color_secundario.'")';
            $resp_estilo=$res_con->query($qr_inserta_estilo);
             //OBTIENE EL ID DEL ESTILO
            $id_estilo = mysqli_insert_id($res_con);
            //INSERTA DATOS EN TABLA DE CONFIGURACION
            $qr_inserta_config_proyecto='insert into tbl_configuracion_proyecto(id_tipo_registro,id_tipo_proyecto,id_estilo,bono_bienvenida,bono_cumple,id_tipo_comunicacion_registro,id_tipo_comunicacion_acumular,id_tipo_comunicacion_redimir,id_tipo_comunicacion_periodica,id_lapso_comunicacion_periodica,porcentaje_margen,puntos_por_peso,num_min_visitas,puntos_por_visita,puntos_bienvenida,puntos_cumple,sucursales,menus,promociones,niveles,titulo_home,detalle_home,tel_marca,mail_marca,cta_face,cta_twit,nsucursales,app, tipo_confirm_operacion) values("'.$autoregistro.'","'.$tipo_acumulacion.'","'.$id_estilo.'","'.$bono_bienvenida.'","'.$bono_cumpleanios.'","'.rtrim($tipo_comunicacion_registro,'-').'","'.rtrim($tipo_comunicacion_acumular,'-').'","'.rtrim($tipo_comunicacion_redimir,'-').'","'.rtrim($tipo_comun_periodica,'-').'","'.$lapso_comun.'","'.$por_margen.'","'.$puntos_por_peso.'","'.$num_min_visitas.'","'.$puntos_por_visita.'","'.$ptos_bienvenida.'","'.$ptos_cumpleanios.'","'.$sucursales.'","'.$menus.'","'.$promociones.'","'.$niveles.'","'.$title.'","'.$detail.'","'.$tel.'","'.$email.'","'.$face.'","'.$twit.'","'.$nsucursales.'","'.$app.'",'.$tipoConfirmation.')';
            //die($qr_inserta_config_proyecto);
            $resp_config=$res_con->query($qr_inserta_config_proyecto);
            //OBTIENE EL ID DE LA CONFIGURACION
            $id_configuracion = mysqli_insert_id($res_con);
            //INSERTAMOS LA RELACION CONFIGURACION->FOMRA ACUMULACION
            $elementos_forma_acumulacion = explode("-", rtrim($forma_acumulacion,'-')); 
             for($i=0;$i<count($elementos_forma_acumulacion);$i++){
                $qr_inserta_rel_conf_forma_acumulacion='insert into tbl_rel_config_forma_acumulacion(id_configuracion_proyecto,id_forma_acumulacion) values("'.$id_configuracion.'","'.$elementos_forma_acumulacion[$i].'")';
                $res_con->query($qr_inserta_rel_conf_forma_acumulacion);
             }
            //INSERTAMOS LA RELACION CONFIGURACION->TIPO REDENCION
            $elementos_tipo_redencion = explode("-", rtrim($tipo_redencion,'-')); 
             for($i=0;$i<count($elementos_tipo_redencion);$i++){
                $qr_inserta_rel_conf_tipo_redencion='insert into tbl_rel_config_tipo_redencion(id_configuracion_proyecto,id_tipo_redencion) values("'.$id_configuracion.'","'.$elementos_tipo_redencion[$i].'")';
                $res_con->query($qr_inserta_rel_conf_tipo_redencion);
             }
            //INSERTAMOS LA RELACION CONFIGURACION->MECANICA REDENCION
            $elementos_mecanica_redencion = explode("-", rtrim($mecanica_redencion,'-')); 
             for($i=0;$i<count($elementos_mecanica_redencion);$i++){
                $qr_inserta_rel_conf_mecanica_redencion='insert into tbl_rel_config_mecanica_redencion(id_configuracion_proyecto,id_mecanica_redencion) values("'.$id_configuracion.'","'.$elementos_mecanica_redencion[$i].'")';
                $res_con->query($qr_inserta_rel_conf_mecanica_redencion);
             }
             //INSERTA LA RELACION CONFIGURACION->CAMPOS REGISTRO
            foreach($reg as $posicion=>$dato)
            {
                $ex=explode('|',$dato);
                $qr_inserta_rel_conf_campo_reg='insert into tbl_rel_config_campos_registro(id_config,id_campo,posicion,clave) values('.$id_configuracion.','.$posicion.','.$ex[0].','.$ex[1].')';
                $res_con->query($qr_inserta_rel_conf_campo_reg);
                
            }
             //CREA USUARIO DE ADMINISTRACION AUTOMATICAMENTE
             $qr_crea_admin='insert into tbl_usuario(usuario,password,activo,rol) values("Admin","'.md5(admin).'",0,1)';
             $resp_crea_admin=$res_con->query($qr_crea_admin);
             //OBTENEMOS ID DE ADMIN CREADO
             $id_admin = mysqli_insert_id($res_con);
             //INSERTAMOS RELACION USUARIO AREA ADMIN
             $qr_insert_rel_usuario_area='insert into tbl_rel_usuario_area(id_usuario,id_area,activo) values('.$id_admin.',1,0)';
             $resp_insert_rel_usuario_area=$res_con->query($qr_insert_rel_usuario_area);
              //INSERTAMOS RELACION USUARIO AREA REPORTES
             $qr_insert_rel_usuario_rep='insert into tbl_rel_usuario_area(id_usuario,id_area,activo) values('.$id_admin.',2,0)';
             $resp_insert_rel_usuario_rep=$res_con->query($qr_insert_rel_usuario_rep);
             //ASIGNA PANTALLAS  POR DEFAULT AL ADMINISTRADOR CREADO
             //$qr_asigna_pantalla='insert into tbl_pantalla(nombre,ruta,activo,id_area,icono) values("Usuarios","../usuarios/usuarios.php",0,1,"users")';
            // $resp_asigna_pantalla=$res_con->query($qr_asigna_pantalla);
             //echo $qr_inserta_estilo.'<br>';
             //echo $qr_inserta_config_proyecto;
             echo '0';
             //echo $qr_inserta_config_proyecto;
        } 
    }

public function checks($ser,$usu,$pas,$bd){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

    $sql="SELECT tbl_configuracion_proyecto.*,
    tbl_estilo.*,
    tbl_rel_config_forma_acumulacion.*,
    tbl_rel_config_tipo_redencion.*,
    tbl_rel_config_mecanica_redencion.*,
    tbl_rel_config_campos_registro.*
    FROM tbl_configuracion_proyecto
    LEFT JOIN tbl_estilo on tbl_configuracion_proyecto.id_estilo=tbl_estilo.id_estilo
    LEFT JOIN tbl_rel_config_forma_acumulacion on tbl_configuracion_proyecto.id_configuracion_proyecto=tbl_rel_config_forma_acumulacion.id_configuracion_proyecto
    LEFT JOIN tbl_rel_config_tipo_redencion on tbl_configuracion_proyecto.id_configuracion_proyecto=tbl_rel_config_tipo_redencion.id_configuracion_proyecto
    LEFT JOIN tbl_rel_config_mecanica_redencion on tbl_configuracion_proyecto.id_configuracion_proyecto=tbl_rel_config_mecanica_redencion.id_configuracion_proyecto
    LEFT JOIN tbl_rel_config_campos_registro on tbl_configuracion_proyecto.id_configuracion_proyecto=tbl_rel_config_campos_registro.id_config";

    $query=$res_con->query($sql);
    return $query;
}

public function grideditpto($ser,$usu,$pas,$bd){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

    $sql="SELECT * FROM tbl_tipo_punto_venta";

    $query=$res_con->query($sql);
    return $query;
}

public function gridedit($ser,$usu,$pas,$bd){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

    $sql="SELECT * FROM tbl_cat_premios_productos_servicios";

    $query=$res_con->query($sql);
    return $query;
}

public function updaterw($ser,$usu,$pas,$bd,$id,$name,$pts,$visits,$details,$cant){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

    $sql="UPDATE tbl_cat_premios_productos_servicios SET nombre='$name', valor_puntos='$pts', valor_visitas='$visits', detalle='$details', cantidad='$cant' WHERE id_cat_premios_productos_servicios ='$id'; ";

    $query=$res_con->query($sql);
    return $query;
}

public function updatepto($ser,$usu,$pas,$bd,$id,$name){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

    $sql="UPDATE tbl_tipo_punto_venta SET nombre='$name' WHERE id_tipo_punto_venta ='$id'; ";

    $query=$res_con->query($sql);
    return $query;
}

public function updateimg($ser,$usu,$pas,$bd,$id,$file){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

    $sql="UPDATE tbl_cat_premios_productos_servicios SET imagen='$file' WHERE id_cat_premios_productos_servicios ='$id'; ";

    $query=$res_con->query($sql);
    return $query;
}

public function deleterw($ser,$usu,$pas,$bd,$id){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

    $sql="DELETE FROM tbl_cat_premios_productos_servicios WHERE id_cat_premios_productos_servicios ='$id'; ";

    $query=$res_con->query($sql);
    return $query;
}

public function deletepto($ser,$usu,$pas,$bd,$id){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

    $sql="DELETE FROM tbl_tipo_punto_venta WHERE id_tipo_punto_venta ='$id'; ";

    $query=$res_con->query($sql);
    return $query;
}
  
   /*public function obtiene_datos_detalle_bitacora($ser,$usu,$pas,$bd,$f_ini,$f_fin){
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $sql="select BD.id_bitacora,BD.nombre as archivo_descargado,BD.fecha_descarga,
                CONCAT(usuario.nombre,' ',usuario.ap_paterno,' ',usuario.ap_materno) as usuario,
                cat_tipo.nombre as tipo  from bitacora_descargas as BD
                inner join usuario on BD.id_usuario=usuario.idusuario 
                inner join cat_tipo on BD.id_tipo=cat_tipo.id_tipo
                where  date(BD.fecha_descarga) BETWEEN CAST('$f_ini' AS DATE) AND CAST('$f_fin' AS DATE) order by usuario.nombre ASC";
       
        $query=$res_con->query($sql);
        return $query;
    }*/
//FUNCION CONECTA A BASE DE DATOS
    public function conecta_bd($ser,$usu,$pas,$bd,$con){
        $con = mysqli_connect($ser,$usu,$pas,$bd);
        if ($con)
        {
            return $con;
            mysqli_close($con);
        }else{
            echo("Error description: " . mysqli_connect_error($con));
            exit(); 
        }
    }


    
    
}

?>