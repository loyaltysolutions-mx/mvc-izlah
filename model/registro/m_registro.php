<?php
 /*  * ##+> ################################# <+##
 * MODELO DE CONFIGURACION  REGISTRO
 * Desarrolado ->Miguel Ruiz
 *  * ##+> ################################# <+##
 */

class M_registro{

    
    public function __construct($ser,$usu,$pas,$bd) {
        $this->conexion = $this->conecta_bd($ser,$usu,$pas,$bd,$con);
    }
    

//FUNCION REEMPLAZA CARACTERES
public function reemplaza_caracteres($campo){
    $arr_caracteres=array('+','%40','%C3%91','%C3%B1','%C3%81','%C3%A1','%C3%89','%C3%A9','%C3%8D','%C3%AD','%C3%93','%C3%B3','%C3%9A','%C3%BA','%20');
    $arr_remplazo=array(' ','@','Ñ','ñ','Á','á','É','é','Í','í','Ó','ó','Ú','ú',' ');
    $campo_limpio= str_replace($arr_caracteres,$arr_remplazo,$campo);
    return  $campo_limpio;
}

private function cleanEspacio($txt){
    return preg_replace("/(\s*)/i","",trim($txt));
}

private function cleanSQL($query){
    return preg_replace("/\s(AND|OR|>|<|=|IN|NOT|BETWEEN|LIKE|XOR|UNION|JOIN|INNER)\s/i"," ",$query);
}

//FUNCION INSERTA NUEVO USIARIO
public function inserta_usuario($ser,$usu,$pas,$bd,$nombre,$edad,$telefono,$email,$rfc,$clave,$tarjeta,$fecha_cumple,$id_usu_session,$nivel,$genero){
    switch ($clave){
        case 'nombre':
            $reg=$nombre;
        break;
        case 'edad':
            $reg=$edad;
        break;
        case 'telefono':
        case 'teléfono':
        case 'tel%EF%BF%BDfono':
        case 'telï¿½fono':
        case 'tel%C3%A9fono';
            $reg=$telefono;
        break;
        case 'email':
            $reg=$email;
        break;
        case 'rfc':
            $reg=$rfc;
        break;
        case 'id%20Tarjeta':
        case 'id Tarjeta':
        case 'idTarjeta':
            $reg=$tarjeta;
        break;
        case 'nivel':
            $reg=$nivel;
        break;
        default:
        break;
    }
    //important if you don´t have levels actived
    if(empty($POST['nivel'])){
        $nivel=0;
    }

    $fecha_cumple = (str_replace('%2F', '-', $fecha_cumple));
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    //VALIDAMOS SI EL USUARIO YA EXISTE
    if($nombre==''){
        $r=0;
    }else{
        $qr_consulta_usuario='select count(*) as n from  tbl_usuario where usuario="'.utf8_decode($this->reemplaza_caracteres(trim($reg))).'"';
        //echo $qr_consulta_usuario;
        $resp_consulta_usuario=$res_con->query($qr_consulta_usuario);
        $r= mysqli_fetch_assoc($resp_consulta_usuario);
    }
    
    
    if( $r[n]>0){
          echo 'El usuario ya existe en la plataforma';
    }else{
    if(empty($genero)){
        $genero=null;
    }    
    //INSERTAMOS NUEVO USUARIO
    $token = str_shuffle("abcdefghijklmnopqrstuvwxyz0123456789".uniqid());
    $reg = utf8_decode($this->reemplaza_caracteres(trim($reg)));
    $nombre = utf8_decode($this->reemplaza_caracteres($nombre));
    $email = utf8_decode($this->reemplaza_caracteres($email));
    $rfc = utf8_decode($this->reemplaza_caracteres($rfc));
    $tarjeta = utf8_decode($this->reemplaza_caracteres($tarjeta));
    $qr_insert_usuario="INSERT INTO tbl_usuario (usuario, activo, rol, token, nombre, email, rfc, telefono, tarjeta, fecha_nacimiento, fecha_registro_plataforma, id_usuario_registro, id_nivel, genero) VALUES('$reg', 0, 3, '$token', '$nombre', '$email', '$rfc', '$telefono', '$tarjeta', '$fecha_cumple', NOW(), '$id_usu_session', $nivel, '$genero')";
    //die($qr_insert_usuario);
    $resp_insert_usuario=$res_con->query($qr_insert_usuario);
    $id_insert=$res_con->insert_id;

    //echo $qr_insert_usuario;
    //CREAMOS IMAGEN CODIGO QR DE USUARIO
    include("../../inc/phpqrcode/qrlib.php");  
    //Declaramos una carpeta temporal para guardar la imagenes generadas
    $dir = "../../../intranet/webservices/API_burgerfi/inc/images_qr/";
    //$dir = "images_qr/";
    $nombre_qr=$id_insert.'.png';
    //Si no existe la carpeta la creamos
    if (!file_exists($dir)){
        mkdir($dir);
        //echo 'creo fichero';
    return;
    }
    //Declaramos la ruta y nombre del archivo a generar
    $filename = $dir.$nombre_qr;
    //Parametros de Condiguración
    $tamaño = 10; //Tamaño de Pixel
    $level = 'L'; //Precisión Baja
    $framSize = 3; //Tamaño en blanco
    $contenido = $id_insert; 
    //Enviamos los parametros a la Función para generar código QR 
    QRcode::png($contenido, $filename, $level, $tamaño, $framSize); 
    
     //INSERTAMOS LA RELACION USUARIO AREA
    $qr_insert_rel_usuario_area='insert into tbl_rel_usuario_area(id_usuario,id_area,activo) values('.$id_insert.',4,0)';
    $resp_insert_rel_usuario_area=$res_con->query($qr_insert_rel_usuario_area);
     //EVALUAMOS SI TIENE BONO DE BIENVENIDA Y CUANTOS PUNTOS
    $qr_consulta_config='select *  from  tbl_configuracion_proyecto';
    $resp_consulta_config=$res_con->query($qr_consulta_config);
    $res= mysqli_fetch_assoc($resp_consulta_config);
    if($res['bono_bienvenida']==1){
        //SI TIENE BONO DE BIENVENIDA Y SE INSERTA EL BONO
        $ptos_bienvenida=$res['puntos_bienvenida'];
        $qr_insert_bono='insert into tbl_registros(id_usuario,ticket,id_configuracion,puntos,fecha_registro,id_tipo_registro) values('.$id_insert.',"Bono Bienvenida",'.$res['id_configuracion_proyecto'].',"'.$ptos_bienvenida.'",now(),3)';
        $res_con->query($qr_insert_bono);
        //INSERTAMOS PUNTOS EN TABLA DE PUNTOS TOTALES
        $qr_insert_total='insert into tbl_puntos_totales(id_usuario,puntos_totales,id_tipo_puntos,fecha_registro) values('.$id_insert.',"'.$ptos_bienvenida.'",1,now())';
        $res_con->query($qr_insert_total);
        //echo $qr_insert_total;
    }else{
        $qr_insert_total='insert into tbl_puntos_totales(id_usuario,puntos_totales, visitas_totales, id_tipo_puntos,fecha_registro) values('.$id_insert.', 0, 0, 1, now())';
        $res_con->query($qr_insert_total);

        $qr_insert_total='insert into tbl_puntos_totales(id_usuario,puntos_totales, visitas_totales, id_tipo_puntos,fecha_registro) values('.$id_insert.', 0, 0, 2, now())';
        $res_con->query($qr_insert_total);
    }
    //echo $qr_insert_usuario;
    //return $token;
    return array('token' => $token, 'id_usuario' => $id_insert);
  
    }
  
}
//FUNCION TRAE LOS CAMPOS DEL REGISTRO    
 public function campos_registro($ser,$usu,$pas,$bd){
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $sql="select CCR.*,CONF.id_configuracion_proyecto,CONF.bono_cumple, CR.nombre as campo from tbl_rel_config_campos_registro as CCR
                inner join  tbl_configuracion_proyecto as CONF on CCR.id_config=CONF.id_configuracion_proyecto
                inner join tbl_campos_registro as CR on CR.id_campos_registro=CCR.id_campo
                order by CCR.posicion asc ";
    $resultado=$res_con->query($sql);
   return $resultado;
        
}

public function revisar($ser,$usu,$pas,$bd,$telefono,$email){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    if($email!=''){
        $email = utf8_decode($this->reemplaza_caracteres($email));
        $sql="SELECT COUNT(*) as numero FROM tbl_usuario WHERE email ='$email' ";
    }else{
        $sql="SELECT COUNT(*) as numero FROM tbl_usuario WHERE telefono='$telefono' ";
    }

    $resultado=$res_con->query($sql);
    return $resultado;
}

public function obtener_usuario($ser,$usu,$pas,$bd,$telefono,$email){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    if($email!=''){
        $email = utf8_decode($this->reemplaza_caracteres($email));
        $sql="SELECT id_usuario FROM tbl_usuario WHERE email ='$email' ";
    }else{
        $sql="SELECT id_usuario FROM tbl_usuario WHERE telefono='$telefono' ";
    }
    
    $resultado=$res_con->query($sql);
    return $resultado;
}

public function addtarjeta($ser,$usu,$pas,$bd,$tarjeta,$id){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql="INSERT INTO tbl_rel_usuario_tarjeta (id_usuario,tarjeta) VALUES ('$id','$tarjeta')";
    $resultado=$res_con->query($sql);
    return $resultado;
}

public function trae_campos_reg($ser,$usu,$pas,$bd){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql="SELECT rel.*, acu.alias FROM tbl_forma_acumulacion AS acu
        INNER JOIN tbl_rel_config_forma_acumulacion AS rel
        ON rel.id_forma_acumulacion = acu.id_forma_acumulacion";
    $resultado=$res_con->query($sql);
    return $resultado;
}

public function trae_nivel($ser,$usu,$pas,$bd){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql="SELECT niveles FROM tbl_configuracion_proyecto";
    $resultado=$res_con->query($sql);
    return $resultado;
}

public function select_niveles($ser,$usu,$pas,$bd){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql="SELECT * FROM tbl_niveles";
    $resultado=$res_con->query($sql);
    return $resultado;
}

public function obt_nivel($ser,$usu,$pas,$bd,$variable){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql="SELECT * FROM tbl_niveles WHERE '$variable' BETWEEN ini AND fin OR '$variable' > fin ORDER BY fin DESC LIMIT 1 ";
    $resultado=$res_con->query($sql);
    return $resultado;
}

public function get_sum($ser,$usu,$pas,$bd,$user){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    //$sql="SELECT SUM(puntos) as sumpuntos FROM tbl_registros WHERE id_usuario='$user' AND id_tipo_registro=1";
    //die($sql);
    $sql = "SELECT puntos_totales AS sumpuntos FROM tbl_puntos_totales WHERE id_usuario = $user AND id_tipo_puntos = 1";
    $resultado=$res_con->query($sql);
    return $resultado;
}

public function get_sum_bienvenida($ser,$usu,$pas,$bd,$user){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql="SELECT SUM(puntos) as sumpuntos FROM tbl_registros WHERE id_usuario=$user AND id_tipo_registro=3";
    //die($sql);
    $resultado=$res_con->query($sql);
    return $resultado;
}

public function update_nivel($ser,$usu,$pas,$bd,$user,$lvls){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql="UPDATE tbl_usuario SET id_nivel='$lvls' WHERE id_usuario='$user' ";
    $resultado=$res_con->query($sql);
    return $resultado;
}

public function select_niveles1($ser,$usu,$pas,$bd){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql="SELECT * FROM tbl_niveles";
    $resultado=$res_con->query($sql);
    return $this->arrayToAssoc($resultado);
}

public function get_usuario($ser,$usu,$pas,$bd, $idUsuario, $tel = NULL, $mail = NULL, $id = NULL){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $filter=" A.id_usuario IS NOT NULL AND A.rol = 3 ";
    $this->cleanSQL($idUsuario) && $filter.= " AND (B.tarjeta = '$idUsuario' OR A.id_usuario = $idUsuario)";
    $this->cleanSQL($tel) && $filter.= " AND A.telefono = '".$tel."'";
    $this->cleanSQL($mail) && $filter.= " AND A.email = '".$mail."'";
    $this->cleanSQL($id) && $filter.= " AND B.tarjeta = '".$id."'";

    $sql = " SELECT *, A.id_usuario AS idUsuario, C.nombre AS nombreNivel, C.fin as finNivel, A.nombre AS nombreUsuario  FROM tbl_usuario AS A LEFT JOIN tbl_rel_usuario_tarjeta AS B ON (A.id_usuario = B.id_usuario) LEFT JOIN tbl_niveles AS C ON (C.id_nivel = A.id_nivel) WHERE $filter ";
    //die($sql);
        
    $resultado=$res_con->query($sql);
    return $this->arrayToAssoc($resultado);
}

    //FUNCION VERIFICAR USUARIO
    public function verifyUser($ser,$usu,$pas,$bd,$username = NULL){
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        if($username != NULL){
            $sql=" SELECT * FROM tbl_usuario WHERE usuario = '$username' ";
            //die($sql);
            return $this->arrayToAssoc($res_con->query($sql));
        }

        return false;
    }

public function get_puntos_disponibles($ser,$usu,$pas,$bd,$idUsuario){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    //$sql="SELECT * FROM tbl_puntos_totales AS A WHERE A.id_usuario = $idUsuario AND A.id_tipo_puntos = 1 ORDER BY A.fecha_registro DESC ";    
    $sql="SELECT * FROM tbl_puntos_totales AS A INNER JOIN tbl_usuario AS B ON (A.id_usuario = B.id_usuario) WHERE A.id_usuario = $idUsuario AND A.id_tipo_puntos = 1 ORDER BY A.fecha_registro DESC ";    
    //die($sql);
    $resultado=$res_con->query($sql);
    return $this->arrayToAssoc($resultado);
}

public function get_productos($ser,$usu,$pas,$bd,$puntos = NULL,$visitas = NULL){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

    $filter=" A.id_cat_premios_productos_servicios IS NOT NULL AND A.activo = 0 ";
    $puntos != NULL && $filter.= " AND A.valor_puntos <= ".$puntos;
    $visitas != NULL && $filter.= " AND A.valor_visitas <= ".$visitas;

    $sql=" SELECT * FROM tbl_cat_premios_productos_servicios AS A WHERE $filter ORDER BY A.valor_puntos ASC, A.valor_visitas ASC, A.nombre ASC ";
    //die($sql);

    $resultado=$res_con->query($sql);
    return $this->arrayToAssoc($resultado);
}

public function getActiveCashback($ser,$usu,$pas,$bd,$fecha){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd);

    $sql=" SELECT * FROM tbl_promociones AS A WHERE '$fecha' BETWEEN A.comienza AND A.vigencia AND A.tipo = 1 AND A.activo = 0 ORDER BY A.nombre ASC ";
    //die($sql);
    $resultado=$res_con->query($sql);
    return $this->arrayToAssoc($resultado);
}

public function get_promociones($ser,$usu,$pas,$bd,$idUsuario){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql=" SELECT DISTINCT D.id_promociones, D.nombre, D.texto_corto, D.detalle, D.limite, D.limite_por_usuario, D.tipo, D.cashback, D.cashbackperc, D.descuento, D.comienza, D.vigencia, E.nombre AS nombreCampana FROM tbl_rel_promo_suc_segmento AS A LEFT JOIN tbl_segmento AS B ON (B.id_segmento = A.id_Segmento) LEFT JOIN tbl_rel_segmento_usuario AS C ON (C.id_segmento = B.id_segmento) INNER JOIN tbl_promociones AS D ON (A.id_promo = D.id_promociones) INNER JOIN tbl_campanias AS E ON (E.id_camp = D.tipo) LEFT JOIN tbl_usuario AS F ON (F.id_nivel = A.id_nivel) WHERE D.activo = 0 AND (C.id_usuario = $idUsuario OR F.id_usuario = $idUsuario) AND NOW() BETWEEN CAST(CONCAT(D.comienza,' 00:00:00') AS DATETIME) AND CAST(CONCAT(D.vigencia, ' 23:59:59') AS DATETIME) AND D.tipo != 1 ";
    //die($sql);
    $resultado=$res_con->query($sql);
    return $this->arrayToAssoc($resultado);
}

public function getPromocion($ser, $usu, $pas, $bd, $idPromocion){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql = " SELECT * FROM tbl_promociones AS A WHERE id_promociones = $idPromocion ";
    //die($sql);
    $resultado=$res_con->query($sql);
    return $this->arrayToAssoc($resultado);   
}

public function get_cashback_user($ser,$usu,$pas,$bd,$idUsuario){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql=" SELECT DISTINCT B.id_promociones, B.nombre, B.texto_corto, B.detalle, B.limite, B.limite_por_usuario, B.tipo, B.cashback, B.cashbackperc, B.descuento, B.comienza, B.vigencia, C.nombre AS nombreCampana FROM tbl_rel_usuario_cash AS A INNER JOIN tbl_promociones AS B ON (A.id_promo = B.id_promociones) INNER JOIN tbl_campanias AS C ON (C.id_camp = B.tipo) WHERE B.activo = 0 AND A.id_usuario = $idUsuario AND A.fecha_registro BETWEEN CAST(CONCAT(B.comienza,' 00:00:00') AS DATETIME) AND CAST(CONCAT(B.vigencia, ' 23:59:59') AS DATETIME) AND B.tipo = 1 AND B.activo = 0";
    $resultado=$res_con->query($sql);
    return $this->arrayToAssoc($resultado);
}

//AGREGA A LOS USUARIOS QUE OBTUVIERON ALGUN CASHBACK DURANTE SU COMPRA
public function insertUserCashback($ser, $usu, $pas, $bd, $idUsuario = NULL, $idPromocion = NULL, $fecha){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql = " INSERT INTO tbl_rel_usuario_cash (id_usuario, id_promo, fecha_registro) VALUES ($idUsuario, $idPromocion, '$fecha') ";
    //die($sql);
    return $res_con->query($sql);
}

public function get_count_promociones($ser, $usu, $pas, $bd, $idUsuario = NULL, $idPromocion = NULL, $fecha = NULL){

    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
   
    $filter=" A.id_registro_acumulacion IS NOT NULL AND A.id_tipo_registro = 5 ";    
    $this->cleanSQL($idUsuario) && $filter.= " AND A.id_usuario = ".$idUsuario;
    $this->cleanSQL($idPromocion) && $filter.= " AND A.id_promo = ".$idPromocion;
    $this->cleanSQL($fecha) && $filter.= " AND A.fecha_registro BETWEEN '".$fecha."'";

    $sql=" SELECT B.id_promociones AS id_promo, count(A.id_promo) AS conteo FROM tbl_registros AS A INNER JOIN tbl_promociones AS B ON (B.id_promociones = A.id_promo) WHERE $filter AND B.activo = 0 ";
    //echo $sql;
    //die($sql);
    $resultado=$res_con->query($sql);
    return $this->arrayToAssoc($resultado);
}

public function get_count_productos($ser, $usu, $pas, $bd, $idUsuario = NULL, $idProducto = NULL){

    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
   
    $filter=" A.id_registro_acumulacion IS NOT NULL AND A.id_tipo_registro = 2 ";    
    $this->cleanSQL($idUsuario) && $filter.= " AND A.id_usuario = ".$idUsuario;
    $this->cleanSQL($idProducto) && $filter.= " AND A.id_cat_premio = ".$idProducto;    

    $sql=" SELECT A.id_cat_premio, count(A.id_cat_premio) AS conteo FROM tbl_registros AS A INNER JOIN tbl_cat_premios_productos_servicios AS B ON (B.id_cat_premios_productos_servicios = A.id_cat_premio) WHERE $filter ";
    //echo $sql;
    $resultado=$res_con->query($sql);
    return $this->arrayToAssoc($resultado);
}

private function arrayToAssoc($result){
    $auxArray = array();
    while ($fila = mysqli_fetch_assoc($result)) {
        foreach ($fila as $key => $value) {
            $fila[$key] = utf8_encode($value);
        }
        array_push($auxArray, $fila);
    }
    return $auxArray;
}

public function get_sucursal($ser,$usu,$pas,$bd, $password){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql=" SELECT A.id_usuario FROM tbl_usuario AS A WHERE A.password = '$password' AND A.rol = 2 ";    
    $resultado=$res_con->query($sql);
    return $this->arrayToAssoc($resultado);
}

public function get_redencion($ser,$usu,$pas,$bd, $idRedencion){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql=" SELECT A.id_cat_premios_productos_servicios, A.valor_puntos, nombre FROM tbl_cat_premios_productos_servicios AS A WHERE A.id_cat_premios_productos_servicios = $idRedencion ";    
    $resultado=$res_con->query($sql);
    return $this->arrayToAssoc($resultado);
}

public function removeCashbackUser($ser,$usu,$pas,$bd, $idUsuario){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql=" DELETE FROM tbl_rel_usuario_cash WHERE id_usuario = $idUsuario ";    
    $resultado=$res_con->query($sql);
    return $resultado;
}

public function verifyTicket($ser,$usu,$pas,$bd, $ticket){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql=" SELECT COUNT(*) AS countTicket FROM tbl_registros AS A WHERE ticket = '$ticket' ";    
    $resultado=$res_con->query($sql);
    $existTicket = $this->arrayToAssoc($resultado);
    return $existTicket[0]['countTicket'] > 0 ? true : false;
}

public function verifyEmail($ser,$usu,$pas,$bd, $email = null){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    if($email != null){
        $sql=" SELECT COUNT(*) AS countRegistro FROM tbl_usuario AS A WHERE email = '$email' ";    
        $resultado=$res_con->query($sql);
        $existTicket = $this->arrayToAssoc($resultado);
        return $existTicket[0]['countRegistro'] > 0 ? true : false;
    }

    return false;
    
}

public function verifyRegistro($ser, $usu, $pas, $bd, $idUsuario){
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
    $sql=" SELECT COUNT(*) AS countRegistro FROM tbl_usuario AS A WHERE id_usuario = $idUsuario AND ( token IS NULL OR token = '' )";
    //die($sql);
    $resultado=$res_con->query($sql);
    $verifyRegistro = $this->arrayToAssoc($resultado);
    return $verifyRegistro[0]['countRegistro'] > 0 ? true : false;
}

public function insert_registro($ser,$usu,$pas,$bd,$id_usuario_plataforma, $id_sucursal, $monto_ticket, $num_ticket){
    
    require "../../inc/parametros.php";
    include_once '../../inc/funciones.php';
    
    $result = 2;

    $inst_basicas=new Funciones_Basicas();
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

    $qri_all=" SELECT * FROM tbl_usuario where id_usuario = $id_usuario_plataforma";
    //die($qri_all);
    $resp_consulta_id_us=$res_con->query($qri_all);
    $reg= mysqli_fetch_assoc($resp_consulta_id_us);
    $num_celular_cte=$reg['telefono'];
    $mail_cte=$reg['email'];
    
    //****REGLA DE NEGOCIO  PARA OBTENER PUNTOS DEL TICKET ****
    $qri_config='select * from  tbl_configuracion_proyecto';
    $resp_consulta_qri_config=$res_con->query($qri_config);
    $reg_config= mysqli_fetch_assoc($resp_consulta_qri_config);

    switch ($reg_config['id_tipo_proyecto']) {
        case 1:
        case 2:
            if($reg_config['id_tipo_proyecto'] == 1){
                $porcentaje_otorgado=$reg_config['porcentaje_margen'];
                $puntos_por_peso=$reg_config['puntos_por_peso'];
                $porcentaje_venta=($porcentaje_otorgado*$monto_ticket)/100;
                $puntos_generados=$puntos_por_peso*$porcentaje_venta;
            }else{
                $puntos_generados=$reg_config['puntos_por_visita'];
            }
            
            $fecha = date("d-m-Y");
            $nombre_dia=$inst_basicas->nombre_dia($fecha);
            //INSERTAMOS REGISTO DE ACUMULACION
            $qri_acumulacion='insert into tbl_registros(id_usuario,ticket,monto_ticket,id_configuracion,puntos,fecha_registro,dia_registro,id_tipo_registro,id_usuario_registro)'
                    . 'values('.$id_usuario_plataforma.',"'.$num_ticket.'","'.$monto_ticket.'",'.$reg_config['id_configuracion_proyecto'].',"'.$puntos_generados.'",now(),"'.$nombre_dia.'",1,'.$id_sucursal.')';
            $resp_consulta_qri_acumnulacion=$res_con->query($qri_acumulacion); 
            //INSERTAMOS REGISTRO DE PUNTOS
             $qri_consulta_pts_acumulacion=' select * from tbl_puntos_totales where id_usuario='.$id_usuario_plataforma.' and id_tipo_puntos=1';
             $resp_consulta_pts_acumulacion=$res_con->query($qri_consulta_pts_acumulacion); 
             $reg= mysqli_fetch_assoc($resp_consulta_pts_acumulacion);
             $ptos_actuales=$reg['puntos_totales'];
             $nuevos_ptos=$ptos_actuales+$puntos_generados; 
             if($ptos_actuales==''){
                //INSERTA
                $qri_insert_pts_acumulacion='insert tbl_puntos_totales(id_usuario,puntos_totales,id_tipo_puntos,fecha_registro)values('.$id_usuario_plataforma.',"'.$nuevos_ptos.'",1,now())';
                $result = $res_con->query($qri_insert_pts_acumulacion);
            }else{
                 //ACTUALIZA
                $id_reg=$reg['id_puntos_totales']; 
                $qri_actualiza_pts_acumulacion='update tbl_puntos_totales set puntos_totales='.$nuevos_ptos.',fecha_registro=now() where id_puntos_totales='.$id_reg;
                $result = $res_con->query($qri_actualiza_pts_acumulacion);        
            }
        break;
        
        case 3:
            $fecha =date("d-m-Y");
            $nombre_dia=$inst_basicas->nombre_dia($fecha);
            //INSERTAMOS REGISTO DE ACUMULACION
            $qri_acumulacion=" INSERT INTO tbl_registros(id_usuario,ticket,monto_ticket,id_configuracion,num_visita,fecha_registro,dia_registro,id_tipo_registro,id_usuario_registro) VALUES('$id_usuario_plataforma','$num_ticket','$monto_ticket','".$reg_config['id_configuracion_proyecto']."','1',now(),'$nombre_dia',1,'$id_usuario_registro') ";
            $resp_consulta_qri_acumnulacion=$res_con->query($qri_acumulacion); 
             //INSERTAMOS REGISTRO DE VISITAS
            $qri_consulta_visitas_acumulacion=" SELECT * from tbl_puntos_totales where id_usuario=$id_usuario_plataforma AND id_tipo_puntos=1 ";
            $resp_consulta_visitas_acumulacion=$res_con->query($qri_consulta_visitas_acumulacion); 
            $reg= mysqli_fetch_assoc($resp_consulta_visitas_acumulacion);
            $visitas_actuales=$reg['visitas_totales'];
            $nuevos_num_visitas=$visitas_actuales+1; 
            if($visitas_actuales==''){
                 //INSERTA
                $qri_insert_vis_acumulacion=" INSERT INTO tbl_puntos_totales (id_usuario,visitas_totales,id_tipo_puntos,fecha_registro)values($id_usuario_plataforma,'$nuevos_num_visitas',1,now())";
                $result = $res_con->query($qri_insert_vis_acumulacion); 
            }else{
                 //ACTUALIZA
                $id_reg=$reg['id_puntos_totales']; 
                $qri_actualiza_vis_acumulacion=" UPDATE tbl_puntos_totales SET visitas_totales = $nuevos_num_visitas, fecha_registro = now() WHERE id_puntos_totales = $id_reg ";
                $result =  $res_con->query($qri_actualiza_vis_acumulacion);
            }
        break;        
    }
    
    if($result == 1){
        $tipo_comuni_acumular=$reg_config['id_tipo_comunicacion_acumular'];        
        $tipo_comunicado=explode('-',$tipo_comuni_acumular);
        for($i=0;$i<=count($tipo_comunicado)-1;$i++){
            switch ( $tipo_comunicado[$i]){
                case 1:
                    //ENVIA SMS
                    if($reg_config['id_tipo_proyecto'] == 1 || $reg_config['id_tipo_proyecto'] == 2)
                        $msg="Felicidades ".$reg['nombre'].", acumulaste $puntos_generados puntos por tu compra, ve tu historial de puntos en www.desclub.com.mx/izlah";
                    else
                        $msg="Felicidades ".$reg['nombre'].", acumulaste  1 visita al programa de Lealtad, ve tu historial de puntos en www.desclub.com.mx/izlah";

                    $cel=$num_celular_cte;
                    //$cel="5532074760";
                    //die($usu_sms);
                    $inst_basicas->envia_sms($msg, $cel, $usu_sms, $pas_sms, $url_sms);                    
                break;
                case 2:
                    //ENVIA MAIL
                    $mail=$mail_cte;

                    if($reg_config['id_tipo_proyecto'] == 1 || $reg_config['id_tipo_proyecto'] == 2){
                        $mensa='<h3>Notificaci&oacute;n Acumulación  </h3> Felicidades '.$reg['nombre'].', acumulaste <b>'. $puntos_generados .'</b> puntos por tu compra, ve tu historial de puntos en www.desclub.com.mx/izlah<br><br>
                        <hr><h5><font color="#8c8c8c" size="1">* Este correo es generado de forma automatica por la Plataforma de Lealtad, no es necesario que lo respondas</font></h5>';
                    }else{
                        $mensa='<h3>Notificaci&oacute;n Acumulación  </h3> Felicidades '.$reg['nombre'].', acumulaste <b> una visita </b> al programa de lealtad, ve tu historial de puntos en www.desclub.com.mx/izlah<br>
                                    <hr><h5><font color="#8c8c8c" size="1">* Este correo es generado de forma automatica por la Plataforma de Lealtad, no es necesario que lo respondas</font></h5>';
                    }                    

                    $inst_basicas->envia_mail($usu_mail, $pas_mail, $from_mail,$mail , 'Acumulación',$mensa);                 
                break;

                default:
                break;
            }
        }        
    }

    return array( 'result' => $result, 'puntos' => $puntos_generados ? $puntos_generados : NULL );

  }

public function insert_redencion($ser,$usu,$pas,$bd,$id_usuario_plataforma, $id_sucursal, $puntos, $idRedencion){

    require "../../inc/parametros.php";
    include_once '../../inc/funciones.php';

    $result = 2;
    $inst_basicas=new Funciones_Basicas();
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

    $qri_all=" SELECT * FROM tbl_usuario where id_usuario = $id_usuario_plataforma";
    //die($qri_all);
    $resp_consulta_id_us=$res_con->query($qri_all);
    $reg= mysqli_fetch_assoc($resp_consulta_id_us);    
    $num_celular_cte=$reg['telefono'];
    $mail_cte=$reg['email'];
    $nombreCliente = $reg['nombre'];

    // **** SE OBTIENE EL TIPO DE PROYECTO ****
    $qri_config='select * from  tbl_configuracion_proyecto';
    $resp_consulta_qri_config=$res_con->query($qri_config);
    $reg_config= mysqli_fetch_assoc($resp_consulta_qri_config);  

    $id_tipo_proyecto=$reg_config['id_tipo_proyecto'];
    $result = 2;
    //EVALUAMOS  TIPO DE PROYECTO PUNTOS O VISITAS
    $val_visitas = NULL;
    switch($id_tipo_proyecto){
        //PUNTOS
        case 1:
        case 2:
            $puntos_generados=$puntos;
            $fecha =date("d-m-Y");
            $nombre_dia=$inst_basicas->nombre_dia($fecha);
            //INSERTAMOS REGISTO DE REDENCION
            $qri_acumulacion='insert into tbl_registros(id_usuario,id_configuracion,puntos,fecha_registro,dia_registro,id_tipo_registro,id_usuario_registro, id_cat_premio)'
                    . 'values('.$id_usuario_plataforma.','.$reg_config['id_configuracion_proyecto'].',"'.$puntos_generados.'",now(),"'.$nombre_dia.'",2,'.$id_sucursal.','.$idRedencion.')';
            $resp_consulta_qri_acumnulacion=$res_con->query($qri_acumulacion); 
            //INSERTAMOS REGISTRO DE PUNTOS
            $qri_consulta_pts_acumulacion=' select * from tbl_puntos_totales where id_usuario='.$id_usuario_plataforma.' and id_tipo_puntos=2';
            $resp_consulta_pts_acumulacion=$res_con->query($qri_consulta_pts_acumulacion); 
            $reg= mysqli_fetch_assoc($resp_consulta_pts_acumulacion);

            $qri_consulta_pts_acumulacion1=' select * from tbl_puntos_totales where id_usuario='.$id_usuario_plataforma.' and id_tipo_puntos=1';
            $resp_consulta_pts_acumulacion1=$res_con->query($qri_consulta_pts_acumulacion1); 
            $reg1= mysqli_fetch_assoc($resp_consulta_pts_acumulacion1);

            $ptos_actuales=$reg['puntos_totales'];
            $nuevos_ptos=$ptos_actuales+$puntos_generados; 
            if($ptos_actuales==''){
                 //INSERTA
                $qri_insert_pts_acumulacion='insert tbl_puntos_totales(id_usuario,puntos_totales,id_tipo_puntos,fecha_registro)values('.$id_usuario_plataforma.',"'.$nuevos_ptos.'",2,now())';
                $res_con->query($qri_insert_pts_acumulacion);

                $ptos_actuales_acum1=$reg1['puntos_totales']-$puntos;
                $qri_actualiza_pts_acum='update tbl_puntos_totales set puntos_totales='.$ptos_actuales_acum1.',fecha_registro=now() where id_usuario='.$id_usuario_plataforma.' and id_tipo_puntos=1';
                $res_con->query($qri_actualiza_pts_acum);

                $result = 1;
            }else{
                //ACTUALIZA
                $ptos_actuales_acum1=$reg1['puntos_totales']-$puntos;
                $qri_actualiza_pts_acum='update tbl_puntos_totales set puntos_totales='.$ptos_actuales_acum1.',fecha_registro=now() where id_usuario='.$id_usuario_plataforma.' and id_tipo_puntos=1';
                $res_con->query($qri_actualiza_pts_acum);
                
                $qri_actualiza_pts_acumulacion='update tbl_puntos_totales set puntos_totales='.$nuevos_ptos.',fecha_registro=now() where id_usuario='.$id_usuario_plataforma.' and id_tipo_puntos=2';
                $res_con->query($qri_actualiza_pts_acumulacion);
                $result = 1;
            }

            $redencion = $this->get_redencion($ser,$usu,$pas,$bd, $idRedencion);
        break;
        
        //VISITAS
        case 3:
            $fecha =date("d-m-Y");
            $nombre_dia=$inst_basicas->nombre_dia($fecha);
            $qri_cat=" SELECT * FROM tbl_cat_premios_productos_servicios where id_cat_premios_productos_servicios = $idRedencion";
            //die($qri_all);
            $resp_query_cat=$res_con->query($qri_cat);
            $res= mysqli_fetch_assoc($resp_query_cat); 
            $val_visitas=$res['valor_visitas'];
            $qr_inserta_redencion='insert into tbl_registros(id_usuario,num_visita,fecha_registro,dia_registro,id_tipo_registro,id_usuario_registro,id_cat_premio) '
                . 'values('.$id_usuario_plataforma.',"'.$val_visitas.'",now(),"'.$nombre_dia.'",2,'.$id_usuario_registro.','.$idRedencion.')';
            $resp_inserta_redencion=$res_con->query($qr_inserta_redencion);
            
            //CONSULTAMOS VISITAS DE REDENCION
            $qri_consulta_visitas_redencion=' select * from tbl_puntos_totales where id_usuario='.$id_usuario_plataforma.' and id_tipo_puntos=2';
            $resp_consulta_visitas_redencion=$res_con->query($qri_consulta_visitas_redencion); 
            $reg= mysqli_fetch_assoc($resp_consulta_visitas_redencion);
            $visitas_actuales=$reg['visitas_totales'];
            $nuevos_visit=$visitas_actuales+$val_visitas;


            if($visitas_actuales==''){
                //INSERTA REDENCION
                $qri_insert_visitas_redencion='insert tbl_puntos_totales(id_usuario,visitas_totales,id_tipo_puntos,fecha_registro)values('.$id_usuario_plataforma.',"'.$nuevos_visit.'",2,now())';
                $res_con->query($qri_insert_visitas_redencion);
                //DESCUENTA ACUMULADOS
                $qri_consulta_visitas_acumulacion=' select * from tbl_puntos_totales where id_usuario='.$id_usuario_plataforma.' and id_tipo_puntos=1';
                $resp_consulta_pts_acumulacion=$res_con->query($qri_consulta_visitas_acumulacion);
                $reg= mysqli_fetch_assoc($resp_consulta_pts_acumulacion);
                $id_reg=$reg['id_puntos_totales'];
                $visit_actuales_acum=$reg['visitas_totales']-$val_visitas; 
                $qri_actualiza_visit_acum='update tbl_puntos_totales set visitas_totales='.$visit_actuales_acum.',fecha_registro=now() where id_puntos_totales='.$id_reg;
                $res_con->query($qri_actualiza_visit_acum);                         
                $result = 1;
            }else{
                //ACTUALIZA REDENCIONES
                $id_reg=$reg['id_puntos_totales']; 
                $qri_actualiza_visitas_redencion='update tbl_puntos_totales set visitas_totales='.$nuevos_visit.',fecha_registro=now() where id_puntos_totales='.$id_reg;
                $res_con->query($qri_actualiza_visitas_redencion);
                //DESCUENTA ACUMULADOS
                $qri_consulta_visitas_acumulacion=' select * from tbl_puntos_totales where id_usuario='.$id_usuario_plataforma.' and id_tipo_puntos=1';
                $resp_consulta_visitas_acumulacion=$res_con->query($qri_consulta_visitas_acumulacion); 
                $reg_visit_acum= mysqli_fetch_assoc($resp_consulta_visitas_acumulacion);
                $id_reg_acum=$reg_visit_acum['id_puntos_totales'];   
                $visitas_actuales_acum=$reg_visit_acum['visitas_totales']-$val_visitas; 
                $qri_actualiza_visit_acum='update tbl_puntos_totales set visitas_totales='.$visitas_actuales_acum.',fecha_registro=now() where id_puntos_totales='.$id_reg_acum;
                $res_con->query($qri_actualiza_visit_acum);
                $result = 1;
            }
        break;
    }
        
    //EMISION DE SMS, MAIL, Y/O PUSH NOTIFICATIONS    
    if($result == 1){
        
        $tipo_comuni_acumular=$reg_config['id_tipo_comunicacion_redimir'];
        $tipo_comunicado=explode('-',$tipo_comuni_acumular);
    
        for($i=0;$i<=count($tipo_comunicado)-1;$i++){
            switch ( $tipo_comunicado[$i]){
                case 1:
                    //ENVIA SMS
                    if($id_tipo_proyecto == 1 || $id_tipo_proyecto == 2)
                        $msg=" Felicidades $nombreCliente, redimiste ".$puntos_generados." puntos por ".$redencion[0]['nombre'].". Para consultar tu historial de puntos ingresa a www.desclub.com.mx/izlah";
                    else
                        $msg="Felicidades $nombreCliente, acumulaste 1 visita al programa de Lealtad. Para consultar tu historial de puntos ingresa a www.desclub.com.mx/izlah";
                    //die("Count:".$url_sms);
                    $cel=$num_celular_cte;
                    //$cel="5532074760";
                    $inst_basicas->envia_sms($msg, $cel, $usu_sms, $pas_sms, $url_sms);
                break;
                case 2:
                    //ENVIA MAIL
                    $mail=$mail_cte;

                    if($id_tipo_proyecto == 1 || $id_tipo_proyecto == 2)
                        $mensa="<h3>Notificaci&oacute;n Redención  </h3> ¡Felicidades $nombreCliente, redimiste <b>$puntos_generados</b> puntos por tu ".$redencion[0]['nombre']."!<br><br><hr><h5><font color='#8c8c8c' size='1'>** Este correo es generado de forma automatica por la Plataforma de Lealtad, no es necesario que lo respondas **</font></h5>";
                    else
                        $mensa="<h3>Notificaci&oacute;n Redención  </h3> Felicidades $nombreCliente, redimiste  <b>$val_visitas</b> visitas por <b>".$redencion[0]['nombre']."</b> ¡Disfrútalo! <br><br><hr><h5><font color='#8c8c8c' size='1'>* Este correo es generado de forma automatica por la Plataforma de Lealtad, no es necesario que lo respondas</font></h5>";
                    
                    $inst_basicas->envia_mail($usu_mail, $pas_mail, $from_mail,$mail , 'Redención',$mensa);
                break;

                default:
                break;
            }            
        }        
    }

    return $result;

  }

/*public function insert_promocion($ser,$usu,$pas,$bd,$id_usuario_plataforma, $id_sucursal, $idPromocion){


    /*CONFIGURACION DE CUENTA DE MAIL
    $usu_mail='seguimiento.leads@desclub.com.mx';
    $pas_mail='Leads_2016*';
    $from_mail='Plataforma Lealtad';

    /*CONFIGURACION SMS
    $usu_sms='miguelruiz';
    $pas_sms='B92DB17';
    $url_sms='http://www.masmensajes.com.mx/wss/jsonService.php';

    $result = 2;

    include_once '../../inc/funciones.php';
    $inst_basicas=new Funciones_Basicas();
    $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

    $qri_all=" SELECT * FROM tbl_usuario where id_usuario = $id_usuario_plataforma";
    //die($qri_all);
    $resp_consulta_id_us=$res_con->query($qri_all);
    $reg= mysqli_fetch_assoc($resp_consulta_id_us);    
    $num_celular_cte=$reg['telefono'];
    $mail_cte=$reg['email'];

    //****REGLA DE NEGOCIO  PARA OBTENER PUNTOS DEL TICKET ****
    $qri_config='select * from  tbl_configuracion_proyecto';
    $resp_consulta_qri_config=$res_con->query($qri_config);
    $reg_config= mysqli_fetch_assoc($resp_consulta_qri_config);

    //OBTENER LA PROMOCION
    $qri_promo="SELECT * from tbl_promociones where id_promociones = $idPromocion";
    
    $resp_consulta_qri_promo=$res_con->query($qri_promo);
    $reg_promo= mysqli_fetch_assoc($resp_consulta_qri_promo);
 
    $fecha =date("d-m-Y");
    $nombre_dia=$inst_basicas->nombre_dia($fecha);
    //INSERTAMOS REGISTO DE ACUMULACION
    $qri_acumulacion='insert into tbl_registros(id_usuario,id_configuracion,fecha_registro,dia_registro,id_tipo_registro,id_usuario_registro, id_promo)'
            . 'values('.$id_usuario_plataforma.','.$reg_config['id_configuracion_proyecto'].',now(),"'.$nombre_dia.'",5,'.$id_sucursal.','.$idPromocion.')';
    $resp_consulta_qri_acumnulacion=$res_con->query($qri_acumulacion);     
    
    if($resp_consulta_qri_acumnulacion){
        $result = 1;
        $tipo_comuni_acumular=$reg_config['id_tipo_comunicacion_acumular'];
        $tipo_comunicado=explode('-',$tipo_comuni_acumular);
        for($i=0;$i<=count($tipo_comunicado)-1;$i++){
            switch ( $tipo_comunicado[$i]){
                case 1:
                    //ENVIA SMS
                   $msg='Felicidades, se ha aplicado la promoción '.$reg_promo['nombre'].' ('.$reg_promo['detalle'].') por tu compra. Ve tu historial de puntos y promociones en www.sitio.com.mx/puntos';
                   $cel=$num_celular_cte;
                    //$cel=5527278290;
                    $inst_basicas->envia_sms($msg, $cel, $usu_sms, $pas_sms, $url_sms);
                    
                break;
                case 2:
                  //ENVIA MAIL
                  $mail=$mail_cte;                  
                  $mensa='<h3>Notificaci&oacute;n Promoción  </h3> ¡Felicidades, se ha aplicado la promoción <b>'.$reg_promo['nombre'].' ('.$reg_promo['detalle'].')</b> por tu compra. Ve tu historial de puntos y promociones en www.desclub.com.mx/plataforma_lealtad<br><br>
                  <hr><h5><font color="#8c8c8c" size="1">* Este correo es generado de forma automatica por la Plataforma de Lealtad, no es necesario que lo respondas</font></h5>';
                 $inst_basicas->envia_mail($usu_mail, $pas_mail, $from_mail,$mail , 'Promoción',$mensa);                 
                break;

                default:
                break;
            }            
        }        
    }

    return $result;

  }*/

    public function insert_promocion($ser,$usu,$pas,$bd, $id_usuario_plataforma, $id_sucursal, $monto_ticket, $num_ticket, $montoCashback = 0, $montoDescuento = 0, $idPromocion, $fecha){

    
        require "../../inc/parametros.php";
        include_once "../../inc/funciones.php";    

        /*CONFIGURACION DE CUENTA DE MAIL*/            
        $result = 2;
      
        $inst_basicas=new Funciones_Basicas();
        $res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);

        $qri_all=" SELECT * FROM tbl_usuario WHERE id_usuario = $id_usuario_plataforma";
        //die($qri_all);
        $resp_consulta_id_us=$res_con->query($qri_all);
        $reg= mysqli_fetch_assoc($resp_consulta_id_us);    
        $num_celular_cte=$reg['telefono'];
        $mail_cte=$reg['email'];
        
        //****REGLA DE NEGOCIO  PARA OBTENER PUNTOS DEL TICKET ****
        $qri_config='select * from  tbl_configuracion_proyecto';
        $resp_consulta_qri_config=$res_con->query($qri_config);
        $reg_config= mysqli_fetch_assoc($resp_consulta_qri_config);

         $id_tipo_proyecto=$reg_config['id_tipo_proyecto'];
        //EVALUAMOS  TIPO DE PROYECTO PUNTOS O VISITAS
        switch($id_tipo_proyecto){
            //PUNTOS
            case 1:
            case 2:
                
                if($reg_config['id_tipo_proyecto'] == 1){
                    $porcentaje_otorgado=$reg_config['porcentaje_margen'];
                    $puntos_por_peso=$reg_config['puntos_por_peso'];

                    $nuevo_monto = $monto_ticket;

                    if($montoDescuento != 0 || $montoCashback != 0){
                        if($montoDescuento != 0){
                            $nuevo_monto = $montoDescuento;            
                        }else{
                            $nuevo_monto = $monto_ticket - $montoCashback;            
                        }
                    }

                    $porcentaje_venta=($porcentaje_otorgado*$nuevo_monto)/100;
                    $puntos_generados=$puntos_por_peso*$porcentaje_venta;
                }else{
                    $puntos_generados=$reg_config['puntos_por_visita'];
                }
                
                $fecha =date("d-m-Y");
                $nombre_dia=$inst_basicas->nombre_dia($fecha);
                //INSERTAMOS REGISTO DE ACUMULACION
                $qri_acumulacion=" INSERT INTO tbl_registros(id_usuario,ticket,monto_ticket,id_configuracion,puntos,fecha_registro,dia_registro,id_tipo_registro,id_usuario_registro, monto_cashback, monto_descuento, id_promo) VALUES ($id_usuario_plataforma,'$num_ticket',$monto_ticket,".$reg_config['id_configuracion_proyecto'].",$puntos_generados, NOW(),'$nombre_dia', 5, $id_sucursal, $montoCashback, $montoDescuento, $idPromocion) ";
                //die($qri_acumulacion);
                $resp_consulta_qri_acumnulacion=$res_con->query($qri_acumulacion); 
                //INSERTAMOS REGISTRO DE PUNTOS
                 $qri_consulta_pts_acumulacion=' select * from tbl_puntos_totales where id_usuario='.$id_usuario_plataforma.' and id_tipo_puntos=1';
                 $resp_consulta_pts_acumulacion=$res_con->query($qri_consulta_pts_acumulacion); 
                 $reg= mysqli_fetch_assoc($resp_consulta_pts_acumulacion);
                 $ptos_actuales=$reg['puntos_totales'];
                 $nuevos_ptos=$ptos_actuales+$puntos_generados; 
                 if($ptos_actuales==''){
                     //INSERTA
                    $qri_insert_pts_acumulacion='insert tbl_puntos_totales(id_usuario,puntos_totales,id_tipo_puntos,fecha_registro)values('.$id_usuario_plataforma.',"'.$nuevos_ptos.'",1,now())';
                    $res_con->query($qri_insert_pts_acumulacion); 
                    $result = 1;
                }else{
                     //ACTUALIZA
                    $id_reg=$reg['id_puntos_totales']; 
                    $qri_actualiza_pts_acumulacion='update tbl_puntos_totales set puntos_totales='.$nuevos_ptos.',fecha_registro=now() where id_puntos_totales='.$id_reg;
                    $res_con->query($qri_actualiza_pts_acumulacion);
                    $result = 1;
                }

            break;

            //VISITAS
            case 3:
                $fecha =date("d-m-Y");
                $nombre_dia=$inst_basicas->nombre_dia($fecha);
                //INSERTAMOS REGISTO DE ACUMULACION
                $qri_acumulacion=" INSERT INTO tbl_registros(id_usuario,ticket,monto_ticket,id_configuracion,num_visita,fecha_registro,dia_registro,id_tipo_registro,id_usuario_registro, monto_cashback, monto_descuento, id_promo) VALUES('$id_usuario_plataforma','$num_ticket','$monto_ticket','".$reg_config['id_configuracion_proyecto']."','1',now(),'$nombre_dia',5,'$id_usuario_registro', $montoCashback, $montoDescuento, $idPromocion) ";
                $resp_consulta_qri_acumnulacion=$res_con->query($qri_acumulacion); 
                 //INSERTAMOS REGISTRO DE VISITAS
                $qri_consulta_visitas_acumulacion=" SELECT * from tbl_puntos_totales where id_usuario=$id_usuario_plataforma AND id_tipo_puntos=1 ";
                $resp_consulta_visitas_acumulacion=$res_con->query($qri_consulta_visitas_acumulacion); 
                $reg= mysqli_fetch_assoc($resp_consulta_visitas_acumulacion);
                $visitas_actuales=$reg['visitas_totales'];
                $nuevos_num_visitas=$visitas_actuales+1; 
                if($visitas_actuales==''){
                    //INSERTA
                    $qri_insert_vis_acumulacion=" INSERT INTO tbl_puntos_totales (id_usuario,visitas_totales,id_tipo_puntos,fecha_registro)values($id_usuario_plataforma,'$nuevos_num_visitas',1,now())";
                    $result = $res_con->query($qri_insert_vis_acumulacion); 
                }else{
                     //ACTUALIZA
                    $id_reg=$reg['id_puntos_totales']; 
                    $qri_actualiza_vis_acumulacion=" UPDATE tbl_puntos_totales SET visitas_totales = $nuevos_num_visitas, fecha_registro = now() WHERE id_puntos_totales = $id_reg ";
                    $result =  $res_con->query($qri_actualiza_vis_acumulacion);
                }
            break;
        }
        
        $promocion = $this->getPromocion($ser,$usu,$pas,$bd, $idPromocion);
        
        if($result == 1){
            $tipo_comuni_acumular=$reg_config['id_tipo_comunicacion_acumular'];
            $tipo_comunicado=explode('-',$tipo_comuni_acumular);
            //print_r($tipo_comuni_acumular);
            //die();
            for($i=0;$i<=count($tipo_comunicado)-1;$i++){
                switch ( $tipo_comunicado[$i]){
                    case 1:
                        //ENVIA SMS
                        if($id_tipo_proyecto == 1 || $id_tipo_proyecto == 2)
                            $msg='Felicidades '.$reg['nombre'].', acumulaste '.$puntos_generados .' puntos por tu promoción '.$promocion[0]['nombre'].'. Ve tu historial de puntos en www.desclub.com.mx/izlah';
                        else
                            $msg="Felicidades ".$reg['nombre'].", acumulaste  1 visita por tu promoción ".$promocion[0]['nombre'].". Ve tu historial de puntos en www.desclub.com.mx/izlah";
                       
                       $cel=$num_celular_cte;
                       //$cel="5532074760";
                       $inst_basicas->envia_sms(utf8_decode($msg), $cel, $usu_sms, $pas_sms, $url_sms);
                        
                    break;
                    case 2:
                        //ENVIA MAIL
                        $mail=$mail_cte;
                        if($id_tipo_proyecto == 1 || $id_tipo_proyecto == 2){
                            $mensa='<h3>Notificaci&oacute;n Promoción</h3> Felicidades '.$reg['nombre'].', acumulaste <b>'. $puntos_generados .'</b> puntos por tu promocion '.$promocion[0]['nombre'].'<br><br>
                            <hr><h5><font color="#8c8c8c" size="1">** Este correo es generado de forma automatica por la Plataforma de Lealtad, no es necesario que lo respondas **</font></h5>';
                        }else{
                            $mensa='<h3>Notificaci&oacute;n Promoción</h3> Felicidades '.$reg['nombre'].', acumulaste <b> una visita </b>  por tu promocion '.$promocion[0]['nombre'].', Te invitamos a ver tu historial de puntos en www.desclub.com.mx/izlah<br>
                            <hr><h5><font color="#8c8c8c" size="1">* Este correo es generado de forma automatica por la Plataforma de Lealtad, no es necesario que lo respondas</font></h5>';
                        }
                        $inst_basicas->envia_mail($usu_mail, $pas_mail, $from_mail,$mail , 'Promoción',$mensa);                 
                    break;

                    default:
                    break;
                }
            }        
        }
        return $result;
    }

    //FUNCION CONECTA A BASE DE DATOS
    public function conecta_bd($ser,$usu,$pas,$bd,$con){
        $con = mysqli_connect($ser,$usu,$pas,$bd);
        if ($con){
            return $con;
            mysqli_close($con);
        }else{
            echo("Error description: " . mysqli_error($con));
            exit(); 
        }
    }
    
}

?>