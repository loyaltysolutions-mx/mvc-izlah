<?php
 /*  * ##+> ################################# <+##
 * MODELO DE VALIDACION DE PLATAFORMA DE LEALTAD
 * Desarrolado ->Miguel Ruiz
 *  * ##+> ################################# <+##
 */
class M_valida{

    private $ser;
    private $usu;
    private $pas;
    private $bd;

    public function __construct($ser,$usu,$pas,$bd) {
         $this->ser=$ser;
         $this->usu=$usu;
         $this->pas=$pas;
         $this->bd=$bd;
         $this->res_con=$this->conecta_bd($ser,$usu,$pas,$bd);

     }

     public function insertMenuClient($idUsuario = null){
        if($idUsuario != null){
            $sql = "INSERT INTO tbl_rel_usuario_area (id_usuario, id_area, activo) VALUES ($idUsuario, 4, 0)";
            //die($sql);
            $result = $this->res_con->query($sql);
        }
     }

     public function insertClient($datos){
        $fields = null;
        $values = null;
        foreach ($datos as $key => $value) {
            $fields.= $fields == NULL ? $key : ", $key";
            switch(gettype($value)){
                case "boolean":
                case "integer":
                case "double":
                    $values.= $values == NULL ? $value : ", $value";
                break;

                default:
                    $values.= $values == NULL ? $value : ", '$value'";
                break;
            }            
        }

        $sql = "INSERT INTO tbl_usuario ($fields) VALUES ($values)";
        //die($sql);
        $result = $this->res_con->query($sql);

        return array('result' => $result, 'idUsuario' => $this->res_con->insert_id);
        //die($sql); 
     }
    
    //FUNCION INSERTA PASSWORD DE LA CUENTA    
    public function inserta_pass($idUsuario, $usuario, $pass, $nombre, $telefono, $email, $rfc, $tarjeta, $edad = NULL, $genero = NULL, $encuesta = NULL){
        //$this->res_con=$this->conecta_bd($ser,$usu,$pas,$bd,$con);
        $qr_inserta_pass=" UPDATE tbl_usuario SET password='$pass', usuario = '$usuario', nombre = '$nombre', telefono = '$telefono', email = '$email', rfc = '$rfc', tarjeta = '$tarjeta', token = NULL, genero = $genero, fecha_nacimiento = '$edad', encuesta = '$encuesta' WHERE id_usuario = $idUsuario ";
        //die($qr_inserta_pass);
        $respo=$this->res_con->query($qr_inserta_pass);
        //echo $qr_inserta_pass;

        //SI SE ACTUALIZO BIEN LA INFORMACION SE ENVIA NOTIFICACION
        if($respo){
            require "../../inc/parametros.php";
            include_once '../../inc/funciones.php';
            //OBTENEMOS CONFIGURACION
            $qri_config="SELECT * FROM tbl_configuracion_proyecto";
            $resp_consulta_qri_config=$this->res_con->query($qri_config);
            $reg_config= mysqli_fetch_assoc($resp_consulta_qri_config);
            $tipo_comuni_registro=$reg_config['id_tipo_comunicacion_registro'];
            $tipo_comunicado=explode('-',$tipo_comuni_registro);
            $inst_basicas=new Funciones_Basicas();
            for($i=0;$i<=count($tipo_comunicado)-1;$i++){
                switch ($tipo_comunicado[$i]){
                    case 1:
                        //ENVIA SMS                        
                        $msg="Felicidades $nombre!, has concluido tu registro correctamente. Ya puedes consultar toda tu informacion en el siguiente link: www.desclub.com.mx/izlah";
                        $inst_basicas->envia_sms($msg, $telefono, $usu_sms, $pas_sms, $url_sms);
                    break;
                    case 2:
                        //ENVIA MAIL                       
                        $mensa="<h3>Registro completo.  </h3> ¡Felicidades <b>$nombre</b>!, has concluido tu registro a la plataforma de forma correcta. <br> Ahora ya podrás consultar tu historial de compras, recompensas y/o promociones disponibles en el siguiente link: <a target='_blank' href='www.desclub.com.mx/izlah'>www.desclub.com.mx/izlah</a> 
                        <hr><h5><font color='#8c8c8c' size='1'>** Este correo es generado de forma automatica por la Plataforma de Lealtad, no es necesario que lo respondas **</font></h5>";                  
                        $inst_basicas->envia_mail($usu_mail, $pas_mail, $from_mail,$email, 'Bienvenido a la plataforma de lealtad - Izlah',$mensa);
                    break;

                    default:
                    break;
                }
            }
        }

        return $respo ? 1 : 0;
    }

    //FUNCION VERIFICAR USUARIO
    public function verifyUser($username = NULL){
        if($username != NULL){
            $sql=" SELECT * FROM tbl_usuario WHERE usuario = '$username' ";
            return $this->arrayToAssoc($this->res_con->query($sql));
        }

        return false;
    }

    //FUNCION VERIFICAR EMAIL
    public function verifyMail($username = NULL){
        if($username != NULL){
            $sql=" SELECT * FROM tbl_usuario WHERE email = '$username' ";
            return $this->arrayToAssoc($this->res_con->query($sql));
        }

        return false;
    }

    //FUNCION VERIFICAR CELULAR
    public function verifyTel($username = NULL){
        if($username != NULL){
            $sql=" SELECT * FROM tbl_usuario WHERE telefono = $username ";
            return $this->arrayToAssoc($this->res_con->query($sql));
        }

        return false;
    }

    public function sendNotification($idUsuario = NULL, $mensajeSMS = '', $tituloMail = '', $mensajeMail = ''){
        require "../../inc/parametros.php";
        include_once '../../inc/funciones.php';

        if($idUsuario != NULL){
            //Obtenemos info del usuario
            $sql="SELECT * FROM tbl_usuario WHERE id_usuario = $idUsuario ";
            //die($sql);
            $resultSQL=$this->res_con->query($sql);

            //OBTENEMOS CONFIGURACION
            $qri_config="SELECT * FROM tbl_configuracion_proyecto";
            $resp_consulta_qri_config=$this->res_con->query($qri_config);
            $reg_config= mysqli_fetch_assoc($resp_consulta_qri_config);
            $tipo_comuni_registro=$reg_config['id_tipo_comunicacion_registro'];
            $tipo_comunicado=explode('-',$tipo_comuni_registro);
            $inst_basicas=new Funciones_Basicas();
            
            while ($user = mysqli_fetch_assoc($resultSQL)) {
                for($i=0;$i<=count($tipo_comunicado)-1;$i++){
                    switch ($tipo_comunicado[$i]){
                        case 1:
                            //ENVIA SMS
                            //$msg="Felicidades $nombre!, has concluido tu registro correctamente. Ya puedes consultar toda tu informacion en el siguiente link: www.desclub.com.mx/izlah";
                            $telefono = $user['telefono'];
                            $inst_basicas->envia_sms($mensajeSMS, $telefono, $usu_sms, $pas_sms, $url_sms);

                        break;
                        case 2:
                            //ENVIA MAIL
                            $email = $user['email'];
                            $inst_basicas->envia_mail($usu_mail, $pas_mail, $from_mail,$email, $tituloMail, $mensajeMail);
                        break;
                    }
                }
            }

            return true;
        }
        return false;
        
    }

    public function generarToken(){        
        return str_shuffle(uniqid());
    }

    public function updateUser($idUsuario = NULL, $data = array()){
        if($idUsuario != NULL && count($data) > 0){
            $fields = NULL;
            foreach ($data as $key => $value) {

                switch(gettype($value)){
                    case "boolean":
                    case "integer":
                    case "double":
                        $fields.= $fields == NULL ? "$key = $value" : ", $key = $value";
                    break;

                    default:
                        $fields.= $fields == NULL ? "$key = '$value'" : ", $key = '$value'";
                    break;
                }
            }
            $sql = " UPDATE tbl_usuario SET $fields WHERE id_usuario = $idUsuario ";
            //die($sql);
            return $this->res_con->query($sql);
        }
    }

    private function arrayToAssoc($result){
        $auxArray = array();
        while ($fila = mysqli_fetch_assoc($result)) {
            foreach ($fila as $key => $value) {
                $fila[$key] = utf8_encode($value);
            }
            array_push($auxArray, $fila);
        }
        return $auxArray;
    }
    
    //FUNCION CONECTA A BASE DE DATOS
    public function conecta_bd($ser,$usu,$pas,$bd){
        $con = mysqli_connect($ser,$usu,$pas,$bd);
        if ($con){
            return $con;
            mysqli_close($con);
        }else{
            echo("Error description: " . mysqli_error($con));
            exit(); 
        }
    }   
}
